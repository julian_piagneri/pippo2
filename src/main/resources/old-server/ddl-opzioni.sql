--------------------------------------------------------
--  DDL for Sequence OPZIONI_APPLICAZIONE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "DIGHE_2X"."OPZIONI_APPLICAZIONE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table OPZIONI_APPLICAZIONE
--------------------------------------------------------

  CREATE TABLE "DIGHE_2X"."OPZIONI_APPLICAZIONE" 
   (	"ID" NUMBER, 
	"PWD_EXPIRE" NUMBER DEFAULT 30
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into DIGHE_2X.OPZIONI_APPLICAZIONE
SET DEFINE OFF;
Insert into DIGHE_2X.OPZIONI_APPLICAZIONE (ID,PWD_EXPIRE) values ('1','30');
--------------------------------------------------------
--  DDL for Index OPZIONI_APPLICAZIONE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "DIGHE_2X"."OPZIONI_APPLICAZIONE_PK" ON "DIGHE_2X"."OPZIONI_APPLICAZIONE" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table OPZIONI_APPLICAZIONE
--------------------------------------------------------

  ALTER TABLE "DIGHE_2X"."OPZIONI_APPLICAZIONE" ADD CONSTRAINT "OPZIONI_APPLICAZIONE_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "DIGHE_2X"."OPZIONI_APPLICAZIONE" MODIFY ("PWD_EXPIRE" NOT NULL ENABLE);
  ALTER TABLE "DIGHE_2X"."OPZIONI_APPLICAZIONE" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  DDL for Trigger OPZIONI_APPLICAZIONE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DIGHE_2X"."OPZIONI_APPLICAZIONE_TRG" 
BEFORE INSERT ON OPZIONI_APPLICAZIONE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT OPZIONI_APPLICAZIONE_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "DIGHE_2X"."OPZIONI_APPLICAZIONE_TRG" ENABLE;
ALTER TABLE OPZIONI_APPLICAZIONE ADD (WARNING_DAYS NUMBER DEFAULT 3 NOT NULL);

ALTER TABLE OPZIONI_APPLICAZIONE RENAME COLUMN PWD_EXPIRE TO KEY;

ALTER TABLE OPZIONI_APPLICAZIONE RENAME COLUMN WARNING_DAYS TO VALUE;

ALTER TABLE OPZIONI_APPLICAZIONE  
MODIFY (KEY VARCHAR2(4000 BYTE) );

ALTER TABLE OPZIONI_APPLICAZIONE  
MODIFY (VALUE VARCHAR2(4000 BYTE) );

ALTER TABLE OPZIONI_APPLICAZIONE
ADD CONSTRAINT OPZIONI_APPLICAZIONE_UK1 UNIQUE 
(
  KEY 
)
ENABLE;

ALTER TABLE OPZIONI_APPLICAZIONE  
MODIFY (KEY_NAME DEFAULT NULL );

ALTER TABLE OPZIONI_APPLICAZIONE  
MODIFY (KEY_VALUE DEFAULT NULL );

INSERT INTO "DIGHE_2X"."OPZIONI_APPLICAZIONE" (KEY_NAME, KEY_VALUE) VALUES ('PWD_EXPIRE', '30');
INSERT INTO "DIGHE_2X"."OPZIONI_APPLICAZIONE" (KEY_NAME, KEY_VALUE) VALUES ('WARNING_DAYS', '3');
