<%@ page contentType="text/html"%>
<%@ page import="utility.*,it.thematica.enterprise.app.login.*,java.util.*"%>
<%@ page import="it.thematica.dighe.util.*,it.thematica.enterprise.sys.ResultStruct.*"%>
<%@ page session="true" %>
<%@ page errorPage="../Condivisi/PaginaErrore.jsp?debug=log"%>
<% request.setAttribute("sourcePage",request.getRequestURI());%>
<html>
<head><title>JSP Page</title></head>
<body style="cursor:wait">
<SCRIPT>
var y;
var newSite=false;
if (navigator.userAgent.indexOf("MSIE")>0)
      y=window.top.dialogArguments.main;
else
      y=window.top.opener.top.main;
	  

//Se y � vuoto allora � la nuova finestra
if(typeof y.document === "undefined"){
	y=window.top.opener;
	//console.log(y);
	newSite=true;
	
}



function sessioneScaduta()
{
   y.kill(window.top);
}

function adeguaLarghezza(font)
{
 var k=0;
var y;
if (navigator.userAgent.indexOf("MSIE")>0)
      y=window.top.dialogArguments.main;
else
      y=window.top.opener.top.main;
 if(font==16)
      k=100;
    else 
       if(font==18)
            k=120;
     y.document.all['riempi'].innerHTML='';
   for(var i=0;i<k;i++)
     y.document.all['riempi'].innerHTML+='&nbsp;';
}
   
</SCRIPT>
<% String browser=request.getHeader("User-Agent");
   response.setDateHeader("Expires", 0);
   String pageName="Scelta Aspetto Utente";
   UserRightsEntry edu = (UserRightsEntry)session.getAttribute("EDU");
   if (edu==null)
        out.print("<SCRIPT>sessioneScaduta();</SCRIPT>"); 
   else {
   JSPConnector.getInstance().updateUserVisitedArea((String)session.getAttribute("Intid"), pageName);
   String font=request.getParameter("font");
   String color=request.getParameter("colori");
   String sfondo=request.getParameter("sfondo");
   String icone=request.getParameter("icone");
   char s,i;
      if (sfondo!=null) s='S';
      else s='N';
         if (icone!=null) i='S';
         else i='N';
     String col="";
     String Filecss="FILECSS_"+font+"_"+color+"_"+s+"_"+i;      
     String msg="N";
     String pref=JSPConnector.getInstance().setAspetto(Filecss,edu.getPreferences());
     edu.setPreferences(pref); 
     if(JSPConnector.getInstance().setUserPreferences(edu.getId(),pref))
       msg="S";
     session.setAttribute("EDU",edu);
%>
<SCRIPT>
	if(!newSite){
     <% if (browser.indexOf("MSIE")>0){
			out.println("y.document.TabellaApplet.ricaricaCSS();");
			out.println("window.top.dialogArguments.app.location.reload();");
			out.println("adeguaLarghezza("+font+");");
		} else {
			out.println("y.document.TabellaApplet.ricaricaCSS();");
			out.println("window.top.opener.top.app.location.reload();");
			out.println("adeguaLarghezza("+font+");");   
		}
      %>
	} else {
		y.changeStyles(<% out.print("\""+font+"\", \""+color+"\""); %>);
	}
    location.href='Aspetto.jsp?Msg=<%=msg%>';
</SCRIPT>
<%}%>
</body>
</html>
