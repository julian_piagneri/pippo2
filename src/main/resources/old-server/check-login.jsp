﻿<%@ page import="java.util.*,java.io.*, java.util.Map.*"%>
<%@ page import="it.thematica.enterprise.app.login.*"%>
<%
//Controlla se l'utente è loggato
response.setDateHeader("Expires", 0);
UserRightsEntry edu = (UserRightsEntry)session.getAttribute("EDU");
if (edu==null){
	//We can't use 401 or 403 error because the angular TokenStorage will think we are not logged in
	response.sendError(500, "You are not authorized to view the requested component");
}
%>