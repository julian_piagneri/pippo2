--------------------------------------------------------
--  File creato - giovedÃ¬-aprile-30-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence UTENTE_VISTE_BASE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "DIGHE_2X"."UTENTE_VISTE_BASE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table UTENTE_VISTE_BASE
--------------------------------------------------------

  CREATE TABLE "DIGHE_2X"."UTENTE_VISTE_BASE" 
   (	"ID" NUMBER, 
	"VIEWCONTENT" CLOB DEFAULT '{}'
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" 
 LOB ("VIEWCONTENT") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
REM INSERTING into DIGHE_2X.UTENTE_VISTE_BASE
SET DEFINE OFF;
Insert into DIGHE_2X.UTENTE_VISTE_BASE (ID) values ('1');
--------------------------------------------------------
--  DDL for Index UTENTE_VISTE_BASE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "DIGHE_2X"."UTENTE_VISTE_BASE_PK" ON "DIGHE_2X"."UTENTE_VISTE_BASE" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table UTENTE_VISTE_BASE
--------------------------------------------------------

  ALTER TABLE "DIGHE_2X"."UTENTE_VISTE_BASE" ADD CONSTRAINT "UTENTE_VISTE_BASE_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "DIGHE_2X"."UTENTE_VISTE_BASE" MODIFY ("VIEWCONTENT" NOT NULL ENABLE);
  ALTER TABLE "DIGHE_2X"."UTENTE_VISTE_BASE" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  DDL for Trigger UTENTE_VISTE_BASE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DIGHE_2X"."UTENTE_VISTE_BASE_TRG" 
BEFORE INSERT ON UTENTE_VISTE_BASE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT UTENTE_VISTE_BASE_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "DIGHE_2X"."UTENTE_VISTE_BASE_TRG" ENABLE;
--------------------------------------------------------
--  DML for UTENTE_VISTE_BASE
--------------------------------------------------------

declare

vClob varchar(80000);

begin

vClob := '{"showFilter":{"select": 0, "rid": true, "accertamento": false, "progetto": false, "competenza": false, "altro": false}, "mapOptions":{ "moveMap": true, "filterMap": true}, "status" : {"columns":[{"name":"groupingRowHeaderCol","visible":false,"width":30,"sort":{},"filters":[{}]},{"name":"numeroArchivio","visible":true,"width":70,"sort":{},"filters":[{}]},{"name":"sub","visible":true,"width":50,"sort":{},"filters":[{}]},{"name":"nomeDiga","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"bacino","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"funzSedeCentrale","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"funzUffPerif","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"uffSedeCentrale","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"uffPeriferico","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"status1Sigla","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"status2Sigla","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"altezzaDigaL584","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"volumeTotInvasoL584","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"quotaAutoriz","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.nomeRegione","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.nomeProvincia","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.nomeComune","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.utilizzo.value.utilizzo","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.utilizzo.value.priorita","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"nomeFiumeSbarrato","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomeFiumeValle.value","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.zonaSismicaComune","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.comuneOperaPresa.value","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"nomeLago","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"annoConsegnaLavori","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"annoUltimazLavori","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"dataCertCollaudo","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"altezzaDigaDm","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"descrClassDiga","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"quotaMaxRegolaz","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"volumeTotInvasoDm","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"volumeLaminazione","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"volumeAutoriz","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"portMaxPienaProgetto","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"condotteForzate","visible":false,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.tipoOrganoScarico.value","visible":false,"width":200,"sort":{},"filters":[{}]}],"scrollFocus":{},"selection":[{"identity":false,"row":0}],"grouping":{"grouping":[],"aggregations":[]},"treeView":{}}}';

INSERT INTO UTENTE_VISTE_BASE (ID, VIEWCONTENT) VALUES (1, vClob);

end;

--------------------------------------------------------
--  DDL for Sequence SCHEDA_PRIVILEGI_EXTRA_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "DIGHE_2X"."SCHEDA_PRIVILEGI_EXTRA_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SCHEDA_EXTRA_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "DIGHE_2X"."SCHEDA_EXTRA_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table SCHEDA_PRIVILEGI_EXTRA
--------------------------------------------------------

  CREATE TABLE "DIGHE_2X"."SCHEDA_PRIVILEGI_EXTRA" 
   (	"ID" NUMBER, 
	"ID_SCHEDA" NUMBER, 
	"ID_GRUPPO" NUMBER, 
	"ORDINE" NUMBER(10,0) DEFAULT -1 NOT NULL
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table SCHEDA_EXTRA
--------------------------------------------------------

  CREATE TABLE "DIGHE_2X"."SCHEDA_EXTRA" 
   (	"ID" NUMBER, 
	"ID_SCHEDA" NUMBER, 
	"HREF" VARCHAR2(1000 BYTE) DEFAULT '#' NOT NULL, 
	"IS_SCHEDA" CHAR(1 BYTE) DEFAULT 'N' NOT NULL, 
	"KEY" VARCHAR2(20 BYTE),
	"TYPE" NUMBER DEFAULT 1 NOT NULL, 
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;

--------------------------------------------------------
--  DDL for Index SCHEDA_EXTRA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "DIGHE_2X"."SCHEDA_EXTRA_PK" ON "DIGHE_2X"."SCHEDA_EXTRA" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table SCHEDA_PRIVILEGI_EXTRA
--------------------------------------------------------

  ALTER TABLE "DIGHE_2X"."SCHEDA_PRIVILEGI_EXTRA" ADD CONSTRAINT "SCHEDA_PRIVILEGI_DEFAULT_E_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "DIGHE_2X"."SCHEDA_PRIVILEGI_EXTRA" MODIFY ("ID_GRUPPO" NOT NULL ENABLE);
  ALTER TABLE "DIGHE_2X"."SCHEDA_PRIVILEGI_EXTRA" MODIFY ("ID_SCHEDA" NOT NULL ENABLE);
  ALTER TABLE "DIGHE_2X"."SCHEDA_PRIVILEGI_EXTRA" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SCHEDA_EXTRA
--------------------------------------------------------

  ALTER TABLE "DIGHE_2X"."SCHEDA_EXTRA" ADD CONSTRAINT "SCHEDA_EXTRA_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "DIGHE_2X"."SCHEDA_EXTRA" MODIFY ("ID_SCHEDA" NOT NULL ENABLE);
  ALTER TABLE "DIGHE_2X"."SCHEDA_EXTRA" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table SCHEDA_PRIVILEGI_EXTRA
--------------------------------------------------------

  ALTER TABLE "DIGHE_2X"."SCHEDA_PRIVILEGI_EXTRA" ADD CONSTRAINT "SCHEDA_PRIVILEGI_EXTRA_FK1" FOREIGN KEY ("ID_GRUPPO", "ID_SCHEDA")
	  REFERENCES "DIGHE_2X"."SCHEDA_PRIVILEGI_DEFAULT" ("ID_GRUPPO", "ID_SCHEDA") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SCHEDA_EXTRA
--------------------------------------------------------

  ALTER TABLE "DIGHE_2X"."SCHEDA_EXTRA" ADD CONSTRAINT "SCHEDA_EXTRA_FK1" FOREIGN KEY ("ID_SCHEDA")
	  REFERENCES "DIGHE_2X"."SCHEDA" ("ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  DDL for Trigger SCHEDA_PRIVILEGI_EXTRA_TRG
--------------------------------------------------------

CREATE OR REPLACE TRIGGER "DIGHE_2X"."SCHEDA_PRIVILEGI_EXTRA_TRG" 
BEFORE INSERT ON SCHEDA_PRIVILEGI_EXTRA 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT SCHEDA_PRIVILEGI_EXTRA_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "DIGHE_2X"."SCHEDA_PRIVILEGI_EXTRA_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SCHEDA_EXTRA_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DIGHE_2X"."SCHEDA_EXTRA_TRG" 
BEFORE INSERT ON SCHEDA_EXTRA 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT SCHEDA_EXTRA_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "DIGHE_2X"."SCHEDA_EXTRA_TRG" ENABLE;

CREATE SEQUENCE  "DIGHE_2X"."UTENTE_VISTA_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 61 CACHE 20 NOORDER  NOCYCLE ;


CREATE TABLE "DIGHE_2X"."UTENTE_VISTA" 
(
	"ID" NUMBER NOT NULL ENABLE, 
	"NAME" VARCHAR2(1000 BYTE), 
	"DEFAULT_VIEW" NUMBER DEFAULT 0, 
	"STATUS" CLOB NOT NULL ENABLE, 
	"ID_UTENTE" NUMBER NOT NULL ENABLE, 
	CONSTRAINT "UTENTE_VISTA_PK" PRIMARY KEY ("ID"), 
	CONSTRAINT "UTENTE_VISTA_FK1" FOREIGN KEY ("ID_UTENTE") REFERENCES "DIGHE_2X"."UTENTE" ("ID") ON DELETE CASCADE ENABLE
);

CREATE OR REPLACE TRIGGER "DIGHE_2X"."UTENTE_VISTA_TRG" 
BEFORE INSERT ON "DIGHE_2X"."UTENTE_VISTA"  
FOR EACH ROW 
BEGIN
	<<COLUMN_SEQUENCES>>
	BEGIN
		IF INSERTING AND :NEW.ID IS NULL THEN
			SELECT UTENTE_VISTA_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
		END IF;
	END COLUMN_SEQUENCES;
END;
/

ALTER TRIGGER "DIGHE_2X"."UTENTE_VISTA_TRG" ENABLE;
