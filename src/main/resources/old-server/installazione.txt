Inserire i jar di jackson (core, annotations, databind) in C:\dighe_2x\bin\www\WEB-INF\lib
Inserire applet-params.jsp e check-login.jsp in C:\dighe_2x\bin\www\jsp\
ATTENZIONE: URL hardcoded all'interno della jsp, deve puntare a dammap/api/menu/getDam

Inserire SalvaAspetto.jsp in C:\dighe_2x\bin\www\jsp\Profilo
Inserire ipServer.properties in C:\dighe_2x\bin\www\WEB-INF\classes\it\thematica\dighe\properties

Il file server.xml deve sostituire la configurazione base di tomcat 6 (nella cartella di tomcat, sottocartella "conf").
Se si vuole attivare solo la compressione e la porta 80 basta sostituire al connector di default (solitamente <Connector port="8080"...) con
quello qui presente:

    <Connector port="80" protocol="HTTP/1.1" 
               connectionTimeout="20000" 
               redirectPort="8443" 
			   compression="4096"
			   compressionMinSize="4096"
			   noCompressionUserAgents="gozilla, traviata"
			   compressableMimeType="application/json,application/xml,text/html,text/xml,text/plain"
			   />


DDL:
ddl-drop: cancella le colonne che abbiamo aggiunto in scheda e scheda privilegi default
ddl-create: crea le nuove tabelle
insert-dati: inserisce i dati nelle nuove tabelle
insert-select (NON NECESSARIA): inserisce i dati a partire da una select sulle vecchie colonne
utente-extra: crea la tabella UTENTE_EXTRA contenente le password criptate (algoritmo bcrypto a 13 passaggi e salt casuale autogenerato)
 