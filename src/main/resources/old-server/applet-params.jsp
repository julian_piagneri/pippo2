<%@ page import="java.util.*,java.io.*, java.util.Map.*"%>
<%@ page import="it.thematica.enterprise.app.login.*"%>
<%@ page import="it.thematica.enterprise.sql.ConnectionReference"%>
<%@ page import="it.thematica.enterprise.sql.DBConnection"%>
<%@ page import="it.thematica.enterprise.sql.PreparedStatementReference"%>
<%@ page import="java.io.BufferedReader"%>
<%@ page import="java.io.DataOutputStream"%>
<%@ page import="java.io.InputStreamReader"%>
<%@ page import="java.net.HttpURLConnection"%>
<%@ page import="java.net.URL" %>
<%@ page import="javax.net.ssl.HttpsURLConnection"%>
<%@ page import="com.fasterxml.jackson.core.*" %>
<%@ page import="com.fasterxml.jackson.databind.*" %>
<%@ page import="com.fasterxml.jackson.databind.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>

<%@ page import="java.io.Serializable" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>	
<%!

	public void updateIdScheda(String schedaKey, long idScheda) throws SQLException {
		
		String sql = "";
		PreparedStatementReference ps = null;
		ConnectionReference connection=null;
		
		try {
			
			sql="UPDATE SCHEDA_EXTRA SET KEY=? WHERE ID_SCHEDA=?";
			/*
			Geoanagrafica: 13
			Geologia: 23
			Posizione Amministrativa: 33
			Caratteristiche Costruttive: 43
			*/
		
			//SET QUERY PARAM
			connection=DBConnection.lockConnection();
			
			ps=DBConnection.getPreparedStatement(connection, sql); //dbUtil.getConnection().prepareStatement(sql);
			int i=1;
			ps.setString(i++, schedaKey);
			ps.setLong(i++, idScheda);
			//EXECUTE
			ps.executeUpdate();
			
		} catch (SQLException sqle) {
			throw sqle;
		} finally {
			if (connection != null) {
				DBConnection.releaseConnection(connection);
			}
		}
	}

	public String retrieveIdScheda(String schedaKey) throws SQLException {
		
		String sql = "";
		ResultSet rs = null;
		PreparedStatementReference ps = null;
		Long idScheda=new Long(-1);
		ConnectionReference connection=null;
		
		try {
			//SELECT
			sql =" SELECT ID_SCHEDA AS idScheda FROM "+
				" SCHEDA_EXTRA a "+
				" WHERE a.key=? "
				;
		
			//SET QUERY PARAM
			connection=DBConnection.lockConnection();
			
			ps=DBConnection.getPreparedStatement(connection, sql); //dbUtil.getConnection().prepareStatement(sql);
			int i=1;
			ps.setString(i++, schedaKey);
			//EXECUTE
			rs=ps.executeQuery();
			if(rs.next()){
				idScheda=rs.getLong("idScheda");
			}
			
		} catch (SQLException sqle) {
			throw sqle;
		} finally {
			if (connection != null) {
				DBConnection.releaseConnection(connection);
			}
			if(rs!=null){
				rs.close();
			}
		}
		
		return idScheda.toString();
	}

	public ArrayList<HashMap<String, String>> retrieveSingleDam(String userId, String numeroArchivio, String sub) throws SQLException {
		
		String sql = "";
		ResultSet rs = null;
		PreparedStatementReference ps = null;
		ArrayList<HashMap<String, String>> allElements;
		ConnectionReference connection=null;
		
		try {
			//SELECT
			sql =" SELECT V_ELENCO_DIGHE_SINGOLI.* FROM "+
				" V_ELENCO_DIGHE_SINGOLI "+
				" INNER JOIN  "+
				" (SELECT DISTINCT NUMERO_ARCHIVIO,SUB FROM ACCESSO_DEFAULT "+
				" WHERE ID_GRUPPO IN "+
				" (SELECT ID_GRUPPO FROM UTENTE_GRUPPO WHERE ID_UTENTE = ? ) ) AD "+
				" ON V_ELENCO_DIGHE_SINGOLI.NUMERO_ARCHIVIO=AD.NUMERO_ARCHIVIO AND V_ELENCO_DIGHE_SINGOLI.SUB=AD.SUB "+
				" WHERE V_ELENCO_DIGHE_SINGOLI.NUMERO_ARCHIVIO=? AND V_ELENCO_DIGHE_SINGOLI.SUB=? "
				;
		
			//SET QUERY PARAM
			connection=DBConnection.lockConnection();
			
			ps=DBConnection.getPreparedStatement(connection, sql); //dbUtil.getConnection().prepareStatement(sql);
			int i=1;
			ps.setString(i++, userId);
			ps.setString(i++, numeroArchivio);
			ps.setString(i++, sub);
			//EXECUTE
			rs=ps.executeQuery();
			allElements=this.fillDamDataListForApplet(rs);
			
		} catch (SQLException sqle) {
			throw sqle;
		} finally {
			if (connection != null) {
				DBConnection.releaseConnection(connection);
			}
			if(rs!=null){
				rs.close();
			}
		}
		
		return allElements;
	}
	
	private HashMap<String, String> fillDamDataForApplet(ResultSet rset) throws SQLException{
		HashMap<String, String> damDetails=new HashMap<String, String>();
		int i=1;
		String parametri="ParametriApplet";
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getInt("NUMERO_ARCHIVIO")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("SUB")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("COMPETENZA_SND")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("NOME_DIGA")));
		String status=rset.getString("STATUS_PRINCIPALE")+" - "+rset.getString("STATUS_SECONDARIO")+" ";
		status+=rset.getString("DESCR_STATUS_PRINCIPALE")+" - "+rset.getString("DESCR_STATUS_SECONDARIO");
		damDetails.put(parametri+(i++), this.emptyIfNull(""+status));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("NUM_ISCRIZIONE_RID")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("UFFICIO_SEDE_CENTRALE")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("UFFICIO_PERIFERICO")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("FUNZIONARIO_SEDE_CENTRALE")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("FUNZIONARIO_UFF_PERIFERICO")));
		return damDetails;
	}
	
	private String emptyIfNull(String str){
		if(str==null || str.equals("null") || str.trim().equals("")){
			return "";
		}
		return str;
	}
	
	private ArrayList<HashMap<String, String>> fillDamDataListForApplet(ResultSet rs) throws SQLException{
		ArrayList<HashMap<String, String>> values= new ArrayList<HashMap<String, String>>();
		while (rs.next()) {
			values.add(this.fillDamDataForApplet(rs));
		}
		return values;
	}
%>
<%
//Controlla se l'utente � loggato
response.setDateHeader("Expires", 0);
UserRightsEntry edu = (UserRightsEntry)session.getAttribute("EDU");
boolean skipCheck=false;
if (!skipCheck && edu==null){
	response.sendError(401, "You are not authorized to view the requested component");
} else {
	ArrayList<HashMap<String, String>> damDetailsList=new ArrayList<HashMap<String, String>>();
	if(!skipCheck){
		String userId=""+edu.getId(); //request.getParameter("userid");
		String archivio=request.getParameter("archivio");
		String subArch=request.getParameter("subarch");
		damDetailsList = retrieveSingleDam(userId, archivio, subArch); //mapper.readValue(strResponse.toString(), new ArrayList<HashMap<String, String>>().getClass());
	} else {
		out.println("MODALITA' DEBUG ATTIVA<br/>");
		damDetailsList = retrieveSingleDam("1", "1", "_");
	}
	List<String> allowedParams=new ArrayList<String>();
	for(int iz=1; iz<=10; iz++){
		allowedParams.add("ParametriApplet"+iz);
	}

	for (HashMap<String, String> damDetails : damDetailsList) {
		Set<Entry<String, String>> entrySet=damDetails.entrySet();
		for (Entry<String, String> entry : entrySet) {
			if(allowedParams.contains(entry.getKey())){
				session.setAttribute(entry.getKey(), entry.getValue());
				out.println(entry.getKey()+": "+session.getAttribute(entry.getKey())+"<br/>");
			}
		}
	}
	updateIdScheda("GEOANAGRAFICA", 13);
	updateIdScheda("GEOLOGIA_GEOTECNICA", 23);
	updateIdScheda("POS_AMM", 33);
	updateIdScheda("CAR_COSTR", 43);

	String geoanagrafica=retrieveIdScheda("GEOANAGRAFICA");
	out.println("Geoanagrafica: "+geoanagrafica);
	
	String geologia=retrieveIdScheda("GEOLOGIA_GEOTECNICA");
	out.println("Geologia: "+geologia);
	
	String posAmm=retrieveIdScheda("POS_AMM");
	out.println("Pos Amm: "+posAmm);
	
	String carCostr=retrieveIdScheda("CAR_COSTR");
	out.println("Car Costr: "+carCostr);
	
	String invaso=retrieveIdScheda("INVASO");
	session.setAttribute("CodiceInvaso", invaso);
	out.println("Invaso: "+session.getAttribute("CodiceInvaso"));
	
	String scarichi=retrieveIdScheda("SCARICHI");
	session.setAttribute("CodiceScarichi", scarichi);
	out.println("Scarichi: "+session.getAttribute("CodiceScarichi"));
	
	String informazioni=retrieveIdScheda("INFORMAZIONI");
	session.setAttribute("CodiceInformazioni", informazioni);
	out.println("Informazioni: "+session.getAttribute("CodiceInformazioni"));
	
	String rubrica=retrieveIdScheda("RUBRICA");
	session.setAttribute("CodiceRubrica", rubrica);
	out.println("Rubrica: "+session.getAttribute("CodiceRubrica"));
	
	String procedimenti=retrieveIdScheda("PROCEDIMENTI");
	session.setAttribute("CodiceProcedimenti", procedimenti);
	out.println("Procedimenti: "+session.getAttribute("CodiceProcedimenti"));
	
	String[] gtpc={geoanagrafica, geologia, posAmm, carCostr, invaso, scarichi, informazioni, rubrica, procedimenti};
	session.setAttribute("GTPC",gtpc);
}
%>