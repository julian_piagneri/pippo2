﻿window.ngGrid.i18n['it'] = {
    ngAggregateLabel: 'valori',
    ngGroupPanelDescription: "Trascina l'intestazione di una colonna in questo spazio e rilasciala per raggruppare le righe in base a questo valore.",
    ngSearchPlaceHolder: 'Ricerca...',
    ngMenuText: 'Segli le Colonne:',
    ngShowingItemsLabel: 'Righe Mostrate:',
    ngTotalItemsLabel: 'Righe Totali:',
    ngSelectedItemsLabel: 'Righe Selezionate:',
    ngPageSizeLabel: 'Righe per Pagina:',
    ngPagerFirstTitle: 'Prima Pagina',
    ngPagerNextTitle: 'Pagina Successiva',
    ngPagerPrevTitle: 'Pagina Precedente',
    ngPagerLastTitle: 'Ultima Pagina'
};