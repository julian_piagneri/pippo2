//tutte le occorrenze di $dialogs sono state rinominate in dialog per funzionare con dialog 5

anagrafModule.controller('fenFranosiController', ['$http', '$scope', '$filter', 'getREST', 'ngTableParams', '$rootScope', 'dialogs', 'ngDialog', function ($http, $scope, $filter, getREST, ngTableParams, $rootScope, dialogs, ngDialog) {
	
	$.extend($scope, G_Common_Scope);
	$scope.getREST = getREST;
	// $scope.ngTableParams = ngTableParams;
	// $scope.dialogs = dialogs;
	$scope.fenFranosiGruppi = {};
	$scope.fenFranosiSchede = {};
	$scope.currentFrana = "";
	$scope.nomeNuovaFrana = "";
	$scope.editFrana = {};
	
	/* 
	"id" : "1",
	"descrizioneGruppo" : "Interessa le spalle diga",

	"id" : "2",
	"descrizioneGruppo" : "Interessa il corpo diga",

	"id" : "3",
	"descrizioneGruppo" : "Interessa le opere complementari",

	"id" : "4",
	"descrizioneGruppo" : "Interessa le sponde",

	"id" : "5",
	"descrizioneGruppo" : "Interessata dall\u0027invaso",

	"id" : "6",
	"descrizioneGruppo" : "Sopra il livello di invaso",

	"id" : "7",
	"descrizioneGruppo" : "Materiale_Roccia",

	"id" : "8",
	"descrizioneGruppo" : "Materiale_Sciolti",

	"id" : "9",
	"descrizioneGruppo" : "Tipologia del movimento",

	"id" : "10",
	"descrizioneGruppo" : "Censimento_10",

	"id" : "11",
	"descrizioneGruppo" : "Censimento_freccia",

	"id" : "12",
	"descrizioneGruppo" : "Innescata/riattivata durante",
	 */
	
	$scope.spalleDigaGr = {};
	$scope.corpoDigaGr = {};
	$scope.opereComplementariGr = {};
	$scope.spondeGr = {};
	$scope.invasoGr = {};
	$scope.livelloInvasoGr = {};
	$scope.matRocciaGr = {};
	$scope.matScioltiGr = {};
	$scope.movimentoGr = {};
	$scope.cinematismoGr = {};
	$scope.cinematismoFrecciaGr = {};
	$scope.cinematismoInnescataGr = {};
	
	
	$scope.empty = "";
	// $scope.maxAssocListNumber = 5;
	$scope.siNo = [
		{value: 'S', text: 'Sì'},
		{value: 'N', text: 'No'}
	]; 
	
	$scope.movimentoImage = ['crollo.png', 'ribaltamento.png', 'scorrimentoT.png', 'scorrimentoR.png', 'colata.png', 'creep.png'];
	
	$scope.materialiMeasures = ['0', '0,002mm', '0,06mm', '2cm', '6cm'];
	$scope.cinematismoMeasure = ['0', '10 mm/anno', '1 m/anno', '10 m/anno', '1 m/ora'];
	
	
	$scope.movimentoImage
	
	/* 
	$scope.movimentoImage = function(_ind){
		return $scope.movimentoImage[_ind];
	};
	 */
	$scope.setGruppoSN = function(_gruppo){
		if(_gruppo.attivo) _gruppo.sn = "S";
		else _gruppo.sn = "N";
	};
	
	$scope.setCheckSN = function(_check){
		if(_check.attivo) _check.isChecked = "S";
		else _check.isChecked = "N";
	};
	
	
	$scope.getTextFromValue = function(_val, _lookup){
		for(var key in _lookup) if(_lookup[key].value == _val) return _lookup[key].text;
		return "";
	};
	
	$scope.initGruppoCheck = function(_gruppo){
		if(_gruppo.sn == 'S') _gruppo.attivo = true;
		else _gruppo.attivo = false;
		
		for(var key in _gruppo.fenDigheCheck){
			if(_gruppo.fenDigheCheck[key].isChecked == 'S') _gruppo.fenDigheCheck[key].attivo = true;
			else _gruppo.fenDigheCheck[key].attivo = false;
		}
	};
	
	
	$scope.saveGruppoCheck = function(_gruppo){
		var esclus = (_gruppo.esclusivo == "S");
		var nSelect = 0;
		
		$scope.setGruppoSN(_gruppo);
		for(var key in _gruppo.fenDigheCheck){
			if(_gruppo.fenDigheCheck[key].attivo) nSelect++;
			$scope.setCheckSN(_gruppo.fenDigheCheck[key]);
		}
		if(esclus && nSelect > 1){
			alert("Esclusiv");
			return "";
		}
		else getREST.restPut($scope.fenomeniFranosiGruppoREST + '/' +  $scope.currentFrana, _gruppo);
		
	};
	
	$scope.checkEsclusivo = function(_gruppo, _check){
		if(_gruppo.esclusivo == "N") return;
		if(!_check.attivo) return;
		
		for(var key in _gruppo.fenDigheCheck){
			if(_gruppo.fenDigheCheck[key] != _check) _gruppo.fenDigheCheck[key].attivo = false;
		}
		
	};
	
	$scope.initEditFrana = function(){
		$scope.editFrana = {};
		
		for(var key in $scope.fenFranosiSchede){
			if($scope.fenFranosiSchede[key].idSchedaFenomeniFranosi == $scope.currentFrana) $scope.editFrana = $scope.fenFranosiSchede[key];
		}
		
	}
	
	$scope.editNuovaFrana = function(_form){
		// console.log($scope.editFrana);
		
		var callBackFunction = (function(_getREST){return function(_resp){
			_getREST.restJson($scope.fenomeni_franosiREST + '?damID=' +  G_damID);  
		};})(getREST);
		
		/* 
		var dati = {
			descrizione: $scope.editFrana.descrizione
		};
		 */
		
		getREST.restPut($scope.fenomeni_franosiREST + "/" + $scope.editFrana.idSchedaFenomeniFranosi, {descrizione: $scope.editFrana.descrizione}, _form, callBackFunction);
		return "";
	};
	
	$scope.resetNomeNuovaFrana = function(){
		$scope.nomeNuovaFrana = "";
	};
	
	$scope.saveNuovaFrana = function(_form){
		/* 
		var callBackFunction = (function(_getREST){return function(_resp){
			_getREST.restJson($scope.portateNoteREST + "?damID=" + G_damID);
		};})(getREST);
		*/
		
		/* 
		var callBackFunction = (function(_getREST, _form){return function(_resp){
			
			var callBackFunction2  = (function(_getREST){return function(_resp){
				// _getREST.restJson($scope.fenomeni_franosiREST + '?damID=' +  G_damID);
			};})(_getREST);
			
			_getREST.restPut($scope.portateNoteREST + "?damID=" + G_damID, {}, _form, callBackFunction2);
		};})(getREST, _form);
		*/
		
		// var dati = {note: $scope.portateNote};
		
		// console.log($scope.nomeNuovaFrana);
		// if($scope.nomeNuovaFrana == "") console.log(1);
		// if($scope.nomeNuovaFrana == null) console.log(2);
		// if(typeof $scope.nomeNuovaFrana == 'undefined') console.log(3);
		
		if($scope.nomeNuovaFrana == "" || $scope.nomeNuovaFrana == null || typeof $scope.nomeNuovaFrana == 'undefined') return "";
		
		// getREST.restPost($scope.fenomeni_franosiREST + '/' +  G_damID, {}, _form, $scope.fenomeni_franosiREST + '?damID=' +  G_damID);
		// getREST.restPost($scope.fenomeni_franosiREST + '/' +  G_damID, {id_new: 0}, _form);
		getREST.restPost($scope.fenomeni_franosiREST + '/' +  G_damID, {id_new: 0, descrizione:$scope.nomeNuovaFrana}, _form);
		
		return "";
	};
	
	$scope.deleteFrana = function(){
		
		
		var callBackFunction = (function(_getREST){return function(_resp){
			_getREST.restJson($scope.fenomeni_franosiREST + '?damID=' +  G_damID);  
		};})(getREST);
		
		
		var dlg = dialogs.confirm('Conferma cancellazione','Sei sicuro di voler cancellare? La scheda frana verr&agrave; cancellata definitivamente.');
		dlg.result.then(function(btn){
			getREST.restDelete($scope.fenomeni_franosiREST + "/" + $scope.currentFrana, [], 0, callBackFunction);
			/* 
			for(var key in $scope.fenFranosiSchede){
				if($scope.fenFranosiSchede[key].idSchedaFenomeniFranosi == $scope.currentFrana) getREST.restDelete($scope.fenomeni_franosiREST + "/" + $scope.editFrana.idSchedaFenomeniFranosi, [], 0, callBackFunction);
			}
			*/
		},function(btn){
			
		});
		
		// getREST.restDelete($scope.fenomeni_franosiREST + "/" + $scope.editFrana.idSchedaFenomeniFranosi, [], 0, callBackFunction);
	};
	
	$scope.switchSchedaById = function(_id){
		for(var key in $scope.fenFranosiSchede){
			if($scope.fenFranosiSchede[key].idSchedaFenomeniFranosi == _id) $scope.switchScheda(key);
		}
	};
	
	$scope.switchScheda = function(_index){
	
		$scope.fenFranosiGruppi = $scope.fenFranosiSchede[_index].fenomeniFranosi[0].fenDigheGruppi;
		
		console.log(_index);
		
		
		for(var key in $scope.fenFranosiGruppi){
			$scope.fenFranosiGruppi[key].sn.onEdit = false;
			
			if($scope.fenFranosiGruppi[key].sn == 'S') $scope.fenFranosiGruppi[key].attivo = true;
			else $scope.fenFranosiGruppi[key].attivo = false;
			
			for(var key2 in $scope.fenFranosiGruppi[key].fenDigheCheck){
				if($scope.fenFranosiGruppi[key].fenDigheCheck[key2].isChecked == 'S') $scope.fenFranosiGruppi[key].fenDigheCheck[key2].attivo = true;
				else $scope.fenFranosiGruppi[key].fenDigheCheck[key2].attivo = false;
			}
			
			switch($scope.fenFranosiGruppi[key].id){
				case "1":
					$scope.spalleDigaGr = $scope.fenFranosiGruppi[key];
					break;
				case "2":
					$scope.corpoDigaGr = $scope.fenFranosiGruppi[key];
					break;
				case "3":
					$scope.opereComplementariGr = $scope.fenFranosiGruppi[key];
					break;
				case "4":
					$scope.spondeGr = $scope.fenFranosiGruppi[key];
					break;
				case "5":
					$scope.invasoGr = $scope.fenFranosiGruppi[key];
					break;
				case "6":
					$scope.livelloInvasoGr = $scope.fenFranosiGruppi[key];
					break;
				case "7":
					$scope.matRocciaGr = $scope.fenFranosiGruppi[key];
					break;
				case "8":
					$scope.matScioltiGr = $scope.fenFranosiGruppi[key];
					break;
				case "9":
					$scope.movimentoGr = $scope.fenFranosiGruppi[key];
					break;
				case "10":
					$scope.cinematismoGr = $scope.fenFranosiGruppi[key];
					break;
				case "11":
					$scope.cinematismoFrecciaGr = $scope.fenFranosiGruppi[key];
					break;
				case "12":
					$scope.cinematismoInnescataGr = $scope.fenFranosiGruppi[key];
					break;
				default:
					break;
			}
		}
		
	};
	
	
	$scope.$on('RESTreceived', function() {
		
		var path = $scope.fenomeni_franosiREST + '?damID=' + G_damID;
		if(getREST.results[path]){
			if(getREST.results[path].newDataZZ){
				
				$scope.fenFranosiSchede = getREST.results[path];
				// console.log($scope.fenFranosiSchede);
				
				// console.log($scope.currentFrana);
				
				var currentFranaOK = false;
				if($scope.currentFrana == ""){
					for(var key in $scope.fenFranosiSchede){
						if($scope.fenFranosiSchede[key].idSchedaFenomeniFranosi == $scope.currentFrana) currentFranaOK = true;
					}
				}
				
				if(!currentFranaOK){
					if($scope.fenFranosiSchede.length > 0){
						$scope.currentFrana = $scope.fenFranosiSchede[0].idSchedaFenomeniFranosi;
						$scope.switchScheda(0);
					}
				}
				else $scope.switchSchedaById($scope.currentFrana);
				
				getREST.results[path].newDataZZ = false;
			}
		}
		
		var path = $scope.fenomeni_franosiREST + '/' +  G_damID + '/new/0';
		if(getREST.results[path]){
			if(getREST.results[path].newDataZZ){
				// console.log(getREST.results[path]);
				// getREST.restPut($scope.fenomeni_franosiREST + "/" + getREST.results[path].idFen, {descrizione:$scope.nomeNuovaFrana});
				
				getREST.restJson($scope.fenomeni_franosiREST + '?damID=' +  G_damID); 
				getREST.results[path].newDataZZ = false;
			}
		}
		
	});
	
	/* 
	getREST.restJson($scope.provinciaREST);
	getREST.restJson($scope.regioneREST);
	getREST.restJson($scope.tipiContattoREST);
	getREST.restJson($scope.tipiAltroEnteREST);
	getREST.restJson($scope.tipiAutoritaREST);
	 */
	
	// $rootScope.$broadcast('TypeLoadCompleted');
	
	
	getREST.restJson($scope.fenomeni_franosiREST + '?damID=' +  G_damID);  
	
	$scope.dati = {};	
	var par = {
			method: 'GET',
			url: ($scope.testataREST + "?damID=" + G_damID),
			headers: {
				'X-AUTH-TOKEN' : G_token
			}
	}
	
	$http(par).success(function(response) {
		$scope.dati = response;
	}).error(function(response) {
		
	});	
	
}]);

