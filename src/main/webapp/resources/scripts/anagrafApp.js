//tutte le occorrenze di $dialogs sono state rinominate in dialog per funzionare con dialog 5

anagrafModule.controller('anagrafController', ['$scope', '$filter', 'getREST', 'ngTableParams', '$rootScope', 'dialogs', 'ngDialog', function ($scope, $filter, getREST, ngTableParams, $rootScope, dialogs, ngDialog) {
	
	$.extend($scope, G_Common_Scope);
	$scope.getREST = getREST;
	$scope.ngTableParams = ngTableParams;
	$scope.dialogs = dialogs;
	
	$scope.empty = "";
	$scope.maxAssocListNumber = 5;
	
	$scope.nuovoConcess = {};
	$scope.nuovoConcess.$visible = false;
	
	$scope.nuovoGestore = {};
	$scope.nuovoGestore.$visible = false;
	
	$scope.nuovaAutorita = {};
	$scope.nuovaAutorita.$visible = false;
	
	$scope.nuovoEnte = {};
	$scope.nuovoEnte.$visible = false;
	
	
	
	$scope.reloadTableConcess = false;
	$scope.reloadTableGestori = false;
	$scope.reloadTableIngegneri = false;
	$scope.reloadTableAutorita = false;
	$scope.reloadTableAltraEnte = false;
	$scope.reloadTableAssocTo = false;
	$scope.reloadTableAssocFrom = false;
	
	
	$scope.show_hide_Concess_details = function(_concess){
		if(_concess.onEdit) return;
		if(_concess.isC_details){
			_concess.gestoriAssoc = $scope.gestori;
			getREST.restJson($scope.concessionarioREST+"/"+_concess.id);
			/* 
			getREST.restJson($scope.concessionarioAssocREST.replace("?", _concess.id));
			getREST.restJson($scope.concessionarioDigheREST.replace("?", _concess.id));
			getREST.restJson($scope.concessionarioUtentiREST.replace("?", _concess.id));
			 */
		}
		else _concess.isC_details = true;
	};
	
	$scope.show_hide_Gestore_details = function(_gestore){
		if(_gestore.onEdit) return;
		if(_gestore.isC_details){
			// _gestore.concessAssoc = $scope.concessionari;
			getREST.restJson($scope.gestoreREST+"/"+_gestore.id);
			/* 
			getREST.restJson($scope.gestoreAssocREST.replace("?", _gestore.id));
			getREST.restJson($scope.gestoreDigheREST.replace("?", _gestore.id));
			getREST.restJson($scope.gestoreUtentiREST.replace("?", _gestore.id));
			 */
		}
		else _gestore.isC_details = true;
	};
	
	$scope.show_hide_Ingegnere_details = function(_ingegnere){
		if(_ingegnere.isC_details) getREST.restJson($scope.ingegnereREST+"/"+_ingegnere.id);
		else _ingegnere.isC_details = true;
	};
	
	$scope.show_hide_Autorita_details = function(_autorita){
		if(_autorita.onEdit) return;
		if(_autorita.isC_details){
			getREST.restJson($scope.autoritaREST+"/"+_autorita.id);
			/* 
			getREST.restJson($scope.autoritaDigheREST.replace("?", _autorita.id));
			getREST.restJson($scope.autoritaUtentiREST.replace("?", _autorita.id));
			 */
		}
		else _autorita.isC_details = true;
	};
	
	$scope.show_hide_Altro_Ente_details = function(_ente){
		if(_ente.onEdit) return;
		if(_ente.isC_details){
			getREST.restJson($scope.altraEnteREST+"/"+_ente.id);
			/* 
			getREST.restJson($scope.altraEnteDigheREST.replace("?", _ente.id));
			getREST.restJson($scope.altraEnteUtentiREST.replace("?", _ente.id));
			 */
		}
		else _ente.isC_details = true;
	};
	
	/* 
	$scope.deleteContatto = function(_keyREST, _idSogg, _contatto, _sogg){
		var dlg = dialogs.confirm('Conferma cancellazione','Sei sicuro di voler cancellare? Il conttato verr&agrave; cancellato definitivamente.');
		dlg.result.then(function(btn){
			// $scope.confirmed = 'You thought this quite awesome!';
			getREST.restDelete(_keyREST+"/"+_idSogg+"/contatto/"+_contatto.id, _sogg.details.contatto, _contatto.id);
		},function(btn){
			// $scope.confirmed = 'Shame on you for not thinking this is awesome!';
		});
		// getREST.restDelete(_keyREST+"/"+_idSogg+"/contatto/"+_contatto.id, _sogg.details.contatto, _contatto.id);
	};
	 */
	/* 
	$scope.deleteIndirizzo = function(_keyREST, _idSogg, _indirizzo, _sogg){
		var dlg = dialogs.confirm('Conferma cancellazione','Sei sicuro di voler cancellare? L\'indirizzo verr&agrave; cancellato definitivamente.');
		dlg.result.then(function(btn){
			getREST.restDelete(_keyREST+"/"+_idSogg+"/indirizzo/"+_indirizzo.id, _sogg.details.indirizzo, _indirizzo.id);
		},function(btn){
			
		});
	};
	 */
	
	$scope.addConcessionario = function(_form){
		
		$scope.tableConcess;
	
		$scope.nuovoConcess.nome = null;
		$scope.nuovoConcess.$visible = true;
		
		_form.$show();
		
	};
	
	$scope.addGestore = function(_form){
		$scope.nuovoGestore.nome = null;
		$scope.nuovoGestore.$visible = true;
		
		_form.$show();
		
	};
	
	$scope.addAutorita = function(_form){
		$scope.nuovaAutorita.descrizione = null;
		$scope.nuovaAutorita.siglaProvincia = null;
		$scope.nuovaAutorita.nomeRegione = null;
		$scope.nuovaAutorita.tipoAutorita = null;
		
		$scope.nuovaAutorita.$visible = true;
		
		_form.selectedRegione = "-";
		_form.$show();
	};
	
	$scope.addAltroEnte = function(_form){
		$scope.nuovoEnte.nome = null;
		$scope.nuovoEnte.tipoEnte = null;
		$scope.nuovoEnte.$visible = true;
		
		_form.$show();
	};
	
	$scope.deleteConcess = function(_idSogg){
		var dlg = dialogs.confirm('Conferma cancellazione','Sei sicuro di voler cancellare? Il concessionario verr&agrave; cancellato definitivamente.');
		dlg.result.then(function(btn){
			$scope.reloadTableConcess = true;
			getREST.restDelete($scope.concessionarioREST+"/"+_idSogg, $scope.concessionari, _idSogg);
		},function(btn){
			
		});
	};
	
	$scope.deleteAutorita = function(_idSogg){
		var dlg = dialogs.confirm('Conferma cancellazione','Sei sicuro di voler cancellare? L\'aurit&agrave; di protezione civile verr&agrave; cancellata definitivamente.');
		dlg.result.then(function(btn){
			$scope.reloadTableAutorita = true;
			getREST.restDelete($scope.autoritaREST+"/"+_idSogg, $scope.autorita, _idSogg);
		},function(btn){
			
		});
	};
	
	$scope.deleteAltroEnte = function(_idSogg){
		var dlg = dialogs.confirm('Conferma cancellazione','Sei sicuro di voler cancellare? L\'ente verr&agrave; cancellato definitivamente.');
		dlg.result.then(function(btn){
			$scope.reloadTableAltraEnte = true;
			getREST.restDelete($scope.altraEnteREST+"/"+_idSogg, $scope.altriEnti, _idSogg);
		},function(btn){
			
		});
	};
	
	$scope.deleteGestore = function(_idSogg){
		var dlg = dialogs.confirm('Conferma cancellazione','Sei sicuro di voler cancellare? Il gestore verr&agrave; cancellato definitivamente.');
		dlg.result.then(function(btn){
			$scope.reloadTableGestori = true;
			getREST.restDelete($scope.gestoreREST+"/"+_idSogg, $scope.gestori, _idSogg);
		},function(btn){
			
		});
	};
	
	
	$scope.saveConcess = function(_concess, _form){
		
		var callBackFunction = (function(_concess){return function(_resp){
			$scope.removeOnEdit(_concess);
		};})(_concess);
		
		var dati_cl = $.extend(true, {}, _concess);
		delete dati_cl.details;
		delete dati_cl.contatto;
		delete dati_cl.indirizzo;
		// $scope.removeOnEdit(_concess);
		getREST.restPut($scope.concessionarioREST+"/"+_concess.id, dati_cl, _form, callBackFunction);
		$scope.reloadTableConcess = true;
		return "";
	}
	
	$scope.saveAutorita = function(_autorita, _form){
		
		
		var callBackFunction = (function(_autorita){return function(_resp){
			$scope.removeOnEdit(_autorita);
		};})(_autorita);
		
		
		var dati_cl = $.extend(true, {}, _autorita);
		delete dati_cl.details;
		delete dati_cl.contatto;
		delete dati_cl.indirizzo;
		// $scope.removeOnEdit(_autorita);
		getREST.restPut($scope.autoritaREST+"/"+_autorita.id, dati_cl, _form, callBackFunction);
		$scope.reloadTableAutorita = true;
		return "";
	}
	
	$scope.saveAltroEnte = function(_altroEnte, _form){
		
		var callBackFunction = (function(_altroEnte){return function(_resp){
			$scope.removeOnEdit(_altroEnte);
		};})(_altroEnte);
		
		var dati_cl = $.extend(true, {}, _altroEnte);
		delete dati_cl.details;
		delete dati_cl.contatto;
		delete dati_cl.indirizzo;
		// $scope.removeOnEdit(_altroEnte);
		getREST.restPut($scope.altraEnteREST+"/"+_altroEnte.id, dati_cl, _form, callBackFunction);
		$scope.reloadTableAltraEnte = true;
		return "";
	}
	
	$scope.saveGestore = function(_gestore, _form){
		
		var callBackFunction = (function(_gestore){return function(_resp){
			$scope.removeOnEdit(_gestore);
		};})(_gestore);
		
		var dati_cl = $.extend(true, {}, _gestore);
		delete dati_cl.details;
		delete dati_cl.contatto;
		delete dati_cl.indirizzo;
		// $scope.removeOnEdit(_gestore);
		getREST.restPut($scope.gestoreREST+"/"+_gestore.id, dati_cl, _form, callBackFunction);
		$scope.reloadTableGestori = true;
		return "";
	}
	
	$scope.saveNuovoGestore = function(_form){
		// alert("dsf");
		$scope.nuovoGestore.orderNum = 1;
		// $scope.nuovoGestore.numIscrizioneRid = 0;
		$scope.nuovoGestore.id_new = 1;
		$scope.reloadTableGestori = true;
		getREST.restPost($scope.gestoreREST, $scope.nuovoGestore, _form);
		return "";
	};
	
	$scope.saveNuovoConcess = function(_form){
		// alert("dsf");
		/* 
		var callBackFunction = (function(_sogg, _keyREST){return function(response, state){
			alert("edit");
		};})(_sogg, _keyREST);
		 */
		
		$scope.nuovoConcess.orderNum = 1;
		$scope.nuovoConcess.numIscrizioneRid = 0;
		$scope.nuovoConcess.id_new = 1;
		$scope.reloadTableConcess = true;
		getREST.restPost($scope.concessionarioREST, $scope.nuovoConcess, _form);
		return "";
	};
	
	$scope.saveNuovaAutorita = function(_form){
		// alert("dsf");
		$scope.nuovaAutorita.orderNum = 1;
		// $scope.nuovaAutorita.numIscrizioneRid = 0;
		$scope.nuovaAutorita.id_new = 1;
		$scope.reloadTableAutorita = true;
		getREST.restPost($scope.autoritaREST, $scope.nuovaAutorita, _form);
		return "";
	};
	
	$scope.saveNuovoEnte = function(_form){
		// alert("dsf");
		$scope.nuovoEnte.orderNum = 1;
		$scope.nuovoEnte.id_new = 1;
		$scope.reloadTableAltraEnte = true;
		getREST.restPost($scope.altraEnteREST, $scope.nuovoEnte, _form);
		return "";
	};
	
	$scope.checkCampoObb = function(_data){
		var _errore = "Campo Obbligatorio";
		if(typeof _data === 'undefined') return _errore;
		if(_data == "") return _errore;
		if(_data == null) return _errore;
		return true;
	};
	
	$scope.checkCAP = function(_data){
		var _errore = "La lunghezza del CAP � di massimo 5 caratteri";
		if(_data.length > 5) return _errore;
		return true;
	};
	
	$scope.assocSogg = function(event, ui, _sogg, _keyREST, _assocDigheKeyREST){
		if($scope.dragObject.FromToType == 'to') return false;
		// alert(_assocDigheKeyREST);
		var callBackFunction = function(){
			$scope.assocTo.push($scope.dragObject);
			for(var key in $scope.assocFrom){
				if($scope.assocFrom[key].id == $scope.dragObject.id){
					$scope.assocFrom.splice(key, 1);
					break;
				}
			}
			$scope.tableAssocTo.reload();
			$scope.tableAssocFrom.reload();
			if(_assocDigheKeyREST) getREST.restJson(_assocDigheKeyREST.replace("?", $scope.dragObject.id));
		};
		getREST.restPost(_keyREST.replace("?", _sogg.id) + "/" + $scope.dragObject.id, {}, null, _keyREST.replace("?", _sogg.id), callBackFunction);
		return true;
	};
	
	$scope.delAssocSogg = function(event, ui, _sogg, _keyREST, _assocDigheKeyREST){
	
		if($scope.dragObject.FromToType == 'from') return false;
		
		var callBackFunction = (function(_sogg, _keyREST, _assocDigheKeyREST){return function(response, state){
			// alert("cb");
			// return;
			
			getREST.restJson(_keyREST.replace("?", _sogg.id));
			
			//// Ricarica le dighe dell'eventuale gestore ////
			
			////////
			
			for (var key in $scope.assocTo){
				if($scope.assocTo[key].id == $scope.dragObject.id){
					$scope.assocTo.splice(key, 1);
					break;
				}
			}
			$scope.assocFrom.push($scope.dragObject);
			$scope.tableAssocTo.reload();
			$scope.tableAssocFrom.reload();
			if(_assocDigheKeyREST) getREST.restJson(_assocDigheKeyREST.replace("?", $scope.dragObject.id));
		};})(_sogg, _keyREST, _assocDigheKeyREST);
		
		// alert($scope.dragObject.id);
		var localFct = (function(_sogg, _keyREST, callBackFunction){return function(){
			getREST.restDelete(_keyREST.replace("?", _sogg.id) + "/" + $scope.dragObject.id, {}, 0, callBackFunction);
		};})(_sogg, _keyREST, callBackFunction);
		
		
		
		if($scope.assocDelAlertFct != $scope.empty){
			$scope.assocDelAlertFct(localFct);
		}
		else localFct();
		
		// getREST.restDelete(_keyREST.replace("?", _sogg.id) + "/" + $scope.dragObject.id, {}, 0, callBackFunction);
		
		
		return true;
	};
	
	$scope.getDragOgg = function(event, ui, _obj, _type){
		// alert(_obj.nome);
		$scope.dragObject = _obj;
		$scope.dragObject.FromToType = _type;
	};
	
	$scope.getDighe2Remove = function(_gestore, _concess){
		
		var _altriConcess = _gestore.details.concessionari.slice(0);
		for(var key in _altriConcess){
			if(_altriConcess[key].id == _concess.id){
				_altriConcess.splice(key, 1);
				break;
			}
		}
		
		var _offDighe = $scope.getDigheFromConcessArray(_altriConcess);
		
		var _arr = new Array();
		_arr.push(_concess);
		var _concessDighe = $scope.getDigheFromConcessArray(_arr);
		
		var _dighe2Remove = new Array();
		for(var key in _gestore.details.dighe){
			var _digaOff = true;
			for(var key2 in _offDighe){
				if(_offDighe[key2].id == _gestore.details.dighe[key].id){
					_digaOff = false;
					continue;
				}
			}
			if(_digaOff){
				_digaOff = false;
				for(var key2 in _concessDighe){
					if(_concessDighe[key2].id == _gestore.details.dighe[key].id){
						_digaOff = true;
						continue;
					}
				}
			}
			if(_digaOff) _dighe2Remove.push(_gestore.details.dighe[key]);
		}
		
		return _dighe2Remove;
	};
	
	$scope.alertDelAssocGestoreConcess = function(_Fct){
		
		_dighe2Remove = $scope.getDighe2Remove($scope.assocMainSogg, $scope.dragObject);
		if(_dighe2Remove.length == 0) _Fct();
		else{
			var popupString = 'Sei sicuro di voler dissociare il concessionario? Le seguenti dighe verranno dissociate dal gestore: ';
			for(var key in _dighe2Remove){
				popupString += _dighe2Remove[key].denominazione + ', ';
			}
			popupString = popupString.substr(0, popupString.length - 2);
			var dlg = dialogs.confirm('Conferma dissociazione', popupString);
			dlg.result.then(function(btn){
				_Fct();
			},function(btn){
				
			});
		}
	};
	
	$scope.alertDelAssocConcessGestore = function(_Fct){
		
		var _currGestore = {};
		for(var key in $scope.gestori){
			if($scope.gestori[key].id){
				if($scope.gestori[key].id == $scope.dragObject.id) _currGestore = $scope.gestori[key];
			}
		}
		
		
		_dighe2Remove = $scope.getDighe2Remove(_currGestore, $scope.assocMainSogg);
		if(_dighe2Remove.length == 0) _Fct();
		else{
			var popupString = 'Sei sicuro di voler dissociare il gestore? Le seguenti dighe gli verranno dissociate: ';
			for(var key in _dighe2Remove){
				popupString += _dighe2Remove[key].denominazione + ', ';
			}
			popupString = popupString.substr(0, popupString.length - 2);
			var dlg = dialogs.confirm('Conferma dissociazione', popupString);
			dlg.result.then(function(btn){
				_Fct();
			},function(btn){
				
			});
		}
	};
	
	$scope.openPopupAssoc = function(_to, _from, _display, _keyREST, _assocDigheKeyREST, _sogg, _titolo, _sTitolo1, _sTitolo2, _order, _alertFct){
		// alert("ddd");
		// if($scope.assocDelAlertFct != $scope.empty) _alertFct();
		
		$scope.assocTo = _to.slice(0);
		$scope.assocFrom = _from.slice(0);
		$scope.assocKeyREST = _keyREST;
		$scope.assocDigheKeyREST = _assocDigheKeyREST;
		$scope.assocMainSogg = _sogg;
		$scope.assocTitolo = _titolo;
		$scope.assocsTitolo1 = _sTitolo1;
		$scope.assocsTitolo2 = _sTitolo2;
		$scope.assocDelAlertFct = _alertFct;
		
		// alert($scope.assocFrom.length);
		for(var key in $scope.assocTo){
			$scope.assocTo[key].displayName = $scope.assocTo[key][_display];
			for(var key2 in $scope.assocFrom){
				if($scope.assocTo[key].id == $scope.assocFrom[key2].id){
					$scope.assocFrom.splice(key2, 1);
					break;
				}
			}
		
		}
		for(var key in $scope.assocFrom) $scope.assocFrom[key].displayName = $scope.assocFrom[key][_display];
		
		$scope.tableAssocTo = $scope.AssocTable($scope.assocTo, _order, $filter);
		$scope.tableAssocFrom = $scope.AssocTable($scope.assocFrom, _order, $filter);
		
		// ngDialog.open({ className: 'ngdialog-theme-plain ngdialog-assoc-table', controller: 'anagrafController', scope: $scope, template: 'assocTemplate'});
		ngDialog.open({ className: 'ngdialog-theme-plain ngdialog-assoc-table', scope: $scope, template: 'assocTemplate'});
	};
	
	
	$scope.openPopupViewList = function(_to, _display, _titolo, _sTitolo1, _order){
		
		$scope.assocTo = _to.slice(0);
		$scope.assocTitolo = _titolo;
		$scope.assocsTitolo1 = _sTitolo1;
		
		for(var key in $scope.assocTo){
			$scope.assocTo[key].displayName = $scope.assocTo[key][_display];
		}
		
		$scope.tableAssocTo = $scope.AssocTable($scope.assocTo, _order, $filter);
		
		ngDialog.open({ className: 'ngdialog-theme-plain ngdialog-assoc-table', scope: $scope, template: 'viewListTemplate'});
	};
	
	$scope.getDigaOrder = function(_id){
		var l = _id.length;
		var _ret = _id;
		for(var ii = l ; ii < 10 ; ii++) _ret = '0' + _ret;
		return _ret;
	};
	
	$scope.getDigheFromConcessArray = function(_userArray){
		// alert("ok");
		var _ret = new Array();
		for(var key in _userArray){
			if(typeof _userArray[key].id === 'undefined') continue;
			var _id = _userArray[key].id;
			for(var key2 in $scope.concessionari){
				if($scope.concessionari[key2].id == _id){
					if($scope.concessionari[key2].details.dighe){
						for(var key3 in $scope.concessionari[key2].details.dighe){
							if($scope.concessionari[key2].details.dighe[key3]){
								var _new = true;
								for(var key4 in _ret){
									if(_ret[key4].id == $scope.concessionari[key2].details.dighe[key3].id) _new = false;
								}
								if(_new) _ret.push($scope.concessionari[key2].details.dighe[key3]);
							}
						}
					}
					break;
				}
			}
		}
		// return $scope.dighe;
		return _ret;
	};
	
	
	$scope.$on('TypeLoadCompleted', function() {
		getREST.restJson($scope.concessionarioREST);
		getREST.restJson($scope.gestoreREST);
		getREST.restJson($scope.ingegnereREST);
		getREST.restJson($scope.autoritaREST);
		getREST.restJson($scope.altraEnteREST);
		getREST.restJson($scope.digheREST);
		getREST.restJson($scope.utentiREST);
	});
	
	
	$scope.nomeTipoContatto = function(_idTipo){
		var selected = $filter('filter')($scope.tipiContatto, {id: _idTipo});
		return (_idTipo) ? selected[0].nome : '';
	};
	
	$scope.AnagraficaTable = function(_array, _order){
		var _obj = new ngTableParams({page: 1, count: 10
		}, {
			getData: function($defer, params) {
				var _data = params.filter() ? $filter('filter')(_array, params.filter()) : _array;
				// _data = $filter('orderBy')(_data, 'nome', false);
				_data = $filter('orderBy')(_data, _order, false);
				var _pp = params.page();
				var _pc = params.count();
				var _nn = (_pp - 1) * _pc;
				if(_nn >= _data.length && _pp > 1) params.page(_pp - 1);
				params.total(_data.length);
				// alert(params.count());
				$defer.resolve(_data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
			}
		});
		return _obj;
	};
	
	$scope.getInsertedContattoREST = function(_array, _path){
		// _array = $scope.concessionari[key];
		// _path = $scope.concessionarioREST;
		
		for(var key2 in _array.details.contatto){
			// var concessPath = _path+"/"+_array.id+"/contatto/"+_array.details.contatto[key2].id_new;
			var concessPath = _path+"/"+_array.id+"/contatto/new/"+_array.details.contatto[key2].id_new;
			if(getREST.results[concessPath]){
				if(getREST.results[concessPath].newDataZZ){
					_array.details.contatto[key2] = getREST.results[concessPath];
					getREST.results[concessPath].newDataZZ = false;
				}
			}
		}
		
	};
	
	$scope.getInsertedIndirizzoREST = function(_array, _path){
		// _array = $scope.concessionari[key];
		// _path = $scope.concessionarioREST;
		
		for(var key2 in _array.details.indirizzo){
			// var concessPath = _path+"/"+_array.id+"/indirizzo/"+_array.details.indirizzo[key2].id_new;
			var concessPath = _path+"/"+_array.id+"/indirizzo/new/"+_array.details.indirizzo[key2].id_new;
			if(getREST.results[concessPath]){
				if(getREST.results[concessPath].newDataZZ){
					_array.details.indirizzo[key2] = getREST.results[concessPath];
					getREST.results[concessPath].newDataZZ = false;
				}
			}
		}
		
	};
	
	
	$scope.$on('RESTreceived', function() {
		for(var key in $scope.concessionari){
			var concessPath = $scope.concessionarioREST+"/"+$scope.concessionari[key].id;
			if(getREST.results[concessPath]){
				if(getREST.results[concessPath].newDataZZ){
					$scope.concessionari[key].details = getREST.results[concessPath];
					$scope.concessionari[key].isC_details = false;
					getREST.results[concessPath].newDataZZ = false;
					
					getREST.restJson($scope.concessionarioAssocREST.replace("?", $scope.concessionari[key].id));
					getREST.restJson($scope.concessionarioDigheREST.replace("?", $scope.concessionari[key].id));
					getREST.restJson($scope.concessionarioUtentiREST.replace("?", $scope.concessionari[key].id));
					
				}
			}
			
			concessPath = $scope.concessionarioAssocREST.replace("?", $scope.concessionari[key].id);
			if(getREST.results[concessPath]){
				if(getREST.results[concessPath].newDataZZ){
					// alert("ciao");
					if(typeof $scope.concessionari[key].details === 'undefined') $scope.concessionari[key].details = {};
					$scope.concessionari[key].details.gestori = getREST.results[concessPath];
					// alert($scope.concessionari[key].details.gestori.length);
					// $scope.concessionari[key].isC_details = false;
					getREST.results[concessPath].newDataZZ = false;
					for(var key2 in $scope.concessionari[key].details.gestori){
						if($scope.concessionari[key].details.gestori[key2].id) getREST.restJson($scope.gestoreDigheREST.replace("?", $scope.concessionari[key].details.gestori[key2].id));
						if($scope.concessionari[key].details.gestori[key2].id) getREST.restJson($scope.gestoreAssocREST.replace("?", $scope.concessionari[key].details.gestori[key2].id));
					}
				}
			}
			
			concessPath = $scope.concessionarioDigheREST.replace("?", $scope.concessionari[key].id);
			if(getREST.results[concessPath]){
				if(getREST.results[concessPath].newDataZZ){
					// alert("ciao");
					if(typeof $scope.concessionari[key].details === 'undefined') $scope.concessionari[key].details = {};
					$scope.concessionari[key].details.dighe = getREST.results[concessPath];
					
					for(var key2 in $scope.concessionari[key].details.dighe){
						$scope.concessionari[key].details.dighe[key2].denominazione = $scope.concessionari[key].details.dighe[key2].numeroArchivio + $scope.concessionari[key].details.dighe[key2].sub + ' ' + $scope.concessionari[key].details.dighe[key2].nomeDiga;
						if($scope.concessionari[key].details.dighe[key2].id) $scope.concessionari[key].details.dighe[key2].orderId = $scope.getDigaOrder($scope.concessionari[key].details.dighe[key2].id);
					}
					
					// alert($scope.concessionari[key].details.gestori.length);
					// $scope.concessionari[key].isC_details = false;
					getREST.results[concessPath].newDataZZ = false;
				}
			}
			
			concessPath = $scope.concessionarioUtentiREST.replace("?", $scope.concessionari[key].id);
			if(getREST.results[concessPath]){
				if(getREST.results[concessPath].newDataZZ){
					$scope.concessionari[key].details.utenti = getREST.results[concessPath];
					for(var key2 in $scope.concessionari[key].details.utenti) $scope.concessionari[key].details.utenti[key2].denominazione = $scope.concessionari[key].details.utenti[key2].cognome + ' ' + $scope.concessionari[key].details.utenti[key2].nome;
					// $scope.concessionari[key].isC_details = false;
					getREST.results[concessPath].newDataZZ = false;
				}
			}
			
			if($scope.concessionari[key].details === undefined) continue;
			if($scope.concessionari[key].details.contatto !== undefined) $scope.getInsertedContattoREST($scope.concessionari[key], $scope.concessionarioREST);
			if($scope.concessionari[key].details.indirizzo !== undefined) $scope.getInsertedIndirizzoREST($scope.concessionari[key], $scope.concessionarioREST);
			
		}
		for(var key in $scope.gestori){
			var gestorePath = $scope.gestoreREST+"/"+$scope.gestori[key].id;
			if(getREST.results[gestorePath]){
				if(getREST.results[gestorePath].newDataZZ){
					// alert("OK");
					$scope.gestori[key].details = getREST.results[gestorePath];
					$scope.gestori[key].isC_details = false;
					getREST.results[gestorePath].newDataZZ = false;
					
					getREST.restJson($scope.gestoreAssocREST.replace("?", $scope.gestori[key].id));
					getREST.restJson($scope.gestoreDigheREST.replace("?", $scope.gestori[key].id));
					getREST.restJson($scope.gestoreUtentiREST.replace("?", $scope.gestori[key].id));
				}
			}
			
			gestorePath = $scope.gestoreAssocREST.replace("?", $scope.gestori[key].id);
			if(getREST.results[gestorePath]){
				if(getREST.results[gestorePath].newDataZZ){
					if(typeof $scope.gestori[key].details === 'undefined') $scope.gestori[key].details = {};
					$scope.gestori[key].details.concessionari = getREST.results[gestorePath];
					// $scope.gestori[key].isC_details = false;
					getREST.results[gestorePath].newDataZZ = false;
					for(var key2 in $scope.gestori[key].details.concessionari){
						if($scope.gestori[key].details.concessionari[key2].id) getREST.restJson($scope.concessionarioDigheREST.replace("?", $scope.gestori[key].details.concessionari[key2].id));
					}
				}
			}
			gestorePath = $scope.gestoreDigheREST.replace("?", $scope.gestori[key].id);
			if(getREST.results[gestorePath]){
				if(getREST.results[gestorePath].newDataZZ){
					if(typeof $scope.gestori[key].details === 'undefined') $scope.gestori[key].details = {};
					$scope.gestori[key].details.dighe = getREST.results[gestorePath];
					for(var key2 in $scope.gestori[key].details.dighe){
						$scope.gestori[key].details.dighe[key2].denominazione = $scope.gestori[key].details.dighe[key2].numeroArchivio + $scope.gestori[key].details.dighe[key2].sub + ' ' + $scope.gestori[key].details.dighe[key2].nomeDiga;
						if($scope.gestori[key].details.dighe[key2].id) $scope.gestori[key].details.dighe[key2].orderId = $scope.getDigaOrder($scope.gestori[key].details.dighe[key2].id);
					}
					// $scope.gestori[key].isC_details = false;
					getREST.results[gestorePath].newDataZZ = false;
				}
			}
			
			gestorePath = $scope.gestoreUtentiREST.replace("?", $scope.gestori[key].id);
			if(getREST.results[gestorePath]){
				if(getREST.results[gestorePath].newDataZZ){
					$scope.gestori[key].details.utenti = getREST.results[gestorePath];
					for(var key2 in $scope.gestori[key].details.utenti) $scope.gestori[key].details.utenti[key2].denominazione = $scope.gestori[key].details.utenti[key2].cognome + ' ' + $scope.gestori[key].details.utenti[key2].nome;
					// $scope.gestori[key].isC_details = false;
					getREST.results[gestorePath].newDataZZ = false;
				}
			}
			if($scope.gestori[key].details === undefined) continue;
			if($scope.gestori[key].details.contatto !== undefined) $scope.getInsertedContattoREST($scope.gestori[key], $scope.gestoreREST);
			if($scope.gestori[key].details.indirizzo !== undefined) $scope.getInsertedIndirizzoREST($scope.gestori[key], $scope.gestoreREST);
			
		}
		for(var key in $scope.ingegneri){
			var ingegnerePath = $scope.ingegnereREST+"/"+$scope.ingegneri[key].id;
			if(getREST.results[ingegnerePath]){
				if(getREST.results[ingegnerePath].newDataZZ){
					// alert("OK");
					$scope.ingegneri[key].details = getREST.results[ingegnerePath];
					$scope.ingegneri[key].isC_details = false;
					getREST.results[ingegnerePath].newDataZZ = false;
				}
			}
			if($scope.ingegneri[key].details === undefined) continue;
			if($scope.ingegneri[key].details.contatto !== undefined) $scope.getInsertedContattoREST($scope.ingegneri[key], $scope.ingegnereREST);
			if($scope.ingegneri[key].details.indirizzo !== undefined) $scope.getInsertedIndirizzoREST($scope.ingegneri[key], $scope.ingegnereREST);
			
		}
		for(var key in $scope.autorita){
			var autoritaPath = $scope.autoritaREST+"/"+$scope.autorita[key].id;
			if(getREST.results[autoritaPath]){
				if(getREST.results[autoritaPath].newDataZZ){
					// alert("OK");
					$scope.autorita[key].details = getREST.results[autoritaPath];
					$scope.autorita[key].isC_details = false;
					getREST.results[autoritaPath].newDataZZ = false;
					
					getREST.restJson($scope.autoritaDigheREST.replace("?", $scope.autorita[key].id));
					getREST.restJson($scope.autoritaUtentiREST.replace("?", $scope.autorita[key].id));
				}
			}
			
			autoritaPath = $scope.autoritaDigheREST.replace("?", $scope.autorita[key].id);
			if(getREST.results[autoritaPath]){
				if(getREST.results[autoritaPath].newDataZZ){
					$scope.autorita[key].details.dighe = getREST.results[autoritaPath];
					for(var key2 in $scope.autorita[key].details.dighe){
						$scope.autorita[key].details.dighe[key2].denominazione = $scope.autorita[key].details.dighe[key2].numeroArchivio + $scope.autorita[key].details.dighe[key2].sub + ' ' + $scope.autorita[key].details.dighe[key2].nomeDiga;
						if($scope.autorita[key].details.dighe[key2].id) $scope.autorita[key].details.dighe[key2].orderId = $scope.getDigaOrder($scope.autorita[key].details.dighe[key2].id);
					}
					$scope.autorita[key].isC_details = false;
					getREST.results[autoritaPath].newDataZZ = false;
				}
			}
			
			/* 
			autoritaPath = $scope.autoritaDigheREST.replace("?", $scope.autorita[key].id);
			if(getREST.results[autoritaPath]){
				if(getREST.results[autoritaPath].newDataZZ){
					$scope.autorita[key].details.utenti = getREST.results[autoritaPath];
					for(var key2 in $scope.autorita[key].details.utenti) $scope.autorita[key].details.utenti[key2].denominazione = $scope.autorita[key].details.utenti[key2].cognome + ' ' + $scope.autorita[key].details.utenti[key2].nome;
					$scope.autorita[key].isC_details = false;
					getREST.results[autoritaPath].newDataZZ = false;
				}
			}
			 */
			autoritaPath = $scope.autoritaUtentiREST.replace("?", $scope.autorita[key].id);
			if(getREST.results[autoritaPath]){
				if(getREST.results[autoritaPath].newDataZZ){
					$scope.autorita[key].details.utenti = getREST.results[autoritaPath];
					for(var key2 in $scope.autorita[key].details.utenti) $scope.autorita[key].details.utenti[key2].denominazione = $scope.autorita[key].details.utenti[key2].cognome + ' ' + $scope.autorita[key].details.utenti[key2].nome;
					$scope.autorita[key].isC_details = false;
					getREST.results[autoritaPath].newDataZZ = false;
				}
			}
			
			if($scope.autorita[key].details === undefined) continue;
			if($scope.autorita[key].details.contatto !== undefined) $scope.getInsertedContattoREST($scope.autorita[key], $scope.autoritaREST);
			if($scope.autorita[key].details.indirizzo !== undefined) $scope.getInsertedIndirizzoREST($scope.autorita[key], $scope.autoritaREST);
			
		}
		
		for(var key in $scope.altriEnti){
			var altroEntePath = $scope.altraEnteREST+"/"+$scope.altriEnti[key].id;
			if(getREST.results[altroEntePath]){
				if(getREST.results[altroEntePath].newDataZZ){
					// alert("OK");
					$scope.altriEnti[key].details = getREST.results[altroEntePath];
					$scope.altriEnti[key].isC_details = false;
					getREST.results[altroEntePath].newDataZZ = false;
					
					getREST.restJson($scope.altraEnteDigheREST.replace("?", $scope.altriEnti[key].id));
					getREST.restJson($scope.altraEnteUtentiREST.replace("?", $scope.altriEnti[key].id));
				}
			}
			
			altroEntePath = $scope.altraEnteDigheREST.replace("?", $scope.altriEnti[key].id);
			if(getREST.results[altroEntePath]){
				if(getREST.results[altroEntePath].newDataZZ){
					$scope.altriEnti[key].details.dighe = getREST.results[altroEntePath];
					for(var key2 in $scope.altriEnti[key].details.dighe){
						$scope.altriEnti[key].details.dighe[key2].denominazione = $scope.altriEnti[key].details.dighe[key2].numeroArchivio + $scope.altriEnti[key].details.dighe[key2].sub + ' ' + $scope.altriEnti[key].details.dighe[key2].nomeDiga;
						if($scope.altriEnti[key].details.dighe[key2].id) $scope.altriEnti[key].details.dighe[key2].orderId = $scope.getDigaOrder($scope.altriEnti[key].details.dighe[key2].id);
					}
					$scope.altriEnti[key].isC_details = false;
					getREST.results[altroEntePath].newDataZZ = false;
				}
			}
			
			
			altroEntePath = $scope.altraEnteDigheREST.replace("?", $scope.altriEnti[key].id);
			if(getREST.results[altroEntePath]){
				if(getREST.results[altroEntePath].newDataZZ){
					$scope.altriEnti[key].details.utenti = getREST.results[altroEntePath];
					for(var key2 in $scope.altriEnti[key].details.utenti) $scope.altriEnti[key].details.utenti[key2].denominazione = $scope.altriEnti[key].details.utenti[key2].cognome + ' ' + $scope.altriEnti[key].details.utenti[key2].nome;
					$scope.altriEnti[key].isC_details = false;
					getREST.results[altroEntePath].newDataZZ = false;
				}
			}
			
			altroEntePath = $scope.altraEnteUtentiREST.replace("?", $scope.altriEnti[key].id);
			if(getREST.results[altroEntePath]){
				if(getREST.results[altroEntePath].newDataZZ){
					$scope.altriEnti[key].details.utenti = getREST.results[altroEntePath];
					for(var key2 in $scope.altriEnti[key].details.utenti) $scope.altriEnti[key].details.utenti[key2].denominazione = $scope.altriEnti[key].details.utenti[key2].cognome + ' ' + $scope.altriEnti[key].details.utenti[key2].nome;
					$scope.altriEnti[key].isC_details = false;
					getREST.results[altroEntePath].newDataZZ = false;
				}
			}
			
			if($scope.altriEnti[key].details === undefined) continue;
			if($scope.altriEnti[key].details.contatto !== undefined) $scope.getInsertedContattoREST($scope.altriEnti[key], $scope.altraEnteREST);
			if($scope.altriEnti[key].details.indirizzo !== undefined) $scope.getInsertedIndirizzoREST($scope.altriEnti[key], $scope.altraEnteREST);
			
		}
		
		if(getREST.results[$scope.concessionarioREST]){
			if(getREST.results[$scope.concessionarioREST].newDataZZ){
				$scope.concessionari = getREST.results[$scope.concessionarioREST];
				
				for(var key in $scope.concessionari){
					$scope.concessionari[key].isC_details = true;
					// $scope.concessionari[key].gestoriAssoc = $scope.gestori;
					// $scope.concessionari[key].gestoriAssocTable = new $scope.AssocTable($scope.concessionari[key].gestoriAssoc , '', $filter);
				}
				$scope.tableConcess = new $scope.AnagraficaTable($scope.concessionari, 'nome', $filter);
				
				getREST.results[$scope.concessionarioREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.gestoreREST]){
			if(getREST.results[$scope.gestoreREST].newDataZZ){
				$scope.gestori = getREST.results[$scope.gestoreREST];
				for(var key in $scope.gestori) $scope.gestori[key].isC_details = true;
				$scope.tableGestori = new $scope.AnagraficaTable($scope.gestori, 'nome', $filter);
				
				getREST.results[$scope.gestoreREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.ingegnereREST]){
			if(getREST.results[$scope.ingegnereREST].newDataZZ){
				$scope.ingegneri = getREST.results[$scope.ingegnereREST];
				for(var key in $scope.ingegneri){
					$scope.ingegneri[key].isC_details = true;
					$scope.ingegneri[key].nomeCognome = $scope.ingegneri[key].nome+' '+$scope.ingegneri[key].cognome;
				}
				$scope.tableIngegneri = new $scope.AnagraficaTable($scope.ingegneri, ['cognome', 'nome'], $filter);
				
				getREST.results[$scope.ingegnereREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.autoritaREST]){
			if(getREST.results[$scope.autoritaREST].newDataZZ){
				$scope.autorita = getREST.results[$scope.autoritaREST];
				for(var key in $scope.autorita) $scope.autorita[key].isC_details = true;
				$scope.tableAutorita = new $scope.AnagraficaTable($scope.autorita, 'descrizione', $filter);
				
				getREST.results[$scope.autoritaREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.altraEnteREST]){
			if(getREST.results[$scope.altraEnteREST].newDataZZ){
				$scope.altriEnti = getREST.results[$scope.altraEnteREST];
				for(var key in $scope.altriEnti) $scope.altriEnti[key].isC_details = true;
				$scope.tableAltriEnti = new $scope.AnagraficaTable($scope.altriEnti, 'descrizione', $filter);
				
				getREST.results[$scope.altraEnteREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.provinciaREST]){
			if(getREST.results[$scope.provinciaREST].newDataZZ){
				$scope.province = getREST.results[$scope.provinciaREST];
				
				getREST.results[$scope.provinciaREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.regioneREST]){
			if(getREST.results[$scope.regioneREST].newDataZZ){
				$scope.regioni = getREST.results[$scope.regioneREST];
				
				getREST.results[$scope.regioneREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.tipiContattoREST]){
			if(getREST.results[$scope.tipiContattoREST].newDataZZ){
				$scope.tipiContatto = getREST.results[$scope.tipiContattoREST];
				for(var key in $scope.tipiContatto) $scope.tipiContatto[key].id = parseInt($scope.tipiContatto[key].id, 10);
				getREST.results[$scope.tipiContattoREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.tipiAltroEnteREST]){
			if(getREST.results[$scope.tipiAltroEnteREST].newDataZZ){
				$scope.tipiAltroEnte = getREST.results[$scope.tipiAltroEnteREST];
				for(var key in $scope.tipiAltroEnte) $scope.tipiAltroEnte[key].id = parseInt($scope.tipiAltroEnte[key].id, 10);
				getREST.results[$scope.tipiAltroEnteREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.tipiAutoritaREST]){
			if(getREST.results[$scope.tipiAutoritaREST].newDataZZ){
				$scope.tipiAutorita = getREST.results[$scope.tipiAutoritaREST];
				
				getREST.results[$scope.tipiAutoritaREST].newDataZZ = false;
			}
		}
		
		if(getREST.results[$scope.digheREST]){
			if(getREST.results[$scope.digheREST].newDataZZ){
				$scope.dighe = getREST.results[$scope.digheREST];
				
				for(var key in $scope.dighe){
					$scope.dighe[key].denominazione = $scope.dighe[key].numeroArchivio + $scope.dighe[key].sub + ' ' + $scope.dighe[key].nomeDiga;
					if($scope.dighe[key].id) $scope.dighe[key].orderId = $scope.getDigaOrder($scope.dighe[key].id);
				}
				getREST.results[$scope.digheREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.utentiREST]){
			if(getREST.results[$scope.utentiREST].newDataZZ){
				$scope.utenti = getREST.results[$scope.utentiREST];
				
				for(var key in $scope.utenti) $scope.utenti[key].denominazione = $scope.utenti[key].cognome + ' ' + $scope.utenti[key].nome;
				
				getREST.results[$scope.utentiREST].newDataZZ = false;
			}
		}
		
		var path = $scope.concessionarioREST+'/new/'+$scope.nuovoConcess.id_new;
		if(getREST.results[path]){
			if(getREST.results[path].newDataZZ){
				getREST.results[path].isC_details = true;
				$scope.concessionari.push(getREST.results[path]);
				getREST.results[path].newDataZZ = false;
				
				$scope.tableConcess.$params.filter = {
					nome: getREST.results[path].nome
				};
				$scope.show_hide_Concess_details(getREST.results[path]);
				
			}
		}
		
		var path = $scope.gestoreREST+'/new/'+$scope.nuovoGestore.id_new;
		if(getREST.results[path]){
			if(getREST.results[path].newDataZZ){
				getREST.results[path].isC_details = true;
				$scope.gestori.push(getREST.results[path]);
				getREST.results[path].newDataZZ = false;
				
				$scope.tableGestori.$params.filter = {
					nome: getREST.results[path].nome
				};
				$scope.show_hide_Gestore_details(getREST.results[path]);
				
			}
		}
		
		var path = $scope.autoritaREST+'/new/'+$scope.nuovaAutorita.id_new;
		if(getREST.results[path]){
			if(getREST.results[path].newDataZZ){
				getREST.results[path].isC_details = true;
				$scope.autorita.push(getREST.results[path]);
				getREST.results[path].newDataZZ = false;
				
				$scope.tableAutorita.$params.filter = {
					descrizione: getREST.results[path].descrizione
				};
				$scope.show_hide_Autorita_details(getREST.results[path]);
				
			}
		}
		
		var path = $scope.altraEnteREST+'/new/'+$scope.nuovoEnte.id_new;
		if(getREST.results[path]){
			if(getREST.results[path].newDataZZ){
				getREST.results[path].isC_details = true;
				$scope.altriEnti.push(getREST.results[path]);
				getREST.results[path].newDataZZ = false;
				
				$scope.tableAltriEnti.$params.filter = {
					nome: getREST.results[path].nome
				};
				$scope.show_hide_Altro_Ente_details(getREST.results[path]);
				
			}
		}
		
		
		if($scope.reloadTableConcess){
			$scope.tableConcess.reload();
			$scope.reloadTableConcess = false;
		}
		if($scope.reloadTableGestori){
			$scope.tableGestori.reload();
			$scope.reloadTableGestori = false;
		}
		if($scope.reloadTableIngegneri){
			$scope.tableIngegneri.reload();
			$scope.reloadTableIngegneri = false;
		}
		if($scope.reloadTableAutorita){
			$scope.tableAutorita.reload();
			$scope.reloadTableAutorita = false;
		}
		if($scope.reloadTableAltraEnte){
			$scope.tableAltriEnti.reload();
			$scope.reloadTableAltraEnte = false;
		}
		if($scope.reloadTableAssocTo){
			$scope.tableAssocTo.reload();
			$scope.reloadTableAssocTo = false;
		}
		if($scope.reloadTableAssocFrom){
			$scope.tableAssocFrom.reload();
			$scope.reloadTableAssocFrom = false;
		}
		/* 
		$scope.tableConcess.reload();
		$scope.tableGestori.reload();
		$scope.tableIngegneri.reload();
		$scope.tableAutorita.reload();
		 */
		// $scope.tableAssocTo.reload();
		// $scope.tableAssocFrom.reload();
		
	});
	
	
	getREST.restJson($scope.provinciaREST);
	getREST.restJson($scope.regioneREST);
	getREST.restJson($scope.tipiContattoREST);
	getREST.restJson($scope.tipiAltroEnteREST);
	getREST.restJson($scope.tipiAutoritaREST);
	
	$rootScope.$broadcast('TypeLoadCompleted');
	
	
}]);

