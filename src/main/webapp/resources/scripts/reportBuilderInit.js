
var reportBuilderModule = angular.module('reportBuilderApp', ['ngGrid', 'ui.bootstrap', 'ngTable', 'xeditable', 
                                                  'dialogs.main', 'ngDragDrop', 'ngDialog', 'ngTouch', 'ngSanitize', 'frapontillo.bootstrap-switch']); //'dialogs'

reportBuilderModule.run(function (editableOptions) {
	editableOptions.theme = 'bs3';
});

Date.prototype.toItDateString = function(){
	// var test = this.toLocaleDateString("it-IT", {year: "numeric", month: "2-digit", day: "2-digit"});
	var g = this.getDate();
	var m = this.getMonth() + 1;
	var yyyy = this.getFullYear();
	
	if(g < 10) var gg = "0" + g;
	else var gg = "" + g;
	if(m < 10) var mm = "0" + m;
	else var mm = "" + m;
	
	// return this.toLocaleDateString("it-IT", {year: "numeric", month: "2-digit", day: "2-digit"});
	return gg + "/" + mm + "/" + yyyy;
};

/* 
 Date.prototype.yyyymmdd = function() {
   var yyyy = this.getFullYear().toString();
   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
   var dd  = this.getDate().toString();
   return yyyy + (mm[1]?mm:"0"+mm[0]) + (dd[1]?dd:"0"+dd[0]); // padding
  };
   */

reportBuilderModule.config(function ($translateProvider) {
	$translateProvider.translations('it',{
        DIALOGS_ERROR: "Errore",
        DIALOGS_ERROR_MSG: "Si è verificato un errore sconosciuto.",
        DIALOGS_CLOSE: "Chiudi",
        DIALOGS_PLEASE_WAIT: "Si prega di Attendere",
        DIALOGS_PLEASE_WAIT_ELIPS: "Si prega di Attendere...",
        DIALOGS_PLEASE_WAIT_MSG: "Attendere il completamento dell'operazione.",
        DIALOGS_PERCENT_COMPLETE: "% Completato",
        DIALOGS_NOTIFICATION: "Notifica",
        DIALOGS_NOTIFICATION_MSG: "Notifica Applicazione Sconosciuta.",
        DIALOGS_CONFIRMATION: "Conferma",
        DIALOGS_CONFIRMATION_MSG: "Conferma Richiesta.",
        DIALOGS_OK: "OK",
        DIALOGS_YES: "Si",
        DIALOGS_NO: "No"
    });
});