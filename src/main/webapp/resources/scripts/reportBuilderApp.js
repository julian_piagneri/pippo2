﻿//tutte le occorrenze di $dialogs sono state rinominate in dialog per funzionare con dialog 5

G_report_source_scope = window.opener.G_report_source_scope;
// G_count = 0;
// G_report_source_scope.focus();

reportBuilderModule.controller('reportBuilderController',
    ['$scope', '$http', '$filter', 'getREST', 'ngTableParams', '$rootScope', 'dialogs',  'ngDialog', 'helpersService', 'TokenStorage', 
        // 'dialogService',
    function ($scope, $http, $filter, getREST, ngTableParams, $rootScope, dialogs, ngDialog, helpersService, TokenStorage
        // , dialogService
        ) {


	$scope.helpersService=helpersService;
    $.extend($scope, G_Common_Scope);
    $scope.getREST = getREST;
    // $scope.dialogService = dialogService;

	// console.log($scope.JSON_fromSource_test);
	
	// alert($scope.JSON_fromSource_test);
	/*
    $scope.JSON_fromSource = {
        "titolo": "Selezione Box con estremi in: [7.27, 44.67] - [8.24, 44.41]",
        "dati":
		[
			{
			    "gruppo": "Fiumi",
			    "colonne": ["name", "id_fiume", "versante", "ordine", "tipo"],
			    //"colonne2":[
                //    {"nome":"name","stato":false,"nomenew":"yyyy"},
                //    {"nome":"id_fiume","stato":false}
			    //],
                "ordinamento":[
                    {"nome":"versante","verso":"asc"}
                ],
			    "righe":[
					{
					    "name": "TANARO",
					    "id_fiume": "1788",
					    "versante": "PA",
					    "ordine": "2",
					    "tipo": "A"
					},
					{
					    "name": "MAIRA",
					    "id_fiume": "1800",
					    "versante": "PA",
					    "ordine": "2",
					    "tipo": "T"
					},
					{
					    "name": "VARAITA",
					    "id_fiume": "1801",
					    "versante": "PA",
					    "ordine": "2",
					    "tipo": "T"
					},
					{
					    "name": "PO",
					    "id_fiume": "140",
					    "versante": "PA",
					    "ordine": "1",
					    "tipo": "A"
					}
				],
			    "ordine": 1
			},
		    {
		        "gruppo": "Comuni",
		        "colonne": ["nomeComune", "provincia", "Sindaco", "n_abitanti", "superfice", "param1", "param2", "param3", "param4", "param5"],
		        "righe": [
					{
					    "nomeComune": "Roma",
					    "provincia": "RM",
					    "Sindaco": "Pippo",
					    "n_abitanti": 10000,
					    "superfice": 654,
					    "param1": 12,
					    "param2": 23,
					    "param3": 34,
					    "param4": 46,
					    "param5": 56
					},
					{
					    "nomeComune": "Milano",
					    "provincia": "MI",
					    "Sindaco": "Pluto",
					    "n_abitanti": 1234000,
					    "superfice": 567,
					    "param1": 112,
					    "param2": 213,
					    "param3": 314,
					    "param4": 416,
					    "param5": 516
					},
					{
					    "nomeComune": "Napoli",
					    "provincia": "NA",
					    "Sindaco": "Paperino",
					    "n_abitanti": 65423456,
					    "superfice": 234,
					    "param1": 122,
					    "param2": 223,
					    "param3": 324,
					    "param4": 426,
					    "param5": 526
					}
		        ],
		        "ordine": 2
		    },
			{
			    "gruppo": "Laghi",
			    "colonne": ["nomeLago", "area"],
			    "righe": [
					{
					    "nomeLago": "PO",
					    "area": 10000
					},
					{
					    "nomeLago": "TEVERE",
					    "area": 10033
					}
			    ],
			    "ordine": 3
			}

		]
    };
	*/
    //$scope.JSON_fromSource = null;
    $scope.draggedCampi = null;

    $scope.aggiungiOrdinamento = function (tabella, colonna) {

        //console.log(colonna);
        //tabella.ordinamento[colonna.nome] = { "colonna": colonna.nomenew, "verso": "asc" };
        var trovato = false;
        for (var id_colonna in tabella.ordinamento)
            if (tabella.ordinamento[id_colonna].nome == colonna.nome) {
                trovato = true;
                break;
            }

        if (!trovato)
            tabella.ordinamento.push({ "nome": colonna.nome, "verso": "asc" });

        console.log(tabella);
    }

    $scope.cambiaVersoOrdinamento = function (posizione) {
        console.log(posizione.verso);
        if (posizione.verso == "asc")
            posizione.verso = "desc";
        else
            posizione.verso = "asc";
        console.log(posizione.verso);
    }

    $scope.getnomenew = function (tabella, nomecampo) {
        for (var id_campo in tabella.colonne)
            if (tabella.colonne[id_campo].nome == nomecampo)
                return tabella.colonne[id_campo].nomenew;
    }

    $scope.generaJSON = function () {

        var JSON_export = $.extend(true, {}, $scope.JSON_fromSource);
        for (var id_dati = JSON_export.dati.length - 1; id_dati >= 0;id_dati--)
        {
            var contaColonne = 0;
            for (var id_colonna = JSON_export.dati[id_dati].colonne.length - 1; id_colonna >= 0; id_colonna--)
            {
                var nomeCampo = JSON_export.dati[id_dati].colonne[id_colonna].nome;
                var nomeCampoNew = JSON_export.dati[id_dati].colonne[id_colonna].nomenew;
                //console.log(nomeCampo);
                //console.log(JSON_export.dati[id_dati].colonne[id_colonna].stato);

                if (!JSON_export.dati[id_dati].colonne[id_colonna].stato) {
                    for (var id_ordine = JSON_export.dati[id_dati].ordinamento.length - 1; id_ordine >= 0; id_ordine--) {
                        if (JSON_export.dati[id_dati].ordinamento[id_ordine].nome == nomeCampo)
                            JSON_export.dati[id_dati].ordinamento.splice(id_ordine, 1);
                    }

                    JSON_export.dati[id_dati].colonne.splice(id_colonna, 1);

                    for (var id_riga = JSON_export.dati[id_dati].righe.length - 1; id_riga >= 0; id_riga--)
                        delete JSON_export.dati[id_dati].righe[id_riga][nomeCampo];
                }
                else
                {

                    for (var id_ordine = JSON_export.dati[id_dati].ordinamento.length - 1; id_ordine >= 0; id_ordine--) {
                        if (JSON_export.dati[id_dati].ordinamento[id_ordine].nome == nomeCampo)
                            JSON_export.dati[id_dati].ordinamento[id_ordine].nome = nomeCampoNew;
                    }

                    JSON_export.dati[id_dati].colonne[id_colonna] = nomeCampoNew;

                    if (nomeCampo != nomeCampoNew)
                    {
                        for (var id_riga in JSON_export.dati[id_dati].righe)
                        {
                            JSON_export.dati[id_dati].righe[id_riga][nomeCampoNew] = JSON_export.dati[id_dati].righe[id_riga][nomeCampo];
                            delete JSON_export.dati[id_dati].righe[id_riga][nomeCampo];
                        }
                    }
                    contaColonne++;
                }
                    
            }
            if (contaColonne==0)
                JSON_export.dati.splice(id_dati, 1);
        }

        //alert("OK");
        console.log(JSON_export);
        //console.log(JSON.stringify(JSON_export));

        var jsonappo = { "jsonData": JSON_export };

        console.log(jsonappo);
        console.log("OK");

        $http.post(damRoutes.routeReportJson, jsonappo).success(function (data) {
            //$scope.dialogService.closeLoadingDialog($scope);

            //var success = dialogs.notify("Successo", "Report generato con successo, premere OK per visualizzarlo ");
            //success.result.then(function () {
            //    window.open(damRoutes.routePenthaoLayer);
            //});

            // window.open(damRoutes.routePenthaoLayer);
			helpersService.openExternalWindow(damRoutes.routePentahoReport);
			
        }).error(function (result, status, headers) {
            dialogs.error("Errore", "Si è verificato un errore durante la generazione del report");
        });

        console.log("OK");
 
    };

    $scope.caricamentoIniziale = function () {
        //alert("JSON_dati: " + $scope.JSON_fromSource.dati.length);
        //console.log($scope.JSON_fromSource);
        //alert("Caricamento");
			// dialogService.openLoadingDialog($scope); 
		
        $scope.JSON_fromSource_old = G_report_source_scope().$apply(function(){return G_report_source_scope().getReportJSON({});});
		
		$scope.JSON_fromSource = $.extend(true, {}, $scope.JSON_fromSource_old);

        console.log($scope.JSON_fromSource);

		for (var id_dati in $scope.JSON_fromSource.dati) {
            for (var id_colonna in $scope.JSON_fromSource.dati[id_dati].colonne) {
                var nomeColonna = $scope.JSON_fromSource.dati[id_dati].colonne[id_colonna];
                $scope.JSON_fromSource.dati[id_dati].colonne[id_colonna] = { "nome": nomeColonna, "nomenew": nomeColonna, "stato": false };
            }
            $scope.JSON_fromSource.dati[id_dati].ordinamento = [];
		}

        console.log($scope.JSON_fromSource);
    };

    $scope.getDragCampi = function (event, ui, campi) {
        $scope.draggedCampi = campi;
    };

    $scope.moveCampi = function (event, ui, myRec, myArray) {
        var new_index = null;
        var old_index = null;

        for (var key in myArray) {
            if (myArray[key] == myRec) new_index = key;
            if (myArray[key] == $scope.draggedCampi) old_index = key;
        }

        if (new_index == null || old_index == null) return;

        myArray.splice(new_index, 0, myArray.splice(old_index, 1)[0]);

        console.log($scope.JSON_fromSource.dati[dato].colonne);

    };

    $scope.checkVisibleRow = function (posizione, tabella) {
        for (var id_colonna in tabella.colonne) {
            if (tabella.colonne[id_colonna].nome == posizione.nome) {
                return tabella.colonne[id_colonna].stato;
            }
        }

        return (false);
    };

    $scope.checkOrderRow = function (colonna, tabella) {
        for (var id_colonna in tabella.ordinamento) {
            if (tabella.ordinamento[id_colonna].nome == colonna.nome) {
                return (true);
            }
        }

        return (false);
    };

    $scope.removeOrderRow = function (colonna, tabella) {
        for (var id_colonna in tabella.ordinamento)
        {
            if (tabella.ordinamento[id_colonna].nome == colonna.nome) {
                tabella.ordinamento.splice(id_colonna, 1);
                return;
            }
        }
    };

    $scope.caricamentoIniziale();
}
]);

