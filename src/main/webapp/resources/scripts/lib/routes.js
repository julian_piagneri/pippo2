var damIPs={
	ipMap: "http://10.10.0.25/",
	// ipMap: "http://10.10.0.22:8001/",
	ipReport: "http://10.10.0.25:8080/"
};


var damRoutes={
	routeHome:"home-content",
	routeMenuGet:'api/menu/get',
	routeLoginForm:'login-form',
	routeLoadingDialog:"loading",
	routeListDialog:"overlay-list",
	routeCurrentUser:'api/users/current',
	routeLogin:'api/login',
	routeLogout:'api/logout',
	routeOldLogin:'/snd/jsp/Login/Login.jsp',
	routeOldList:'/snd/jsp/ElencoDighe/ElencoFrame.jsp',
	routeSetDamParameters:'/snd/jsp/applet-params.jsp',
	routeCheckLogin:'/snd/jsp/check-login.jsp',

	routeDamList:'api/diga/extra',
	routeFilterDamList:'api/diga/extra/filters',
	routeSaveFavourites:"api/diga/favorite",
	routeLayerList:'resources/layerList.json',


	viewsList:'api/viste/all',
	saveViewsList:'api/viste/save',
	getBaseView:'api/viste/base',

	routeHelpSnd:'/snd/jsp/Help/HelpModal.jsp?CAP=01.0&FileCss=FILECSS_12_0.css',
	routeStyleSnd:'/snd/jsp/Profilo/SceltaAspetto.jsp?CodMenu=1',
	routeDatiProfilo: 'user-edit',//'/snd/jsp/Profilo/DatiUtente.jsp?CodMenu=1',

	routeDynCss:"resources/style/dynamic/",

	routeUserBlock:"sections/user/utenti",
	routeUserDetails:"api/users/details",	
	routeSaveUser:"api/users/update",
	routeValidateUser:"api/users/validate",
	routeNewUser:"api/users/new",
	
	routeExtraInfoFiumi:"api/extrafiumiinfo",
		
	routeSaveReports:"api/report/saveids",
	routeGetReportsByUser:"api/report/user/current",
	routeGetReportTemplates:"api/report/templates",
	
	routeReportJson: "api/report/jsonData",

	routePentahoReport: damIPs.ipMap + "pentaho/content/reporting/reportviewer/report.html?solution=Main-Folder&path=&name=CrossReportTemplate.prpt&locale=en",
	routePentahoWaqr: damIPs.ipMap + "pentaho/adhoc/waqr.html?solution=Main-Folder&path=%2F&filename=digheReport.waqr.xreportspec&from_dammap=true", 
	routeMapping: damIPs.ipMap + "search_layer.php"
};