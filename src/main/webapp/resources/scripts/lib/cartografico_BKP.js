G_dam_list_scope = function(){return angular.element($("#damList")).scope();}
G_srsname_default = 'epsg:4326'; // Standard Geografico (latitudine, longitudine)
G_projection = 'EPSG:3857'; // Standard Geografico (latitudine, longitudine)
G_dam_map_icon = new ol.style.Circle({fill: null,stroke: new ol.style.Stroke({color: 'red',width: 1}),radius: 5});
G_dam_map_icon_selected = new ol.style.Circle({fill: null,stroke: new ol.style.Stroke({color: 'yellow',width: 1}),radius: 5});
G_aer_map_icon = new ol.style.Circle({fill: null,stroke: new ol.style.Stroke({color: 'black',width: 1}),radius: 5});
// G_url_mapserver = 'http://10.10.0.24/cgi-bin/mapserv';
//G_url_mapserver = 'http://10.10.0.25/cgi-bin/mapserv.exe';
//G_map_mapserver = 'C:/ms4w/Map/dighe_geo.map';
// G_map_mapserver = 'dighe_geo.map';
// G_map_project = 'dighe_geo.map';
// alert("qqq");
	
//PROVA
G_url_mapserver = 'http://10.10.0.25/cgi-bin/mapserv.exe';
G_map_mapserver = 'C:/ms4w/Map/Prova.map';
G_dam_map=null;
var damMapObj = function(){
	this.vectorLayer = function(_prm){
		// this.prm = _prm;
		this.vectorSource = new ol.source.ServerVector({
			format: new ol.format.GeoJSON(),
			loader:	function(extent, resolution, projection){
					var url = G_url_mapserver+'?MAP='+G_map_mapserver+'&service=WFS&callback=?&version=1.1.0&request=GetFeature&typename='+_prm.layer+'&outputFormat='+_prm.output_name+'&srsname='+_prm.srsname;
					$.getJSON(url,(function(_src){return function(response, state, _obj){
						_src.addFeatures(_src.readFeatures(response));
					};})(this));
					// alert("ww");
			},
			// strategy: ol.loadingstrategy.createTile(new ol.tilegrid.XYZ({maxZoom: 19})),
			strategy: ol.loadingstrategy.bbox,
			projection: G_projection
		});
		this.vector = new ol.layer.Vector({
			source: this.vectorSource,
			opacity: _prm.opacity,
			minResolution: _prm.minResolution,
			maxResolution: _prm.maxResolution,
			style: new ol.style.Style(
				{
					image: _prm.icon_for_point,
					stroke: new ol.style.Stroke({color: _prm.stroke_color,width: _prm.stroke_width}), 
					fill: new ol.style.Fill({color: _prm.fill_color})
				}
			)
		});
		this.vector.dighe = _prm.dighe;
		this.vector.selected_style = _prm.icon_for_point_selected;
		
		return this;
	};
	
	/** Creazione layer:
	/** PRM: (layer, output_name, srsname, stroke_color, stroke_width, opacity, fill_color, icon_for_point, icon_for_point_selected, dighe)
	**/
	this.digheLayer = null;
	
	this.digheLayer = new this.vectorLayer({
		layer: 'dighe.dighe_ott12',
		output_name: 'geojson',
		srsname: G_srsname_default,
		// minResolution: 0,
		// maxResolution: 0,
		stroke_color: 'green',
		stroke_width: 5,
		opacity: 1,
		fill_color: 'green',
		icon_for_point: G_dam_map_icon,
		icon_for_point_selected: G_dam_map_icon_selected,
		dighe: true
	});
	this.aerLayer = new this.vectorLayer({
		layer: 'dighe.aeroporti',
		output_name: 'geojson4',
		srsname: G_srsname_default,
		minResolution: 200,
		maxResolution: 2000,
		stroke_color: 'yellow',
		stroke_width: 5,
		opacity: 1,
		fill_color: 'yellow',
		icon_for_point: G_aer_map_icon,
		icon_for_point_selected: null,
		dighe: false
	});
	
	this.diropLayer = new this.vectorLayer({
		layer: 'dighe.dir_operazioni',
		output_name: 'geojson2',
		srsname: G_srsname_default,
		minResolution: 200,
		maxResolution: 2000,
		stroke_color: 'yellow',
		stroke_width: 1,
		opacity: 0.3,
		fill_color: 'blue',
		icon_for_point: null,
		icon_for_point_selected: null,
		dighe: false
	});
	// this.istatLayer = new this.vectorLayer('dighe.istat');
	
	var bingMap = new ol.layer.Tile({
		preload: Infinity,
		source: new ol.source.BingMaps({
		  key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
		  imagerySet: 'AerialWithLabels',
		  opacity: 0.5,
		  zoom:0.5
		})
	});
	// var GoogleMap = new google.maps.Map(document.getElementById('map'),{
  // zoom: 3,
  // center: exampleLoc,
  // disableDefaultUI: true,
  // zoomControl: true,
  // zoomControlOptions: {
    // style: google.maps.ZoomControlStyle.SMALL
  // },
  // mapTypeId: google.maps.MapTypeId.ROADMAP
// });
	this.view = new ol.View({
		projection: G_projection,
		// projection: 'EPSG:3857',
		//center: [810000, 4647000],
		//zoom: 0.5
		center: [1370000, 5200000],
		zoom: 5.5,
		minZoom: 3.5
	});
	
	this.map = new ol.Map({
		// layers: [bingMap, this.diropLayer.vector, this.aerLayer.vector,  this.digheLayer.vector],
		
    // renderer: 'webgl',
		layers: [bingMap, this.digheLayer.vector],
		// overlays: [overlay],
		target: 'map',
		// interactions: ol.interaction.defaults().extend([dragBoxDams]),
		// interactions: ol.interaction.defaults().extend([select, modify]),
		view: this.view

	});
	
	
	this.selectDamOnMap = function(pixel) {
		
		var feature = this.map.forEachFeatureAtPixel(pixel, function(feature, layer) {
			if(layer.dighe) return feature;
		});		
		if (feature) {
			// var scope = angular.element($("#damList")).scope();
			G_dam_list_scope().$apply(function(){G_dam_list_scope().selectDam(feature.get('ArcSub'));});
			var fts = this.SelectedDams.getFeatures();
			
			fts.forEach(function(element, index, array){
				this.removeFeature(element);
			}, this.SelectedDams);
			
			this.SelectedDams.addFeature(feature);
			
		}
		

	};
	
	
	this.selectDamOnGrid = function(damId) {
		
		var feature = this.digheLayer.vectorSource.forEachFeature(function(feature) {
			if(feature.get('ArcSub') == this) return feature;
		}, damId);
		
		if (feature) {
			
			var fts = this.SelectedDams.getFeatures();
			
			fts.forEach(function(element, index, array){
				this.removeFeature(element);
			}, this.SelectedDams);
			
			this.SelectedDams.addFeature(feature);
			// alert("he");
			this.panTo(feature.getGeometry().getClosestPoint(this.view.getCenter()));
		}		
	};
	
	this.loadLayers = function(_layers){
		for(var _key in _layers){
			// alert(_layers[_key].name);
			switch(_layers[_key].type){
				case "vector":
					var newLayer = new this.vectorLayer({
						layer: _layers[_key].layer,
						output_name: _layers[_key].output_name,
						srsname: G_srsname_default,
						minResolution: _layers[_key].minResolution,
						maxResolution: _layers[_key].maxResolution,
						stroke_color: _layers[_key].stroke_color,
						stroke_width: _layers[_key].stroke_width,
						opacity: _layers[_key].opacity,
						fill_color: _layers[_key].fill_color
					});
					// alert("biz2");
					this.map.addLayer(newLayer.vector);
					break;
				case "dighe":
					// alert("ee");
					// var newLayer = new this.vectorLayer({
					this.digheLayer = new this.vectorLayer({
						layer: _layers[_key].layer,
						output_name: _layers[_key].output_name,
						srsname: G_srsname_default,
						minResolution: _layers[_key].minResolution,
						maxResolution: _layers[_key].maxResolution,
						stroke_color: _layers[_key].stroke_color,
						stroke_width: _layers[_key].stroke_width,
						opacity: _layers[_key].opacity,
						fill_color: _layers[_key].fill_color,
						icon_for_point: G_dam_map_icon,
						icon_for_point_selected: G_dam_map_icon_selected,
						dighe: true
					});
					// this.map.addLayer(newLayer.vector);
					// alert(this.digheLayer.vector);
					
					this.map.addLayer(this.digheLayer.vector);
					// alert("tt2");
					break;
				default:
					break;
			}
			
		}
		
	};
	
	this.SelectedDams = new ol.FeatureOverlay({
		map: this.map,
		style: new ol.style.Style({image: G_dam_map_icon_selected})
	});
	
	this.panTo = function(coord){
		
	  var pan = ol.animation.pan({
		duration: 2000,
		source: /** @type {ol.Coordinate} */ (this.view.getCenter())
	  });
	  this.map.beforeRender(pan);
	  this.view.setCenter(coord);
	};
	
	this.dragBoxDams = new ol.interaction.DragBox({
	   map:this.map,
       condition: ol.events.condition.altKeyOnly,
		 style: new ol.style.Style({
			stroke: new ol.style.Stroke({
			// image: G_dam_map_icon_selected
			color: 'yellow',
			width: 2
			})
		})
	});
	
	
	this.dragBoxDams.on('boxend', function(pixel) {
			var feature = this.map.forEachFeatureAtPixel(pixel, function(feature, layer) {
			if(layer.dighe) return feature;
		});		
		if (feature) {
			// var scope = angular.element($("#damList")).scope();
			G_dam_list_scope().$apply(function(){G_dam_list_scope().selectDam(feature.get('ArcSub'));});
			var fts = this.dragBoxDams.getFeatures();
			
			fts.forEach(function(element, index, array){
				this.removeFeature(element);
			}, this.dragBoxDams);
			
			this.dragBoxDams.addFeature(feature);
			}
	});
	
	// this.dragBoxDams.on('boxend', function(feature) {
  // var info = [];
  // var feature = map.getGeometry().getExtent();
  // vectorSource.forEachFeatureIntersectingExtent(feature, function(feature, layer) {
			// if(layer.dighe) return feature;
  // });
  	// if (feature) {
	// var fts = this.dragBoxDams.getFeatures();
	// fts.forEach(function(element, index, array){
				// this.removeFeature(element);
			// }, this.dragBoxDams);
			
			// this.fts.addFeature(feature);
    // fts.push(feature);
    // info.push(feature.get('name'));
  // if (info.length > 0) {
    // infoBox.innerHTML = info.join(', ');
  // }
  // }
// });
	
};
	

	/*******************/
	/*** OPEN LAYERS ***/
	/*******************/
	/* 
ol.proj.addProjection(new ol.proj.Projection({
	code: 'EPSG:4326',
	units: 'm',
	extent: [6, 36.6, 18.5, 47.6]       
}));
ol.proj.addProjection(new ol.proj.Projection({
	code: 'EPSG:32632',
	units: 'm'      
}));
ol.proj.addProjection(new ol.proj.Projection({
	code: 'EPSG:23032',
	units: 'm'      
}));
 */
