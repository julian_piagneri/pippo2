G_dam_list_scope = function(){return angular.element($("#damList")).scope();}
G_layer_list_scope = function(){return angular.element($("#layerList")).scope();}

G_srsname_default = 'epsg:4326'; // Standard Geografico (latitudine, longitudine)
G_projection = 'EPSG:3857'; // Standard Geografico (latitudine, longitudine)
G_dam_map_icon = new ol.style.Circle({fill: new ol.style.Fill({color: 'red'}),stroke: new ol.style.Stroke({color: 'red',width: 2}),radius: 2});
G_dam_map_icon_selected = new ol.style.Circle({fill: new ol.style.Fill({color: 'yellow'}),stroke: new ol.style.Stroke({color: 'yellow',width: 1}),radius: 5});
G_dam_map_icon_selected_MultiPoint = new ol.style.Circle({fill: new ol.style.Fill({color: '#ffcc33'/* 'yellow' */}),stroke: new ol.style.Stroke({color: 'green',width: 1}),radius: 5});
G_poly_style = new ol.style.Style({fill: new ol.style.Fill({color: 'rgba(255, 255, 255, 0.2)'}),stroke: new ol.style.Stroke({color: '#ffcc33',width: 2}),image: new ol.style.Circle({radius: 7,fill: new ol.style.Fill({color: '#ffcc33'})})});
G_dam_map_layer_selected = new ol.style.Style({fill: new ol.style.Fill({color: 'rgba(0, 255, 0, 0.2)'}),stroke: new ol.style.Stroke({color: '#ffcc33',width: 5}),image: new ol.style.Circle({radius: 7,fill: new ol.style.Fill({color: '#ffcc33'})})});
G_dam_map_layer_overlay = new ol.style.Style({fill: new ol.style.Fill({color: 'rgba(255, 0, 0, 0.2)'}),stroke: new ol.style.Stroke({color: '#ff0000',width: 5}),image: new ol.style.Circle({radius: 7,fill: new ol.style.Fill({color: '#ff0000'})})});
G_dam_map_layerBuffer_selected = new ol.style.Style({fill: new ol.style.Fill({color: 'rgba(255, 255, 255, 0.2)'}),stroke: new ol.style.Stroke({color: 'white',width: 5}),image: new ol.style.Circle({radius: 7,fill: new ol.style.Fill({color: 'white'})})});
G_hidden_style = new ol.style.Style({display: 'none'});
G_Buffer_style = new ol.style.Style({fill: new ol.style.Fill({color: 'rgba(255, 255, 0, 0.2)'}),stroke: new ol.style.Stroke({color: 'blue',width: 2}),image: new ol.style.Circle({radius: 7,fill: new ol.style.Fill({color: 'blue'})})});

// G_url_mapserver = 'http://10.10.0.25/cgi-bin/mapserv.exe';
// G_url_mapserver = 'http://10.10.0.25/search_layer.php';
G_url_mapserver = damRoutes.routeMapping;
// G_url_mapserver = 'http://10.20.241.124/search_layer.php';
// G_map_mapserver = '/var/www/cgi-bin/ProvaJ.map';
G_map_mapserver = 'C:/ms4w/Map/Cartografico.map';
G_dam_map=null;


var damMapObj = function(){
	
	this.overlayActive = false;
	this.bufferActive = false;
	this.featureDamActive = false;
	this.featureLayerActive = false;
	this.featActive = false;
	this.damSemaphore = false;
	this.tempFeatureToList=[];
	
	this.tooltDiv=$('#tooltdiv');
	this.tooltip=this.tooltDiv.tooltip( {
		track: true,
        content: function () { 
            var lowertext = "<div class='tooltiptext'>"+$("#tooltdiv").html()+"</div>";
            return lowertext;
        }
	});
	
	this.getObjLength = function(_obj){
		var ii = 0;
		for(var k in _obj) ii++;
		return ii;
	};

	
/**
		Layer Definition --> GeoJSON
**/
	this.vectorLayer = function(_prm){
		if (_prm.geo_json){
			this.vectorSource = new ol.source.GeoJSON({
				object: _prm.geo_json
			});
		}
		else{
			var VSprm = {
				format: new ol.format.GeoJSON(), 
				loader:	function(extent, resolution, projection){
						this.last_extent = extent;
						var extent_geo = [new ol.proj.transform([extent[0], extent[1]], 'EPSG:3857', 'EPSG:4326'),new ol.proj.transform([extent[2], extent[3]], 'EPSG:3857', 'EPSG:4326')];
						var extent_str = extent.join(',');
						var resolution_str = resolution;
						var url = G_url_mapserver+'?MAP='+G_map_mapserver+'&service=WFS&callback=?&version=1.1.0&request=GetFeature&typename='+_prm.layer+'&outputFormat='+_prm.output_name+'&srsname='+_prm.srsname+'&bbox=' + extent_geo.join(',') + '&resolution=' +resolution_str ;
						if(this.last_url === undefined) this.last_url = "";
						if(this.last_url != url){
							this.isLoadding = true;
							
							$.getJSON(url,(function(_src, extent_get, resolution_get){return function(response, state, _obj){
							   if(response.features === undefined) console.log("Non ci sono feature nella zona esplorata");
								if(_src.last_resolution == resolution_get && _src.last_extent.join(',') == extent_get){
									_src.clear(true);
									_src.addFeatures(_src.readFeatures(response));
									/////////////////////////////Caricamento Feature List Dinamica///////////////////////////
									if(_src.showFeature){	
									    G_layer_list_scope().featureList = new Array();
										this.tempFeatureToList = _src.getFeatures();
											this.tempFeatureToList.sort(sorters);
											var temp = new Array();
											for (key in this.tempFeatureToList){
												this.tempFeatureToList[key].GeometryName = this.tempFeatureToList[key].getProperties().name.replace(/&#39;/g, "'");
												this.tempFeatureToList[key].layerName = _src.legend_name;
											} 								
										G_layer_list_scope().featureList = this.tempFeatureToList;
									}
									
									// if(G_layer_list_scope().filterLayer.active && !G_layer_list_scope().buffer.active && !G_layer_list_scope().overlay.active) G_layer_list_scope().filtraLayer();
									
									_src.isLoadding = false;					
							         /////////////////////////////////////////////////////////////////////////
								}
								
							};})(this, extent_str, resolution_str));
							/* 
							
							setTimeout((function(_src, extent_get, resolution_get, _url){
								if(_src.last_url != _url) return function(){};
								else return function(){
									$.getJSON(_url,(function(_src, extent_get, resolution_get){return function(response, state, _obj){
										if(_src.last_resolution == resolution_get && _src.last_extent.join(',') == extent_get){
											_src.clear(true);
											_src.addFeatures(_src.readFeatures(response));
											_src.isLoadding = false;
										}
									};})(_src, extent_get, resolution_get));
								};
							})(this, extent_str, resolution_str, url), 5);
							*/
						}
						this.last_url = url;
				},
				// strategy: ol.loadingstrategy.bbox,
				strategy: function(extent, resolution) {
					if(this.last_resolution === undefined) this.last_resolution = resolution;
					if(this.last_resolution != resolution){
						this.loadfct(extent, resolution, this.getProjection());
					}
					this.last_resolution = resolution;
					return [extent];
				},
				projection: G_projection
			};
			
			this.vectorSource = new ol.source.ServerVector(VSprm);
			this.vectorSource.loadfct = VSprm.loader;
			this.vectorSource.isLoadding = false;
        }
		
		
		this.vector = new ol.layer.Vector({
			source: this.vectorSource,
			opacity: _prm.opacity,
			minResolution: _prm.minResolution,
			maxResolution: _prm.maxResolution,
			style: createStyleFunction(_prm)
		});
		this.vector.dighe = _prm.dighe;
		this.vector.selected_style = _prm.icon_for_point_selected;
		return this;
	};
	

/**
		Stile dei nomi delle dighe. Visibile in base a livello di zoom
**/
	var createStyleFunction = function(_prmStyle) {
		return function(feature, resolution) {
			if (_prmStyle.dighe)
			{
				var style = new ol.style.Style({
					image: _prmStyle.icon_for_point,
					stroke: new ol.style.Stroke({color: _prmStyle.stroke_color,width: _prmStyle.stroke_width}), 
					fill: new ol.style.Fill({color: _prmStyle.fill_color}) ,
					text: createTextStyle(feature, resolution) 
				});
			}
			else 
			{		
				var style = new ol.style.Style({
					image: _prmStyle.icon_for_point,
					stroke: new ol.style.Stroke({color: _prmStyle.stroke_color,width: _prmStyle.stroke_width}), 
					fill: new ol.style.Fill({color: _prmStyle.fill_color})
				});
			}
			return [style];;
		};
	};

	var getText = function(feature, resolution) {
		var maxResolution = 800;
		var text = '';
		if (resolution < maxResolution){
			text = feature.get('name');
		}
		return text;
	};

	var createTextStyle = function(feature, resolution) {
		return new ol.style.Text({
			baseline: 'Bottom',
			weight: 'Bold',
			offsetY: 14,
			text: getText(feature, resolution),
			/* 
			fill: new ol.style.Fill({
				color: '#fff'
			})
			*/
			fill: new ol.style.Fill({
				color: '#fff'
			}),
			stroke: new ol.style.Stroke({
				color: 'red',
				width: 7
			})
		});
	};
	
	
	
/** 
		Caricamento delle mappe Bing, Google, OSM, Here
**/
	this.digheLayer = null;	
	var styles = ['Road','Aerial','AerialWithLabels','OSM','SAT','OSM2','GooglePhysical','GoogleSatellite','GoogleHybrid','GoogleStreet','GoogleWaterOverlay','HereStreet','HereSatellite','HereHybrid','HereTraffic'];
	var layers = [];
	var i, ii;
	for (i = 0, ii = 3; i < ii; ++i) {
  		layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.BingMaps({
      			key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
      			imagerySet: styles[i]
    			})
  		}));
	}
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.MapQuest({layer: 'osm'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.MapQuest({layer: 'sat'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.OSM()
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.XYZ({
    			url: 'http://mt0.google.com/vt/lyrs=t&z={z}&x={x}&y={y}'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.XYZ({
    			url: 'http://mt0.google.com/vt/lyrs=s&z={z}&x={x}&y={y}'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.XYZ({
    			url: 'http://mt0.google.com/vt/lyrs=y&z={z}&x={x}&y={y}'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.XYZ({
    			url: 'http://mt0.google.com/vt/lyrs=m&z={z}&x={x}&y={y}'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.XYZ({
    			url: 'http://mt0.google.com/vt/lyrs=r&z={z}&x={x}&y={y}'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.XYZ({
    			url: 'http://2.base.maps.cit.api.here.com/maptile/2.1/maptile/newest/normal.day/{z}/{x}/{y}/256/png8?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.XYZ({
    			url: 'http://2.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.XYZ({
    			url: 'http://2.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/hybrid.day/{z}/{x}/{y}/256/png8?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg'})
  		}));
  	layers.push(new ol.layer.Tile({
    		visible: false,
    		preload: Infinity,
    		source: new ol.source.XYZ({
    			url: 'http://1.traffic.maps.cit.api.here.com/maptile/2.1/traffictile/newest/normal.traffic.day/{z}/{x}/{y}/256/png8?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg'})
  		}));

	$('#layer-select').change(function() {
  		var style = $(this).find(':selected').val();
	  	var i, ii;
	  	for (i = 0, ii = layers.length; i < ii; ++i) {
    			layers[i].setVisible(styles[i] == style);
  		}
	});
	$('#layer-select').trigger('change');

	
	
/**
		Setting Map
**/	
	var scaleLineControl = new ol.control.ScaleLine();	
	this.scaleLine = new ol.control.defaults({
		attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
		collapsible: false
		})
	});
  
	this.view = new ol.View({
		projection: G_projection,
		// projection: 'EPSG:3857',
		//center: [810000, 4647000],
		//zoom: 0.5
		center: [1370000, 5200000],
		zoom: 4.8,
		//zoom: 5.5,
	    minZoom: 3.5,
	    maxZoom: 20
	});
	
	this.map = new ol.Map({
		controls: this.scaleLine.extend([scaleLineControl],[new ol.control.FullScreen()]),
		layers: layers,
		target: 'map',
		renderer: 'canvas',
		view: this.view
	});

	zoomslider = new ol.control.ZoomSlider();
    this.map.addControl(zoomslider);
  
	this.panTo = function(coord){
		var pan = ol.animation.pan({
			duration: 2000,
			source: /** @type {ol.Coordinate} */ (this.view.getCenter())
		});
		this.map.beforeRender(pan);
		this.view.setCenter(coord);
	};	
	
	this.zoomTo = function(coord){
	console.log(this.view.getResolution());
		var pan = ol.animation.pan({
			duration: 2000,
			source: /** @type {ol.Coordinate} */ (this.view.getCenter())
		});
		var bounce = ol.animation.bounce({
			duration: 2000,
			resolution: 800/* this.view.getResolution() */
		});
		
		var zoom = ol.animation.zoom({
			duration: 2000,
			resolution: 9
		});
		this.map.beforeRender(bounce);
		this.map.beforeRender(pan);
		this.view.setCenter(coord);
		this.view.setZoom(9); // da metter param
	}; 	

	
	
/**
		Dighe Layer
**/			
	this.addDigheLayer = function(jsonObj) {
		var geoJson = this.json2geojson(jsonObj);	
		this.SelectedDams.getFeatures().clear();	
		if(this.digheLayer) this.map.removeLayer(this.digheLayer.vector);
		
		this.digheLayer = new this.vectorLayer({
			geo_json : geoJson,
			stroke_color: 'green',
			stroke_width: 5,
			opacity: 1,
			fill_color: 'green',
			icon_for_point: G_dam_map_icon,
			icon_for_point_selected: G_dam_map_icon_selected,
			dighe: true
		});
		// Aggiunto il 7_9_2015		
		this.map.addLayer(this.digheLayer.vector);
		if(!this.damSemaphore){
		    this.damSemaphore = true;
			this.dig = {
				"legend_name":"Dighe",
				"display": true,
				"output_name": geoJson,
				"stroke_color": "red",
				"stroke_width": 5,
				"opacity": 1,
				"fill_color": "red",
				"radius": 3,
				"type": "point"
			}
			this.digheLayer.vector.fromBDD = true;
			this.digheLayer.vector.legend_name = (this.dig.legend_name);
			this.digheLayer.vector.setVisible(this.dig.display);
			this.dig.olLayer = this.digheLayer.vector;
			
			var pos = this.getLayerPosition(this.digheLayer.vector);
			this.dig.position = pos;
			 
			// G_layer_list_scope().$apply(function(){G_layer_list_scope().addDigLayer(this.dig);});
		}
	};
	
	this.json2geojson = function(jsonObj) {
		var _geoJsonArray = new Array();
		
		for(var key in jsonObj){
			var mercator = new ol.proj.transform([parseFloat(jsonObj[key].longCentrale),parseFloat(jsonObj[key].latCentrale)], 'EPSG:4326', 'EPSG:3857');
			var newfeature = {
				'type': 'Feature',
				'id': '1',
				'geometry':
				{
					'type': 'Point',
					'coordinates': mercator
				},
				'geometryName': 'a',
				'properties':
				{
					'bbox': mercator,
					'name': jsonObj[key].nomeDiga,
					'ArcSub': jsonObj[key].id,
					'GridObj': jsonObj[key]
				}
			};
			_geoJsonArray.push(newfeature);
		}
		var digheGeoJson = (
			{
				"type":"FeatureCollection",
				"features": _geoJsonArray
			}
		);
		return digheGeoJson;
	};
	
	this.tooltipOpen = true;
	this.currentFeature=null;
	this.digheTooltip = jQuery.debounce(250, function(_evt){
		var that=this;			
		var pixel = that.map.getEventPixel(_evt);
		var feature = that.map.forEachFeatureAtPixel(pixel, function(feature, layer) {
			if(layer){
				if(layer.fromBDD) return feature;
			}
		});
		if(feature){
			that.setTooltContent(that, feature.get('name'));
		}
		that.openTooltip(feature, that, _evt);
	});
		
	this.setTooltContent=function(that, content){
		that.tooltDiv.html(content);
	};
	
	this.openTooltip=function(feature, that, _evt){
		if (feature){
			if(that.currentFeature==null || that.currentFeature.get('name')!=feature.get('name')){
				that.currentFeature=feature;
				that.tooltip.tooltip('close');
			}
			that.tooltDiv.css({position:"absolute", left:_evt.pageX,top:_evt.pageY});
			that.tooltip.tooltip('open');
		} else {
			that.tooltip.tooltip('close');
		}
	};
	
	
	this.getLatLongFromEvent = function(_evt){
		var pixel = this.map.getEventPixel(_evt);
		var coord = this.map.getCoordinateFromPixel(pixel);
		var lonlat = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
		return lonlat;
	};
	
	
/**
		Selection Feature
**/	
	this.loadFeatureInList = function(_layer){
	
		var nameFeat = new Array();
		var obj = _layer.olLayer.getSource().getFeatures();
		obj.sort(sorters);
		var temp = new Array();
		for (key in obj){
			obj[key].GeometryName = obj[key].getProperties().name.replace(/&#39;/g, "'");;
			obj[key].layerName = _layer.legend_name;
		} 
		return obj;
	}; 
	
	// this.loadFeatureInListFromCharge = function(obj){
	// obj.sort(sorters);
		// var temp = new Array();
		// for (key in obj){
			// obj[key].GeometryName = obj[key].getProperties().name.replace(/&#39;/g, "'");;
			// obj[key].layerName = _layer.legend_name;
		// } 
		// return obj;
		// };
	
	
	var sorters =  function(a,b) {
		return ((a.getProperties().name < b.getProperties().name) ? -1 : ((a.getProperties().name > b.getProperties().name) ? 1 : 0));
	};
		
	this.loadColumn = function(_fts){
	
		var colValue = [];
		var colName = _fts.getProperties().nomeCampi;
		for(idx in colName){
			var columnValue = _fts.getProperties()[colName[idx]];
			colValue.push(columnValue);
		}
		return {colName:colName, colValue:colValue};
	}; 
		
    this.loadColumnInList = function(_layer){
	
		var obj = _layer.olLayer.getSource().getFeatures();
		var colValue = [];
		var colName = obj[0].getProperties().nomeCampi;
		for (key in obj){
			var colValueTemp = [];
			for(idx in colName){
				var columnValue = obj[key].getProperties()[colName[idx]];
				colValueTemp.push(columnValue);
			}
			colValue.push(colValueTemp);
		}	
		return {colName:colName, colValue:colValue};
	}; 
	
	this.hideFeatureLayers = function(_layerList){
		for(key in _layerList){
			_layerList[key].olLayer.getSource().showFeature = false;
		}
	}
	
	this.hideColumnLayers = function(_layerList){
		for(key in _layerList){
			_layerList[key].showColumn = false;
		}
	}
	
	// Da Elenco
	this.selectFeatureOnMap = function(_feature, overlayActive, bufferActive, featureDamActive) {
	
		this.overlayActive = overlayActive;
		if (bufferActive || overlayActive){
			if (bufferActive) {
				this.BufferLayer.getFeatures().clear();
				this.BufferLayer.addFeature(_feature);
				G_layer_list_scope().addBuffer(_feature);
			}																						
			if (overlayActive) {		
				var tt = this.OverlayLayer.getFeatures().getArray();
				if($.inArray(_feature, tt) === -1){
					this.OverlayLayer.addFeature(_feature);
					G_layer_list_scope().addOverlay(_feature);
				}
			}
		}
		else{		
			var fts = this.SelectedLayer.getFeatures();	
			
			fts.forEach(function(element, index, array){
			this.removeFeature(element);
			}, this.SelectedLayer);
			
			this.removeSelectLayer();
			this.SelectedLayer.addFeature(_feature);
			// _feature.layerName = name;
			G_layer_list_scope().addSelection(_feature);
			if (featureDamActive) this.selectDamsFromFeature(_feature, true); /*   G_layer_list_scope().filtraDighe();*/
		}
	}
	
	this.selectLayerOnMap = function(pixel, overlayActive, bufferActive, featureDamActive, featureLayerActive, featActive) {
		var name = "";
		var feature = this.map.forEachFeatureAtPixel(pixel, function(feature, layer) {	
			if(layer!=null && !layer.dighe){
				name = layer.legend_name;
				feature.fiumi = layer.fiumi;
				return feature;
			}
		});		
		if (feature && featActive && !this.drawCircle.getActive() && !this.drawPoly.getActive() && !this.dragBox.getActive()) {
			feature.layerName = name;
			if (bufferActive || overlayActive){
				if (bufferActive) this.addFeature2Buffer(feature);
				if (overlayActive) this.addFeature2Overlay(feature);
			}
			else{
				var fts = this.SelectedLayer.getFeatures();
				fts.forEach(function(element, index, array){
					this.removeFeature(element);
				}, this.SelectedLayer);
				this.removeSelectLayer();
				this.SelectedLayer.addFeature(feature);
				G_layer_list_scope().addSelection(feature);
				if (featureDamActive) G_layer_list_scope().filtraDighe();  /* this.selectDamsFromFeature(feature);  */  
				if(featureLayerActive) G_layer_list_scope().filtraLayer();
				featActive = false;
			}
			
		}
		return {feature:feature};
	};
	
	this.selectDamsFromFeature = function(_feature, _SelFromFeat){
	
		var _control = _feature.getProperties().GeometryControl;
		if (_control) var featGeoJson = _feature.n;
		else var featGeoJson = this.ToMultiPoly(_feature, 'GeoJSON');
		var extent = _feature.getGeometry().getExtent();
		
		this.digheLayer.vectorSource.forEachFeatureIntersectingExtent(extent, (function(DammapObj, control, _featGeoJson)
		{return function(evt)
			{
				var _ctrl;
				_ctrl = DammapObj.isOverlay(_featGeoJson, evt, control);
			     if(_ctrl){
					// if(!_feat.FlagSisma){	
						DammapObj.addDam2MultiPoints(evt);
					// }
					// else
					// {
						// DammapObj.SismaControllo1(_feat,evt);
					// }
				}
			};
		})(this, _control, featGeoJson));
		
		var _selectedDamIDHome = $.extend(true, {}, this.selectedDamIDHome);
		
		if(this.getObjLength(this.selectedDamIDHome) == 0){
			G_dam_list_scope().$apply(function(){G_dam_list_scope().setDamsListFromMap({});});
			if(_SelFromFeat) G_layer_list_scope().removeFilter();
			else             G_layer_list_scope().$apply(function(){G_layer_list_scope().removeFilter();});
		}
		
		else G_dam_list_scope().$apply(function(){G_dam_list_scope().setDamsListFromMap(_selectedDamIDHome);});		
	};

	this.openClickToolTip = function(_str, _coo){
	
			var content = $('#popup-content');
			var element = $('#popup');
			var closer = $('#popup-closer');
			
			this.map.removeOverlay(this.overlay);
			
			this.overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
				element: element,
				autoPan: true,
				autoPanAnimation: {
					duration: 250
				}
			}));
			
			closer.click((function(DammapObj){return function(){
				DammapObj.overlay.setPosition(undefined);
				closer.blur();
				return false;
			};})(this));
			
			content.html(_str);
			this.overlay.setPosition(_coo);
			this.map.addOverlay(this.overlay);
	};
	
	this.selectDamWithTooltip=function(evt, pixel, bufferActive){
		var that=this;
		var data=this.selectDamOnMap(pixel, bufferActive);
		var gridObj=data.gridObj;
		var feature=data.feature;
		if(typeof feature!== "undefined"){	
			var str = this.buildPopupString(gridObj, G_POPUP_DAMS);
			
			var coo = feature.getGeometry().getCoordinates();
			this.openClickToolTip(str, coo);
		}
		else this.selectRiverWithTooltip(evt, pixel);
	};
	
	this.selectRiverWithTooltip=function(evt, pixel){
		var that=this;
		var data=this.selectLayerOnMap(pixel);
		
		if (data.feature != undefined){
			
			var isfiume=data.feature.fiumi;
			if(isfiume){
				
				var _infoFiumi;
				G_layer_list_scope().$apply(function(){
					_infoFiumi = G_layer_list_scope().infoFiumiPopup;
				});
				
				var idFiume = data.feature.getProperties().id_fiume;
				
				for(var key in _infoFiumi){
					if(_infoFiumi[key].id == idFiume) break;
				}
				var infoObj = _infoFiumi[key];
				infoObj.nome = data.feature.getProperties().name;
				if(infoObj !== undefined){
					var str = this.buildPopupString(infoObj, G_POPUP_RIVERS);
					var coo = data.feature.getGeometry().getCoordinates();
					this.openClickToolTip(str, coo[0][0]);
				}
			}
		}
	};
	
	this.selectDamOnMap = function(pixel, bufferActive) {
	
		var _gridObj =[];
		var feature=null;
		var that=this;
		feature = this.map.forEachFeatureAtPixel(pixel, function(feature, layer) {
			if(layer!=null && layer.dighe) return feature;
		});		
		if (feature && !this.drawCircle.getActive() && !this.drawPoly.getActive() && !this.dragBox.getActive()) {
			G_dam_list_scope().$apply(function(){G_dam_list_scope().selectDam(feature.get('ArcSub'));});
			var fts = this.SelectedDams.getFeatures();
			
			fts.forEach(function(element, index, array){
				this.removeFeature(element);
			}, this.SelectedDams);
			
			this.SelectedDams.addFeature(feature);
			var _gridObj = feature.getProperties().GridObj;
			var InfoBoxValues = [];
			
			if (bufferActive) this.addFeature2Buffer(feature);
		}
		return { 
			gridObj:_gridObj,
			feature: feature			
		};
	};
	
	this.selectDamOnGrid = function(damId, _move, _zoom) {
		var feature = this.digheLayer.vectorSource.forEachFeature(function(feature) {
			if(feature.get('ArcSub') == this) return feature;
		}, damId);
		
		
		var fts = this.SelectedDams.getFeatures();
		
		var passObj = {SelectedDams: this.SelectedDams};
		fts.forEach(function(element, index, array){
			this.SelectedDams.removeFeature(element);
			this.oldDam = element;
		}, passObj);
		
		var newDam = true;
		if(passObj.oldDam == feature) newDam = false;
		
		if (feature) {
			this.SelectedDams.addFeature(feature);
			if(_move && newDam) {
				if(_zoom) this.zoomTo(feature.getGeometry().getClosestPoint(this.view.getCenter()));
				else this.panTo(feature.getGeometry().getClosestPoint(this.view.getCenter()));
			}
		}
	};	
	
/* 	this.selectDamOnOnda = function(damId, _move, _zoom) {
		var feature = this.digheLayer.vectorSource.forEachFeature(function(feature) {
			if(feature.get('ArcSub') == this) return feature;
		}, damId);
		
		
		var fts = this.SelectedDams.getFeatures();
		
		var passObj = {SelectedDams: this.SelectedDams};
		fts.forEach(function(element, index, array){
			this.SelectedDams.removeFeature(element);
			this.oldDam = element;
		}, passObj);
		
		var newDam = true;
		if(passObj.oldDam == feature) newDam = false;
		
		if (feature) {
			this.SelectedDams.addFeature(feature);
			if(_move && newDam) {
				if(_zoom) this.zoomTo(feature.getGeometry().getClosestPoint(this.view.getCenter()));
				else this.panTo(feature.getGeometry().getClosestPoint(this.view.getCenter()));
			}
		}
	}; */
	
	this.buildPopupString = function(_obj, _config){		
		var str='<ul class="list-unstyled">';
		for(var key in _config){
			var resolution = this.map.getView().getResolution();
			if (_config[key].resolution > resolution || _config[key].resolution === undefined){
				var nomeCampo = _config[key].nome.replace(/ /g, "&nbsp;"); 
				var val = _obj[_config[key].field];
				if(val === undefined) val = "";
				str+="<li>"+nomeCampo+":&nbsp;"+val+"</li>";
			}
		}
		str+="</ul>";
		
		return str;
	};	


	
/** 	Layer
		PRM: (layer, output_name, srsname, stroke_color, stroke_width, opacity, fill_color, icon_for_point, icon_for_point_selected, dighe)
**/
	this.loadLayers = function(_layers){
		var ondaObj = G_dam_list_scope().getOndaDiPiena(G_dam_list_scope().ondePienaPrm);
		for(var _key in _layers){
			switch(_layers[_key].type){			
				case "wms":
					var newWmsLayer = new ol.layer.Tile({
						extent: [681746, 4367541, 2101583, 5983013], // ITALIA
						preload: Infinity,
						opacity: _layers[_key].opacity,
						source: new ol.source.TileWMS({
								url: _layers[_key].url,
								params: {'LAYERS': _layers[_key].layer, 'TILED': true }
						})
					});
					this.map.addLayer(newWmsLayer);
					
					newWmsLayer.setVisible(_layers[_key].display);
					_layers[_key].olLayer = newWmsLayer;
					var pos = this.getLayerPosition(newWmsLayer);
					_layers[_key].position = pos;
					G_layer_list_scope().$apply(function(){G_layer_list_scope().addLayer(_layers[_key]);});
					break;		
				case "line":
				case "vector":
					var newLayer = new this.vectorLayer({
						layer: _layers[_key].layer,
						output_name: _layers[_key].output_name,
						srsname: G_srsname_default,
						minResolution: _layers[_key].minResolution,
						maxResolution: _layers[_key].maxResolution,
						stroke_color: _layers[_key].stroke_color,
						stroke_width: _layers[_key].stroke_width,
						opacity: _layers[_key].opacity,
						fill_color: _layers[_key].fill_color
					});
					
					
					// newLayer.vector.showFeature = false;
					var _featView = this.ondaControl(ondaObj, _layers[_key]);
					
					newLayer.vector.fromBDD = true;
					newLayer.vector.legend_name = _layers[_key].legend_name;
					newLayer.vector.fiumi = _layers[_key].fiumi;
					this.map.addLayer(newLayer.vector);
					newLayer.vector.setVisible(_layers[_key].display);
					_layers[_key].olLayer = newLayer.vector;
					_layers[_key].olLayer.getSource().showFeature =_featView;
					
					
					var pos = this.getLayerPosition(newLayer.vector);
					_layers[_key].position = pos;
					G_layer_list_scope().$apply(function(){G_layer_list_scope().addLayer(_layers[_key]);});
					break;			
				case "point":
					var newLayer = new this.vectorLayer({
						layer: _layers[_key].layer,
						output_name: _layers[_key].output_name,
						srsname: G_srsname_default,
						minResolution: _layers[_key].minResolution,
						maxResolution: _layers[_key].maxResolution,
						opacity: _layers[_key].opacity,
						icon_for_point:  new ol.style.Circle({fill: new ol.style.Fill({color: _layers[_key].fill_color}),stroke: new ol.style.Stroke({color: _layers[_key].stroke_color,width: _layers[_key].stroke_width}),radius: _layers[_key].radius})
					});
					
					newLayer.vector.showFeature = false;
					newLayer.vector.fromBDD = true;
					newLayer.vector.legend_name = _layers[_key].legend_name;
					this.map.addLayer(newLayer.vector);
					newLayer.vector.setVisible(_layers[_key].display);
					_layers[_key].olLayer = newLayer.vector;
					
					var pos = this.getLayerPosition(newLayer.vector);
					_layers[_key].position = pos;
					 
					G_layer_list_scope().$apply(function(){G_layer_list_scope().addLayer(_layers[_key]);});
					break;
				case "dighe":
					/* this.digheLayer = new this.vectorLayer({
						layer: _layers[_key].layer,
						output_name: _layers[_key].output_name,
						srsname: G_srsname_default,
						minResolution: _layers[_key].minResolution,
						maxResolution: _layers[_key].maxResolution,
						stroke_color: _layers[_key].stroke_color,
						stroke_width: _layers[_key].stroke_width,
						opacity: _layers[_key].opacity,
						fill_color: _layers[_key].fill_color,
						icon_for_point: G_dam_map_icon,
						icon_for_point_selected: G_dam_map_icon_selected,
						dighe: true
					}); */
					break;
				default:
					break;
			}
			//Onde Sommersione
		/* 	if (ondaObj.active && (_layers[_key].name == "onde_area")){
			console.log(_layers[_key]);
			G_layer_list_scope().leggiFeatures(_layers[_key]);
				this.view.zoom = 3.5;
				// this.CaricaLayerOndePiena(_layers[_key], ondaObj);		
			} */
			
		}	
	};

	/* this.getFirstActiveLayer = function(){
		var layers = this.map.getLayers();		
		var length = layers.getLength();
		for (var i = length - 1; i >= 0; i--) {
			if(layers.item(i).fromBDD){
				if(layers.item(i).getVisible()) return layers.item(i);
			}
		}
		return false;
	}; */
	
	this.getActiveLayer = function(){
		var layers = this.map.getLayers();
		var listLayers = [];
		
		var length = layers.getLength();
		for (var i = length - 1; i >= 0; i--) {
			if(layers.item(i).fromBDD){
				if(layers.item(i).getVisible()) {
					listLayers.push(layers.item(i))
				
				}
			}
		}
		if (listLayers != []){
		return listLayers;}
		else
		return false;
	};
	
	this.moveLayerPosition = function(_from, _to){
		if(_from == _to) return;
		
		var layers = this.map.getLayers();
		var _layer = layers.item(_from);
		
		if(_from > _to){
			for(var i=_from-1 ; i >= _to ; i--){
				var _l = layers.item(i);
				layers.setAt(i+1, _l);
			}
		}
		
		
		if(_from < _to){
			for(var i=_from+1 ; i <= _to ; i++){
				var _l = layers.item(i);
				layers.setAt(i-1, _l);
			}
		}
		
		layers.setAt(_to, _layer);
		
	};
	
	this.getLayerPosition = function(layer){
		var layers = this.map.getLayers();
		var length = layers.getLength();
		for (var i = 0; i < length; i++) {
			if (layer === layers.item(i)) return i;
		}
		return -1;
	};


	
/**
		Object Definition
**/		
	this.showOverlay = function(){
		this.OverlayLayer.setStyle(G_dam_map_layer_overlay);
	};	
	
	this.showBuffer = function(){
		this.BufferLayer.setStyle(G_dam_map_layerBuffer_selected);
	};
	
	this.hideOverlay = function(){
		this.OverlayLayer.setStyle(G_hidden_style);
	};
	
	this.hideBuffer = function(){
		this.BufferLayer.setStyle(G_hidden_style);
	};
	
	this.SelectedDams = new ol.FeatureOverlay({
		map: this.map,
		style: new ol.style.Style({image: G_dam_map_icon_selected})
	});
		
	this.OverlayLayer = new ol.FeatureOverlay({
		map: this.map,
		style: G_dam_map_layer_overlay
	});	
	
	this.BufferLayer = new ol.FeatureOverlay({
		map: this.map,
		style: G_dam_map_layerBuffer_selected
	});
	
	this.SelectedLayer = new ol.FeatureOverlay({
		map: this.map,
		style: G_dam_map_layer_selected
	});
	
	this.SelectedDamsMultiPoint = new ol.FeatureOverlay({
		map: this.map,
		style: new ol.style.Style({image: G_dam_map_icon_selected_MultiPoint})
		
	});
	
	this.FilteredLayer = new ol.FeatureOverlay({
		map: this.map,
		style: G_dam_map_layer_selected
	});
	
	
	
/**
		Filtered Features into selected Layer
**/		
	this.selectDams4CurrentFeat = function(){
		
		var _layer = [];
		var fts = this.SelectedLayer.getFeatures();
		
		fts.forEach(function(element, index, array){
			this.push(element);
		}, _layer);
		
		this.removeSelectDams();
		if (_layer.length != 0){
		this.selectDamsFromFeature(_layer[0]);}
	};
	
	this.selectLayer4CurrentFeat = function(){
		var _layer = [];
		var fts = this.SelectedLayer.getFeatures();
		
		fts.forEach(function(element, index, array){
			this.push(element);
		}, _layer);
		
		//this.removeSelectDams();
		if (_layer.length != 0){
			var mapValues = this.startSelectIntersectLayer(_layer);
			return mapValues;
		}
		else return;
	};
	
	this.controlHole = function(_coor, arrayLength, pos){
	
		var holeControl = new Array();
		var result_coord = new Array();
		
		for (var key in _coor) if (key != pos) holeControl.push(turf.polygon(_coor[key]));
		
		var _c = this.isDifferenceWithHole(turf.polygon(_coor[pos]), holeControl, pos);
		if(_c.bool){
			var idx = _c.holePosition.length - 1;
			while (idx >= 0){
				if (_c.holePosition[idx] != "-1") _coor.splice(_c.holePosition[idx], 1);
				idx--; 
			}
		} 
		return _coor;
	}; 
	 
	this.ToMultiPoly = function(_feature, _type){
	
		var _geojson = new ol.format.GeoJSON();
		var _geojsonFinal = new ol.format.GeoJSON(); 
		var _geojsonFeat = _geojson.writeFeatureObject(_feature);
		
		if(_geojsonFeat.geometry.coordinates.length > 1 && (_geojsonFeat.geometry.type != "MultiLineString")) {
			var _coor = _feature.getGeometry().getCoordinates();
			var _array = [];
			var _arrayLength = [];
			var pos = -1, max = -1;
			var j = 0; 
			if (_geojsonFeat.geometry.type == "Point")  _array.push([_coor]);
			else
			{
				while (j < _geojsonFeat.geometry.coordinates.length){
					if(Array.isArray(_coor[j][0][0])) _array.push(_coor[j]);
					else _array.push([_coor[j]]);
					_arrayLength.push(_array[j][0].length);
					if(_arrayLength[j] > max){
						pos = j;
						max = _arrayLength[j];
					}
					j = j+1;
				} 
				/* if (_geojsonFeat.geometry.type == "MultiLineString") var _multi = new ol.geom.MultiLineString(_array);
				else{ */
					if (_geojsonFeat.properties.GeometryControl === undefined || !_geojsonFeat.properties.GeometryControl)
						_array = this.controlHole(_array, _arrayLength, pos); 
					var _multi = new ol.geom.MultiPolygon(_array);
				/* } */
				_feature.setGeometry(_multi);
			}
		}	
		var _geojsonResult = _geojsonFinal.writeFeatureObject(_feature);
		if (_type == 'feature'){
			return _feature;
		}
		if (_type == 'GeoJSON'){
			return _geojsonResult;
		}
	};
	
	this.addDam2MultiPoints = function(_feature){
		var fts = this.SelectedDamsMultiPoint.getFeatures();
		this.selectedDamID.push(_feature.get('ArcSub'));
		this.selectedDamIDHome[_feature.get('ArcSub')]=true;
	};
	
	this.setName = function(_layer){
	console.log(_layer);
		var name = "";
		switch (_layer.getProperties().name/* GeometryName */){
			case 'Selezione Box':
				name = _layer.getProperties().name+'\nEstremi: ['+_layer.getProperties().ext1+'] - ['+_layer.getProperties().ext2+']';
			break;
			case 'Selezione Circolare':
			    name = _layer.getProperties().name+'\nCentro: ['+_layer.getProperties().center+']\nRaggio: '+_layer.getProperties().radius+' Km';
			break;
			case 'Selezione Poligonale':
				name = _layer.getProperties().name+' a '+(_layer.getGeometry().getCoordinates()[0].length-1)+' lati';
			break;
			case 'Controlli Tipo 1':
				name = 'Sisma: Controlli di tipo 1 \nMagnitudo: '+_layer.getProperties().magnitudo+'\nEpicentro: ['+_layer.getProperties().center+']. \nCopertura controlli: da '+_layer.getProperties().radius1+' a '+_layer.getProperties().radius+' Km dall\'epicentro';
			break;
			case 'Controlli Tipo 2':
				name = 'Sisma: Controlli tipo 2 \nMagnitudo: '+_layer.getProperties().magnitudo+'\nEpicentro: ['+_layer.getProperties().center+']. \nCopertura controlli: fino a '+_layer.getProperties().radius+' Km dall\'epicentro';
			break;
			case 'Cerchio Sisma':
				name = 'Informazioni Sisma \nMagnitudo: '+_layer.getProperties().magnitudo+'\nEpicentro: ['+_layer.getProperties().center+']\nRaggio massimo: '+_layer.getProperties().radius+' Km dall\'epicentro';
			break;
		}
		// if(_layer.layerName == "Onda di Collasso")  name = layerList.layerName.replace(/&#39;/g, "'")+': '+layerList.GeometryName.replace(/&#39;/g, "'")+'\nPortata massima scaricata: '+layerList.getProperties().q_max_dig+' mc/s\nQuota di massimo invaso: '+layerList.getProperties().q_max_inv+'m.s.m.\nQuota massima di regolazione: '+layerList.getProperties().q_max_reg+'m.s.m.';
		
		return name;
	}
	
	this.setNameOverlay = function(_name1, _name2, type){
		var title = "";
		switch (type){
			case 'UNION':
				title = _name1+'\n    **UNION**   \n'+_name2;
			break;
			case 'INTERSECT':
				title = _name1+'\n    **INTERSECT** \n'+_name2;
			break;
			case 'DIFF':
				title = _name1+'\n    **DIFF** \n'+_name2;
			break;
			case 'BUFFER':
				title = _name1+' \n+  BUFFER '+_name2+' km';
			break;
		}
		return title;
	};
	
	this.startSelectIntersectLayer = function(_layerList){
		var layers = this.getActiveLayer();
		if(layers.length == 0) return alert('Nessuna copertura selezionata nella scheda "Layer".'); 
		else {
			var mapValues = new Array();
			var lName = new Array();
			var nameSelection = "";
			var lColumns = new Array();
			var mapValuesColumns=new Array();
			
			var layerList = _layerList[0];
			var extent = layerList.getGeometry().getExtent();
			var _control = layerList.getProperties().GeometryControl;
			if (_control) var featLayer_1 = layerList.n;
			else var featLayer_1 = this.ToMultiPoly(layerList, 'GeoJSON');
			
			for (var i = 0; i < layers.length; i++){
				this.filteredLayerID = [];
				this.filteredLayerCampi = [];
				this.FilteredLayer.getFeatures().clear();
				var lName_temp="";
				var lColumnsTemp="";
				var mapValues_temp=[];
				var mapValuesColumns_temp=[];
				
				if(_layerList.length > 0){
				// this.setName(layerList);
					if(layerList.layerName === undefined) 	{
						// nameSelection = 'Feature Utente: '+layerList.GeometryName;	
						layerList.GeometryName = /* this.setName(layerList) */layerList.getProperties().name;
						nameSelection = layerList.GeometryName.replace(/&#39;/g, "'");
					}
					else nameSelection = layerList.layerName.replace(/&#39;/g, "'")+': '+layerList.GeometryName.replace(/&#39;/g, "'");
					
					if ((layerList.layerName != layers[i].legend_name && layers[i].legend_name != 'Dighe') &&
						(layers[i].getProperties().minResolution<this.view.getResolution() && this.view.getResolution()<layers[i].getProperties().maxResolution)){
						var fts = layers[i].getSource().getFeatures();
						
						for (var key in fts){
								lName_temp=layers[i].legend_name;
								lColumnsTemp= this.loadColumn(fts[key]).colName;
								if(this.isOverlay(featLayer_1, fts[key], _control)){
									if($.inArray(fts[key].get('name').replace(/&#39;/g, "'"), this.filteredLayerID) === -1 ){
										this.filteredLayerID.push(fts[key].get('name').replace(/&#39;/g, "'"));
										if (fts[key].get('name').replace(/&#39;/g, "'") != "") 
											mapValues_temp.push(fts[key].get('name').replace(/&#39;/g, "'"));
										if(lColumnsTemp === undefined) lColumnsTemp = [];
										else
										{						
											// if($.inArray(fts[idx].get(lColumns[0]), this.filteredLayerID) === -1){
												var prova = this.loadColumn(fts[key]).colValue;
												mapValuesColumns_temp.push(prova);
											// }
										}
									}
								}
							}
						// })(this, layers[i].legend_name,_control));
						/* layers[i].getSource().forEachFeatureIntersectingExtent(extent, (function(DammapObj, layerName,_control){
							return function(fts){
								lName_temp=layerName;
								lColumnsTemp= DammapObj.loadColumn(fts).colName;
								if(DammapObj.isOverlay(featLayer_1, fts, _control)){
									if($.inArray(fts.get('name').replace(/&#39;/g, "'"), DammapObj.filteredLayerID) === -1 ){
										DammapObj.filteredLayerID.push(fts.get('name').replace(/&#39;/g, "'"));
										if (fts.get('name').replace(/&#39;/g, "'") != "") 
											mapValues_temp.push(fts.get('name').replace(/&#39;/g, "'"));
										if(lColumnsTemp === undefined) lColumnsTemp = [];
										else
										{						
											// if($.inArray(fts[idx].get(lColumns[0]), this.filteredLayerID) === -1){
												var prova = DammapObj.loadColumn(fts).colValue;
												mapValuesColumns_temp.push(prova);
											// }
										}
									}
								}
							};
						})(this, layers[i].legend_name,_control)); */
						
						mapValuesColumns.push(mapValuesColumns_temp);
						lColumns.push(lColumnsTemp);
					}
				}
				// console.log(lColumns);
				if (lName_temp != ""){
					lName.push(lName_temp);
					mapValues.push(mapValues_temp);
				}
			}
		// var jsonObj = this.mapValue2json(nameSelection, lName,mapValues, lColumns, mapValuesColumns);
		

		// return {selectionName:nameSelection, layerName:lName, mapValues:mapValues/* [[1]] */, jsonObj:jsonObj}; 
		return {selectionName:nameSelection, layerName:lName, mapValues:mapValues, layerColumns:lColumns, mapValuesColumns:mapValuesColumns}; 
		}
	};	

	this.mapValue2json = function (_selection, _nameLayer, _values, _nameColumns, _valuesColumns) {
	    console.log(_selection);
	    console.log(_nameLayer);
	    console.log(_values);
	    console.log(_nameColumns);
	    console.log(_valuesColumns);

	    var order = 1;
		var ArrayFeat = [];
		for(var key in _nameLayer){
		    var ArrayValue = [];
			for(var idx in _values[key]){
				var values = {/* 'nome': _values[key][idx] */};
				if(_nameColumns != []){
					for (var pos in _nameColumns[key]){
						 values[_nameColumns[key][pos]] = _valuesColumns[key][idx][pos];
						}
				ArrayValue.push(values);
				}
			}
			var Layer = {
				'gruppo': _nameLayer[key],
				'colonne':_nameColumns[key],
				'righe':ArrayValue,
				'ordine': order
			};
			order = order+1;
			ArrayFeat.push(Layer);
		}
		
		var jsonOut = {'titolo': _selection, 'dati':ArrayFeat};
		jsonStr = JSON.stringify(jsonOut);
		/* console.log(jsonOut); */
		return jsonOut;
	} /**/
	
	this.zoomToSelectFeature = function(_feat){
		console.log(this.view.getResolution());
        var polygon =  /** @type {ol.geom.SimpleGeometry} */ (_feat.getGeometry());
		if(_feat.getGeometry().getCoordinates().lengt == 1){
			this.view.centerOn(
			  polygon.getCoordinates(),
			  this.map.getSize(),
			  [570, 500]
			);
		}
		else{
			this.view.fitGeometry(
				polygon,
				this.map.getSize(),
				{
				padding: [20, 0, 20, 0],
				constrainResolution: false
				}
			);	
		}

		if (this.view.getResolution() < 100) 
	    this.view.setResolution(152);
	};
	
/**
*		Interaction & Draw
*/	
	this.selectedDamID = [];
	this.filteredLayerID = [];
	this.selectedDamIDHome={};
	this.overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({}));
	this.marker = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({}));
	
	this.dragBox = new ol.interaction.DragBox({
		map: this.map,
		style:G_poly_style 
	});
	this.dragBox.setActive(false);	
	
	this.dragBox.on('boxstart', this.Box = (function(DammapObj){
		return function(e)
			{
				if (DammapObj.featureDamActive.active) 
				DammapObj.removeSelectDams();
			};
		}
		)(this));	
	
	this.dragBox.on('boxend', this.Box = (function(DammapObj){
		return function(e)
			{
				G_layer_list_scope().selectPan();
				// Draw Box Feature
				var extent = this.getGeometry().getExtent();
				var box = [[extent[0], extent[1]], [extent[0], extent[3]],[extent[2], extent[3]], [extent[2], extent[1]],[extent[0], extent[1]]];
				var info_extent = [new ol.proj.transform([extent[0], extent[3]], 'EPSG:3857', 'EPSG:4326'),new ol.proj.transform([extent[2], extent[1]], 'EPSG:3857', 'EPSG:4326')];
				var stringifyFunc = ol.coordinate.createStringXY(2);
				var polygon = new ol.geom.Polygon([box]);
				var feat = new ol.Feature(polygon);
				feat.set("name", 'Selezione Box');
				feat.set("ext1", stringifyFunc(info_extent[0]));
				feat.set("ext2", stringifyFunc(info_extent[1]));
				feat.set("name", DammapObj.setName(feat));
				if (DammapObj.overlayActive.active || DammapObj.bufferActive.active){
					if (DammapObj.overlayActive.active) DammapObj.addFeature2Overlay(feat);
					if (DammapObj.bufferActive.active) DammapObj.addFeature2Buffer(feat);
				}
				else{
					// pulizia
					DammapObj.removeSelectLayer();
					DammapObj.SelectedLayer.addFeature(feat);
					G_layer_list_scope().addSelection(feat);
					if (DammapObj.featureDamActive.active) G_layer_list_scope().filtraDighe();
					if (DammapObj.featureLayerActive.active) G_layer_list_scope().filtraLayer();
				}
				DammapObj.dragBox.setActive(false);
			};
		}
		)(this));
	
	this.drawPoly = new ol.interaction.Draw({
		map: this.map,
		type:"Polygon",
		style: G_poly_style
	});
	this.drawPoly.setActive(false);	
	
	this.drawPoly.on('drawend', this.drawPoligon = (function(DammapObj){
		return function(e)
			{
				//Draw Circle e estrai informazioni
				e.feature.set("name", 'Selezione Poligonale');
				e.feature.set("name", DammapObj.setName(e.feature));
				//CONTROL
				var poly = turf.polygon([e.feature.getGeometry().getCoordinates()[0]]);
				var _controlPoly=turf.kinks(poly);

				if (_controlPoly.intersections.features.length != 0) alert('Il poligono disegnato non è valido, poichè presenta linee incrociate.');
				else{
					if (DammapObj.overlayActive.active || DammapObj.bufferActive.active){
						if (DammapObj.overlayActive.active) DammapObj.addFeature2Overlay(e.feature);
						if (DammapObj.bufferActive.active) DammapObj.addFeature2Buffer(e.feature);
					}
					else{
						DammapObj.removeSelectLayer();
						DammapObj.SelectedLayer.addFeature(e.feature);
						G_layer_list_scope().addSelection(e.feature);
						if (DammapObj.featureDamActive.active)   G_layer_list_scope().filtraDighe();
						if (DammapObj.featureLayerActive.active) G_layer_list_scope().filtraLayer();
					}
				}
			};
		}
	)(this));		
	
/* 	this.controlPoly = function (_coord){
	var line = [];
	var feat = [];
	var bool = false;
		for (var i in _coord){
			if (i != 0)  {
				line.push(new ol.geom.LineString([_coord[i], _coord[i-1]]));
				feat.push(new ol.Feature(line[i-1]))
			}
			console.log(feat);
		}
		for (var j in line){
				var resultControl = this.overlayTurfExecute(feat[j], feat[j+1], 'intersect',0);
				console.log(resultControl);
				if (resultControl != null) bool = true;
		}
		return bool;
	}; */
	
	this.drawCircle = new ol.interaction.Draw({
		map: this.map,
		type:"Circle",
		style:G_poly_style
	});	
	this.drawCircle.setActive(false);	
	
	this.drawCircle.on('drawend', this.drawCir = (function(DammapObj){
		return function(e)
			{
			    // DammapObj.removeSelectLayer();
				var radius = e.feature.getGeometry().getRadius();
				var center = e.feature.getGeometry().getCenter();
				
				
				var center4326 = new ol.proj.transform(center, 'EPSG:3857', 'EPSG:4326');
				DammapObj.drawCircleOnMap(center4326, radius, 0,'Selezione Circolare', 0);
				
			};
		}
	   )(this));	

	this.removeSelectLayer = function(){
		this.SelectedLayer.getFeatures().clear();
		this.removeSelectDams();
	};
	
	this.removeSelectDams = function(){
		this.SelectedDamsMultiPoint.getFeatures().clear();
		this.selectedDamID = [];
		this.selectedDamIDHome = {};
	};		
	
	this.removeInteraction = function(){
		this.map.removeInteraction(this.dragBox);
		this.map.removeInteraction(this.drawCircle);
		this.map.removeInteraction(this.drawPoly);
		this.dragBox.setActive(false);
		this.drawCircle.setActive(false);
		this.drawPoly.setActive(false);
	};
	
	this.startSelectBox = function(overlayActive, bufferActive, featureDamActive, featureLayerActive){
		//this.removeSelectLayer();
		this.removeInteraction();
		this.map.addInteraction(this.dragBox);
		this.dragBox.setActive(true);		
		this.overlayActive = overlayActive;
		this.bufferActive = bufferActive;
		this.featureDamActive = featureDamActive;
		this.featureLayerActive = featureLayerActive;
		
	};	
	
	this.startSelectCircle = function(overlayActive, bufferActive, featureDamActive, featureLayerActive){
		//this.removeSelectLayer();
		this.removeInteraction();
		this.map.addInteraction(this.drawCircle);
		this.drawCircle.setActive(true);
		this.overlayActive = overlayActive;
		this.bufferActive = bufferActive;
		this.featureDamActive = featureDamActive;
		this.featureLayerActive = featureLayerActive;
		this.remove = function(pixel) {
			
		};
	};
	
	this.addMarker = function(_evt){
		this.map.removeOverlay(this.marker);
		
		this.marker = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
			element:  $('<img src="resources/img/marker.png">').css({marginTop: '-200%', marginLeft: '-50%'}),
			autoPan: true,
			autoPanAnimation: {
				duration: 250
			}
		}));
		var pixel = this.map.getEventPixel(_evt);
		var coord = this.map.getCoordinateFromPixel(pixel);
		
		this.marker.setPosition(coord);
		
		this.map.addOverlay(this.marker);
		
	};
	
	this.removeMarker = function(){
		this.map.removeOverlay(this.marker);		
	};
		
	this.SismaControllo1 = function(_feature,event){
		var center = new ol.proj.transform([parseFloat(_feature.center[0]),parseFloat(_feature.center[1])], 'EPSG:4326', 'EPSG:3857'); 
		var radius = _feature.radius;
		var radiusType1 = _feature.radiusType1;
		var coord = event.getGeometry().getCoordinates(); 
		var line = new ol.geom.LineString([coord, center]);
		if( line.getLength() < radius && line.getLength() > radiusType1){
			this.addDam2MultiPoints(event);
		}	
	};
	
    this.startCalculateSisma = function(overlayActive, featureDamActive, _lat, _long, _radius, _radiusType1, _type, bufferActive, _M){
		this.overlayActive = overlayActive;
		this.bufferActive = bufferActive;
		this.featureDamActive = featureDamActive;
		var name;
		if (_lat == 0 && _long == 0) alert('Inserire le coordinate')
		else{
			if(_radius == 0) alert('Inserire la magnitudo');
			else{
				var center = [parseFloat(_lat), parseFloat(_long)];
				switch(_type){
					case 0:
						name = 'Cerchio Sisma';
					break;
					case '0':
						name = 'Cerchio Sisma';
					break;
					case '1':
						name = 'Controlli Tipo 1';
					break;
					case '2':
						name = 'Controlli Tipo 2';
					break;
				}
				this.drawCircleOnMap(center, _radius/0.74, _radiusType1/0.74, name, _M);	
			}
		}
		
	};
	
	this.drawCircleOnMap = function(center, radius, radiusType1, name, _M){
		var wgs84Sphere = new ol.Sphere(6378137);
			var circle4326 = new ol.geom.Polygon.circular(wgs84Sphere, center, radius*0.74, 128);
			var circle3857 = circle4326.clone().transform('EPSG:4326', 'EPSG:3857');
		if (name != 'Controlli Tipo 1'){
			var feat = new ol.Feature(circle3857);
		    feat.set("center", (ol.coordinate.createStringXY(2))(center));
		    feat.set("radius", (radius*0.74/1000).toFixed(3));
			feat.set("magnitudo", _M);
		}
		else{
			var circle4326_Type1 = new ol.geom.Polygon.circular(wgs84Sphere, center, radiusType1*0.74, 128);
			var circle3857_Type1 = circle4326_Type1.clone().transform('EPSG:4326', 'EPSG:3857');
			var f_1 = new ol.Feature(circle3857);
			var feat_1 = new ol.Feature(circle3857_Type1);
			var overlaySisma = new Array();	
			overlaySisma.push(f_1);
			overlaySisma.push(feat_1);
			var feat = this.turfOverlay(overlaySisma, 'sisma');
			feat.FlagSisma = true;
			feat.GeometryName = name+' (HOLE)';
			feat.radius = (radius);
			feat.radiusType1 = (radiusType1);
			feat.center = center;
			feat.GeometryControl = true;
		    feat.set("center", (ol.coordinate.createStringXY(2))(center));
		    feat.set("radius", (radius*0.74/1000).toFixed(3));
		    feat.set("radius1", (radiusType1*0.74/1000).toFixed(3));
			feat.set("magnitudo", _M);
		}
		
	    feat.set("name", name);	
		feat.set("name",this.setName(feat));
		if (this.overlayActive.active || this.bufferActive.active){
		
			if (this.overlayActive.active) {
				setTimeout((function(DammapObj, feat){return function(){DammapObj.addFeature2Overlay(feat);};})(this, feat), 0);
				}				
			if (this.bufferActive.active) setTimeout((function(DammapObj, feat){return function(){DammapObj.addFeature2Buffer(feat);};})(this, feat), 0);		
		}
		else{
			this.removeSelectLayer();
			this.SelectedLayer.addFeature(feat);
			G_layer_list_scope().addSelection(feat);
			if (this.featureDamActive.active)   G_layer_list_scope().filtraDighe();
			if (this.featureLayerActive.active) G_layer_list_scope().filtraLayer();
		}
		
		
	};
		
	this.startSelectPoly = function(overlayActive, bufferActive, featureDamActive, featureLayerActive){
		//this.removeSelectLayer();
		this.removeInteraction();
		this.map.addInteraction(this.drawPoly);
		this.drawPoly.setActive(true);
		this.overlayActive = overlayActive;
		this.bufferActive = bufferActive;
		this.featureDamActive = featureDamActive;
		this.featureLayerActive = featureLayerActive;
		
	};
	
	

/**
*		Overlay & Buffering
*/	
	var diffHole = new Array();
	var diffExternal = new Array();
	var featureWithHole = new Array();		
	var diffHoleBuffer = new Array();
	var diffExternalBuffer = new Array();
	
	this.turfOverlay = function(_layerList, turfType){
	
		var _layerList_cp = $.extend(true, {}, _layerList);
		var featLayer_1 = this.ToMultiPoly(_layerList[0], 'GeoJSON');
		if (featLayer_1.properties === null) featLayer_1.properties = {};
		
		if (_layerList[0].layerName !== undefined) featLayer_1.properties.name =  _layerList[0].layerName+': '+featLayer_1.properties.name;

		while(_layerList.length > 1 || turfType == 'buffer'){
		
			if (turfType!= 'buffer'){
				var _f = _layerList.pop();
				var featLayer_2 = this.ToMultiPoly(_f, 'GeoJSON');
		        if (featLayer_2.properties === null) featLayer_2.properties = {};
				if (_f.layerName !== undefined){ featLayer_2.properties.name =  _f.layerName+': '+featLayer_2.properties.name};
			}
			switch(turfType){
				case 'union':
				    featLayer_1 = this.executeTurf(featLayer_1, featLayer_2, 'union');
					
					if(featLayer_1 !== undefined && !featLayer_1.isBad) {
						if(featLayer_1.properties === null) featLayer_1.properties = {};
						featLayer_1.properties.name = this.setNameOverlay(featLayer_1.properties.name, featLayer_2.properties.name, 'UNION');
					} 
					break;
				case 'intersect':
				    if(featLayer_1 === undefined || featLayer_2 === undefined) break;
					else
					{
						featLayer_1 = this.executeTurf(featLayer_1, featLayer_2, 'intersect');
							if(featLayer_1 !== undefined && !featLayer_1.isBad) {
								if(featLayer_1.properties === null) featLayer_1.properties = {};
								featLayer_1.properties.name = this.setNameOverlay(featLayer_1.properties.name, featLayer_2.properties.name, 'INTERSECT');
							}
						break;
					}
				case 'diff':
				    var controlGeometry;
					featLayer_1 = this.executeTurfSismaDiff(featLayer_1, featLayer_2, controlGeometry, 'Differenza');
					break;
				case 'sisma':
				    var controlGeometry;
					featLayer_1 = this.executeTurfSismaDiff(featLayer_1, featLayer_2, controlGeometry, 'Controlli Tipo 1');
					break;
			}
		}	
		
		if(featLayer_1){
		    this.removeOverlayFeatures(_layerList_cp);
			var feature = this.calculateResult(featLayer_1);
			return feature;
		}
		else return false;
	};
	
	this.turfBuffer = function(_layerList, bufferValues){	
	
		var _layerList_cp = $.extend(true, {}, _layerList);
		_feature = _layerList[0];
		var feat;
		this.removeBufferFeatures(_layerList_cp);
		var bufferNameReport = /* this.setName(_feature) */_feature.getProperties().name;
		
		if (_feature.layerName !== undefined) bufferNameReport = _feature.layerName+': '+bufferNameReport;
		
		var featLayer_1 = this.ToMultiPoly(_feature, 'GeoJSON');
		
		if(featLayer_1.properties.GeometryControl){
			var actualPos = featLayer_1.properties.Position;
			var newPos = diffExternal.length;
			diffExternal.push(diffExternal[actualPos]);
			diffHole.push(diffHole[actualPos]);
			featLayer_1.properties.Position = newPos;
			var index = featLayer_1.properties.Position;
			var feat = this.turfCompostHole(diffExternal[index], diffHole[index], null, 'buffer', 2*0.74*bufferValues);
			feat.properties.Position = index;
			diffExternal[index] = feat; 	
		}
		else{
			// var featLayer = turf.buffer(featLayer_1, 2*0.74*bufferValues, 'kilometres');
			var featLayer = this.overlayTurfExecute(featLayer_1, null, 'buffer', 2*0.74*bufferValues);
			feat = featLayer.features[0];  
		}
			
		var bufferName = bufferNameReport;
		if(feat !== undefined) {
			if(feat.properties === null) feat.properties = {};
			feat.properties.name = this.setNameOverlay(bufferName, bufferValues/1000, 'BUFFER');
		}	 						
			
		if(feat){
			var feature = this.calculateResult(feat);
			this.BufferLayer.addFeature(feature);
			return feature;
		}
		else return false;
	}

	this.executeTurfSismaDiff = function(_feat1, _feat2, _control, _name){
		console.log(_feat1);
		if(_feat1.properties === null) _feat1.properties = {};
		if(_feat2.properties === null) _feat2.properties = {};
		var _f1Properties = _feat1.properties.name;
		var _f2Properties = _feat2.properties.name;
		var HoleTemp = new Array(); 
		var index = _feat1.properties.Position;
		var idx = _feat2.properties.Position;
		
		if(_feat1.properties.GeometryControl || _feat2.properties.GeometryControl){
			if(_feat1.properties.GeometryControl && _feat2.properties.GeometryControl){
				 for (var key in diffHole[idx]) diffHole[index].push(diffHole[idx][key])
					_feat1 = this.turfCompostHole(diffExternal[index], diffHole[index], diffExternal[idx], 'difference', 0);
			}
			else if (_feat1.properties.GeometryControl){
				_feat1 = this.turfCompostHole(diffExternal[index], diffHole[index], _feat2, 'difference', 0);	
				console.log(_feat1);
				HoleTemp.push(_feat2);
				if (this.isDifferenceWithHole(diffExternal[index], HoleTemp, -1).bool) diffHole[index].push(_feat2);
			}
			else if (_feat2.properties.GeometryControl){
				_feat2 = this.turfCompostHole(_feat1, diffHole[idx], diffExternal[idx], 'difference', 0); index = idx;
				HoleTemp.push(_feat1);
				if (this.isDifferenceWithHole(diffExternal[index], HoleTemp, -1).bool) diffHole[index].push(_feat1);
				_feat1 = _feat2;
			}
			diffExternal[index] = _feat1;
			_control = true;
		} 
		else{ 
			console.log(_feat1);
			HoleTemp.push(_feat2);
			_feat1_Diff = this.overlayTurfExecute(_feat1, _feat2, 'erase', 0);
			_control = this.isDifferenceWithHole(_feat1, HoleTemp, -1).bool;
			if (_control) {
				diffHole.push(HoleTemp);
				diffExternal.push(_feat1);
			
			}
			_feat1 = _feat1_Diff; 
		}
		if(_feat1 !== undefined) {
			if(_feat1.properties === null) _feat1.properties = {};
			// _feat1.properties.name = this.setNameOverlay(_feat1.properties.name, _feat2.properties.name, 'DIFF');
			_feat1.properties.name = this.setNameOverlay(_f1Properties, _f2Properties, 'DIFF');
			_feat1.properties.GeometryControl = _control; 
		
			if (_control){
				_feat1.GeometryType = true;
				// _feat1.properties.name = _name+' con "hole" interni alla feature';
				// _feat1.properties.name = _feat1.properties.name+' **DIFF** '+_feat2.properties.name;
				_feat1.properties.Position = diffExternal.length - 1;
			}
		}
		else alert('Errore differenza: disegnare prima la feature esterna e poi quella interna');
		
		return _feat1;
	}	

	this.isDifferenceWithHole = function(_feat0, _feat1, position){
		var bool = false;
		var number = 0;
		var holePosition = new Array();
		var realPosition = 0;
		for (var key in _feat1){
			if (key == position) 
			{
			 // holePosition.push("-1");
			 realPosition = realPosition + 1;
			}
			// var turfInt = turf.intersect(_feat0, _feat1[key]);
			var turfInt = this.overlayTurfExecute(_feat0,_feat1[key], 'intersect', 0);
			if (turfInt != undefined){
				// var semaph = turf.erase(_feat1[key], turfInt);
				var semaph = this.overlayTurfExecute(_feat1[key], turfInt, 'erase', 0);
				if( semaph === undefined) {
					number = number +1;
				    holePosition.push(realPosition);
				}
			}
			realPosition = realPosition + 1;
		}
		if (number != 0) bool = true;
	    return {bool:bool, holePosition:holePosition};
	};
	
	this.turfCompostHole = function(_featExt, _featHole, _featNew, type, bufferV){
		switch (type){
			case 'union':
				// var turfResult = turf.union(_featExt, _featNew);
				var turfResult = this.overlayTurfExecute(_featExt, _featNew,'union',0);
			break;
			case 'intersect':
				// var turfResult = turf.intersect(_featExt, _featNew);	
				var turfResult = this.overlayTurfExecute(_featExt, _featNew,'intersect',0);	
			break;
			case 'difference':
				// var turfResult = turf.erase(_featExt, _featNew);
				var turfResult = this.overlayTurfExecute(_featExt, _featNew,'erase',0);
			break;
			case 'buffer':
				// var turfRes_Temp = turf.buffer(_featExt, bufferV, 'kilometres');
				var turfRes_Temp = this.overlayTurfExecute(_featExt, _featNew,'buffer',bufferV);
				turfResult = turfRes_Temp.features[0];
			break;
		}	
		for(var key in _featHole){
			// var turfResult = turf.erase(turfResult, _featHole[key]);
			var turfResult = this.overlayTurfExecute(turfResult, _featHole[key],'erase',0);
		}
		if(turfResult !== undefined) turfResult.properties.GeometryControl = true;	
		return turfResult;
	};
	
	this.executeTurf = function(_feat1, _feat2, type){
		var index = _feat1.properties.Position;
		var idx = _feat2.properties.Position;
		var tempName = _feat1.properties.name;
		if(_feat1.properties.GeometryControl  || _feat2.properties.GeometryControl ){
			if (_feat1.properties.GeometryControl && _feat2.properties.GeometryControl) {
				for (var key in diffHole[idx]) diffHole[index].push(diffHole[idx][key])
				_feat1 = this.turfCompostHole(diffExternal[index], diffHole[index], diffExternal[idx], type, 0);
			}
			else if (_feat1.properties.GeometryControl)  _feat1 = this.turfCompostHole(diffExternal[index], diffHole[index], _feat2, type, 0);
			else if (_feat2.properties.GeometryControl)  {_feat1 = this.turfCompostHole(diffExternal[idx], diffHole[idx], _feat1, type, 0); index = idx;}
			if(_feat1!== undefined){
				_feat1.properties.name = tempName;
				_feat1.properties.Position = index;		
				diffExternal[index] = _feat1;
			}
			else alert('Intersezione vuota');
		}
		else {
				_feat1 = this.overlayTurfExecute(_feat1, _feat2,type,0);
			/* switch (type){
				case 'union': 
					try { 
					   _feat1 = turf.union(_feat1, _feat2);
					}
					catch(err) {
						_feat1.isBad = true;
						alert('Non è possibile utilizzare poligoni con linee incrociate nell\'overlay');
						break;
					}
					
				break;
				case 'intersect': 
					try { 
					   _feat1 = turf.intersect(_feat1, _feat2);
					}
					catch(err) {
						_feat1.isBad = true;
						alert('Non è possibile utilizzare poligoni con linee incrociate nell\'overlay');
						break;
					}
					
				break;
				} */ 
				 if (_feat1 !== undefined){
				 _feat1.properties.GeometryControl = false;
					if (_feat1.properties.name === undefined) _feat1.properties.name = tempName;
				 }
				 else alert('Intersezione vuota');
		}
		return _feat1;
	};
	
	this.overlayTurfExecute = function (_feat1, _feat2, type, bufferV){
					try { 
						switch(type){
							case 'union':
								_feat1 = turf.union(_feat1, _feat2);
							break;
							case 'erase':
								_feat1 = turf.erase(_feat1, _feat2);
							break;
							case 'intersect':
								_feat1 = turf.intersect(_feat1, _feat2);
							break;
							case 'buffer':
								_feat1 = turf.buffer(_feat1, bufferV, 'kilometres');
							break;
						}
					}
					catch(err) {
						// _feat1.isBad = true;
						console.log('Non è possibile utilizzare poligoni con linee incrociate nell\'overlay');
					}
					// if (_feat1.isBad ) return null;
					// else
					return _feat1;
	};
	
	this.isOverlay = function(featLayer_1, _layer2, _control){
		var result;
//		console.log(featLayer_1);
		var featLayer_2 = this.ToMultiPoly(_layer2, 'GeoJSON');
		// Da attivare qualora nella selezione con hole non si vogliono prendere le feature che sono anche all'interno del buco
		if (_control)
		{ 
			var index = featLayer_1.Position;
		    // var resultTemp1 = this.overlayTurfExecute(diffHole[index][0], featLayer_2,'intersect',0);
		    var resultTemp0 = this.overlayTurfExecute(diffExternal[index], featLayer_2,'intersect',0);
			// if (resultTemp1 === undefined && resultTemp0 !== undefined)
			if (!this.isDifferenceWithHole(diffHole[index][0], resultTemp0, -1).bool)
			result = resultTemp0;
		}
		else
		{  
			var turfType = 'intersect';
		    // var result = turf.intersect(featLayer_1, featLayer_2);//setTimeout
		    var result = this.overlayTurfExecute(featLayer_1, featLayer_2,'intersect',0);
	    } 
		if(result) return true;
		else return false;
	}; 
			
	this.calculateResult = function(_out){
		
		var format = new ol.format.GeoJSON();
		var feature = format.readFeature(_out);
		
		return feature;
	};	
	
	this.removeOverlayFeatures = function(_features){
		
		for(var key in _features) this.OverlayLayer.removeFeature(_features[key]);
		
	};
	
	this.removeBufferFeatures = function(_features){
		
		for(var key in _features) this.BufferLayer.removeFeature(_features[key]);
		
	};

	this.Overlay2SelectLayer = function(_feature, featureDamActive, bufferActive){
		this.featureDamActive = featureDamActive;
		// this.bufferActive = bufferActive;
		var feature = _feature[0];
		if (feature === undefined) alert('Seleziona una feature nella scheda "Overlay"');
		else
		{
			if (bufferActive){
				this.BufferLayer.getFeatures().clear();
				this.BufferLayer.addFeature(feature);
				G_layer_list_scope().addBuffer(feature);
			}
			else
			{
				var fts = this.SelectedLayer.getFeatures();
					
				fts.forEach(function(element, index, array){
					this.removeFeature(element);
				}, this.SelectedLayer);
				
				this.removeSelectLayer();
			
				this.SelectedLayer.addFeature(feature);
				G_layer_list_scope().addSelection(feature);
				
				if (this.featureDamActive){
					this.selectDamsFromFeature(feature, true);
				}
			}
		}
	};
	
	this.Buffer2SelectLayer = function(_feature, featureDamActive, overlayActive){
		this.featureDamActive = featureDamActive;
		var feature = _feature[0];
		if (feature === undefined) alert('Nessuna feature presente nella scheda "Buffer"');
		else
		{
			if (overlayActive){	
				var tt = this.OverlayLayer.getFeatures().getArray();
				if($.inArray(feature, tt) === -1){
					this.OverlayLayer.addFeature(feature);
					G_layer_list_scope().addOverlay(feature);
				}
				this.removeBufferFeatures(feature);
				G_layer_list_scope().removeBuffer(feature);
			}
			else
			{
				if (feature === undefined) alert('Seleziona una feature nella scheda "Buffer"')
				else
				{
					var fts = this.SelectedLayer.getFeatures();
						
					fts.forEach(function(element, index, array){
						this.removeFeature(element);
					}, this.SelectedLayer);
					
					this.removeSelectLayer();
					this.SelectedLayer.addFeature(feature);
					G_layer_list_scope().addSelection(feature);
					
					if (this.featureDamActive) this.selectDamsFromFeature(feature, true);
				}
			}
		}
	};
	
	this.addFeature2Overlay = function(_feature){
		
		var tt = this.OverlayLayer.getFeatures().getArray();
		if($.inArray(_feature, tt) === -1){
			this.OverlayLayer.addFeature(_feature);
			G_layer_list_scope().$apply(function(){G_layer_list_scope().addOverlay(_feature);});
		}
	};
	
	this.addFeature2Buffer = function(_feature){
		
		var tt = this.BufferLayer.getFeatures().getArray();
		if($.inArray(_feature, tt) === -1){
		    this.BufferLayer.getFeatures().clear();
			this.BufferLayer.addFeature(_feature);
			G_layer_list_scope().$apply(function(){G_layer_list_scope().addBuffer(_feature);});
		}
	};
	
	this.removeFeatureOverlay = function(_feature){
		this.OverlayLayer.removeFeature(_feature);
		//if (_feature.getProperties().GeometryControl) featHole = new Array();
	};
	
	this.removeFeatureBuffer = function(_feature){
		this.BufferLayer.removeFeature(_feature);
	};
	
/*
*		ONDE DI PIENA
*/
	// this.ondeDiPiena = function(){
	/* this.CaricaLayerOndePiena = function(_layer, _ondaObj){
	 
		if (_ondaObj.layer1 ==_layer.name || _ondaObj.layer2 ==_layer.name){
			console.log(_layer);
			G_layer_list_scope().layerVisibleCheck(_layer);
			G_layer_list_scope().layerVisible(_layer);
			// G_layer_list_scope().leggiFeatures(_layer);
		}
	};
	 */
	this.ondaControl = function(_ondaObj, _l){
		if (_ondaObj.layer1 == _l.name || _ondaObj.layer2 == _l.name) {
			_l.display=true;
			G_layer_list_scope().layersOptions.activeVisible =true;
			G_layer_list_scope().showAccordions = true;
			if (_l.name==_ondaObj.layer2) _l.showFeature=true;
		}
		else _l.showFeature=false;
		return _l.showFeature;
	};
	


}
