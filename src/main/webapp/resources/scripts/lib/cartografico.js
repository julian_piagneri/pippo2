G_dam_list_scope = function(){return angular.element($("#damList")).scope();}
G_srsname_default = 'epsg:4326'; // Standard Geografico (latitudine, longitudine)
G_projection = 'EPSG:3857'; // Standard Geografico (latitudine, longitudine)
G_dam_map_icon = new ol.style.Circle({fill: null,stroke: new ol.style.Stroke({color: 'red',width: 1}),radius: 5});
G_dam_map_icon_selected = new ol.style.Circle({fill: null,stroke: new ol.style.Stroke({color: 'yellow',width: 1}),radius: 5});
G_aer_map_icon = new ol.style.Circle({fill: null,stroke: new ol.style.Stroke({color: 'black',width: 1}),radius: 5});
	
//PROVA
G_url_mapserver = 'http://10.10.0.25/cgi-bin/mapserv.exe';
G_map_mapserver = 'C:/ms4w/Map/Prova.map';
G_dam_map=null;
var damMapObj = function(){
	this.vectorLayer = function(_prm){
		// this.prm = _prm;
		this.vectorSource = new ol.source.ServerVector({
			format: new ol.format.GeoJSON(),
			loader:	function(extent, resolution, projection){
					var url = G_url_mapserver+'?MAP='+G_map_mapserver+'&service=WFS&callback=?&version=1.1.0&request=GetFeature&typename='+_prm.layer+'&outputFormat='+_prm.output_name+'&srsname='+_prm.srsname;
					$.getJSON(url,(function(_src){return function(response, state, _obj){
						_src.addFeatures(_src.readFeatures(response));
					};})(this));
					// alert("ww");
			},
			// strategy: ol.loadingstrategy.createTile(new ol.tilegrid.XYZ({maxZoom: 19})),
			strategy: ol.loadingstrategy.bbox,
			projection: G_projection
		});
		this.vector = new ol.layer.Vector({
			source: this.vectorSource,
			opacity: _prm.opacity,
			minResolution: _prm.minResolution,
			maxResolution: _prm.maxResolution,
			style: new ol.style.Style(
				{
					image: _prm.icon_for_point,
					stroke: new ol.style.Stroke({color: _prm.stroke_color,width: _prm.stroke_width}), 
					fill: new ol.style.Fill({color: _prm.fill_color})
				}
			)
		});
		this.vector.dighe = _prm.dighe;
		this.vector.selected_style = _prm.icon_for_point_selected;
		
		return this;
	};
	
	/** Creazione layer:
	/** PRM: (layer, output_name, srsname, stroke_color, stroke_width, opacity, fill_color, icon_for_point, icon_for_point_selected, dighe)
	**/
	this.digheLayer = null;
	
	this.digheLayer = new this.vectorLayer({
		layer: 'dighe.dighe_ott12',
		output_name: 'geojson',
		srsname: G_srsname_default,
		// minResolution: 0,
		// maxResolution: 0,
		stroke_color: 'green',
		stroke_width: 5,
		opacity: 1,
		fill_color: 'green',
		icon_for_point: G_dam_map_icon,
		icon_for_point_selected: G_dam_map_icon_selected,
		dighe: true
	});
	this.aerLayer = new this.vectorLayer({
		layer: 'dighe.aeroporti',
		output_name: 'geojson4',
		srsname: G_srsname_default,
		minResolution: 200,
		maxResolution: 2000,
		stroke_color: 'yellow',
		stroke_width: 5,
		opacity: 1,
		fill_color: 'yellow',
		icon_for_point: G_aer_map_icon,
		icon_for_point_selected: null,
		dighe: false
	});
	
	this.diropLayer = new this.vectorLayer({
		layer: 'dighe.dir_operazioni',
		output_name: 'geojson2',
		srsname: G_srsname_default,
		minResolution: 200,
		maxResolution: 2000,
		stroke_color: 'yellow',
		stroke_width: 1,
		opacity: 0.3,
		fill_color: 'blue',
		icon_for_point: null,
		icon_for_point_selected: null,
		dighe: false
	});
	// this.istatLayer = new this.vectorLayer('dighe.istat');
	
	var bingMap = new ol.layer.Tile({
		preload: Infinity,
		source: new ol.source.BingMaps({
		  key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
		  imagerySet: 'AerialWithLabels',
		  opacity: 0.5,
		  zoom:0.5
		})
	});
	
// create a vector layer used for editing
var vector_layer = new ol.layer.Vector({
  name: 'my_vectorlayer',
  source: new ol.source.Vector(),
  style: new ol.style.Style({
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 255, 0.2)'
    }),
    stroke: new ol.style.Stroke({
      color: '#ffcc33',
      width: 2
    }),
    image: new ol.style.Circle({
      radius: 7,
      fill: new ol.style.Fill({
        color: '#ffcc33'
      })
    })
  })
});
	
	this.view = new ol.View({
		projection: G_projection,
		// projection: 'EPSG:3857',
		//center: [810000, 4647000],
		//zoom: 0.5
		center: [1370000, 5200000],
		zoom: 5.5,
		minZoom: 3.5
	});
	
	this.map = new ol.Map({
		// layers: [bingMap, this.diropLayer.vector, this.aerLayer.vector,  this.digheLayer.vector],
		// renderer: 'webgl',
		layers: [bingMap, this.digheLayer.vector, vector_layer],
		// overlays: [overlay],
		target: 'map',
		
		 renderer: exampleNS.getRendererFromQueryString(),
		// interactions: ol.interaction.defaults().extend([dragBoxDams]),
		// interactions: ol.interaction.defaults().extend([select, modify]),
		view: this.view

	});
	
	/* var typeSelect = document.getElementById('type'); */
	
	////INTERACTION
	// this.selectDamOnMap = function(pixel) {	
		// var feature = this.map.forEachFeatureAtPixel(pixel, function(feature, layer) {
			// if(layer.dighe) return feature;
		// });		
		// if (feature) {
			// //var scope = angular.element($("#damList")).scope();
			// G_dam_list_scope().$apply(function(){G_dam_list_scope().selectDam(feature.get('ArcSub'));});
			// var fts = this.SelectedDams.getFeatures();
			
			// fts.forEach(function(element, index, array){
				// this.removeFeature(element);
			// }, this.SelectedDams);			
			// this.SelectedDams.addFeature(feature);			
		// }
	// };
	
	
	/*this.selectDamOnGrid = function(damId) {
		
		var feature = this.digheLayer.vectorSource.forEachFeature(function(feature) {
			if(feature.get('ArcSub') == this) return feature;
		}, damId);
		
		if (feature) {
			
			var fts = this.SelectedDams.getFeatures();
			
			fts.forEach(function(element, index, array){
				this.removeFeature(element);
			}, this.SelectedDams);
			
			this.SelectedDams.addFeature(feature);
			this.panTo(feature.getGeometry().getClosestPoint(this.view.getCenter()));
		}		
	};*/
	
	this.loadLayers = function(_layers){
		for(var _key in _layers){
			// alert(_layers[_key].name);
			switch(_layers[_key].type){
				case "vector":
					var newLayer = new this.vectorLayer({
						layer: _layers[_key].layer,
						output_name: _layers[_key].output_name,
						srsname: G_srsname_default,
						minResolution: _layers[_key].minResolution,
						maxResolution: _layers[_key].maxResolution,
						stroke_color: _layers[_key].stroke_color,
						stroke_width: _layers[_key].stroke_width,
						opacity: _layers[_key].opacity,
						fill_color: _layers[_key].fill_color
					});
					// alert("biz2");
					this.map.addLayer(newLayer.vector);
					break;
				case "dighe":
					// alert("ee");
					// var newLayer = new this.vectorLayer({
					this.digheLayer = new this.vectorLayer({
						layer: _layers[_key].layer,
						output_name: _layers[_key].output_name,
						srsname: G_srsname_default,
						minResolution: _layers[_key].minResolution,
						maxResolution: _layers[_key].maxResolution,
						stroke_color: _layers[_key].stroke_color,
						stroke_width: _layers[_key].stroke_width,
						opacity: _layers[_key].opacity,
						fill_color: _layers[_key].fill_color,
						icon_for_point: G_dam_map_icon,
						icon_for_point_selected: G_dam_map_icon_selected,
						dighe: true
					});
					// this.map.addLayer(newLayer.vector);
					// alert(this.digheLayer.vector);
					
					this.map.addLayer(this.digheLayer.vector);
					// alert("tt2");
					break;
				default:
					break;
			}
			
		}
		
	};
	
	// this.SelectedDams = new ol.FeatureOverlay({
		// map: this.map,
		// style: new ol.style.Style({image: G_dam_map_icon_selected})
	// });
	
	this.panTo = function(coord){
		
	  var pan = ol.animation.pan({
		duration: 2000,
		source: /** @type {ol.Coordinate} */ (this.view.getCenter())
	  });
	  this.map.beforeRender(pan);
	  this.view.setCenter(coord);
	};
	
var select_interaction,
    draw_interaction,
    modify_interaction;
	
// get the interaction type
var $interaction_type = $('[name="interaction_type"]');
// rebuild interaction when changed
$interaction_type.on('click', function(e) {
  // add new interaction
  if (this.value === 'draw') {
  alert('draw');
    addDrawInteraction();
  } else {
    addModifyInteraction();
  }
});
	
// get geometry type
var $geom_type = $('#geom_type');
// rebuild interaction when the geometry type is changed
$geom_type.on('change', function(e) {
   alert('change geo'); 
  map.removeInteraction(draw_interaction);
  addDrawInteraction();
});

// get data type to save in
$data_type = $('#data_type');
// clear map and rebuild interaction when changed
$data_type.onchange = function() {
  clearMap();
   alert('clear'); 
  map.removeInteraction(draw_interaction);
  addDrawInteraction();
};

// build up modify interaction
// needs a select and a modify interaction working together
function addModifyInteraction() {
  // remove draw interaction
  map.removeInteraction(draw_interaction);
  // create select interaction
  select_interaction = new ol.interaction.Select({
    // make sure only the desired layer can be selected
    layers: function(vector_layer) {
      return vector_layer.get('name') === 'my_vectorlayer';
    }
  });
  map.addInteraction(select_interaction);
  
  // grab the features from the select interaction to use in the modify interaction
  var selected_features = select_interaction.getFeatures();
  // when a feature is selected...
  selected_features.on('add', function(event) {
    // grab the feature
    var feature = event.element;
    // ...listen for changes and save them
    feature.on('change', saveData);
    // listen to pressing of delete key, then delete selected features
    $(document).on('keyup', function(event) {
      if (event.keyCode == 46) {
        // remove all selected features from select_interaction and my_vectorlayer
        selected_features.forEach(function(selected_feature) {
          var selected_feature_id = selected_feature.getId();
          // remove from select_interaction
          selected_features.remove(selected_feature);
          // features aus vectorlayer entfernen
          var vectorlayer_features = vector_layer.getSource().getFeatures();
          vectorlayer_features.forEach(function(source_feature) {
            var source_feature_id = source_feature.getId();
            if (source_feature_id === selected_feature_id) {
              // remove from my_vectorlayer
              vector_layer.getSource().removeFeature(source_feature);
              // save the changed data
              saveData();
            }
          });
        });
        // remove listener
        $(document).off('keyup');
      }
    });
  });
  // create the modify interaction
  modify_interaction = new ol.interaction.Modify({
    features: selected_features,
    // delete vertices by pressing the SHIFT key
    deleteCondition: function(event) {
      return ol.events.condition.shiftKeyOnly(event) &&
        ol.events.condition.singleClick(event);
    }
  });
  // add it to the map
  map.addInteraction(modify_interaction);
}


// creates a draw interaction
function addDrawInteraction() {
  // remove other interactions
  map.removeInteraction(select_interaction);
  map.removeInteraction(modify_interaction);
  
  // create the interaction
  draw_interaction = new ol.interaction.Draw({
    source: vector_layer.getSource(),
    type: /** @type {ol.geom.GeometryType} */ ($geom_type.val())
  });
	// add it to the map
  map.addInteraction(draw_interaction);
  
  // when a new feature has been drawn...
  draw_interaction.on('drawend', function(event) {
    // create a unique id
    // it is later needed to delete features
    var id = uid();
    // give the feature this id
    event.feature.setId(id);
    // save the changed data
    saveData(); 
  });
}

// add the draw interaction when the page is first shown
addDrawInteraction();
	// shows data in textarea
// replace this function by what you need
function saveData() {
  // get the format the user has chosen
  var data_type = $data_type.val(),
      // define a format the data shall be converted to
 		format = new ol.format[data_type](),
      // this will be the data in the chosen format
 		data;
  try {
    // convert the data of the vector_layer into the chosen format
    data = format.writeFeatures(vector_layer.getSource().getFeatures());
  } catch (e) {
    // at time of creation there is an error in the GPX format (18.7.2014)
    $('#data').val(e.name + ": " + e.message);
    return;
  }
  if ($data_type.val() === 'GeoJSON') {
    // format is JSON
    $('#data').val(JSON.stringify(data, null, 4));
  } else {
    // format is XML (GPX or KML)
    var serializer = new XMLSerializer();
    $('#data').val(serializer.serializeToString(data));
  }
}

// clear map when user clicks on 'Delete all features'
$("#delete").click(function() {
  clearMap();
});

// clears the map and the output of the data
function clearMap() {
  vector_layer.getSource().clear();
  if (select_interaction) {
  	select_interaction.getFeatures().clear();
  }
  $('#data').val('');
}

// creates unique id's
function uid(){
  var id = 0;
  return function() {
    if (arguments[0] === 0) {
      id = 0;
    }
    return id++;
  }
}
};
	