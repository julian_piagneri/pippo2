G_Common_Scope = {};

G_Common_Scope.provinciaREST = 'api/anagrafica/elenco/provincia';
G_Common_Scope.regioneREST = 'api/anagrafica/elenco/regione';
G_Common_Scope.tipiContattoREST = 'api/anagrafica/elenco/tipo/contatto';
G_Common_Scope.tipiAutoritaREST = 'api/anagrafica/elenco/tipo/autorita';
G_Common_Scope.tipiAltroEnteREST = 'api/anagrafica/elenco/tipo/altraente';
G_Common_Scope.digheInvasoREST = 'api/rubrica/diga/?/invaso';
G_Common_Scope.casaGuardiaREST = 'api/rubrica/?/casaguardia';
G_Common_Scope.rubricaREST = 'api/rubrica';
G_Common_Scope.concessionarioREST = 'api/anagrafica/concessionario';
G_Common_Scope.gestoreREST = 'api/anagrafica/gestore';
G_Common_Scope.ingegnereREST = 'api/anagrafica/ingegnere';
G_Common_Scope.ingegnereRespDigheREST = 'api/anagrafica/ingegnere/responsabile/?/diga';
G_Common_Scope.ingegnereSostDigheREST = 'api/anagrafica/ingegnere/sostituto/?/diga';
G_Common_Scope.autoritaREST = 'api/anagrafica/autorita';
G_Common_Scope.altraEnteREST = 'api/anagrafica/altraente';
G_Common_Scope.concessionarioAssocREST = 'api/anagrafica/assoc/concessionario/?/gestore';
G_Common_Scope.concessionarioDigheREST = 'api/anagrafica/concessionario/?/diga';
G_Common_Scope.concessionarioUtentiREST = 'api/anagrafica/concessionario/?/utente';
G_Common_Scope.gestoreAssocREST = 'api/anagrafica/assoc/gestore/?/concessionario';
G_Common_Scope.gestoreDigheREST = 'api/anagrafica/gestore/?/diga';
G_Common_Scope.gestoreUtentiREST = 'api/anagrafica/gestore/?/utente';
G_Common_Scope.autoritaDigheREST = 'api/anagrafica/autorita/?/diga';
G_Common_Scope.autoritaUtentiREST = 'api/anagrafica/autorita/?/utente';
G_Common_Scope.altraEnteDigheREST = 'api/anagrafica/altraente/?/diga';
G_Common_Scope.altraEnteUtentiREST = 'api/anagrafica/altraente/?/utente';
G_Common_Scope.digheREST = 'api/diga';
G_Common_Scope.utentiREST = 'api/anagrafica/utente';

G_Common_Scope.rivalutazioneREST = 'api/rivalutazione_idrologico_idrauliche';
G_Common_Scope.portateREST = 'api/portate';
G_Common_Scope.portateNoteREST = 'api/portate_note';
G_Common_Scope.portateFonteREST = 'api/portate_fonte';
G_Common_Scope.portatePortateFonteREST = 'api/portate_portate_fonte';
G_Common_Scope.dateREST = 'api/date_rivalutazioni_idrologiche';
G_Common_Scope.tipoOrigineREST = 'api/tipo_origine';
G_Common_Scope.fontiREST = 'api/all_fonti';

G_Common_Scope.pericolisitaREST = 'api/all_pericolisita';
G_Common_Scope.snellezza_vulnerabilitaREST = 'api/snellezza_vulnerabilita';
G_Common_Scope.classificazioneSismicaDigaREST = 'api/classificazione_sisimica_diga';

G_Common_Scope.fenomeni_franosiREST = 'api/fenomeni_franosi';
G_Common_Scope.fenomeniFranosiGruppoREST = 'api/fenomeni_franosi/gruppo';

G_Common_Scope.progetto_gestioneREST = 'api/progetto_gestione';

G_Common_Scope.testataREST = 'api/testata';

G_Common_Scope.datePickerOptions = {
	formatYear: 'yy',
	startingDay: 1
};

G_Common_Scope.removeOnEdit = function(_obj){
	// _obj.onEdit=false;
	setTimeout(function(){_obj.onEdit=false;}, 100);
};


G_Common_Scope.canWriteDettagli = function(_utenteSoggArray, _idSogg){
	// alert("ciao");
	if(G_canWriteDettagli) return true;
	var tmpArray = window[_utenteSoggArray];
	for(var key in tmpArray){
		if((tmpArray[key]+"") == (_idSogg+"")) return true;
	}
	
	return false;
	
};

G_Common_Scope.getMaxId = function(_array){
	var id_array = new Array();
	for (var key in _array){
		if(_array[key].id_new !== undefined) id_array.push(_array[key].id_new);
	}
	if(id_array.length == 0) var largest = 0;
	else var largest = Math.max.apply(Math, id_array);
	return largest;
};

G_Common_Scope.addContatto = function(_contatti){
	var new_id = this.getMaxId(_contatti) + 1;
	var new_id =  1;
	var nuovo = {
		id: new_id,
		id_new: new_id,
		contactType: null,
		contactInfo: null,
		isNew: true,
		order: 1,
		orderNum: 1
	};
	_contatti.push(nuovo);
};

G_Common_Scope.getNomeTipoAutorita = function(_idTipo){
	for(var key in this.tipiAutorita){
		if(this.tipiAutorita[key].id == _idTipo) return this.tipiAutorita[key].nome;
	}
	return "";
};


G_Common_Scope.getNomeTipoEnte = function(_idTipo){
	for(var key in this.tipiAltroEnte){
		if(this.tipiAltroEnte[key].id == _idTipo) return this.tipiAltroEnte[key].nome;
	}
	return "";
};

G_Common_Scope.saveContatto = function(_keyREST, _idSogg, _contatto, _form, _sogg){
	
	var callBackFunction = (function(_keyREST, _getREST){return function(_resp){
		
		/* 
		// $scope.removeOnEdit(_gestore);
		if(_sogg == null) return;
		if(_resp == null) return;
		if(_sogg == "") return;
		if(_resp == "") return;
		if(_sogg.lastModifiedDate && _sogg.lastModifiedUserId){
			if(_resp.lastModifiedDate && _resp.lastModifiedUserId){
				// _sogg.lastModifiedDate = _resp.lastModifiedDate;
				_sogg.lastModifiedDate = new Date(_resp.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
				_sogg.lastModifiedUserId = _resp.lastModifiedUserId;
			}
		}
		 */
		// this.getREST.restJson(_keyREST+"/"+_idSogg);
		_getREST.restJson(_keyREST+"/"+_idSogg);
	};})(_keyREST, this.getREST);
	
	
	if(_contatto.isNew) this.getREST.restPost(_keyREST+"/"+_idSogg+"/contatto", _contatto, _form, "", callBackFunction);
	else this.getREST.restPut(_keyREST+"/"+_idSogg+"/contatto/"+_contatto.id, _contatto, _form, callBackFunction);
	// if(_contatto.isNew) this.getREST.restPost(_keyREST+"/"+_idSogg+"/contatto", _contatto, _form);
	// else this.getREST.restPut(_keyREST+"/"+_idSogg+"/contatto/"+_contatto.id, _contatto, _form);
	return "";
};

G_Common_Scope.saveIndirizzo = function(_keyREST, _idSogg, _indirizzo, _form, _sogg){
	
	var callBackFunction = (function(_keyREST, _getREST){return function(_resp){
		
		_getREST.restJson(_keyREST+"/"+_idSogg);
		
	};})(_keyREST, this.getREST);
	
	if(_indirizzo.isNew) this.getREST.restPost(_keyREST+"/"+_idSogg+"/indirizzo", _indirizzo, _form, "", callBackFunction);
	else this.getREST.restPut(_keyREST+"/"+_idSogg+"/indirizzo/"+_indirizzo.id, _indirizzo, _form, callBackFunction);
	// if(_indirizzo.isNew) this.getREST.restPost(_keyREST+"/"+_idSogg+"/indirizzo", _indirizzo, _form);
	// else this.getREST.restPut(_keyREST+"/"+_idSogg+"/indirizzo/"+_indirizzo.id, _indirizzo, _form);
	return "";
};

G_Common_Scope.addIndirizzo = function(_indirizzi){
	var new_id = this.getMaxId(_indirizzi) + 1;
	var nuovo = {
		// id: new_id,
		id_new: new_id,
		indirizzo: null,
		cap: null,
		nomeComune: null,
		siglaProvincia: null,
		isNew: true,
		orderNum: 1
	};
	_indirizzi.push(nuovo);
};

G_Common_Scope.removeNewEmptyRecord = function(_ind, _array, _new, _id){
	// alert(_id);
	if(_new){
		for (var key in _array){
			if(_array[key].id == _id) _array.splice(key, 1);
		}
	}
};

G_Common_Scope.AssocTable = function(_array, _order, _$filter){
	var _obj = new this.ngTableParams({page: 1, count: 5
	}, {
		getData: function($defer, params) {
			// return;
			// alert("okok");
			
			var _data = params.filter() ? _$filter('filter')(_array, params.filter()) : _array;
			
			var _pp = params.page();
			var _pc = params.count();
			var _nn = (_pp - 1) * _pc;
			if(_nn >= _data.length && _pp > 1) params.page(_pp - 1);
			// _data = $filter('orderBy')(_data, 'nome', false);
			if(_order != '') _data = _$filter('orderBy')(_data, _order, false);
			params.total(_data.length);
			$defer.resolve(_data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
		},
		counts: [] // hide page counts control
	});
	return _obj;
};

/* 
G_Common_Scope.AnagraficaTable = function(_array, _$filter){
	var _obj = new this.ngTableParams({page: 1, count: 10, filter: {nome: ''} 
	}, {
		getData: function($defer, params) {
			var _data = params.filter() ? _$filter('filter')(_array, params.filter()) : _array;
			_data = _$filter('orderBy')(_data, 'nome', false);
			params.total(_data.length);
			$defer.resolve(_data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
		}
	});
	return _obj;
};
 */

G_Common_Scope.deleteContatto = function(_keyREST, _idSogg, _contatto, _contattoArray){
	var dlg = this.dialogs.confirm('Conferma cancellazione','Sei sicuro di voler cancellare? Il conttato verr&agrave; cancellato definitivamente.');
	dlg.result.then(
	/* 
	function(btn){
		getREST.restDelete(_keyREST+"/"+_idSogg+"/contatto/"+_contatto.id, _sogg.details.contatto, _contatto.id);
	}
	 */
	(function(_getREST){return function(btn){
		var callBackFunction = (function(_keyREST, _getREST){return function(_resp){
			_getREST.restJson(_keyREST+"/"+_idSogg);
		};})(_keyREST, _getREST);
		
		_getREST.restDelete(_keyREST+"/"+_idSogg+"/contatto/"+_contatto.id, _contattoArray, _contatto.id, callBackFunction);
	};})(this.getREST)
	,function(btn){
		
	});
};

G_Common_Scope.deleteIndirizzo = function(_keyREST, _idSogg, _indirizzo, _indirizzoArray){
	var dlg = this.dialogs.confirm('Conferma cancellazione','Sei sicuro di voler cancellare? L\'indirizzo verr&agrave; cancellato definitivamente.');
	dlg.result.then(
	/* 
	function(btn){
		_getREST.restDelete(_keyREST+"/"+_idSogg+"/indirizzo/"+_indirizzo.id, _sogg.details.indirizzo, _indirizzo.id);
	}
	 */
	(function(_getREST){return function(btn){
		
		var callBackFunction = (function(_keyREST, _getREST){return function(_resp){
			_getREST.restJson(_keyREST+"/"+_idSogg);
		};})(_keyREST, _getREST);
		
		_getREST.restDelete(_keyREST+"/"+_idSogg+"/indirizzo/"+_indirizzo.id, _indirizzoArray, _indirizzo.id, callBackFunction);
	};})(this.getREST),function(btn){
		
	});
};