schedaRivModule.controller('schedaRivController', ['$http', '$scope', '$filter', 'getREST', 'ngTableParams', '$rootScope', 'dialogs', 'ngDialog',
	function ($http, $scope, $filter, getREST, ngTableParams, $rootScope, dialogs, ngDialog) {
		$.extend($scope, G_Common_Scope);
		$scope.getREST = getREST;
		$scope.ngTableParams = ngTableParams;
		$scope.dialogs = dialogs;
		$scope.datiInvaso = {};
		
		
		$scope.portate50 = {
		  "id" : G_damID,
//		  "portataAfflusso" : 0.0,
//		  "portataLaminata" : 0.0,
//		  "quotaInvaso" : 0.0,
//		  "franco" : 0.0,
		  "tempoDiRitorno" : 50
		};
		$scope.portate100 = {
		  "id" : G_damID,
//		  "portataAfflusso" : 0.0,
//		  "portataLaminata" : 0.0,
//		  "quotaInvaso" : 0.0,
//		  "franco" : 0.0,
		  "tempoDiRitorno" : 100
		};
		$scope.portate200 = {
		  "id" : G_damID,
//		  "portataAfflusso" : 0.0,
//		  "portataLaminata" : 0.0,
//		  "quotaInvaso" : 0.0,
//		  "franco" : 0.0,
		  "tempoDiRitorno" : 200
		};
		$scope.portate500 = {
		  "id" : G_damID,
//		  "portataAfflusso" : 0.0,
//		  "portataLaminata" : 0.0,
//		  "quotaInvaso" : 0.0,
//		  "franco" : 0.0,
		  "tempoDiRitorno" : 500
		};
		$scope.portate1000 = {
		  "id" : G_damID,
//		  "portataAfflusso" : 0.0,
//		  "portataLaminata" : 0.0,
//		  "quotaInvaso" : 0.0,
//		  "franco" : 0.0,
		  "tempoDiRitorno" : 1000
		};		
		
		var httpBuilder = function(_url, _method, _contentType, _data) {
			_method = _method.toUpperCase();
			if (_contentType === undefined && _method === 'GET') {
				return {
					method: _method,
					url: (_url + "?damID=" + G_damID),
					headers: {
						'X-AUTH-TOKEN' : G_token
					}
				};
			} else if (_contentType !== undefined && _data !== undefined && _method === 'PUT') {
				return {
					method: _method,
					url: _url,
					headers: {
						'Content-Type' : _contentType,
						'X-AUTH-TOKEN' : G_token
					},
					data : _data
				};
			}
		}
		
		//TODO: tamplate di base
		$scope.saveIvaso = function(_form){
			
			var callBackFunction = (function(_getREST){return function(_resp){
				_getREST.restJson($scope.rivalutazioneREST + "?damID=" + G_damID);				
			};})(getREST);
			
			getREST.restPut($scope.rivalutazioneREST + "/" + G_damID, $scope.datiInvaso, _form, callBackFunction);
			
			return "";
		};
		
		
		/* 
		$scope.datiInvaso = {};
		$http(httpBuilder($scope.rivalutazioneREST, 'GET')).success(function(response) {
			$scope.datiInvaso = response;
		}).error(function(response) {
			;
		});
		 */
		var assegnaAfflusso = function(anni) {
			for (var i = 0; i < $scope.datiPortate.length; i++) {
				if ($scope.datiPortate[i].tempoDiRitorno == anni) {
					return $scope.datiPortate[i].portataAfflusso;
				}
			}
			
			return 0;
		}
		
		var assegnaLaminata = function(anni) {
			for (var i = 0; i < $scope.datiPortate.length; i++) {
				if ($scope.datiPortate[i].tempoDiRitorno == anni) {
					return $scope.datiPortate[i].portataLaminata;
				}
			}
			
			return 0;
		}
		
		var assegnaInvaso = function(anni) {
			for (var i = 0; i < $scope.datiPortate.length; i++) {
				if ($scope.datiPortate[i].tempoDiRitorno == anni) {
					return $scope.datiPortate[i].quotaInvaso;
				}
			}
			
			return 0;
		}
		
		var assegnaFranco = function(anni) {
			for (var i = 0; i < $scope.datiPortate.length; i++) {
				if ($scope.datiPortate[i].tempoDiRitorno == anni) {
					return $scope.datiPortate[i].franco;
				}
			}
			
			return 0;
		}
		
		$scope.afflusso50 = "";
		$scope.afflusso100 = "";
		$scope.afflusso200 = "";
		$scope.afflusso500 = "";
		$scope.afflusso1000 = "";
		$scope.laminata50 = "";
		$scope.laminata100 = "";
		$scope.laminata200 = "";
		$scope.laminata500 = "";
		$scope.laminata1000 = "";
		$scope.Francoa50 = "";
		$scope.invaso100 = "";
		$scope.invaso200 = "";
		$scope.invaso500 = "";
		$scope.invaso1000 = "";
		$scope.francoa50 = "";
		$scope.franco100 = "";
		$scope.franco200 = "";
		$scope.franco500 = "";
		$scope.franco1000 = "";
		$scope.datiPortate = {};
		/* 
		$http(httpBuilder($scope.portateREST, 'GET')).success(function(response) {
			$scope.datiPortate = response;
			$scope.afflusso50 = assegnaAfflusso(50);
			$scope.afflusso100 = assegnaAfflusso(100);
			$scope.afflusso200 = assegnaAfflusso(200);
			$scope.afflusso500 = assegnaAfflusso(500);
			$scope.afflusso1000 = assegnaAfflusso(1000);
			$scope.laminata50 = assegnaLaminata(50);
			$scope.laminata100 = assegnaLaminata(100);
			$scope.laminata200 = assegnaLaminata(200);
			$scope.laminata500 = assegnaLaminata(500);
			$scope.laminata1000 = assegnaLaminata(1000);
			$scope.invaso50 = assegnaInvaso(50);
			$scope.invaso100 = assegnaInvaso(100);
			$scope.invaso200 = assegnaInvaso(200);
			$scope.invaso500 = assegnaInvaso(500);
			$scope.invaso1000 = assegnaInvaso(1000);
			$scope.franco50 = assegnaFranco(50);
			$scope.franco100 = assegnaFranco(100);
			$scope.franco200 = assegnaFranco(200);
			$scope.franco500 = assegnaFranco(500);
			$scope.franco1000 = assegnaFranco(1000);
		}).error(function(response) {
			;
		});
		 */
		 
		$scope.savePortate = function(_form){
			
			
			
			var callBackFunction = (function(_getREST){return function(_resp){
				_getREST.restJson($scope.portateREST + "?damID=" + G_damID);				
				_getREST.restJson($scope.portateFonteREST + "?damID=" + G_damID);				
			};})(getREST);
			
			var dati = {portate: [
				$scope.portate50, 
				$scope.portate100, 
				$scope.portate200, 
				$scope.portate500, 
				$scope.portate1000 	
			],fonte: $scope.datiPortateFonte};
		
			getREST.restPut($scope.portatePortateFonteREST+ "/" + G_damID, dati, _form, callBackFunction);
			
			return "";
		};
		
		
		$scope.savePortateNote = function(_form){
			
			var callBackFunction = (function(_getREST){return function(_resp){
				_getREST.restJson($scope.portateNoteREST + "?damID=" + G_damID);
			};})(getREST);
			
			var dati = {note: $scope.portateNote};
			
			// getREST.restPut($scope.portateNoteREST+ "/" + G_damID, dati, _form, callBackFunction);
			getREST.restPut($scope.portateNoteREST+ "/" + G_damID, $scope.portateNote, _form, callBackFunction);
			
			return "";
		};
		
		$scope.datiPortateFonte = {};
		$scope.datiAllFonte = {};
		
		var getIdFonteDefault = function() {
			for (var i = 0; i < $scope.datiAllFonte.length; ++i) {
				if ($scope.datiAllFonte[i].predefinito === 1) {
					return $scope.datiAllFonte[i].idFonte;
				}
			}
		}
		
		var setRightFonte = function(idFonte) {
			if (idFonte === undefined || idFonte === null) {
				return $scope.idFonteDefault;
			}
			
			return idFonte;
		}
		 
		$scope.portateNote = {};
		
		$http(httpBuilder($scope.fontiREST, 'GET')).success(function(response) {
			$scope.datiAllFonte = response;
			$scope.idFonteDefault = getIdFonteDefault();
			$http(httpBuilder($scope.portateFonteREST, 'GET')).success(function(response) {
				$scope.datiPortateFonte = response;
				
				$scope.datiPortateFonte.fonteAfflusso = setRightFonte($scope.datiPortateFonte.fonteAfflusso);
				$scope.datiPortateFonte.fonteLaminata = setRightFonte($scope.datiPortateFonte.fonteLaminata);
				$scope.datiPortateFonte.fonteInvaso = setRightFonte($scope.datiPortateFonte.fonteInvaso);
				$scope.datiPortateFonte.fonteFranco = setRightFonte($scope.datiPortateFonte.fonteFranco);

//				$scope.selectedAfflusso = $scope.datiPortateFonte.fonteAfflusso;
//				$scope.selectedLaminata = $scope.datiPortateFonte.fonteLaminata;
//				$scope.selectedInvaso = $scope.datiPortateFonte.fonteInvaso;
//				$scope.selectedFranco = $scope.datiPortateFonte.fonteFranco;
			}).error(function(response) {
				;
			});
		}).error(function(response) {
			;
		});
		
		$scope.getFonteName = function(_idFonte){
			for(var key in $scope.datiAllFonte){
				if($scope.datiAllFonte[key].idFonte == _idFonte) return $scope.datiAllFonte[key].fonte;
			}
			return "";
		};
		
		$scope.datiDate = {};
		$http(httpBuilder($scope.dateREST, 'GET')).success(function(response) {
			$scope.datiDate = response;
			$scope.origineSelected = $scope.datiDate.idOrigineRivalutazione;
//			$scope.datiDate.dataVerificaIdrologica = new Date($scope.datiDate.dataVerificaIdrologica.substr(0,19).replace(' ', 'T')).toItDateString();
//			$scope.datiDate.dataArrivoUtd = new Date($scope.datiDate.dataArrivoUtd.substr(0,19).replace(' ', 'T')).toItDateString();
//			$scope.datiDate.dataArrivoDG = new Date($scope.datiDate.dataArrivoDG.substr(0,19).replace(' ', 'T')).toItDateString();
//			$scope.datiDate.dataParereUIDR = new Date($scope.datiDate.dataParereUIDR.substr(0,19).replace(' ', 'T')).toItDateString();
//			$scope.datiDate.dataPrescrizione = new Date($scope.datiDate.dataPrescrizione.substr(0,19).replace(' ', 'T')).toItDateString();
		}).error(function(response) {
			;
		});
		
		$scope.datiTipoOrigine = {};
		$http(httpBuilder($scope.tipoOrigineREST, 'GET')).success(function(response) {
			$scope.datiTipoOrigine = response;
		}).error(function(response) {
			;
		});
		
		//TODO: testare
		$scope.getOrigineRivalutazioneName = function(id) {
			for (var i = 0; i < $scope.datiTipoOrigine.length; i++) {
				if ($scope.datiTipoOrigine[i].id === id) {
					return $scope.datiTipoOrigine[i].nome;
				}
			}
			
			return "";
		};
		
		//TODO: testare
		$scope.saveDateRiv = function(data) {
			var callBackFunction = (function(_getREST){return function(_resp){
				_getREST.restJson($scope.dateREST + "?damID=" + G_damID);							
			};})(getREST);
			
			getREST.restPut($scope.dateREST + "/" + G_damID, $scope.datiDate, data, callBackFunction);
		};
		
		$scope.portataMaxShow = false;
		$scope.buttonPortataCSS = "btn btn-info glyphicon glyphicon-edit pull-right";
		$scope.switchPortataMaxShow = function() {
			$scope.portataMaxShow = !$scope.portataMaxShow;
			if (!$scope.portataMaxShow) {
				$scope.buttonPortataCSS = "btn btn-info glyphicon glyphicon-edit pull-right";
			} else {
				$scope.buttonPortataCSS = "btn btn-default glyphicon glyphicon-ok pull-right";
			}
		}
		
		$scope.laminataMaxShow = false;
		$scope.buttonLaminataCSS = "btn btn-info glyphicon glyphicon-edit pull-right";
		$scope.switchLaminataMaxShow = function() {
			$scope.laminataMaxShow = !$scope.laminataMaxShow;
			if (!$scope.laminataMaxShow) {
				$scope.buttonLaminataCSS = "btn btn-info glyphicon glyphicon-edit pull-right";
			} else {
				$scope.buttonLaminataCSS = "btn btn-default glyphicon glyphicon-ok pull-right";
			}
		}
		
		$scope.invasoMaxShow = false;
		$scope.buttonInvasoCSS = "btn btn-info glyphicon glyphicon-edit pull-right";
		$scope.switchInvasoMaxShow = function() {
			$scope.invasoMaxShow = !$scope.invasoMaxShow;
			if (!$scope.invasoMaxShow) {
				$scope.buttonInvasoCSS = "btn btn-info glyphicon glyphicon-edit pull-right";
			} else {
				$scope.buttonInvasoCSS = "btn btn-default glyphicon glyphicon-ok pull-right";
			}
		}
		
		var oldValuesFranco = {
			'50' : $scope.franco50,
			'100' : $scope.franco100,
			'200' : $scope.franco200,
			'500' : $scope.franco500,
			'1000' : $scope.franco1000
		};
		$scope.francoMaxShow = false;
		$scope.buttonFrancoCSS = "btn btn-info glyphicon glyphicon-edit pull-right";
		$scope.switchFrancoMaxShow = function() {
			$scope.francoMaxShow = !$scope.francoMaxShow;
			if (!$scope.francoMaxShow) {
				$scope.buttonFrancoCSS = "btn btn-info glyphicon glyphicon-edit pull-right";
				console.info("Ciao mondo");
			} else {
				$scope.buttonFrancoCSS = "btn btn-default glyphicon glyphicon-ok pull-right";
			}
		}
		
		
		
		
		$scope.$on('RESTreceived', function() {
			
			var path = $scope.portateFonteREST + "?damID=" + G_damID;
			if(getREST.results[path]){
				if(getREST.results[path].newDataZZ){
					$scope.datiPortateFonte = getREST.results[path];
					getREST.results[path].newDataZZ = false;
				}
			}
			
			var path = $scope.rivalutazioneREST + "?damID=" + G_damID;
			if(getREST.results[path]){
				if(getREST.results[path].newDataZZ){
					
					$scope.datiInvaso = getREST.results[path];
					getREST.results[path].newDataZZ = false;
				}
			}
			
			var path = $scope.portateNoteREST + "?damID=" + G_damID;
			if(getREST.results[path]){
				if(getREST.results[path].newDataZZ){
					
					$scope.portateNote = getREST.results[path];
					getREST.results[path].newDataZZ = false;
				}
			}
			
			var path = $scope.portateREST + "?damID=" + G_damID;
			if(getREST.results[path]){
				if(getREST.results[path].newDataZZ){
					
					$scope.portate = getREST.results[path];
					
					for(var key in $scope.portate){
						switch($scope.portate[key].tempoDiRitorno){
							case 50:
								$scope.portate50 = $scope.portate[key];
								break;
							case 100:
								$scope.portate100 = $scope.portate[key];
								break;
							case 200:
								$scope.portate200 = $scope.portate[key];
								break;
							case 500:
								$scope.portate500 = $scope.portate[key];
								break;
							case 1000:
								$scope.portate1000 = $scope.portate[key];
								break;
						}
					}
					
					getREST.results[path].newDataZZ = false;
				}
			}
			
			
		});
		
		getREST.restJson($scope.rivalutazioneREST + "?damID=" + G_damID);
		getREST.restJson($scope.portateREST + "?damID=" + G_damID);
		getREST.restJson($scope.portateNoteREST + "?damID=" + G_damID);
		
		$scope.dati = {};	
		var par = {
			method: 'GET',
			url: ($scope.testataREST + "?damID=" + G_damID),
			headers: {
				'X-AUTH-TOKEN' : G_token
			}
		}
		
		$http(par).success(function(response) {
			$scope.dati = response;
		}).error(function(response) {
			
		});
	}
]);