TestataModule.controller('TestataController', ['$http', '$scope', '$filter', 'ngTableParams', '$rootScope', 'dialogs', 'ngDialog',
	function ($http, $scope, $filter, ngTableParams, $rootScope, dialogs, ngDialog) {
		$.extend($scope, G_Common_Scope);
		$scope.ngTableParams = ngTableParams;
		$scope.dialogs = dialogs;

		$scope.dati = {};	
		var par = {
				method: 'GET',
				url: ($scope.testataREST + "?damID=" + G_damID),
				headers: {
					'X-AUTH-TOKEN' : G_token
				}
		}
		
		$http(par).success(function(response) {
			$scope.dati = response;
		}).error(function(response) {
			
		});
	}]
);