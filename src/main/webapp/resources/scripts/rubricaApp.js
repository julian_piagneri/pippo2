﻿//tutte le occorrenze di $dialogs sono state rinominate in dialog per funzionare con dialog 5

// G_count = 0;

rubricaModule.controller('rubricaController', ['$scope', '$filter', 'getREST', 'ngTableParams', '$rootScope', 'dialogs', 'ngDialog', function ($scope, $filter, getREST, ngTableParams, $rootScope, dialogs, ngDialog) {
	
	$.extend($scope, G_Common_Scope);
	$scope.getREST = getREST;
	$scope.ngTableParams = ngTableParams;
	$scope.dialogs = dialogs;
	
	$scope.concessionari = new Array();
	$scope.gestori = new Array();
	$scope.ingegneriResp = new Array();
	$scope.ingegneriSost = new Array();
	$scope.autorita = new Array();
	$scope.altriEnti = new Array();
	
	$scope.nuovoIngegnereResp = {};
	$scope.nuovoIngegnereResp.$visible = false;
	$scope.nuovoIngegnereResp.open = false;
	
	$scope.nuovoIngegnereSost = {};
	$scope.nuovoIngegnereSost.$visible = false;
	$scope.nuovoIngegnereSost.open = false;
	
	$scope.ingAssocKeyREST = "";
	
	
	$scope.addIngTitolo = "Elenco Ingegneri";
	
	$scope.ingRespParams = {
		showhistory: false
	};
	$scope.ingSostParams = {
		showhistory: false
	};
	
	// $scope.reloadTableIngegneriResp = false;
	
	$scope.toDateFormat = function(_date){
		// if(typeof _date !== 'Date'){
		if(_date instanceof Date) {
			return _date;
		} else {
			// var st = "26.04.2013";
			var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
			var dt = new Date(_date.replace(pattern,'$3-$2-$1'));
			return dt;
		}
		// else return _date;
	};
	
	$scope.datiRubrica = {};
	
	$scope.show_hide_Concess_details = function(_concess){
		if(_concess.isC_details) {
			getREST.restJson($scope.concessionarioREST+"/"+_concess.id);
		} else {
			_concess.isC_details = true;
		}
	};
	
	$scope.show_hide_Gestore_details = function(_gestore){
		if(_gestore.isC_details) {
			getREST.restJson($scope.gestoreREST+"/"+_gestore.id);
		} else {
			_gestore.isC_details = true;
		}
	};
	
	$scope.show_hide_Ing_details = function(_ingResp){
		if(_ingResp.onEdit) {
			return;
		}
		_ingResp.isC_details = !_ingResp.isC_details;
	};
	
	$scope.show_hide_Autorita_details = function(_autorita){
		if(_autorita.onEdit) {
			return;
		}
		if(_autorita.isC_details) {
			getREST.restJson($scope.autoritaREST+"/"+_autorita.id);
			// getREST.restJson($scope.autoritaDigheREST.replace("?", _autorita.id));
			// getREST.restJson($scope.autoritaUtentiREST.replace("?", _autorita.id));
		}
		else {
			_autorita.isC_details = true;
		}
	};
	
	$scope.show_hide_Altro_Ente_details = function(_ente){
		if(_ente.onEdit) {
			return;
		}
		if(_ente.isC_details){
			getREST.restJson($scope.altraEnteREST+"/"+_ente.id);
			// getREST.restJson($scope.altraEnteDigheREST.replace("?", _ente.id));
			// getREST.restJson($scope.altraEnteUtentiREST.replace("?", _ente.id));
		}
		else {
			_ente.isC_details = true;
		}
	};
	
	$scope.saveConcess = function(_concess, _form){
		
		var callBackFunction = (function(_concess) {
			return function(_resp){
				$scope.removeOnEdit(_concess);
				getREST.restJson($scope.concessionarioREST+"/"+_concess.id);
			};
		})(_concess);
		
		var dati_cl = $.extend(true, {}, _concess);
		delete dati_cl.details;
		delete dati_cl.contatto;
		delete dati_cl.indirizzo;
		// $scope.removeOnEdit(_concess);
		getREST.restPut($scope.concessionarioREST+"/"+_concess.id, dati_cl, _form, callBackFunction);
		// $scope.reloadTableConcess = true;
		return "";
	}
	
	$scope.saveGestore = function(_gestore, _form){
		
		var callBackFunction = (function(_gestore){return function(_resp){
			$scope.removeOnEdit(_gestore);
			getREST.restJson($scope.gestoreREST+"/"+_gestore.id);
		};})(_gestore);
		
		var dati_cl = $.extend(true, {}, _gestore);
		delete dati_cl.details;
		delete dati_cl.contatto;
		delete dati_cl.indirizzo;
		// $scope.removeOnEdit(_gestore);
		getREST.restPut($scope.gestoreREST+"/"+_gestore.id, dati_cl, _form, callBackFunction);
		// $scope.reloadTableGestori = true;
		return "";
	};
	
	$scope.saveAutorita = function(_autorita, _form){
		
		
		var callBackFunction = (function(_autorita){return function(_resp){
			$scope.removeOnEdit(_autorita);
			getREST.restJson($scope.autoritaREST+"/"+_autorita.id);
		};})(_autorita);
		
		
		var dati_cl = $.extend(true, {}, _autorita);
		delete dati_cl.details;
		delete dati_cl.contatto;
		delete dati_cl.indirizzo;
		// $scope.removeOnEdit(_autorita);
		getREST.restPut($scope.autoritaREST+"/"+_autorita.id, dati_cl, _form, callBackFunction);
		// $scope.reloadTableAutorita = true;
		return "";
	};
	
	$scope.saveAltroEnte = function(_altroEnte, _form){
		
		var callBackFunction = (function(_altroEnte){return function(_resp){
			$scope.removeOnEdit(_altroEnte);
			getREST.restJson($scope.altraEnteREST+"/"+_altroEnte.id);
		};})(_altroEnte);
		
		var dati_cl = $.extend(true, {}, _altroEnte);
		delete dati_cl.details;
		delete dati_cl.contatto;
		delete dati_cl.indirizzo;
		// $scope.removeOnEdit(_altroEnte);
		getREST.restPut($scope.altraEnteREST+"/"+_altroEnte.id, dati_cl, _form, callBackFunction);
		// $scope.reloadTableAltraEnte = true;
		return "";
	};
	
	$scope.select_ingegnere = function(_ing){
		for(var key in $scope.ingegneri){
			if($scope.ingegneri[key].id != _ing.id){
				$scope.ingegneri[key].isC_details = true;
				$scope.ingegneri[key].dataNomina = null;
			}
		}
		_ing.isC_details = !_ing.isC_details;
	};
	
	/* 
	$scope.deleteContatto = function(_keyREST, _idSogg, _contatto, _sogg){
		var as = dialogs.confirm('Conferma cancellazione','Is this awesome or what?');
		// getREST.restDelete(_keyREST+"/"+_idSogg+"/contatto/"+_contatto.id, _sogg.details.contatto, _contatto.id);
	};
	 */
	$scope.saveIngegnere = function(_ing, _form, _ingDigaKeyREST){
		
		var callBackFunction = (function(_ing){return function(_resp){
			_newDate = new Date();
			_ing.denominazione = _ing.nome+' '+_ing.cognome;
			if(_ing.newDataNomina instanceof Date) _ing.dataNomina = _ing.newDataNomina.toItDateString();
			else _ing.dataNomina = _ing.newDataNomina;
			// _ing.dataNominaOrder = $scope.toDateOrder($scope.toDateFormat(_ing.newDataNomina));
			if(_ing.newDataNomina instanceof Date) _ing.dataNominaOrder = $scope.toDateOrder(_ing.newDataNomina.toItDateString());
			else _ing.dataNominaOrder = $scope.toDateOrder(_ing.newDataNomina);
			$scope.removeOnEdit(_ing);
			
			getREST.restJson($scope.ingegnereREST+"/"+_ing.id);
			
			// _ing.lastModifiedDate = new Date(_resp.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();			
			
			// alert("ok");
			// var a = 0;
		};})(_ing);
		
		var dati_cl = $.extend(true, {}, _ing);
		
		// var _dt = $scope.toDateFormat(_ing.newDataNomina);
		if(_ing.newDataNomina instanceof Date) dati_cl.dataNomina = _ing.newDataNomina.toItDateString();
		else dati_cl.dataNomina = _ing.newDataNomina;
		if (dati_cl.dataNomina == null) dati_cl.dataNomina = "";
		delete dati_cl.details;
		delete dati_cl.contatto;
		delete dati_cl.indirizzo;
		if(_ing.newDataNomina instanceof Date) _ing.dataNomina = _ing.newDataNomina.toItDateString();
		else _ing.dataNomina = _ing.newDataNomina;
		// getREST.restPut($scope.ingegnereREST+"/"+_ing.id, dati_cl, _form, callBackFunction);
		getREST.restPut($scope.ingegnereREST+"/"+_ing.id, dati_cl);
		getREST.restPut(_ingDigaKeyREST.replace("?", _ing.id) + "/" + G_damID, dati_cl, _form, callBackFunction);
		return "";
	};
	
	
	$scope.saveCasaGuardia = function(_form){
		
		var callBackFunction = function(_resp){
			
			$scope.datiRubrica.casaDiGuarda.lastModifiedUserId = _resp.lastModifiedUserId;
			$scope.datiRubrica.casaDiGuarda.lastModifiedDate = new Date(_resp.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
			
		};
		
		// $scope.datiRubrica.casaDiGuarda.id = $scope.datiRubrica.numeroArchivio + $scope.datiRubrica.sub;
		$scope.datiRubrica.casaDiGuarda.numeroArchivio = $scope.datiRubrica.numeroArchivio;
		$scope.datiRubrica.casaDiGuarda.sub = $scope.datiRubrica.sub;
		getREST.restPut($scope.casaGuardiaREST.replace("?", G_damID), $scope.datiRubrica.casaDiGuarda, _form, callBackFunction);
		
		return "";
		
	};
	
	$scope.saveNuovoIngegnereResp = function(_form){
		var callBackFunction = function(){
			var path = $scope.ingegnereREST+'/new/'+$scope.nuovoIngegnereResp.id_new;
			if(getREST.results[path]){
				if(getREST.results[path].newDataZZ){
					// getREST.results[path].newDataZZ = false;
					var nominationDate=$scope.nuovoIngegnereResp.dataNomina;
					if(nominationDate!=null && nominationDate!=""){
						nominationDate=nominationDate.toItDateString()
					}
					getREST.restPost($scope.ingegnereRespDigheREST.replace("?", getREST.results[path].id) + "/" + G_damID, {dataNomina: nominationDate, id_new: 0});
					
				}
			}
			
		};
		
		$scope.nuovoIngegnereResp.orderNum = 1;
		$scope.nuovoIngegnereResp.id_new = 1;
		// $scope.reloadTableIngegneriResp = true;
		getREST.restPost($scope.ingegnereREST, $scope.nuovoIngegnereResp, _form, '', callBackFunction);
		return "";
		
		
	};
	
	$scope.deleteIngegnereDiga = function(_ing, resp){
		var as = dialogs.confirm('Conferma','Vuoi rimuovere questo Ingegnere dalla lista ingegneri per questa diga?');
		as.result.then(function(btn){
			var deletePath="";
			var ingArray=new Array();
			if(resp){
				deletePath=$scope.ingegnereRespDigheREST.replace("?", _ing.id) + "/" + G_damID;
				ingArray=$scope.ingegneriResp;
			} else {
				deletePath=$scope.ingegnereSostDigheREST.replace("?", _ing.id) + "/" + G_damID;
				ingArray=$scope.ingegneriSost;
			}
			getREST.restDelete(deletePath, ingArray, _ing.id);
        },function(btn){});
		// getREST.restDelete(_keyREST+"/"+_idSogg+"/contatto/"+_contatto.id, _sogg.details.contatto, _contatto.id);
		return "";
	};
	
	$scope.saveNuovoIngegnereSost = function(_form){
		var callBackFunction = function(){
			var path = $scope.ingegnereREST+'/new/'+$scope.nuovoIngegnereSost.id_new;
			if(getREST.results[path]){
				if(getREST.results[path].newDataZZ){
					// getREST.results[path].newDataZZ = false;
					var nominationDate=$scope.nuovoIngegnereSost.dataNomina;
					if(nominationDate!=null && nominationDate!=""){
						nominationDate=nominationDate.toItDateString()
					}
					getREST.restPost($scope.ingegnereSostDigheREST.replace("?", getREST.results[path].id) + "/" + G_damID, {dataNomina: nominationDate, id_new: 0});
					
				}
			}
			
		};
		
		$scope.nuovoIngegnereSost.orderNum = 1;
		$scope.nuovoIngegnereSost.id_new = 2;
		// $scope.reloadTableIngegneriResp = true;
		getREST.restPost($scope.ingegnereREST, $scope.nuovoIngegnereSost, _form, '', callBackFunction);
		return "";
		
		
	};
	
	$scope.addIngegnere = function(_form, _ing){
		_ing.nome = null;
		_ing.cognome = null;
		_ing.dataNomina = null;
		_ing.$visible = true;
		
		_form.$show();
		
	};
	
	$scope.assocIng = function(_id, _dataNomina, _assocKeyREST){
		
		var callBackFunction = function(){
			ngDialog.close();
			// getREST.restJson($scope.ingegnereREST + "/" + _id);
		};
		
		var nominationDate = _dataNomina
		if(nominationDate!=null && nominationDate!=""){
			nominationDate=nominationDate.toItDateString();
		}
		
		getREST.restPost(_assocKeyREST.replace("?", _id) + "/" + G_damID, {dataNomina: nominationDate, id_new: 0}, null, '', callBackFunction);
		
		// path, dati, form, refreshKeyREST, fct
		
	};
	
	$scope.caricaIngegnere = function(_assocKeyREST){
		for(var key in $scope.ingegneri){
			$scope.ingegneri[key].isC_details = true;
			$scope.ingegneri[key].dataNomina = null;
		}
		$scope.ingAssocKeyREST = _assocKeyREST;
		ngDialog.open(
				{ className: 'ngdialog-theme-plain ngdialog-assoc-table',
					scope: $scope, 
					template: 'addIngTemplate'
				}
		);
		
	};
	
	$scope.$on('TypeLoadCompleted', function() {
		//alert($scope.rubricaREST+"/"+G_damID);
		getREST.restJson($scope.rubricaREST+"/"+G_damID);
		getREST.restJson($scope.ingegnereREST);
		getREST.restJson($scope.digheInvasoREST.replace("?", G_damID));
		// getREST.restJson($scope.concessionarioREST);
		// getREST.restJson($scope.gestoreREST);
	});
	
	$scope.toDateOrder = function(_data){
		if(_data==null || _data==""){
			return "";
		}
		var _ret = _data.substring(6, 10)+_data.substring(3, 5)+_data.substring(0, 2);
		return _ret;
	};
	
	$scope.nomeTipoContatto = function(_idTipo){
		for(var key in $scope.tipiContatto){
			if($scope.tipiContatto[key].id == _idTipo) return $scope.tipiContatto[key].nome;
		}
		return '';
	};
	
	$scope.$on('RESTreceived', function() {
		for(var key in $scope.datiRubrica.idConcess){
		
			var concessPath = $scope.concessionarioREST+"/"+$scope.datiRubrica.idConcess[key];
			if(getREST.results[concessPath]){
				if(getREST.results[concessPath].newDataZZ){
					var _obj = getREST.results[concessPath];
					_obj.lastModifiedDate = new Date(_obj.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
					
					var _isNew = true;
					for(var key2 in $scope.concessionari){
						if($scope.concessionari[key2].id == _obj.id){
							_obj.isC_details = $scope.concessionari[key2].isC_details;
							$scope.concessionari[key2] = _obj;
							_isNew = false;
						}
					}
					
					if(_isNew){
						_obj.isC_details = true;
						$scope.concessionari.push(_obj);
					}
					getREST.results[concessPath].newDataZZ = false;
				}
			}
			
		}
		for(var key in $scope.datiRubrica.idGestore){
			var gestorePath = $scope.gestoreREST+"/"+$scope.datiRubrica.idGestore[key];
			if(getREST.results[gestorePath]){
				if(getREST.results[gestorePath].newDataZZ){
					// $scope.gestori[key].details = getREST.results[gestorePath];
					// $scope.gestori[key].isC_details = false;
					
					
					var _obj = getREST.results[gestorePath];
					_obj.lastModifiedDate = new Date(_obj.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
					
					
					var _isNew = true;
					for(var key2 in $scope.gestori){
						if($scope.gestori[key2].id == _obj.id){
							_obj.isC_details = $scope.gestori[key2].isC_details;
							$scope.gestori[key2] = _obj;
							_isNew = false;
						}
					}
					
					if(_isNew){
						_obj.isC_details = true;
						$scope.gestori.push(_obj);
					}
					
					getREST.results[gestorePath].newDataZZ = false;
				}
			}
		}
		for(var key in $scope.datiRubrica.idIngResp){
			// if(!$scope.datiRubrica.idIngResp[key].empty) continue;
			var ingegnerePath = $scope.ingegnereREST+"/"+$scope.datiRubrica.idIngResp[key].id;
			if(getREST.results[ingegnerePath]){
				if(getREST.results[ingegnerePath].newDataZZ){
					
					// var _obj = getREST.results[ingegnerePath];
					var _obj = $.extend(true, {}, getREST.results[ingegnerePath]);
					// _obj.isC_details = true;
					// if($scope.datiRubrica.idIngResp[key].empty) _obj.isC_details = true;
					_obj.lastModifiedDate = new Date(_obj.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
					
					_obj.denominazione = _obj.cognome + ' ' + _obj.nome;
					
					
					var _isNew = true;
					for(var key2 in $scope.ingegneriResp){
						if($scope.ingegneriResp[key2].id == _obj.id){
							_obj.isC_details = $scope.ingegneriResp[key2].isC_details;
							_obj.dataNomina = $scope.ingegneriResp[key2].dataNomina;
							_obj.dataNominaOrder = $scope.toDateOrder($scope.ingegneriResp[key2].dataNomina);
							_obj.progressivo = $scope.ingegneriResp[key2].progressivo;
							$scope.ingegneriResp[key2] = _obj;
							_isNew = false;
						}
					}
					
					if(_isNew){
						_obj.isC_details = true;
						_obj.dataNomina = $scope.datiRubrica.idIngResp[key].dataNomina;
						_obj.dataNominaOrder = $scope.toDateOrder($scope.datiRubrica.idIngResp[key].dataNomina);
						_obj.progressivo = $scope.datiRubrica.idIngResp[key].progressivo;
						$scope.ingegneriResp.push(_obj);
					}
					
					// $scope.ingegneriResp.push(_obj);
					$scope.datiRubrica.idIngResp[key].empty = false;
					// getREST.results[ingegnerePath].newDataZZ = false;
					getREST.results[ingegnerePath].newData_Done = true;
				}
			}
		}
		// var qq = 1;
		for(var key in $scope.datiRubrica.idIngSost){
			// if(!$scope.datiRubrica.idIngSost[key].empty) continue;
			var ingegnerePath = $scope.ingegnereREST+"/"+$scope.datiRubrica.idIngSost[key].id;
			if(getREST.results[ingegnerePath]){
				if(getREST.results[ingegnerePath].newDataZZ){
					
					// var _obj = getREST.results[ingegnerePath];
					var _obj = $.extend(true, {}, getREST.results[ingegnerePath]);
					// _obj.isC_details = true;
					// if($scope.datiRubrica.idIngSost[key].empty) _obj.isC_details = true;
					_obj.lastModifiedDate = new Date(_obj.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
					
					
					_obj.denominazione = _obj.cognome + ' ' + _obj.nome;
					
					
					var _isNew = true;
					for(var key2 in $scope.ingegneriSost){
						if($scope.ingegneriSost[key2].id == _obj.id){
							_obj.isC_details = $scope.ingegneriSost[key2].isC_details;
							_obj.dataNomina = $scope.ingegneriSost[key2].dataNomina;
							_obj.dataNominaOrder = $scope.toDateOrder($scope.ingegneriSost[key2].dataNomina);
							_obj.progressivo = $scope.ingegneriSost[key2].progressivo;
							$scope.ingegneriSost[key2] = _obj;
							_isNew = false;
						}
					}
					
					if(_isNew){
						_obj.isC_details = true;
						_obj.dataNomina = $scope.datiRubrica.idIngSost[key].dataNomina;
						_obj.dataNominaOrder = $scope.toDateOrder($scope.datiRubrica.idIngSost[key].dataNomina);
						_obj.progressivo = $scope.datiRubrica.idIngSost[key].progressivo;
						$scope.ingegneriSost.push(_obj);
					}
					
					// $scope.ingegneriSost.push(_obj);
					$scope.datiRubrica.idIngSost[key].empty = false;
					// if(){
						// G_count ++;
					// }
					getREST.results[ingegnerePath].newDataZZ = false;
				}
			}
		}
		
		for(var key in getREST.results){
			if(getREST.results[key].newData_Done){
				getREST.results[key].newDataZZ = false;
				getREST.results[key].newData_Done = false;
			}
		}
		
		for(var key in $scope.datiRubrica.idAutorita){
			var autoritaPath = $scope.autoritaREST+"/"+$scope.datiRubrica.idAutorita[key];
			if(getREST.results[autoritaPath]){
				if(getREST.results[autoritaPath].newDataZZ){
					
					var _obj = getREST.results[autoritaPath];
					// _obj.isC_details = true;
					_obj.lastModifiedDate = new Date(_obj.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
					// _obj.denominazione = _obj.cognome + ' ' + _obj.nome;
					
					var _isNew = true;
					for(var key2 in $scope.autorita){
						if($scope.autorita[key2].id == _obj.id){
							_obj.isC_details = $scope.autorita[key2].isC_details;
							$scope.autorita[key2] = _obj;
							_isNew = false;
						}
					}
					
					if(_isNew){
						_obj.isC_details = true;
						$scope.autorita.push(_obj);
					}
					
					// $scope.autorita.push(_obj);
					
					getREST.results[autoritaPath].newDataZZ = false;
				}
			}
		}
		for(var key in $scope.datiRubrica.idAltraEnte){
			var altraEntePath = $scope.altraEnteREST+"/"+$scope.datiRubrica.idAltraEnte[key];
			if(getREST.results[altraEntePath]){
				if(getREST.results[altraEntePath].newDataZZ){
					
					var _obj = getREST.results[altraEntePath];
					_obj.lastModifiedDate = new Date(_obj.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
					// _obj.isC_details = true;
					// _obj.denominazione = _obj.cognome + ' ' + _obj.nome;
					
					var _isNew = true;
					for(var key2 in $scope.altriEnti){
						if($scope.altriEnti[key2].id == _obj.id){
							_obj.isC_details = $scope.altriEnti[key2].isC_details;
							$scope.altriEnti[key2] = _obj;
							_isNew = false;
						}
					}
					
					if(_isNew){
						_obj.isC_details = true;
						$scope.altriEnti.push(_obj);
					}
					
					// $scope.altriEnti.push(_obj);
					
					getREST.results[altraEntePath].newDataZZ = false;
				}
			}
		}
		
		for(var key in $scope.ingegneri){
			var ingPath = $scope.ingegnereRespDigheREST.replace("?", $scope.ingegneri[key].id) + "/" + G_damID + "/new/0";
			if(getREST.results[ingPath]){
				if(getREST.results[ingPath].newDataZZ){
					
					/* 
					// var _obj = getREST.results[ingPath];
					var _obj = $scope.ingegneri[key];
					_obj.isC_details = true;
					_obj.lastModifiedDate = new Date(_obj.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
					_obj.denominazione = _obj.cognome + ' ' + _obj.nome;
					_obj.dataNomina = getREST.results[ingPath].dataNomina;
					_obj.dataNominaOrder = $scope.toDateOrder(getREST.results[ingPath].dataNomina);
					$scope.ingegneriResp.push(_obj);
					 */
					
					var _obj = {id:$scope.ingegneri[key].id, empty: true, dataNomina: getREST.results[ingPath].dataNomina};
					$scope.datiRubrica.idIngResp.push(_obj);
					
					getREST.restJson($scope.ingegnereREST + "/" + $scope.ingegneri[key].id);
					
					getREST.results[ingPath].newDataZZ = false;
				}
			}
			
			var ingPath = $scope.ingegnereSostDigheREST.replace("?", $scope.ingegneri[key].id) + "/" + G_damID + "/new/0";
			if(getREST.results[ingPath]){
				if(getREST.results[ingPath].newDataZZ){
					
					/* 
					// var _obj = getREST.results[ingPath];
					var _obj = $scope.ingegneri[key];
					_obj.isC_details = true;
					_obj.denominazione = _obj.cognome + ' ' + _obj.nome;
					_obj.dataNomina = getREST.results[ingPath].dataNomina;
					_obj.dataNominaOrder = $scope.toDateOrder(getREST.results[ingPath].dataNomina);
					$scope.ingegneriSost.push(_obj);
					 */
					
					var _obj = {id:$scope.ingegneri[key].id, empty: true, dataNomina: getREST.results[ingPath].dataNomina};
					$scope.datiRubrica.idIngSost.push(_obj);
					
					getREST.restJson($scope.ingegnereREST + "/" + $scope.ingegneri[key].id);
					
					
					getREST.results[ingPath].newDataZZ = false;
				}
			}
		}
		
		if(getREST.results[$scope.concessionarioREST]){
			if(getREST.results[$scope.concessionarioREST].newDataZZ){
				$scope.concessionari = getREST.results[$scope.concessionarioREST];
				
				for(var key in $scope.concessionari) $scope.concessionari[key].isC_details = true;
				// $scope.tableConcess = new $scope.AnagraficaTable($scope.concessionari, $filter);
				
				getREST.results[$scope.concessionarioREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.gestoreREST]){
			if(getREST.results[$scope.gestoreREST].newDataZZ){
				$scope.gestori = getREST.results[$scope.gestoreREST];
				for(var key in $scope.gestori) $scope.gestori[key].isC_details = true;
				// $scope.tableGestori = new $scope.AnagraficaTable($scope.gestori, $filter);
				
				getREST.results[$scope.gestoreREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.provinciaREST]){
			if(getREST.results[$scope.provinciaREST].newDataZZ){
				$scope.province = getREST.results[$scope.provinciaREST];
				
				getREST.results[$scope.provinciaREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.tipiContattoREST]){
			if(getREST.results[$scope.tipiContattoREST].newDataZZ){
				$scope.tipiContatto = getREST.results[$scope.tipiContattoREST];
				for(var key in $scope.tipiContatto) $scope.tipiContatto[key].id = parseInt($scope.tipiContatto[key].id, 10);
				getREST.results[$scope.tipiContattoREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.tipiAutoritaREST]){
			if(getREST.results[$scope.tipiAutoritaREST].newDataZZ){
				$scope.tipiAutorita = getREST.results[$scope.tipiAutoritaREST];
				
				for(var key in $scope.tipiAutorita){
					if($scope.tipiAutorita[key].nome) $scope.tipiAutorita[key].nome = $scope.tipiAutorita[key].nome.toLowerCase();
				}
				
				getREST.results[$scope.tipiAutoritaREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.tipiAltroEnteREST]){
			if(getREST.results[$scope.tipiAltroEnteREST].newDataZZ){
				$scope.tipiAltroEnte = getREST.results[$scope.tipiAltroEnteREST];
				
				for(var key in $scope.tipiAltroEnte){
					if($scope.tipiAltroEnte[key].nome) $scope.tipiAltroEnte[key].nome = $scope.tipiAltroEnte[key].nome.toLowerCase();
					$scope.tipiAltroEnte[key].id = parseInt($scope.tipiAltroEnte[key].id, 10);
				}
				
				getREST.results[$scope.tipiAltroEnteREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.ingegnereREST]){
			if(getREST.results[$scope.ingegnereREST].newDataZZ){
				$scope.ingegneri = getREST.results[$scope.ingegnereREST];
				for(var key in $scope.ingegneri){
					$scope.ingegneri[key].isC_details = true;
					$scope.ingegneri[key].nomeCognome = $scope.ingegneri[key].nome+' '+$scope.ingegneri[key].cognome;
				}
				$scope.tableIngegneri = $scope.AssocTable($scope.ingegneri, ['cognome', 'nome'], $filter);
				
				getREST.results[$scope.ingegnereREST].newDataZZ = false;
			}
		}
		if(getREST.results[$scope.regioneREST]){
			if(getREST.results[$scope.regioneREST].newDataZZ){
				$scope.regioni = getREST.results[$scope.regioneREST];
				
				getREST.results[$scope.regioneREST].newDataZZ = false;
			}
		}
		var path = $scope.ingegnereREST+'/new/'+$scope.nuovoIngegnereResp.id_new;
		if(getREST.results[path]){
			if(getREST.results[path].newDataZZ){
				getREST.results[path].isC_details = true;
				$scope.ingegneri.push(getREST.results[path]);
				// $scope.nuovoIngegnereResp = getREST.results[path];
				getREST.results[path].newDataZZ = false;
				
				// $scope.tableConcess.$params.filter = {
					// nome: getREST.results[path].nome
				// };
				// $scope.show_hide_Concess_details(getREST.results[path]);
				
			}
		}
		
		var path = $scope.ingegnereREST+'/new/'+$scope.nuovoIngegnereSost.id_new;
		if(getREST.results[path]){
			if(getREST.results[path].newDataZZ){
				getREST.results[path].isC_details = true;
				$scope.ingegneri.push(getREST.results[path]);
				// $scope.nuovoIngegnereSost = getREST.results[path];
				getREST.results[path].newDataZZ = false;
				
				// $scope.tableConcess.$params.filter = {
					// nome: getREST.results[path].nome
				// };
				// $scope.show_hide_Concess_details(getREST.results[path]);
				
			}
		}
		
		var path = $scope.digheInvasoREST.replace("?", G_damID);
		if(getREST.results[path]){
			if(getREST.results[path].newDataZZ){
				/* 
				var digaLocale = "";
				var digheInvaso = new Array();
				for(var key in getREST.results[path]){
					var c = getREST.results[path][key];
					if(c.idInvaso){
						digheInvaso.push(c.id + " - " + c.nomeDiga);
						if(c.id == G_damID) digaLocale = c.id + " " + c.nomeDiga;
					}
				}
				
				if(digheInvaso.length > 1){
					var infoText = "La diga " + digaLocale + " appartiene all\'invaso formato da:<br/><br/>";
					for(var key in digheInvaso) infoText += digheInvaso[key] + "<br />";
					
					var dlg = dialogs.notify("Attenzione! Invaso con più dighe", infoText);
				}
				 */
				
				
				var digaLocale = "";
				$scope.digheInvaso = new Array();
				for(var key in getREST.results[path]){
					var c = getREST.results[path][key];
					if(c.idInvaso){
						$scope.digheInvaso.push(c.id + " - " + c.nomeDiga);
						if(c.id == G_damID) digaLocale = c.id + " " + c.nomeDiga;
					}
				}
				
				if($scope.digheInvaso.length > 1){
					var infoText = "La diga " + digaLocale + " appartiene all\'invaso formato da:<br/><br/>";
					for(var key in $scope.digheInvaso) infoText += $scope.digheInvaso[key] + "<br />";
					
					var dlg = dialogs.notify("Attenzione! Invaso con più dighe", infoText);
				}
				
				getREST.results[path].newDataZZ = false;
			}
		}		
		
		var pathRubrica = $scope.rubricaREST + "/" + G_damID;

		if(getREST.results[pathRubrica]){
			if(getREST.results[pathRubrica].newDataZZ){
				$scope.datiRubrica = getREST.results[pathRubrica];
				getREST.results[pathRubrica].newDataZZ = false;
				
					
				if($scope.datiRubrica.casaDiGuarda)  $scope.datiRubrica.casaDiGuarda.lastModifiedDate = new Date($scope.datiRubrica.casaDiGuarda.lastModifiedDate.substr(0,19).replace(' ', 'T')).toItDateString();
				
				
				//Chiamate singole per recuperare dati 
				for(var key in $scope.datiRubrica.idConcess){
					getREST.restJson($scope.concessionarioREST + "/" + $scope.datiRubrica.idConcess[key]);
				}
				for(var key in $scope.datiRubrica.idGestore){
					getREST.restJson($scope.gestoreREST + "/" + $scope.datiRubrica.idGestore[key]);
				}
				for(var key in $scope.datiRubrica.idIngResp){
					$scope.datiRubrica.idIngResp[key].empty = true;
					getREST.restJson($scope.ingegnereREST + "/" + $scope.datiRubrica.idIngResp[key].id);
				}
				for(var key in $scope.datiRubrica.idIngSost){
					$scope.datiRubrica.idIngSost[key].empty = true;
					getREST.restJson($scope.ingegnereREST + "/" + $scope.datiRubrica.idIngSost[key].id);
				}
				for(var key in $scope.datiRubrica.idAutorita){
					getREST.restJson($scope.autoritaREST + "/" + $scope.datiRubrica.idAutorita[key]);
				}
				for(var key in $scope.datiRubrica.idAltraEnte){
					getREST.restJson($scope.altraEnteREST + "/" + $scope.datiRubrica.idAltraEnte[key]);
				}
				
			}
		}
		/* 
		if($scope.reloadTableIngegneriResp){
			$scope.tableGestori.reload();
			$scope.reloadTableIngegneriResp = false;
		}
		 */
	});
	
	getREST.restJson($scope.provinciaREST);
	getREST.restJson($scope.regioneREST);
	getREST.restJson($scope.tipiContattoREST);
	getREST.restJson($scope.tipiAutoritaREST);
	getREST.restJson($scope.tipiAltroEnteREST);
	
	$rootScope.$broadcast('TypeLoadCompleted');
}]);