mainModule.factory("storageService", ["$timeout", function($timeout) {
	return {
		defaultStorage:{
			sizes:{"12": "small.css", "16":"medium.css", "18":"large.css"},
			colors:{"0": "default.css", "1": "bw.css", "2": "green.css"},
		},
		storage:{
			sizes:{"12": "small.css", "16":"medium.css", "18":"large.css"},
			colors:{"0": "default.css", "1": "bw.css", "2": "green.css"},
		},
		
		get:function(name, defaultValue){
			if(typeof this.storage[name]==="undefined" ){
				//Se il valore non esiste ritorna null oppure il valore di default, se è stato settato
				return typeof defaultValue==="undefined" ? null : defaultValue;
			}
			return this.storage[name];
		},
		
		set:function(name, value){
			this.storage[name]=value;
		},
		
		clear: function(){
			this.storage=jQuery.extend(true, {}, this.defaultStorage);
		}
	}
}]);