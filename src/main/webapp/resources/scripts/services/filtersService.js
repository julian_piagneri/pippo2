mainModule.factory("filtersService", ["$timeout", "storageService",function($timeout, storageService) {
	return {
		
		selectFilters:
		[
		 	function(row){
		 		//Tutte le dighe
		 		return true;
		 	},
		 	function(row){
		 		//Dighe preferite
		 		return row.entity.preferita==true;
		 	},
		 	function(row){
		 		//Dighe di competenza
		 		var user=storageService.get("login.user");
		 		return user.id==row.entity.idFunzionarioCentrale || user.id==row.entity.idFunzionarioPerif;
		 	}
		],
		
		applyFavorites:function($scope, event){
			var that=this;
	    	var rows=$scope.gridApi.grid.rows;
			angular.forEach(rows, function(row, index){
				row.clearThisRowInvisible('select');
				//Nasconde le dighe che non soddisfano le condizioni
				if(!that.selectFilters[$scope.showFilter.select](row)){
					row.setThisRowInvisible('select');
				}
			});
			//Seleziona la prima riga delle righe visibili
			$timeout(function(){$scope.selectRow(0, true)});
			//$scope.lockEvents={ selectChange:true, filtersChange:true, columnsChange:true};
			//$scope.setPendingEdit(true, event);
	    },
	    
	    applyFilters:function($scope, event){
	    	var that=this;
	    	var rows=$scope.gridApi.grid.rows;
			angular.forEach(rows, function(data, index){
				that.setFilterForRow($scope, data);
			});
			//Seleziona la prima riga delle righe visibili
			$timeout(function(){$scope.selectRow(0, true)});
			//$scope.setPendingEdit(true, event);
	    },
	    
		setFilterForRow:function($scope, row){
			//var isCustomInvisible=false;
			var filters=$scope.showFilter;
//			if ( typeof(row.invisibleReason) !== 'undefined' &&  typeof(row.invisibleReason.filters) !== 'undefined' ){
//				isCustomInvisible=row.invisibleReason.filters;
//			}
			var mustShow=false;
			mustShow=this.ridFilter(filters.rid, row, mustShow);
			mustShow=this.statusFilter(filters.accertamento, row, ["AC"], mustShow);
			mustShow=this.statusFilter(filters.progetto, row, ["PR", "PM", "PE"], mustShow);
			mustShow=this.statusFilter(filters.competenza, row, ["PD"], mustShow);
			mustShow=this.statusFilter(filters.altro, row, ["AD", "ND"], mustShow);
			
			row.clearThisRowInvisible('filters');
			if(!mustShow){
				row.setThisRowInvisible('filters');
			}
		},
		
		executeFilter:function(filter, condition, mustShow){
			//Se il filtro è disabilitato allora continua con i filtri		
			var filterCond=false;
			if(filter==true){
				filterCond=condition;
			}
			return mustShow || filterCond;
		},
		
		ridFilter:function(filter, row, mustShow){
			return this.executeFilter(filter, row.entity.competenzaSnd=="S", mustShow);
		},
		
		statusFilter:function(filter, row, values, mustShow){
			return this.executeFilter(filter, $.inArray(row.entity.status1Sigla, values)!=-1, mustShow);
		},
	}
}]);