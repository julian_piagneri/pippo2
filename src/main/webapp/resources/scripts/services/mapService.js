mainModule.factory("mapService", function() {
	return {
		map_reduced:true,
		//Ridimensiona le colonne
		hRatio:8,
		wRatio:9,
		maxCol:11,
		minCol:1,
		mapCol:2,
		listCol:10,
		className:"col-md-",
		mapOffset:30,
		
		resizeWhenBig:function(damMap, damList, map, size, skip){
			//console.log("window.width: "+$(window).width()+" map.left: "+map.css("left")+" ow: "+damMap.outerWidth()+" size: "+size+" map.w: "+map.width());
			//var offset=0;
			if(damMap.outerWidth()<=(map.width()+this.mapOffset) || skip){ //skip se deve saltare il controllo
				map.width((damMap.outerWidth()-this.mapOffset)+"px");
				// map.height((map.width()*this.hRatio)/this.wRatio);
				
				// alert(damList.outerHeight());
				map.height(damList.outerHeight());
				
				
				map.css("left", damMap.outerWidth()-map.width()-this.mapOffset);
			}
		},
		
		setMapResizable:function(){
			that=this;
			$( "#map" ).resizable({
				handles:"w",
				// aspectRatio: this.wRatio / this.hRatio,
				ghost: true,
				//containment: "body",
				stop: function(event, ui) {
					that.resizeMap(ui);
				}
			});
		},
		
		resizeMap:function(ui){
			var damList=$("#dam-list-list");
			var damMap=$("#dam-list-map");
			var map=$("#map");
			var size=ui.size.width;
			this.executeResize(damList, damMap, map, size);
		},
		
		executeResize:function(damList, damMap, map, size){
			//console.log("STOP window.width: "+$(window).width()+" map.left: "+map.css("left")+" ow: "+damMap.outerWidth()+" size: "+size+" map.w: "+map.width());
			//parte da 0 perchè viene aumentato subito nel ciclo
			var col=this.minCol-1;
			var className=this.className;
			var found=false;
			//Rimuove le attuali classi delle sezioni
			damMap.removeClass(className+this.mapCol);
			damList.removeClass(className+this.listCol);
			while(!found && col<=this.maxCol){
				damMap.removeClass(className+col);
				col++;
				//Se arriva a 9 passa alla visualizzazione su due righe
				if(col>9){
					col=12;
				}
				damMap.addClass(className+col);
				//Controlla se con la nuova dimensione la mappa riesce ad entrare nel contenitore
				//30 è un offset
				if(damMap.outerWidth()>(map.width()+this.mapOffset) || col>this.maxCol){ //quando col arriva a 12 allora entra per forza
					//Saltiamo il controllo della dimensione solo se la nuova dimensione fa cambiare le colonne
					skip=true;
					if(this.mapCol==col){
						skip=false;
					}

					
					this.mapCol=col;
					
					map.css("left", damMap.outerWidth()-map.width()-this.mapOffset);
					this.listCol=this.maxCol-col+1;
					//Se inserisco 12 colonne alla mappa, allora la lista ne avrà 12 pure (si vedranno su 2 righe)
					if(this.listCol<=0){
						this.listCol=12;
					}
					damList.addClass(className+this.listCol);
					found=true;
					//Ridimensiona la mappa alla dimensione della nuova colonna
					this.resizeWhenBig(damMap, damList, map, map.width(), skip);
				}
			}
			G_dam_map.map.updateSize(); 
		},
		
		setMapData: function(data, $scope){
			$scope.layer = data;
			G_dam_map = new damMapObj(); 
			// G_dam_map.loadLayers(data);
			//G_dam_map.addDigheLayer($scope.dighe);
			$(G_dam_map.map.getViewport()).on('click touchstart', function(evt) {
				var pixel = G_dam_map.map.getEventPixel(evt.originalEvent);
				G_dam_map.selectDamOnMap(pixel);
			});
			var mWidth=$("#map").width();
			$("#map").width(mWidth+"px");
			// $("#map").height(($("#map").width()*$scope.mapService.hRatio)/$scope.mapService.wRatio);
			$("#map").height($("#dam-list-list").outerHeight());
			G_dam_map.map.updateSize();
			$scope.mapService.setMapResizable();
		},
		
		/**
		 * INUTILIZZATI
		 */
		expandMap:function(){
			changeMapSize(false);
		},
		
		reduceMap:function(){
			changeMapSize(true);
		},
		
		changeMapSize:function(reduce){
			var damListList=$("#dam-list-list");
			var damListMap=$("#dam-list-map");
			var className="col-md-";
			//var bigClass="col-md-10";
			//var littleClass="col-md-2";
			//Ipotesi di lavoro: stiamo espandendo
			//var firstClass=bigClass;
			//var secondClass=littleClass;
			//La somma tra mapCol e listCol deve essere 12, visto che è il massimo delle colonne nel layout a colonne di Bootstrap
			if(this.mapCol+this.listCol==this.maxCol+this.minCol){
				if(reduce){
					//Stiamo riducendo la mappa
					//var firstClass=littleClass;
					//var secondClass=bigClass;
					damListList.removeClass(className+this.listCol).addClass(className+(++this.listCol));
					damListMap.removeClass(className+this.mapCol).addClass(className+(--this.mapCol));
				} else {
					damListList.removeClass(className+this.listCol).addClass(className+(--this.listCol));
					damListMap.removeClass(className+this.mapCol).addClass(className+(++this.mapCol));
				}
				setTimeout(function(){
					G_dam_map.map.updateSize(); 
					//this.gridLayout.updateGridLayout()
				}, 550);
				G_dam_map.map.updateSize();
				//this.gridLayout.updateGridLayout();
				this.map_reduced=reduce;
			}
		},
		
	}
});