mainModule.factory("helpersService", ['TokenStorage', function(TokenStorage) {
	return {
		//Trasforma l'array di boolean in array di interi
		boolAsInt:function(values){
			var newValues={};
			angular.forEach(values, function(data, index){
				var newVal=data;
				if(data===true){
					newVal=1;
				} else if(data===false){
					newVal=0;
				}
				newValues[index]=newVal;
			});
			return newValues;
		},
		//Trasforma l'array di boolean in array di interi tutti veri
		boolAsIntTrue:function(values){
			var newValues={};
			angular.forEach(values, function(data, index){
				//Per il filtro select
				if(data!==0){
					newValues[index]=true;
				}
			});
			return this.boolAsInt(newValues);
		},
		//Restituisce un tooltip per le celle con più di un valore
		printArrayAsComma:function(array, valName){
			str="";
			angular.forEach(array, function(data, index){
				if(index!=0){
					str+=", ";
				}
				if(valName!=""){
					str+=data[valName];
				} else {
					str+=data;
				}
			});
			return str;
		},
		//Scrolla una lista di valori multipli
		scrollDamList:function(array, row, className, up){
			//var container=$("#"+row.entity.id+"-"+className);
			var index=row.entity.currentGridValues[className].index;
			if(up=="1"){
				index++;
			} else {
				index--;
			}
			//Permette di scorrere in modo circolare
			if(index<0){
				index=array.length-1;
			} else if(index>=array.length){
				index=0;
			}
			if(index>=0 && index<array.length){
				//container.html(array[index][className]);
				row.entity.currentGridValues[className].index=index;
				row.entity.currentGridValues[className].value=array[index];
				//container.data("current", index);
			}
		},
		
		openExternalWindow:function(href){
			var link = href;
			if(href.indexOf("?") > -1) link += "&token="+encodeURIComponent(TokenStorage.retrieve());
			else link += "?token="+encodeURIComponent(TokenStorage.retrieve());
			console.log(link);
			return window.open(link);
		},
		
		openNewSiteWindow:function(href, params, $scope, winName, forceHttp){
			//user="+$rootScope.username+"&
			if(forceHttp){
				var link = document.createElement("a");
				link.href = href;
				href = "http://"+link.host+link.pathname;	
			}
			if (typeof winName === "undefined" || winName == "") {
				winName = 'dlog';
			};
			console.log("log per controllare il bug dell'apertura multipla di finestre");
			var str="";
			angular.forEach(params, function(data, index){
				str+="&"+index+"="+encodeURIComponent(data);
			});
			return window.open(href+"?t="+encodeURIComponent(TokenStorage.retrieve())+str,winName,'_blank,modal=yes,width=1336px,height=698px,resizable,scrollbars,center,screenX=15,screenY=35').focus();
		},
		
		getObjectIfJson:function(string){
			try {
				//Se è già parsato
				if(typeof string ==='object'){
					return string;
				}
				return JSON.parse(string);
			} catch(e) {
				return false;
			}
		}
		
	}
}]);