mainModule.factory("gridService", [function() {
	return {
		maxDebug:0,
		getRowTemplate:function(){
			return '<div tooltip-append-to-body="true" tooltip="{{COL_FIELD | blankplace:'+"''"+'}}" class="ui-grid-cell-contents ng-binding ng-scope row-list-container">'
			+ '<div data-current="0" class="cell-content">{{COL_FIELD | blankplace}}</div>'
			+ '</div>';
		},
		getColumns:function($scope){
			var that=this;
			if(typeof this.columns==="undefined"){
				this.columns=[
					{field: 'numeroArchivio', displayName: "N. Arc.", width: 70, minWidth: 50, type:"numberStr", cellTemplate: that.getRowTemplate()},
					{field: 'sub', displayName: "Sub", width: 50, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'nomeDiga', displayName: "Nome diga", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'funzSedeCentrale', displayName: "Funzionario (sede centrale)", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'funzUffPerif', displayName: "Funzionario (uff. periferico)", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					
					{field: 'concessionari', displayName: "Concessionari", width: 200, minWidth: 50, //currentGridValues.concessionari.value 
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.concessionari.value", "row", "row.entity.concessionari", 'concessionari', 'grid.appScope.', '') //COL_FIELD
					},
					{field: 'gestori', displayName: "Gestori", width: 200, minWidth: 50, //currentGridValues.gestori.value 
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.gestori.value", "row", "row.entity.gestori", 'gestori', 'grid.appScope.', '') //COL_FIELD
					},
					
					
					{field: 'uffSedeCentrale', displayName: "Ufficio centrale", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'uffPeriferico', displayName: "Ufficio periferico", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
	
					//{field: 'localita', displayName: "Località", width: "auto", minWidth: 50},
					//{field: 'regioneRiferimento', displayName: "Sub", width: "auto", minWidth: 50},
					//{field: 'utilizzPrev', displayName: "Sub", width: "auto", minWidth: 50},
					{field: 'status1Sigla', displayName: "Status 1", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'status2Sigla', displayName: "Status 2", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					
					{field: 'altezzaDigaL584', displayName: "H L.584/94", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'volumeTotInvasoL584', displayName: "Vol. tot. invaso L.584/94", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'quotaAutoriz', displayName: "Quota autorizzata", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					
					{field: 'currentGridValues.ingegneri.string.nome', displayName: "Ingegneri Resp.", width: 200, minWidth: 50, //currentGridValues.ingegneri.value.nome 
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.ingegneri.value.nome", "row", "row.entity.ingegneri", 'ingegneri', 'grid.appScope.', 'nome') //COL_FIELD
					},
					
					{field: 'currentGridValues.nomiComune.string.nomeRegione', displayName: "Regione", width: 200, minWidth: 50, //currentGridValues.nomiComune.value.nomeRegione
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.nomiComune.value.nomeRegione", "row", "row.entity.nomiComune", 'nomiComune', 'grid.appScope.', 'nomeRegione') //COL_FIELD
					},
					{field: 'currentGridValues.nomiComune.string.nomeProvincia', displayName: "Provincia", width: 200, minWidth: 50, //currentGridValues.nomiComune.value.nomeProvincia
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.nomiComune.value.nomeProvincia", "row", "row.entity.nomiComune", 'nomiComune', 'grid.appScope.', 'nomeProvincia') //COL_FIELD
					},
					{field: 'currentGridValues.nomiComune.string.nomeComune', displayName: "Comune", width: 200, minWidth: 50, //currentGridValues.nomiComune.value.nomeComune 
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.nomiComune.value.nomeComune", "row", "row.entity.nomiComune", 'nomiComune', 'grid.appScope.', 'nomeComune') //COL_FIELD
					},
					{field: 'currentGridValues.utilizzo.string.utilizzo', displayName: "Utilizzazione", width: 200, minWidth: 50, //currentGridValues.utilizzo.value.utilizzo
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.utilizzo.value.utilizzo", "row", "row.entity.utilizzo", 'utilizzo', 'grid.appScope.', 'utilizzo') //COL_FIELD
					},
					{field: 'currentGridValues.utilizzo.string.priorita', displayName: "Priorità", width: 200, minWidth: 50, //currentGridValues.utilizzo.value.priorita 
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.utilizzo.value.priorita", "row", "row.entity.utilizzo", 'utilizzo', 'grid.appScope.', 'priorita') //COL_FIELD
					},
					{field: 'nomeFiumeSbarrato', displayName: "Fiume sbarrato", width: 200, minWidth: 50},
					{field: 'nomeFiumeValle', displayName: "Fiumi a valle", width: 200, minWidth: 50, //currentGridValues.nomeFiumeValle.value 
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.nomeFiumeValle.value", "row", "row.entity.nomeFiumeValle", 'nomeFiumeValle', 'grid.appScope.', '') //COL_FIELD
					},
					{field: 'currentGridValues.nomiComune.string.zonaSismicaComune', displayName: "Zona sism. comune", width: 200, minWidth: 50, //currentGridValues.nomiComune.value.zonaSismicaComune
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.nomiComune.value.zonaSismicaComune", "row", "row.entity.nomiComune", 'nomiComune', 'grid.appScope.', 'zonaSismicaComune') //COL_FIELD
					},
					{field: 'comuneOperaPresa', displayName: "Comune Opera Presa", width: 200, minWidth: 50, //currentGridValues.comuneOperaPresa.value
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.comuneOperaPresa.value", "row", "row.entity.comuneOperaPresa", 'comuneOperaPresa', 'grid.appScope.', '') //COL_FIELD
					},
					
					{field: 'nomeLago', displayName: "Lago", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'bacino', displayName: "Bacino", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'annoConsegnaLavori', displayName: "Anno consegna lavori", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'annoUltimazLavori', displayName: "Anno ultimazione lavori", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'dataCertCollaudo', displayName: "Data cert. collaudo", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					
					{field: 'altezzaDigaDm', displayName: "H DM 24/03/82", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'descrClassDiga', displayName: "Classifica diga DM 24/03/82", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'quotaMaxRegolaz', displayName: "Quota max regolazione", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'volumeTotInvasoDm', displayName: "Vol. tot. invaso DM 24/03/82", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					
					{field: 'volumeLaminazione', displayName: "Vol di laminazione", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'volumeAutoriz', displayName: "Volume autorizzato", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
								
					{field: 'portMaxPienaProgetto', displayName: "Portata max piena", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					{field: 'condotteForzate', displayName: "Condotte forzate", width: 200, minWidth: 50, cellTemplate: that.getRowTemplate()},
					
					{field: 'tipoOrganoScarico', displayName: "Tipo organo di scarico", width: 200, minWidth: 50, //currentGridValues.tipoOrganoScarico.value 
						cellTemplate: $scope.getMultiRowTemplate("row.entity.currentGridValues.tipoOrganoScarico.value", "row", "row.entity.tipoOrganoScarico", 'tipoOrganoScarico', 'grid.appScope.', '') //COL_FIELD
					},			
				];
			}
			return this.columns;
		}
	}
}]);