mainModule.factory("dialogService",['dialogs', 'ngDialog', '$templateCache', '$timeout', '$http', 
function(dialogs, ngDialog, $templateCache, $timeout, $http) {   
	return {
		dialogWindows:{},
		
		openListDialog:function($scope){
			var settings={
				className: 'ngdialog-theme-plain custom-width ngdialog-assoc-table', 
				showClose: false,
				closeByDocument: false,
				scope: $scope, 
				template: 'listDialog'
			};
			this.openDialog($scope, damRoutes.routeListDialog, 'listDialog', 'listDialog', settings);
		},
		
		closeListDialog:function($scope, variableName){
			this.closeDialog($scope, 'listDialog');
			this.closeDialog($scope, 'loadingDialog');
		},
	
		openLoadingDialog:function($scope){
			var settings={
				className: 'ngdialog-theme-plain custom-width ngdialog-assoc-table', 
				showClose: false,
				closeByDocument: false,
				scope: $scope, 
				template: 'loadingDialog'
			};
			this.openDialog($scope, damRoutes.routeLoadingDialog, 'loadingDialog', 'loadingDialog', settings);
		},
		
		closeLoadingDialog:function($scope, variableName){
			this.closeDialog($scope, 'loadingDialog');
		},
		
		openLoginDialog:function($scope){
			var settings={
					className: 'ngdialog-theme-default ngdialog-assoc-table ngdialog-content', 
					showClose: false, 
					closeByDocument: false,
					scope: $scope, 
					template: 'loginDialog'
			};
			this.openDialog($scope,damRoutes. routeLoginForm, 'loginDialog', 'loginDialog', settings);
		},
		
		closeLoginDialog:function($scope, variableName){
			this.closeDialog($scope, 'loginDialog');
		},
		
		openDialog:function($scope, route, templateName, variableName, settings){
			var loadTemplate=this.loadDialog(route, templateName);
			var that=this;
			loadTemplate.then(function(){
				if(that.dialogWindows[variableName]==null || typeof that.dialogWindows[variableName]==="undefined"){
					that.dialogWindows[variableName]=ngDialog.open(settings);
				}
			});
		},
		
		loadDialog:function(route, templateName){
			return $timeout(function(){
				//Controlla che il template non sia già in cache
				var template=$templateCache.get(templateName);
				if(typeof template==="undefined"){
					return $http.get(route).success(function (data){
						$templateCache.put(templateName, data);
					});
				}
			});
		},
		
		closeDialog:function($scope, variableName){
			if(typeof this.dialogWindows[variableName]!=="undefined" && this.dialogWindows[variableName]!=null){
				this.dialogWindows[variableName].close();
				this.dialogWindows[variableName]=null;
			}
		},
	}
}]);