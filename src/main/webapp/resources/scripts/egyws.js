
G_dam_list_scope = window.opener.G_dam_list_scope;

damMapModule.controller('mapLayerController', 
		['$scope', 'getREST', '$http', '$timeout', '$templateCache', 'dialogs', 'ngDialog',
          //Servizi
         'dialogService', 'storageService', 'helpersService', 
         function ($scope, getREST, $http, $timeout, $templateCache, dialogs, ngDialog,
         //Servizi
         dialogService, storageService, helpersService) {
	
	
	/*** REST ***/
	$scope.dialogService=dialogService;
	$scope.storageService=storageService;
	$scope.helpersService=helpersService;
	
	$scope.overlayListValues={};
	$scope.extraInfoFiumi={};
	
	$scope.restID = 'resources/layerList.json';
	$scope.done = false;
	
	$scope.mapLoadComplete=false;
	$scope.showAccordions=false;
	
	$scope.mouseEventOpen = true;
	$scope.mouseEventcount = 0;
	
	$scope.showLayerControls=function(){
		$scope.showAccordions=!$scope.showAccordions;
	};
	
	$scope.getLegendStyle = function(_type, _stroke_width, _stroke_color, _fill_color,  _radius){
		var _st = {};
		_st.border = 'solid';
		_st.padding = 0;
		switch(_type){
			case 'vector':
				_st.width = '40px';
				_st.height = '20px';
				_st.borderRadius = '50%';
				_st.borderWidth = _stroke_width;
				break;
			case 'line':
				_st.borderWidth = _stroke_width/2;
				_st.marginTop = 7;
				_st.width = '40px';
				break;
			case 'point':
				// Math.PI
				var _width = 2*_radius+_stroke_width;
				_st.marginTop = 7-_stroke_width;
				_st.width = _width;
				_st.height = _width;
				_st.borderRadius = '50%';
				_st.borderWidth = _stroke_width;
				break;
			default:
				break;
		}
		_st.borderColor = _stroke_color;
		_st.background = _fill_color;
		
		return _st;
	};
	
	$scope.overlay = {
		active: false
	};
	$scope.feature = {
		active: false
	};
	
	$scope.filterDam = {
		active: false
	};
	
	
	getREST.setRestID($scope.restID);
	$scope.layerList = new Array();
	$scope.overlayList = new Array();
	
	$scope.latlong = {
		latlong: 0,
		lat: 0,
		long: 0
	};
	
	$scope.sisma = {
		magnitudo: 0,
		raggio: 0,
		type: 0
	};
	
	$scope.draggedLayer = null;
	$scope.draggedOverlay = null;
	
	$scope.btnActive = {
		pan: true,
		feat: false,
		marker: false,
		box: false,
		circle: false,
		poly: false,
		featuredam: false
	};
	
	$scope.layersOptions = {
		zoomVisible: true,
		activeVisible: false
	};
	
	$scope.filtraDighe = function(){
		if(!$scope.filterDam.active) G_dam_list_scope().$apply(function(){G_dam_list_scope().setDamsListFromMap({});});
		else G_dam_map.selectDams4CurrentFeat();
	};
	
	$scope.addLayer = function(_layer){
		_layer.isVisible = _layer.olLayer.getVisible();
		$scope.layerList.push(_layer);
	};	

	$scope.addOverlay = function(_feature){
		_feature.GeometryName = _feature.get("name");	
		$scope.overlayList.push(_feature);
	};
	
	$scope.removeOverlay = function(_feature){
		var idx = $scope.overlayList.indexOf(_feature);
		$scope.overlayList.splice(idx, 1);
		G_dam_map.removeFeatureOverlay(_feature);
	};
	
	$scope.switchVisible = function(_olLayer){
		_olLayer.setVisible(false);
	};
	
	$scope.getDragOverlay = function(event, ui, _overl){
		$scope.draggedOverlay = _overl;
	};
	
	$scope.getDragLayer = function(event, ui, _layer){
		$scope.draggedLayer = _layer;
	};
	
	$scope.moveOverlay = function(event, ui, _overl){
		var new_index = null;
		var old_index = null;
		for(var key in $scope.overlayList){
			if($scope.overlayList[key] == _overl) new_index = key;
			if($scope.overlayList[key] == $scope.draggedOverlay) old_index = key;
		}
		
		if(new_index == null || old_index == null) return;
		$scope.overlayList.splice(new_index, 0, $scope.overlayList.splice(old_index, 1)[0]);
		
	};
	
	$scope.moveLayer = function(event, ui, _layer){
		
		if($scope.draggedLayer == null) return;
		
		G_dam_map.moveLayerPosition($scope.draggedLayer.position, _layer.position);
		
		for(var key in $scope.layerList){
			$scope.layerList[key].position = G_dam_map.getLayerPosition($scope.layerList[key].olLayer);
		}
		$scope.draggedLayer = null;
	};
	
	$scope.changeOpac = function(_layer){
		_layer.olLayer.set('opacity', parseFloat(_layer.opacity));
	};
	
	$scope.pippo = function(_val){
	};
	
	$scope.isResolution = function(_resol){
		if(_resol === undefined) return true;
		if(_resol < G_dam_map.view.getResolution()) return false;
		return true;
	};
	
	$scope.btnReset = function(){
		G_dam_map.removeSelectLayer();
		G_dam_map.removeInteraction();
		$scope.btnActive.pan = false;
		$scope.btnActive.feat = false;
		$scope.btnActive.marker = false;
		$scope.btnActive.box = false;
		$scope.btnActive.circle = false;
		$scope.btnActive.poly = false;
		$scope.btnActive.featuredam = false;
		$scope.mouseEventcount = 0;
		
	};
	
	$scope.overlayEvent = function(){
		if(!$scope.overlay.active) G_dam_map.hideOverlay();
		else G_dam_map.showOverlay();
	};
	
	$scope.selectPan = function(){
		
		$scope.btnReset();
		$scope.btnActive.pan = true;
	};
	
	$scope.selectFeat = function(){
		
		$scope.btnReset();
		$scope.btnActive.feat = true;
	};
	
	$scope.selectMarker = function(){
		
		$scope.btnReset();
		$scope.btnActive.marker = true;
	};
	
	$scope.selectBox = function(){
		$scope.btnReset();
		$scope.btnActive.box = true;
		G_dam_map.startSelectBox($scope.overlay, $scope.filterDam);
	};
	
	$scope.selectCircle = function(){
		$scope.btnReset();
		$scope.btnActive.circle = true;
		G_dam_map.startSelectCircle($scope.overlay, $scope.filterDam);
	};
	
	$scope.selectpoly = function(){
		$scope.btnReset();
		$scope.btnActive.poly = true;
		G_dam_map.startSelectPoly($scope.overlay, $scope.filterDam);
	};
	
	$scope.selectFeatureDam = function(){
		$scope.btnReset();
		$scope.btnActive.featuredam = true;
	};
	
	$scope.getSelectedOverlayF = function(){
		var overlayUnion = [];
		for(var idx in $scope.overlayList) if($scope.overlayList[idx].active) overlayUnion.push($scope.overlayList[idx]);
		
		return overlayUnion;
	};
	
	$scope.removeSelectedOverlayF = function(){
		var flg_remove = true;
		while(flg_remove){
			flg_remove = false;		
			for(var idx in $scope.overlayList){
				if($scope.overlayList[idx].active){
					$scope.overlayList.splice(idx, 1);
					flg_remove = true;
				}
			}	
		}
		
	};
	
	$scope.turf = function(_act){
		if($scope.overlayList.length>1){
			
			var feature = G_dam_map.turfOverlay($scope.getSelectedOverlayF(), _act);
			
			$scope.removeSelectedOverlayF();
			if(feature) $scope.addOverlay(feature);
		}
	};

	$scope.overlayListName="";
	$scope.selectIntersectLayer = function(){
		
		var valuesFromMap = G_dam_map.startSelectIntersectLayer($scope.getSelectedOverlay());
		var feature=valuesFromMap.mapValues;
		
		$scope.overlayListValues=feature;
		$scope.overlayListName=valuesFromMap.layerName;
		dialogService.openListDialog($scope);
	};
	
	$scope.getSelectedOverlay = function(){
		var overlayIntersectLayer = [];
		for(var idx in $scope.overlayList) if($scope.overlayList[idx].active) overlayIntersectLayer.push($scope.overlayList[idx]);
		return overlayIntersectLayer;
	};
	
	$scope.updateLatLong2Array = function(){
		$scope.latlong.latlong = new Array($scope.latlong.long, $scope.latlong.lat);
	};
	
	
	$scope.updateArray2LatLong = function(){
		$scope.latlong.lat = $scope.latlong.latlong[1].toFixed(5);
		$scope.latlong.long = $scope.latlong.latlong[0].toFixed(5);
	};
	
	$scope.calculateSisma = function(){
		G_dam_map.startCalculateSisma($scope.overlay, $scope.filterDam,$scope.latlong.long, $scope.latlong.lat, $scope.sisma.raggio, $scope.sisma.type);
	};
	
	$scope.calculateRadius0 = function(){
		var radius = 0.4255 * Math.pow($scope.sisma.magnitudo, 2.9403) * 1000; // in metri
		return Math.round(radius);
	};
	
	$scope.calculateRadius1 = function(){
		var radius = 0.71 * Math.pow(Math.E, 0.76 * $scope.sisma.magnitudo) * 1000; // in metri
		return Math.round(radius);
	};
	
	$scope.calculateRadius2 = function(){
		var radius = 0.2 * Math.pow(Math.E, 0.84 * $scope.sisma.magnitudo) * 1000; // in metri
		return Math.round(radius);
	};
	
	$scope.calcolaRaggio = function(){
	if($scope.sisma.type == 0)
		$scope.sisma.raggio = $scope.calculateRadius0();
	if($scope.sisma.type == 1)
		$scope.sisma.raggio = $scope.calculateRadius1();
	if($scope.sisma.type == 2)
		$scope.sisma.raggio = $scope.calculateRadius2();
	};
	
	$scope.initMap=function(){
		$http.get(damRoutes.routeExtraInfoFiumi).success(function (data){
			$scope.extraInfoFiumi=data;
			$scope.infoFiumiPopup = $scope.convertInfoFiume2KV(data);
		});
	};
	
	$scope.Overlay2SelectLayer = function(){
		G_dam_map.Overlay2SelectLayer($scope.getSelectedOverlay(), $scope.filterDam.active);
		$scope.overlay.active = false;
		G_dam_map.hideOverlay();
	};
	
	$scope.convertInfoFiume2KV = function(_Info){
		var _return = $.extend(true, {}, _Info);
		for(var key in _return){
			var _ei = $scope.convertExtraInfo2KV(_return[key].extraInfo);
			for(var key2 in _ei){
				_return[key][key2] = _ei[key2];
			}
			if(_return[key].id == 22) console.log(_return[key]);
		}
		return _return;
	};
	
	$scope.convertExtraInfo2KV = function(_extraInfo){
		var _return = $.extend(true, {}, _extraInfo);
		for(var key in _return){
			var a = [];
			for(var keyVal in _return[key].values) a.push(_return[key].values[keyVal].valore);
			_return[key] = a;
			
		}
		if(_return.Lunghezza) console.log(_return);
		return _return;
	};
	
	
	$scope.$on('RESTreceived', function() {
		if(getREST.results[$scope.restID]){
			if(!$scope.done){
				$scope.done = true;
				G_dam_map = new damMapObj();
				window.opener.G_dam_map_window = G_dam_map;
				$scope.layer = getREST.results[$scope.restID];
				G_dam_map.loadLayers(getREST.results[$scope.restID]);
				G_dam_map.view.setZoom(5.5);
				G_dam_list_scope().$apply(function(){return G_dam_list_scope().setDamList2BigMap();});
				//console.log(G_dam_list_scope().selected.entity.id);
				G_dam_map.selectDamOnGrid(G_dam_list_scope().selected.entity.id, G_dam_list_scope().mapOptions.moveMap, true);
				
				$scope.mapLoadComplete=true;
				//console.log($("#map").innerHeight());
				$("#map-buttons").css("max-height", ($("#map").innerHeight()-7)+"px");
				//console.log($("#map-buttons").css("max-height"));
				$(window).on('resize', function(event) {
					$("#map-buttons").css("max-height", ($("#map").innerHeight()-7)+"px");
					console.log($("#map-buttons").css("max-height"));
				});
				$(G_dam_map.map.getViewport()).on('mousemove', function(evt) {
					if($scope.mouseEventOpen){
						$scope.mouseEventOpen = false;
						$scope.mouseEventcount++;
						G_dam_map.digheTooltip(evt.originalEvent);
						$scope.mouseEventOpen = true;
					}

					if($scope.btnActive.marker){
						$scope.latlong.latlong = G_dam_map.getLatLongFromEvent(evt.originalEvent);
						$scope.updateArray2LatLong();
						$scope.$apply();
					}
				});
				
				
				$(G_dam_map.map.getViewport()).on('click touchstart', function(evt) {
					if($scope.btnActive.marker){
						$scope.btnReset();
						$scope.btnActive.pan = true;
						G_dam_map.addMarker(evt.originalEvent);
						
					}
					var pixel = G_dam_map.map.getEventPixel(evt.originalEvent);
					G_dam_map.selectDamWithTooltip(evt, pixel);
					G_dam_map.selectLayerOnMap(pixel, $scope.overlay.active, $scope.filterDam.active, $scope.btnActive.feat);
				});
			}
			getREST.unsetRestID($scope.restID);
		}
	});
	
	
	/*** REST ***/
	
}]);


