mainModule.filter('blankplace', function() {
  return function(input, placeholder) {
    if(typeof placeholder==="undefined"){
		placeholder=".";
	}
    if(typeof input==="undefined" || input==null || input==""){
		input=placeholder;
	}
    return input;
  };
});