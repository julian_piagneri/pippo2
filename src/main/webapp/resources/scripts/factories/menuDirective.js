﻿
mainModule.directive('maMenu', function() {
	return {
		restrict: 'A',
		scope: {
			menu: '=maMenu',
			cls: '=ngClass'
		},
		replace: false,
		template: '<ul class="nav navbar-nav list-inline center-block text-center" id="navbar-list"><li dropdown class="dropdown" ng-repeat="item in menu" ma-menu-item="item" data-sub="0"></li></ul>',
		link: function(scope, element, attrs) {
			element.addClass(attrs.class);
			element.addClass(scope.cls);
		}
	};
});

mainModule.directive('maSubMenu', function() {
	return {
		restrict: 'A',
		scope: {
			menu: '=maSubMenu',
			cls: '=ngClass'
		},
		replace: false,
		template: '<li ng-repeat="item in menu" ma-menu-item="item" data-sub="1"></li>',
		link: function(scope, element, attrs) {
			element.addClass(attrs.class);
			element.addClass(scope.cls);
			//element.attr("class", element.data("classes"));
		}
	};
});

mainModule.directive('maMenuItem', function($compile) {
	var getTemplate=function(item){
//		if(item.hasOwnProperty("submenu") && item.submenu.length>0){
//			return '<a role="button" dropdown-toggle data-toggle="dropdown" class="btn btn-primary" data-target="#" href="#">' +
//						'{{item.title}} <span class="caret"></span>'+
//				   '</a>';
//		}
		return '<a class="btn" href={{item.href}}>{{item.descr}}</a>';
	};
	var linker=function (scope, element, attrs) {
		//Il tag A all'interno dei li
		element.html(getTemplate(scope.item)).show();
		var link=element.children('a');
		if (scope.item.header) {
			element.addClass('nav-header');
			element.text(scope.item.header);
		}
		if (scope.item.divider) {
			element.addClass('divider');
			element.empty();
		}
		//Scheda profilo, aggiunge le voci hardcoded
		if(scope.item.idScheda==1) {
			scope.item.figli.push({descr: 'Dati Utente', type:1, href: damRoutes.routeDatiProfilo});
			scope.item.figli.push({descr: 'Scelta Aspetto', type:1, href: damRoutes.routeStyleSnd});
			
		}
		//Se ha un sottomenu
		if (scope.item.figli && scope.item.figli.length >0) {
			link.attr('href', '#');
			link.data('target', '#');
			//Se è un sottomenu di primo o secondo livello
			if(parseInt(element.data("sub"))==0){
				//link.attr('role', 'button');
				link.attr('dropdown-toggle', '');
				link.data('toggle', 'dropdown');
				element.addClass("multi-level");
				//link.addClass('btn-primary');
				
				link.text(link.text()+" ");
				link.append( $('<span class="caret"></span>'));
				//var subMenu=$('<ul class="dropdown-menu" ma-sub-menu="item.figli"></ul>');
				//element.append(subMenu);
			} else if(parseInt(element.data("sub"))==1){
				//Sottomenu secondario
				element.addClass("dropdown-submenu");
			} 
			var subMenu=$('<ul class="dropdown-menu" ma-sub-menu="item.figli"></ul>');
			
			element.append(subMenu);
		}		
		//Link nuovi
		if(scope.item.type==3 && typeof scope.item.click==="undefined"){
			link.data('href', scope.item.href);
			link.attr('href', '#');
			link.addClass('new-link');
		}
		if(scope.item.type==4 && typeof scope.item.click==="undefined"){
			var ipParamRegex = /{{(.*)}}/;
			var ipVal = ipParamRegex.exec(scope.item.href);			
			var effectiveHref = scope.item.href.replace(/{{(.*)}}/, damIPs[ipVal[1]]);
			
			link.data('href', effectiveHref);
			link.attr('href', '#');
			link.addClass('external-link');
		}
		if (scope.item.click) {
			link.attr('ng-click', "item.click()");
			link.attr('href', '#');
			//element.empty();
			//var clickLink=$('<a class="btn" ng-click="'+scope.item.click+'" href={{item.href}}>{{item.descr}}</a>');
			//element.append(clickLink);
		}
		//Se ha un indirizzo vuol dire che deve essere aperto in un popup (va controllato per ultimo perchè href può essere modificato nel frattempo)
		//if(link.attr('href')!=null && link.attr('href')!=""){
		if(scope.item.type==1  && link.attr('href')!="#"){
			link.addClass('old-link');
		}
		$compile(element.contents())(scope);
	}
	return {
		restrict: 'A',
		replace: true,
		scope: {
			item: '=maMenuItem'
		},
		//template: maTemplate,
		//'<li active-link><a href={{item.href}}>{{item.title}}</a></li>',
		link: linker
	};
});