// filters js
mainModule.filter("nl2br", [ "$sce", function($sce) {
	return function(data) {
		if (!data) return data;
		return $sce.trustAsHtml(data.replace(/\n\r?/g, '<br />'));
	};
}]);