G_to_rest_max = 300;
G_to_rest_min = 100;
G_AJAX_timeout = 5000000;
G_COUNT = '';

	/*******************/
	/*** MOTORE REST ***/
	/*******************/
if(typeof damMapModule !== 'undefined') mainModule = damMapModule;
if(typeof anagrafModule !== 'undefined') mainModule = anagrafModule;
if(typeof rubricaModule !== 'undefined') mainModule = rubricaModule;
if(typeof reportBuilderModule !== 'undefined') mainModule = reportBuilderModule;
if(typeof schedaRivModule !== 'undefined') mainModule = schedaRivModule;
if(typeof classificazioneSismicaModule !== 'undefined') mainModule = classificazioneSismicaModule;
if(typeof ProgettoGestioneModule !== 'undefined') mainModule = ProgettoGestioneModule;
if(typeof TestataModule !== 'undefined') mainModule = TestataModule;
if(typeof ondePienaModule !== 'undefined') mainModule = ondePienaModule;


mainModule.factory('getREST', ['$http', '$rootScope', '$timeout', 'TokenStorage', 
	function ($http, $rootScope, $timeout, TokenStorage){
		var _obj = {};
		_obj.results = new Array();
		_obj.idList = new Array();
		_obj.semaphore = true;
		_obj.exec = function(){
			var _ct = 0;
			var _done = false;
			for(var key in this.idList){
				
				if(this.idList[key] != 0) _ct++;
				if(this.idList[key] == 1 && !_done){
					if(key != "resources/layerList.json") url = addCallback(key);
					else url = key;
					
					/* 
					$.getJSON(url, function(response, state){
						_obj.results[key] = response;
						$rootScope.$broadcast('RESTreceived');
					});
					 */
					
					$.getJSON(url,(function(_key){return function(response, state){
						_obj.results[_key] = response;
						$rootScope.$broadcast('RESTreceived');
					};})(key));
					
					_done = true;
					this.idList[key] = 2;
				}
			}
			if(_ct == 0) _ct = 1;
			var tempo = 500 / _ct;
			if(tempo < G_to_rest_min) tempo = G_to_rest_max;
			
			if(!_done){
				for(var key in this.idList){
					if(this.idList[key] == 2) this.idList[key] = 1;
				}
			}
			// tempo = 500;
			// alert(tempo);
			$timeout(function(){_obj.exec();}, tempo);
		};
		_obj.setRestID = function(_id){
			this.idList[_id] = 1;
		};
		_obj.unsetRestID = function(_id){
			this.idList[_id] = 0;
		};
		_obj.restJson = function(path){ // GET
			if(_obj.semaphore){
				_obj.semaphore = false;
				var url = addCallback(addSignature(path));
				/* 
				$.getJSON(url,(function(_path){return function(response, state){
					// G_COUNT+=_path;
					_obj.results[_path] = response;
					_obj.results[_path].newDataZZ = true;
					$rootScope.$broadcast('RESTreceived');
				};})(path));
				 */
				 
				$.ajax({
					url: url,
					dataType: 'json',
					headers: {'X-AUTH-TOKEN' : TokenStorage.retrieve()},
					success: (function(_path){return function(response, state){
						// G_COUNT+=_path;
						_obj.results[_path] = response;
						_obj.results[_path].newDataZZ = true;
						$rootScope.$broadcast('RESTreceived');
						_obj.semaphore = true;
					};})(path),
					error: function(){
						_obj.semaphore = true;
					},
					timeout: G_AJAX_timeout
				});
			}
			else{
				
				setTimeout(function(){_obj.restJson(path);}, 100);
			}
		};
		_obj.restGet = function(){
			
		};
		_obj.restPut = function(path, dati, form, fct){ // PUT
			if(_obj.semaphore){
				_obj.semaphore = false;
				$.ajax({
					url: addSignature(path),
					type: 'put',
					// data: JSON.stringify(dati),
					data: JSON.stringify(dati),
					contentType: "application/json; charset=utf-8",
					headers: {'X-AUTH-TOKEN' : TokenStorage.retrieve()},
					success: (function(_form, _fct){return function(response, state){
						
						if(typeof  _form !== 'undefined') _form.$hide();
						if(typeof  _fct !== 'undefined') _fct(response);
						_obj.semaphore = true;
						
						// var a = 0;
					};})(form, fct),
					error: function(){
						_obj.semaphore = true;
					},
					timeout: G_AJAX_timeout
				});
			}
			else{
				setTimeout(function(){_obj.restPut(path, dati, form, fct);}, 100);
			}
		};
		_obj.restPost1 = function(){
			
		};
		_obj.restPost = function(path, dati, form, refreshKeyREST, fct){ // POST
			if(_obj.semaphore){
				_obj.semaphore = false;
				// var url = addCallback(path);
				// alert(dati);
				$.ajax({
					url: addSignature(path),
					type: 'post',
					data: JSON.stringify(dati),
					contentType: "application/json; charset=utf-8",
					headers: {'X-AUTH-TOKEN' : TokenStorage.retrieve()},
					success: (function(_form, _path, _refreshKey, _fct){return function(response, state){
						
						_obj.results[_path+'/new/'+dati.id_new] = response;
						if(typeof response !== 'object') _obj.results[_path+'/new/'+dati.id_new] = {};
						_obj.results[_path+'/new/'+dati.id_new].newDataZZ = true;
						
						// _obj.results[_path+response.id] = response;
						// _obj.results[_path+response.id].newDataZZ = false;
						
						if(typeof  _fct !== 'undefined') _fct();
						$rootScope.$broadcast('RESTreceived');
						if(_form != null){
							_form.$hide();
							dati.isNew = false;
						}
						_obj.semaphore = true;
						
						if(typeof  _refreshKey !== 'undefined'){
							if(_refreshKey != '') _obj.restJson(_refreshKey);
						}
					};})(form, path, refreshKeyREST, fct),
					error: function(){
						_obj.semaphore = true;
					},
					timeout: G_AJAX_timeout
				});
			}
			else{
				setTimeout(function(){_obj.restPost(path, dati, form, refreshKeyREST, fct);}, 100);
			}
		};
		_obj.restDelete = function(path, _array, _id, fct){ // DELETE
			if(_obj.semaphore){
				_obj.semaphore = false;
				// var url = addCallback(path);
				$.ajax({
					url: addSignature(path),
					type: 'delete',
					headers: {'X-AUTH-TOKEN' : TokenStorage.retrieve()},
					success: (function(_array, _id, _fct){return function(response, state){
						for (var key in _array){
							if(_array[key].id == _id) _array.splice(key, 1);
						}
						_obj.semaphore = true;
						$rootScope.$broadcast('RESTreceived');
						if(typeof  _fct !== 'undefined') _fct();
					};})(_array, _id, fct),
					error: function(){
						_obj.semaphore = true;
					},
					timeout: G_AJAX_timeout
				});
			}
			else{
				setTimeout(function(){_obj.restDelete(path, _array, _id);}, 100);
			}
		};
		return _obj;
	}
]).factory('TokenStorage', function() {
	var storageKey = 'auth_token';
	return {		
		store : function(token) {
			return localStorage.setItem(storageKey, token);
		},
		retrieve : function() {
			var item=localStorage.getItem(storageKey);
			if(item!=null){
				return item;
			}
			return "";
		},
		clear : function() {
			return localStorage.removeItem(storageKey);
		}
	};
}).factory('TokenAuthInterceptor', function($q, TokenStorage) {
	return {
		request: function(config) {
			var authToken = TokenStorage.retrieve();
			if (authToken) {
				config.headers['X-AUTH-TOKEN'] = authToken;
				// alert("tokeb2");
			}
			return config;
		},
		responseError: function(error) {
			if (error.status === 401 || error.status === 403 || error.status === 500) {
				TokenStorage.clear();
			}
			return $q.reject(error);
		}
	};
}).config(function($httpProvider) {
	$httpProvider.interceptors.push('TokenAuthInterceptor');
});



// function jpjp(response, state){
	// alert(response);
// }

mainModule.run(function(getREST){
	getREST.exec();
});

function addCallback(_url){
	var _return = "";
	if(_url.indexOf("?") == -1) _return = _url + "?";
	else  _return = _url + "&";
	_return += "callback=?";
	return  _return;
}

function addSignature(_url){
	var _return = "";
	if(_url.indexOf("?") == -1) _return = _url + "?";
	else  _return = _url + "&";
	_return += "user="+G_userID;
	var firma = CryptoJS.SHA1("" + getWebappName() + _return + G_token);
	_return +="&signature="+firma;
	return  _return;
}

function getWebappName(){
	var pathname = window.location.pathname;
	var idx = pathname.substring(1).indexOf("/");
	if(idx == -1) return "";
	// alert(idx);
	return pathname.substring(0, idx+2);
}

	/*******************/
	/*******************/

