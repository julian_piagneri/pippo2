
G_dam_list_scope = window.opener.G_dam_list_scope;
G_report_source_scope = function(){return angular.element($("#layerList")).scope();} 


damMapModule.controller('mapLayerController', 
		['$scope', 'getREST', '$http', '$timeout', '$templateCache', 'dialogs', 'ngDialog',
          //Servizi
         'dialogService', 'storageService', 'helpersService', 
         function ($scope, getREST, $http, $timeout, $templateCache, dialogs, ngDialog,
         //Servizi
         dialogService, storageService, helpersService) {
	$scope.currentPage = 0;
	
	/*** REST ***/
	$scope.dialogService=dialogService;
	$scope.storageService=storageService;
	$scope.helpersService=helpersService;
	
	$scope.overlayListValues={};
	$scope.bufferListValues={};
	$scope.selectionListValues={};
	$scope.extraInfoFiumi={};
	
	$scope.restID = 'resources/layerList.json';
	$scope.done = false;
	
	$scope.mapLoadComplete=false;
	$scope.showAccordions=false;
	
	$scope.mouseEventOpen = true;
	$scope.mouseEventcount = 0;
	
// Prova Onde Piena
	// G_dam_list_scope().setOndaDiPiena("diga1", "onde_area", "comuni");
	// $scope.a = G_dam_list_scope().getOndaDiPiena(G_dam_list_scope().ondePienaPrm);
	// console.log($scope.a);
	// console.log(Object.keys($scope.a).length);
	// console.log(G_dam_list_scope().ondePienaPrm);
	
    // if (Object.keys($scope.a).length > 1) G_dam_map.ondeDiPiena();
///////////////////////////////////////////////////////////////////////////////////////////////
	
	$scope.showLayerControls=function(){
		$scope.showAccordions=!$scope.showAccordions;
	};	
	
	$scope.showAccordionElement1 = {
		active: false
	};	
	
	$scope.showAccordionElement2 = {
		active: false
	};
	
	$scope.getLegendStyle = function(_type, _stroke_width, _stroke_color, _fill_color,  _radius){
		var _st = {};
		_st.border = 'solid';
		_st.padding = 0;
		switch(_type){
			case 'vector':
				_st.width = '40px';
				_st.height = '20px';
				_st.borderRadius = '50%';
				_st.borderWidth = _stroke_width;
				break;
			case 'line':
				_st.borderWidth = _stroke_width/2;
				_st.marginTop = 7;
				_st.width = '40px';
				break;
			case 'point':
				// Math.PI
				var _width = 2*_radius+_stroke_width;
				_st.marginTop = 7-_stroke_width;
				_st.width = _width;
				_st.height = _width;
				_st.borderRadius = '50%';
				_st.borderWidth = _stroke_width;
				break;
			default:
				break;
		}
		_st.borderColor = _stroke_color;
		_st.background = _fill_color;
		
		return _st;
	};
	
	$scope.overlay = {
		active: false
	};
	$scope.buffer = { 
		raggio: 0, 
		active: false
	};
	$scope.feature = {
		active: false
	};
	
	$scope.filterDam = {
		active: false
	};
	
	$scope.filterLayer = {
		active: false
	};
	
	$scope.mostraCaricamento = {
		active: false
	};
	
	
	getREST.setRestID($scope.restID);
	$scope.layerList = new Array();
	$scope.layerDigList = new Array();
	$scope.overlayList = new Array();
	$scope.bufferList = new Array();
	$scope.selectionList = new Array();
	// $scope.featureList = new Array();
	$scope.layerList = new Array();
	
	$scope.latlong = {
		latlong: 0,
		lat: 0,
		long: 0
	};
	
	$scope.sisma = {
		magnitudo: 0,
		raggio: 0,
		raggioTipo1: 0,
		type: 0
	};
	
	$scope.draggedLayer = null;
	$scope.draggedOverlay = null;
	
	$scope.btnActive = {
		pan: true,
		feat: false,
		list:false,
		marker: false,
		box: false,
		circle: false,
		poly: false,
		featuredam: false
	};
	
	$scope.layersOptions = {
		zoomVisible: true,
		activeVisible: false
	};
	
	$scope.filtraDighe = function(){
		if(!$scope.filterDam.active) G_dam_list_scope().$apply(function(){G_dam_list_scope().setDamsListFromMap({});});
		else G_dam_map.selectDams4CurrentFeat();
	};
	
	$scope.filtraLayerCheck = function(){
		$scope.filterLayer.active/*  = !$scope.filterLayer.active */;
		$scope.mostraCaricamento.active/*  = !$scope.mostraCaricamento.active */;
	};
 	
	$scope.filtraLayer = function(){
		if($scope.filterLayer.active) {
			dialogService.openLoadingDialog($scope); 
			$timeout(function(){$scope.filtraLayer2();}, 500);
		}
	};
	
	$scope.filtraLayer2 = function(){
			// $scope.mostraCaricamento.active = true; 
			var valuesFromMap = G_dam_map.selectLayer4CurrentFeat();
			if (valuesFromMap === undefined) {
				dialogService.closeLoadingDialog($scope);
				// $scope.filterLayer.active = !$scope.filterLayer.active;
				// alert('Selezionare o disegnare una feature');
			}
			else
			{   
				dialogService.closeLoadingDialog($scope);
				var feature=valuesFromMap.mapValues;	
				for (var key in feature) feature[key].sort();	
				$scope.overlayListValues=feature;
				$scope.overlayListName=valuesFromMap.layerName;
				$scope.overlayListTitle=valuesFromMap.selectionName;
				dialogService.openListDialog($scope);
				 // var jsonObj =valuesFromMap.jsonObj; 
				var jsonObj = $scope.getJSON(valuesFromMap);
				$scope.mostraCaricamento.active = false; 
			}
	}; 
	
	$scope.getReportJSON = function(){
		// alert("dd");
	    // if($scope.filterLayer.active) {
			var valuesFromMap = G_dam_map.selectLayer4CurrentFeat();
			if (valuesFromMap === undefined) {
				// $scope.filterLayer.active = !$scope.filterLayer.active;
				// alert('Selezionare o disegnare una feature');
				return 1;
			}
			else
			{

				var jsonObj = $scope.getJSON(valuesFromMap);
				return jsonObj;
			}
		// }
	};
	
    $scope.openReport=function(){
		return $scope.helpersService.openNewSiteWindow('reportBuilder', {}, $scope, "");
		
	}
	
	$scope.getJSON = function (_valuesFromMap){
	   var _jsonObj = G_dam_map.mapValue2json(_valuesFromMap.selectionName, _valuesFromMap.layerName, _valuesFromMap.mapValues, _valuesFromMap.layerColumns, _valuesFromMap.mapValuesColumns);
	   return _jsonObj;
	};
	
	
	$scope.addDigLayer = function(_layer){
		_layer.isVisible = _layer.olLayer.getVisible();
		$scope.layerDigList.push(_layer); 
	};	
	
	$scope.addLayer = function(_layer){
		_layer.isVisible = _layer.olLayer.getVisible();
		// _layer.showFeature = false;
		_layer.showColumn = false;
		_layer.olLayer.getSource().legend_name = _layer.legend_name;
		$scope.layerList.push(_layer);
	};	
	
	$scope.leggiFeatures = function(_layer){
		$scope.featureList = new Array();
		$scope.currentPage=0;
		if(_layer.olLayer.getSource().showFeature) {_layer.olLayer.getSource().showFeature = !_layer.olLayer.getSource().showFeature;
		$scope.featureList = new Array();}
		else
		{
			G_dam_map.hideFeatureLayers($scope.layerList);
			_layer.olLayer.getSource().showFeature = !_layer.olLayer.getSource().showFeature;
			$scope.featureList = G_dam_map.loadFeatureInList(_layer);
		}
		return _layer.olLayer.getSource().showFeature;
	};
	
	

    // $scope.currentPage = 0;
    $scope.pageSize = 10;
  
  
    $scope.numberOfPages=function(){
		if ($scope.featureList !== undefined){
			count = $scope.featureList.length;
			var data = [];  
			while(count) {
			  data[count] = count--;
			} 
			$scope.data = data; 
			return Math.floor($scope.data.length/$scope.pageSize)+1;
        }		
	};	
	
	$scope.getSubArray = function (start, end) {
	    if ($scope.featureList !== undefined)
		return $scope.featureList.slice(start, end);
	};



    $scope.pageSizeOverList = 10;
	
    $scope.numberOfPagesOverList=function(_over){
		if (_over !== undefined){
			count = _over.length;
			var data = [];  
			while(count) {
			  data[count] = count--;
			} 
			$scope.data = data; 
			return Math.floor($scope.data.length/$scope.pageSize)+1;
        }		
	};	
	
	$scope.getSubArrayOverlayList = function (start, end, _over) {
	    if (_over !== undefined)
		return _over.slice(start, end);
	};
	
	$scope.layerOndeVisible= function(layer) {
	    layer.display = true;
		layer.olLayer.setVisible(!layer.olLayer.getVisible());
		layer.olLayer.getSource().isLoadding = false;
		layer.olLayer.getSource().showFeature = true;
	};
	
	$scope.layerVisible= function(layer) {
		layer.olLayer.setVisible(!layer.olLayer.getVisible());
		layer.olLayer.getSource().isLoadding = false;
		layer.olLayer.getSource().showFeature = false;
	};
	
	$scope.layerVisibleCheck= function(layer) {
		layer.display;
		layer.olLayer.getSource().showFeature;
	};
	
	$scope.leggiCampi = function(_layer){	
		$scope.columnList = new Array();
		if(_layer.showColumn) {_layer.showColumn = !_layer.showColumn;
		$scope.columnList = new Array();}
		else{
			G_dam_map.hideColumnLayers($scope.columnList);
			_layer.showColumn = !_layer.showColumn;
	        $scope.columnList = G_dam_map.loadColumnInList(_layer).colName;
		 }
		return _layer.showColumn;
	};
	
	$scope.selectFeatureFromList = function(_feat){
		console.log(_feat);
		G_dam_map.selectFeatureOnMap(_feat, $scope.overlay.active, $scope.buffer.active, $scope.filterDam.active);
		$scope.btnReset();
		$scope.btnActive.pan = true;
		if ($scope.filterLayer.active && !$scope.buffer.active && !$scope.overlay.active) $scope.filtraLayer();
	};
	
	$scope.addOverlay = function(_feature){
		_feature.GeometryName = _feature.get("name").replace(/&#39;/g, "'");	
		$scope.overlayList.push(_feature);
	};
	
	$scope.addSelection = function(_feature){
		_feature.GeometryName = _feature.get("name").replace(/&#39;/g, "'");
		$scope.selectionList = new Array();		
		$scope.selectionList.push(_feature);
	};

	$scope.addBuffer = function(_feature){
		_feature.GeometryName = _feature.get("name").replace(/&#39;/g, "'");	
		// _feature.GeometryName = _feature.get("name");	
		// _feature.isBuffer = true;
		$scope.bufferList = new Array();	
		$scope.bufferList.push(_feature);
	};	
	
	$scope.removeOverlay = function(_feature){
		if (!_feature)$scope.overlayList = new Array();
		var idx = $scope.overlayList.indexOf(_feature);
		$scope.overlayList.splice(idx, 1);
		G_dam_map.removeFeatureOverlay(_feature);
	};
	
	$scope.removeBuffer = function(_feature){
		if (!_feature)$scope.bufferList = new Array();
		var idx = $scope.bufferList.indexOf(_feature);
		$scope.bufferList.splice(idx, 1);
		G_dam_map.removeFeatureBuffer(_feature);
	};
	
	$scope.switchVisible = function(_olLayer){
		_olLayer.setVisible(false);
	};
	
	$scope.getDragOverlay = function(event, ui, _overl){
		$scope.draggedOverlay = _overl;
	};
	
	$scope.getDragLayer = function(event, ui, _layer){
		$scope.draggedLayer = _layer;
	};
	
	$scope.moveOverlay = function(event, ui, _overl){
		var new_index = null;
		var old_index = null;
		for(var key in $scope.overlayList){
			if($scope.overlayList[key] == _overl) new_index = key;
			if($scope.overlayList[key] == $scope.draggedOverlay) old_index = key;
		}
		
		if(new_index == null || old_index == null) return;
		$scope.overlayList.splice(new_index, 0, $scope.overlayList.splice(old_index, 1)[0]);
		
	};
	
	$scope.moveLayer = function(event, ui, _layer){
		
		if($scope.draggedLayer == null) return;
		
		G_dam_map.moveLayerPosition($scope.draggedLayer.position, _layer.position);
		
		for(var key in $scope.layerList){
			$scope.layerList[key].position = G_dam_map.getLayerPosition($scope.layerList[key].olLayer);
		}
		$scope.draggedLayer = null;
	};
	
	$scope.changeOpac = function(_layer){
		_layer.olLayer.set('opacity', parseFloat(_layer.opacity));
	};
	
	$scope.pippo = function(_val){
	};
	
	$scope.isResolution = function(_resol){
		if(_resol === undefined) return true;
		if(_resol < G_dam_map.view.getResolution()) return false;
		return true;
	};	
	
	$scope.zoomToSel = function(_feat){
		G_dam_map.zoomToSelectFeature(_feat);
	};
	
	$scope.isVisible = function(_resol, _showFeat){
	_showFeat = false;
		if(_resol === undefined) return true;
		if(_resol > G_dam_map.view.getResolution()) return false;
		return true;
	};
	
	$scope.removeSelection = function(_feature){
		if (!_feature) $scope.selectionList=new Array();
		var idx = $scope.selectionList.indexOf(_feature);
		$scope.filterDam.active = false;
		$scope.filterLayer.active = false;
		$scope.selectionList.splice(idx, 1);
		G_dam_map.removeSelectLayer();
	};
	
	$scope.removeFilter = function(){
		$scope.filterDam.active = false;
	};
	
	$scope.btnReset = function(){
		//G_dam_map.removeSelectLayer();
		G_dam_map.removeInteraction();
		$scope.btnActive.pan = false;
		$scope.btnActive.feat = false;
		$scope.btnActive.list = false;
		$scope.btnActive.marker = false;
		$scope.btnActive.box = false;
		$scope.btnActive.circle = false;
		$scope.btnActive.poly = false;
		$scope.btnActive.featuredam = false;
		$scope.mouseEventcount = 0;
		
	};
	
	$scope.overlayEvent = function(){
		if(!$scope.overlay.active) G_dam_map.hideOverlay();
		else G_dam_map.showOverlay();
	};
	
	$scope.bufferEvent = function(){
		if(!$scope.buffer.active) G_dam_map.hideBuffer();
		else G_dam_map.showBuffer();
	};
	
	$scope.selectPan = function(){
		
		$scope.btnReset();
		$scope.btnActive.pan = true;
	};
	
	$scope.selectFeat = function(){
		
		$scope.btnReset();
		$scope.btnActive.feat = true;
	};
	
	$scope.selectList = function(){
		
		$scope.btnReset();
		$scope.btnActive.list = true;
		
	};
	
	$scope.selectMarker = function(){
		G_dam_map.removeMarker();
		$scope.btnActive.marker = false;
		$scope.btnReset();
		$scope.btnActive.marker = true;
	};
	
	$scope.selectBox = function(){
		$scope.btnReset();
		$scope.btnActive.box = true;
		// if($scope.filterDam){
		// G_dam_map.removeSelectDams();
		// }
		G_dam_map.startSelectBox($scope.overlay, $scope.buffer, $scope.filterDam, $scope.filterLayer);
	};
	
	$scope.selectCircle = function(){
		$scope.btnReset();
		$scope.btnActive.circle = true;
		G_dam_map.startSelectCircle($scope.overlay, $scope.buffer, $scope.filterDam, $scope.filterLayer);
	};
	
	$scope.selectpoly = function(){
		$scope.btnReset();
		$scope.btnActive.poly = true;
		G_dam_map.startSelectPoly($scope.overlay, $scope.buffer, $scope.filterDam, $scope.filterLayer);
	};
	
	$scope.selectFeatureDam = function(){
		$scope.btnReset();
		$scope.btnActive.featuredam = true;
	};
	
	$scope.getSelectedOverlayF = function(){
		var overlayUnion = [];
		for(var idx in $scope.overlayList) if($scope.overlayList[idx].active) overlayUnion.push($scope.overlayList[idx]);
		
		return overlayUnion;
	};
	
	$scope.getSelectedBufferF = function(){
		var bufferUnion = [];
		for(var idx in $scope.bufferList) /* if($scope.bufferList[idx].active) */ bufferUnion.push($scope.bufferList[idx]);
		
		return bufferUnion;
	};
	
	$scope.removeSelectedOverlayF = function(){
		var flg_remove = true;
		while(flg_remove){
			flg_remove = false;		
			for(var idx in $scope.overlayList){
				if($scope.overlayList[idx].active){
					$scope.overlayList.splice(idx, 1);
					flg_remove = true;
				}
			}	
		}
		
	};
	
	$scope.removeSelectedBufferF = function(){
		var flg_remove = true;
		while(flg_remove){
			flg_remove = false;		
			for(var idx in $scope.bufferList){
				if($scope.bufferList[idx].active){
					$scope.bufferList.splice(idx, 1);
					flg_remove = true;
				}
			}	
		}
		
	};
	
	$scope.turf = function(_act){
	
		if(_act == 'buffer'){
			if($scope.bufferList.length>0){
			
				var radius = $scope.buffer.raggio;
				var feature = G_dam_map.turfBuffer($scope.getSelectedBufferF(), /* 2* */radius*1000);
				
				$scope.removeSelectedBufferF();
				if(feature) $scope.addBuffer(feature);
			}
		}
		else{
			if($scope.overlayList.length>1){
				
				var feature = G_dam_map.turfOverlay($scope.getSelectedOverlayF(), _act);
				if(feature) {
					$scope.removeSelectedOverlayF();
					$scope.addOverlay(feature);
					G_dam_map.OverlayLayer.addFeature(feature);
				}
			}
		}
	};

	$scope.overlayListName="";
	$scope.overlayListTitle="";
	// $scope.selectIntersectLayer = function(){
		
		// var valuesFromMap = G_dam_map.startSelectIntersectLayer($scope.getSelectedOverlay());
		// var feature=valuesFromMap.mapValues;
		
		// $scope.overlayListValues=feature;
		// $scope.overlayListName=valuesFromMap.layerName;
		// dialogService.openListDialog($scope);
	// };
	
	$scope.bufferListName="";
	// $scope.selectBufferIntersectLayer = function(){
		
		// var valuesFromMap = G_dam_map.startBufferSelectIntersectLayer($scope.getSelectedBuffer());
		// var feature=valuesFromMap.mapValues;
		
		// $scope.bufferListValues=feature;
		// $scope.bufferListName=valuesFromMap.layerName;
		// dialogService.openListDialog($scope);
	// };
	
	$scope.getSelectedOverlay = function(){
		var overlayIntersectLayer = [];
		for(var idx in $scope.overlayList) if($scope.overlayList[idx].active) overlayIntersectLayer.push($scope.overlayList[idx]);
		return overlayIntersectLayer;
	};
	
	$scope.getSelectedBuffer = function(){
		var bufferIntersectLayer = [];
		for(var idx in $scope.bufferList) /* if($scope.bufferList[idx].active) */ bufferIntersectLayer.push($scope.bufferList[idx]);
		return bufferIntersectLayer;
	};
	
	$scope.updateLatLong2Array = function(){
		$scope.latlong.latlong = new Array($scope.latlong.long, $scope.latlong.lat);
	};
	
	
	$scope.updateArray2LatLong = function(){
		$scope.latlong.lat = $scope.latlong.latlong[1].toFixed(5);
		$scope.latlong.long = $scope.latlong.latlong[0].toFixed(5);
	};
	
	$scope.calculateSisma = function(){
		G_dam_map.startCalculateSisma($scope.overlay, $scope.filterDam, $scope.latlong.long, $scope.latlong.lat, $scope.sisma.raggio, $scope.sisma.raggioTipo1, $scope.sisma.type, $scope.buffer, $scope.sisma.magnitudo);
		if($scope.filterLayer.active && !$scope.overlay.active && !$scope.buffer.active) $scope.filtraLayer();
	};
	
	$scope.calculateRadius0 = function(){
		var radius = 0.4255 * Math.pow($scope.sisma.magnitudo, 2.9403) * 1000; // in metri
		return Math.round(radius);
	};
	
	$scope.calculateRadius1 = function(){
		var radius = 0.71 * Math.pow(Math.E, 0.76 * $scope.sisma.magnitudo) * 1000; // in metri
		return Math.round(radius);
	};
	
	$scope.calculateRadius2 = function(){
		var radius = 0.2 * Math.pow(Math.E, 0.84 * $scope.sisma.magnitudo) * 1000; // in metri
		return Math.round(radius);
	};
	
	$scope.calcolaRaggio = function(){
	if($scope.sisma.type == 0)
		$scope.sisma.raggio = $scope.calculateRadius0();
	if($scope.sisma.type == 1)
		$scope.sisma.raggio = $scope.calculateRadius1();
		$scope.sisma.raggioTipo1 = $scope.calculateRadius2(); 
	if($scope.sisma.type == 2)
		$scope.sisma.raggio = $scope.calculateRadius2();
	};
	
	$scope.initMap=function(){
		$http.get(damRoutes.routeExtraInfoFiumi).success(function (data){
			$scope.extraInfoFiumi=data;
			$scope.infoFiumiPopup = $scope.convertInfoFiume2KV(data);
		});
	};
	
	$scope.Overlay2SelectLayer = function(){
		G_dam_map.Overlay2SelectLayer($scope.getSelectedOverlay(), $scope.filterDam.active, $scope.buffer.active);
		$scope.overlay.active = false;
		G_dam_map.hideOverlay();
		if($scope.filterLayer.active  && !$scope.buffer.active &&!$scope.overlay.active) $scope.filtraLayer();
		if($scope.filterDam.active) $scope.filtraDighe();
	};
	
	$scope.Buffer2SelectLayer = function(){
		G_dam_map.Buffer2SelectLayer($scope.getSelectedBuffer(), $scope.filterDam.active, $scope.overlay.active);
		$scope.buffer.active = false;
		G_dam_map.hideBuffer();
		if($scope.filterLayer.active && !$scope.overlay.active) $scope.filtraLayer();
	};
	
	$scope.convertInfoFiume2KV = function(_Info){
		var _return = $.extend(true, {}, _Info);
		for(var key in _return){
			var _ei = $scope.convertExtraInfo2KV(_return[key].extraInfo);
			for(var key2 in _ei){
				_return[key][key2] = _ei[key2];
			}
			if(_return[key].id == 22) console.log(_return[key]);
		}
		return _return;
	};
	
	$scope.convertExtraInfo2KV = function(_extraInfo){
		var _return = $.extend(true, {}, _extraInfo);
		for(var key in _return){
			var a = [];
			for(var keyVal in _return[key].values) a.push(_return[key].values[keyVal].valore);
			_return[key] = a;
			
		}
		if(_return.Lunghezza) console.log(_return);
		return _return;
	};
	
	
	$scope.$on('RESTreceived', function() {
		if(getREST.results[$scope.restID]){
			if(!$scope.done){
				$scope.done = true;
				G_dam_map = new damMapObj();
				// window.opener.G_dam_map_window = G_dam_map;
				G_dam_list_scope().$apply(function(){return G_dam_list_scope().setMap(G_dam_map);});

				$scope.layer = getREST.results[$scope.restID];
				G_dam_map.loadLayers(getREST.results[$scope.restID]);
				// G_dam_map.view.setZoom(5.5);
				// G_dam_map.view.setZoom(5.5);
				G_dam_list_scope().$apply(function(){return G_dam_list_scope().setDamList2BigMap();});
				//console.log(G_dam_list_scope().selected.entity.id);
				if (G_dam_list_scope().ondePienaPrm.active){
				    G_dam_map.view.setZoom(4);
					G_dam_map.selectDamOnGrid(G_dam_list_scope().ondePienaPrm.nomeOnda, G_dam_list_scope().mapOptions.moveMap, true);
					$scope.showAccordionElement1.active = true;
					$scope.showAccordionElement2.active = true;
				}
				else{
					G_dam_map.view.setZoom(9);
					G_dam_map.selectDamOnGrid(G_dam_list_scope().selected.entity.id, G_dam_list_scope().mapOptions.moveMap, true);
				}
				$scope.mapLoadComplete=true;
				//console.log($("#map").innerHeight());
				// $("#map").css("height", ($(window).innerHeight()-$(".TitoloScheda").outerHeight())+"px");
				$("#map").css("height", ($(window).innerHeight()-$(".header-block").outerHeight()-10)+"px");
				$("#map-buttons").css("max-height", ($("#map").innerHeight())+"px");
				//Per qualche motivo in questa sezione l'altezza è diversa
				$("#map-layers").css("max-height", ($("#map").outerHeight()-$("#map-controls").outerHeight()-38)+"px");
				G_dam_map.map.updateSize();
				//console.log($("#map-buttons").css("max-height"));
				$(window).on('resize', function(event) {
					// $("#map").css("height", ($(window).innerHeight()-$(".TitoloScheda").outerHeight())+"px");
					$("#map").css("height", ($(window).innerHeight()-$(".header-block").outerHeight()-10)+"px");
					$("#map-buttons").css("max-height", ($("#map").innerHeight())+"px");
					$("#map-layers").css("max-height", ($("#map").outerHeight()-$("#map-controls").outerHeight()-5)+"px");
					G_dam_map.map.updateSize();
					//console.log($("#map-buttons").css("max-height"));
				});
				$(G_dam_map.map.getViewport()).on('mousemove', function(evt) {
					if($scope.mouseEventOpen){
						$scope.mouseEventOpen = false;
						$scope.mouseEventcount++;
						G_dam_map.digheTooltip(evt.originalEvent);
						$scope.mouseEventOpen = true;
					}

					if($scope.btnActive.marker){
						$scope.latlong.latlong = G_dam_map.getLatLongFromEvent(evt.originalEvent);
						$scope.updateArray2LatLong();
						$scope.$apply();
					}
				});
				
				
				$(G_dam_map.map.getViewport()).on('click touchstart', function(evt) {
					if($scope.btnActive.marker){
						$scope.btnReset();
						$scope.btnActive.pan = true;
						G_dam_map.addMarker(evt.originalEvent);
						
					}
					var pixel = G_dam_map.map.getEventPixel(evt.originalEvent);
					G_dam_map.selectDamWithTooltip(evt, pixel, $scope.buffer.active);
					var feature = G_dam_map.selectLayerOnMap(pixel, $scope.overlay.active, $scope.buffer.active, $scope.filterDam.active, $scope.filterLayer.active, $scope.btnActive.feat);		
					if (feature.feature !== undefined && !$scope.overlay.active)  {$scope.btnActive.feat = false; $scope.btnActive.pan = true;}
				});
			}
			getREST.unsetRestID($scope.restID);
		}
	});
	
	
	/*** REST ***/
	
}]);


/* damMapModule.filter('startFrom', function() {
	return function(input, start) {
		start = +start; 
		return input.slice(start);
	};
});	 */
