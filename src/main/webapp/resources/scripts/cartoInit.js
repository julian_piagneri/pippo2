
var damMapModule = angular.module('damMapApp', ['ui.bootstrap', 'xeditable', 'dialogs', 'ngDragDrop', 'ngDialog', 'ngTouch', 'ngSanitize', 'frapontillo.bootstrap-switch']);

damMapModule.run(function(editableOptions) {
	editableOptions.theme = 'bs3';
});

