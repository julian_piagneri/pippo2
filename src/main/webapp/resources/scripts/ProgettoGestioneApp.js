ProgettoGestioneModule.controller('ProgettoGestioneController', ['$http', '$scope', '$filter', 'getREST', 'ngTableParams', '$rootScope', 'dialogs', 'ngDialog',
	function ($http, $scope, $filter, getREST, ngTableParams, $rootScope, dialogs, ngDialog) {
		$.extend($scope, G_Common_Scope);
		$scope.getREST = getREST;
		$scope.ngTableParams = ngTableParams;
		$scope.dialogs = dialogs;
		
		$scope.dati = {};	
		var par = {
				method: 'GET',
				url: ($scope.testataREST + "?damID=" + G_damID),
				headers: {
					'X-AUTH-TOKEN' : G_token
				}
		}
		
		$http(par).success(function(response) {
			$scope.dati = response;
		}).error(function(response) {
			
		});
		
		
		$scope.datePickerCtrl = {
			presentazione: false,
			parere: false,
			approvazione: false,
			rilievo: false,
		};
		
		
		
		$scope.save = function(_form){			
			var callBackFunction = (function(_getREST){return function(_resp){
				getREST.restJson($scope.progetto_gestioneREST + "?damID=" + G_damID);
			};})(getREST);
			
			if($scope.datiProg.dataPresentazione instanceof Date) $scope.datiProg.dataPresentazione = $scope.datiProg.dataPresentazione.toItDateString();
			if($scope.datiProg.dataParere instanceof Date) $scope.datiProg.dataParere = $scope.datiProg.dataParere.toItDateString();
			if($scope.datiProg.dataApprovazione instanceof Date) $scope.datiProg.dataApprovazione = $scope.datiProg.dataApprovazione.toItDateString();
			if($scope.datiProg.dataRilievo instanceof Date) $scope.datiProg.dataRilievo = $scope.datiProg.dataRilievo.toItDateString();
			
			getREST.restPut($scope.progetto_gestioneREST + "/" + G_damID, $scope.datiProg, _form, callBackFunction);
			
			return "";
		};
		
		$scope.datiProg = {};
		
		$scope.$on('RESTreceived', function() {
			var path = $scope.progetto_gestioneREST + "?damID=" + G_damID;
			if(getREST.results[path]) {
				if(getREST.results[path].newDataZZ) {
					$scope.datiProg = getREST.results[path];
					/* 
					$scope.datiProg.dataPresentazione = new Date($scope.datiProg.dataPresentazione.substr(0,19).replace(' ', 'T')).toItDateString();
					$scope.datiProg.dataParere = new Date($scope.datiProg.dataParere.substr(0,19).replace(' ', 'T')).toItDateString();
					$scope.datiProg.dataApprovazione = new Date($scope.datiProg.dataApprovazione.substr(0,19).replace(' ', 'T')).toItDateString();
					$scope.datiProg.dataRilievo = new Date($scope.datiProg.dataRilievo.substr(0,19).replace(' ', 'T')).toItDateString();
					
					
					$scope.datiProg.dataPresentazione = $scope.datiProg.dataPresentazione.toItDateString();
					$scope.datiProg.dataParere = $scope.datiProg.dataParere.toItDateString();
					$scope.datiProg.dataApprovazione = $scope.datiProg.dataApprovazione.toItDateString();
					$scope.datiProg.dataRilievo = $scope.datiProg.dataRilievo.toItDateString();
					
					*/
					
					getREST.results[path].newDataZZ = false;
				}
			}
		});
		
		getREST.restJson($scope.progetto_gestioneREST + "?damID=" + G_damID);
	}]
);