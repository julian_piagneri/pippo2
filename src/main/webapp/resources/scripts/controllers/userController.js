﻿
mainModule.controller('userController', 
		['$scope', '$http', 'getREST', '$timeout', 'i18nService', 'uiGridGroupingConstants', '$sce', 
		 '$templateCache', 'dialogs', 'ngDialog', '$rootScope', 'TokenStorage', '$q',
		 //Servizi
		 'mapService', 'dialogService', 'helpersService', 'storageService', 'filtersService', 'gridService',
function ($scope, $http, getREST, $timeout, i18nService, uiGridGroupingConstants, $sce, 
		$templateCache, dialogs, ngDialog, $rootScope, TokenStorage, $q,
		//Servizi
		mapService, dialogService, helpersService, storageService, filtersService, gridService
		) {
			
	$scope.mapService=mapService;
	$scope.dialogService=dialogService;
	$scope.helpersService=helpersService;
	$scope.storageService=storageService;
	$scope.filtersService=filtersService;
	$scope.gridService=gridService;
	//$scope.content='';
	i18nService.setCurrentLang('it');
	$scope.userData={};
	$scope.userDataShow={};
	$scope.isNew=typeof isNew==="undefined" ? false : isNew;
	$scope.tableIndex=0;

	//onbeforesave="checkCampoObb($data)"
	$scope.initUserPage=function(){
		$scope.dialogService.openLoadingDialog($scope);
		$(".user-data-row").each(function(index) {
			if(index%2==0){
				$(this).addClass("users-even");
			} else {
				$(this).addClass("users-odd");
			}
		});
		var userDetailsRoute=damRoutes.routeUserDetails;
		if(userId>-1){
			userDetailsRoute+="?id="+userId;
		}
		//per nuovo utente
		if(isNew){
			userDetailsRoute=damRoutes.routeNewUser;
		}
		$http.get(userDetailsRoute).success(function (data){
			$scope.setUserData(data);
			$scope.dialogService.closeLoadingDialog($scope);
		}).error(function(){
			dialogs.error("Errore", "Attenzione: Si è verificato un errore durante il recupero dei dati");
			$scope.dialogService.closeLoadingDialog($scope);
		});
	};
	
	$scope.setUserData=function(data){
		data.lastPwdUpdate=new Date(typeof data.lastPwdUpdate==="undefined" ? 0 : data.lastPwdUpdate);
		data.pwdExpireDate=new Date(typeof data.pwdExpireDate==="undefined" ? 0 : data.pwdExpireDate);
		$scope.userData=data;
		$scope.userDataShow=jQuery.extend(true, {}, data);
	};
	
	$scope.handleUserData=function(data){
		$scope.setUserData(data);
		$scope.dialogService.closeLoadingDialog($scope);
	};
	
	$scope.getDataToSend=function(data){
		var dataToSend=jQuery.extend(true, {}, $scope.userData);
		dataToSend=jQuery.extend(true, dataToSend, data);
		dataToSend.lastPwdUpdate=dataToSend.lastPwdUpdate.getTime();
		dataToSend.pwdExpireDate=dataToSend.pwdExpireDate.getTime();
		dataToSend.userDetails=jQuery.extend(true, dataToSend.userDetails, data);
		return dataToSend
	}
	
	$scope.validateUtente=function(data, editUserForm){
		$scope.dialogService.openLoadingDialog($scope);
		var d = $q.defer();
		var dataToSend=$scope.getDataToSend(data);
		$http.post(damRoutes.routeValidateUser, dataToSend).success(function(res) {
			res = res || {};
			if(res.status === 'ok') { // {status: "ok"}
				d.resolve();
			} else {
				angular.forEach(res.messages, function(message, index){
					var error="<ul>";
					angular.forEach(message, function(msg, index){
						error+="<li>"+msg+"</li>";
					});
					error+="</ul>"
					editUserForm.$setError(index, error); 
				});
				d.resolve(res.status);
				$scope.dialogService.closeLoadingDialog($scope);
			}
		}).error(function(e){
			d.reject('Server error!');
		});
		return d.promise;
	};
	
	$scope.saveUtente=function(editUserForm){
		$scope.dialogService.openLoadingDialog($scope);
		//Cambia la data con un timestamp
		var userData=jQuery.extend(true, {}, $scope.userData);
		userData.lastPwdUpdate=userData.lastPwdUpdate.getTime();
		userData.pwdExpireDate=userData.pwdExpireDate.getTime();
		//Nuovo utente
		if(typeof $scope.userData.id === 'undefined' || $scope.userData.id==null){
			$http.post(damRoutes.routeSaveUser, userData).success(function(data){
				$scope.handleUserData(data);
				var success=dialogs.notify("Successo", "Utente creato con successo");
				success.result.then(function(){
					location.href="user-edit?id="+$scope.userData.id;
				});
			});
		} else {
			//Utente esistente
			$timeout(function(){
				$http.put(damRoutes.routeSaveUser+"/"+$scope.userData.id, userData).success(function(data){
					$scope.handleUserData(data);
					dialogs.notify("Successo", "Utente modificato con successo");
				});
			});
		}
	}
}]);