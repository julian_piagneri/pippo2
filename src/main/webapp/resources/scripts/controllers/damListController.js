﻿G_report_source_scope = function () { return angular.element($("#damList")).scope(); }

G_dam_map_window = null;


mainModule.controller('damListController', 
		['$scope', '$http', 'getREST', '$timeout', 'i18nService', 'uiGridGroupingConstants', '$sce', 
		 '$templateCache', 'dialogs', 'ngDialog', '$rootScope', 'TokenStorage', '$q',
		 //Servizi
		 'mapService', 'dialogService', 'helpersService', 'storageService', 'filtersService', 'gridService',
function ($scope, $http, getREST, $timeout, i18nService, uiGridGroupingConstants, $sce, 
		$templateCache, dialogs, ngDialog, $rootScope, TokenStorage, $q,
		//Servizi
		mapService, dialogService, helpersService, storageService, filtersService, gridService
		) {
			
	$scope.mapService=mapService;
	$scope.dialogService=dialogService;
	$scope.helpersService=helpersService;
	$scope.storageService=storageService;
	$scope.filtersService=filtersService;
	
	$scope.gridService=gridService;
	
	i18nService.setCurrentLang('it');
	$scope.selectedDamName="Nessuna";
	$scope.blocSelectEvent = false;
	$scope.selected=null;
	$scope.damLang='it';
	$scope.switchSize="small";
	$scope.stateNames="saved_views"; 
	$scope.timeoutDelay=100;
	$scope.currentGridValues={};
	$scope.states={};
	$scope.showFilter={};
	$scope.mapOptions={};
	$scope.mapDamIds={};
	
	
	
	$scope.reports={};
	$scope.reportTemplates={};
	
	$scope.setMap = function(obj){
		G_dam_map_window = obj;
	};
	
//PROVE ONDE PIENA /////////////////////////////////////////////////////////////
	$scope.ondePienaPrm={
		active: false
	};
	
	$scope.getOndaDiPiena = function(_prm){
		return _prm;
	};
	
	$scope.setOndaDiPiena = function(bool, _prm, _prmLayer1, _prmLayer2){
		$scope.ondePienaPrm.active = bool;
		$scope.ondePienaPrm.nomeOnda = _prm;
		$scope.ondePienaPrm.layer1 = _prmLayer1;
		$scope.ondePienaPrm.layer2 = _prmLayer2;
	};
	// $scope.setOndaDiPiena(true, "376_", "comuni", "onde_area");
	
	
	// G_dam_map.selectDamOnOnda($scope.ondePienaPrm.nomeOnda, true, true);
////////////////////////////////////////////////////////////////////////////////
	
	$scope.waqrUrl="";
	
	// $scope.currentState=null;
	$scope.currentState={};
	// $scope.currentState.moveMap = true;
	$scope.selectFilters=[
	    {value: 0, name: "Tutte le dighe"}, 
	    {value: 1, name: "Dighe di preferenza utente"},
	    {value: 2, name: "Dighe di competenza utente"}
	];
	//Workaround per bug di ng-grid 2. chiamare selectRow fa partire l'evento due volte
	$scope.fireOnlyOnce=true;
	
	//$scope.pendingEdit=false;
	// $scope.mapOptions = {moveMap:true};
	
	// $scope.moveMap=true;
	
	$scope.openCartografico=function(){
	    $scope.setOndaDiPiena(false, "", "", "");
		// $scope.helpersService.openNewSiteWindow('cartografico', {}, $scope, 'cartoDlog', true);
		$scope.helpersService.openNewSiteWindow('cartografico', {}, $scope, 'cartoDlog', false);
		
	}
	
	$scope.setDamList2BigMap = function(){
		G_dam_map_window.addDigheLayer($scope.getRowsForMap(false));
	};
	
	
	//Salva tutte le viste
	$scope.saveAllStates=function(loadStates){
		var time=$timeout(function(){
			return $http.post(damRoutes.saveViewsList, $scope.states).error(function (result, status, headers) {
				dialogs.error("Errore", "Attenzione: Si è verificato un errore durante il salvataggio delle viste")
				$scope.dialogService.closeLoadingDialog($scope);
			}).success(function(){
				if(loadStates){
					$scope.dialogService.openLoadingDialog($scope);
					$scope.loadState(false, { selectChange:"", filtersChange:""});
				} else {
					$scope.dialogService.closeLoadingDialog($scope);
				}
			});
		}, $scope.timeoutDelay);
		return time;
	};
	
	//Bug fix per gli switch, la prima volta che vengono inizializzati fanno partire ApplyFilters che a sua volta chiama setPendingEdit
	//anche se non ci sono modifiche
//	$scope.lockEvents={selectChange:true, filtersChange:true, columnsChange:true};
//	$scope.setPendingEdit=jQuery.debounce(500, true, function(value, event){
//		//Controlla che tutti gli eventi siano liberi. Se anche un solo evento è bloccato allora non cambia il pending
//		var changePending=false;
//		if(value==true){
//			angular.forEach($scope.lockEvents, function(data, index){
//				if(data==true){
//					changePending=false;
//				}
//				//indica l'evento scatenante, essendo passato lo mette a false
//				if(index==event){
//					$scope.lockEvents[index]=false;
//				}
//			});
//		}
//		console.log("enter event: "+event+" value: "+value);
//		console.log($scope.lockEvents);
//		if(changePending){
//			console.log("not lock");
//			$timeout(function(){$scope.pendingEdit=value}, $scope.timeoutDelay);
//		}
//	});
	
//	$scope.setLockPending=function(){
//		angular.forEach($scope.lockEvents, function(data, index){
//			$scope.lockEvents[index]=true;
//		});
//	}
	
	$scope.isBaseView=function(){
		return $scope.currentState.name=="Vista Base";
	}
	
	$scope.saveState=function() {$timeout(function(){
		if($scope.isBaseView()){
			dialogs.error("Errore", "Impossibile Modificare la Vista Base");
		} else {
			$scope.currentState.status=$scope.gridApi.saveState.save();
			$scope.currentState.showFilter=jQuery.extend(true, {}, $scope.showFilter);
			$scope.currentState.mapOptions=jQuery.extend(true, {}, $scope.mapOptions);
			$scope.currentState.mapDamIds=jQuery.extend(true, {}, $scope.mapDamIds);
			//$scope.currentState.moveMap
			$scope.dialogService.openLoadingDialog($scope);
			//Dopo aver salvato uno stato non ci sono modifiche da fare
			//$scope.setPendingEdit(false);
			var statesSaved=$scope.saveAllStates(false);
			statesSaved.then(function(){
				dialogs.notify("Successo", "Vista salvata con successo"); 
				//$scope.setPendingEdit(false, "");
			});
		}
		$('#save-view').blur();
		$('#save-view').mouseout();
	}, $scope.timeoutDelay)};
	
	$scope.defaultState=function(){$timeout(function(){
		//{"columns":[{"name":"groupingRowHeaderCol","visible":false,"width":30,"sort":{},"filters":[{}]},{"name":"numeroArchivio","visible":true,"width":70,"sort":{},"filters":[{}]},{"name":"sub","visible":true,"width":50,"sort":{},"filters":[{}]},{"name":"nomeDiga","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"funzSedeCentrale","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"funzUffPerif","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"uffSedeCentrale","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"uffPeriferico","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"status1Sigla","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"status2Sigla","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"altezzaDigaL584","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"volumeTotInvasoL584","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"quotaAutoriz","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.nomeRegione","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.nomeProvincia","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.nomeComune","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.utilizzo.value.utilizzo","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.utilizzo.value.priorita","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"nomeFiumeSbarrato","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomeFiumeValle.value","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.zonaSismicaComune","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.comuneOperaPresa.value","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"nomeLago","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"bacino","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"annoConsegnaLavori","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"annoUltimazLavori","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"dataCertCollaudo","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"altezzaDigaDm","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"descrClassDiga","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"quotaMaxRegolaz","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"volumeTotInvasoDm","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"volumeLaminazione","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"volumeAutoriz","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"portMaxPienaProgetto","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"condotteForzate","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.tipoOrganoScarico.value","visible":true,"width":200,"sort":{},"filters":[{}]}],"scrollFocus":{},"selection":[],"grouping":{"grouping":[],"aggregations":[]},"treeView":{}},"moveMap":true,"showFilter":{"select":0,"rid":true,"accertamento":true,"progetto":true,"competenza":true,"altro":true}},{"name":"Dighe che iniziano per K","defaultView":0,"moveMap":true,"status":{"columns":[{"name":"groupingRowHeaderCol","visible":false,"width":30,"sort":{},"filters":[{}]},{"name":"numeroArchivio","visible":true,"width":70,"sort":{},"filters":[{}]},{"name":"sub","visible":true,"width":50,"sort":{},"filters":[{}]},{"name":"nomeDiga","visible":true,"width":200,"sort":{},"filters":[{"term":"k"}]},{"name":"funzSedeCentrale","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"funzUffPerif","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"uffSedeCentrale","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"uffPeriferico","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"status1Sigla","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"status2Sigla","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"altezzaDigaL584","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"volumeTotInvasoL584","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"quotaAutoriz","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.nomeRegione","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.nomeProvincia","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.nomeComune","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.utilizzo.value.utilizzo","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.utilizzo.value.priorita","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"nomeFiumeSbarrato","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomeFiumeValle.value","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.nomiComune.value.zonaSismicaComune","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.comuneOperaPresa.value","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"nomeLago","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"bacino","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"annoConsegnaLavori","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"annoUltimazLavori","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"dataCertCollaudo","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"altezzaDigaDm","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"descrClassDiga","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"quotaMaxRegolaz","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"volumeTotInvasoDm","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"volumeLaminazione","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"volumeAutoriz","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"portMaxPienaProgetto","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"condotteForzate","visible":true,"width":200,"sort":{},"filters":[{}]},{"name":"currentGridValues.tipoOrganoScarico.value","visible":true,"width":200,"sort":{},"filters":[{}]}],"scrollFocus":{},"selection":[{"identity":false,"row":-1}],"grouping":{"grouping":[],"aggregations":[]},"treeView":{}}
		//Rimuove il predefinito da tutte le altre viste
		angular.forEach($scope.states, function(data, index){
			data.defaultView=0;
		});
		$scope.currentState.defaultView=1;
		$scope.dialogService.openLoadingDialog($scope);
		var statesSaved=$scope.saveAllStates(false);
		statesSaved.then(function(){dialogs.notify("Successo", "Vista selezionata come predefinita");});
		$('#def-view').blur();
		$('#def-view').mouseout();
	}, $scope.timeoutDelay)};
	
    $scope.selectNewState = function(_item){
        //blocca la riattivazione dei tasti per tutti gli eventi
    	//$scope.setLockPending();
        //Disabilita i tasti per salvare in quanto non ci sono modifiche
        //$scope.setPendingEdit(false, "");
        $scope.currentState = _item;
        $scope.loadState(false, {selectChange:"selectChange", filtersChange:"filtersChange"});
    };
    

    $scope.selectNewFilter = function(_item){
        $scope.showFilter.select = _item;
        //Filtra i preferiti
        filtersService.applyFavorites($scope, "selectChange");
    };
	
    $scope.initReportBuilder = function () {
        return $scope.helpersService.openNewSiteWindow('reportBuilder', {}, $scope, "", false);
    };


    $scope.getReportJSON = function () {
        // console.log($scope.damGrid.data);
		var campi = [];
		for(var key in $scope.gridApi.grid.columns){
			if($scope.gridApi.grid.columns[key].visible && $scope.gridApi.grid.columns[key].field != "treeBaseRowHeaderCol"){
				campi.push({field:$scope.gridApi.grid.columns[key].field, displayName: $scope.gridApi.grid.columns[key].displayName});
				// console.log($scope.gridApi.grid.columns[key]);
			}
		}
		// console.log(campi);
		
		var firstRow = true;
		var layerColumns = [];
		var mapValues = [];
		var mapValuesColumns = [];
		var rows = $scope.gridApi.core.getVisibleRows($scope.damGrid);
		for(var key in rows){
		    rows[key].entity
		    var appoRiga = [];
			for (var key2 in campi) {
                if (firstRow)
                    layerColumns.push(campi[key2].displayName);
                if (campi[key2].displayName == "Nome diga")
                    mapValues.push(rows[key].entity[campi[key2].field]);

                appoRiga.push(rows[key].entity[campi[key2].field]);
			}
			firstRow = false;

			mapValuesColumns.push(appoRiga);
		}
		
		var selectionName = "Report from Home Page";
		var layerName = [];
		layerName.push("Home");
		var mapValuesContainer = [];
		mapValuesContainer.push(mapValues);
		var layerColumnsContainer=[];
		layerColumnsContainer.push(layerColumns);
		var mapValuesColumnsContainer = [];
		mapValuesColumnsContainer.push(mapValuesColumns);

		console.log(selectionName);
		console.log(layerName);
		console.log(mapValuesContainer);
		console.log(layerColumnsContainer);
		console.log(mapValuesColumnsContainer);

		var _jsonObj = $scope.mapValue2json(selectionName, layerName, mapValuesContainer, layerColumnsContainer, mapValuesColumnsContainer);
		console.log(_jsonObj);
		
		return (_jsonObj);
	};
	
    $scope.mapValue2json = function (_selection, _nameLayer, _values, _nameColumns, _valuesColumns) {
        var order = 1;
        var ArrayFeat = [];
        for (var key in _nameLayer) {
            var ArrayValue = [];
            for (var idx in _values[key]) {
                var values = {/* 'nome': _values[key][idx] */ };
                if (_nameColumns != []) {
                    for (var pos in _nameColumns[key]) {
                        values[_nameColumns[key][pos]] = _valuesColumns[key][idx][pos];
                    }
                    ArrayValue.push(values);
                }
            }
            var Layer = {
                'gruppo': _nameLayer[key],
                'colonne': _nameColumns[key],
                'righe': ArrayValue,
                'ordine': order
            };
            order = order + 1;
            ArrayFeat.push(Layer);
        }

        var jsonOut = { 'titolo': _selection, 'dati': ArrayFeat };
        jsonStr = JSON.stringify(jsonOut);
        /* console.log(jsonOut); */
        return jsonOut;
    } /**/
	
    $scope.saveReportJson=function(newReportName){
    	var report=$scope.generateReport();
    	var reportObject={jsonData:report, name:newReportName, damIds:report.damIds};
    	delete report.damIds;
    	$scope.dialogService.openLoadingDialog($scope); 
    	$http.post(damRoutes.routeSaveReports, reportObject).success(function (data){
    		$scope.dialogService.closeLoadingDialog($scope); 
    		// var success=dialogs.notify("Successo", "Report generato con successo, premere OK per visualizzarlo ");
    		/* 
			success.result.then(function(){
    			$scope.dialogService.openLoadingDialog($scope); 
    			$scope.loadExistingReports();
    			//window.open(damRoutes.routePentahoReport+"&report_id="+data);
			});
			 */
			// alert();
			// console.log(data);
			$scope.helpersService.openExternalWindow(damRoutes.routePentahoWaqr+"&report_id="+data);
    	}).error(function (result, status, headers) {
    		dialogs.error("Errore", "Si è verificato un errore durante la generazione del report");
    	});
    };
    
	$scope.loadExistingReports=function(){
		var time=$timeout(function(){
			$http.get(damRoutes.routeGetReportsByUser).success(function (data){
				$scope.reports=data;
				$scope.waqrUrl=damRoutes.routePentahoWaqr+"&from_dammap=true&token="+encodeURIComponent(TokenStorage.retrieve());
				$scope.dialogService.closeLoadingDialog($scope);
				//console.log($scope.reports);
			});
		}, $scope.timeoutDelay);
		return time;
	};
	
	$scope.loadTemplates=function(){
		var time=$timeout(function(){
			$http.get(damRoutes.routeGetReportTemplates).success(function (data){
				$scope.reportTemplates=data;
				$scope.dialogService.closeLoadingDialog($scope);
			});
		}, $scope.timeoutDelay);
		return time;
	};
	
	 $scope.openReport=function(report, template) {
		var url=damRoutes.routePentahoReport;
		if(template.url!=null && template.url!=""){
			url=template.url;
		}
	   	window.open(url+"&name="+template.template+"&report_id="+report.id+"&token="+encodeURIComponent(TokenStorage.retrieve()));
	 };
    
    $scope.generateReport=function(){
    	var report={damIds:[]};
    	report.columns=[];
    	var colValues=[];
    	//Salva le colonne ed i nomi dei valori
    	angular.forEach($scope.damGrid.columnDefs, function(column, index){
    		if(typeof column.visible==="undefined" || (typeof column.visible!=="undefined" && column.visible==true)){
    			report.columns.push(column.displayName);
    			colValues.push(column.field);
    		}
		});
    	var rows=$scope.gridApi.core.getVisibleRows($scope.damGrid);
    	//Salva le righe
    	report.values=[];
    	angular.forEach(rows, function(row, index){
    		//salva gli ID
			report.damIds.push({numeroArchivio: row.entity.numeroArchivio, sub: row.entity.sub});
    		var rowData=[];
    		angular.forEach(colValues, function(colValue, colIndex){    			
    			var currValue;
    			//è un valore che si trova nel secondo+ livello
    			if(colValue.indexOf(".") > -1){
    				var splitCol=colValue.split(".");
    				currValue={};
    				angular.forEach(splitCol, function(splitVal, splitIndex){
    					if(splitVal=="string"){
    						splitVal="value";
    					}
    					if(typeof row.entity[splitVal]!=="undefined"){
    						currValue=row.entity[splitVal];
    					}
    				});
    			} else {
    				//valore da primo livello
    				if(typeof row.entity[colValue]!=="undefined"){
    					currValue=row.entity[colValue];
    				}
    			}
    			if(typeof currValue!=="undefined"){
    				//checks if the value is an array
    				if(angular.isArray(currValue)){
    					//then gets the index for the current value
    					if(colValue.indexOf("currentGridValues") > -1){
    						//it's a special object
    						var valueSplit=colValue.split(".");
    						currValue=currValue[row.entity.currentGridValues[valueSplit[1]].index];
    						//if the value is set gets the actual value, otherwise an empty string
    						if(typeof currValue!=="undefined"){
    							currValue=currValue[valueSplit[3]];
    						} else {
    							currValue="";
    						}
    					} else {
    						currValue=currValue[row.entity.currentGridValues[colValue].index];
    						//if the value is not set puts an empty string
    						if(typeof currValue==="undefined"){
    							currValue="";
    						}
    					}
    				}
    				rowData.push(currValue);
    			} else {
    				//Adds an empty string if the value is not set
    				rowData.push("");
    			}
    		});
    		report.values.push(rowData);
		});
    	return report;
    };    
    
	$scope.loadState=function(reloadData, filtersEvents){
		var time=$timeout(function(){
			$scope.gridApi.saveState.restore($scope, $scope.currentState.status);
			// $scope.mapOptions.moveMap=$scope.currentState.moveMap;
			$scope.showFilter=jQuery.extend(true, {}, $scope.currentState.showFilter);
			$scope.mapOptions=jQuery.extend(true, {}, $scope.currentState.mapOptions);
			
			if(typeof $scope.currentState.mapDamIds!=="undefined"){
				$scope.mapDamIds=jQuery.extend(true, {}, $scope.currentState.mapDamIds);
			} else {
				$scope.mapDamIds={};
			}
			
			if(reloadData){
				return $scope.reloadDamData(filtersEvents);
			} else {
				$scope.dialogService.closeLoadingDialog($scope);
				//reloadDamData applica pure i filtri
				//$scope.applyFilters();
				//{ selectChange:true, filtersChange:true, columnsChange:true}
				$scope.filtersService.applyFilters($scope, filtersEvents.filtersChange);
				//Per lanciare l'evento del cambio di selezione
				$scope.filtersService.applyFavorites($scope, filtersEvents.selectChange);
				$scope.setDamsListFromMap({}, true);
			}
		}, $scope.timeoutDelay);
		return time;
	};
	
    $scope.saveNew=function(_newStateName, newStateForm, newState){$timeout(function(){
//    	  console.log($("#new-state-name").find("input").length);
//        console.log($("#new-state-name").find("form").length);
//        console.log($("#new-state-name").length);
//        console.log($("#new-state-name").find("span:hidden").length);
//        console.log($("#new-state-name").find("input:hidden").length);
//        console.log(_newStateName);
//        console.log(newStateForm.$editables[0].inputEl[0]);
//        console.log(newState);
//        console.log(newStateForm.$editables[0].inputEl[0].value);
        newStateForm.$editables[0].inputEl[0].value="";
        if(_newStateName==null || _newStateName=="" || _newStateName=="Vista Base"){
        	dialogs.error("Errore", "Si prega di inserire un nome per la nuova vista diverso da Vista Base");
        } else if(_newStateName.length>50){
        	dialogs.error("Errore", "Il nome della vista è troppo lungo (max 50 caratteri)");
        } else {
            var found=false;
            //Controlla se esiste già una vista col nome
            angular.forEach($scope.states, function(data, index){
                if(data.name==_newStateName){
                    found=true;
                    dialogs.error("Errore", "Esiste già una vista con questo nome");
                }
            });
            if(!found){
                var state=$scope.gridApi.saveState.save()
                $scope.currentState={
                    name: _newStateName,
                    defaultView: 0,
                    mapOptions: jQuery.extend(true, {}, $scope.mapOptions),
                    status: state,
                    showFilter: jQuery.extend(true, {}, $scope.showFilter),
                    mapDamIds: jQuery.extend(true, {}, $scope.mapDamIds)
                };
                $scope.states.push($scope.currentState);
                //Ripristina la vista di base nel caso in cui i filtri siano stati modificati
                $scope.dialogService.openLoadingDialog($scope);
    			var statesSaved=$scope.saveAllStates(false);
    			statesSaved.then(function(){
    				dialogs.notify("Successo", "Vista salvata con successo"); 
    				//$scope.setPendingEdit(false, "");
    			});
            }
        }       
    }, $scope.timeoutDelay)};
    
	$scope.deleteState=function(){$timeout(function(){
		var values=[];
		if($scope.isBaseView()){
			dialogs.error("Errore", "Impossibile Cancellare la Vista Base");
		} else {
			var confirmation = dialogs.confirm('Conferma cancellazione','Vuoi davvero cancellare questa vista?');
			confirmation.result.then(function(btn){
				angular.forEach($scope.states, function(data, index){
					if(data.name!=$scope.currentState.name){
						values.push(data);
					}
				});
				$scope.states=values;
				//Se è stata cancellata la vista di default, quella base torna ad essere la vista predefinita
				if($scope.currentState.defaultView==1){
					$scope.states[0].defaultView=1;
				}
				//Inserisce la vista base come attuale
				$scope.currentState=$scope.states[0];
				var statesSaved=$scope.saveAllStates(true);
				//statesSaved.then(function(){$scope.setPendingEdit(false, "");});
			},function(btn){
				
			}); 
			//confirm("Vuoi davvero cancellare questa vista?");
//			if(confirmation){
//				
//			}
		}
		$('#del-view').blur();
		$('#del-view').mouseout();
	}, $scope.timeoutDelay)};
	
	$scope.getMultiRowTemplate=function(valueName, entityName, arrayName, className, clickPrefix, singleValueName){
		return '<div tooltip-append-to-body="true" tooltip="{{'+clickPrefix+'helpersService.printArrayAsComma('+arrayName+', '+"'"+singleValueName+"'"+')}}" class="ui-grid-cell-contents ng-binding ng-scope row-list-container">'+
		'<div class="row-list-btn btn-group" ng-show="'+arrayName+'.length>1">'+
		'<button class="btn btn-primary btn-xs scroll-dam-btn" ng-click="'+clickPrefix+"helpersService.scrollDamList("+arrayName+", "+entityName+", '"+className+"', '0')"+'"><span class="glyphicon glyphicon-chevron-left"></span></button>'+
		'<button class="btn btn-primary btn-xs scroll-dam-btn" ng-click="'+clickPrefix+"helpersService.scrollDamList("+arrayName+", "+entityName+", '"+className+"', '1')"+'"><span class="glyphicon glyphicon-chevron-right"></span></button>'+
		'</div>'+
		'<div data-current="0" class="cell-content">'+
		'{{'+valueName+' | blankplace}}'+
		'</div>'+
		'</div>';
	};
	
	$scope.getMultiRowDetails=function(entityName, arrayName, className){
		return $sce.trustAsHtml(($scope.getMultiRowTemplate(entityName, arrayName, className)));
	};
	
	$scope.rowtpl="<div  ng-class=\"{'favorite': row.entity.preferita==true, 'ui-grid-row-header-cell': col.isRowHeader }\" ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ui-grid-cell></div>";
	
	$scope.baseView={name: "Vista Base", defaultView: 1, status: {}, 
		mapOptions: { moveMap: true, filterMap: true},
		showFilter: {select: 0, rid: true, accertamento: true, progetto: false, competenza: false, altro: false},
		mapDamIds: {}
	};
	
	//Imposta i dati di una vista 
	$scope.setViewData=function(view, data){
		//console.log(data);
		view.status=data.status;
		view.mapOptions=data.mapOptions;
		view.showFilter=data.showFilter;
		if(typeof data.mapDamIds==="undefined"){
			data.mapDamIds={};
		}
		view.mapDamIds=jQuery.extend(true, {}, data.mapDamIds);
	};
	
	$scope.selectDam = function(_NA){
		var found=false;
        angular.forEach($scope.dighe, function(data, index){
            if(data.id == _NA && !found){
				found=true;
                $timeout(function(){
                	$scope.selectRow(index, false);
				}, $scope.timeoutDelay);
            }
        });
    };

	//Inizializza i primi valori per le tabelle con valori multipli
	$scope.setCurrentValues=function(data){
		var arrayNames=["nomiComune", "nomeFiumeValle", "comuneOperaPresa", 
		"tipoOrganoScarico", "altraDenominaz", "utilizzo", "digheInvaso", "ingegneri", "concessionari", "gestori"];
		angular.forEach(data, function(value) {
			value.currentGridValues={};
			angular.forEach(arrayNames, function(arrayName) {
				value.currentGridValues[arrayName]={index:0};
				if(typeof value[arrayName][0] !=='undefined'){
					value.currentGridValues[arrayName].value=value[arrayName][0];
					//Crea la stringa per i filtri
					//value[arrayName] è un array con tutti gli oggetti di un certo tipo (tipo tutti i dati di comune provincia regione)
					angular.forEach(value[arrayName], function(arrays) {
						//Controlla se l'array di stringhe già esiste
						if(typeof value.currentGridValues[arrayName].string ==='undefined'){
							value.currentGridValues[arrayName].string={};
						}
						//Se l'oggetto non è una stringa
						if(typeof arrays !== 'undefined'){
							if(typeof arrays === 'string'){
								if(typeof value.currentGridValues[arrayName].string[arrayName] ==='undefined'){
									value.currentGridValues[arrayName].string[arrayName]="";
								}
								value.currentGridValues[arrayName].string[arrayName]+=arrays+" ";
							} else {
								//Per ogni oggetto nell'array
								angular.forEach(arrays, function(values, index) {
									if(typeof value.currentGridValues[arrayName].string[index] ==='undefined'){
										value.currentGridValues[arrayName].string[index]="";
									}
									value.currentGridValues[arrayName].string[index]+=values+" ";
								});
							}
						}
					});
				}
			});
		});
		//console.log(data);
	};
	
	$scope.selectRow=function(rowId, checkVisible){
		var currentRows=$scope.damGrid.data;
		//Se la selezione deve essere fatta solo sulle righe visibili
		if(checkVisible){
			var rows=$scope.gridApi.core.getVisibleRows($scope.damGrid);
			var currentRows=[];
			angular.forEach(rows, function(data, index){
				currentRows.push(data.entity);
			});
		}
		$scope.gridApi.selection.selectRow(currentRows[rowId]);
		$scope.gridApi.core.scrollTo(currentRows[rowId], $scope.damGrid.columnDefs[0]);
	};
	
	$scope.setRowFavorite=function(favorite){
		var idsToSend=[];
		var rows=$scope.gridApi.selection.getSelectedGridRows();
		var currentRows=[];
		//Raccoglie gli id delle dighe da mettere come favorite
		angular.forEach(rows, function(data, index){
			idsToSend.push({numeroArchivio: data.entity.numeroArchivio, sub: data.entity.sub});
			currentRows.push(data);
		});
		$scope.updateFavorites(currentRows, favorite, idsToSend);
	};
	
	$scope.updateFavorites=function(rows, favorite, idsToSend){
		//Imposta le dighe preferite
		$timeout(function(){
			$http.post(damRoutes.routeSaveFavourites+"/"+favorite, idsToSend).success(function (data){
				angular.forEach(rows, function(data, index){
					data.entity.preferita=favorite;
					if(favorite==false && $scope.showFilter.select==1){
						//Nasconde la riga rimossa dai preferiti
						//$scope.gridApi.core.setRowInvisible(data);
						data.setThisRowInvisible('select');
					}
				});
				$scope.dialogService.closeLoadingDialog($scope);
			});
		}, $scope.timeoutDelay);
	};
	
	$scope.setVisibleRowsFavorite=function(favorite){
		var confirmation = null;
		if(favorite){
			confirmation=dialogs.confirm('Conferma','Vuoi aggiungere tutte le righe visualizzate ai preferiti?');
		} else {
			confirmation = dialogs.confirm('Conferma','Vuoi rimuovere tutte le righe visualizzate dai preferiti?');
		}
		
		confirmation.result.then(function(btn){
			$scope.dialogService.openLoadingDialog($scope);
			var idsToSend=[];
			var rows=$scope.gridApi.core.getVisibleRows($scope.damGrid);
			var currentRows=[];
			//Raccoglie gli id delle dighe da mettere come favorite
			angular.forEach(rows, function(data, index){
				idsToSend.push({numeroArchivio: data.entity.numeroArchivio, sub: data.entity.sub});
				currentRows.push(data);
			});
			$scope.updateFavorites(currentRows, favorite, idsToSend);
		},function(btn){
			
		});
	};
	
	$scope.reloadDamData=function(filtersEvents){
		$scope.dialogService.openLoadingDialog($scope);
		//Trasforma i filtri in interi
		//Usa i filtri della vista base in quanto i filtri vengono applicati client side ora
		var normFilters=$scope.helpersService.boolAsIntTrue($scope.baseView.showFilter);
		//Deseleziona le righe selezionate prima di caricare i nuovi dati 
		$timeout(function(){$scope.gridApi.selection.clearSelectedRows();}, $scope.timeoutDelay);
		var time=$timeout(function(){
			return $http.get(damRoutes.routeFilterDamList+"/"+encodeURIComponent(JSON.stringify(normFilters))).success(function (data){
				$scope.setCurrentValues(data);
				$scope.dighe = data;
				$scope.damGrid.data=data;
				$timeout(function() {
					//$scope.applyFilters();
					$scope.filtersService.applyFilters($scope, filtersEvents.filtersChange);
					filtersService.applyFavorites($scope, filtersEvents.selectChange);
					$scope.applyMapFilters(false, false);
					$scope.setDamsListFromMap({}, true);
					//Seleziona la prima riga delle righe visibili
					$scope.selectRow(0, true);
					$scope.dialogService.closeLoadingDialog($scope);
				});
			});
		}, $scope.timeoutDelay);
		return time;
	};
	
	$scope.applyMapFilters=jQuery.debounce(250, function(checkOption, resetWhenOff){
		//Controlla se deve tenere conto dell'opzione
		var check=checkOption ? $scope.mapOptions.filterMap : true;
		//Se filtermap è falso allora fa apparire tutte le dighe indipendentemente dai filtri
		if(resetWhenOff && $scope.mapOptions.filterMap==false){
			G_dam_map.addDigheLayer($scope.getRowsForMap(true));
			if(G_dam_map_window) G_dam_map_window.addDigheLayer($scope.getRowsForMap(true));
		} else if(check){
		    G_dam_map.removeSelectDams();
			G_dam_map.addDigheLayer($scope.getRowsForMap(false));
			if(G_dam_map_window) G_dam_map_window.addDigheLayer($scope.getRowsForMap(false));
		}
		//Riseleziona la diga attuale
		if($scope.selected!=null){
			G_dam_map.selectDamOnGrid($scope.selected.entity.id, $scope.mapOptions.moveMap, true);
			if(G_dam_map_window) G_dam_map_window.selectDamOnGrid($scope.selected.entity.id, $scope.mapOptions.moveMap, false);
		}
	});
	
	
	$scope.triggerRowsChanged=true;
	$scope.getRowsForMap=function(showAll){
		var rows=null;
		if(!showAll){
			rows=$scope.gridApi.core.getVisibleRows($scope.damGrid);
		} else {
			rows=$scope.gridApi.grid.rows;
		}
		var currentRows=[];
		angular.forEach(rows, function(data, index){
			if(typeof data.entity.latCentrale!=="undefined" && data.entity.latCentrale!=null &&	data.entity.latCentrale!=0 &&  
			   typeof data.entity.longCentrale!=="undefined" && data.entity.longCentrale!=null && data.entity.longCentrale!=0){
				currentRows.push(data.entity);
			}
		});
		return currentRows;
	};
	
	$scope.setDamsListFromMap=function(damIds, fromHome){
		//mapDamIds
		//$scope.currentState
		//console.log(damIds);
		if(!fromHome){
			$scope.mapDamIds=damIds;
		}
		$timeout(function(){
			var rows=$scope.gridApi.grid.rows;
			angular.forEach(rows, function(data, index){
				data.clearThisRowInvisible('map');
				//Se il parametro è null allora fa solo riapparire le dighe
				if(typeof $scope.mapDamIds!== "undefined" && $scope.mapDamIds!=null && !jQuery.isEmptyObject($scope.mapDamIds)){
					if(typeof $scope.mapDamIds[data.entity.id]==="undefined"){
						data.setThisRowInvisible('map');
						//console.log("map invisible");
					}
				}
			});
		});
	};
	
	$scope.initDamList=function(){
		$scope.dialogService.openLoadingDialog($scope); 
		//Carica le viste dell'utente
		$timeout(function(){$http.get(damRoutes.viewsList).success(function (data){
			var defaultViews=$scope.setDefaultViews(data);
			//Mappe
			defaultViews.then(function(){
				$timeout(function(){
					$http.get(damRoutes.routeLayerList).success(function (data){
						$scope.mapService.setMapData(data, $scope);
						var stateLoaded=$scope.loadState(true, {selectChange:"selectChange", filtersChange:"filtersChange"});
						//{ selectChange:true, filtersChange:true, columnsChange:true}
						stateLoaded.then(function(){
							$scope.loadTemplates();
							$scope.loadExistingReports();
							console.log("Inizializzazione new-link dam list");
							//Aggiunge un evento per aprire i nuovi link
							$(document).ready(function(){
								jQuery(document).off('click', ".new-link");
								jQuery(document).on('click', ".new-link", $scope.newLinkHandler);
								jQuery(document).off('click', ".external-link");
								jQuery(document).on('click', ".external-link", $scope.externalLinkHandler);
							});
						});
					});
				}, $scope.timeoutDelay);
			});
		}, $scope.timeoutDelay)});
		//Aggiunge un evento per cambiare le dimensioni della mappa quando la finestra viene ridimensionata
		$(window).off('resize');
		$(window).on('resize', function(event) {
			//i resizable di jquery ui attivano l'evento window.resize, controlliamo che non provenga da questi
			if (!$(event.target).hasClass('ui-resizable')) {
				var damMap=$("#dam-list-map");
				var damList=$("#dam-list-list");
				var map=$("#map");
				var size=map.width();
				$scope.mapService.resizeWhenBig(damMap, damList, map, size, true);
			}
		});
	};
	
	$scope.newLinkHandler=jQuery.debounce(250, true, function (event){
		event.preventDefault();
		var href=$(this).data("href");
		//var params=$.parseJSON($(this).data("params"));
		//+"&dam="+
		var params={};
		// if(href=="rubrica" || href=="rivalutazione"){
		switch(href){
			case "rubrica":
			case "rivalutazione":
			case "sismica":
			case "frane":
			case "prog_gestione":
			case "onde_piena":
				params = {dam:$scope.selected.entity.numeroArchivio+$scope.selected.entity.sub};
			break;
		}
		$scope.helpersService.openNewSiteWindow(href, params, $scope, false);
	});
	
	$scope.externalLinkHandler=jQuery.debounce(250, true, function (event){
		event.preventDefault();
		var href=$(this).data("href");
		$scope.helpersService.openExternalWindow(href);
	});
	
	$scope.setDefaultViews=function(data){
		//Imposta lo status originale
		return $timeout(function(){
			return $http.get(damRoutes.getBaseView).success(function (baseView){
				$scope.setViewData($scope.baseView, baseView);
				$scope.states=data;
				//inizializzazione
				//$scope.states=null; //rimuovi il commento per resettare le dighe dell'utente
				if($scope.states==null){
					$scope.states=[];
				}
				//Controlla che non sia vuota, se non lo è cerca la vista predefinita e la inserisce come vista corrente
				if($scope.states.length>0){
					//Imposta i dati della vista base a quello base globale
					$scope.setViewData($scope.states[0], baseView);
					//$scope.states[0].status=baseView;
					angular.forEach($scope.states, function(data, index){
						if(data.defaultView==1){
							$scope.currentState=data;
						}
					});
				} else {
					//Altrimenti inserisce la vista base nell'array e la setta come vista corrente
					$scope.states=[$scope.baseView];
					$scope.currentState=$scope.states[0];
				}
			});
		}, $scope.timeoutDelay);
	};
	
	$scope.applyFilters=jQuery.debounce(250, function(){
		$scope.filtersService.applyFilters($scope, "filtersChange");
	});
	
	$scope.selectRowForDetails=function(row){
		$timeout(function(){
			G_dam_map.selectDamOnGrid(row.entity.id, $scope.mapOptions.moveMap, true);
			if(G_dam_map_window) G_dam_map_window.selectDamOnGrid(row.entity.id, $scope.mapOptions.moveMap, false);
			$scope.selected=row;
		}, $scope.timeoutDelay);
	};
	
	$scope.cancelRowSelection=$q.defer();
	$scope.damGrid = {
			data: 'dighe',
			enableFiltering: true,
			rowTemplate: $scope.rowtpl,
	        columnDefs: $scope.gridService.getColumns($scope),
	        groupingNullLabel: '',
			showGroupPanel: true,
			multiSelect: false,
			showGridFooter: true,
			saveFocus: false,
			enableRowSelection: true,
			enableSelectAll: false,
			noUnselect:true,
			enableGridMenu: true,
			enableRowHeaderSelection: false,
			onRegisterApi: function( gridApi ) {
				$scope.gridApi = gridApi;
				gridApi.core.on.rowsVisibleChanged($scope, jQuery.debounce(250, function(row){
//					console.log("rowsvisible l: "+$scope.gridApi.core.getVisibleRows($scope.damGrid).length);
					if($scope.triggerRowsChanged){
						$scope.applyMapFilters(true);
						console.log("rowsvisiblechanged");
					}
					//$scope.lockEvents={ selectChange:true, filtersChange:true, columnsChange:true};
				}));
				gridApi.core.on.filterChanged($scope, jQuery.debounce(250, function(row){
					//console.log("filterschanged l: "+$scope.gridApi.core.getVisibleRows($scope.damGrid).length);
					//$scope.setPendingEdit(true, "columnsChange");
					$scope.applyMapFilters(true, false);
					console.log("filterschanged");
				}));
				gridApi.selection.on.rowSelectionChanged($scope, jQuery.debounce(250, function(row){
					//console.log(row);
					//if($scope.fireOnlyOnce){
						if (row.isSelected && !$scope.blocSelectEvent){
							//console.log($scope.gridApi);
							//console.log($scope.damGrid);
							var user=$scope.storageService.get("login.user");
							//Se è un utente esterno salta la get per impostare i parametri
							if(user.external==false){
								$timeout(function(){
									//Cancella la chiamata precedente per selezionare la riga, se è ancora in corso
									$scope.cancelRowSelection.resolve("user cancelled");
									//Prepara una nuova promise per essere cancellata dalla chiamata successiva
									$scope.cancelRowSelection=$q.defer();
									$http.get(damRoutes.routeSetDamParameters+"?archivio="+row.entity.numeroArchivio+"&subarch="+row.entity.sub, { timeout: $scope.cancelRowSelection.promise }).success(function (data){
										//$scope.selectedDamName=row.entity.nomeDiga;
										//if($scope.moveMap){
										//$timeout(function(){G_dam_map.selectDamOnGrid(row.entity.id, $scope.mapOptions.moveMap)}, $scope.timeoutDelay);
										//}
										//$scope.selected=row;
										$scope.selectRowForDetails(row);
									});
								}, $scope.timeoutDelay);
							} else {
								//$timeout(function(){G_dam_map.selectDamOnGrid(row.entity.id, $scope.mapOptions.moveMap)}, $scope.timeoutDelay);
								//$scope.selected=row;
								$scope.selectRowForDetails(row);
							}
						}
					//} else {
					//	$scope.fireOnlyOnce=true;
					//}
				}));
			},
		};
}]);
	
	
	//"tipoOrganoScarico", "altraDenominaz", "utilizzo", "digheInvaso"
			/*
			*Valori non usati*
			id
			latCentrale
			longCentrale
			competenzaSnd
			quotaMax
			idInvaso
			
			*Nomi valori nella vecchia applicazione*
			N. Arc. X
			Sub X
			Nome diga X
			Ufficio periferico X
			Ufficio centrale X
			Funzionario (sede centrale) X
			Funzionario (uff. periferico) X
			Concessionario XX
			Gestore XX
			Status 1 X
			Status 2 X
			H L.584/94 X
			Vol. tot. invaso L.584/94 X
			Quota autorizzata X
			Ingegnere resp XX
			Regione X
			Provincia X
			Comune X
			Utilizzazione X
			Fiume sbarrato X
			Fiumi a valle X
			Zona sism. comune X 
			Comune Opera Presa X
			Lago X
			Bacino X
			Anno consegna lavori X
			Anno ultimazione lavori X
			Data cert. collaudo X
			H DM 24/03/82 X
			Classifica diga DM 24/03/82 X
			Quota max regolazione X
			Vol. tot. invaso DM 24/03/82 X
			Vol di laminazione X
			Volume autorizzato X
			Portata max piena X
			Condotte forzate
			Priorità X
			Tipo organo di scarico X
			*/