﻿mainModule.controller('AuthCtrl', ['$scope', '$http', '$timeout', '$templateCache', 
                                   'TokenStorage', 'dialogs', 'ngDialog', "$rootScope",
                                   //Servizi
                                   'dialogService', 'storageService', 'helpersService',
function ($scope, $http, $timeout, $templateCache, TokenStorage, dialogs, ngDialog, $rootScope, 
		//Servizi
		dialogService, storageService, helpersService) {
	$scope.dialogService=dialogService;
	$scope.storageService=storageService;
	$scope.helpersService=helpersService;
	$scope.storageService.set("login.authenticated", false);
	$scope.oldAuth=false;
	$scope.content="main";
	$scope.child={};
	$scope.loginDialog=null;
	$scope.menu="";
	$scope.loginDialog=null;
	$scope.currentRoute='';
	$scope.init = function (route, params) {
		$scope.currentRoute=damRoutes[route]+params;
		var isLogged=$scope.getUser(damRoutes[route]+params);
	};
	
	$scope.loadMenu=function(){
		$http.get(damRoutes.routeMenuGet).success(function (data){
			data.unshift({descr:'Logout', href:"#", type: 2, click:function(){$scope.logout();}}); //$parent.logout()
			data.push({descr:'Aiuto', type: 1, href: damRoutes.routeHelpSnd});
			$scope.menu=data;
		});
	}
	
	$scope.clearCreds=function(){
		TokenStorage.clear();
		$scope.storageService.set("login.authenticated", false);
		$scope.storageService.set("login.user", null);
		$scope.storageService.set("login.username", "");
		$scope.content="main";
		$scope.menu="";
	}
	
	$scope.openLoginDialog=function(clearCredentials){
		if(clearCredentials){
			$scope.clearCreds();
		}
		$scope.dialogService.openLoginDialog($scope);
	}
	
	$scope.setAuthorization=function(user, showPage){
		$scope.storageService.set("login.authenticated", true);
		$scope.storageService.set("login.username", user.username);
		$scope.storageService.set("login.user", user);
		//Imposta le preferenze di font e colori
		var styles=user.preferenze.substring(8, 13).split("_");
		var sizes=$scope.storageService.get("sizes");
		var colors=$scope.storageService.get("colors");
		$scope.storageService.set("styles.size", damRoutes.routeDynCss+"size/"+sizes[styles[0]]);
		$scope.storageService.set("styles.color", damRoutes.routeDynCss+"color/"+colors[styles[1]]);
		//console.log(styles);
		//FILECSS_12_0_N_N 
		//$scope.username = user.username;
		//$rootScope.username=user.username;
		$scope.dialogService.closeLoginDialog($scope);
		if(showPage!=''){
			//Workaround per bug di jQuery che carica <script> in modo sincrono anche durante chiamate asincrone
			//In browser di ultima generazione questo causa un avviso Deprecated poichè le chiamate sincrone sono state deprecate
			$.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
				options.async = true;
			});
			$scope.content=showPage;
		}
	}
	
	$scope.getUser = function(showPage){
		return $http.get(damRoutes.routeCurrentUser).success(function (user){
			$scope.dialogService.openLoadingDialog($scope);
			//I casi in cui un utente non è loggato è se c'è un errore recuperando l'utente oppure se il nome utente è "anonymousUser"
			if(user.username !== 'anonymousUser'){
				var expired=null;
				if(user.pwdExpired==true){
					expired=dialogs.error("ATTENZIONE", "La tua password scadrà il "+new Date(user.pwdExpireDate).toItDateString()+", si prega di aggiornarla il prima possibile");
				} else {
					expired={};
					expired.result=$timeout(function(){});
				}
				expired.result.then(function(){
					//Controlla che si sia loggati anche sulla vecchia applicazione
					var oldLogin=$scope.checkOldLogin();
					oldLogin.success(function(){
						$scope.setAuthorization(user, showPage);
						//Carica il menu
						$scope.loadMenu();
					}).error(function(){
						if(user.external==true){
							//Se l'utente non ha gruppi lo fa loggare solo nella nuova applicazione
							$scope.setAuthorization(user, showPage);
							//Carica il menu
							$scope.loadMenu();
						} else {
							//timeout serve per evitare errore "inprogress digest"
							$timeout(function(){
								var expiredOldLogin=dialogs.error("ATTENZIONE", "la sessione sulla vecchia applicazione è scaduta, si prega di effettuare di nuovo il login");
								expiredOldLogin.result.then(function(){
									$scope.openLoginDialog(true);
								});
							});
						}
					});
				});
			} else {
				$scope.openLoginDialog(true);
			}
		}).error(function(data, status, headers, config){
			if (status === 401 || status === 403 || status === 500) {
				$scope.openLoginDialog(true);
			}
		});
	}

	$scope.login = function () {
		//Workaround per un bug di angular che non aggiorna il model sull'autocomplete
		$scope.child.username=$("#un").val();
		$scope.child.password=$("#pw").val();
		var vname = $scope.child.username;
		var vpassword = $scope.child.password;
		if(vname=='' && vpassword=='') {  
			dialogs.error("ATTENZIONE","Si prega di inserire username e password");
		} else if(vname==''){
			dialogs.error("ATTENZIONE",'Il campo username è obbligatorio');
		} else if(vpassword==''){
			dialogs.error("ATTENZIONE",'Il campo password è obbligatorio');
		} else {
			$scope.dialogService.openLoadingDialog($scope);
			$http.post(damRoutes.routeLogin, { username: $scope.child.username, password: $scope.child.password })
			.success(function (result, status, headers) {
				TokenStorage.store(headers('X-AUTH-TOKEN'));
				var oldLoginCheck=$scope.checkOldLogin();
				oldLoginCheck.error(function(){
					//Fa il login solo se non si è più loggati sul vecchio sito (Caricare la pagina di login del vecchio sito effettua il logout)
					var oldLoginDo=$scope.oldLogin();
					oldLoginDo.success(function(a){
						//Infine, dopo aver fatto il login sulla vecchia applicazione controlla che sia tutto a posto
						var regexErr = /\<SCRIPT\>location\.href\=document\.URL\+\'\?Errore\=(.*)'\;\<\/SCRIPT\>/;
						// $scope.getUser($scope.currentRoute);
						if(regexErr.test(a)){
							var oldErrString = regexErr.exec(a)[1];
							$scope.dialogService.closeLoadingDialog($scope);
							dialogs.error("ATTENZIONE", "La vecchia applicazione ha ritornato l'errore: \""+oldErrString+"\"");
							$scope.openLoginDialog(true);
						}
						else $scope.getUser($scope.currentRoute);
					}).error(function(){
						//Controlla che l'errore sia dovuto ad un problema con la vecchia applicazione o al fatto che
						//è un utente esterno
						var loadedUser=$scope.getUser($scope.currentRoute);
						loadedUser.success(function(){
							var user=$scope.storageService.get("login.user");
							//Se è un utente esterno salta la get per impostare i parametri
							if(user.external==false){
								$scope.dialogService.closeLoadingDialog($scope);
								$scope.openLoginDialog(true);
								dialogs.error("ATTENZIONE", "Si è verificato un errore durante il login nella vecchia applicazione");
							}
						});
					});
				}).success(function(){
					//se si è già loggato sulla vecchia applicazione controlla che sia tutto a posto e carica la pagina
					$scope.getUser($scope.currentRoute);
					//$scope.dialogService.closeLoadingDialog($scope);
				});
			}).error(function(data, status){
				$scope.dialogService.closeLoadingDialog($scope);
				if (status === 401 || status === 500) {
					var errorMessage=$scope.helpersService.getObjectIfJson(data);
					var message="";
					// alert(1);
					// console.log(errorMessage);
					// alert(errorMessage);
					// alert(errorMessage.message);
					// alert(errorMessage.isUnauthError);
					
					//Controlla che sia un messaggio personalizzato
					// if(errorMessage!==false && typeof errorMessage.message!=="undefined" && typeof errorMessage.isUnauthErrror!=="undefined" && errorMessage.isUnauthError==true){
					if(errorMessage!==false && typeof errorMessage.message!=="undefined" && typeof errorMessage.isUnauthError!=="undefined"){
						// alert(2);
						message=errorMessage.message;
					}
					// alert(3);
					switch(status){
						case 401:
							dialogs.error("ATTENZIONE", "Impossibile effettuare il login: <br/>"+message);
							break;
						case 500:
							dialogs.error("ATTENZIONE", "Si è verificato un errore durante il login: <br/>"+message);
							// alert(4);
							break;
					}
					
				} else {
					dialogs.error("ATTENZIONE", "Si è verificato un errore durante il login");
				}
				
			});  
		} 
	};
	
	$scope.checkOldLogin = function(){
		return $http.get(damRoutes.routeCheckLogin)
		.success(function(){
			$scope.oldAuth=true;
		}).error(function(){
			$scope.oldAuth=false;
		});
	}
	
	$scope.oldLogin = function(){
		return $http.post(damRoutes.routeOldLogin+'?UserID='+$scope.child.username+"&Password="+$scope.child.password, {})
		.success(function (result, status, headers) {
			
		}).error(function(result, status, headers, config){
			//alert("Si è verificato un errore durante il login");
		});
	};

	$scope.logout = function () {
		var user=$scope.storageService.get("login.user");
		if(user.external==false){
			//Effettua il logout dall'applicazione vecchia se l'utente non è esterno
			$http.get(damRoutes.routeOldLogin, {}).success(function(){});
		}
		//Rimuove l'utente dalla lista online
		$http.get(damRoutes.routeLogout).success(function(){
			$scope.storageService.clear();
			$scope.openLoginDialog(true);
		});
	};
}]);