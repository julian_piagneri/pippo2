classificazioneSismicaModule.controller('classificazioneSismicaController', ['$http', '$scope', '$filter', 'getREST', '$rootScope', 'dialogs', 'ngDialog',
	function ($http, $scope, $filter, getREST, $rootScope, dialogs, ngDialog, TokenStorage) {
	    $.extend($scope, G_Common_Scope);
	    $scope.getREST = getREST;
	    $scope.dialogs = dialogs;
		$scope.snellezza_vulnerabilita = {};
	    // $scope.datiPericolisita = {};
	    $scope.datiPericolisita = [];
		
		$scope.pericolositaTR = {};
		$scope.pericolositaSLD = {};
		$scope.pericolositaSLC = {};
		
	    $scope.retrivePericolositaColumn = function(kind) {
	        for (var i = 0; i < $scope.datiPericolisita.length; i++) {
	            if ($scope.datiPericolisita[i].tempoDiRitorno == kind) {
	                return $scope.datiPericolisita[i];
	            }
	        }
			
			var newCol = {
				accelerazioneMassima: 0,
				fattoreAmplificazione: 0,
				tempoDiRitorno: kind
			};
			
			$scope.datiPericolisita.push(newCol);
			
	        return newCol;
	    };
		

	    //$scope.snellezzaCSS = "active";
	    //$scope.vunerabiltaCSS = "";
		
	    $scope.datiSnellezza = {};
	    $scope.datiVulnerabilita = {};
	    $scope.datiSnellezza.coeficienteSnellezzaCL = "";
	    $scope.datiSnellezza.clcritico = "";
	    $scope.datiVulnerabilita.pgacFiltramento = "";
	    $scope.datiVulnerabilita.pgacFessurazione = "";
	    $scope.datiVulnerabilita.pgacScorrimento = "";
	    $scope.tipoClassificazioneSismica = "";
	    $scope.tipiClassificazioneSismica =
            [
                { tipo: "V", text: "Vulnerabilit\340 sismica" },
                { tipo: "S", text: "Snellezza" }
	        ];
			
		$scope.setTempDiscr = function (_data) {
			// console.log(_data);
			$scope.discriminatoreTmp = _data;
		};
		
	    $scope.initDiscriminatoreTmp = function () {
			$scope.discriminatoreTmp = $scope.snellezza_vulnerabilita.discriminatore;
		}
		

	    $scope.checkSnellezza = function (_onEdit) {
			if(_onEdit){
				return ($scope.discriminatoreTmp == "S");
			}
			else{
				return ($scope.snellezza_vulnerabilita.discriminatore == "S");
			}
		};

	    $scope.checkVulnerabilita = function (_onEdit) {
			if(_onEdit){
				return ($scope.discriminatoreTmp == "V");
			}
			else{
				return ($scope.snellezza_vulnerabilita.discriminatore == "V");
			}
	    };

	    $scope.textdescriminatore = function (tipo) {
		    for (var id_tipo in $scope.tipiClassificazioneSismica) {
		        if ($scope.tipiClassificazioneSismica[id_tipo].tipo == tipo) {
		            return ($scope.tipiClassificazioneSismica[id_tipo].text);
		        }
		    }
		    return ("");
		};
		
	    $scope.saveSnellVuln = function (_form) {
			
			var callBackFunction = (function(_getREST){return function(_resp){
				getREST.restJson($scope.snellezza_vulnerabilitaREST + "?damID=" + G_damID);
			};})(getREST);
			
			getREST.restPut($scope.snellezza_vulnerabilitaREST + "/" + G_damID, $scope.snellezza_vulnerabilita, _form, callBackFunction);
			return "";
		};
		
	    $scope.savePericolosita = function (_form) {
			
			var callBackFunction = (function(_getREST){return function(_resp){
				getREST.restJson($scope.pericolisitaREST + "?damID=" + G_damID);
			};})(getREST);
			
			getREST.restPut($scope.pericolisitaREST + "/" + G_damID, $scope.datiPericolisita, _form, callBackFunction);
			return "";
		};
		
		$scope.classificazioneSismicaDiga = {};
		
		$scope.$on('RESTreceived', function () {
			var Path = $scope.pericolisitaREST + "?damID=" + G_damID;
		    if (getREST.results[Path]) {
		    	if (getREST.results[Path].newDataZZ) {
		    		$scope.datiPericolisita = getREST.results[Path];
						
		    		$scope.pericolositaTR = $scope.retrivePericolositaColumn("475");
		    		$scope.pericolositaSLD = $scope.retrivePericolositaColumn("SLD");
		    		$scope.pericolositaSLC = $scope.retrivePericolositaColumn("SLC");
						
		    		getREST.results[Path].newDataZZ = false;

		    	}
		    }

		    var Path = $scope.snellezza_vulnerabilitaREST + "?damID=" + G_damID;
		    if (getREST.results[Path]) {
		    	if (getREST.results[Path].newDataZZ) {
		    		$scope.snellezza_vulnerabilita = getREST.results[Path];
						
		    		$scope.discriminatoreTmp = $scope.snellezza_vulnerabilita.discriminatore;
		    		getREST.results[Path].newDataZZ = false;
		    	}
		    }
		    
		    var Path = $scope.classificazioneSismicaDigaREST + "?damID=" + G_damID;
		    if (getREST.results[Path]) {
		    	if (getREST.results[Path].newDataZZ) {
		    		$scope.classificazioneSismicaDiga = getREST.results[Path];
		    		getREST.results[Path].newDataZZ = false;
		    	}
		    }
		});
		
		getREST.restJson($scope.classificazioneSismicaDigaREST + "?damID=" + G_damID);
	    getREST.restJson($scope.snellezza_vulnerabilitaREST + "?damID=" + G_damID);
	    getREST.restJson($scope.pericolisitaREST + "?damID=" + G_damID);
	    
	    $scope.saveclassificazioneSismicaDiga = function (_form) {
			
			var callBackFunction = (function(_getREST){return function(_resp){
				getREST.restJson($scope.classificazioneSismicaDigaREST + "?damID=" + G_damID);
			};})(getREST);
			
			getREST.restPut($scope.classificazioneSismicaDigaREST + "/" + G_damID, $scope.classificazioneSismicaDiga, _form, callBackFunction);
			return "";
		};

	    $scope.dati = {};	
		var par = {
			method: 'GET',
			url: ($scope.testataREST + "?damID=" + G_damID),
			headers: {
				'X-AUTH-TOKEN' : G_token
			}
		}
		
		$http(par).success(function(response) {
			$scope.dati = response;
		}).error(function(response) {
			
		});
	}
]);