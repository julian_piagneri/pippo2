<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String schedaId = "283";
String percorsoMenu = "\\Gestione Tecnica\\Pericolisit&agrave; Sismica";
String titoloPagina = "Scheda Pericolisit&agrave; Sismica";
boolean canWrite=false;
boolean canRead=false;
%>
<%@ include file="user_permissions.jsp" %>
<%
canWrite=permessiClassSismica.get("canWrite");
canRead=permessiClassSismica.get("canRead");

//test
// canRead = true;
// canWrite = true;

String appName="classificazioneSismicaApp";
String controller = "classificazioneSismicaController";
%>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-grid.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/dialogs.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-table.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/xeditable.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-plain.css">
		<link rel="stylesheet" type="text/css" href="resources/style/main.css">
		<link rel="stylesheet" type="text/css" href="resources/style/text-align-responsive.css">
		<link rel="stylesheet" type="text/css" href="resources/style/damlist.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon-regular.css">
        <link rel="stylesheet" type="text/css" href="resources/style/scheda-print.css" media="print">
		<%@ include file="scripts_block.jsp" %>
		<script type="text/javascript">
			G_token = "<%=tokenREST%>";
			G_userID = "<%=userId%>";
			G_damID = '<%=request.getParameter("dam")%>'
		</script>
		<script type="text/ng-template" id="addIngTemplate"><%@ include file="popup_add_ing.jsp" %></script>
		<%@ include file="js_user_permissions.jsp" %>
		<script type="text/javascript" src="resources/scripts/classificazioneSismicaModule.js"></script>
		<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
		<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
		<script type="text/javascript" src="resources/scripts/classificazioneSismicaController.js"></script>
	</head>
	<body ng-app="<%=appName%>" id="<%=appName%>">
		<%@ include file="header-logo.jsp" %>
		<p class="TitoloScheda" align="CENTER"> <%=titoloPagina%> </p>
		<% if(canRead) { %>
			<div class="container-fluid" style="position: relative">
				<div ng-controller="<%=controller%>" class="row" id="<%=controller%>">
					<header class="col-xs-12 col-md-12">
						<div class="col-xs-12  col-md-12 text-center" style="border-top: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; margin-top: 5px; margin-bottom: 5px; padding: 5px;">	
							<jsp:include page="header.jsp"></jsp:include>
						</div>
						<!-- PARTE DELLA CLASSIFICAZIONE SISMICA DIGA -->
						<form editable-form name="classificazioneSismicaDigaForm" onaftersave="saveclassificazioneSismicaDiga(classificazioneSismicaDigaForm)">
							<div class="col-xs-12 col-md-12 text-center" style="border-top: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; margin-top: 5px; margin-bottom: 5px; padding: 5px;">
								<div class="col-xs-4 col-md4">
									Classificazione Sismica Diga:
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="col-xs-6 col-md-6">
										<span editable-text="classificazioneSismicaDiga.classificazioneSismicaDiga">
											{{classificazioneSismicaDiga.classificazioneSismicaDiga | number:3}}
										</span>
									</div>
									<div class="col-xs-6 col-md-6 print-hide" style="padding-bottom: 5px;">
										<button type="button" class="btn btn-info" ng-click="classificazioneSismicaDigaForm.$show();" ng-show="!classificazioneSismicaDigaForm.$visible " >
											<span class="glyphicon glyphicon-edit"></span>
										</button>
										<span ng-show="classificazioneSismicaDigaForm.$visible">
											<button type="submit" class="btn btn-primary" ng-disabled="classificazioneSismicaDigaForm.$waiting">
												<span class="glyphicon glyphicon-ok"></span>
											</button>
											<button type="button" class="btn btn-default" ng-disabled="classificazioneSismicaDigaForm.$waiting" ng-click="classificazioneSismicaDigaForm.$cancel()">
												<span class="glyphicon glyphicon-remove"></span>
											</button>
										</span>
									</div>
								</div>
								<span class="col-xs-4 col-md4"></span>
							</div>
						</form>
						<!-- ----------------------------------------------------------------------------------- --> 
					</header>
					<div class="col-xs-12" style="margin-top: 10px;">
						<accordion close-others="false">
							<accordion-group heading="Pericolisit&agrave; sismica">
								<%@ include file="sections/classificazione/pericolisita.jsp" %>
							</accordion-group>
							<accordion-group heading="Snellezza / Vulnerabilit&agrave; sismica">
								<%@ include file="sections/classificazione/snellezza_vulnerabilita.jsp" %>
							</accordion-group>
						</accordion>
					</div>
                    <!--<div class="col-md-12 col-xs-12 col-sm-12 text-center">
                        <button class="btn btn-primary" ng-print print-element-id="printThisElement"><i class="fa fa-print"></i> Print</button>
                        <br>
                        <br>
                    </div>-->
                    <div class="col-xs-12 text-center">
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-info print-hide" onClick="window.print();">
                                <span class="glyphicon glyphicon-print"></span>
                            </button>
                        </div>
                        <div class="col-xs-6">
                            <%@ include file="blocks/close.jsp" %>
                        </div>
                    </div>
                </div>
			</div>	
		<% } else { %>
			<script type="text/javascript">
				window.close();
			</script>
		<% } %>
	</body>
</html
>