

<p class="TitoloScheda" align="CENTER"> {{addIngTitolo}} </p>
<table ng-table="tableIngegneri" show-filter="true" class="table">
	<tr>
		<td filter="{ 'nomeCognome': 'text' }"></td>
	</tr>
	<tr ng-repeat="ingegnere in $data" ng-click="select_ingegnere(ingegnere);">
		<td>
			<table class="soggTable">
				<tr class="soggName" ng-class="{'activeSogg': !ingegnere.isC_details}">
					<td>{{ingegnere.nomeCognome}}<span ng-show="!ingegnere.isC_details"><br /></span></td>
				</tr>
				<tr ng-show="!ingegnere.isC_details">
					<td>
						<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-model="ingegnere.dataNomina" datepicker-popup="dd/MM/yyyy" is-open="ingegnere.open" ng-focus="ingegnere.open = true" ng-click="$event.stopPropagation(); ingegnere.open = true;" placeholder="Data Nomina"/>&nbsp;&nbsp;
						
						<button type="button" class="btn btn-info" ng-click="assocIng(ingegnere.id, ingegnere.dataNomina, ingAssocKeyREST)">
							<span class="glyphicon glyphicon-ok"></span>
						</button>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
