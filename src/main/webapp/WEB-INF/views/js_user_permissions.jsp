
		<script type="text/javascript">
			G_token = "<%=tokenREST%>";
			G_userID = "<%=userId%>";
			
			G_autorita_Write = new Array();
			G_concess_Write = new Array();
			G_gestore_Write = new Array();
			G_altroEnte_Write = new Array();
			<% for(String tmp : autoritaWrite){ %>
			G_autorita_Write.push('<%=tmp%>');
			<% } %>
			<% for(String tmp : concessWrite){ %>
			G_concess_Write.push('<%=tmp%>');
			<% } %>
			<% for(String tmp : gestoreWrite){ %>
			G_gestore_Write.push('<%=tmp%>');
			<% } %>
			<% for(String tmp : altroEnteWrite){ %>
			G_altroEnte_Write.push('<%=tmp%>');
			<% } %>
			
			G_canWrite = <%=permessiRubrica.get("canWrite")%>;
			G_canWriteDettagli = <%=permessiAnagrafica.get("canWriteDettagli")%>;
			G_canRead = <%=permessiRubrica.get("canRead")%>;
			if(G_canWrite) G_canWriteDettagli = true;
			
		</script>