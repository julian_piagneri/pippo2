<div class="col-xs-12" style="padding-bottom: 40px;">
	<h3>
		<div class="row table detailsTable">
			<div class="col-md-8 col-xs-12 detailsLabel">
				Periodo di ritorno TR [anni]
			</div>
			<div class="col-md-4 col-xs-6">
				<div class="col-md-4 col-xs-6 detailsLabel">TR=475</div>
				<div class="col-md-4 col-xs-6 detailsLabel">SLD</div>
				<div class="col-md-4 col-xs-6 detailsLabel">SLC</div>
			</div>
		</div>
	</h3>
	<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD;"></div>
	<div class="row table detailsTable">
		<div class="col-md-8 col-xs-12">
			<h4>Accelerazione al sito normalizata rispetto all'accelerazione di gravit&agrave; ag/g</h4>
		</div>
		<div class="col-md-4 col-xs-6">
			<div class="col-md-4 col-xs-6">{{accelerazioneMassimaTR | number:3}}</div>
			<div class="col-md-4 col-xs-6">{{accelerazioneMassimaSLD | number:3}}</div>
			<div class="col-md-4 col-xs-6">{{accelerazioneMassimaSLC | number:3}}</div>
		</div>
		<div class="col-md-8 col-xs-12">
			<h4>Valore massimo del fattore di amplificazione dello spettro in accellerazione orizzontale Fo</h4>
		</div>
		<div class="col-md-4 col-xs-6">
			<div class="col-md-4 col-xs-6">{{fattoreAmplificazioneTR | number:3}}</div>
			<div class="col-md-4 col-xs-6">{{fattoreAmplificazioneSLD | number:3}}</div>
			<div class="col-md-4 col-xs-6">{{fattoreAmplificazioneSLC | number:3}}</div>
		</div>
	</div>
</div>