<script type="text/javascript" src="resources/jquery/v2.1.1/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="resources/jquery/ui-v1.10.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="resources/jquery/ui.touch-punch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="resources/angularjs/v1.2.26/angular.min.js"></script>
<script type="text/javascript" src="resources/angularjs/table/ng-table.min.js"></script>
<script type="text/javascript" src="resources/angularjs/bootstrap-v0.12.0/ui-bootstrap-tpls-0.12.0.min.js"></script>
<!-- <script type="text/javascript" src="resources/angularjs/dialogs/dialogs.min.js"></script> -->
<script type="text/javascript" src="resources/angularjs/dialogs/dialogs-5.min.js"></script>
<script type="text/javascript" src="resources/angularjs/ngDialog/ngDialog.min.js"></script>
<script type="text/javascript" src="resources/angularjs/grid-v2.0.14/ng-grid-2.0.14.min.js"></script>
<script type="text/javascript" src="resources/angularjs/xeditable/xeditable.js"></script>
<script type="text/javascript" src="resources/scripts/bootstrap/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="resources/scripts/lib/angular-bootstrap-switch.js"></script>
<script type="text/javascript" src="resources/angularjs/angular-dragdrop/angular-dragdrop.min.js"></script>
<script type="text/javascript" src="resources/angularjs/angular-touch-v1.2.28/angular-touch.min.js"></script>
<script type="text/javascript" src="resources/cryptojs/v3.1.2/sha1.js"></script>
<!-- <script type="text/javascript" src="resources/angularjs/angular-sanitize-v1.0.4/angular-sanitize.min.js"></script> -->
<script type="text/javascript" src="resources/angularjs/sanitize/angular-sanitize.min.js"></script>
<script type="text/javascript" src="resources/angularjs/locale_it/angular-locale_it-it.js"></script>
