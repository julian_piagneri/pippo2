<% 
String title="Scheda Utente";
String percorsoMenu ="\\Utenti-Gruppi\\Gestione Utenti\\Aggiungi Utente";
String userId="{{storageService.get('login.username', '')}}";
%>
<%@ include file="include/header.jsp" %>
<body ng-controller="AuthCtrl" ng-init="init('routeUserBlock', '?isNew=true')">
<script>isNew=true;</script>
<%@ include file="include/get-userid.jsp" %>
<div ng-show="storageService.get('login.authenticated', false)">
<%@ include file="header-logo.jsp" %>
</div>
<div style="height: 70px;"></div>
<div ng-include="content"></div>
<%@ include file="include/footer.jsp" %>