<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String schedaId = "383";
String percorsoMenu = "\\Gestione Tecnica\\Fenomeni Franosi";
String titoloPagina = "Scheda Fenomeni Franosi";
boolean canWrite=false;
boolean canRead=false;
%>
<%@ include file="user_permissions.jsp" %>
<%
canWrite=permessiFenFranosi.get("canWrite");
canRead=permessiFenFranosi.get("canRead");

//test
// canRead = true;
// canWrite = true;

String appName = "fenFranosiApp";
String controller = "fenFranosiController";
%>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-grid.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/dialogs.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-table.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap-switch.css">
		<link rel="stylesheet" type="text/css" href="resources/style/xeditable.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-plain.css">
		<link rel="stylesheet" type="text/css" href="resources/style/main.css">
		<link rel="stylesheet" type="text/css" href="resources/style/text-align-responsive.css">
		<link rel="stylesheet" type="text/css" href="resources/style/damlist.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon-regular.css">
        <link rel="stylesheet" type="text/css" href="resources/style/scheda-print.css" media="print" >
		<%@ include file="scripts_block.jsp" %>
		<script type="text/javascript">
			G_token = "<%=tokenREST%>";
			G_userID = "<%=userId%>";
			G_damID = '<%=request.getParameter("dam")%>'
		</script>
		
		<script type="text/javascript" src="resources/scripts/fenFranosiInit.js"></script>
		<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
		<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
		<script type="text/javascript" src="resources/scripts/fenFranosiApp.js"></script>
	</head>
	<body ng-app="<%=appName%>" id="<%=appName%>">
		<%@ include file="header-logo.jsp" %>
		<p class="TitoloScheda" align="CENTER"> <%=titoloPagina%> </p>
		<% if(canRead) { %>
			<div class="container-fluid" style="position: relative">
				<div ng-controller="<%=controller%>" class="row" id="<%=controller%>">
					<div class="col-xs-12">
						<div class="col-xs-12 text-center" style="border-top: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; margin-top: 5px; margin-bottom: 5px; padding: 5px;">
							<header>
								<jsp:include page="header.jsp"></jsp:include>
							</header>
						</div>
					</div>
					<div class="col-xs-12 scheda-second-header" style="margin-top: 10px; margin-bottom: 10px;">
						<div class="col-xs-12">
							<div class="col-xs-12 col-sm-2 h4">
								FRANA :
							</div>
							<div class="col-xs-12 col-sm-5" ng-show="!fenFranosiSchede.length" >
								Nessuna scheda frana esistente per questa diga.
							</div>
							<div class="col-xs-12 col-sm-5" ng-show="fenFranosiSchede.length" >
								<select class="form-control" id="schedeFrane" ng-model="currentFrana" ng-change="switchSchedaById(currentFrana)">
									<option ng-selected="frana.idSchedaFenomeniFranosi == currentFrana" ng-repeat="frana in fenFranosiSchede" value="{{frana.idSchedaFenomeniFranosi}}">{{frana.descrizione}}</option>
								</select>
								
							</div>
							
							<% if(canWrite) { %>
							<div class="col-xs-12 col-sm-5 print-hide">
								<div class="col-xs-12">
									<form editable-form name="nuovaFranaForm" onaftersave="saveNuovaFrana(nuovaFranaForm);">
										<span editable-text="nomeNuovaFrana" ng-show="nuovaFranaForm.$visible" e-placeholder="Nome Nuova Frana">{{nomeNuovaFrana}}</span>
										<span ng-show="nuovaFranaForm.$visible">
											<button type="submit" class="btn btn-primary" ng-disabled="nuovaFranaForm.$waiting">
												<span class="glyphicon glyphicon-ok"></span>
											</button>
											<button type="button" class="btn btn-default" ng-disabled="nuovaFranaForm.$waiting" ng-click="nuovaFranaForm.$cancel()">
												<span class="glyphicon glyphicon-remove"></span>
											</button>
										</span>
									</form>
								</div>
								<div class="col-xs-12">
									<form editable-form name="editFranaForm" onaftersave="editNuovaFrana(editFranaForm);">
										<span editable-text="editFrana.descrizione" ng-show="editFranaForm.$visible">{{editFrana.descrizione}}</span>
										<span ng-show="editFranaForm.$visible">
											<button type="submit" class="btn btn-primary" ng-disabled="editFranaForm.$waiting">
												<span class="glyphicon glyphicon-ok"></span>
											</button>
											<button type="button" class="btn btn-default" ng-disabled="editFranaForm.$waiting" ng-click="editFranaForm.$cancel()">
												<span class="glyphicon glyphicon-remove"></span>
											</button>
										</span>
									</form>
								
								</div>
								
								<div ng-show="!nuovaFranaForm.$visible && !editFranaForm.$visible" class="col-xs-12">
									<button class="btn btn-info btn-add" ng-click="resetNomeNuovaFrana(); nuovaFranaForm.$show()">
										<span class="glyphicon glyphicon-plus"></span>
									</button>
									<button ng-show="fenFranosiSchede.length" class="btn btn-info" ng-click="initEditFrana(); editFranaForm.$show()" type="button">
										<span class="glyphicon glyphicon-edit"></span>
									</button>
									<button ng-show="fenFranosiSchede.length" class="btn btn-danger" ng-click="deleteFrana()" type="button">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</div>
							</div>
							<% } %>
						</div>
					</div>
					<div ng-show="fenFranosiSchede.length" class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
						<accordion close-others="false">
							<accordion-group heading="Posizione Frana">
								<div class="col-md-3 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="spalleDigaGr" />
											<jsp:param name="displayTitle" value="true" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="true" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
								<div class="col-md-3 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="corpoDigaGr" />
											<jsp:param name="displayTitle" value="true" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="true" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
								<div class="col-md-6 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="opereComplementariGr" />
											<jsp:param name="displayTitle" value="true" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="true" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
								<div class="col-md-6 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="spondeGr" />
											<jsp:param name="displayTitle" value="true" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="true" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
								<div class="col-md-3 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="invasoGr" />
											<jsp:param name="displayTitle" value="true" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="true" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
								<div class="col-md-3 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="livelloInvasoGr" />
											<jsp:param name="displayTitle" value="true" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="true" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
							</accordion-group>
							<accordion-group heading="Materiale">
								<div class="col-md-6 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="matRocciaGr" />
											<jsp:param name="displayTitle" value="true" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="true" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
								<div class="col-md-6 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="matScioltiGr" />
											<jsp:param name="displayTitle" value="true" />
											<jsp:param name="freccia" value="true" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="true" />
											<jsp:param name="measures" value="materialiMeasures" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
							</accordion-group>
							<accordion-group heading="Tipologia del movimento">
								<div class="col-md-12 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="movimentoGr" />
											<jsp:param name="displayTitle" value="true" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="true" />
											<jsp:param name="displaySN" value="false" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
							</accordion-group>
							<accordion-group heading="Cinematismo">
								<div class="col-md-12 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="cinematismoGr" />
											<jsp:param name="displayTitle" value="false" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="false" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
								<div class="col-md-12 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="cinematismoFrecciaGr" />
											<jsp:param name="displayTitle" value="false" />
											<jsp:param name="freccia" value="true" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="false" />
											<jsp:param name="measures" value="cinematismoMeasure" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
								<div class="col-md-12 col-xs-12 gruppo-scheda">
									<div class="col-xs-12">
										<jsp:include page="blocks/fen_franosi_gruppo.jsp" > 
											<jsp:param name="gruppo" value="cinematismoInnescataGr" />
											<jsp:param name="displayTitle" value="false" />
											<jsp:param name="freccia" value="false" />
											<jsp:param name="movimento" value="false" />
											<jsp:param name="displaySN" value="false" />
											<jsp:param name="canWrite" value="<%=canWrite%>" />
										</jsp:include>
									</div>
								</div>
							</accordion-group>
						</accordion>
					</div>
                    <div class="col-xs-12 text-center">
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-info print-hide" onClick="window.print();">
                                <span class="glyphicon glyphicon-print"></span>
                            </button>
                        </div>
                        <div class="col-xs-6">
                            <%@ include file="blocks/close.jsp" %>
                        </div>
                    </div>
				</div>
			</div>	
		<% } else { %>
			<script type="text/javascript">
				window.close();
			</script>
		<% } %>
	</body>
</html>