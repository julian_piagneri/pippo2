

<p class="TitoloScheda" align="CENTER"> {{assocTitolo}} </p>
<br />


<table class="table-assoc-container">
	<tr>
		<td data-drop="true" ng-model='dropModel' jqyoui-droppable="{multiple:true, onDrop:'assocSogg(assocMainSogg, assocKeyREST, assocDigheKeyREST)'}">
			<span class="h5">{{assocsTitolo1}}</span>
			<table ng-table="tableAssocTo" show-filter="true" class="ng-table-assoc table">
				<tr>
					<td filter="{ 'displayName': 'text' }"></td>
				</tr>
				<tr ng-repeat="to in $data">
					<td>
						<div class="draggableSogg btn btn-default btn-dad" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="assocTo" ng-hide="!to.displayName" jqyoui-draggable="{ animate: true, placeholder: 'keep', onStart:'getDragOgg(to, \'to\')'}">{{to.displayName}}</div>
					</td>
				</tr>
			</table>
		</td>
		<td data-drop="true" ng-model='dropModel' jqyoui-droppable="{multiple:true, onDrop:'delAssocSogg(assocMainSogg, assocKeyREST, assocDigheKeyREST)'}">
			<span class="h5">{{assocsTitolo2}}</span>
			<table ng-table="tableAssocFrom" show-filter="true" class="ng-table-assoc table">
				<tr>
					<td filter="{ 'displayName': 'text' }"></td>
				</tr>
				<tr ng-repeat="from in $data">
					<td>
						<div class="draggableSogg btn btn-default btn-dad" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="assocFrom" ng-hide="!from.displayName" jqyoui-draggable="{ animate: true, placeholder: 'keep', onStart:'getDragOgg(from, \'from\')'}">{{from.displayName}}</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
