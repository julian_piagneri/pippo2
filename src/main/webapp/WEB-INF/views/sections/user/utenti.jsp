<%@ include file="../../include/taglibs.jsp" %>
<sec:authorize access="hasRole('10100-S-W')" var="isUserAdmin" />
<sec:authorize access="(!${param.isNew} && hasRole('ROLE_USER')) || (${param.isNew} && ${isUserAdmin}) ">
	<div ng-controller="userController" ng-init="initUserPage()">
	<div class="col-xs-12" style="padding-bottom: 20px;">	
		<div class="table col-xs-12" style="padding: 5px;">
			<div class="table">
			<form editable-form shown="editUserForm.$visible || isNew" name="editUserForm" onbeforesave="validateUtente($data, editUserForm)" onaftersave="saveUtente($data, editUserForm)">
				<div class="soggName col-xs-12 no-margins" style="padding-bottom: 20px;">
					<div class="row">
						<div class="col-sm-1 hidden-xs"></div>
						<div class="TitoloScheda col-xs-12 col-sm-9 text-center-xs h5" style="padding-top: 0px !important;">
							Scheda Utente <span ng-show="!isNew">({{userDataShow.username}})</span>
						</div>
						<div class="col-xs-12 col-sm-1 text-center-xs text-right-sm" style="padding-top: 5px;">
							<div class="btn-sogg" ng-show="!editUserForm.$visible">
								<button e-form="editUserForm" type="button" class="btn btn-info" ng-click="editUserForm.$show(); $event.stopPropagation();">
									<span class="glyphicon glyphicon-edit"></span>
								</button>
							</div>
						</div>
						<div class="col-sm-1 hidden-xs"></div>
					</div>
					<div class="row">
						<div class="col-sm-1 hidden-xs"></div>
						<div class="col-xs-12 col-sm-10 no-padding-children no-margins" style="border: 1px solid #000000">
						<c:choose>
	   						<c:when test="${isUserAdmin}">
								<div class="col-xs-12 user-data-row">
									<div class="col-sm-3 col-xs-12 detailsLabel">Login Abilitato: 
									</div>
									<div class="col-sm-9 col-xs-12">
										<span e-name="active" editable-checkbox="userData.active">
										{{ userDataShow.active ? "Si" : "No" }}
										</span>
									</div>
								</div>
							</c:when>
						</c:choose>
							<div class="col-xs-12 user-data-row">
								<div class="col-sm-3 col-xs-12 detailsLabel">Username: </div>
								<div class="col-sm-9 col-xs-12">
								<c:choose>
	   								<c:when test="${isUserAdmin}">
										<span editable-text="userData.username" e-name="username" e-placeholder="Username">{{userDataShow.username}}</span>
									</c:when>
									<c:otherwise>
										{{userDataShow.username}}
									</c:otherwise>
								</c:choose>
								</div>
							</div>
							<div class="col-xs-12 user-data-row" ng-show="editUserForm.$visible">
								<div class="col-sm-3 col-xs-12 detailsLabel"><span ng-show="!isNew">Nuova</span> Password: </div>
								<div class="col-sm-9 col-xs-12">
									<span editable-text="userData.newPassword" e-type="password" e-name="newPassword" e-placeholder="Nuova Password">{{userDataShow.newPassword}}</span>
								</div>
							</div>
							<div class="col-xs-12 user-data-row" ng-show="editUserForm.$visible">
								<div class="col-sm-3 col-xs-12 detailsLabel">Ripeti Password: </div>
								<div class="col-sm-9 col-xs-12">
									<span editable-text="userData.repeatPassword" e-type="password" e-name="repeatPassword" e-placeholder="Ripeti Password">{{userDataShow.repeatPassword}}</span>
								</div>
							</div>
							<div class="col-xs-12 user-data-row">
								<div class="col-sm-3 col-xs-12 detailsLabel">Nome: </div>
								<div class="col-sm-9 col-xs-12">
									<span editable-text="userData.userDetails.nome" e-name="nome" e-placeholder="Nome">{{userDataShow.userDetails.nome}}</span>
								</div>
							</div>
							<div class="col-xs-12 user-data-row">
								<div class="col-sm-3 col-xs-12 detailsLabel">Cognome: </div>
								<div class="col-sm-9 col-xs-12">
									<span editable-text="userData.userDetails.cognome" e-name="cognome" e-placeholder="Cognome">{{userDataShow.userDetails.cognome}}</span>
								</div>
							</div>
							<div class="col-xs-12 user-data-row">
								<div class="col-sm-3 col-xs-12 detailsLabel">Ruolo: </div>
								<div class="col-sm-9 col-xs-12">
									<span editable-text="userData.userDetails.ruolo" e-name="ruolo" e-placeholder="Ruolo">{{userDataShow.userDetails.ruolo}}</span>
								</div>
							</div>
							<div class="col-xs-12 user-data-row">
								<div class="col-sm-3 col-xs-12 detailsLabel">Tel. Ufficio: </div>
								<div class="col-sm-9 col-xs-12">
									<span editable-text="userData.userDetails.telUfficio" e-name="telUfficio" e-placeholder="Tel. Ufficio">{{userDataShow.userDetails.telUfficio}}</span>
								</div>
							</div>
							<div class="col-xs-12 user-data-row">
								<div class="col-sm-3 col-xs-12 detailsLabel">Tel. Cellulare: </div>
								<div class="col-sm-9 col-xs-12">
									<span editable-text="userData.userDetails.telCellulare" e-name="telCellulare" e-placeholder="Tel. Cellulare">{{userDataShow.userDetails.telCellulare}}</span>
								</div>
							</div>
							<div class="col-xs-12  user-data-row">
								<div class="col-sm-3 col-xs-12 detailsLabel">Altri Recapiti: </div>
								<div class="col-sm-9 col-xs-12">
									<span editable-text="userData.userDetails.altriRecapiti" e-name="altriRecapiti" e-placeholder="Altri Recapiti">{{userDataShow.userDetails.altriRecapiti}}</span>
								</div>
							</div>
							<c:if test="${!param.isNew}">	
								<div class="col-xs-12 user-data-row">
									<div class="col-sm-3 col-xs-12 detailsLabel">Data Scadenza Password: </div>
									<div class="col-sm-9 col-xs-12">
									<c:choose>
										<c:when test="${isUserAdmin}">
											<input style="width: auto;" class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="editUserForm.$visible" ng-model="userData.pwdExpireDate" datepicker-popup="dd/MM/yyyy" is-open="editUserForm.open" ng-focus="editUserForm.open = true" ng-click="editUserForm.open = true" placeholder="Data Ultimo Aggiornamento" current-text="Oggi" clear-text="Annulla" close-text="Chiudi"/>
											<!-- <span editable-bsdate="userDataShow.pwdExpireDate" e-datepicker-popup="dd-MMMM-yyyy" e-datepicker-options="datePickerOptions" e-current-text="Oggi" e-clear-text="Annulla" e-close-text="Chiudi" e-name="pwdExpireDate" e-placeholder="Ultimo Aggiornamento Password">{{userDataShow.pwdExpireDate}}</span> -->
											<span ng-show="!editUserForm.$visible">{{userDataShow.pwdExpireDate.toItDateString()}}</span>
										</c:when>
										<c:otherwise>
											{{userDataShow.pwdExpireDate.toItDateString()}}
										</c:otherwise>
									</c:choose>													
									<div>
										La data di scadenza, a meno di interventi da parte dell'amministrazione, � impostata automaticamente a {{userDataShow.userDetails.pwdExpire}} giorni dall'ultimo aggiornamento della password.<br/>
										Avvisi di scadenza della password verranno mostrati all'utente {{userDataShow.userDetails.warningDays}} giorni prima della scadenza
									</div>
									</div>
								</div>
								<div class="col-xs-12 user-data-row">
									<div class="col-sm-3 col-xs-12 detailsLabel">Data Ultimo Aggiornamento Password: </div>
									<div class="col-sm-9 col-xs-12">
										{{userDataShow.lastPwdUpdate.toItDateString()}}												
									</div>
								</div>
							</c:if>
							<div class="col-xs-12  user-data-row">
								<div class="col-sm-3 col-xs-12 detailsLabel">Categoria: </div>
								<div class="col-sm-9 col-xs-12">
									{{userDataShow.userDetails.categoria | categoria}}
								</div>
							</div>
							<div class="col-xs-12  user-data-row">
								<div class="col-sm-3 col-xs-12 detailsLabel">Uffici: </div>
								<div class="col-sm-9 col-xs-12">
									<ul>
										<li ng-repeat="ufficio in userDataShow.userDetails.uffici">{{ufficio}}</li>
									</ul>
								</div>
							</div>
							<div class="col-xs-12 user-data-row">
							<div class="col-xs-12 detailsLabel">Descrizione</div>
							<div class="col-xs-12">
							<span editable-textarea="userData.userDetails.descrizione" e-name="descrizione" e-placeholder="Descrizione">
					           <span ng-bind-html="userDataShow.userDetails.descrizione | nl2br "></span>
	      					</span>
	      					</div>
							</div>
							<div class="col-xs-12 user-data-row" ng-show="editUserForm.$visible">
								<div style="padding-left: 5px;">
								<button e-form="editUserForm" type="submit" class="btn btn-primary" ng-disabled="editUserForm.$waiting">
									<span class="glyphicon glyphicon-ok"></span>
								</button>
								<button e-form="editUserForm" type="button" class="btn btn-default" ng-disabled="editUserForm.$waiting" ng-click="editUserForm.$cancel()">
									<span class="glyphicon glyphicon-remove"></span>
								</button>
								</div>
							</div>
						</div>
						<div class="col-sm-1 hidden-xs"></div> 
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
</sec:authorize>