<div class="col-xs-12" style="padding-bottom: 40px;">
    <!--<ul class="nav nav-pills">-->
    <form editable-form name="snellVulnForm" onaftersave="saveSnellVuln(snellVulnForm)">
		<!-- SCELTA -->
        <div class="col-xs-12 col-md-10 no-margins">
            <div class="col-xs-12 col-sm-6">
                <span class="detailsLabel">Tipo: </span>
                <span editable-select="snellezza_vulnerabilita.discriminatore" e-ng-change="setTempDiscr($data)" e-ng-options="tipoclasse.tipo as tipoclasse.text for tipoclasse in tipiClassificazioneSismica">
                    {{textdescriminatore(snellezza_vulnerabilita.discriminatore)}}
                </span>
            </div>
        </div>
		
        <div class="col-xs-12">&nbsp;</div>
		<!-- --- -->
		<!-- VULNERABILITA -->
		<span ng-show="checkVulnerabilita(snellVulnForm.$visible)">
			<div class="row table detailsTable">
				<table class="table">
					<tr class="soggName">
						<td><span class="detailsLabel">Filtramento [g]:&nbsp;</span>
							<span editable-text="snellezza_vulnerabilita.pgacFiltramento" 
								  e-placeholder="Filtramento">
							{{snellezza_vulnerabilita.pgacFiltramento | number:3}}
							</span>&nbsp;&nbsp;
						</td>
						<td><span class="detailsLabel">Fessurazione [g]:&nbsp;</span>
							<span editable-text="snellezza_vulnerabilita.pgacFessurazione" 
								  e-placeholder="Fessurazione">
							{{snellezza_vulnerabilita.pgacFessurazione | number:3}}
							</span>&nbsp;&nbsp;
						</td>
						<td><span class="detailsLabel">Scorrimento [g]:&nbsp;</span>
							<span editable-text="snellezza_vulnerabilita.pgacScorrimento" 
								  e-placeholder="Scorrimento">
							{{snellezza_vulnerabilita.pgacScorrimento | number:3}}
							</span>&nbsp;&nbsp;
						</td>
					</tr>
				</table>
			</div>
		</span>
		
		<!-- --- -->
		<!-- SNELLEZZA -->
		
		<span ng-show="checkSnellezza(snellVulnForm.$visible)">
			<div class="row table detailsTable">
				<table class="table">
					<tr class="soggName">
						<td>
							<span class="detailsLabel">Coeficiente snellezza CL:&nbsp;</span>
							<span editable-text="snellezza_vulnerabilita.coeficienteSnellezzaCL"
								  e-placeholder="coeficienteSnellezzaCL">
								{{snellezza_vulnerabilita.coeficienteSnellezzaCL | number:2}}
							</span>&nbsp;&nbsp;
						</td>
						<td>
							<span class="detailsLabel">CL critico:&nbsp;</span>
							<span editable-text="snellezza_vulnerabilita.clcritico"
								  e-placeholder="clcritico">
								{{snellezza_vulnerabilita.clcritico | number:2}}
							</span>&nbsp;&nbsp;
						</td>
					</tr>
				</table>
					
			</div>
		</span>
		<!-- --- -->
		
		<% if(canWrite) { %>
		
		<div class="col-xs-12 col-md-2 print-hide" style="padding-bottom: 5px;">
			<button type="button" class="btn btn-info" ng-click="initDiscriminatoreTmp(); snellVulnForm.$show();" ng-show="!snellVulnForm.$visible " >
				<span class="glyphicon glyphicon-edit"></span>
			</button>
			<span ng-show="snellVulnForm.$visible">
				<button type="submit" class="btn btn-primary" ng-disabled="snellVulnForm.$waiting">
					<span class="glyphicon glyphicon-ok"></span>
				</button>
				<button type="button" class="btn btn-default" ng-disabled="snellVulnForm.$waiting" ng-click="snellVulnForm.$cancel()">
					<span class="glyphicon glyphicon-remove"></span>
				</button>
			</span>
		</div>
		<% } %>
    </form>
    <!--</ul>-->
    <br />
    <br />


</div>