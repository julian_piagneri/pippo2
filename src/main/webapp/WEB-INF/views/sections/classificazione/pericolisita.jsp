<div class="col-xs-12" style="padding-bottom: 40px;">
	<div class="row table detailsTable">
		<div class="col-md-8 col-xs-12 detailsLabel h4">
			Periodo di ritorno TR [anni]
		</div>
		<div class="col-md-4 col-xs-6">
			<div class="col-md-4 col-xs-6 detailsLabel h4">TR=475</div>
			<div class="col-md-4 col-xs-6 detailsLabel h4">SLD</div>
			<div class="col-md-4 col-xs-6 detailsLabel h4">SLC</div>
		</div>
	</div>
		
	<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD;"></div>
	<div class="col-xs-12" >
		<form editable-form name="pericolositaForm" onaftersave="savePericolosita(pericolositaForm)">
			<div class="col-md-8 col-xs-12">
				<h4>Accelerazione al sito normalizata rispetto all'accelerazione di gravit&agrave; ag/g</h4>
			</div>
			<div class="col-md-4 col-xs-6">
				<div class="col-md-4 col-xs-6">
					<span editable-text="pericolositaTR.accelerazioneMassima">
						{{pericolositaTR.accelerazioneMassima | number:3}}
					</span>
				</div>
				<div class="col-md-4 col-xs-6">
					<span editable-text="pericolositaSLD.accelerazioneMassima">
						{{pericolositaSLD.accelerazioneMassima | number:3}}
					</span>
				</div>
				<div class="col-md-4 col-xs-6">
					<span editable-text="pericolositaSLC.accelerazioneMassima">
						{{pericolositaSLC.accelerazioneMassima | number:3}}
					</span>
				</div>
			</div>
			<div class="col-md-8 col-xs-12">
				<h4>Valore massimo del fattore di amplificazione dello spettro in accellerazione orizzontale Fo</h4>
			</div>
			<div class="col-md-4 col-xs-6">
				<div class="col-md-4 col-xs-6">
					<span editable-text="pericolositaTR.fattoreAmplificazione">
						{{pericolositaTR.fattoreAmplificazione | number:3}}
					</span>
				</div>
				<div class="col-md-4 col-xs-6">
					<span editable-text="pericolositaSLD.fattoreAmplificazione">
						{{pericolositaSLD.fattoreAmplificazione | number:3}}
					</span>
				</div>
				<div class="col-md-4 col-xs-6">
					<span editable-text="pericolositaSLC.fattoreAmplificazione">
						{{pericolositaSLC.fattoreAmplificazione | number:3}}
					</span>
				</div>
				
			</div>
			<% if(canWrite) { %>
			
			<div class="col-xs-12 print-hide" style="padding-bottom: 5px;">
				<button type="button" class="btn btn-info" ng-click="pericolositaForm.$show();" ng-show="!pericolositaForm.$visible " >
					<span class="glyphicon glyphicon-edit"></span>
				</button>
				<span ng-show="pericolositaForm.$visible">
					<button type="submit" class="btn btn-primary" ng-disabled="pericolositaForm.$waiting">
						<span class="glyphicon glyphicon-ok"></span>
					</button>
					<button type="button" class="btn btn-default" ng-disabled="pericolositaForm.$waiting" ng-click="pericolositaForm.$cancel()">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
				</span>
			</div>
			<% } %>
		</form>
	</div>
</div>