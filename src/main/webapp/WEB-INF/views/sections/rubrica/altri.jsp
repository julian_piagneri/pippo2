<div class="row table" style="width: auto; float: left;">
	<div ng-repeat="tipoA in tipiAltroEnte" class="col-xs-12" style="padding-bottom: 20px;">
		<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD;">{{tipoA.nome}}</div>
		<div class="table col-xs-12 soggName" style="padding: 5px;"  ng-repeat="altroEnte in altriEnti | filter: {tipoEnte: tipoA.id}" ng-class="{'active': !altroEnte.isC_details}">
			<div class="soggTable col-xs-12 ">
				<div class="soggName col-xs-12 no-margins" ng-class="{'activeSoggTd': !altroEnte.isC_details}">
					<form editable-form name="altraEnteForm" onaftersave="saveAltroEnte(altroEnte, altraEnteForm);" oncancel="removeOnEdit(altroEnte)">
						<div class="col-xs-12 col-sm-9 text-center-xs text-left-sm" ng-click="altraEnteForm.$visible ? void(0) : altroEnte.isC_details=!altroEnte.isC_details;" ng-class="{'activeSogg': !altroEnte.isC_details}">
							<div ng-show="!altraEnteForm.$visible">
								{{altroEnte.nome}}&nbsp;&nbsp;({{getNomeTipoEnte(altroEnte.tipoEnte)}})&nbsp;&nbsp;
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm print-hide" style="padding-top: 5px;">
							<div class="btn-sogg">
								<button e-form="altraEnteForm" type="button" class="btn btn-info" ng-click="altraEnteForm.$show(); $event.stopPropagation(); altroEnte.onEdit=true;" ng-show="!altroEnte.isC_details">
									<span class="glyphicon glyphicon-edit"></span>
								</button>
								<!-- button e-form="altraEnteForm" type="button" class="btn btn-danger" ng-click="deleteAltroEnte(altroEnte.id); $event.stopPropagation();" ng-show="!altroEnte.isC_details">
									<span class="glyphicon glyphicon-trash"></span>
								</button -->
							</div>
						</div>
						<% if(canWriteAnagrafica) { %>
						<div ng-show="altraEnteForm.$visible">
							<span e-form="altraEnteForm" editable-text="altroEnte.nome" e-size="50">{{altroEnte.nome}}</span>
							<br />Tipo&nbsp;Ente:&nbsp;<span editable-select="altroEnte.tipoEnte" e-ng-options="tipo.id as tipo.nome for tipo in (tipiAltroEnte | orderBy : 'nome' : false)">{{getNomeTipoEnte(altroEnte.tipoEnte)}}</span>&nbsp;&nbsp;
						</div>
						
						<span ng-show="altraEnteForm.$visible">
							<button type="submit" class="btn btn-primary" ng-disabled="altraEnteForm.$waiting">
								<span class="glyphicon glyphicon-ok"></span>
							</button>
							<button type="button" class="btn btn-default" ng-disabled="altraEnteForm.$waiting" ng-click="altraEnteForm.$cancel()">
								<span class="glyphicon glyphicon-remove"></span>
							</button>
						</span>
						<% } %>
					</form>
				</div>
			</div>
				
			<div class="col-xs-12 detailsTable no-margins no-padding-children" collapse="altroEnte.isC_details">
				<div class="col-xs-12">
					<jsp:include page="blocks/indirizzo_details_block.jsp" >
						<jsp:param name="mainArray" value="altroEnte.indirizzo" />
						<jsp:param name="regione" value="empty" />
						<jsp:param name="keyREST" value="altraEnteREST" />
						<jsp:param name="soggetto" value="altroEnte" />
						<jsp:param name="idSogg" value="altroEnte.id" />
						<jsp:param name="UtenteSoggArray" value="G_altroEnte_Write" />
						<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
					</jsp:include>
				</div>
				<div class="col-xs-12">
					<jsp:include page="blocks/contatto_details_block.jsp" >
						<jsp:param name="mainArray" value="altroEnte.contatto" />
						<jsp:param name="keyREST" value="altraEnteREST" />
						<jsp:param name="soggetto" value="altroEnte" />
						<jsp:param name="idSogg" value="altroEnte.id" />
						<jsp:param name="UtenteSoggArray" value="G_altroEnte_Write" />
						<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
					</jsp:include>
				</div>
				<div class="col-xs-12">
					<jsp:include page="blocks/data_agg_block.jsp" >
						<jsp:param name="dato" value="altroEnte" />
						<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
					</jsp:include>
				</div>
			</div>
		</div>
	</div>
</div>