<div class="row table" style="width: auto; float: left;">
	<div class="col-xs-12" style="padding-bottom: 20px;" ng-repeat="tipoA in tipiAutorita">
		<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD;">{{tipoA.descrizione}}</div>
		<div class="table col-xs-12 soggName" style="padding: 5px;" ng-repeat="autoritaPC in autorita | filter: {tipoAutorita: tipoA.id}" ng-class="{'active': !autoritaPC.isC_details}">
			<div class="soggTable col-xs-12 ">
				<div class="soggName col-xs-12 no-margins" ng-click="autoritaForm.$visible ? void(0) : autoritaPC.isC_details=!autoritaPC.isC_details;" ng-class="{'activeSogg': !autoritaPC.isC_details}">
				<form editable-form name="autoritaForm" onaftersave="saveAutorita(autoritaPC, autoritaForm);" oncancel="removeOnEdit(autoritaPC)" onshow="autoritaForm.selectedRegione = autoritaPC.nomeRegione">	
				<div class="col-xs-12 col-sm-9 text-center-xs text-left-sm" ng-class="{'activeSoggTd': !autoritaPC.isC_details}">
						<div ng-show="!autoritaForm.$visible">
							{{autoritaPC.descrizione}} ({{autoritaPC.nomeRegione}} {{autoritaPC.siglaProvincia}} {{getNomeTipoAutorita(autoritaPC.tipoAutorita)}})
						</div>
						<% if(canWriteAnagrafica) { %>
						<div class="print-hide" ng-show="autoritaForm.$visible">
							<span e-form="autoritaForm" editable-text="autoritaPC.descrizione" e-size="50">{{autoritaPC.descrizione}}</span>
							<br />Regione: <span e-ng-change="autoritaForm.selectedRegione = this.$data" editable-select="autoritaPC.nomeRegione" e-ng-options="regione.nome as regione.nome for regione in (regioni | orderBy : 'nome' : false)">{{autoritaPC.nomeRegione}}</span> 
							Provincia: <span editable-select="autoritaPC.siglaProvincia" e-ng-options="provincia.sigla as provincia.sigla for provincia in (province | filter:{nomeRegione:autoritaForm.selectedRegione, sigla:'!NN'} | orderBy : 'sigla' : false)">{{autoritaPC.siglaProvincia}}</span> 
							<br />Tipo Autorit&agrave;: <span editable-select="autoritaPC.tipoAutorita" e-ng-options="tipo.id as tipo.nome for tipo in (tipiAutorita | orderBy : 'nome' : false)">{{getNomeTipoAutorita(autoritaPC.tipoAutorita)}}</span> 
						</div>
						
						<span class="print-hide" ng-show="autoritaForm.$visible">
							<button type="submit" class="btn btn-primary" ng-disabled="autoritaForm.$waiting">
								<span class="glyphicon glyphicon-ok"></span>
							</button>
							<button type="button" class="btn btn-default" ng-disabled="autoritaForm.$waiting" ng-click="autoritaForm.$cancel()">
								<span class="glyphicon glyphicon-remove"></span>
							</button>
						</span>
						<% } %>
					</div>
					<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm print-hide" style="padding-top: 5px;">
						<% if(canWriteAnagrafica) { %>
						<div class="btn-sogg">
							<button e-form="autoritaForm" type="button" class="btn btn-info" ng-click="autoritaForm.$show(); $event.stopPropagation(); autoritaPC.onEdit=true;" ng-show="!autoritaPC.isC_details">
								<span class="glyphicon glyphicon-edit"></span>
							</button>
							<!--button e-form="autoritaForm" type="button" class="btn btn-danger" ng-click="deleteAutorita(autoritaPC.id); $event.stopPropagation();" ng-show="!autoritaPC.isC_details">
								<span class="glyphicon glyphicon-trash"></span>
							</button-->
						</div>
						<% } %>
					</div>
					</form>
				</div>
					
					<!-- td ng-class="{'activeSoggTd': !autoritaPC.isC_details}">
						<span>{{autoritaPC.descrizione}}&nbsp;&nbsp;({{autoritaPC.nomeRegione}}&nbsp;&nbsp;{{autoritaPC.siglaProvincia}}&nbsp;&nbsp;{{getNomeTipoAutorita(autoritaPC.tipoAutorita)}})&nbsp;&nbsp;</span>
					</td -->
					<div class="col-xs-12 detailsTable no-margins no-padding-children" collapse="autoritaPC.isC_details">
					<div class="col-xs-12">
						<jsp:include page="blocks/indirizzo_details_block.jsp" >
							<jsp:param name="mainArray" value="autoritaPC.indirizzo" />
							<jsp:param name="regione" value="autoritaPC.nomeRegione" />
							<jsp:param name="keyREST" value="autoritaREST" />
							<jsp:param name="soggetto" value="autoritaPC" />
							<jsp:param name="idSogg" value="autoritaPC.id" />
							<jsp:param name="UtenteSoggArray" value="G_autorita_Write" />
							<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/contatto_details_block.jsp" >
							<jsp:param name="mainArray" value="autoritaPC.contatto" />
							<jsp:param name="keyREST" value="autoritaREST" />
							<jsp:param name="soggetto" value="autoritaPC" />
							<jsp:param name="idSogg" value="autoritaPC.id" />
							<jsp:param name="UtenteSoggArray" value="G_autorita_Write" />
							<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/data_agg_block.jsp" >
							<jsp:param name="dato" value="autoritaPC" />
							<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
						</jsp:include>
					</div>
				</div>
			</div>				
		</div>
	</div>
</div>