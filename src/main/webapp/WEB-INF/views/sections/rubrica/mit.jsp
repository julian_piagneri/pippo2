<div class="col-xs-12" style="padding-bottom: 20px;">
	<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD; ">Ufficio Tecnico per le Dighe</div>
	<div class="row table detailsTable no-padding-children">
	<div class="col-xs-12 col-md-6">
		<div class="col-xs-12 col-sm-6 detailsLabel">Denominazione:</div> 
		<div class="col-xs-12 col-sm-6">{{datiRubrica.uffPerif.descrizione}}</div>
		<div class="col-xs-12 col-sm-6 detailsLabel">Indirizzo:</div>
		<div class="col-xs-12 col-sm-6">{{datiRubrica.uffPerif.indirizzo}}</div>
	</div>
	<div class="col-xs-12 col-md-6">
		<div class="col-xs-12 col-sm-6 detailsLabel">Telefono:</div> 
		<div class="col-xs-12 col-sm-6">{{datiRubrica.uffPerif.telefono}}</div>
		<div class="col-xs-12 col-sm-6 detailsLabel">Fax:</div> 
		<div class="col-xs-12 col-sm-6">{{datiRubrica.uffPerif.fax}}</div>
	</div>
	</div>
	
	<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD; ">Ufficio Sede Centrale</div>
	<div class="row table detailsTable no-padding-children">
	<div class="col-xs-12 col-md-6">
		<div class="col-xs-12 col-sm-6 detailsLabel">Denominazione:</div> 
		<div class="col-xs-12 col-sm-6">{{datiRubrica.uffCoord.descrizione}}</div>
		<div class="col-xs-12 col-sm-6 detailsLabel">Indirizzo:</div>
		<div class="col-xs-12 col-sm-6">{{datiRubrica.uffCoord.indirizzo}}</div>
	</div>
	<div class="col-xs-12 col-md-6">
		<div class="col-xs-12 col-sm-6 detailsLabel">Telefono:</div> 
		<div class="col-xs-12 col-sm-6">{{datiRubrica.uffCoord.telefono}}</div>
		<div class="col-xs-12 col-sm-6 detailsLabel">Fax:</div> 
		<div class="col-xs-12 col-sm-6">{{datiRubrica.uffCoord.fax}}</div>
	</div>
	</div>
</div>