<% 
boolean isIng = true;
%>
<div class="row table" style="width: auto; float: left;">
	<div class="col-xs-12" style="padding-bottom: 20px;">
		<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD;">Concessionari</div>
		<div class="table col-xs-12 soggName" style="padding: 5px;" ng-repeat="concessionario in concessionari" ng-class="{'active': !concessionario.isC_details}">
			<div class="soggTable col-xs-12 ">
				<div class="soggName col-xs-12 no-margins" ng-class="{'activeSogg': !concessionario.isC_details}">
					<div class="col-xs-12 col-sm-9 text-center-xs text-left-sm" ng-class="{'activeSoggTd': !concessionario.isC_details}"  ng-click="concessForm.$visible ? void(0) : concessionario.isC_details=!concessionario.isC_details;">
						<span e-form="concessForm" editable-text="concessionario.nome" onaftersave="saveConcess(concessionario, concessForm)" oncancel="removeOnEdit(concessionario);" e-size="50">{{concessionario.nome}}</span>
					</div>
					<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm print-hide" style="padding-top: 5px;">
					<% if(canWriteAnagrafica) { %>
						<div class="btn-sogg">
							<button e-form="concessForm" type="button" class="btn btn-info" ng-click="concessForm.$show(); $event.stopPropagation(); concessionario.onEdit=true;" ng-show="!concessionario.isC_details">
								<span class="glyphicon glyphicon-edit"></span>
							</button>
							<!-- button e-form="concessForm" type="button" class="btn btn-danger" ng-click="deleteConcess(concessionario.id); $event.stopPropagation();" ng-show="!concessionario.isC_details">
								<span class="glyphicon glyphicon-trash"></span>
							</button -->
						</div>
					<% } %>
					</div>
				</div>
				
				<div class="col-xs-12 detailsTable no-margins no-padding-children" collapse="concessionario.isC_details">
					<div class="col-xs-12">
						<jsp:include page="blocks/indirizzo_details_block.jsp" >
							<jsp:param name="mainArray" value="concessionario.indirizzo" />
							<jsp:param name="regione" value="empty" />
							<jsp:param name="keyREST" value="concessionarioREST" />
							<jsp:param name="soggetto" value="concessionario" />
							<jsp:param name="idSogg" value="concessionario.id" />
							<jsp:param name="UtenteSoggArray" value="G_concess_Write" />
							<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />													
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/contatto_details_block.jsp" >
							<jsp:param name="mainArray" value="concessionario.contatto" />
							<jsp:param name="keyREST" value="concessionarioREST" />
							<jsp:param name="soggetto" value="concessionario" />
							<jsp:param name="idSogg" value="concessionario.id" />
							<jsp:param name="UtenteSoggArray" value="G_concess_Write" />
							<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/data_agg_block.jsp" >
							<jsp:param name="dato" value="concessionario" />
							<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
						</jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-xs-12" style="padding-bottom: 20px;">
		<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD;">Gestori</div>
		<div class="table col-xs-12 soggName" style="padding: 5px;" ng-repeat="gestore in gestori" ng-class="{'active': !gestore.isC_details}">
				<div class="soggTable col-xs-12 ">
					<div class="soggName col-xs-12 no-margins" ng-class="{'activeSogg': !gestore.isC_details}">
						<div class="col-xs-12 col-sm-9 text-center-xs text-left-sm" ng-class="{'activeSoggTd': !gestore.isC_details}"  ng-click="gestoreForm.$visible ? void(0) : gestore.isC_details=!gestore.isC_details;">
							<span e-form="gestoreForm" editable-text="gestore.nome" onaftersave="saveGestore(gestore, gestoreForm)" oncancel="removeOnEdit(gestore);" e-size="50">{{gestore.nome}}</span>
						</div>
						<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm print-hide" style="padding-top: 5px;">
						<% if(canWriteAnagrafica) { %>
							<div class="btn-sogg">
								<button e-form="gestoreForm" type="button" class="btn btn-info" ng-click="gestoreForm.$show(); $event.stopPropagation(); gestore.onEdit=true;" ng-show="!gestore.isC_details">
									<span class="glyphicon glyphicon-edit"></span>
								</button>
								<!-- button e-form="gestoreForm" type="button" class="btn btn-danger" ng-click="deleteConcess(gestore.id); $event.stopPropagation();" ng-show="!gestore.isC_details">
									<span class="glyphicon glyphicon-trash"></span>
								</button -->
							</div>
						<% } %>
						</div>			
						<!-- td ng-class="{'activeSoggTd': !gestore.isC_details}">
							<span>{{gestore.nome}}</span>
						</td -->
					</div>
				
				<div class="col-xs-12 detailsTable no-margins no-padding-children" collapse="gestore.isC_details">
					<div class="col-xs-12">
						<jsp:include page="blocks/indirizzo_details_block.jsp" >
							<jsp:param name="mainArray" value="gestore.indirizzo" />
							<jsp:param name="regione" value="empty" />
							<jsp:param name="keyREST" value="gestoreREST" />
							<jsp:param name="soggetto" value="gestore" />
							<jsp:param name="idSogg" value="gestore.id" />
							<jsp:param name="UtenteSoggArray" value="G_gestore_Write" />
							<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/contatto_details_block.jsp" >
							<jsp:param name="mainArray" value="gestore.contatto" />
							<jsp:param name="keyREST" value="gestoreREST" />
							<jsp:param name="soggetto" value="gestore" />
							<jsp:param name="idSogg" value="gestore.id" />
							<jsp:param name="UtenteSoggArray" value="G_gestore_Write" />
							<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/data_agg_block.jsp" >
							<jsp:param name="dato" value="gestore" />
							<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
						</jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xs-12" style="padding-bottom: 20px;">
		<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD; ">Ingegneri Responsabili</div>
		
		<div class="table col-xs-12" style="padding: 5px;">
			<div class="col-xs-12 soggName" ng-style="ind==0 ? {'margin-bottom': '5px'} : {}" style="border-top: 1px solid #DDDDDD;" ng-repeat="(ind, ingegnereResp) in ingegneriResp | orderBy : 'dataNominaOrder' : true" ng-show="ind==0 || ingRespParams.showhistory" ng-class="{'active': !ingegnereResp.isC_details, 'current-sogg': ind==0, 'history-sogg': ind!=0}">
				<div class="soggTable col-xs-12 ">
					<div class="soggName col-xs-12 no-margins" style="padding-bottom: 20px;" ng-class="{'activeSoggTd': !ingegnereResp.isC_details}">
						<form editable-form shown="ingRespForm.$visible" name="ingRespForm" onaftersave="saveIngegnere(ingegnereResp, ingRespForm, ingegnereRespDigheREST)" oncancel="removeOnEdit(ingegnereResp)">
							<div class="col-xs-12 col-sm-9 text-center-xs text-left-sm" ng-click="show_hide_Ing_details(ingegnereResp)" ng-class="{'activeSogg': !ingegnereResp.isC_details}" >
								<div class="col-xs-12 col-sm-8 no-margins">			
									<span ng-show="!ingRespForm.$visible">{{ingegnereResp.denominazione}}</span>
									<div class="col-xs-12 no-margins" ng-show="ingRespForm.$visible">
										Nome: <span e-form="ingRespForm" editable-text="ingegnereResp.nome"></span>
									</div>
									<div class="col-xs-12 no-margins" ng-show="ingRespForm.$visible">
										Cognome: <span e-form="ingRespForm" editable-text="ingegnereResp.cognome"></span>
									</div>
								</div>	
								<% if(canWriteIng) { %>
								<div class="col-xs-12 no-margins text-center-xs text-right-sm" ng-class="{'col-sm-12 text-left-sm': ingRespForm.$visible, 'col-sm-4 text-right-sm': !ingRespForm.$visible }">
									<span ng-show="ingRespForm.$visible">
										Data nomina: <input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="ingRespForm.$visible" ng-model="ingegnereResp.newDataNomina" datepicker-popup="dd/MM/yyyy" is-open="ingRespForm.open" ng-focus="ingRespForm.open = true" ng-click="ingRespForm.open = true" placeholder="Data Nomina" current-text="Oggi" clear-text="Cancella" close-text="Chiudi"/>
									</span>
									<span ng-show="!ingRespForm.$visible">Data nomina: {{ingegnereResp.dataNomina}}</span>
								</div>
								<div class="col-xs-12 no-margins" ng-show="ingRespForm.$visible">
									<button type="submit" class="btn btn-primary" ng-disabled="ingRespForm.$waiting">
										<span class="glyphicon glyphicon-ok"></span>
									</button>
									<button type="button" class="btn btn-default" ng-disabled="ingRespForm.$waiting" ng-click="ingRespForm.$cancel()">
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</div>
								<% } %>
							</div>
							<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm print-hide" style="padding-top: 5px;">
								<% if(canWriteIng) { %>
								<div class="btn-sogg">
									<button e-form="ingRespForm" type="button" class="btn btn-info" ng-click="ingRespForm.$show(); $event.stopPropagation(); ingegnereResp.onEdit=true; ingegnereResp.newDataNomina=ingegnereResp.dataNomina;" ng-show="!ingegnereResp.isC_details">
										<span class="glyphicon glyphicon-edit"></span>
									</button>
									<button e-form="ingRespForm" type="button" class="btn btn-danger" ng-click="deleteIngegnereDiga(ingegnereResp, true); $event.stopPropagation();" ng-show="!ingegnereResp.isC_details">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</div>
								<% } %>
							</div>
						</form>
					</div>
					<div class="col-xs-12 detailsTable no-margins no-padding-children" collapse="ingegnereResp.isC_details">
						<div class="col-xs-12">
							<jsp:include page="blocks/indirizzo_details_block.jsp" >
							<jsp:param name="mainArray" value="ingegnereResp.indirizzo" />
							<jsp:param name="regione" value="empty" />
							<jsp:param name="keyREST" value="ingegnereREST" />
							<jsp:param name="soggetto" value="ingegnereResp" />
							<jsp:param name="idSogg" value="ingegnereResp.id" />
							<jsp:param name="isIng" value="<%=isIng%>" />
							<jsp:param name="canWrite" value="<%=canWriteAnagrafica || canWriteIng%>" />
							</jsp:include>
						</div>
						<div class="col-xs-12">
							<jsp:include page="blocks/contatto_details_block.jsp" >
							<jsp:param name="mainArray" value="ingegnereResp.contatto" />
							<jsp:param name="keyREST" value="ingegnereREST" />
							<jsp:param name="soggetto" value="ingegnereResp" />
							<jsp:param name="idSogg" value="ingegnereResp.id" />
							<jsp:param name="isIng" value="<%=isIng%>" />
							<jsp:param name="canWrite" value="<%=canWriteAnagrafica || canWriteIng%>" />
							</jsp:include>
						</div>
						<div class="col-xs-12">
							<jsp:include page="blocks/data_agg_block.jsp" >
							<jsp:param name="dato" value="ingegnereResp" />
							<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
							</jsp:include>
						</div>
					</div>
				</div>
			</div>
			<div class="sogg-history col-xs-12 print-hide" style="margin-bottom: 0px; border-top: 1px solid #DDDDDD;" ng-click="ingRespParams.showhistory = !ingRespParams.showhistory" class="sogg-history">
				<div ng-show="!ingRespParams.showhistory">
					<span class="glyphicon glyphicon-plus"></span> Mostra Storico Ingegneri Responsabili
				</div>
				<div ng-show="ingRespParams.showhistory">
					<span class="glyphicon glyphicon-minus"></span> Nascondi Storico Ingegneri Responsabili
				</div>
			</div>
		</div>
		
		
		<% if(canWriteIng) { %>
		<div class="col-xs-12 print-hide">
			<button type="submit" class="btn btn-primary" ng-click="caricaIngegnere(ingegnereRespDigheREST)">
				Aggiungi Ingegnere Responsabile Esistente
			</button>

			<button type="submit" class="btn btn-primary" ng-click="addIngegnere(nuovoIngegnereRespForm, nuovoIngegnereResp)">
				Crea e Aggiungi Ingegnere Responsabile
			</button>
		</div>
		
		<div class="col-xs-12">
			<form editable-form shown="nuovoIngegnereRespForm.$visible" name="nuovoIngegnereRespForm" onaftersave="saveNuovoIngegnereResp(nuovoIngegnereRespForm, ingegnereRespDigheREST); nuovoIngegnereResp.$visible=false;" oncancel="nuovoIngegnereResp.$visible=false">

				<span ng-show="nuovoIngegnereResp.$visible">Nuovo Ingegnere Responsabile:</span> 
				<span ng-show="nuovoIngegnereResp.$visible" editable-text="nuovoIngegnereResp.nome" e-placeholder="Nome">{{nuovoIngegnereResp.nome}}</span> 
				<span ng-show="nuovoIngegnereResp.$visible" editable-text="nuovoIngegnereResp.cognome" e-placeholder="Cognome">{{nuovoIngegnereResp.cognome}}</span> 
				<!-- span ng-show="nuovoIngegnereResp.$visible" ng-model="nuovoIngegnereResp.dataNomina" datepicker-popup="dd/MM/yyyy" is-open="false">{{nuovoIngegnereResp.dataNomina}}</span --> 
				<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="nuovoIngegnereResp.$visible" ng-model="nuovoIngegnereResp.dataNomina" datepicker-popup="dd/MM/yyyy" is-open="nuovoIngegnereResp.open" ng-focus="nuovoIngegnereResp.open = true" ng-click="nuovoIngegnereSost.open = true" placeholder="Data Nomina"/> 


				<span ng-show="nuovoIngegnereRespForm.$visible">
					<button type="submit" class="btn btn-primary" ng-disabled="nuovoIngegnereRespForm.$waiting">
						<span class="glyphicon glyphicon-ok"></span>
					</button>
					<button type="button" class="btn btn-default" ng-disabled="nuovoIngegnereRespForm.$waiting" ng-click="nuovoIngegnereRespForm.$cancel()">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
				</span>
			</form>
		</div>
		<% } %>
	</div>



	<div class="col-xs-12" style="padding-bottom: 20px;">
		<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD; ">Ingegneri Sostituti</div>
			<div class="table col-xs-12" style="padding: 5px;">
				<div class="col-xs-12 soggName" ng-style="ind==0 ? {'margin-bottom': '5px'} : {}" style="border-top: 1px solid #DDDDDD;" ng-repeat="(ind, ingegnereSost) in ingegneriSost | orderBy : 'dataNominaOrder' : true" ng-show="ind==0 || ingSostParams.showhistory" ng-class="{'active': !ingegnereSost.isC_details, 'current-sogg': ind==0, 'history-sogg': ind!=0}">
						<div class="soggTable col-xs-12 ">
							<div class="soggName col-xs-12 no-margins" style="padding-bottom: 20px;" ng-class="{'activeSoggTd': !ingegnereSost.isC_details}">
								<form editable-form shown="ingSostForm.$visible" name="ingSostForm" onaftersave="saveIngegnere(ingegnereSost, ingSostForm, ingegnereSostDigheREST)" oncancel="removeOnEdit(ingegnereSost)">
									<div class="col-xs-12 col-sm-9 text-center-xs text-left-sm" ng-click="show_hide_Ing_details(ingegnereSost)" ng-class="{'activeSoggTd': !ingegnereSost.isC_details}">
										<div class="col-xs-12 col-sm-8 no-margins">
											<span ng-show="!ingSostForm.$visible">{{ingegnereSost.denominazione}}</span>
											<div class="col-xs-12 no-margins" ng-show="ingSostForm.$visible">
												Nome: <span e-form="ingSostForm" editable-text="ingegnereSost.nome"></span>
											</div>
											<div class="col-xs-12 no-margins" ng-show="ingSostForm.$visible">
												Cognome: <span e-form="ingSostForm" editable-text="ingegnereSost.cognome"></span>
											</div>
										</div>
									<% if(canWriteIng) { %>
										<div class="col-xs-12 no-margins text-center-xs text-right-sm" ng-class="{'col-sm-12 text-left-sm': ingRespForm.$visible, 'col-sm-4 text-right-sm': !ingRespForm.$visible }">
										<span ng-show="ingSostForm.$visible">
											Data nomina: <input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="ingSostForm.$visible" ng-model="ingegnereSost.newDataNomina" datepicker-popup="dd/MM/yyyy" is-open="ingSostForm.open" ng-focus="ingSostForm.open = true" ng-click="ingSostForm.open = true" placeholder="Data Nomina" current-text="Oggi" clear-text="Cancella" close-text="Chiudi"/>
										</span>
										<span ng-show="!ingSostForm.$visible">Data nomina: {{ingegnereSost.dataNomina}}</span>
										</div>
										<div class="col-xs-12 no-margins" ng-show="ingSostForm.$visible">
											<button type="submit" class="btn btn-primary" ng-disabled="ingSostForm.$waiting">
												<span class="glyphicon glyphicon-ok"></span>
											</button>
											<button type="button" class="btn btn-default" ng-disabled="ingSostForm.$waiting" ng-click="ingSostForm.$cancel()">
												<span class="glyphicon glyphicon-remove"></span>
											</button>
										</div>
									<% } %>
									</div>
									<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm print-hide" style="padding-top: 5px;">
										<% if(canWriteIng) { %>
										<div class="btn-sogg">
											<button e-form="ingSostForm" type="button" class="btn btn-info" ng-click="ingSostForm.$show(); $event.stopPropagation(); ingegnereSost.onEdit=true; ingegnereSost.newDataNomina=ingegnereSost.dataNomina;" ng-show="!ingegnereSost.isC_details">
												<span class="glyphicon glyphicon-edit"></span>
											</button>
											<button e-form="ingSostForm" type="button" class="btn btn-danger" ng-click="deleteIngegnereDiga(ingegnereSost, false); $event.stopPropagation();" ng-show="!ingegnereSost.isC_details">
												<span class="glyphicon glyphicon-trash"></span>
											</button>
										</div>
										<% } %>
									</div>
								</form>
							</div>
						<div class="col-xs-12 detailsTable no-margins no-padding-children" collapse="ingegnereSost.isC_details">
						<div class="col-xs-12">
							<jsp:include page="blocks/indirizzo_details_block.jsp" >
								<jsp:param name="mainArray" value="ingegnereSost.indirizzo" />
								<jsp:param name="regione" value="empty" />
								<jsp:param name="keyREST" value="ingegnereREST" />
								<jsp:param name="soggetto" value="ingegnereSost" />
								<jsp:param name="idSogg" value="ingegnereSost.id" />
								<jsp:param name="isIng" value="<%=isIng%>" />
								<jsp:param name="canWrite" value="<%=canWriteAnagrafica || canWriteIng%>" />
							</jsp:include>
						</div>
						<div class="col-xs-12">
							<jsp:include page="blocks/contatto_details_block.jsp" >
								<jsp:param name="mainArray" value="ingegnereSost.contatto" />
								<jsp:param name="keyREST" value="ingegnereREST" />
								<jsp:param name="soggetto" value="ingegnereSost" />
								<jsp:param name="idSogg" value="ingegnereSost.id" />
								<jsp:param name="isIng" value="<%=isIng%>" />
								<jsp:param name="canWrite" value="<%=canWriteAnagrafica || canWriteIng%>" />
							</jsp:include>
						</div>
						<div class="col-xs-12">
							<jsp:include page="blocks/data_agg_block.jsp" >
								<jsp:param name="dato" value="ingegnereSost" />
								<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
							</jsp:include>
				</div>
			</div>
		</div>
		</div>
		<div class="sogg-history col-xs-12 print-hide" style="margin-bottom: 0px; border-top: 1px solid #DDDDDD;" ng-click="ingSostParams.showhistory = !ingSostParams.showhistory" class="sogg-history">
			<div ng-show="!ingSostParams.showhistory">
				<span class="glyphicon glyphicon-plus"></span> Mostra Storico Ingegneri Sostituti
			</div>
			<div ng-show="ingSostParams.showhistory">
				<span class="glyphicon glyphicon-minus"></span> Nascondi Storico Ingegneri Sostituti
			</div>
		</div>
		</div>
	<% if(canWriteIng) { %>
	<div class="col-xs-12 print-hide">
	<button type="submit" class="btn btn-primary" ng-click="caricaIngegnere(ingegnereSostDigheREST)">
		Aggiungi Ingegnere Sostituto Esistente
	</button>

	<button type="submit" class="btn btn-primary" ng-click="addIngegnere(nuovoIngegnereSostForm, nuovoIngegnereSost)">
		Crea e Aggiungi Ingegnere Sostituto
	</button>
	</div>
	<div class="col-xs-12">
	<form editable-form shown="nuovoIngegnereSostForm.$visible" name="nuovoIngegnereSostForm" onaftersave="saveNuovoIngegnereSost(nuovoIngegnereSostForm, ingegnereSostDigheREST); nuovoIngegnereSost.$visible=false;" oncancel="nuovoIngegnereSost.$visible=false">
		 <span ng-show="nuovoIngegnereSost.$visible">Nuovo Ingegnere Sostituto:</span>  
		 <span ng-show="nuovoIngegnereSost.$visible" editable-text="nuovoIngegnereSost.nome" e-placeholder="Nome">{{nuovoIngegnereSost.nome}}</span>  
		 <span ng-show="nuovoIngegnereSost.$visible" editable-text="nuovoIngegnereSost.cognome" e-placeholder="Cognome">{{nuovoIngegnereSost.cognome}}</span>  
		 <!-- span ng-show="nuovoIngegnereSost.$visible" ng-model="nuovoIngegnereSost.dataNomina" datepicker-popup="dd/MM/yyyy" is-open="false">{{nuovoIngegnereSost.dataNomina}}</span -->  
		 <input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="nuovoIngegnereSost.$visible" ng-model="nuovoIngegnereSost.dataNomina" datepicker-popup="dd/MM/yyyy" is-open="nuovoIngegnereSost.open" ng-focus="nuovoIngegnereSost.open = true" ng-click="nuovoIngegnereSost.open = true" placeholder="Data Nomina"/>  
		
		
		<span ng-show="nuovoIngegnereSostForm.$visible">
			<button type="submit" class="btn btn-primary" ng-disabled="nuovoIngegnereSostForm.$waiting">
				<span class="glyphicon glyphicon-ok"></span>
			</button>
			<button type="button" class="btn btn-default" ng-disabled="nuovoIngegnereSostForm.$waiting" ng-click="nuovoIngegnereSostForm.$cancel()">
				<span class="glyphicon glyphicon-remove"></span>
			</button>
		</span>
	</form>
	</div>
	<% } %>
	</div>

	<div class="col-xs-12" style="padding-bottom: 20px;">
		<div class="col-xs-12 h5" style="padding-top: 10px !important; border-top: 1px solid #DDDDDD; ">Casa di Guardia</div>
		<div class="table col-xs-12" style="padding: 5px;">
			<div class="table">
				<form editable-form shown="casaGuardiaForm.$visible" name="casaGuardiaForm" onaftersave="saveCasaGuardia(casaGuardiaForm);">
					<div class="soggName col-xs-12 no-margins" style="padding-bottom: 20px;">
						<div class="col-xs-12 col-sm-9 no-padding-children">
							<div class="col-xs-4">
								<div class="col-sm-6 col-xs-12 detailsLabel">Telefono: </div>
								<div class="col-sm-6 col-xs-12">
									<span editable-text="datiRubrica.casaDiGuarda.telefono" e-placeholder="Telefono">{{datiRubrica.casaDiGuarda.telefono}}</span>
								</div>
							</div>
							<div class="col-xs-4"> 
								<div class="col-sm-6 col-xs-12 detailsLabel">Telefono Posto Presidiato: </div>
								<div class="col-sm-6 col-xs-12">
									<span editable-text="datiRubrica.casaDiGuarda.telefonoPostoPresidiato" e-placeholder="Telefono Posto Presidiato">{{datiRubrica.casaDiGuarda.telefonoPostoPresidiato}}</span>
								</div>
							</div>
							<div class="col-xs-4"> 
								<div class="col-sm-6 col-xs-12 detailsLabel">Telefono Cantiere: </div>
								<div class="col-sm-6 col-xs-12">
									<span editable-text="datiRubrica.casaDiGuarda.telefonoCantiere" e-placeholder="Telefono Cantiere">{{datiRubrica.casaDiGuarda.telefonoCantiere}}</span>
								</div>
							</div>

							<% if(canWriteIng) { %>
							<div class="col-xs-12 no-margins print-hide" ng-show="casaGuardiaForm.$visible">
								<button e-form="casaGuardiaForm" type="submit" class="btn btn-primary" ng-disabled="casaGuardiaForm.$waiting">
									<span class="glyphicon glyphicon-ok"></span>
								</button>
								<button e-form="casaGuardiaForm" type="button" class="btn btn-default" ng-disabled="casaGuardiaForm.$waiting" ng-click="casaGuardiaForm.$cancel()">
									<span class="glyphicon glyphicon-remove"></span>
								</button>
							</div>
							<% } %>
						</div> 
						<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm print-hide" style="padding-top: 5px;">
							<% if(canWriteIng) { %>
							<div class="btn-sogg" ng-show="!casaGuardiaForm.$visible">
								<button e-form="casaGuardiaForm" type="button" class="btn btn-info" ng-click="casaGuardiaForm.$show(); $event.stopPropagation();">
									<span class="glyphicon glyphicon-edit"></span>
								</button>
								<!-- button e-form="casaGuardiaForm" type="button" class="btn btn-danger" ng-click="deleteConcess(concessionario.id); $event.stopPropagation();" ng-show="!concessionario.isC_details">
									<span class="glyphicon glyphicon-trash"></span>
								</button -->
							</div>
							<% } %>
						</div>
					</div>
					<div class="col-xs-12 detailsTable no-margins no-padding-children" collapse="ingegnereResp.isC_details">
						<div class="col-xs-12">
							<jsp:include page="blocks/data_agg_block.jsp" >
							<jsp:param name="dato" value="datiRubrica.casaDiGuarda" />
							<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
							</jsp:include>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>