<table ng-table="tableConcess" show-filter="true" class="table">
	<tr>
		<td filter="{ 'nome': 'text' }"></td>
	</tr>
	<tr ng-repeat="concessionario in $data" ng-class="{'active': !concessionario.isC_details}">
		<td filter="{ 'nome': 'text' }">
			<div class="soggTable">
				<div class="soggName col-xs-12" ng-click="show_hide_Concess_details(concessionario);" ng-class="{'activeSogg': !concessionario.isC_details}" >
				<div ng-class="{'activeSoggTd': !concessionario.isC_details}" class="col-xs-12 col-sm-9 text-center-xs text-left-sm" >
					<span e-form="concessForm" editable-text="concessionario.nome" onaftersave="saveConcess(concessionario, concessForm)" oncancel="removeOnEdit(concessionario);" e-size="50">{{concessionario.nome}}</span>
				</div>
				<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm">
					<% if(canWrite) { %>
					<div class="btn-sogg">
						<button e-form="concessForm" type="button" class="btn btn-info" ng-click="concessForm.$show(); $event.stopPropagation(); concessionario.onEdit=true;" ng-show="!concessionario.isC_details">
							<span class="glyphicon glyphicon-edit"></span>
						</button>
						<button e-form="concessForm" type="button" class="btn btn-danger" ng-click="deleteConcess(concessionario.id); $event.stopPropagation();" ng-show="!concessionario.isC_details">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
					</div>
					<% } %>
				</div>
				</div>
				<div class="row detailsTable" collapse="concessionario.isC_details">
					<% //indirizzo_details_block %>
					<div class="col-xs-12">
						<jsp:include page="blocks/indirizzo_details_block.jsp" > 
							<jsp:param name="mainArray" value="concessionario.details.indirizzo" />
							<jsp:param name="regione" value="empty" />
							<jsp:param name="keyREST" value="concessionarioREST" />
							<jsp:param name="soggetto" value="concessionario" />
							<jsp:param name="idSogg" value="concessionario.id" />
							<jsp:param name="UtenteSoggArray" value="G_concess_Write" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
							<jsp:param name="canWriteDettagli" value="<%=canWriteDettagli%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<% //contatto_details_block %>
						<jsp:include page="blocks/contatto_details_block.jsp" > 
							<jsp:param name="mainArray" value="concessionario.details.contatto" />
							<jsp:param name="keyREST" value="concessionarioREST" />
							<jsp:param name="soggetto" value="concessionario" />
							<jsp:param name="idSogg" value="concessionario.id" />
							<jsp:param name="UtenteSoggArray" value="G_concess_Write" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
							<jsp:param name="canWriteDettagli" value="<%=canWriteDettagli%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/assoc_details_block.jsp" >
							<jsp:param name="mainArray" value="concessionario.details.gestori" />
							<jsp:param name="assocArray" value="gestori" />
							<jsp:param name="keyREST" value="concessionarioAssocREST" />
							<jsp:param name="assocDigheKeyREST" value="gestoreAssocREST" />
							<jsp:param name="soggetto" value="concessionario" />
							<jsp:param name="idSogg" value="concessionario.id" />
							<jsp:param name="nomeSogg" value="concessionario.nome" />
							<jsp:param name="titolo" value="Gestori Associati" />
							<jsp:param name="sTitolo2" value="Elenco Gestori" />
							<jsp:param name="displayName" value="nome" />
							<jsp:param name="orderField" value="nome" />
							<jsp:param name="alertFct" value="alertDelAssocConcessGestore" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/assoc_details_block.jsp" >
							<jsp:param name="mainArray" value="concessionario.details.dighe" />
							<jsp:param name="assocArray" value="dighe" />
							<jsp:param name="keyREST" value="concessionarioDigheREST" />
							<jsp:param name="assocDigheKeyREST" value="empty" />
							<jsp:param name="soggetto" value="concessionario" />
							<jsp:param name="idSogg" value="concessionario.id" />
							<jsp:param name="nomeSogg" value="concessionario.nome" />
							<jsp:param name="titolo" value="Dighe Associate" />
							<jsp:param name="sTitolo2" value="Elenco Dighe" />
							<jsp:param name="displayName" value="denominazione" />
							<jsp:param name="orderField" value="orderId" />
							<jsp:param name="alertFct" value="empty" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/assoc_details_block.jsp" >
							<jsp:param name="mainArray" value="concessionario.details.utenti" />
							<jsp:param name="assocArray" value="utenti" />
							<jsp:param name="keyREST" value="concessionarioUtentiREST" />
							<jsp:param name="assocDigheKeyREST" value="empty" />
							<jsp:param name="soggetto" value="concessionario" />
							<jsp:param name="idSogg" value="concessionario.id" />
							<jsp:param name="nomeSogg" value="concessionario.nome" />
							<jsp:param name="titolo" value="Utenti Associati" />
							<jsp:param name="sTitolo2" value="Elenco Utenti" />
							<jsp:param name="displayName" value="denominazione" />
							<jsp:param name="orderField" value="denominazione" />
							<jsp:param name="alertFct" value="empty" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
						</jsp:include>
					</div>
				</div>
			</div>
		</td>
	</tr>
</table>
<% if(canWrite) { %>
<div class="row">
<!--<div class="col-sm-4 col-md-3 col-lg-2 col-xs-12 text-right-sm text-center-xs" style="padding-top: 10px; padding-bottom: 10px;">-->
<div class="col-sm-8 col-md-9 col-lg-10 col-xs-12 text-left-sm text-center-xs" style="padding-top: 10px; padding-bottom: 10px;">
<button type="submit" class="btn btn-primary" ng-click="addConcessionario(nuovoConcessForm)">
	Aggiungi Concessionario
</button>
</div>
<div class="col-sm-8 col-md-9 col-lg-10 col-xs-12 text-left-sm text-center-xs">
	<span onbeforesave="checkCampoObb($data)" e-placeholder="nome concessionario" e-form="nuovoConcessForm" editable-text="nuovoConcess.nome" ng-show="nuovoConcess.$visible" onaftersave="saveNuovoConcess(nuovoConcessForm); nuovoConcess.$visible=false;" oncancel="nuovoConcess.$visible=false" e-size="30">{{nuovoConcess.nome}}</span>
</div>
</div>
<% } %>