<table ng-table="tableGestori" show-filter="true" class="table">
	<tr>
		<td filter="{ 'nome': 'text' }"></td>
	</tr>
	<tr ng-repeat="gestore in $data" ng-class="{'active': !gestore.isC_details}">
		<td filter="{ 'nome': 'text' }">
			<div class="soggTable">
				<div class="soggName col-xs-12" ng-click="show_hide_Gestore_details(gestore);" ng-class="{'activeSogg': !gestore.isC_details}" >
					<div ng-class="{'activeSoggTd': !gestore.isC_details}" class="col-xs-12 col-sm-9 text-center-xs text-left-sm" >
						<span e-form="gestoreForm" editable-text="gestore.nome" onaftersave="saveGestore(gestore, gestoreForm)" oncancel="removeOnEdit(gestore);" e-size="50">{{gestore.nome}}</span>
					</div>
					<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm">
						<% if(canWrite) { %>
						
						<div class="btn-sogg">
							<button e-form="gestoreForm" type="button" class="btn btn-info" ng-click="gestoreForm.$show(); $event.stopPropagation(); gestore.onEdit=true;" ng-show="!gestore.isC_details">
								<span class="glyphicon glyphicon-edit"></span>
							</button>
							<button e-form="gestoreForm" type="button" class="btn btn-danger" ng-click="deleteGestore(gestore.id); $event.stopPropagation();" ng-show="!gestore.isC_details">
								<span class="glyphicon glyphicon-trash"></span>
							</button>
						</div>
						<% } %>
					</div>
				</div>
					<div class="row detailsTable" collapse="gestore.isC_details">
					<div class="col-xs-12">
						<jsp:include page="blocks/indirizzo_details_block.jsp" >
							<jsp:param name="mainArray" value="gestore.details.indirizzo" />
							<jsp:param name="regione" value="empty" />
							<jsp:param name="keyREST" value="gestoreREST" />
							<jsp:param name="soggetto" value="gestore" />
							<jsp:param name="idSogg" value="gestore.id" />
							<jsp:param name="UtenteSoggArray" value="G_gestore_Write" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
							<jsp:param name="canWriteDettagli" value="<%=canWriteDettagli%>" />
						</jsp:include>
						</div>
						<div class="col-xs-12">
						<jsp:include page="blocks/contatto_details_block.jsp" >
							<jsp:param name="mainArray" value="gestore.details.contatto" />
							<jsp:param name="keyREST" value="gestoreREST" />
							<jsp:param name="soggetto" value="gestore" />
							<jsp:param name="idSogg" value="gestore.id" />
							<jsp:param name="UtenteSoggArray" value="G_gestore_Write" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
							<jsp:param name="canWriteDettagli" value="<%=canWriteDettagli%>" />
						</jsp:include>
						</div>
						<div class="col-xs-12">
						<jsp:include page="blocks/assoc_details_block.jsp" >
							<jsp:param name="mainArray" value="gestore.details.concessionari" />
							<jsp:param name="assocArray" value="concessionari" />
							<jsp:param name="keyREST" value="gestoreAssocREST" />
							<jsp:param name="assocDigheKeyREST" value="concessionarioDigheREST" />
							<jsp:param name="soggetto" value="gestore" />
							<jsp:param name="idSogg" value="gestore.id" />
							<jsp:param name="nomeSogg" value="gestore.nome" />
							<jsp:param name="titolo" value="Concessionari Associati" />
							<jsp:param name="sTitolo2" value="Elenco Concessionari" />
							<jsp:param name="displayName" value="nome" />
							<jsp:param name="orderField" value="nome" />
							<jsp:param name="alertFct" value="alertDelAssocGestoreConcess" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
						</jsp:include>
						</div>
						<div class="col-xs-12">
						<jsp:include page="blocks/assoc_details_block.jsp" >
							<jsp:param name="mainArray" value="gestore.details.dighe" />
							<jsp:param name="assocArray" value="getDigheFromConcessArray(gestore.details.concessionari)" />
							<jsp:param name="keyREST" value="gestoreDigheREST" />
							<jsp:param name="assocDigheKeyREST" value="empty" />
							<jsp:param name="soggetto" value="gestore" />
							<jsp:param name="idSogg" value="gestore.id" />
							<jsp:param name="nomeSogg" value="gestore.nome" />
							<jsp:param name="titolo" value="Dighe Associate" />
							<jsp:param name="sTitolo2" value="Elenco Dighe" />
							<jsp:param name="displayName" value="denominazione" />
							<jsp:param name="orderField" value="orderId" />
							<jsp:param name="alertFct" value="empty" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
						</jsp:include>
						</div>
						<div class="col-xs-12">
						<jsp:include page="blocks/assoc_details_block.jsp" >
							<jsp:param name="mainArray" value="gestore.details.utenti" />
							<jsp:param name="assocArray" value="utenti" />
							<jsp:param name="keyREST" value="gestoreUtentiREST" />
							<jsp:param name="assocDigheKeyREST" value="empty" />
							<jsp:param name="soggetto" value="gestore" />
							<jsp:param name="idSogg" value="gestore.id" />
							<jsp:param name="nomeSogg" value="gestore.nome" />
							<jsp:param name="titolo" value="Utenti Associati" />
							<jsp:param name="sTitolo2" value="Elenco Utenti" />
							<jsp:param name="displayName" value="denominazione" />
							<jsp:param name="orderField" value="denominazione" />
							<jsp:param name="alertFct" value="empty" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
						</jsp:include>
						</div>
					</div>
			</div>
		</td>
	</tr>
</table>
<% if(canWrite) { %>
<div class="row">
<!--<div class="col-sm-3 col-md-2 col-xs-12 text-left-sm text-center-xs" style="padding-top: 10px; padding-bottom: 10px;">-->
<div class="col-sm-8 col-md-9 col-lg-10 col-xs-12 text-left-sm text-center-xs" style="padding-top: 10px; padding-bottom: 10px;">
<button type="submit" class="btn btn-primary" ng-click="addGestore(nuovoGestoreForm)">
	Aggiungi Gestore
</button>
</div>
<div class="col-sm-9 col-md-10 col-xs-12 text-left-sm text-center-xs">
	<span onbeforesave="checkCampoObb($data)" e-placeholder="nome gestore" e-form="nuovoGestoreForm" editable-text="nuovoGestore.nome" ng-show="nuovoGestore.$visible" onaftersave="saveNuovoGestore(nuovoGestoreForm); nuovoGestore.$visible=false;" oncancel="nuovoGestore.$visible=false" e-size="50">{{nuovoGestore.nome}}</span>
</div>
</div>
<% } %>