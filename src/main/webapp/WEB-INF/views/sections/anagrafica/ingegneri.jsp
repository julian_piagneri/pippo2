<table ng-table="tableIngegneri" show-filter="true" class="table">
	<tr>
		<td filter="{ 'nomeCognome': 'text' }"></td>
	</tr>
	<tr ng-repeat="ingegnere in $data" ng-class="{'active': !ingegnere.isC_details}">
		<td filter="{ 'nomeCognome': 'text' }">
			<table class="soggTable">
				<tr class="soggName" ng-click="show_hide_Ingegnere_details(ingegnere);" ng-class="{'activeSogg': !ingegnere.isC_details}"><td ng-class="{'activeSoggTd': !ingegnere.isC_details}">{{ingegnere.cognome}} {{ingegnere.nome}}</td></tr>
				
				<tr><td>
					<table class="detailsTable" collapse="ingegnere.isC_details">
						<jsp:include page="indirizzo_details_block.jsp" >
							<jsp:param name="mainArray" value="ingegnere.details.indirizzo" />
							<jsp:param name="regione" value="empty" />
							<jsp:param name="keyREST" value="ingegnereREST" />
							<jsp:param name="soggetto" value="ingegnere" />
							<jsp:param name="idSogg" value="ingegnere.id" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
							<jsp:param name="canWriteDettagli" value="<%=canWriteDettagli%>" />
						</jsp:include>
						<jsp:include page="contatto_details_block.jsp" >
							<jsp:param name="mainArray" value="ingegnere.details.contatto" />
							<jsp:param name="keyREST" value="ingegnereREST" />
							<jsp:param name="soggetto" value="ingegnere" />
							<jsp:param name="idSogg" value="ingegnere.id" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
							<jsp:param name="canWriteDettagli" value="<%=canWriteDettagli%>" />
						</jsp:include>
					</table>
				</td></tr>
			</table>
		</td>
	</tr>
</table>