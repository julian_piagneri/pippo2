<table ng-table="tableAltriEnti" show-filter="true" class="table">
	<tr>
		<td filter="{ 'nome': 'text' }"></td>
	</tr>
	<tr ng-repeat="altraEnte in $data" ng-class="{'active': !altraEnte.isC_details}">
		<td filter="{ 'nome': 'text' }">
			<div class="soggTable">
				<div class="soggName col-xs-12" ng-click="show_hide_Altro_Ente_details(altraEnte);" ng-class="{'activeSogg': !altraEnte.isC_details}">
					<form editable-form name="altraEnteForm" onaftersave="saveAltroEnte(altraEnte, altraEnteForm);" oncancel="removeOnEdit(altraEnte)">
						<div class="col-xs-12 col-sm-9 text-center-xs text-left-sm" ng-class="{'activeSoggTd': !altraEnte.isC_details}">
							<div ng-show="!altraEnteForm.$visible">
								{{altraEnte.nome}} ({{getNomeTipoEnte(altraEnte.tipoEnte)}})
							</div>
							
							<% if(canWrite) { %>
								<div ng-show="altraEnteForm.$visible">
									<span e-form="altraEnteForm" editable-text="altraEnte.nome" e-size="50">{{altraEnte.nome}}</span>
									<br />Tipo Ente: <span editable-select="altraEnte.tipoEnte" e-ng-options="tipo.id as tipo.nome for tipo in (tipiAltroEnte | orderBy : 'nome' : false)">{{getNomeTipoEnte(altraEnte.tipoEnte)}}</span>
								</div>
								
								<span ng-show="altraEnteForm.$visible">
									<button type="submit" class="btn btn-primary" ng-disabled="altraEnteForm.$waiting">
										<span class="glyphicon glyphicon-ok"></span>
									</button>
									<button type="button" class="btn btn-default" ng-disabled="altraEnteForm.$waiting" ng-click="altraEnteForm.$cancel()">
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</span>
							<% } %>
						</div>
						<div class="col-xs-12 col-sm-3 text-center-xs text-right-sm">
							<% if(canWrite) { %>
							<div class="btn-sogg">
								<button e-form="altraEnteForm" type="button" class="btn btn-info" ng-click="altraEnteForm.$show(); $event.stopPropagation(); altraEnte.onEdit=true;" ng-show="!altraEnte.isC_details">
									<span class="glyphicon glyphicon-edit"></span>
								</button>
								<button e-form="altraEnteForm" type="button" class="btn btn-danger" ng-click="deleteAltroEnte(altraEnte.id); $event.stopPropagation();" ng-show="!altraEnte.isC_details">
									<span class="glyphicon glyphicon-trash"></span>
								</button>
							</div>
							<% } %>
						</div>
					</form>			
				</div>
				<div class="row detailsTable" collapse="altraEnte.isC_details">
					<div class="col-xs-12">
						<jsp:include page="blocks/indirizzo_details_block.jsp" >
							<jsp:param name="mainArray" value="altraEnte.details.indirizzo" />
							<jsp:param name="regione" value="empty" />
							<jsp:param name="keyREST" value="altraEnteREST" />
							<jsp:param name="soggetto" value="altraEnte" />
							<jsp:param name="idSogg" value="altraEnte.id" />
							<jsp:param name="UtenteSoggArray" value="G_altroEnte_Write" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
							<jsp:param name="canWriteDettagli" value="<%=canWriteDettagli%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/contatto_details_block.jsp" >
							<jsp:param name="mainArray" value="altraEnte.details.contatto" />
							<jsp:param name="keyREST" value="altraEnteREST" />
							<jsp:param name="soggetto" value="altraEnte" />
							<jsp:param name="idSogg" value="altraEnte.id" />
							<jsp:param name="UtenteSoggArray" value="G_altroEnte_Write" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
							<jsp:param name="canWriteDettagli" value="<%=canWriteDettagli%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/assoc_details_block.jsp" >
							<jsp:param name="mainArray" value="altraEnte.details.dighe" />
							<jsp:param name="assocArray" value="dighe" />
							<jsp:param name="keyREST" value="altraEnteDigheREST" />
							<jsp:param name="assocDigheKeyREST" value="empty" />
							<jsp:param name="soggetto" value="altraEnte" />
							<jsp:param name="idSogg" value="altraEnte.id" />
							<jsp:param name="nomeSogg" value="altraEnte.nome" />
							<jsp:param name="titolo" value="Dighe Associate" />
							<jsp:param name="sTitolo2" value="Elenco Dighe" />
							<jsp:param name="displayName" value="denominazione" />
							<jsp:param name="orderField" value="orderId" />
							<jsp:param name="alertFct" value="empty" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
						</jsp:include>
					</div>
					<div class="col-xs-12">
						<jsp:include page="blocks/assoc_details_block.jsp" >
							<jsp:param name="mainArray" value="altraEnte.details.utenti" />
							<jsp:param name="assocArray" value="utenti" />
							<jsp:param name="keyREST" value="altraEnteUtentiREST" />
							<jsp:param name="assocDigheKeyREST" value="empty" />
							<jsp:param name="soggetto" value="altraEnte" />
							<jsp:param name="idSogg" value="altraEnte.id" />
							<jsp:param name="nomeSogg" value="altraEnte.nome" />
							<jsp:param name="titolo" value="Utenti Associati" />
							<jsp:param name="sTitolo2" value="Elenco Utenti" />
							<jsp:param name="displayName" value="denominazione" />
							<jsp:param name="orderField" value="denominazione" />
							<jsp:param name="alertFct" value="empty" />
							<jsp:param name="canWrite" value="<%=canWrite%>" />
						</jsp:include>
					</div>
				</div>
			</div>
		</td>
	</tr>
</table>
<% if(canWrite) { %>
<button type="submit" class="btn btn-primary" ng-click="addAltroEnte(nuovoEnteForm)">
	Aggiungi Altro Ente
</button>

<form editable-form shown="nuovoEnteForm.$visible" name="nuovoEnteForm" onaftersave="saveNuovoEnte(nuovoEnteForm); nuovoEnte.$visible=false;" oncancel="nuovoEnte.$visible=false">					
	<div ng-show="nuovoEnteForm.$visible">
		Nome&nbsp;<span onbeforesave="checkCampoObb($data)" ng-show="nuovoEnte.$visible" e-placeholder="Nome Ente" editable-text="nuovoEnte.nome" e-size="50">{{nuovoEnte.nome}}</span>&nbsp;&nbsp;
		Tipo&nbsp;Ente:&nbsp;<span onbeforesave="checkCampoObb($data)" ng-show="nuovoEnte.$visible" editable-select="nuovoEnte.tipoEnte" e-ng-options="tipoAltroEnte.id as tipoAltroEnte.nome for tipoAltroEnte in (tipiAltroEnte | orderBy : 'id' : false)">{{nuovoEnte.tipoEnte}}</span>&nbsp;&nbsp;
	</div>
	
	
	
	<span ng-show="nuovoEnteForm.$visible">
		<button type="submit" class="btn btn-primary" ng-disabled="nuovoEnteForm.$waiting">
			<span class="glyphicon glyphicon-ok"></span>
		</button>
		<button type="button" class="btn btn-default" ng-disabled="nuovoEnteForm.$waiting" ng-click="nuovoEnteForm.$cancel()">
			<span class="glyphicon glyphicon-remove"></span>
		</button>
	</span>
</form>
<% } %>

