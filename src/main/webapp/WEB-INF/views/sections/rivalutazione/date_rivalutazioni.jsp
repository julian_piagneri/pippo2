<form editable-form name="dateRivForm" onaftersave="saveDateRiv(dateRivForm);">
	<div class="col-xs-12" style="padding-bottom: 20px;">
		<div class="row table detailsTable no-padding-children">
			<div class="col-xs-12 col-sm-6 detailsLabel">Motivo della rivalutazione:</div>
			<div class="col-xs-12 col-sm-6">
				<span editable-select="datiDate.idOrigineRivalutazione" e-ng-options="tipoO.id as tipoO.nome for tipoO in datiTipoOrigine">
					{{getOrigineRivalutazioneName(datiDate.idOrigineRivalutazione)}}
				</span>
			</div>
		</div>
		<div class="col-md-12 col-xs-12">
			<h5 class="detailsLabel">Altri motivi di rivalutazione</h5>
			<span id="note" name="note" rows="8" editable-text="datiDate.note">{{datiDate.note}}</span>
		</div>
	</div>
	<div class="col-xs-12 col-md-2 print-hide" style="padding-bottom: 5px;">
		<% if(canWrite) { %>
			<button type="button" class="btn btn-info" ng-click="dateRivForm.$show()" ng-show="!dateRivForm.$visible">
				<span class="glyphicon glyphicon-edit"></span>
			</button>
			<span ng-show="dateRivForm.$visible">
				<button type="submit" class="btn btn-primary" ng-disabled="dateRivForm.$waiting">
					<span class="glyphicon glyphicon-ok"></span>
				</button>
				<button type="button" class="btn btn-default" ng-disabled="dateRivForm.$waiting" ng-click="dateRivForm.$cancel()">
			 		<span class="glyphicon glyphicon-remove"></span>
				</button>
			</span>
		<% } %>
	</div>
</form>