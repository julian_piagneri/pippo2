<div class="col-xs-12" style="padding-bottom: 40px;">

	<div class="col-xs-12">
		<form editable-form name="portateForm" onaftersave="savePortate(portateForm);">
		<h5 class="detailsLabel">Portate Rivalutate</h5>
			<table class="dati-scheda">
				<tr>
					<th>Tempi di ritorno [anni]</th>
					<th>50</th>
					<th>100</th>
					<th>200</th>
					<th>500</th>
					<th>1000</th>
					<th>Fonte</th>
				</tr>
				<tr>
					<td>
						Portata max di afflusso [m3/s] 
					</td>
					<td>
						<span editable-text="portate50.portataAfflusso">{{portate50.portataAfflusso | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate100.portataAfflusso">{{portate100.portataAfflusso | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate200.portataAfflusso">{{portate200.portataAfflusso | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate500.portataAfflusso">{{portate500.portataAfflusso | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate1000.portataAfflusso">{{portate1000.portataAfflusso | number:2}}</span>
					</td>
					<td>
						<span ng-trim="false" e-required editable-select="datiPortateFonte.fonteAfflusso" e-ng-options="fonte.idFonte as fonte.fonte for fonte in datiAllFonte">
							{{getFonteName(datiPortateFonte.fonteAfflusso)}}
						</span>
					</td>
				</tr>
				<tr>
					<td>
						Portata max laminata [m3/s] 
					</td>
					<td>
						<span editable-text="portate50.portataLaminata">{{portate50.portataLaminata | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate100.portataLaminata">{{portate100.portataLaminata | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate200.portataLaminata">{{portate200.portataLaminata | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate500.portataLaminata">{{portate500.portataLaminata | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate1000.portataLaminata">{{portate1000.portataLaminata | number:2}}</span>
					</td>
					<td>
						<span editable-select="datiPortateFonte.fonteLaminata" e-ng-options="fonte.idFonte as fonte.fonte for fonte in datiAllFonte">
							{{getFonteName(datiPortateFonte.fonteLaminata)}}
						</span>
					</td>
				</tr>
				<tr>
					<td>
						Quota max invaso [m s.m] 
					</td>
					<td>
						<span editable-text="portate50.quotaInvaso">{{portate50.quotaInvaso | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate100.quotaInvaso">{{portate100.quotaInvaso | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate200.quotaInvaso">{{portate200.quotaInvaso | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate500.quotaInvaso">{{portate500.quotaInvaso | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate1000.quotaInvaso">{{portate1000.quotaInvaso | number:2}}</span>
					</td>
					<td>
						<span editable-select="datiPortateFonte.fonteInvaso" e-ng-options="fonte.idFonte as fonte.fonte for fonte in datiAllFonte">
							{{getFonteName(datiPortateFonte.fonteInvaso)}}
						</span>
					</td>
				</tr>
				<tr>
					<td>
						Franco [m] 
					</td>
					<td>
						<span editable-text="portate50.franco">{{portate50.franco | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate100.franco">{{portate100.franco | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate200.franco">{{portate200.franco | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate500.franco">{{portate500.franco | number:2}}</span>
					</td>
					<td>
						<span editable-text="portate1000.franco">{{portate1000.franco | number:2}}</span>
					</td>
					<td>
						<span editable-select="datiPortateFonte.fonteFranco" e-ng-options="fonte.idFonte as fonte.fonte for fonte in datiAllFonte">
							{{getFonteName(datiPortateFonte.fonteFranco)}}
						</span>
					</td>
				</tr>
			</table>
			<div class="col-xs-12 col-md-2 print-hide" style="padding-bottom: 5px;">
				<% if(canWrite) { %>
				<button type="button" class="btn btn-info" ng-click="portateForm.$show()" ng-show="!portateForm.$visible">
					<span class="glyphicon glyphicon-edit"></span>
				</button>
				<span ng-show="portateForm.$visible">
					<button type="submit" class="btn btn-primary" ng-disabled="portateForm.$waiting">
						<span class="glyphicon glyphicon-ok"></span>
					</button>
					<button type="button" class="btn btn-default" ng-disabled="portateForm.$waiting" ng-click="portateForm.$cancel()">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
				</span>
				<% } %>
			</div>
		</form>
	</div>
	<div class="col-xs-12 h5 hb"></div>
	<div class="col-xs-12">
		<h5 class="detailsLabel">Note</h5>
		<form editable-form name="portateNoteForm" onaftersave="savePortateNote(portateNoteForm);">
			<div class="col-xs-12">
				<span editable-textarea="portateNote.note" e-maxlength="4000">
					{{portateNote.note}}
				</span>
			</div>
			<div class="col-xs-12 col-md-2 print-hide" style="padding-bottom: 5px;">
				<% if(canWrite) { %>
				<button type="button" class="btn btn-info" ng-click="portateNoteForm.$show()" ng-show="!portateNoteForm.$visible">
					<span class="glyphicon glyphicon-edit"></span>
				</button>
				<span ng-show="portateNoteForm.$visible">
					<button type="submit" class="btn btn-primary" ng-disabled="portateNoteForm.$waiting">
						<span class="glyphicon glyphicon-ok"></span>
					</button>
					<button type="button" class="btn btn-default" ng-disabled="portateNoteForm.$waiting" ng-click="portateNoteForm.$cancel()">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
				</span>
				<% } %>
			</div>
		</form>
	</div>
	

</div>