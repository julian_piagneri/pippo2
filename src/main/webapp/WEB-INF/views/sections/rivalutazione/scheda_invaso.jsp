<div class="col-xs-12" style="padding-bottom: 20px;">
	<div class="col-xs-12 h5 hb"></div>
	<form editable-form name="invasoForm" onaftersave="saveIvaso(invasoForm);">
		<div class="row table detailsTable no-padding-children">
			<div class="col-xs-12 col-md-6">
				<div class="col-xs-12 col-sm-6 detailsLabel">Altitudine media bacino:</div>
				<div class="col-xs-12 col-sm-6">
					<span editable-text="datiInvaso.altitudineMediaBacina">{{datiInvaso.altitudineMediaBacina}}</span>
				</div>
				<div class="col-xs-12 col-sm-6 detailsLabel">Tempo di corrivazione:</div>
				<div class="col-xs-12 col-sm-6">
					<span editable-text="datiInvaso.tempoDiCorrivizione">{{datiInvaso.tempoDiCorrivizione}}</span>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="col-xs-12 col-sm-6 detailsLabel"> Franco netto minimo DM 1982:</div>
				<div class="col-xs-12 col-sm-6">
					<span editable-text="datiInvaso.francoNettoMinimoDM1982">{{datiInvaso.francoNettoMinimoDM1982}}</span>
				</div>
				<div class="col-xs-12 col-sm-6 detailsLabel"> Franco netto minimo Schema NTDighe:</div>
				<div class="col-xs-12 col-sm-6">
					<span editable-text="datiInvaso.francoNettoMinimoSchemaNtdighe">{{datiInvaso.francoNettoMinimoSchemaNtdighe}}</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-2 print-hide" style="padding-bottom: 5px;">
			<% if(canWrite) { %>
			<button type="button" class="btn btn-info" ng-click="invasoForm.$show()" ng-show="!invasoForm.$visible">
				<span class="glyphicon glyphicon-edit"></span>
			</button>
			<span ng-show="invasoForm.$visible">
				<button type="submit" class="btn btn-primary" ng-disabled="invasoForm.$waiting">
					<span class="glyphicon glyphicon-ok"></span>
				</button>
				<button type="button" class="btn btn-default" ng-disabled="invasoForm.$waiting" ng-click="invasoForm.$cancel()">
					<span class="glyphicon glyphicon-remove"></span>
				</button>
			</span>
			<% } %>
		</div>
	</form>
</div>










