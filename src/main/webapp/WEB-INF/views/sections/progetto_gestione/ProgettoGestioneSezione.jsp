<div class="col-xs-12" style="padding-bottom: 40px;">
	<div class="col-xs-12 col-md-12">
		<form editable-form name="progettoForm" onaftersave="save(progettoForm);">
			<div class="col-xs-12 col-md-12">
				<div class="col-xs-12 col-md-12">
					<div class="col-xs-6 col-md-6">
						<h4>Data Presentazione</h4>
					</div>
					<div class="col-xs-6 col-md-6">
						<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="progettoForm.$visible" ng-model="datiProg.dataPresentazione" datepicker-popup="dd/MM/yyyy" is-open="datePickerCtrl.presentazione" ng-focus="datePickerCtrl.presentazione = true" ng-click="datePickerCtrl.presentazione = true" placeholder="Data Presentazione" current-text="Oggi" clear-text="Cancella" close-text="Chiudi"/>
						<span ng-show="!progettoForm.$visible">{{datiProg.dataPresentazione}}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-12">
					<div class="col-xs-6 col-md-6">
						<h4>Data Parere</h4>
					</div>
					<div class="col-xs-6 col-md-6">
						<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="progettoForm.$visible" ng-model="datiProg.dataParere" datepicker-popup="dd/MM/yyyy" is-open="datePickerCtrl.parere" ng-focus="datePickerCtrl.parere = true" ng-click="datePickerCtrl.parere = true" placeholder="Data Parere" current-text="Oggi" clear-text="Cancella" close-text="Chiudi" />
						<span ng-show="!progettoForm.$visible">{{datiProg.dataParere}}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-12">
					<div class="col-xs-6 col-md-6">
						<h4>Data Approvazione</h4>
					</div>
					<div class="col-xs-6 col-md-6">
						<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="progettoForm.$visible" ng-model="datiProg.dataApprovazione" datepicker-popup="dd/MM/yyyy" is-open="datePickerCtrl.approvazione" ng-focus="datePickerCtrl.approvazione = true" ng-click="datePickerCtrl.approvazione = true" placeholder="Data Approvazione" current-text="Oggi" clear-text="Cancella" close-text="Chiudi" />
						<span ng-show="!progettoForm.$visible">{{datiProg.dataApprovazione}}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-12">
					<div class="col-xs-6 col-md-6">
						<h4>Data Rilievo</h4>
					</div>
					<div class="col-xs-6 col-md-6">
						<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="progettoForm.$visible" ng-model="datiProg.dataRilievo" datepicker-popup="dd/MM/yyyy" is-open="datePickerCtrl.rilievo" ng-focus="datePickerCtrl.rilievo = true" ng-click="datePickerCtrl.rilievo = true" placeholder="Data Rilievo" current-text="Oggi" clear-text="Cancella" close-text="Chiudi" />
						<span ng-show="!progettoForm.$visible">{{datiProg.dataRilievo}}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-12">
					<div class="col-xs-6 col-md-6">
						<h4>Volume Totale</h4>
					</div>
					<div class="col-xs-6 col-md-6">
						<span editable-text="datiProg.volumeTotale">{{datiProg.volumeTotale | number:2}}</span>
					</div>
				</div>
				<br />
				<div class="col-xs-12 col-md-12">
					<h4>Note</h4>
					<span class="col-xs-12 col-md-12" editable-textarea="datiProg.note" e-maxlength="4000">{{datiProg.note}}</span>
				</div>
			</div>
			<br />
			<br />
			<div class="col-xs-12">&nbsp;</div>
			<div class="col-xs-12 print-hide" style="padding-bottom: 5px;">
				<% if(canWrite) { %>
				<button type="button" class="btn btn-info" ng-click="progettoForm.$show()" ng-show="!progettoForm.$visible">
					<span class="glyphicon glyphicon-edit"></span>
				</button>
				<span ng-show="progettoForm.$visible">
					<button type="submit" class="btn btn-primary" ng-disabled="progettoForm.$waiting">
						<span class="glyphicon glyphicon-ok"></span>
					</button>
					<button type="button" class="btn btn-default" ng-disabled="progettoForm.$waiting" ng-click="progettoForm.$cancel()">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
				</span>
				<% } %>
			</div>
		</form>
	</div>
</div>