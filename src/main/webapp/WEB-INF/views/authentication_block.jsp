<%@page import="it.cinqueemmeinfo.dammap.utility.EncodingUtil"%>
<%@page import="it.cinqueemmeinfo.dammap.utility.UserUtility"%>
<%@page import="it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean"%>
<%@page import="it.cinqueemmeinfo.dammap.security.TokenAuthenticationService"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ include file="getUserRigth.jsp" %>
<%@page import="it.cinqueemmeinfo.dammap.dao.UserService"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%! public UserService userService = null; %>
<%! public String toBePrinted = ""; %>
<%
// boolean bb = getRightOnDam(USERID, 'concessionario', IDiga);
// boolean bb = getRightOnDam(USERID, 'gestore', IDiga);
//dammap/authblock

ApplicationContext ac = RequestContextUtils.getWebApplicationContext(request);
userService = (UserService)ac.getBean("userService");//request.getAttribute("userService");



String token = "";
String userId = "";
String tokenREST = "";
UserSecurityBean user=null;

if(request.getParameter("t") != null) token = request.getParameter("t");

UserUtility AuthObj = new UserUtility();
UserRightsEntry edu = null;
RightNode permessi = null;
user=AuthObj.getUserDetails();


boolean logged = false;

if(user != null){
	userId=user.getUsername();
	logged = true;
	tokenREST = token;
	session.setAttribute("userId", userId);
	session.setAttribute("tokenREST", tokenREST);
	session.setAttribute("logged", "1");
}
%>
<%!

public boolean UcanWrite(UserUtility _AuthObj, String _idScheda){
	return _AuthObj.hasRole(_idScheda+"-S-W");
}


public boolean UcanWriteDam(String _dam, String _idScheda){
	ArrayList<String> _permessi = null;
	try{
		_permessi = (ArrayList<String>)userService.getUserAccessRigthsForDam(_dam, _idScheda);
	} catch(Exception e){}
	
	toBePrinted = _permessi.toString();
	if(_permessi.get(0).equals("W")) return true;
	return false;
}


public boolean UcanRead(UserUtility _AuthObj, String _idScheda){
	return _AuthObj.hasRole(_idScheda+"-S-W") || _AuthObj.hasRole(_idScheda+"-S-R");
}

public boolean UcanReadDam(String _dam, String _idScheda){
	ArrayList<String> _permessi = null;
	try{
		_permessi = (ArrayList<String>)userService.getUserAccessRigthsForDam(_dam, _idScheda);
	}
	catch(Exception e){}
	if(_permessi.get(0).equals("W")) return true;
	if(_permessi.get(0).equals("R")) return true;
	return false;
}

%>