<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="damMapApp">
	<head>
		<title>Home</title>
		<script type="text/javascript" src="resources/angularjs/v1.2.26/angular.min.js"></script>
		<script type="text/javascript" src="resources/scripts/app.js"></script>
	</head>
	<body>
		<div ng-controller="homeController">
			<h1>
				Hello world!  
			</h1>
			
			<P>  The time on the server is {{serverTime}}. </P>
			
			<p>  This server base url is {{baseUrl}} </p> 
			<%
				Object obj = getServletContext().getAttribute("userRights");
				String id = String.valueOf(getServletContext().getAttribute("userId"));
				out.println("edu value="+obj);
				out.println("\nid value="+id);
			%>
		</div>
	</body>
</html>