<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String schedaId = "44";
String percorsoMenu = "\\Protezione Civile\\Onde di Piena";
String titoloPagina = "Scheda Onde di Piena";
// boolean canWrite=false;
// boolean canRead=false;
%>
<%@ include file="user_permissions.jsp" %>

<%
String appName="ondePienaModule";
String controller = "ondePienaController";
%>

<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-grid.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/dialogs.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-table.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/xeditable.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-plain.css">
		<link rel="stylesheet" type="text/css" href="resources/style/main.css">
		<link rel="stylesheet" type="text/css" href="resources/style/text-align-responsive.css">
		<link rel="stylesheet" type="text/css" href="resources/style/damlist.css">
	  	<%@ include file="scripts_block.jsp" %>
		<script type="text/javascript">
			G_token = "<%=tokenREST%>";
			G_userID = "<%=userId%>";
			G_damID = '<%=request.getParameter("dam")%>'
		</script>
		
		<script type="text/javascript" src="resources/scripts/ondeInit.js"></script>
		<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
		<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
		<script type="text/javascript" src="resources/scripts/services/helpersService.js"></script>
		<script type="text/javascript" src="resources/scripts/ondeApp.js"></script>

	</head>
		<body ng-app="<%=appName%>" id="<%=appName%>">
		<%@ include file="header-logo.jsp" %>
		<p class="TitoloScheda" align="CENTER"> <%=titoloPagina%> </p>

			<div class="container-fluid" style="position: relative">
			<div ng-controller="<%=controller%>" class="row" id="<%=controller%>">
				<div class="col-xs-12 text-center" style="border-top: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; margin-top: 5px; margin-bottom: 5px; padding: 5px;">
					<div class="col-sm-3 col-xs-12"><span class="detailsLabel mainInfo">ID Diga:</span> <span>{{datiDiga.id}}</span></div> 
				</div>
				
				<div class="col-xs-12 col-sm-2 h4">
					Tipo di Onda :
				</div>
				<div class="col-xs-12 col-sm-5">
					<!--<select class="form-control" id="schedeFrane" ng-model="currentFrana" ng-change="switchSchedaById(currentFrana)">
						<option ng-selected="frana.idSchedaFenomeniFranosi == currentFrana" ng-repeat="frana in fenFranosiSchede" value="{{frana.idSchedaFenomeniFranosi}}">{{frana.descrizione}}</option>
					</select>-->
					<select class="form-control" id="ondePiena" ng-model="currentOnda" ng-change="switchOndaById(currentOnda)">
						<option ng-selected="onde.id == currentOnda" ng-repeat="onde in ondePiena.tipoOnda" value="{{onde.id}}">{{onde.nome}}</option>
					</select>
					
				</div>
				<div class="no-margins col-xs-12">
					<button class="btn btn-primary" type="button" style="float: right; margin-right: 25px;" ng-click="openCartografico()">
						<span class="glyphicon glyphicon-globe"></span>
					</button>
				</div>
			</div>	

	</body>
						
	
</html
>