<div class="content-fluid">
	<div class="row">
		<div class="col-xs-12 text-right">
			<button class="btn btn-primary" ng-click="dialogService.closeListDialog()" style="" type="button">
				<span class="glyphicon glyphicon-remove" title="Chiudi"></span>
			</button>

		</div>

		<div class="col-xs-12 text-center">
			<span class="col-xs-12" style="white-space:pre-line;"><h2>{{overlayListTitle}}</h2></span>
		</div>

		<div class="col-xs-6" ng-repeat="name in overlayListName">
 <!--style="height:380px;"-->
			<div class="col-xs-12" style="height:380px;">
				<div class="col-xs-12 text-left">
					<h2>{{overlayListName[$index]}}</h2>
				</div>
				<div class="col-xs-12">
					<ul class="row-xs-12">
						<table class="table">
							<li ng-repeat="element in getSubArrayOverlayList(currentPage*pageSizeOverList, currentPage*pageSizeOverList+pageSizeOverList, overlayListValues[$index]) | limitTo:pageSizeOverList">
									{{element}}</li>
						</table>
					</ul> 
				</div>
				<div class="col-xs-12">
					<button class="btn btn-info btn-xs" ng-disabled="currentPage <= 0" ng-click="currentPage=currentPage-1"><<</button>
					<button class="btn btn-info btn-xs" ng-disabled="currentPage == numberOfPagesOverList(overlayListValues[$index])-1" ng-click="currentPage=currentPage+1">>></button>
					Pagina {{currentPage+1}} di {{numberOfPagesOverList(overlayListValues[$index])}}
				</div>
			</div>

		</div>
		</br>
	</div>
</div>

		<script type="text/javascript" src="resources/scripts/cartoApp.js"></script>