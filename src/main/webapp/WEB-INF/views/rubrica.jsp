<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String schedaId = "24";
String percorsoMenu = "\\Protezione Civile\\Rubrica Telefonica";
String titoloPagina = "Rubrica<BR />Documento di Protezione civile";
boolean canWrite=false;
boolean canRead=false;
boolean canWriteAsConcess=false;
boolean canWriteIng=false;

boolean canWriteDettagli=false;
boolean canWriteAnagrafica=false;
%>
<%@ include file="user_permissions.jsp" %>
<%
//CompatibilitÃ  con vecchio sistema. I nuovi permessi vanno presi direttamente dalla mappa
canWrite=permessiRubrica.get("canWrite");
canRead=permessiRubrica.get("canRead");
canWriteAsConcess=permessiRubrica.get("canWriteAsConcess");
// canWriteAsGestore=permessiRubrica.get("canWriteAsGestore");
canWriteIng=permessiRubrica.get("canWriteIng");

canWriteDettagli=permessiAnagrafica.get("canWriteDettagli") || canWrite;
canWriteAnagrafica=permessiAnagrafica.get("canWrite");
// if() 
// if(canWrite) canWriteDettagli = true;
//if(logged) {
//	canWrite = UcanWrite(AuthObj, schedaId);
//	canRead = UcanRead(AuthObj, schedaId);
//	canWriteAsConcess = userService.checkDamWriteRigthsForConcessionario(session.getAttribute("userId").toString(), request.getParameter("dam"));
//	canWriteDettagli = UcanWrite(AuthObj, schedaIdDettagli);
//	canWriteAnagrafica = UcanWrite(AuthObj, schedaIdAnagrafica);
//	if(canWriteAsConcess) canWrite = true;
//	if(canWriteAsConcess) canWriteIng = true;
//	if(canWrite) canWriteIng = true;
// if(canWrite) canWriteAnagrafica = true;
// if(canWrite) canWriteDettagli = true;
	
	////////////////////
	 //canWriteIng = true;
	 //canWrite = true;
	 //canRead = true;
	////////////////////
//}
String appName="rubricaApp";
%>
<%@ include file="header_block.jsp" %>
<% if(canRead) { %>
<div class="container-fluid" style="position: relative">
	<div ng-controller="rubricaController" class="row" id="rubrica">
	
		<div class="col-xs-12">
			<div class="col-xs-12 text-center" style="border-top: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; margin-top: 5px; margin-bottom: 5px; padding: 5px;">
				<div class="col-sm-3 col-xs-12"><span class="detailsLabel mainInfo">N&deg; Archivio:</span> <span>{{datiRubrica.numeroArchivio}}</span></div> 
				<div class="col-sm-3 col-xs-12"><span class="detailsLabel mainInfo">Sub:</span> <span e-size="5">{{datiRubrica.sub}}</span></div>
				<div class="col-sm-6 col-xs-12"><span class="detailsLabel mainInfo">Diga:</span> <span>{{datiRubrica.nomeDiga}}</span></div> 
			</div>
			<div class="col-xs-12 text-center print-hide">
				<div class="btn-group" dropdown is-open="dropdownInvaso.isopen">
					<button type="button" class="btn btn-default dropdown-toggle" dropdown-toggle>
						Dighe dello stesso invaso <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li ng-repeat="digaInvaso in digheInvaso">{{digaInvaso}}</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-xs-12" style="margin-top: 10px;">
			<accordion close-others="false">
				<accordion-group heading="MIT Direzione Generale per le Dighe e le Infrastrutture Idriche ed Elettriche">
					<%@ include file="sections/rubrica/mit.jsp" %>
				</accordion-group>
				<accordion-group heading="Concessionario - Gestore">
					<%@ include file="sections/rubrica/concgest.jsp" %>
				</accordion-group>
				<accordion-group heading="Amministrazioni Destinatarie Comunicazioni D.P.C.">
					<%@ include file="sections/rubrica/dpc.jsp" %>
				</accordion-group>
				<accordion-group heading="Altri enti">
					<%@ include file="sections/rubrica/altri.jsp" %>
				</accordion-group>
			</accordion>
		</div>
		<div class="col-xs-12 text-center">
			<div class="col-xs-6">
				<button type="button" class="btn btn-info print-hide" onClick="window.print();">
					<span class="glyphicon glyphicon-print"></span>
				</button>
			</div>
			<div class="col-xs-6">
				<%@ include file="blocks/close.jsp" %>
			</div>
		</div>
	</div>
</div>
	<script type="text/javascript">
		G_token = "<%=tokenREST%>";
		G_userID = "<%=userId%>";
		G_damID = '<%=request.getParameter("dam")%>'
	</script>
	<script type="text/ng-template" id="addIngTemplate"><%@ include file="popup_add_ing.jsp" %></script>
	
	<%@ include file="js_user_permissions.jsp" %>
	<%@ include file="scripts_block.jsp" %>

	<script type="text/javascript" src="resources/scripts/rubricaInit.js"></script>
	<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
	<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
	<script type="text/javascript" src="resources/scripts/rubricaApp.js"></script>
<% } else{ %>
	<script type="text/javascript">
		window.close();
	</script>
<% } %>
	</body>
</HTML>
