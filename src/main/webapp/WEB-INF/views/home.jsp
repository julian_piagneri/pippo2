<% String title="Home"; %>
<%@ include file="include/header.jsp" %>
<body ng-controller="AuthCtrl" ng-init="init('routeHome', '')">
	<link id="size-css" rel="stylesheet" type="text/css" ng-href="{{storageService.get('styles.size')}}">
	<link id="size-css" rel="stylesheet" type="text/css" ng-href="{{storageService.get('styles.color')}}">
	<nav class="navbar navbar-fixed-top no-margins navbar-static-xs navbar-custom" role="navigation"  ng-show="storageService.get('login.authenticated', false)">
		<div ng-show="storageService.get('login.authenticated', false)" class="container-fluid no-margins">
			<div class="row navbar-gradient" style="margin: 0">
				<div class="col-xs-12">
					<div class="navbar-header col-xs-12 no-margins">
						<div class="col-sm-7 col-xs-12 no-margins">
						<img src="resources/img/ridheader.png" style="margin-left: -15px; width: 100%; max-width: 358px; height: auto;" />
						</div>
						<div class="col-xs-4 no-margins hidden-md hidden-lg">
						<button style="float: none;" type="button" class="navbar-toggle" ng-init="isCollapsed = true" ng-click="isCollapsed = !isCollapsed">
							<span class="sr-only">Toggle navigation</span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			            </button>
			            </div>
						<div class="col-sm-5 col-xs-8 text-right" ng-show="storageService.get('login.authenticated', false)">
							<span>User: </span><span class="detailsLabel">{{storageService.get('login.username', '')}}</span>
						</div>
					</div>
			    </div>
				<div id="navbar-menu" ma-menu="menu" class="col-xs-12 navbar-collapse dropdown" ng-class="{collapse: isCollapsed}">
				</div>
			</div>
		</div>
	</nav>
	<div class="navbar-placeholder"></div>
	<div ng-include="content"></div>
<%@ include file="include/footer.jsp" %>