<div class="container-fluid" style="position: relative">
    <div ng-controller="damListController" ng-init="initDamList()" class="row">
		<div class="col-xs-12">
		
		</div>
		
		<div id="home-content-page" class="col-xs-12" style="margin-bottom: 10px; margin-top: 10px; margin-left: -2px;">
		<accordion close-others="false">
			<%@ include file="include/dam-details.jsp" %>
			<%@ include file="include/dam-filters.jsp" %>
			<%@ include file="include/dam-reporting.jsp" %>
			<%@ include file="include/map-tools.jsp" %>
		</accordion>
		</div>
		<div class="no-margins col-xs-12">
			<button class="btn btn-primary" type="button" style="float: right; margin-right: 25px;" ng-click="openCartografico()">
				<span class="glyphicon glyphicon-globe"></span>
			</button>
		</div>
		<div class="no-margins col-xs-12">
			<div id="dam-list-list" class="col-xs-12 col-sm-12 col-md-10" style="margin-top: 10px;">
				<div class="dams_list" id="damList"> 
					<div class="gridStyle damGrid" ui-grid-save-state ui-grid-resize-columns ui-grid-auto-resize ui-grid-selection ui-grid-move-columns ui-grid-grouping ui-grid="damGrid"></div>
				</div> 
			</div>
			<!-- div id="resizer" class="resizer ui-draggable ui-draggable-handle hidden-xs hidden-sm col-md-1" >
				<span></span>
			</div -->
			<div  id="dam-list-map" class="col-xs-12 col-sm-12 col-md-2" style="margin-top: 10px;">
				<div id="layerList" ></div>
				<div id="map" class="map"></div>
			</div>
		</div>

		<div class="col-xs-12" style="margin-bottom: 10px; margin-top: 10px; margin-left: -2px;">
		</div>
	</div>
	<div style="height: 10px;"></div>
	<div class="col-xs-8"  style="display:none;">
	<select id="layer-select">
		<option value="Road">Bing Road</option>
		<option value="Aerial">Bing Aerial</option>
		<option value="AerialWithLabels" selected>Bing Aerial with labels</option>
		<option value="OSM">OSM Road</option>
		<option value="SAT">OSM SAT</option>
		<option value="OSM2">OSM2</option>
		<option value="GooglePhysical">Google Physical</option>
		<option value="GoogleSatellite">Google Satellite</option>
		<option value="GoogleHybrid">Google Hybrid</option>
		<option value="GoogleStreet">Google Street</option>
		<option value="GoogleWaterOverlay">Google WaterOverlay</option>
		<option value="HereStreet">Here Street</option>
		<option value="HereSatellite">Here Satellite</option>
		<option value="HereHybrid">Here Hybrid</option>
		<option value="HereTraffic">Here Traffic</option>
	</select>
	</div>
</div>
<script>
function changeStyles(font, color){
	var scope = angular.element("#home-content-page").scope();
	var sizes=scope.storageService.get("sizes");
	var colors=scope.storageService.get("colors");
	scope.storageService.set("styles.size", damRoutes.routeDynCss+"size/"+sizes[font]);
	scope.storageService.set("styles.color", damRoutes.routeDynCss+"color/"+colors[color]);
	scope.$apply();
	//console.log(scope.storageService.get("styles.size"));
	//var applo=$("<link></link>")
	//	.attr('id', "size-css")
	//	.attr("rel", "stylesheet")
	//	.attr("type", "text/css")
	//	.attr("href", "resources/style/dynamic/size/"+styles[font]);
	//	$('head').append(applo);
}

$(document).ready(function(){
	var scope = angular.element("#home-content-page").scope();
	//console.log(scope);
	if(false){
		var applo=$("<link></link>")
		.attr('id', "size-css")
		.attr("rel", "stylesheet")
		.attr("type", "text/css")
		.attr("href", "resources/style/dynamic/size/large.css");
		$('head').append(applo);
		setTimeout(function(){ 
			$("#size-css").attr("href", "resources/style/dynamic/size/small.css");
			setTimeout(function(){ $("#size-css").attr("href", "resources/style/dynamic/size/medium.css"); }, 30000);
		}, 30000);
	}
});
</script>