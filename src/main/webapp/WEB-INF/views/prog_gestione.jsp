<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String schedaId = "483";
String percorsoMenu = "\\Gestione Tecnica\\Progetti di Gestione";
String titoloPagina = "Scheda Progetti di Gestione";
boolean canWrite=false;
boolean canRead=false;
%>
<%@ include file="user_permissions.jsp" %>
<%
canWrite=permessiProgGestione.get("canWrite");
canRead=permessiProgGestione.get("canRead");

//test
// canRead = true;
// canWrite = true;

String appName = "ProgettoGestioneApp";
String controller = "ProgettoGestioneController";
%>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-grid.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/dialogs.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-table.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/xeditable.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-plain.css">
		<link rel="stylesheet" type="text/css" href="resources/style/main.css">
		<link rel="stylesheet" type="text/css" href="resources/style/text-align-responsive.css">
		<link rel="stylesheet" type="text/css" href="resources/style/damlist.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon-regular.css">
        <link rel="stylesheet" type="text/css" href="resources/style/scheda-print.css" media="print" >
		<%@ include file="scripts_block.jsp" %>
		<script type="text/javascript">
			G_token = "<%=tokenREST%>";
			G_userID = "<%=userId%>";
			G_damID = '<%=request.getParameter("dam")%>'
		</script>
		<script type="text/ng-template" id="addIngTemplate"><%@ include file="popup_add_ing.jsp" %></script>
		<%@ include file="js_user_permissions.jsp" %>
		<script type="text/javascript" src="resources/scripts/ProgettoGestioneModule.js"></script>
		<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
		<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
		<script type="text/javascript" src="resources/scripts/ProgettoGestioneApp.js"></script>
	</head>
	<body ng-app="<%=appName%>" id="<%=appName%>">
		<%@ include file="header-logo.jsp" %>
		<p class="TitoloScheda" align="CENTER"> <%=titoloPagina%> </p>
		<% if(canRead) { %>
			<div class="container-fluid" style="position: relative">
                <div ng-controller="<%=controller%>" class="row" id="<%=controller%>">
                    <header class="col-xs-12">
                        <div class="col-xs-12 text-center" style="border-top: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; margin-top: 5px; margin-bottom: 5px; padding: 5px;">
                            <jsp:include page="header.jsp"></jsp:include>
                        </div>
                    </header>
                    <div class="col-xs-12" style="margin-top: 10px;">
                        <accordion close-others="false">
                            <accordion-group heading="Progetti di Gestione" is-open="true">
                                <%@ include file="sections/progetto_gestione/ProgettoGestioneSezione.jsp" %>
                            </accordion-group>
                        </accordion>
                    </div>

                    <div class="col-xs-12 text-center">
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-info print-hide" onClick="window.print();">
                                <span class="glyphicon glyphicon-print"></span>
                            </button>
                        </div>
                        <div class="col-xs-6">
                            <%@ include file="blocks/close.jsp" %>
                        </div>
                    </div>
                </div>
				</div>
			</div>	
		<% } else { %>
			<script type="text/javascript">
				window.close();
			</script>
		<% } %>
	</body>
</html>