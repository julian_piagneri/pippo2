<% 
boolean canWrite = false;
if(request.getParameter("canWrite") != null){
	if(request.getParameter("canWrite").equals("true")) canWrite = true;
}

boolean canWriteDettagli = false;
if(request.getParameter("canWriteDettagli") != null){
	if(request.getParameter("canWriteDettagli").equals("true")) canWriteDettagli = true;
}

if(canWrite) canWriteDettagli = true;

%>
<tr><td class="h5">Contatti</td></tr>
<tr>
	<td>
		<table>
			<tr class="detailsRow" ng-repeat="contatto in (${param.mainArray} | orderBy : ['contactType', 'order'] : false)">
				<td>
					<form editable-form name="contattoForm" shown="contatto.isNew" onaftersave="saveContatto(${param.keyREST}, ${param.idSogg}, contatto, contattoForm, ${param.soggetto});" oncancel="removeNewEmptyRecord($index, ${param.mainArray}, contatto.isNew, contatto.id);">
						<span e-form="contattoForm" editable-select="contatto.contactType" e-ng-options="tipoC.id as tipoC.nome for tipoC in tipiContatto">{{nomeTipoContatto(contatto.contactType)}}</span>&nbsp;&nbsp;
					</form>
					
				</td>
				<td>
					<span e-placeholder="Contatto" e-form="contattoForm" editable-text="contatto.contactInfo">{{contatto.contactInfo || 'nuovo contatto' }}</span>&nbsp;&nbsp;
				</td>
				<td>
					<button e-form="contattoForm" type="button" class="btn btn-info" ng-click="contattoForm.$show()" ng-show="!contattoForm.$visible && canWriteDettagli('${param.UtenteSoggArray}', ${param.idSogg})">
						<span class="glyphicon glyphicon-edit"></span>
					</button>
					<% if(canWrite) { %>
					<button e-form="contattoForm" type="button" class="btn btn-danger" ng-click="deleteContatto(${param.keyREST}, ${param.idSogg}, contatto, ${param.mainArray})" ng-show="!contattoForm.$visible">
						<span class="glyphicon glyphicon-trash"></span>
					</button>
					<% } %>
					<span ng-show="contattoForm.$visible">
						<button e-form="contattoForm" type="submit" class="btn btn-primary" ng-disabled="contattoForm.$waiting" ng-click="contattoForm.$submit()">
							<span class="glyphicon glyphicon-ok"></span>
						</button>
						<button e-form="contattoForm" type="button" class="btn btn-default" ng-disabled="contattoForm.$waiting" ng-click="contattoForm.$cancel()">
							<span class="glyphicon glyphicon-remove"></span>
						</button>
					</span>
				</td>
			</tr>
		</table>
		
	</td>
	
</tr>
<% if(canWrite) { %><tr><td><button class="btn btn-info btn-add" ng-click="addContatto(${param.mainArray});"><span class="glyphicon glyphicon-plus"></span></button></td></tr><% } %>
