<%@ include file="authentication_block.jsp" %>
<%
ArrayList<String> autoritaWrite = new ArrayList<String>();
ArrayList<String> concessWrite = new ArrayList<String>();
ArrayList<String> gestoreWrite = new ArrayList<String>();
ArrayList<String> altroEnteWrite = new ArrayList<String>();
String anagraficaId="1100"; // "59";
String rubricaId="24";
String rubricaEsterniId="34";
String dettagliId="2100"; //"69";
String reportBuilderId="57";

String rivalutazioniId="183";
String classSismicaId="283";
String fenFranosiId="383";
String progGestioneId="483";

Map <String, Boolean> permessiRubrica=new HashMap<String, Boolean>();
permessiRubrica.put("canWrite", false);
permessiRubrica.put("canRead", false);
permessiRubrica.put("canReadUpdate", false);
permessiRubrica.put("canWriteAsConcess", false);
permessiRubrica.put("canWriteIng", false);

Map<String, Boolean> permessiRivalutazione = new HashMap<String, Boolean>();
permessiRivalutazione.put("canWrite", false);
permessiRivalutazione.put("canRead", false);

Map<String, Boolean> permessiClassSismica = new HashMap<String, Boolean>();
permessiClassSismica.put("canWrite", false);
permessiClassSismica.put("canRead", false);

Map<String, Boolean> permessiFenFranosi = new HashMap<String, Boolean>();
permessiFenFranosi.put("canWrite", false);
permessiFenFranosi.put("canRead", false);

Map<String, Boolean> permessiProgGestione = new HashMap<String, Boolean>();
permessiProgGestione.put("canWrite", false);
permessiProgGestione.put("canRead", false);

Map <String, Boolean> permessiAnagrafica=new HashMap<String, Boolean>();
permessiAnagrafica.put("canWrite", false);
permessiAnagrafica.put("canRead", false);
permessiAnagrafica.put("canWriteDettagli", false);


Map <String, Boolean> permessiReportBuilder=new HashMap<String, Boolean>();
permessiReportBuilder.put("canWrite", false);
permessiReportBuilder.put("canRead", false);


if(logged) {
	autoritaWrite = userService.getUserRightsForAutorita();
	concessWrite = userService.getUserRightsForConcessionario();
	gestoreWrite = userService.getUserRightsForGestore();
	altroEnteWrite = userService.getUserRightsForAltraEnte();
	
	//Modifica i permessi della rubrica
	if(request.getParameterMap().containsKey("dam")){
		
		
		// permessiRubrica.get("canWriteAsConcess")
		
		// RUBRICA //
		permessiRubrica.put("canWriteAsConcess", userService.checkDamWriteRigthsForConcessionario(request.getParameter("dam")));
		permessiRubrica.put("canWriteAsGestore", userService.checkDamWriteRigthsForGestore(request.getParameter("dam")));
		
		permessiRubrica.put("canWrite", UcanWriteDam(request.getParameter("dam"), rubricaId) || UcanWriteDam(request.getParameter("dam"), rubricaEsterniId));
		
		permessiRubrica.put("canRead", UcanReadDam(request.getParameter("dam"), rubricaId) || UcanReadDam(request.getParameter("dam"), rubricaEsterniId) || permessiRubrica.get("canWriteAsConcess") || permessiRubrica.get("canWriteAsGestore"));
		permessiRubrica.put("canReadUpdate", UcanReadDam(request.getParameter("dam"), rubricaId));
		
		permessiRubrica.put("canWriteIng",  permessiRubrica.get("canWrite") || permessiRubrica.get("canWriteAsConcess") || permessiRubrica.get("canWriteAsGestore"));
		
		
		// RIVALUTAZIONI //
		permessiRivalutazione.put("canWrite", UcanWriteDam(request.getParameter("dam"), rivalutazioniId));
		permessiRivalutazione.put("canRead", UcanReadDam(request.getParameter("dam"), rivalutazioniId));
		
		// CLASSIFICAZIONE SISMICA //
		permessiClassSismica.put("canWrite", UcanWriteDam(request.getParameter("dam"), classSismicaId));
		permessiClassSismica.put("canRead", UcanReadDam(request.getParameter("dam"), classSismicaId));
		
		// FENOMENI FRANOSI //
		permessiFenFranosi.put("canWrite", UcanWriteDam(request.getParameter("dam"), fenFranosiId));
		permessiFenFranosi.put("canRead", UcanReadDam(request.getParameter("dam"), fenFranosiId));
		
		// PROGETTI GESTIONE //
		permessiProgGestione.put("canWrite", UcanWriteDam(request.getParameter("dam"), progGestioneId));
		permessiProgGestione.put("canRead", UcanReadDam(request.getParameter("dam"), progGestioneId));
		
		
	}
	
	//if(canWriteAsConcess) canWrite = true;
	//if(canWriteAsConcess) canWriteIng = true;
	//if(canWrite) canWriteIng = true;
	//if(canWrite) canWriteAnagrafica = true;
	//if(canWrite) canWriteDettagli = true;
	
		
	// REPORT BUILDER //
	permessiReportBuilder.put("canWrite", UcanWrite(AuthObj, reportBuilderId));
	permessiReportBuilder.put("canRead", UcanRead(AuthObj, reportBuilderId));
	
	
	// ANAGRAFICA //
	permessiAnagrafica.put("canWrite", UcanWrite(AuthObj, anagraficaId));
	permessiAnagrafica.put("canWriteDettagli", UcanWrite(AuthObj, dettagliId) || permessiAnagrafica.get("canWrite"));
	permessiAnagrafica.put("canRead", UcanRead(AuthObj, anagraficaId));
	//canWrite = UcanWrite(AuthObj, schedaId);
	//canWriteDettagli = UcanWrite(AuthObj, schedaIdDettagli);
	//canRead = UcanRead(AuthObj, schedaId);
	//if(canWrite) canWriteDettagli = true;
	
}
%>