<div class="container">
	<form class="form-signin" action="#">
		<h2 class="form-signin-heading">Effettuare l'accesso</h2>
		<label for="un" class="sr-only">Nome utente</label>
		<input id="un" type="text" class="form-control" placeholder="Nome utente" ng-model="child.username" required autofocus>
		<label for="pw" class="sr-only">Password</label>
		<input id="pw" type="password" class="form-control" placeholder="Password" ng-model="child.password" required>
		<button class="btn btn-lg btn-primary btn-block" type="submit" onclick="return false;" ng-click="login()">Login</button>
	</form>
</div>