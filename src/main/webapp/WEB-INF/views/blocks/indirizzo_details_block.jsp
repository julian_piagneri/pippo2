<% 
boolean canWrite = false;
if(request.getParameter("canWrite") != null){
	if(request.getParameter("canWrite").equals("true")) canWrite = true;
}

boolean canWriteDettagli = false;
if(request.getParameter("canWriteDettagli") != null){
	if(request.getParameter("canWriteDettagli").equals("true")) canWriteDettagli = true;
}

boolean isIng = false;
if(request.getParameter("isIng") != null){
	if(request.getParameter("isIng").equals("true")) isIng = true;
}

if(canWrite) canWriteDettagli = true;

%>
<div class="col-xs-12 h5">Indirizzi</div>
<div class="detailsRow" ng-repeat="indirizzo in (${param.mainArray} | orderBy : 'order' : false)">
	<form editable-form shown="indirizzo.isNew" name="indirizzoForm" onaftersave="saveIndirizzo(${param.keyREST}, ${param.idSogg}, indirizzo, indirizzoForm, ${param.soggetto});" oncancel="removeNewEmptyRecord($index, ${param.mainArray}, indirizzo.isNew, indirizzo.id);">
		<div class="col-xs-12 col-md-10 no-margins">
			<div class="col-xs-12 col-sm-12 col-lg-5 no-margins">
				<div class="col-xs-12 col-sm-6 col-lg-8">
					<span class="detailsLabel">Indirizzo:</span> 
					<span editable-text="indirizzo.indirizzo">{{indirizzo.indirizzo}}</span>
				</div>
				<div class="col-xs-12 col-sm-6 col-lg-4">
					<span class="detailsLabel">CAP:</span> 
					<span onbeforesave="checkCAP($data)" editable-text="indirizzo.cap" e-size="5">{{indirizzo.cap}}</span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-7 no-margins">
				<div class="col-xs-12 col-sm-6">
					<span class="detailsLabel">Comune:</span> 
					<span editable-text="indirizzo.nomeComune">{{indirizzo.nomeComune}}</span>
				</div>
				<div class="col-xs-12 col-sm-6">
					<span class="detailsLabel">Provincia:</span> 
					<span editable-select="indirizzo.siglaProvincia" e-ng-options="provincia.sigla as provincia.sigla for provincia in (province | filter:{nomeRegione:${param.regione}, sigla:'!NN'} | orderBy : 'sigla' : false)">
						{{indirizzo.siglaProvincia}}
					</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-2 print-hide" style="padding-bottom: 5px;">		
			<button type="button" class="btn btn-info" ng-click="indirizzoForm.$show()" ng-show="!indirizzoForm.$visible <% if(!isIng) { %>&& canWriteDettagli('${param.UtenteSoggArray}', ${param.idSogg})<% } %>">
				<span class="glyphicon glyphicon-edit"></span>
			</button>
			<% if(canWrite) { %>
			<button type="button" class="btn btn-danger" ng-click="deleteIndirizzo(${param.keyREST}, ${param.idSogg}, indirizzo, ${param.mainArray})" ng-show="!indirizzoForm.$visible">
				<span class="glyphicon glyphicon-trash"></span>
			</button>
			<% } %>
			<span ng-show="indirizzoForm.$visible">
				<button type="submit" class="btn btn-primary" ng-disabled="indirizzoForm.$waiting">
					<span class="glyphicon glyphicon-ok"></span>
				</button>
				<button type="button" class="btn btn-default" ng-disabled="indirizzoForm.$waiting" ng-click="indirizzoForm.$cancel()">
					<span class="glyphicon glyphicon-remove"></span>
				</button>
			</span>
		</div>
	</form>
</div>
<% if(canWrite) { %>
	<div class="col-xs-12"><button class="btn btn-info btn-add print-hide" ng-click="addIndirizzo(${param.mainArray});"><span class="glyphicon glyphicon-plus"></span></button></div>
<% } %>