<% 
boolean canWrite = false;
if(request.getParameter("canWrite") != null){
	if(request.getParameter("canWrite").equals("true")) canWrite = true;
}
%>

<div class="col-xs-12 h5">${param.titolo}</div>
<div class="col-xs-12" data-drop="true" ng-model='dropModel'>
	<div class="col-xs-12 detailsRow" ng-repeat="(ind, soggAssoc) in (${param.mainArray} | orderBy : '${param.orderField}' : false)" ng-show="ind < maxAssocListNumber">
		{{soggAssoc.${param.displayName}}}
	</div>
	<div class="row">
		<div class="col-xs-12" ng-show="${param.mainArray}.length > maxAssocListNumber">+ {{${param.mainArray}.length-maxAssocListNumber}} altri/e ${param.titolo}</div>
	<% if (!canWrite) { %>
		<div class="draggableSogg btn btn-default"
			ng-click="openPopupViewList(${param.mainArray}, '${param.displayName}', '${param.titolo} a ' + ${param.nomeSogg}, '${param.titolo}', '${param.orderField}')">
			<span class="glyphicon glyphicon-eye-open"></span>
		</div> 
	<% } %>
	<% if(canWrite) { %>
		<div class="col-xs-12">
			<div class="draggableSogg btn btn-default" ng-click="openPopupAssoc(${param.mainArray}, ${param.assocArray}, '${param.displayName}', ${param.keyREST}, ${param.assocDigheKeyREST}, ${param.soggetto}, '${param.titolo} a ' + ${param.nomeSogg}, '${param.titolo}', '${param.sTitolo2}', '${param.orderField}', ${param.alertFct})"> <span class="glyphicon glyphicon-wrench"></span> </div>
		</div>
	<% } %>
	</div>
</div>
