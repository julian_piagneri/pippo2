<% 
boolean canReadDataAgg = false;
if(request.getParameter("canReadDataAgg") != null){
	if(request.getParameter("canReadDataAgg").equals("true")) canReadDataAgg = true;
}
%>
	<div class="datiUpdate">
		<%if(canReadDataAgg){ %>
			<div class="col-xs-12 col-md-6 no-margins" ng-model="${param.dato}">
				<div class="detailsLabel col-xs-12 col-sm-6">Aggiornato da: </div>
				<div class="col-xs-12 col-sm-6">{{${param.dato}.lastModifiedUserId}}</div>
			</div>
		<% } %>
		<div class="col-xs-12 col-md-6 no-margins">
			<div class="detailsLabel col-xs-12 col-sm-6">Data	Aggiornamento: </div>
			<div class="col-xs-12 col-sm-6">{{${param.dato}.lastModifiedDate}}</div>
		</div>
	</div>
