<accordion-group heading="Reporting">

<div class="col-xs-12 col-md-6">
	<div class="col-xs-12 col-sm-6 no-margins margin-children">
		<div class="col-xs-8 no-margins text-left h5" style="padding-top:0px !important">
			Report a partire da elenco dighe
		</div>
		<div class="col-xs-4 text-center-md text-center-xs text-right">
			<a tooltip-append-to-body="true"
				tooltip="Apri Interactive Report" target="_blank" class="btn btn-primary" role="button" ng-click="saveReportJson('home')">
				<span class="glyphicon glyphicon-list-alt"></span>
			</a>
		</div>
		<div class="col-xs-8 no-margins text-left h5" style="padding-top:0px !important">
			Report a partire da Report Builder
		</div>
		<div class="col-xs-4 text-center-md text-center-xs text-right">
			<a tooltip-append-to-body="true"
				tooltip="Apri Report builder" target="_blank" class="btn btn-primary" role="button" ng-click="initReportBuilder()">
				<span class="glyphicon glyphicon-list-alt"></span>
			</a>
		</div>
		
		
		<!-- div ng-repeat="report in reports">
			<div class="col-xs-12 col-sm-6 no-margins" style="font-weight: bold">Nome</div>
			<div class="col-xs-12 col-sm-6 no-margins text-left">{{report.name}}</div>
			<div class="col-xs-12 col-sm-6 no-margins" style="font-weight: bold">ID per filtro</div>
			<div class="col-xs-12 col-sm-6 no-margins text-left">
			<div class="col-xs-3 no-margins text-left">
			{{report.id}}
			</div>
			<div class="col-xs-9 no-margins text-left" style="margin-bottom: 3px;">
			</div>
			</div>
			<div class="col-xs-12 no-margins">
				<div dropdown is-open="dropdownStates.templates.isopen">
					<button type="button" class="btn btn-default dropdown-toggle" dropdown-toggle>
						Apri Template <span class="caret"></span>
					</button>
					<ul class="dropdown-menu dd-menu-clickable" role="menu">
						<li ng-repeat="item in reportTemplates" ng-click="openReport(report, item)">{{item.name}}</li>
						<li role="separator" class="divider"></li>
					</ul>
				</div>
			</div>
		</div -->
	</div>
</div>   
</accordion-group>