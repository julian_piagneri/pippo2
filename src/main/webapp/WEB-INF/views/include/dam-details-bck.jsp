<table class="dam-details" style="margin-bottom: 0px; table-layout: fixed;">
	<thead class="dam-details-header">
		<tr>
			<th>N. Archivio</th>
			<th>Sub</th>
			<th>N. iscr. RID</th>
			<th>Nome Diga</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{selected.entity.numeroArchivio | blankplace}}</td>
			<td>{{selected.entity.sub | blankplace}}</td>
			<td>{{selected.entity.numIscrizRid | blankplace}}</td>
			<td style="font-weight: bold">{{selected.entity.nomeDiga | blankplace }}</td>
		</tr>
	</tbody>
</table>
<table class="dam-details"
	style="margin-bottom: 0px; margin-top: 0px; table-layout: fixed;">
	<tbody>
		<tr>
			<td class="dam-details-header">Status</td>
			<td colspan="5">
				{{selected.entity.status1Sigla | blankplace:""}} 
				- {{selected.entity.status2Sigla | blankplace:"" }}
				{{selected.entity.status1Dettaglio | blankplace:"" }} -
				{{selected.entity.status2Dettaglio | blankplace:"" }}</td>
		</tr>
		<tr>
			<td class="dam-details-header">Comune</td>
			<td	compile="getMultiRowTemplate('selected.entity.currentGridValues.nomiComune.value.nomeComune', 'selected', 'selected.entity.nomiComune', 'nomiComune', '', 'nomeComune')">

			</td>
			<td class="dam-details-header">Provincia</td>
			<td	compile="getMultiRowTemplate('selected.entity.currentGridValues.nomiComune.value.nomeProvincia', 'selected', 'selected.entity.nomiComune', 'nomiComune', '', 'nomeProvincia')">

			</td>
			<td class="dam-details-header">Regione</td>
			<td compile="getMultiRowTemplate('selected.entity.currentGridValues.nomiComune.value.nomeRegione', 'selected', 'selected.entity.nomiComune', 'nomiComune', '', 'nomeRegione')">

			</td>
		</tr>
	</tbody>
</table>
<table class="dam-details" style="margin-top: 0px; table-layout: fixed;">
	<tbody>
		<tr>
			<td class="dam-details-header">Zona sism. comune</td>
			<td	compile="getMultiRowTemplate('selected.entity.currentGridValues.nomiComune.value.zonaSismicaComune', 'selected', 'selected.entity.nomiComune', 'nomiComune', '', 'zonaSismicaComune')">

			</td>
			<td class="dam-details-header">Ufficio periferico</td>
			<td>{{selected.entity.uffPeriferico | blankplace }}</td>
		</tr>
		<tr>
			<td class="dam-details-header">Fiume sbarrato</td>
			<td>{{selected.entity.nomeFiumeSbarrato | blankplace }}</td>
			<td class="dam-details-header">Concessionario</td>
			<td></td>
		</tr>
		<tr>
			<td class="dam-details-header">Fiumi a valle</td>
			<td	compile="getMultiRowTemplate('selected.entity.currentGridValues.nomeFiumeValle.value', 'selected', 'selected.entity.nomeFiumeValle', 'nomeFiumeValle', '', '')">

			</td>
			<td class="dam-details-header">Gestore</td>
			<td></td>
		</tr>
		<tr>
			<td class="dam-details-header">Lago</td>
			<td>{{selected.entity.nomeLago | blankplace }}</td>
			<td class="dam-details-header">Ingegnere resp.</td>
			<td></td>
		</tr>
		<tr>
			<td class="dam-details-header">Vol. tot. invaso L.584/94</td>
			<td>{{selected.entity.volumeTotInvasoL584 | blankplace }}</td>
			<td class="dam-details-header">Telefono ing. resp.</td>
			<td></td>
		</tr>
		<tr>
			<td class="dam-details-header">H L.584/94</td>
			<td>{{selected.entity.altezzaDigaL584 | blankplace }}</td>
			<td class="dam-details-header">Utilizzazione</td>
			<td	compile="getMultiRowTemplate('selected.entity.currentGridValues.utilizzo.value.utilizzo', 'selected', 'selected.entity.utilizzo', 'utilizzo', '', 'utilizzo')">

			</td>
		</tr>
		<tr>
			<td class="dam-details-header">Altra denominazione</td>
			<td colspan="3"	compile="getMultiRowTemplate('selected.entity.currentGridValues.altraDenominaz.value', 'selected', 'selected.entity.altraDenominaz', 'altraDenominaz', '', '')">

			</td>
		</tr>
	</tbody>
</table>