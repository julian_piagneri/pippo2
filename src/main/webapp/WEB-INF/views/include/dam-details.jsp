	<accordion-group heading='Dettaglio: {{selected.entity.numeroArchivio | blankplace:""}}{{selected.entity.sub | blankplace:""}} - {{selected.entity.nomeDiga | blankplace:"" }}'>
	
	<div class="row row-separator">
	<div class="col-sm-4 col-xs-12 no-margins-children ">
	<div class="col-xs-12 col-sm-6 detailsLabel" style="padding: 5px;">N. Archivio</div>
	<div class="col-xs-12 col-sm-6" style="padding: 5px;">{{selected.entity.numeroArchivio | blankplace}}</div>
	</div>
	
	<div class="col-sm-4 col-xs-12 no-margins-children">
	<div class="col-xs-12 col-sm-6 detailsLabel">Sub</div>
	<div class="col-xs-12 col-sm-6">{{selected.entity.sub | blankplace}}</div>
	</div>
	
	<div class="col-sm-4 col-xs-12 no-margins-children">
	<div class="col-xs-12 col-sm-6 detailsLabel" style="padding: 5px;">N. iscr. RID</div>
	<div class="col-xs-12 col-sm-6" style="padding: 5px;">{{selected.entity.numIscrizRid | blankplace}}</div>
	</div>
	
	<div class="col-xs-12 no-margins-children">
	<div class="col-xs-12 col-sm-2 detailsLabel" style="padding: 5px;">Nome Diga</div>
	<div class="col-xs-12 col-sm-10" style="padding: 5px;">{{selected.entity.nomeDiga | blankplace}}</div>
	</div>
	</div>
	
	<div class="row row-separator">
	<div class="col-xs-12 no-margins-children">
	<div class="col-md-2 col-xs-12 detailsLabel" style="padding: 5px;">Status:</div> 
	<div class="col-md-10 col-xs-12" style="padding: 5px;">
		{{selected.entity.status1Sigla | blankplace:""}} 
		- {{selected.entity.status2Sigla | blankplace:"" }}
		{{selected.entity.status1Dettaglio | blankplace:"" }} -
		{{selected.entity.status2Dettaglio | blankplace:"" }}</span>
	</div>
	</div>
	</div>
	
	<div class="row row-separator">
	<div class="col-sm-4 col-xs-12 no-margins-children ">
	<div class="col-xs-12 col-md-6 detailsLabel" style="padding: 5px;">Comune</div>
	<div class="col-xs-12 col-md-6 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.nomiComune.value.nomeComune', 'selected', 'selected.entity.nomiComune', 'nomiComune', '', 'nomeComune')"></div>
	</div>
	
	<div class="col-sm-4 col-xs-12 no-margins-children">
	<div class="col-xs-12 col-md-6 detailsLabel" style="padding: 5px;">Provincia</div>
	<div class="col-xs-12 col-md-6 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.nomiComune.value.nomeProvincia', 'selected', 'selected.entity.nomiComune', 'nomiComune', '', 'nomeProvincia')"></div>
	</div>
	
	<div class="col-sm-4 col-xs-12 no-margins-children">
	<div class="col-xs-12 col-md-6 detailsLabel" style="padding: 5px;">Regione</div>
	<div class="col-xs-12 col-md-6 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.nomiComune.value.nomeRegione', 'selected', 'selected.entity.nomiComune', 'nomiComune', '', 'nomeRegione')"></div>
	</div>
	</div>
	
	<div class="row row-separator">
	<div class="col-md-6 col-sm-12 no-margins"> 
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Zona sism. comune</div>
		<div class="col-xs-12 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.nomiComune.value.zonaSismicaComune', 'selected', 'selected.entity.nomiComune', 'nomiComune', '', 'zonaSismicaComune')"></div>
		</div>
		
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Ufficio periferico</div>
		<div class="col-xs-12" style="padding: 5px;">{{selected.entity.uffPeriferico | blankplace:""}}</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-12 no-margins">		
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Fiume sbarrato</div>
		<div class="col-xs-12" style="padding: 5px;">{{selected.entity.nomeFiumeSbarrato | blankplace:"" }}</div>
		</div>
		
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Fiumi a valle</div>
		<div class="col-xs-12 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.nomeFiumeValle.value', 'selected', 'selected.entity.nomeFiumeValle', 'nomeFiumeValle', '', '')"></div>
		</div>
	</div>
	</div>
	
	<div class="row row-separator">
	<div class="col-md-6 col-sm-12 no-margins"> 
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Lago</div>
		<div class="col-xs-12" style="padding: 5px;">{{selected.entity.nomeLago | blankplace:"" }}</div>
		</div>
		
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Vol. tot. invaso L.584/94</div>
		<div class="col-xs-12" style="padding: 5px;">{{selected.entity.volumeTotInvasoL584 | blankplace:"" }}</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-12 no-margins">	
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">H L.584/94</div>
		<div class="col-xs-12" style="padding: 5px;">{{selected.entity.altezzaDigaL584 | blankplace:"" }}</div>
		</div>
		
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Utilizzazione</div>
		<div class="col-xs-12 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.utilizzo.value.utilizzo', 'selected', 'selected.entity.utilizzo', 'utilizzo', '', 'utilizzo')"></div>
		</div>
	</div>
	</div>
	
	<div class="row row-separator">
	<div class="col-md-6 col-sm-12 no-margins"> 
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Concessionari</div>
		<div class="col-xs-12 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.concessionari.value', 'selected', 'selected.entity.concessionari', 'concessionari', '', '')"></div>
		</div>
		
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Gestori</div>
		<div class="col-xs-12 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.gestori.value', 'selected', 'selected.entity.gestori', 'gestori', '', '')"></div>
		</div>
	</div>
	<div class="col-md-6 col-sm-12 no-margins">	
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Ingegneri resp.</div>
		<div class="col-xs-12 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.ingegneri.value.nome', 'selected', 'selected.entity.ingegneri', 'ingegneri', '', 'nome')"></div>
		</div>
		
		<div class="col-md-6 col-sm-6 col-xs-12 no-margins-children">
		<div class="col-xs-12 detailsLabel" style="padding: 5px;">Telefono ing. resp.</div>
		<div class="col-xs-12 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.ingegneri.value.telefono', 'selected', 'selected.entity.ingegneri', 'ingegneri', '', 'telefono')"></div>
		</div>
	</div>
	</div>
	
	<div class="row" style="padding: 5px;">
	<div class="col-xs-12 no-margins-children ">
	<div class="col-xs-12 col-md-2 detailsLabel" style="padding: 5px;">Altra denominazione</div>
	<div class="col-xs-12 col-md-10 no-margins" compile="getMultiRowTemplate('selected.entity.currentGridValues.altraDenominaz.value', 'selected', 'selected.entity.altraDenominaz', 'altraDenominaz', '', '')"></div>
	</div>
	</div>
	</accordion-group>
