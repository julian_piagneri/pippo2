<%@ include file="taglibs.jsp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it" ng-app="damMapApp">
	<head>
		<title><% out.print(title); %></title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="resources/style/mappa.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ol.css">
		<!-- <link rel="stylesheet" type="text/css" href="resources/style/ng-grid.min.css"> -->
		
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap-switch.css">
		
		<link rel="stylesheet" type="text/css" href="resources/style/text-align-responsive.css">
		
		<link rel="stylesheet" type="text/css" href="resources/style/dialogs-5.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-table.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/xeditable.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-plain.css">
		<link rel="stylesheet" type="text/css" href="resources/angularjs/ui-grid/ui-grid-unstable.css">
		
		<link rel="stylesheet" type="text/css" href="resources/style/main.css">
		<link rel="stylesheet" type="text/css" href="resources/style/home-filters.css">
		<link rel="stylesheet" type="text/css" href="resources/style/dropdown.css">
		<link rel="stylesheet" type="text/css" href="resources/style/animations.css">
		<link rel="stylesheet" type="text/css" href="resources/style/damlist.css">
		<link rel="stylesheet" type="text/css" href="resources/style/navbar.css">
		<link rel="stylesheet" type="text/css" href="resources/style/media-queries.css">
		<link rel="stylesheet" type="text/css" href="resources/style/loading-spinner.css">
		<link rel="stylesheet" type="text/css" href="resources/style/users.css">
		<!-- link rel="stylesheet" type="text/css" href="resources/style/googleapi.css" -->
		
		<link rel="stylesheet" type="text/css" href="resources/jquery-ui/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="resources/jquery-ui/jquery-ui.structure.css">
		<link rel="stylesheet" type="text/css" href="resources/jquery-ui/jquery-ui.theme.css">
		
		
		
		<script type="text/javascript" src="resources/jquery/v2.1.1/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/v1.2.26/angular.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/ui-grid/ui-grid-unstable.js"></script>
		<script type="text/javascript" src="resources/openlayers/v3.4.0/build/ol.js"></script>
		
		<script type="text/javascript" src="resources/angularjs/bootstrap-v0.12.0/ui-bootstrap-tpls-0.12.0.min.js"></script>
		<script type="text/javascript" src="resources/scripts/bootstrap/bootstrap-switch.min.js"></script>
		<script type="text/javascript" src="resources/scripts/lib/angular-bootstrap-switch.js"></script>
		<script type="text/javascript" src="resources/angularjs/sanitize/angular-sanitize.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/xeditable/xeditable-sce.js"></script>
		<script type="text/javascript" src="resources/angularjs/dialogs/dialogs-5.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/dialogs/dialogs-default-translations.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/ngDialog/ngDialog.min.js"></script>
		<script type="text/javascript" src="resources/scripts/lib/debounce.js"></script>
		<script type="text/javascript" src="resources/jquery-ui/jquery-ui.min.js"></script>
		<!-- script type="text/javascript" src="resources/gmaps/googlemapsapi.js"></script>
		<script type="text/javascript" src="resources/gmaps/main.js"></script>
		
		
		<!-- script src="http://maps.google.com/maps/api/js?v=3&amp;sensor=false"></script -->
		
		<script type="text/javascript" src="resources/scripts/lib/routes.js"></script>
		<script type="text/javascript" src="resources/scripts/lib/olEngine.js"></script>
		
		<%if(request.getRequestURL().toString().contains("localhost")){ 
			//per le prove in locale se non si � in possesso dell'applicazione vecchia %>
		<script type="text/javascript" src="resources/scripts/lib/routes-local.js"></script>
		<% } %>
		<script type="text/javascript" src="resources/scripts/damMapInit.js"></script>
		<script type="text/javascript" src="resources/scripts/lib/main-module.js"></script>
		
		<script type="text/javascript" src="resources/scripts/services/storageService.js"></script>
		<script type="text/javascript" src="resources/scripts/services/mapService.js"></script>
		<script type="text/javascript" src="resources/scripts/services/dialogService.js"></script>
		<script type="text/javascript" src="resources/scripts/services/helpersService.js"></script>
		<script type="text/javascript" src="resources/scripts/services/filtersService.js"></script>
		<script type="text/javascript" src="resources/scripts/services/gridService.js"></script>
		
		<script type="text/javascript" src="resources/scripts/controllers/damListController.js"></script>
		<script type="text/javascript" src="resources/scripts/controllers/loginController.js"></script>
		<script type="text/javascript" src="resources/scripts/controllers/userController.js"></script>
		
		<script type="text/javascript" src="resources/scripts/factories/menuDirective.js"></script>
		<script type="text/javascript" src="resources/scripts/factories/restEngine.js"></script>
		<script type="text/javascript" src="resources/scripts/factories/token-storage.js"></script>
		<script type="text/javascript" src="resources/scripts/factories/compile-provider.js"></script>
		<script type="text/javascript" src="resources/scripts/factories/blank-filter.js"></script>
		<script type="text/javascript" src="resources/scripts/factories/nl2br-filter.js"></script>
		<script type="text/javascript" src="resources/scripts/factories/misc-filters.js"></script>
		<script type="text/javascript" src="resources/scripts/lib/menu-events.js"></script>
		<script type="text/javascript" src="resources/angularjs/locale_it/angular-locale_it-it.js"></script>
				
		<script type="text/ng-template" id="main"></script>
	    <!-- Bootstrap -->
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	    <![endif]-->
	    <!--[if gte IE 9]>
		<style type="text/css">
		    .gradient {
		       filter: none;
		    }
		  </style>
		<![endif]-->
	</head>