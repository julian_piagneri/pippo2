	<accordion-group heading='Dettaglio: {{selected.entity.numeroArchivio | blankplace:""}}{{selected.entity.sub | blankplace:""}} - {{selected.entity.nomeDiga | blankplace:"" }}'>
		<table ng-table="tableConcess" show-filter="true" class="table">
			<tr class="detailsRow">
				<td>
					<form name="detailForm">
						<span class="detailsLabel">N. Archivio:</span> <span>{{selected.entity.numeroArchivio | blankplace}}</span>
						<span class="detailsLabel">Sub:</span> <span>{{selected.entity.sub | blankplace}}</span>
						<span class="detailsLabel">N. iscr. RID:</span> <span>{{selected.entity.numIscrizRid | blankplace}}</span>
						<span class="detailsLabel">Nome Diga:</span> <span style="font-weight: bold">{{selected.entity.nomeDiga | blankplace}}</span>
					</form>
				</td>
			</tr>
			<tr class="detailsRow">
				<td>
					<form name="detailForm">
						<div class="col-xs-12">
							<span class="detailsLabel">Status:</span> <span>{{selected.entity.status1Sigla | blankplace:""}} 
							- {{selected.entity.status2Sigla | blankplace:"" }}
							{{selected.entity.status1Dettaglio | blankplace:"" }} -
							{{selected.entity.status2Dettaglio | blankplace:"" }}</span>
						</div>
					</form>
				</td>
			</tr>
			<tr class="detailsRow">
				<td>
					<form name="detailForm">
						<div class="col-xs-12">
							<span class="detailsLabel">Comune:</span>&nbsp;<span>{{selected.entity.currentGridValues.nomiComune.value.nomeComune}}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">Provincia:</span>&nbsp;<span>{{selected.entity.currentGridValues.nomiComune.value.nomeProvincia}}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">Regione:</span>&nbsp;<span>{{selected.entity.currentGridValues.nomiComune.value.nomeRegione}}</span>
						</div>
					</form>
				</td>
			</tr>
			<tr class="detailsRow">
				<td>
					<form name="detailForm">
						<div class="col-xs-12">
							<span class="detailsLabel">Zona sism. comune:</span>&nbsp;<span>{{selected.entity.currentGridValues.nomiComune.value.zonaSismicaComune}}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">Ufficio periferico:</span>&nbsp;<span>{{selected.entity.uffPeriferico | blankplace:""}}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">Fiume sbarrato:</span>&nbsp;<span>{{selected.entity.nomeFiumeSbarrato | blankplace:"" }}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">Fiumi a valle:</span>&nbsp;<span>{{selected.entity.currentGridValues.nomeFiumeValle.value}}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">Lago:</span>&nbsp;<span>{{selected.entity.nomeLago | blankplace:"" }}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">Vol. tot. invaso L.584/94:</span>&nbsp;<span>{{selected.entity.volumeTotInvasoL584 | blankplace:"" }}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">H L.584/94:</span>&nbsp;<span>{{selected.entity.altezzaDigaL584 | blankplace:"" }}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">Utilizzazione:</span>&nbsp;<span>{{selected.entity.currentGridValues.utilizzo.value.utilizzo}}</span>
						</div>
						<div class="col-xs-12">
							<span class="detailsLabel">Altra denominazione:</span>&nbsp;<span>{{selected.entity.currentGridValues.altraDenominaz.value}}</span>
						</div>
					</form>
				</td>
			</tr>
		</table>
	</accordion-group>
