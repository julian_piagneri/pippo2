<accordion-group heading="Filtri & Viste">
		<div class="col-xs-12 col-md-6">
			<div class="no-margins margin-children">
				<div class="col-xs-12 no-margins h5">Seleziona</div>
                <div class="col-xd-12 no-margins">
					<div class="btn-group" dropdown is-open="dropdownStates.preferiti.isopen">
						<button style="min-width: 215px;" type="button" class="btn btn-default dropdown-toggle" dropdown-toggle>
							{{selectFilters[showFilter.select].name}} <span class="caret"></span>
						</button>
						<!-- <input bs-switch switch-size="mini" switch-change="applyFilters()" type="checkbox" ng-checked="showFilter.rid" ng-model="showFilter.rid" /> -->
						<ul class="dropdown-menu dd-menu-clickable" role="menu">
							<li ng-repeat="item in selectFilters"
								ng-class="{selected: item.value == showFilter.select }"
								ng-click="selectNewFilter(item.value);">{{item.name}}</li>
						</ul>
					</div>
				</div>
                <div class="col-xs-12 no-margins h5">Filtri</div>
                <div class="col-xs-12 no-margins">
                    <div class="col-xs-6 no-margins" >Competenza RID</div>
                    <div class="col-xs-6 no-margins text-center-md text-center-xs text-right" tooltip-append-to-body="true" tooltip="Dighe di competenza RID">
                        <input bs-switch switch-size="mini" switch-change="applyFilters()" type="checkbox" ng-checked="showFilter.rid" ng-model="showFilter.rid" />
                    </div>
                </div>
                <div class="col-xs-12 no-margins">
                    <div class="col-xs-6 no-margins" >In accertamento</div>
                    <div class="col-xs-6 no-margins text-center-md text-center-xs text-right" tooltip-append-to-body="true" tooltip="Dighe in accertamento">
                        <input bs-switch switch-size="mini" switch-change="applyFilters()" type="checkbox" ng-checked="showFilter.accertamento" ng-model="showFilter.accertamento" />
                    </div>
                </div>
                <div class="col-xs-12 no-margins">
                    <div class="col-xs-6 no-margins" >In progetto</div>
                    <div class="col-xs-6 no-margins text-center-md text-center-xs text-right" tooltip-append-to-body="true" tooltip="Dighe in progetto">
                        <input bs-switch switch-size="mini" switch-change="applyFilters()" type="checkbox" ng-checked="showFilter.progetto" ng-model="showFilter.progetto" />
                    </div>
                </div>
                <div class="col-xs-12 no-margins">
                    <div class="col-xs-6 no-margins" >Competenza regionale</div>
                    <div class="col-xs-6 no-margins text-center-md text-center-xs text-right" tooltip-append-to-body="true" tooltip="Dighe di competenza regionale">
                        <input bs-switch switch-size="mini" switch-change="applyFilters()" type="checkbox" ng-checked="showFilter.competenza" ng-model="showFilter.competenza" />
                    </div>
                </div>
                <div class="col-xs-12 no-margins">
                    <div class="col-xs-6 no-margins" >Altre</div>
                    <div class="col-xs-6 no-margins text-center-md text-center-xs text-right" tooltip-append-to-body="true" tooltip="Dighe di altri archivi">
                        <input bs-switch switch-size="mini" switch-change="applyFilters()" type="checkbox" ng-checked="showFilter.altro" ng-model="showFilter.altro" />
                    </div>
                </div>
                
                <div ng-show="false" class="col-xs-12 no-margins text-left">
                	<button id="apply-view" style="padding-right:50px; padding-left:50px;" type="button" tooltip-append-to-body="true" tooltip="Applica i filtri selezionati alla lista delle dighe" class="btn btn-primary btn-sm" ng-click="reloadDamData()">Applica</button>
            	</div>
            </div>
		</div>


<div class="col-xs-12 col-md-6">
	<div class="col-xs-12 col-sm-6 no-margins">
		<div class="col-xs-12 no-margins margin-children">
			<div class="col-xs-12 no-margins text-left h5">Viste & Preferiti</div>
			<div class="col-xs-12 no-margins text-left greyInfo">Vista Attuale: {{currentState.name}}</div>
			<div class="col-xs-12 no-margins">
				<div dropdown is-open="dropdownStates.viste.isopen">
					<button type="button" class="btn btn-default dropdown-toggle" dropdown-toggle>
						Seleziona la vista <span class="caret"></span>
					</button>
					<ul class="dropdown-menu dd-menu-clickable" role="menu">
						<li ng-repeat="item in states"
							ng-class="{selected: item == currentState }"
							ng-click="selectNewState(item)"><span
							class="glyphicon glyphicon-bookmark" ng-show="item.defaultView==1"></span> 
							{{item.name}}</li>
						<li role="separator" class="divider"></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12 no-margins margin-children text-left">
			<div class="col-xs-12 no-margins">
				<div class="btn-group btn-group-lg-sm btn-group-md">
					<button id="save-view" type="button" tooltip-append-to-body="true"
						tooltip="Salva le modifiche alla vista corrente"
						class="btn btn-primary" ng-disabled="isBaseView()" ng-click="saveState()">
						<span class="glyphicon glyphicon-floppy-disk"></span>
					</button>
					<button id="del-view" type="button" tooltip-append-to-body="true"
						tooltip="Cancella la vista attualmente selezionata"
						class="btn btn-primary" ng-disabled="isBaseView()" ng-click="deleteState()">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
					<button id="def-view" type="button" tooltip-append-to-body="true"
						tooltip="Imposta la vista attuale come predefinita"
						class="btn btn-primary" ng-click="defaultState()">
						<span class="glyphicon glyphicon-bookmark"></span>
					</button>
					<button id="new-view" type="button" tooltip-append-to-body="true"
						tooltip="Crea una nuova vista a partire dalle impostazioni attuali"
						class="btn btn-primary" ng-click="newState.$visible=true; newStateForm.$show();">
						<span class="glyphicon glyphicon-file"></span>
					</button>
					<!-- button id="new-view" type="button" tooltip-append-to-body="true"
						tooltip="Genera report a partire dalle dighe attualmente visibili"
						class="btn btn-primary" ng-click="newReport.$visible=true; newReportForm.$show();">
						<span class="glyphicon glyphicon-th-list"></span>
					</button -->
				</div>
				<!-- div id="new-report-name" class="col-xs-12" ng-show="newReport.$visible">
				<span e-placeholder="Nome Report" e-form="newReportForm"
					editable-text="newReportName"
					onaftersave="saveReportJson(newReportName, newReportForm, newReport); newReport.$visible=false;"
					oncancel="newReport.$visible=false" e-size="20"></span>
				</div -->
				
				<div id="new-state-name" class="col-xs-12" ng-show="newState.$visible">
				<span e-placeholder="Nome Vista" e-form="newStateForm"
					editable-text="newStateName"
					onaftersave="saveNew(newStateName, newStateForm, newState); newState.$visible=false;"
					oncancel="newState.$visible=false" e-size="20"></span>
				</div>
			</div>
		</div>    
	    <div class="col-xs-12 no-margins margin-children text-left">
			<!-- <div class="col-xs-12 text-left no-margins h5">Preferiti</div> -->
			<div class="col-xs-12 no-margins margin-children">
				<div class="btn-group btn-group-lg-sm btn-group-md">
					<button type="button" tooltip-append-to-body="true"
						tooltip="Aggiungi le dighe attualmente visualizzate ai preferiti"
						class="btn btn-primary" ng-click="setVisibleRowsFavorite(true)">
						<span class="glyphicon glyphicon-plus"></span>*
					</button>
					<button type="button" tooltip-append-to-body="true"
						tooltip="Aggiungi la diga selezionata ai preferiti"
						class="btn btn-primary" ng-click="setRowFavorite(true)">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
					<button type="button" tooltip-append-to-body="true"
						tooltip="Rimuovi la diga selezionata dai preferiti"
						class="btn btn-primary" ng-click="setRowFavorite(false)">
						<span class="glyphicon glyphicon-minus"></span>
					</button>
					<button type="button" tooltip-append-to-body="true"
						tooltip="Rimuovi le dighe attualmente visualizzate ai preferiti"
						class="btn btn-primary" ng-click="setVisibleRowsFavorite(false)">
						<span class="glyphicon glyphicon-minus"></span>*
					</button>
				</div>
			</div>
		</div>
	</div>
</div>   
</accordion-group>