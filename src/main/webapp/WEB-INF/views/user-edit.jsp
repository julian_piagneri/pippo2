<% 
String title="Scheda Utente";
String percorsoMenu ="\\Utenti-Gruppi\\Gestione Utenti\\Modifica Utente";
String userId="{{storageService.get('login.username', '')}}";
if(!request.getParameterMap().containsKey("id")){ 
	percorsoMenu = "\\Profilo\\Dati Utente ";	
}
%>
<%@ include file="include/header.jsp" %>
<body ng-controller="AuthCtrl" ng-init="init('routeUserBlock', '?isNew=false')">
<script>isNew=false;</script>
<%@ include file="include/get-userid.jsp" %>
<div ng-show="storageService.get('login.authenticated', false)">
<%@ include file="header-logo.jsp" %>
</div>
<div style="height: 70px;"></div>
<div ng-include="content"></div>
<%@ include file="include/footer.jsp" %>

