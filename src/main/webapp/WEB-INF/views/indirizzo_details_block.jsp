<% 
boolean canWrite = false;
if(request.getParameter("canWrite") != null){
	if(request.getParameter("canWrite").equals("true")) canWrite = true;
}

boolean canWriteDettagli = false;
if(request.getParameter("canWriteDettagli") != null){
	if(request.getParameter("canWriteDettagli").equals("true")) canWriteDettagli = true;
}

if(canWrite) canWriteDettagli = true;

%>
<tr><td class="h5">Indirizzi</td></tr>
<tr class="detailsRow" ng-repeat="indirizzo in (${param.mainArray} | orderBy : 'order' : false)">
	<td>
		<form editable-form shown="indirizzo.isNew" name="indirizzoForm" onaftersave="saveIndirizzo(${param.keyREST}, ${param.idSogg}, indirizzo, indirizzoForm, ${param.soggetto});" oncancel="removeNewEmptyRecord($index, ${param.mainArray}, indirizzo.isNew, indirizzo.id);">
			<span class="detailsLabel">Indirizzo:</span>&nbsp;<span editable-text="indirizzo.indirizzo">{{indirizzo.indirizzo}}</span>&nbsp;&nbsp;
			<span class="detailsLabel">CAP:</span>&nbsp;<span onbeforesave="checkCAP($data)" editable-text="indirizzo.cap" e-size="5">{{indirizzo.cap}}</span>&nbsp;&nbsp;
			<span class="detailsLabel">Comune:</span>&nbsp;<span editable-text="indirizzo.nomeComune">{{indirizzo.nomeComune}}</span>&nbsp;&nbsp;
			<span class="detailsLabel">Provincia:</span>&nbsp;<span editable-select="indirizzo.siglaProvincia" e-ng-options="provincia.sigla as provincia.sigla for provincia in (province | filter:{nomeRegione:${param.regione}, sigla:'!NN'} | orderBy : 'sigla' : false)">{{indirizzo.siglaProvincia}}</span>&nbsp;&nbsp;
			
			<button type="button" class="btn btn-info" ng-click="indirizzoForm.$show()" ng-show="!indirizzoForm.$visible && canWriteDettagli('${param.UtenteSoggArray}', ${param.idSogg})">
				<span class="glyphicon glyphicon-edit"></span>
			</button>
			<% if(canWrite) { %>
			<button type="button" class="btn btn-danger" ng-click="deleteIndirizzo(${param.keyREST}, ${param.idSogg}, indirizzo, ${param.mainArray})" ng-show="!indirizzoForm.$visible">
				<span class="glyphicon glyphicon-trash"></span>
			</button>
			<% } %>
			<span ng-show="indirizzoForm.$visible">
				<button type="submit" class="btn btn-primary" ng-disabled="indirizzoForm.$waiting">
					<span class="glyphicon glyphicon-ok"></span>
				</button>
				<button type="button" class="btn btn-default" ng-disabled="indirizzoForm.$waiting" ng-click="indirizzoForm.$cancel()">
					<span class="glyphicon glyphicon-remove"></span>
				</button>
			</span>
		</form>
	</td>
</tr>
<% if(canWrite) { %><tr><td><button class="btn btn-info btn-add" ng-click="addIndirizzo(${param.mainArray});"><span class="glyphicon glyphicon-plus"></span></button></td></tr><% } %>