<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String schedaId = "1100"; //"59";
String schedaIdDettagli = "2100"; //"69";
String percorsoMenu = "\\Anagrafiche\\Anagrafica";
String titoloPagina = "Anagrafica";
boolean canWrite = false;
boolean canWriteDettagli = false;
boolean canRead = false;
%>

<%@ include file="user_permissions.jsp" %>
<%
canWrite = permessiAnagrafica.get("canWrite");
canWriteDettagli = permessiAnagrafica.get("canWriteDettagli");
canRead = permessiAnagrafica.get("canRead");
// String[] autoritaWrite = new String[3];
boolean hide=true;


//if(logged) {

	
	
	// System.out.println(temp);
	

	
	////////////////////
	  //canWrite = true;
	  canWriteDettagli = true;
	  canRead = true;
	////////////////////
//}
String appName="anagrafApp";
%>
<%@ include file="header_block.jsp" %>
<% if(canRead) { %>
<div class="container-fluid" style="position: relative">
    <div ng-controller="anagrafController" class="row" id="anagraf" >
    	<div class="col-xs-12">
    	<accordion close-others="false">
    		<accordion-group heading="CONCESSIONARI">
    			<%@ include file="sections/anagrafica/concessionari.jsp" %>
			</accordion-group>
			<accordion-group heading="GESTORI">
				<%@ include file="sections/anagrafica/gestori.jsp" %>
			</accordion-group>
			<accordion-group ng-show="false" heading="INGEGNERI">
				<%@ include file="sections/anagrafica/ingegneri.jsp" %>
			</accordion-group>
			<accordion-group heading="AMMINISTRAZIONI DESTINATARIE COMUNICAZIONI D.P.C.">
				<%@ include file="sections/anagrafica/autorita.jsp" %>
			</accordion-group>
			<accordion-group heading="ALTRI ENTI">
				<%@ include file="sections/anagrafica/altri.jsp" %>
			</accordion-group>
    	</accordion>
    	</div>
    		<%@ include file="blocks/close.jsp" %>
    </div>
</div>	
			<%
			// boolean yyy = false;
			// yyy = concessWrite.contains("1041");
			%>

		<script type="text/ng-template" id="assocTemplate"><%@ include file="popup_assoc.jsp" %></script>
		<script type="text/ng-template" id="viewListTemplate"><%@ include file="popup_view_list.jsp" %></script>
		
		<%@ include file="js_user_permissions.jsp" %>
		<%@ include file="scripts_block.jsp" %>
		
		<script type="text/javascript" src="resources/scripts/anagrafInit.js"></script>
		<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
		<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
		<script type="text/javascript" src="resources/scripts/anagrafApp.js"></script>
<%
 }
 else{
 %>
	<script type="text/javascript">
		window.close();
	</script>
<% } %>
	</body>
</HTML>
