<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
String schedaId = "24";
String percorsoMenu = "\\Protezione Civile\\Rubrica Telefonica";
String titoloPagina = "Rubrica<BR />Documento di Protezione civile";
boolean canWrite=false;
boolean canRead=false;
boolean canWriteAsConcess=false;
boolean canWriteIng=false;

boolean canWriteDettagli=false;
boolean canWriteAnagrafica=false;
%>


<%@ include file="user_permissions.jsp" %>
<%
//Compatibilità con vecchio sistema. I nuovi permessi vanno presi direttamente dalla mappa
canWrite=permessiRubrica.get("canWrite");
canRead=permessiRubrica.get("canRead");
canWriteAsConcess=permessiRubrica.get("canWriteAsConcess");
// canWriteAsGestore=permessiRubrica.get("canWriteAsGestore");
canWriteIng=permessiRubrica.get("canWriteIng");

canWriteDettagli=permessiAnagrafica.get("canWriteDettagli") || canWrite;
canWriteAnagrafica=permessiAnagrafica.get("canWrite");
// if() 
// if(canWrite) canWriteDettagli = true;
//if(logged) {
//	canWrite = UcanWrite(AuthObj, schedaId);
//	canRead = UcanRead(AuthObj, schedaId);
//	canWriteAsConcess = userService.checkDamWriteRigthsForConcessionario(session.getAttribute("userId").toString(), request.getParameter("dam"));
//	canWriteDettagli = UcanWrite(AuthObj, schedaIdDettagli);
//	canWriteAnagrafica = UcanWrite(AuthObj, schedaIdAnagrafica);
//	if(canWriteAsConcess) canWrite = true;
//	if(canWriteAsConcess) canWriteIng = true;
//	if(canWrite) canWriteIng = true;
// if(canWrite) canWriteAnagrafica = true;
// if(canWrite) canWriteDettagli = true;
	
	////////////////////
	// canWrite = true;
	// canRead = true;
	////////////////////
//}


%>


<html>
	<head>
		<link rel="stylesheet" type="text/css" href="resources/style/ng-grid.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/dialogs.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-table.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/main.css">
		<link rel="stylesheet" type="text/css" href="resources/style/xeditable.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-plain.css">
	</head>
	<body ng-app="rubricaApp">
<%@ include file="header_block.jsp" %>
		<BR />
		<BR />
		<BR />
<% if(canRead) { %>
		<div ng-controller="rubricaController" class="mainTable" id="rubrica" style="{text-align: center;}">

			<table class="table detailsTable">
				<tr>
					<td>
						<span class="detailsLabel mainInfo">N&deg; Archivio:</span>&nbsp;<span>{{datiRubrica.numeroArchivio}}</span>&nbsp;&nbsp;
						<span class="detailsLabel mainInfo">Sub:</span>&nbsp;<span e-size="5">{{datiRubrica.sub}}</span>
						<span class="detailsLabel mainInfo">Diga:</span>&nbsp;<span>{{datiRubrica.nomeDiga}}</span>&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						<div class="btn-group" dropdown is-open="dropdownInvaso.isopen">
							<button type="button" class="btn btn-default dropdown-toggle" dropdown-toggle>
								Dighe dello stesso invaso <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li ng-repeat="digaInvaso in digheInvaso">{{digaInvaso}}</li>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			
			<accordion close-others="false">
				<accordion-group heading="MIT Direzione Generale per le Dighe e le Infrastrutture Idriche ed Elettriche">
					<table class="table">
					
						<tr>
							<td class="h5">Ufficio Tecnico per le Dighe</td>
						</tr>
						<tr>
							<td>
								<table class="table detailsTable">
									<tr>
										<td>
											<span class="detailsLabel">Denominazione:</span>&nbsp;{{datiRubrica.uffPerif.descrizione}}
										</td>
									</tr>
									<tr>
										<td>
											<span class="detailsLabel">Indirizzo:</span>&nbsp;{{datiRubrica.uffPerif.indirizzo}}
										</td>
									</tr>
									<tr>
										<td>
											<span class="detailsLabel">Telefono:</span>&nbsp;{{datiRubrica.uffPerif.telefono}}
										</td>
									</tr>
									<tr>
										<td>
											<span class="detailsLabel">Fax:</span>&nbsp;{{datiRubrica.uffPerif.fax}}
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td class="h5">Ufficio Sede Centrale</td>
						</tr>
						<tr>
							<td>
								<table class="table detailsTable">
									<tr>
										<td>
											<span class="detailsLabel">Denominazione:</span>&nbsp;{{datiRubrica.uffCoord.descrizione}}
										</td>
									</tr>
									<tr>
										<td>
											<span class="detailsLabel">Indirizzo:</span>&nbsp;{{datiRubrica.uffCoord.indirizzo}}
										</td>
									</tr>
									<tr>
										<td>
											<span class="detailsLabel">Telefono:</span>&nbsp;{{datiRubrica.uffCoord.telefono}}
										</td>
									</tr>
									<tr>
										<td>
											<span class="detailsLabel">Fax:</span>&nbsp;{{datiRubrica.uffCoord.fax}}
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
					</table>
				</accordion-group>
				<accordion-group heading="Concessionario - Gestore">
					<table class="table">
						<tr>
							<td class="h5">Concessionari</td>
						</tr>
						<tr>
							<td>
								<table class="table">
									<tr class="soggName" ng-repeat="concessionario in concessionari" ng-class="{'active': !concessionario.isC_details}">
										<td>
											<table class="soggTable">
												<tr class="soggName" ng-class="{'activeSogg': !concessionario.isC_details}">
													<td ng-class="{'activeSoggTd': !concessionario.isC_details}"  ng-click="concessForm.$visible ? void(0) : concessionario.isC_details=!concessionario.isC_details;">
														<span e-form="concessForm" editable-text="concessionario.nome" onaftersave="saveConcess(concessionario, concessForm)" oncancel="removeOnEdit(concessionario);" e-size="50">{{concessionario.nome}}</span>
													</td>
													<td>
													<% if(canWriteAnagrafica) { %>
														<div class="btn-sogg">
															<button e-form="concessForm" type="button" class="btn btn-info" ng-click="concessForm.$show(); $event.stopPropagation(); concessionario.onEdit=true;" ng-show="!concessionario.isC_details">
																<span class="glyphicon glyphicon-edit"></span>
															</button>
															<!-- button e-form="concessForm" type="button" class="btn btn-danger" ng-click="deleteConcess(concessionario.id); $event.stopPropagation();" ng-show="!concessionario.isC_details">
																<span class="glyphicon glyphicon-trash"></span>
															</button -->
														</div>
													<% } %>
													</td>
												</tr>
												
												<tr><td colspan="2">
													<table class="detailsTable" collapse="concessionario.isC_details">
														<jsp:include page="indirizzo_details_block.jsp" >
															<jsp:param name="mainArray" value="concessionario.indirizzo" />
															<jsp:param name="regione" value="empty" />
															<jsp:param name="keyREST" value="concessionarioREST" />
															<jsp:param name="soggetto" value="concessionario" />
															<jsp:param name="idSogg" value="concessionario.id" />
															<jsp:param name="UtenteSoggArray" value="G_concess_Write" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />													
														</jsp:include>
														<jsp:include page="contatto_details_block.jsp" >
															<jsp:param name="mainArray" value="concessionario.contatto" />
															<jsp:param name="keyREST" value="concessionarioREST" />
															<jsp:param name="soggetto" value="concessionario" />
															<jsp:param name="idSogg" value="concessionario.id" />
															<jsp:param name="UtenteSoggArray" value="G_concess_Write" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
														</jsp:include>
													<tr>
													<td>
													<jsp:include page="data_agg_block.jsp" >
														<jsp:param name="dato" value="concessionario" />
														<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
													</jsp:include>
													</td>
													</tr>
											</table>
												</td></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td class="h5">Gestori</td>
						</tr>
						<tr>
							<td>
								<table class="table">
									<tr class="soggName" ng-repeat="gestore in gestori" ng-class="{'active': !gestore.isC_details}">
										<td>
											<table class="soggTable">
												<tr class="soggName" ng-class="{'activeSogg': !gestore.isC_details}">
												
													<td ng-class="{'activeSoggTd': !gestore.isC_details}"  ng-click="gestoreForm.$visible ? void(0) : gestore.isC_details=!gestore.isC_details;">
														<span e-form="gestoreForm" editable-text="gestore.nome" onaftersave="saveGestore(gestore, gestoreForm)" oncancel="removeOnEdit(gestore);" e-size="50">{{gestore.nome}}</span>
													</td>
													<td>
													<% if(canWriteAnagrafica) { %>
														<div class="btn-sogg">
															<button e-form="gestoreForm" type="button" class="btn btn-info" ng-click="gestoreForm.$show(); $event.stopPropagation(); gestore.onEdit=true;" ng-show="!gestore.isC_details">
																<span class="glyphicon glyphicon-edit"></span>
															</button>
															<!-- button e-form="gestoreForm" type="button" class="btn btn-danger" ng-click="deleteConcess(gestore.id); $event.stopPropagation();" ng-show="!gestore.isC_details">
																<span class="glyphicon glyphicon-trash"></span>
															</button -->
														</div>
													<% } %>
													</td>
													
													
												
													<!-- td ng-class="{'activeSoggTd': !gestore.isC_details}">
														<span>{{gestore.nome}}</span>
													</td -->
												</tr>
												
												<tr><td>
													<table class="detailsTable" collapse="gestore.isC_details">
														<jsp:include page="indirizzo_details_block.jsp" >
															<jsp:param name="mainArray" value="gestore.indirizzo" />
															<jsp:param name="regione" value="empty" />
															<jsp:param name="keyREST" value="gestoreREST" />
															<jsp:param name="soggetto" value="gestore" />
															<jsp:param name="idSogg" value="gestore.id" />
															<jsp:param name="UtenteSoggArray" value="G_gestore_Write" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
														</jsp:include>
														<jsp:include page="contatto_details_block.jsp" >
															<jsp:param name="mainArray" value="gestore.contatto" />
															<jsp:param name="keyREST" value="gestoreREST" />
															<jsp:param name="soggetto" value="gestore" />
															<jsp:param name="idSogg" value="gestore.id" />
															<jsp:param name="UtenteSoggArray" value="G_gestore_Write" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
														</jsp:include>
													<tr>
													<td>
													<jsp:include page="data_agg_block.jsp" >
														<jsp:param name="dato" value="gestore" />
														<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
													</jsp:include>
													</td>
													</tr>
													</table>
												</td></tr>
											</table>
											
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td class="h5">Ingegneri Responsabili</td>
						</tr>
						<tr>
							<td>
								<table class="table">
									<tr class="soggName" ng-repeat="(ind, ingegnereResp) in ingegneriResp | orderBy : 'dataNominaOrder' : true" ng-show="ind==0 || ingRespParams.showhistory" ng-class="{'active': !ingegnereResp.isC_details, 'current-sogg': ind==0, 'history-sogg': ind!=0}">
										<td>
											<table class="soggTable">
												<tr class="soggName" ng-click="show_hide_Ing_details(ingegnereResp)" ng-class="{'activeSogg': !ingegnereResp.isC_details}">
													<td ng-class="{'activeSoggTd': !ingegnereResp.isC_details}">
															
														<form editable-form shown="ingRespForm.$visible" name="ingRespForm" onaftersave="saveIngegnere(ingegnereResp, ingRespForm, ingegnereRespDigheREST)" oncancel="removeOnEdit(ingegnereResp)">
																
															<span ng-show="!ingRespForm.$visible">{{ingegnereResp.denominazione}}</span>
															<span ng-show="ingRespForm.$visible">
																Nome:&nbsp;<span e-form="ingRespForm" editable-text="ingegnereResp.nome"></span>
																Cognome:&nbsp;<span e-form="ingRespForm" editable-text="ingegnereResp.cognome"></span>
															</span>
															
															<% if(canWriteIng) { %>
															<div class="btn-sogg">
																<button e-form="ingRespForm" type="button" class="btn btn-info" ng-click="ingRespForm.$show(); $event.stopPropagation(); ingegnereResp.onEdit=true; ingegnereResp.newDataNomina=ingegnereResp.dataNomina;" ng-show="!ingegnereResp.isC_details">
																	<span class="glyphicon glyphicon-edit"></span>
																</button>
																<button e-form="ingRespForm" type="button" class="btn btn-danger" ng-click="deleteIngegnereDiga(ingegnereResp, true); $event.stopPropagation();" ng-show="!ingegnereResp.isC_details">
																	<span class="glyphicon glyphicon-trash"></span>
																</button>
															</div>
															<span ng-show="ingRespForm.$visible">
																<br />Data&nbsp;nomina:&nbsp;<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="ingRespForm.$visible" ng-model="ingegnereResp.newDataNomina" datepicker-popup="dd/MM/yyyy" is-open="ingRespForm.open" ng-focus="ingRespForm.open = true" ng-click="ingRespForm.open = true" placeholder="Data Nomina" current-text="Oggi" clear-text="Cancella" close-text="Chiudi"/>&nbsp;&nbsp;
															</span>
															<span ng-show="!ingRespForm.$visible" style="float: right;">Data&nbsp;nomina:&nbsp;{{ingegnereResp.dataNomina}}&nbsp;&nbsp;&nbsp;</span>
															<span ng-show="ingRespForm.$visible">
																<button type="submit" class="btn btn-primary" ng-disabled="ingRespForm.$waiting">
																	<span class="glyphicon glyphicon-ok"></span>
																</button>
																<button type="button" class="btn btn-default" ng-disabled="ingRespForm.$waiting" ng-click="ingRespForm.$cancel()">
																	<span class="glyphicon glyphicon-remove"></span>
																</button>
															</span>
															<% } %>
														</form>
													</td>
												</tr>
												
												<tr><td>
													<table class="detailsTable" collapse="ingegnereResp.isC_details">
														<jsp:include page="indirizzo_details_block.jsp" >
															<jsp:param name="mainArray" value="ingegnereResp.indirizzo" />
															<jsp:param name="regione" value="empty" />
															<jsp:param name="keyREST" value="ingegnereREST" />
															<jsp:param name="soggetto" value="ingegnereResp" />
															<jsp:param name="idSogg" value="ingegnereResp.id" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica || canWriteIng%>" />
														</jsp:include>
														<jsp:include page="contatto_details_block.jsp" >
															<jsp:param name="mainArray" value="ingegnereResp.contatto" />
															<jsp:param name="keyREST" value="ingegnereREST" />
															<jsp:param name="soggetto" value="ingegnereResp" />
															<jsp:param name="idSogg" value="ingegnereResp.id" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica || canWriteIng%>" />
														</jsp:include>
														<tr>
														<td>
														<jsp:include page="data_agg_block.jsp" >
															<jsp:param name="dato" value="ingegnereResp" />
															<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
														</jsp:include>
														</td>
														</tr>
													</table>
												</td></tr>
											</table>
											
										</td>
									</tr>
									<tr>
										<td ng-click="ingRespParams.showhistory = !ingRespParams.showhistory" class="sogg-history">
											<div ng-show="!ingRespParams.showhistory">
												<span class="glyphicon glyphicon-plus"></span>&nbsp;Mostra Storico Ingegneri Responsabili
											</div>
											<div ng-show="ingRespParams.showhistory">
												<span class="glyphicon glyphicon-minus"></span>&nbsp;Nascondi Storico Ingegneri Responsabili
											</div>
										</td>
									</tr>
								</table>
								<% if(canWriteIng) { %>
								<button type="submit" class="btn btn-primary" ng-click="caricaIngegnere(ingegnereRespDigheREST)">
									Aggiungi Ingegnere Responsabile Esistente
								</button>
								
								<button type="submit" class="btn btn-primary" ng-click="addIngegnere(nuovoIngegnereRespForm, nuovoIngegnereResp)">
									Crea e Aggiungi Ingegnere Responsabile
								</button>
								
								<form editable-form shown="nuovoIngegnereRespForm.$visible" name="nuovoIngegnereRespForm" onaftersave="saveNuovoIngegnereResp(nuovoIngegnereRespForm, ingegnereRespDigheREST); nuovoIngegnereResp.$visible=false;" oncancel="nuovoIngegnereResp.$visible=false">
									&nbsp;<span ng-show="nuovoIngegnereResp.$visible">Nuovo Ingegnere Responsabile:</span>&nbsp;&nbsp;
									&nbsp;<span ng-show="nuovoIngegnereResp.$visible" editable-text="nuovoIngegnereResp.nome" e-placeholder="Nome">{{nuovoIngegnereResp.nome}}</span>&nbsp;&nbsp;
									&nbsp;<span ng-show="nuovoIngegnereResp.$visible" editable-text="nuovoIngegnereResp.cognome" e-placeholder="Cognome">{{nuovoIngegnereResp.cognome}}</span>&nbsp;&nbsp;
									&nbsp;<!-- span ng-show="nuovoIngegnereResp.$visible" ng-model="nuovoIngegnereResp.dataNomina" datepicker-popup="dd/MM/yyyy" is-open="false">{{nuovoIngegnereResp.dataNomina}}</span -->&nbsp;&nbsp;
									&nbsp;<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="nuovoIngegnereResp.$visible" ng-model="nuovoIngegnereResp.dataNomina" datepicker-popup="dd/MM/yyyy" is-open="nuovoIngegnereResp.open" ng-focus="nuovoIngegnereResp.open = true" ng-click="nuovoIngegnereSost.open = true" placeholder="Data Nomina"/>&nbsp;&nbsp;
									
									
									<span ng-show="nuovoIngegnereRespForm.$visible">
										<button type="submit" class="btn btn-primary" ng-disabled="nuovoIngegnereRespForm.$waiting">
											<span class="glyphicon glyphicon-ok"></span>
										</button>
										<button type="button" class="btn btn-default" ng-disabled="nuovoIngegnereRespForm.$waiting" ng-click="nuovoIngegnereRespForm.$cancel()">
											<span class="glyphicon glyphicon-remove"></span>
										</button>
									</span>
								</form>
								<% } %>
							</td>
						</tr>
						
						<tr>
							<td class="h5">Ingegneri Sostituti</td>
						</tr>
						<tr>
							<td>
								<table class="table">
									<tr class="soggName" ng-repeat="(ind, ingegnereSost) in ingegneriSost | orderBy : 'dataNominaOrder' : true" ng-show="ind==0 || ingSostParams.showhistory" ng-class="{'active': !ingegnereSost.isC_details, 'current-sogg': ind==0, 'history-sogg': ind!=0}">
										<td>
											<table class="soggTable">
												<tr class="soggName" ng-click="show_hide_Ing_details(ingegnereSost)" ng-class="{'activeSogg': !ingegnereSost.isC_details}">
													<td ng-class="{'activeSoggTd': !ingegnereSost.isC_details}">
														<form editable-form shown="ingSostForm.$visible" name="ingSostForm" onaftersave="saveIngegnere(ingegnereSost, ingSostForm, ingegnereSostDigheREST)" oncancel="removeOnEdit(ingegnereSost)">
																
															<span ng-show="!ingSostForm.$visible">{{ingegnereSost.denominazione}}</span>
															<span ng-show="ingSostForm.$visible">
																Nome:&nbsp;<span e-form="ingSostForm" editable-text="ingegnereSost.nome"></span>
																Cognome:&nbsp;<span e-form="ingSostForm" editable-text="ingegnereSost.cognome"></span>
															</span>
															
															<% if(canWriteIng) { %>
															<div class="btn-sogg">
																<button e-form="ingSostForm" type="button" class="btn btn-info" ng-click="ingSostForm.$show(); $event.stopPropagation(); ingegnereSost.onEdit=true; ingegnereSost.newDataNomina=ingegnereSost.dataNomina;" ng-show="!ingegnereSost.isC_details">
																	<span class="glyphicon glyphicon-edit"></span>
																</button>
																<button e-form="ingSostForm" type="button" class="btn btn-danger" ng-click="deleteIngegnereDiga(ingegnereSost, false); $event.stopPropagation();" ng-show="!ingegnereSost.isC_details">
																	<span class="glyphicon glyphicon-trash"></span>
																</button>
															</div>
															<span ng-show="ingSostForm.$visible">
																<br />Data&nbsp;nomina:&nbsp;<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="ingSostForm.$visible" ng-model="ingegnereSost.newDataNomina" datepicker-popup="dd/MM/yyyy" is-open="ingSostForm.open" ng-focus="ingSostForm.open = true" ng-click="ingSostForm.open = true" placeholder="Data Nomina" current-text="Oggi" clear-text="Cancella" close-text="Chiudi"/>&nbsp;&nbsp;
															</span>
															<span ng-show="!ingSostForm.$visible" style="float: right;">Data&nbsp;nomina:&nbsp;{{ingegnereSost.dataNomina}}&nbsp;&nbsp;&nbsp;</span>
															<span ng-show="ingSostForm.$visible">
																<button type="submit" class="btn btn-primary" ng-disabled="ingSostForm.$waiting">
																	<span class="glyphicon glyphicon-ok"></span>
																</button>
																<button type="button" class="btn btn-default" ng-disabled="ingSostForm.$waiting" ng-click="ingSostForm.$cancel()">
																	<span class="glyphicon glyphicon-remove"></span>
																</button>
															</span>
															<% } %>
														</form>
														
														
													</td>
												</tr>
												
												<tr><td>
													<table class="detailsTable" collapse="ingegnereSost.isC_details">
														<jsp:include page="indirizzo_details_block.jsp" >
															<jsp:param name="mainArray" value="ingegnereSost.indirizzo" />
															<jsp:param name="regione" value="empty" />
															<jsp:param name="keyREST" value="ingegnereREST" />
															<jsp:param name="soggetto" value="ingegnereSost" />
															<jsp:param name="idSogg" value="ingegnereSost.id" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica || canWriteIng%>" />
														</jsp:include>
														<jsp:include page="contatto_details_block.jsp" >
															<jsp:param name="mainArray" value="ingegnereSost.contatto" />
															<jsp:param name="keyREST" value="ingegnereREST" />
															<jsp:param name="soggetto" value="ingegnereSost" />
															<jsp:param name="idSogg" value="ingegnereSost.id" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica || canWriteIng%>" />
														</jsp:include>
														<tr>
														<td>
														<jsp:include page="data_agg_block.jsp" >
															<jsp:param name="dato" value="ingegnereSost" />
															<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
														</jsp:include>
														</td>
														</tr>
													</table>
												</td></tr>
											</table>
											
										</td>
									</tr>
									<tr>
										<td ng-click="ingSostParams.showhistory = !ingSostParams.showhistory" class="sogg-history">
											<div ng-show="!ingSostParams.showhistory">
												<span class="glyphicon glyphicon-plus"></span>&nbsp;Mostra Storico Ingegneri Sostituti
											</div>
											<div ng-show="ingSostParams.showhistory">
												<span class="glyphicon glyphicon-minus"></span>&nbsp;Nascondi Storico Ingegneri Sostituti
											</div>
										</td>
									</tr>
								</table>
								<% if(canWriteIng) { %>
								<button type="submit" class="btn btn-primary" ng-click="caricaIngegnere(ingegnereSostDigheREST)">
									Aggiungi Ingegnere Sostituto Esistente
								</button>
								
								<button type="submit" class="btn btn-primary" ng-click="addIngegnere(nuovoIngegnereSostForm, nuovoIngegnereSost)">
									Crea e Aggiungi Ingegnere Sostituto
								</button>
								
								<form editable-form shown="nuovoIngegnereSostForm.$visible" name="nuovoIngegnereSostForm" onaftersave="saveNuovoIngegnereSost(nuovoIngegnereSostForm, ingegnereSostDigheREST); nuovoIngegnereSost.$visible=false;" oncancel="nuovoIngegnereSost.$visible=false">
									&nbsp;<span ng-show="nuovoIngegnereSost.$visible">Nuovo Ingegnere Sostituto:</span>&nbsp;&nbsp;
									&nbsp;<span ng-show="nuovoIngegnereSost.$visible" editable-text="nuovoIngegnereSost.nome" e-placeholder="Nome">{{nuovoIngegnereSost.nome}}</span>&nbsp;&nbsp;
									&nbsp;<span ng-show="nuovoIngegnereSost.$visible" editable-text="nuovoIngegnereSost.cognome" e-placeholder="Cognome">{{nuovoIngegnereSost.cognome}}</span>&nbsp;&nbsp;
									&nbsp;<!-- span ng-show="nuovoIngegnereSost.$visible" ng-model="nuovoIngegnereSost.dataNomina" datepicker-popup="dd/MM/yyyy" is-open="false">{{nuovoIngegnereSost.dataNomina}}</span -->&nbsp;&nbsp;
									&nbsp;<input class="form-control editable-input datepicker-input" datepicker-options="datePickerOptions" ng-show="nuovoIngegnereSost.$visible" ng-model="nuovoIngegnereSost.dataNomina" datepicker-popup="dd/MM/yyyy" is-open="nuovoIngegnereSost.open" ng-focus="nuovoIngegnereSost.open = true" ng-click="nuovoIngegnereSost.open = true" placeholder="Data Nomina"/>&nbsp;&nbsp;
									
									
									<span ng-show="nuovoIngegnereSostForm.$visible">
										<button type="submit" class="btn btn-primary" ng-disabled="nuovoIngegnereSostForm.$waiting">
											<span class="glyphicon glyphicon-ok"></span>
										</button>
										<button type="button" class="btn btn-default" ng-disabled="nuovoIngegnereSostForm.$waiting" ng-click="nuovoIngegnereSostForm.$cancel()">
											<span class="glyphicon glyphicon-remove"></span>
										</button>
									</span>
								</form>
								<% } %>
							</td>
						</tr>
						
						<tr>
							<td class="h5">Casa di Guardia</td>
						</tr>
						<tr>
							<td>
								<form editable-form shown="casaGuardiaForm.$visible" name="casaGuardiaForm" onaftersave="saveCasaGuardia(casaGuardiaForm);">
									<table class="table">
										<tr class="soggName">
											<td><span class="detailsLabel">Telefono:&nbsp;</span><span editable-text="datiRubrica.casaDiGuarda.telefono" e-placeholder="Telefono">{{datiRubrica.casaDiGuarda.telefono}}</span>&nbsp;&nbsp;</td>
											<td><span class="detailsLabel">Telefono Posto Presidiato:&nbsp;</span><span editable-text="datiRubrica.casaDiGuarda.telefonoPostoPresidiato" e-placeholder="Telefono Posto Presidiato">{{datiRubrica.casaDiGuarda.telefonoPostoPresidiato}}</span>&nbsp;&nbsp;</td>
											<td><span class="detailsLabel">Telefono Cantiere:&nbsp;</span><span editable-text="datiRubrica.casaDiGuarda.telefonoCantiere" e-placeholder="Telefono Cantiere">{{datiRubrica.casaDiGuarda.telefonoCantiere}}</span>&nbsp;&nbsp;</td>
											<% if(canWriteIng) { %>
											<td style="width: 100px;">
												<span ng-show="casaGuardiaForm.$visible">
													<button e-form="casaGuardiaForm" type="submit" class="btn btn-primary" ng-disabled="casaGuardiaForm.$waiting">
														<span class="glyphicon glyphicon-ok"></span>
													</button>
													<button e-form="casaGuardiaForm" type="button" class="btn btn-default" ng-disabled="casaGuardiaForm.$waiting" ng-click="casaGuardiaForm.$cancel()">
														<span class="glyphicon glyphicon-remove"></span>
													</button>
												</span>
											</td>
											<td>
												<div class="btn-sogg" ng-show="!casaGuardiaForm.$visible">
													<button e-form="casaGuardiaForm" type="button" class="btn btn-info" ng-click="casaGuardiaForm.$show(); $event.stopPropagation();">
														<span class="glyphicon glyphicon-edit"></span>
													</button>
													<!-- button e-form="casaGuardiaForm" type="button" class="btn btn-danger" ng-click="deleteConcess(concessionario.id); $event.stopPropagation();" ng-show="!concessionario.isC_details">
														<span class="glyphicon glyphicon-trash"></span>
													</button -->
												</div>
											</td>
											<% } %>
										</tr>
									<tr>
										<td colspan="3">
											<jsp:include page="data_agg_block.jsp" >
												<jsp:param name="dato" value="datiRubrica.casaDiGuarda" />
												<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
											</jsp:include>
										</td>
									</tr>
						</table>
								</form>
							</td>
						</tr>
						
					</table>
				</accordion-group>
				<accordion-group heading="Amministrazioni Destinatarie Comunicazioni D.P.C.">
					<table class="table">
						<tr ng-repeat="tipoA in tipiAutorita" >
							<td>
								<span class="h5">{{tipoA.descrizione}}</span>
								<table class="table">
									<tr class="soggName" ng-repeat="autoritaPC in autorita | filter: {tipoAutorita: tipoA.id}" ng-class="{'active': !autoritaPC.isC_details}">
										<td>
											<table class="soggTable">
												<tr class="soggName" ng-click="autoritaForm.$visible ? void(0) : autoritaPC.isC_details=!autoritaPC.isC_details;" ng-class="{'activeSogg': !autoritaPC.isC_details}">
												
												<td ng-class="{'activeSoggTd': !autoritaPC.isC_details}">
													<form editable-form name="autoritaForm" onaftersave="saveAutorita(autoritaPC, autoritaForm);" oncancel="removeOnEdit(autoritaPC)" onshow="autoritaForm.selectedRegione = autoritaPC.nomeRegione">
														<div ng-show="!autoritaForm.$visible">
															{{autoritaPC.descrizione}}&nbsp;&nbsp;({{autoritaPC.nomeRegione}}&nbsp;&nbsp;{{autoritaPC.siglaProvincia}}&nbsp;&nbsp;{{getNomeTipoAutorita(autoritaPC.tipoAutorita)}})&nbsp;&nbsp;
														</div>
														<% if(canWriteAnagrafica) { %>
														
														<div ng-show="autoritaForm.$visible">
															<span e-form="autoritaForm" editable-text="autoritaPC.descrizione" e-size="50">{{autoritaPC.descrizione}}</span>
															<br />Regione:&nbsp;<span e-ng-change="autoritaForm.selectedRegione = this.$data" editable-select="autoritaPC.nomeRegione" e-ng-options="regione.nome as regione.nome for regione in (regioni | orderBy : 'nome' : false)">{{autoritaPC.nomeRegione}}</span>&nbsp;&nbsp;
															Provincia:&nbsp;<span editable-select="autoritaPC.siglaProvincia" e-ng-options="provincia.sigla as provincia.sigla for provincia in (province | filter:{nomeRegione:autoritaForm.selectedRegione, sigla:'!NN'} | orderBy : 'sigla' : false)">{{autoritaPC.siglaProvincia}}</span>&nbsp;&nbsp;
															<br />Tipo&nbsp;Autorit&agrave;:&nbsp;<span editable-select="autoritaPC.tipoAutorita" e-ng-options="tipo.id as tipo.nome for tipo in (tipiAutorita | orderBy : 'nome' : false)">{{getNomeTipoAutorita(autoritaPC.tipoAutorita)}}</span>&nbsp;&nbsp;	
														</div>
														
														<span ng-show="autoritaForm.$visible">
															<button type="submit" class="btn btn-primary" ng-disabled="autoritaForm.$waiting">
																<span class="glyphicon glyphicon-ok"></span>
															</button>
															<button type="button" class="btn btn-default" ng-disabled="autoritaForm.$waiting" ng-click="autoritaForm.$cancel()">
																<span class="glyphicon glyphicon-remove"></span>
															</button>
														</span>
														<div class="btn-sogg">
															<button e-form="autoritaForm" type="button" class="btn btn-info" ng-click="autoritaForm.$show(); $event.stopPropagation(); autoritaPC.onEdit=true;" ng-show="!autoritaPC.isC_details">
																<span class="glyphicon glyphicon-edit"></span>
															</button>
															<!--button e-form="autoritaForm" type="button" class="btn btn-danger" ng-click="deleteAutorita(autoritaPC.id); $event.stopPropagation();" ng-show="!autoritaPC.isC_details">
																<span class="glyphicon glyphicon-trash"></span>
															</button-->
														</div>
														<% } %>
													</form>
												</td>
													
													<!-- td ng-class="{'activeSoggTd': !autoritaPC.isC_details}">
														<span>{{autoritaPC.descrizione}}&nbsp;&nbsp;({{autoritaPC.nomeRegione}}&nbsp;&nbsp;{{autoritaPC.siglaProvincia}}&nbsp;&nbsp;{{getNomeTipoAutorita(autoritaPC.tipoAutorita)}})&nbsp;&nbsp;</span>
													</td -->
												</tr>
												
												<tr><td>
													<table class="detailsTable" collapse="autoritaPC.isC_details">
														<jsp:include page="indirizzo_details_block.jsp" >
															<jsp:param name="mainArray" value="autoritaPC.indirizzo" />
															<jsp:param name="regione" value="autoritaPC.nomeRegione" />
															<jsp:param name="keyREST" value="autoritaREST" />
															<jsp:param name="soggetto" value="autoritaPC" />
															<jsp:param name="idSogg" value="autoritaPC.id" />
															<jsp:param name="UtenteSoggArray" value="G_autorita_Write" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
														</jsp:include>
														<jsp:include page="contatto_details_block.jsp" >
															<jsp:param name="mainArray" value="autoritaPC.contatto" />
															<jsp:param name="keyREST" value="autoritaREST" />
															<jsp:param name="soggetto" value="autoritaPC" />
															<jsp:param name="idSogg" value="autoritaPC.id" />
															<jsp:param name="UtenteSoggArray" value="G_autorita_Write" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
														</jsp:include>
														<tr>
														<td>
															<jsp:include page="data_agg_block.jsp" >
																<jsp:param name="dato" value="autoritaPC" />
																<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
															</jsp:include>
														</td>
														</tr>
													</table>
												</td></tr>
											</table>
											
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</accordion-group>
				<accordion-group heading="Altri enti">
					<table class="table">
						
						<tr ng-repeat="tipoA in tipiAltroEnte" >
							<td>
								<span class="h5">{{tipoA.nome}}</span>
								<table class="table">
									<tr class="soggName" ng-repeat="altroEnte in altriEnti | filter: {tipoEnte: tipoA.id}" ng-class="{'active': !altroEnte.isC_details}">
										<td>
											<table class="soggTable">
												
												<tr class="soggName" ng-click="altraEnteForm.$visible ? void(0) : altroEnte.isC_details=!altroEnte.isC_details;" ng-class="{'activeSogg': !altroEnte.isC_details}">
													<td ng-class="{'activeSoggTd': !altroEnte.isC_details}">
													
													
													
														<form editable-form name="altraEnteForm" onaftersave="saveAltroEnte(altroEnte, altraEnteForm);" oncancel="removeOnEdit(altroEnte)">
															<div ng-show="!altraEnteForm.$visible">
																{{altroEnte.nome}}&nbsp;&nbsp;({{getNomeTipoEnte(altroEnte.tipoEnte)}})&nbsp;&nbsp;
															</div>
															<% if(canWriteAnagrafica) { %>
															
															<div ng-show="altraEnteForm.$visible">
																<span e-form="altraEnteForm" editable-text="altroEnte.nome" e-size="50">{{altroEnte.nome}}</span>
																<br />Tipo&nbsp;Ente:&nbsp;<span editable-select="altroEnte.tipoEnte" e-ng-options="tipo.id as tipo.nome for tipo in (tipiAltroEnte | orderBy : 'nome' : false)">{{getNomeTipoEnte(altroEnte.tipoEnte)}}</span>&nbsp;&nbsp;
															</div>
															
															<span ng-show="altraEnteForm.$visible">
																<button type="submit" class="btn btn-primary" ng-disabled="altraEnteForm.$waiting">
																	<span class="glyphicon glyphicon-ok"></span>
																</button>
																<button type="button" class="btn btn-default" ng-disabled="altraEnteForm.$waiting" ng-click="altraEnteForm.$cancel()">
																	<span class="glyphicon glyphicon-remove"></span>
																</button>
															</span>
															<div class="btn-sogg">
																<button e-form="altraEnteForm" type="button" class="btn btn-info" ng-click="altraEnteForm.$show(); $event.stopPropagation(); altroEnte.onEdit=true;" ng-show="!altroEnte.isC_details">
																	<span class="glyphicon glyphicon-edit"></span>
																</button>
																<!-- button e-form="altraEnteForm" type="button" class="btn btn-danger" ng-click="deleteAltroEnte(altroEnte.id); $event.stopPropagation();" ng-show="!altroEnte.isC_details">
																	<span class="glyphicon glyphicon-trash"></span>
																</button -->
															</div>
															<% } %>
														</form>
														
														
													</td>
												</tr>
												
												
												<tr><td>
													<table class="detailsTable" collapse="altroEnte.isC_details">
														<jsp:include page="indirizzo_details_block.jsp" >
															<jsp:param name="mainArray" value="altroEnte.indirizzo" />
															<jsp:param name="regione" value="empty" />
															<jsp:param name="keyREST" value="altraEnteREST" />
															<jsp:param name="soggetto" value="altroEnte" />
															<jsp:param name="idSogg" value="altroEnte.id" />
															<jsp:param name="UtenteSoggArray" value="G_altroEnte_Write" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
														</jsp:include>
														<jsp:include page="contatto_details_block.jsp" >
															<jsp:param name="mainArray" value="altroEnte.contatto" />
															<jsp:param name="keyREST" value="altraEnteREST" />
															<jsp:param name="soggetto" value="altroEnte" />
															<jsp:param name="idSogg" value="altroEnte.id" />
															<jsp:param name="UtenteSoggArray" value="G_altroEnte_Write" />
															<jsp:param name="canWrite" value="<%=canWriteAnagrafica%>" />
														</jsp:include>
														<tr>
														<td>
															<jsp:include page="data_agg_block.jsp" >
																<jsp:param name="dato" value="altroEnte" />
																<jsp:param name="canReadDataAgg" value="<%=permessiRubrica.get(\"canReadUpdate\")%>" />
															</jsp:include>
														</td>
														</tr>
													</table>
												</td></tr>
											</table>
											
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</accordion-group>
			</accordion>
			<div class="underMenu">
				<button type="button" class="btn btn-danger" onClick="window.close();" >
					Chiudi
				</button>
			<div>
			
		</div>
		<script type="text/javascript">
			G_token = "<%=tokenREST%>";
			G_userID = "<%=userId%>";
			G_damID = '<%=request.getParameter("dam")%>'
		</script>
		<script type="text/ng-template" id="addIngTemplate"><%@ include file="popup_add_ing.jsp" %></script>
		
		<%@ include file="js_user_permissions.jsp" %>

		<script type="text/javascript" src="resources/jquery/v2.1.1/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="resources/jquery/ui-v1.10.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resources/jquery/ui.touch-punch/jquery.ui.touch-punch.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/v1.2.26/angular.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/table/ng-table.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/bootstrap-v0.12.0/ui-bootstrap-tpls-0.12.0.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/dialogs/dialogs.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/ngDialog/ngDialog.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/grid-v2.0.14/ng-grid-2.0.14.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/xeditable/xeditable.js"></script>
		<script type="text/javascript" src="resources/angularjs/angular-dragdrop/angular-dragdrop.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/angular-touch-v1.2.28/angular-touch.min.js"></script>
		<script type="text/javascript" src="resources/cryptojs/v3.1.2/sha1.js"></script>
		<script type="text/javascript" src="resources/angularjs/angular-sanitize-v1.0.4/angular-sanitize.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/locale_it/angular-locale_it-it.js"></script>
		<script type="text/javascript" src="resources/scripts/rubricaInit.js"></script>
		<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
		<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
		<script type="text/javascript" src="resources/scripts/rubricaApp.js"></script>
<%
 }
 else{
 %>
	<script type="text/javascript">
		window.close();
	</script>
<% } %>

	</body>
</HTML>
