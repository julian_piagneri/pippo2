<div id="error-loading" class="error-loading" style="text-align: center; font-size: 12px; display: none">
	<div style="font-size: 14px; cursor: pointer; color: #000000" onclick="location.reload();">
		<span class="glyphicon glyphicon-repeat"></span> Ricarica
	</div>
	Il sistema sta impiegando troppo tempo a rispondere
</div>
<div id="loading" class="loading">
    <div class="outer"></div>
    <div class="inner"></div>
</div>
<script>
	function showErrorLoading(loadingId, errorLoadingId){
		var tempErrorLoadingId=errorLoadingId;
		var tempLoadingId=loadingId;
		setTimeout(function(){ 
			$("#"+tempLoadingId).show(); $("#"+tempErrorLoadingId).hide(); 
		}, 60000);
	}
	
	var errorLoadingDom=$("#error-loading");
	var loadingDom=$("#loading");
	var time=new Date().getTime();
	errorLoadingDom.attr("id", errorLoadingDom.attr("id")+time);
	loadingDom.attr("id", loadingDom.attr("id")+time);
	var errorLoadingId=errorLoadingDom.attr("id");
	var loadingId=loadingDom.attr("id");
	$("#"+errorLoadingId).hide(); 
	$("#"+loadingId).show(); 
	
	
	
	showErrorLoading(errorLoadingId, loadingId);
</script>