<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-grid.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/dialogs.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-table.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/xeditable.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-plain.css">
		<!-- link rel="stylesheet" type="text/css" href="resources/style/main.css" media="screen" -->
		<link rel="stylesheet" type="text/css" href="resources/style/main.css" >
		<link rel="stylesheet" type="text/css" href="resources/style/text-align-responsive.css">
		<link rel="stylesheet" type="text/css" href="resources/style/damlist.css">
        <link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap-switch.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon-regular.css">
        <link rel="stylesheet" type="text/css" href="resources/style/scheda-print.css" media="print" >
        <!-- link rel="stylesheet" type="text/css" href="resources/style/scheda-print.css" -->
		

        <!--<link rel="stylesheet" type="text/css" href="resources/style/mappa.css">
        <link rel="stylesheet" type="text/css" href="resources/style/ol.css">
        <link rel="stylesheet" type="text/css" href="resources/style/dialogs-5.css">
        <link rel="stylesheet" type="text/css" href="resources/jquery-ui/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="resources/jquery-ui/jquery-ui.structure.css">
        <link rel="stylesheet" type="text/css" href="resources/jquery-ui/jquery-ui.theme.css">-->

	</head>
	<body ng-app="<% out.print(appName);%>">

		<%@ include file="header-logo.jsp" %>
		
		<p class="TitoloScheda" align="CENTER"> <%=titoloPagina%> </p>