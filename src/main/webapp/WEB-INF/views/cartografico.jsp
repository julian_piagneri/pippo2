<%@ include file="include/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Cartografico</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="resources/style/mappa.css">
		<link rel="stylesheet" type="text/css" href="resources/style/main.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ol.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-grid.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap-switch.css">
		<link rel="stylesheet" type="text/css" href="resources/style/glyphicon.css">
		<link rel="stylesheet" type="text/css" href="resources/style/glyphicon-regular.css">
		<link rel="stylesheet" type="text/css" href="resources/style/dialogs-5.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-plain.css">
		<link rel="stylesheet" type="text/css" href="resources/style/overlay-list.css">
		
		<link rel="stylesheet" type="text/css" href="resources/jquery-ui/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="resources/jquery-ui/jquery-ui.structure.css">
		<link rel="stylesheet" type="text/css" href="resources/jquery-ui/jquery-ui.theme.css">
		<a href="#" class="arrow"></a>
		<a href="javascript:;" class="arrow"></a>
	</head>
	<body ng-app="damMapApp">
		<c:set var="userName"><sec:authentication property="name" /></c:set>
		<% 
		String userId=pageContext.getAttribute("userName").toString(); 
		String title="Cartografico";
		String percorsoMenu ="\\Cartografico";
		%>
		<%@ include file="header-logo-map.jsp" %>
		
		
		<!-- div class="btn-group">
			  <button type="button" id="PanSelect" class="btn btn-default">PanSelect</button>
			  <button type="button" id="Box" class="btn btn-default">Box</button>
			  <button type="button" id="Poly" class="btn btn-default">Poly</button>
			  <button type="button" id="Circle" class="btn btn-default">Circle</button>
			  <button type="button" id="Intersect" class="btn btn-default">SelDigheFromFeat</button>
			  <button type="button" id="swap" class="btn btn-default">Swap Top Layers</button>
		</div -->
		
		
		<!--<script src="http://maps.google.com/maps/api/js?v=3&amp;sensor=false"></script> -->
		
		<div ng-controller="mapLayerController" id="layerList" ng-init="initMap()" class="container-fluid">
			<div id="tooltdiv" title="tooltmaps" style="overflow: hidden; position: absolute; width: 1px; height: 1px;"></div>
			<div class="row">
				<div id="map" style="margin-bottom: 10px;" class="map col-xs-12">
				<div id="popup" class="ol-popup" style="position: absolute">
					<!-- a href="#" id="popup-closer" class="ol-popup-closer"></a -->
					<span id="popup-closer" class="ol-popup-closer"></span>
					<div id="popup-content"></div>
				</div>
				<div id="map-buttons" ng-show="mapLoadComplete" class="ng-hide btnBox btn-default btn-dad col-xs-9 col-sm-6 col-md-4" 
					data-drag="true" data-jqyoui-options='{containment: "#map", axis:"x"}' jqyoui-draggable="{ animate: true}" style="overflow: hidden;">
					
					<div id="mapMenu" class="col-xs-12" style="padding-left: 0px; padding-right:0px;">
				
					<div id="map-controls" class="col-xs-12" style="padding-bottom:5px; text-align: center;">
					<!--<div class="col-xs-12" style="padding-left: 0px; padding-right: 0px;">-->
						<div class="col-xs-7" style="padding-left: 0px; padding-right: 2px;">
						<div class="btn-group">
							<button tooltip-append-to-body="true" tooltip="Pan" class="btn btn-primary" ng-class="{'btn-selected': btnActive.pan}" ng-click="selectPan()" style="" type="button">
								<span class="glyphicon glyphicon-068"></span>
							</button>
							<button tooltip-append-to-body="true" tooltip="Seleziona da cartografia" class="btn btn-primary" ng-class="{'btn-selected': btnActive.feat}" ng-click="selectFeat()" style="" type="button">
								<span class="glyphicons-reg glyphicon-336"></span>
							</button>
							<button tooltip-append-to-body="true" tooltip="Leggi/Cattura coordinate" class="btn btn-primary" ng-class="{'btn-selected': btnActive.marker}" ng-click="selectMarker()" style="" type="button">
								<span class="glyphicons-reg glyphicon-243"></span>
							</button>
							<button tooltip-append-to-body="true" tooltip="Selezione Box" class="btn btn-primary" ng-class="{'btn-selected': btnActive.box}" ng-click="selectBox()" style="" type="button">
								<span class="glyphicons-reg glyphicon-095"></span>
							</button>
							<button tooltip-append-to-body="true" tooltip="Selezione Circolare" class="btn btn-primary" ng-class="{'btn-selected': btnActive.circle}" ng-click="selectCircle()" style="" type="button">
								<span class="glyphicons-reg glyphicon-096"></span>
							</button>
							<button tooltip-append-to-body="true" tooltip="Selezione Poligonale" class="btn btn-primary" ng-class="{'btn-selected': btnActive.poly}" ng-click="selectpoly()" style="" type="button">
								<span class="glyphicons-reg glyphicon-097"></span>
							</button>
							<!-- button class="btn btn-primary" ng-class="{'btn-selected': btnActive.featuredam}" ng-click="selectFeatureDam()" style="" type="button">
								<span class="glyphicons-reg glyphicon-015"></span>
							</button -->
						</div>
						</div>
						<select id="layer-select" class="col-xs-4" style="padding-bottom:5px; padding-top:4px; padding-left:0px; ">
							<option value="Road">Bing Strade</option>
							<option value="Aerial">Bing Satellite</option>
							<option value="AerialWithLabels" selected>Bing Ibrida</option>
							<option value="OSM">OSM Strade</option>
							<option value="SAT">OSM Satellite</option>
							<option value="OSM2">OSM2</option>
							<option value="GooglePhysical">Google Fisica</option>
							<option value="GoogleSatellite">Google Satellite</option>
							<option value="GoogleHybrid">Google Ibrida</option>
							<option value="GoogleStreet">Google Strade</option>
							<option value="GoogleWaterOverlay">Google Water</option>
							<option value="HereStreet">Here Strade</option>
							<option value="HereSatellite">Here Satellite</option>
							<option value="HereHybrid">Here Ibrida</option>
							<option value="HereTraffic">Here Traffico</option>
						</select>		
						<div class="col-xs-1" style="padding-left: 2px; padding-right: 0px;">
						<div class="btn-group">
						<button tooltip-append-to-body="true" tooltip="Mostra/Nascondi controlli mappa" class="btn btn-primary" ng-click="showLayerControls()" type="button">
								<span class="glyphicon" ng-class="{'glyphicon-chevron-up': showAccordions, 'glyphicon-chevron-down': !showAccordions }"></span>
						</button>
						</div>
						</div>
					<!--</div>-->
					</div>
				
					<div id="map-layers" class="col-xs-12 ng-hide" ng-show="showAccordions" style="padding-left: 2px; padding-right: 2px; overflow: auto; trasparent:0.5;">
						<accordion close-others="false">
							<accordion-group heading="Layer di Selezione" is-open="showAccordionElement1.active"><!--is-open="openA" ng-click="openA = true"-->
							
							<div class="col-xs-12 text-center" >
								<h4>Feature Selezionata</h4>
							</div>
								<div div ng-repeat="feature in selectionList" class="draggableSogg btnBox btn-default btn-dad col-xs-12">
									<div class="col-xs-10" style="padding-top:4px; text-align: left;">
										<span ng-show="feature.legend_name">{{feature.legend_name}} - </span>
										<span class="col-xs-12" style="white-space:pre-line;">{{feature.GeometryName}}</span>
									</div>
									<div class="col-xs-2">
										<button class="btn btn-danger" ng-click="removeSelection(feature)" type="button">
											<span class="glyphicon glyphicon-trash"></span>
										</button>
									</div>
								</div>
								<br/>	
							
							<div class="col-xs-12 text-center" >
								<h4>Filtra per Selezione</h4>
							</div>
							<div class="draggableSogg btnBox btn-default btn-dad col-xs-12">
								<div class="col-xs-12" style="padding-bottom:5px; text-align: left;">
									<div class="col-xs-8">
										Filtra&nbsp;dighe&nbsp;selezionate:&nbsp;
									</div>
									<div class="col-xs-4">
										<input bs-switch switch-size="mini" switch-change="filtraDighe()" type="checkbox" ng-checked="filterDam.active" ng-model="filterDam.active" />
										<!--<button class="btn btn-primary btn-sm" ng-click="damsFilter()" type="button">
											<span class="glyphicon glyphicon-138"></span>
										</button>-->
									</div>
								</div>
								<div class="col-xs-12" style="padding-bottom:5px; text-align: left;">
									<div class="col-xs-8">
										Filtra&nbsp;layers&nbsp;selezionati:&nbsp;
									</div>
									<div class="col-xs-4">
										<input bs-switch switch-size="mini" switch-change="filtraLayer()" type="checkbox" ng-checked="filterLayer.active" ng-model="filterLayer.active" />
									</div>
										
								</div>
								<!--<img width="32px" height="32px" border="0" ng-show="mostraCaricamento.active" src="resources/img/wait.gif">-->
								<div class="col-xs-12" style="padding-bottom:5px; text-align: left;">
									<div class="col-xs-4">
									</div>
									<div class="col-xs-4">
										<!--<input bs-switch switch-size="mini" switch-change="filtraLayer()" type="checkbox" ng-checked="filterLayer.active" ng-model="filterLayer.active" />-->
										<button class="btn btn-primary btn-sm" ng-click="openReport()" type="button">
											<!--<span class="glyphicon glyphicon-138"></span>-->
											<span>Apri report builder</span>
										</button>
									</div>
									<div class="col-xs-4">
									</div>
										
								</div>
							</div>
								<br/>
								<br/>
								<br/>	
							</accordion-group>
							<accordion-group heading="Layer" is-open="showAccordionElement2.active">
								<div class="col-xs-8">
									Solo visibili per il livello di zoom
								</div>
								<div class="col-xs-4">
									<input bs-switch switch-size="mini" switch-change="" type="checkbox" ng-checked="layersOptions.zoomVisible" ng-model="layersOptions.zoomVisible" />
								</div>
								</br>
								<div class="col-xs-8">
									Solo attivi
								</div>
								<div class="col-xs-4">
									<input bs-switch switch-size="mini" switch-change="" type="checkbox" ng-checked="layersOptions.activeVisible" ng-model="layersOptions.activeVisible" />
								</div>
								
								<div class="col-xs-12"><br /></div>
								<!-- <div ng-show="(layer.display || !layersOptions.activeVisible) && (isResolution(layer.maxResolution) || !layersOptions.zoomVisible)" class="draggableSogg btnBox btn-default btn-dad col-xs-12" ng-repeat="layer in layerDigList | orderBy:'position' : true">-->
								<div ng-show="(layer.display || !layersOptions.activeVisible) && (isResolution(layer.maxResolution) || !layersOptions.zoomVisible)" class="draggableSogg btnBox btn-default btn-dad col-xs-12" ng-repeat="layer in layerDigList | orderBy:'position' : true"  style="background-color:#ccc">
									<!-- div class="col-xs-4" ng-style="getLegendStyle(layer.type, layer.stroke_width, layer.stroke_color, layer.fill_color, layer.radius)"></div -->
									<div class="col-xs-3">
										<div class="col-xs-12" ng-style="getLegendStyle(layer.type, layer.stroke_width, layer.stroke_color, layer.fill_color, layer.radius)">
										</div>
									</div>
									<div class="col-xs-6">
										{{layer.legend_name}}
									</div>
									<!--<div class="col-xs-2">
										<button class="leggi" ng-class="{'btn-selected': btnActive.list}" ng-click="leggiFeatures(layer)" style="align:center; height: 25px; text-allign: top" type="button" />
										<b>></b>
									</div> -->
									<!-- div ng-click="alert('ciao');layer.olLayer.setVisible(false);switchVisible(layer.olLayer);">Visible</div -->
									<div class="col-xs-12" style="margin-bottom: 4px; margin-top: 4px"></div>
									<div class="col-xs-3">
										<!--<input bs-switch switch-size="mini" switch-change="layer.olLayer.setVisible(!layer.olLayer.getVisible())" type="checkbox" ng-checked="layer.display" ng-model="layer.display" />-->
									</div>
									<div class="col-xs-9">
										<!--<input class="opacity" type="range" min="0" max="1" step="0.01" ng-model="layer.opacity" ng-change="changeOpac(layer)" />-->
									</div>
									<img width="32px" height="32px" border="0" ng-show="layer.olLayer.getSource().isLoadding" src="resources/img/wait.gif">
									<br />
									
								</div>
								
								<div ng-show="(layer.display || !layersOptions.activeVisible) && (isResolution(layer.maxResolution) || !layersOptions.zoomVisible)" data-drop="true" ng-model='dropModel' jqyoui-droppable="{multiple:true, onDrop:'moveLayer(layer)'}" class="draggableSogg btnBox btn-default btn-dad col-xs-12" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" jqyoui-draggable="{ animate: true, placeholder: 'keep', onStart:'getDragLayer(layer)'}" ng-repeat="layer in layerList | orderBy:'position' : true">
									<!-- div class="col-xs-4" ng-style="getLegendStyle(layer.type, layer.stroke_width, layer.stroke_color, layer.fill_color, layer.radius)"></div -->
									<div class="col-xs-3">
										<div class="col-xs-12" ng-style="getLegendStyle(layer.type, layer.stroke_width, layer.stroke_color, layer.fill_color, layer.radius)">
										</div>
									</div>
									<div class="col-xs-7">
										{{layer.legend_name}}
									</div>
									<div class="col-xs-1">
										<button tooltip-append-to-body="true" tooltip="Mostra features" class="btn btn-primary btn-xs" ng-disabled="isVisible(1000)" ng-click="leggiFeatures(layer)" style="align:center; height: 25px; text-allign: top" type="button">
											<span class="glyphicon" ng-class="{'glyphicon-chevron-up': layer.olLayer.getSource().showFeature, 'glyphicon-chevron-down': !layer.olLayer.getSource().showFeature }"></span>
										</button>
									</div>
									<!--<div class="col-xs-1">
										<button tooltip-append-to-body="true" tooltip="Mostra features" class="btn btn-primary btn-xs"" ng-disabled="isVisible(1000)" ng-click="leggiFeatures(layer)" style="align:center; height: 25px; text-allign: top" type="button">
											<span class="glyphicon" ng-class="{'glyphicon-chevron-up': layer.showFeature, 'glyphicon-chevron-down': !layer.showFeature }"></span>
										</button>
									</div>
									<div class="col-xs-1">
										<button tooltip-append-to-body="true" tooltip="Mostra campi" class="btn btn-success btn-xs"" ng-click="leggiCampi(layer)" style="align:center; height: 25px; text-allign: top" type="button">
											<span class="glyphicon" ng-class="{'glyphicon-chevron-up': layer.showColumn, 'glyphicon-chevron-down': !layer.showColumn }"></span>
										</button>
									</div>-->
									
									<!-- div ng-click="alert('ciao');layer.olLayer.setVisible(false);switchVisible(layer.olLayer);">Visible</div -->
									<div class="col-xs-12" style="margin-bottom: 4px; margin-top: 4px"></div>
									<div class="col-xs-3">
										<input bs-switch switch-size="mini" switch-change="layerVisible(layer)" type="checkbox" ng-checked="layerVisibleCheck(layer)" ng-model="layer.display" />
										<!--<input bs-switch switch-size="mini" switch-change="layer.olLayer.setVisible(!layer.olLayer.getVisible())" type="checkbox" ng-checked="layer.display" ng-model="layer.display" />-->
									</div>
									<div class="col-xs-9">
										<input class="opacity" type="range" min="0" max="1" step="0.01" ng-model="layer.opacity" ng-change="changeOpac(layer)" />
									</div>
									<img width="32px" height="32px" border="0" ng-show="layer.olLayer.getSource().isLoadding" src="resources/img/wait.gif">
									
									<div class="col-xs-9" ng-show="layer.showColumn" >
										<div class="col-xs-12" style="margin-bottom: 4px; margin-top: 4px"></div>
										<div  ng-repeat="column in columnList" class="draggableSogg btnBox btn-default btn-dad col-xs-12" align="left">	
											<div class="bb-button-label col-xs-12" ng-click="">
												<div class="col-xs-6">
										          <input type="checkbox" ng-checked="" />
												</div>	
												<span class="bb-button-label col-xs-6" style="white-space:pre-wrap;">{{column}}</span>						
											</div>
										</div>	
									</div>
									
									<div class="col-xs-12" ng-show="layer.olLayer.getSource().showFeature">
										<div class="col-xs-12" style="margin-bottom: 4px; margin-top: 4px"></div>
										<table class="table">
											<div  ng-repeat="feature in getSubArray(currentPage*pageSize, currentPage*pageSize+pageSize) | limitTo:pageSize" class="draggableSogg btnBox btn-default btn-dad col-xs-12" align="center" ng-show="!isVisible(1000)">
												<div  class="bb-button-label col-xs-12" ng-click="selectFeatureFromList(feature)">
													<span class="bb-button-label col-xs-10" style="white-space:pre-wrap;">{{feature.GeometryName}}</span>
													<div class="col-xs-2">
														<button class="btn btn-link btn-xs" ng-click="zoomToSel(feature)" type="button">
														<span class="glyphicon glyphicon-140"></span>
														</button>
													</div>
												</div>
											</div>
										</table>
										<button class="btn btn-info btn-xs" ng-disabled="currentPage <= 0" ng-click="currentPage=currentPage-1"><<</button>
										<button class="btn btn-info btn-xs" ng-disabled="currentPage >= numberOfPages()-1" ng-click="currentPage=currentPage+1">>></button>
										Pagina {{currentPage+1}} di {{numberOfPages()}} 
										<!--<div  ng-repeat="feature in featureList | orderBy:'GeometryName'" class="draggableSogg btnBox btn-default btn-dad col-xs-12" align="center" ng-show="!isVisible(300)">	
											<div class="bb-button-label col-xs-12" ng-click="selectFeatureFromList(feature)">
												<span class="bb-button-label col-xs-10">{{feature.GeometryName}}</span>
												<div class="col-xs-2">
													<button class="btn btn-link btn-xs" ng-click="zoomToSel(feature)" type="button">
													<span class="glyphicon glyphicon-140"></span>
													</button>
												</div>
																				
											</div>
										</div>-->	
									</div>
									<br />
									
								</div>
							</accordion-group>
							<accordion-group heading="Sisma">
								Lat:&nbsp;<input ng-model="latlong.lat" type="text" class="form-control" style="{width: auto}" ng-disabled="!btnActive.marker" />
								Long:&nbsp;<input ng-model="latlong.long" type="text" class="form-control" size="{width: auto}" ng-disabled="!btnActive.marker" />
								Magnitudo:&nbsp;<input ng-model="sisma.magnitudo" ng-change="calcolaRaggio()" type="number" step="0.1" min="0.0" class="form-control" size="{width: auto}" ng-disabled="btnActive.marker" />
								Raggio&nbsp;(m):&nbsp;<input ng-model="sisma.raggio" type="text" class="form-control" size="{width: auto}" ng-disabled="true" />
								</label><br/>	
								<label>								
									<input type="radio" ng-model="sisma.type" value="0" ng-change="calcolaRaggio()" ng-checked = "sisma.type == 0">
									Raggio del sisma
								</label><br/>
								<label>
									<input type="radio" ng-model="sisma.type" value="1" ng-change="calcolaRaggio()" ng-checked = "sisma.type == 1">
									Controlli di Tipo 1
								</label><br/>
								<label>
									<input type="radio" ng-model="sisma.type" value="2" ng-change="calcolaRaggio()" ng-checked = "sisma.type == 2">
									Controlli di Tipo 2
								</label><br/>
								
								<div class="btn-group">
									<button class="btn btn-primary" ng-class="{'btn-selected': btnActive.sisma}" ng-click="calculateSisma()" style="" type="button" />
									<b>SISMA</b>
								  <label>

								</div> 
							</accordion-group>
							<accordion-group heading="Buffer">
								<input bs-switch switch-size="mini" switch-change="" ng-change="bufferEvent()" type="checkbox" ng-checked="buffer.active" ng-model="buffer.active" /><br /><br />
								<button class="btn btn-primary" ng-click="Buffer2SelectLayer()" style="" type="button">
									<b>Passa a "Layer di Selezione"/"Overlay"</b>
								</button>
								<br />
								<br />
								Raggio&nbsp;(km):&nbsp;<input ng-model="buffer.raggio" type="number" step="0.01" min="0.00" class="form-control" size="{width: auto}" ng-disabled="!buffer.active" />
								</label><br/>	
								<div class="btn-group">
									<button class="btn btn-primary" ng-click="turf('buffer')" style="" type="button" />
									<b>BUFFER</b>
								</div><br/>	<br/>	
								<div ng-repeat="feature in bufferList" class="btnBox btn-default col-xs-12 btn-dad draggableSogg">
									<!--<div class="col-xs-1">
										<input  class="action-checkbox" type="checkbox" style="{width: auto}" ng-checked="feature.active" ng-model="feature.active"/>
									</div>-->
									<div class="col-xs-10" style="padding-top:4px; text-align: left;">  
										<span ng-show="feature.legend_name"style="white-space:pre-wrap;">{{feature.legend_name}} - </span>
										<span class="col-xs-12"style="white-space:pre-wrap;">{{feature.GeometryName}}</span>
									</div>
									<div class="col-xs-2">
										<button class="btn btn-danger" ng-click="removeBuffer(feature)" type="button">
											<span class="glyphicon glyphicon-trash"></span>
										</button>
									</div>
								<br/>	
								</div>

							</accordion-group>
							<accordion-group heading="Overlay">
								<input bs-switch switch-size="mini" switch-change="" ng-change="overlayEvent()" type="checkbox" ng-checked="overlay.active" ng-model="overlay.active" /><br /><br />
								<div class="btn-group">
									<button tooltip-append-to-body="true" tooltip="Aggiunge la feature a 'Buffer' quando la relativa scheda risulta attiva" class="btn btn-primary" ng-click="Overlay2SelectLayer()" style="" type="button">
										<b>Passa a "Layer di Selezione"/"Buffer"</b>
									</button>
								</div>
								<br />
								<br />
								<div class="btn-group">
									<button tooltip-append-to-body="true" tooltip="Unione" class="btn btn-primary" ng-click="turf('union')" style="" type="button">
										<b>+</b>
									</button>
									<button tooltip-append-to-body="true" tooltip="Differenza" class="btn btn-primary" ng-click="turf('diff')" style="" type="button">
										<b>-</b>
									</button>
									<button tooltip-append-to-body="true" tooltip="Intersezione" class="btn btn-primary" ng-click="turf('intersect')" style="" type="button">
										<b>*</b>
									</button>
								</div>
								<br />
								<br />
								<div ng-repeat="feature in overlayList" class="btnBox btn-default col-xs-12 btn-dad draggableSogg" ng-model='dropModel' data-drop="true" jqyoui-droppable="{multiple:false, onDrop:'moveOverlay(feature)'}" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" jqyoui-draggable="{ animate: true, placeholder: 'keep', onStart:'getDragOverlay(feature)'}">
									<div class="col-xs-1" style="padding-top:4px; text-align: left;">
										<input  class="action-checkbox" type="checkbox" ng-checked="feature.active" ng-model="feature.active"/>
									</div>
									<div class="col-xs-9"  style="padding-top:4px; text-align: left;">
										<span ng-show="feature.legend_name"style="white-space:pre-wrap;">{{feature.legend_name}} - </span>
										<!--<span ng-show="feature.layerName"style="white-space:pre-wrap;">{{feature.layerName}}</span>-->
										<span class="col-xs-12"style="white-space:pre-wrap;">{{feature.GeometryName}}</span>
									</div>
									<div class="col-xs-2">
										<button class="btn btn-danger" ng-click="removeOverlay(feature)" type="button">
											<span class="glyphicon glyphicon-trash"></span>
										</button>
									</div>
								</div>
								
							</accordion-group>
						</accordion>
					</div>
				</div>
				</div>
				</div>
			</div>
			
		</div>
		
							
		<script type="text/javascript" src="resources/jquery/v2.1.1/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="resources/jquery/ui-v1.10.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="resources/jquery/ui.touch-punch/jquery.ui.touch-punch.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/v1.2.26/angular.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/grid-v2.0.14/ng-grid-2.0.14.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/grid-v2.0.14/ng-grid-layout.js"></script>
		
		<script type="text/javascript" src="resources/openlayers/v3.4.0/build/ol.js"></script>
		<script type="text/javascript" src="resources/turf/turf.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/bootstrap-v0.12.0/ui-bootstrap-tpls-0.12.0.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/angular-dragdrop/angular-dragdrop.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/angular-touch-v1.2.28/angular-touch.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/dialogs/dialogs.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/ngDialog/ngDialog.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/xeditable/xeditable.js"></script>
		<script type="text/javascript" src="resources/scripts/bootstrap/bootstrap-switch.min.js"></script>
		<script type="text/javascript" src="resources/scripts/lib/angular-bootstrap-switch.js"></script>
		<script type="text/javascript" src="resources/angularjs/angular-sanitize-v1.0.4/angular-sanitize.min.js"></script>
		<script type="text/javascript" src="resources/scripts/lib/routes.js"></script>
		
		<script type="text/javascript" src="resources/scripts/lib/debounce.js"></script>
		
		
		<!-- script type="text/javascript" src="resources/scripts/lib/cartografico.js"></script -->
		<script type="text/javascript" src="resources/scripts/configPopupDams.js"></script>
		<script type="text/javascript" src="resources/scripts/configPopupRivers.js"></script>
		<script type="text/javascript" src="resources/scripts/lib/olEngine.js"></script>
		
		<script type="text/javascript" src="resources/scripts/cartoInit.js"></script>
		<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
		<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
		<script type="text/javascript" src="resources/scripts/cartoApp.js"></script>
		
		<script type="text/javascript" src="resources/scripts/services/storageService.js"></script>
		<script type="text/javascript" src="resources/scripts/services/dialogService.js"></script>
		<script type="text/javascript" src="resources/scripts/services/helpersService.js"></script>
		<script type="text/javascript" src="resources/angularjs/dialogs/dialogs-5.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/dialogs/dialogs-default-translations.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/ngDialog/ngDialog.min.js"></script>
		
		<!-- script type="text/javascript" src="resources/scripts/app3.js"></script -->
		
	</body>
</html>