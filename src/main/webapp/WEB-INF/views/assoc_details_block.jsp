<% 
boolean canWrite = false;
if(request.getParameter("canWrite") != null){
	if(request.getParameter("canWrite").equals("true")) canWrite = true;
}
%>

<tr><td class="h5">${param.titolo}</td></tr>
<tr>
	<td>
		<table>
			<tr>
				<td class="dad_td">
					<!-- table data-drop="true" ng-model='dropModel' jqyoui-droppable="{multiple:true, onDrop:'assocSogg(${param.soggetto}, ${param.keyREST})'}" -->
					<table data-drop="true" ng-model='dropModel' >
						<tr class="detailsRow" ng-repeat="(ind, soggAssoc) in (${param.mainArray} | orderBy : '${param.orderField}' : false)" ng-show="ind < maxAssocListNumber">
							<td>
								{{soggAssoc.${param.displayName}}}
							</td>
						</tr>
						<tr ng-show="${param.mainArray}.length > maxAssocListNumber">
							<td>
								+ {{${param.mainArray}.length-maxAssocListNumber}} altri/e ${param.titolo}
								<% if(!canWrite) { %>
									<div class="draggableSogg btn btn-default" ng-click="openPopupViewList(${param.mainArray}, '${param.displayName}', '${param.titolo} a ' + ${param.nomeSogg}, '${param.titolo}', '${param.orderField}')"> <span class="glyphicon glyphicon-eye-open"></span> </div>
								<% } %>
							</td>
						</tr>
						<% if(canWrite) { %>
						<tr>
							<td>
								<div class="draggableSogg btn btn-default" ng-click="openPopupAssoc(${param.mainArray}, ${param.assocArray}, '${param.displayName}', ${param.keyREST}, ${param.assocDigheKeyREST}, ${param.soggetto}, '${param.titolo} a ' + ${param.nomeSogg}, '${param.titolo}', '${param.sTitolo2}', '${param.orderField}', ${param.alertFct})"> <span class="glyphicon glyphicon-wrench"></span> </div>
							</td>
						</tr>
						<% } %>
					</table>
				</td>
				<td class="dad_td">
					<!-- table class="ng-table-assoc" show-filter="true">
						<tr>
							<input class="input-filter form-control ng-scope ng-pristine ng-valid" type="text" ng-model="${param.soggetto}.gestoriAssocFilter" />
						</tr>
						<tr ng-repeat="sogg2Assoc in ${param.soggetto}.gestoriAssoc | filter: ${param.soggetto}.gestoriAssocFilter">
							<td>
								<div class="draggableSogg btn btn-default" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" ng-model="${param.assocArray}" ng-hide="!sogg2Assoc.nome" jqyoui-draggable="{ animate: true, placeholder: 'keep', onStart:'getDragOgg(sogg2Assoc)'}">{{sogg2Assoc.nome}}</div>
							</td>
						</tr>
					</table -->
				</td>
			</tr>
		</table>
	</td>
</tr>