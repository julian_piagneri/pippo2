<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String schedaId = "183";
String percorsoMenu = "\\Gestione Tecnica\\Rivalutazione idrologico idrauliche";
String titoloPagina = "Scheda Rivalutazione idrologico-idrauliche";
boolean canWrite=false;
boolean canRead=false;
%>
<%@ include file="user_permissions.jsp" %>
<%
canWrite=permessiRivalutazione.get("canWrite");
canRead=permessiRivalutazione.get("canRead");

//test
// canRead = true;
// canWrite = true;

String appName="schedaRivApp";
String controller = "schedaRivController";
%>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-grid.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/dialogs.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ng-table.min.css">
		<link rel="stylesheet" type="text/css" href="resources/style/xeditable.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-default.css">
		<link rel="stylesheet" type="text/css" href="resources/style/ngDialog-theme-plain.css">
		<link rel="stylesheet" type="text/css" href="resources/style/main.css">
		<link rel="stylesheet" type="text/css" href="resources/style/text-align-responsive.css">
		<link rel="stylesheet" type="text/css" href="resources/style/damlist.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon.css">
        <link rel="stylesheet" type="text/css" href="resources/style/glyphicon-regular.css">
        <link rel="stylesheet" type="text/css" href="resources/style/scheda-print.css" media="print" >
		<%@ include file="scripts_block.jsp" %>
		<script type="text/javascript">
			G_token = "<%=tokenREST%>";
			G_userID = "<%=userId%>";
			G_damID = '<%=request.getParameter("dam")%>'
		</script>
		<script type="text/ng-template" id="addIngTemplate"><%@ include file="popup_add_ing.jsp" %></script>
		<%@ include file="js_user_permissions.jsp" %>
		<script type="text/javascript" src="resources/scripts/schedaRivModule.js"></script>
		<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
		<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
		<script type="text/javascript" src="resources/scripts/schedaRivApp.js"></script>
		<%@ include file="include/testata_scripts_block.jsp" %>
	</head>
	<body ng-app="<%=appName%>" ng-controller="<%=controller %>" >
		<%@ include file="header-logo.jsp" %>
		<p class="TitoloScheda" align="CENTER"> <%=titoloPagina%> </p>
		<% if(canRead) { %>
			<div class="col-xs-12">
				<div class="col-xs-12 text-center" style="border-top: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; margin-top: 5px; margin-bottom: 5px; padding: 5px;">	
					<jsp:include page="header.jsp"></jsp:include>
				</div>
			</div>
			<div class="container-fluid" >
				<div class="row" id="schedaRiv">
<%-- 					<div ng-app="<%=appName%>" ng-controller="<%=controller %>" > --%>
						<div class="col-xs-12" style="margin-top: 10px;">
							<accordion close-others="false">
								<accordion-group heading="Invaso">
									<%@ include file="sections/rivalutazione/scheda_invaso.jsp" %>
								</accordion-group>
								<accordion-group heading="Portate rivalutate">
									<%@ include file="sections/rivalutazione/portate_rivalutate.jsp" %>
								</accordion-group>
								<accordion-group heading="Motivo della rivalutazione">
									<%@ include file="sections/rivalutazione/date_rivalutazioni.jsp" %>
								</accordion-group>
							</accordion>
						</div>
<!-- 					</div> -->
                        <div class="col-xs-12 text-center">
                            <div class="col-xs-6">
                                <button type="button" class="btn btn-info print-hide" onClick="window.print();">
                                    <span class="glyphicon glyphicon-print"></span>
                                </button>
                            </div>
                            <div class="col-xs-6">
                                <%@ include file="blocks/close.jsp" %>
                            </div>
                        </div>				
                    </div>
			</div>	
		<% } else { %>
			<script type="text/javascript">
				window.close();
			</script>
		<% } %>
	</body>
</html>