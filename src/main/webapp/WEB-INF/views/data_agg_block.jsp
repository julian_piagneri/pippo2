<% 
boolean canReadDataAgg = false;
if(request.getParameter("canReadDataAgg") != null){
	if(request.getParameter("canReadDataAgg").equals("true")) canReadDataAgg = true;
}
%>
	<table style="width: 100%" class="datiUpdate">
		<tr>
			<%if(canReadDataAgg){ %><td ng-model="${param.dato}"><span class="detailsLabel">Aggiornato da:&nbsp;</span>{{${param.dato}.lastModifiedUserId}}</td><% } %>
			<td><span class="detailsLabel">Data	Aggiornamento:&nbsp;</span>{{${param.dato}.lastModifiedDate}}</td>
		</tr>
	</table>
