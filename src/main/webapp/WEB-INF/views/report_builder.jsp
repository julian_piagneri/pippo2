<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String schedaId = "57";
String percorsoMenu = "\\Report\\Report Builder";
String titoloPagina = "Report Builder";
boolean canWrite=false;
boolean canRead=false;
// boolean canWriteAsConcess=false;
// boolean canWriteIng=false;

// boolean canWriteDettagli=false;
// boolean canWriteAnagrafica=false;
%>
<%@ include file="user_permissions.jsp" %>
<%

canWrite=permessiReportBuilder.get("canWrite");
canRead=permessiReportBuilder.get("canRead");
// canWriteAsConcess=permessiRubrica.get("canWriteAsConcess");
// canWriteAsGestore=permessiRubrica.get("canWriteAsGestore");
// canWriteIng=permessiRubrica.get("canWriteIng");

	
	////////////////////
	 //canWriteIng = true;
	 //canWrite = true;
	 // canRead = true;
	////////////////////
//}
String appName="reportBuilderApp";
%>
<%@ include file="header_block.jsp" %>
<% if(canRead) { %>

<div class="container-fluid" style="position: relative">
	<div ng-controller="reportBuilderController" class="row" id="reportField">

        <div class="col-xs-12 text-center">
            <span class="col-xs-12" e-style="width:80%;" editable-text="JSON_fromSource.titolo" 
                  e-form="textTitoloForm" ng-dblclick="textTitoloForm.$show()" style="white-space:pre-line;">
                <h4>{{ JSON_fromSource.titolo }}</h4>
            </span>
        </div>

        <div class="col-xs-12">
            <br/>
        </div>

        <div ng-repeat="tabella in JSON_fromSource.dati" class="col-md-3 col-xs-12 col-sm-6">
            <accordion close-others="false" >
                <accordion-group heading={{tabella.gruppo}} is-open="true" >
                    <div ng-repeat="colonna in tabella.colonne"
                         class="btnBox btn-default btn-dad draggableSogg col-xs-12"
                         style="padding-bottom:5px; 
                         text-align: left;"
                         data-drag="true"
                         data-drop="true"
                         ng-model='dropModel'
                         data-jqyoui-options="{revert: 'invalid'}"
                         jqyoui-draggable="{ animate: true, placeholder: 'keep', onStart:'getDragCampi(colonna)'}"
                         jqyoui-droppable="{ multiple:false, onDrop:'moveCampi(colonna,tabella.colonne,textBtnForm)'}" >
						
                        <!--<div class="col-xs-8">
                            {{colonna.nomenew}}
                        </div>-->

                        <div class="col-xs-6">
                            <!--<a href="#" editable-text="colonna.nomenew">{{ colonna.nomenew }}</a>-->
                            <!--<span class="col-xs-8" editable-text="colonna.nomenew">{{ colonna.nomenew }} </span>-->
                            <span editable-text="colonna.nomenew" e-form="textBtnForm"  ng-dblclick="textBtnForm.$show()">
                                {{ colonna.nomenew }}
                            </span>
                        </div>

                        <div class="col-xs-2" ng-hide="textBtnForm.$visible">
                            <button class="btn btn-primary btn-xs"
                                    type="button"
                                    ng-hide="!colonna.stato || checkOrderRow(colonna,tabella)"
                                    ng-click="aggiungiOrdinamento(tabella, colonna)">
                                <span class="glyphicon glyphicon-sort"></span>
                            </button>
                        </div>

                        <div class="col-xs-4" ng-hide="textBtnForm.$visible" >
                            <input bs-switch switch-size="mini" type="checkbox" ng-checked="colonna.stato" ng-model="colonna.stato"  />
                        </div>
                    </div>
                </accordion-group>
                <accordion-group heading="Ordinamento" is-open="true">
                    <div ng-repeat="posizione in tabella.ordinamento"
                         class="btnBox btn-default btn-dad draggableSogg col-xs-12"
                         style="padding-bottom:5px;
                         text-align: left;"
                         data-drag="true"
                         data-drop="true"
                         ng-model='dropModel'
                         data-jqyoui-options="{revert: 'invalid'}"
                         jqyoui-draggable="{ animate: true, placeholder: 'keep', onStart:'getDragCampi(posizione)'}"
                         jqyoui-droppable="{ multiple:false, onDrop:'moveCampi(posizione,tabella.ordinamento,textBtnForm)'}"
                         ng-show="checkVisibleRow(posizione,tabella)"
                         >

                        <div class="col-xs-2">
                            <button class="btn btn-primary btn-xs" type="button" ng-click="cambiaVersoOrdinamento(posizione)">
                                <span ng-class="posizione.verso=='asc'? 'glyphicon glyphicon-sort-by-attributes' : 'glyphicon glyphicon-sort-by-attributes-alt' "></span>
                                <!--<span ng-class="glyphicon glyphicon-150"></span>-->
                            </button>
                        </div>
						
                        <div class="col-xs-8">
                            {{getnomenew(tabella, posizione.nome)}}
                        </div>

                        <div class="col-xs-2">
                            <button class="btn btn-danger btn-xs" type="button" ng-click="removeOrderRow(posizione,tabella)">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </div>
                    </div>
                </accordion-group>
            </accordion>
            <br />
        </div>

        <div class="col-xs-12">
            <br />
        </div>

        <div class="col-md-6 col-xs-12 col-sm-6 text-center">
            <button type="button" class="btn btn-info" ng-click="generaJSON()">
                Genera
            </button>
            <br />
            <br />
        </div>

        <div class="col-md-6 col-xs-12 col-sm-6 text-center">
            <button type="button" class="btn btn-danger" onClick="window.close();">
                <span class="glyphicons-reg glyphicon-389"></span>
            </button>
        </div>

        <!--<div>
        <%@ include file="blocks/close.jsp" %>
    </div>-->
    </div>
</div>

	<script type="text/javascript">
		G_token = "<%=tokenREST%>";
		G_userID = "<%=userId%>";
		G_damID = '<%=request.getParameter("dam")%>'
	</script>
	<script type="text/ng-template" id="addIngTemplate"><%@ include file="popup_add_ing.jsp" %></script>
	
	<%@ include file="js_user_permissions.jsp" %>
	<%@ include file="scripts_block.jsp" %>

	<script type="text/javascript" src="resources/scripts/bootstrap/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="resources/scripts/lib/angular-bootstrap-switch.js"></script>
	<script type="text/javascript" src="resources/scripts/reportBuilderInit.js"></script>
	<script type="text/javascript" src="resources/scripts/restEngine.js"></script>
	<script type="text/javascript" src="resources/scripts/commonScope.js"></script>
	<script type="text/javascript" src="resources/scripts/services/helpersService.js"></script>
	<script type="text/javascript" src="resources/scripts/services/dialogService.js"></script>
	<script type="text/javascript" src="resources/scripts/reportBuilderApp.js"></script>
    <script type="text/javascript" src="resources/scripts/lib/routes.js"></script>
<% } else{ %>
	<script type="text/javascript">
		window.close();
	</script>
<% } %>
	</body>
</HTML>
