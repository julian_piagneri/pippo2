<!-- <div ng-app="TestataApp"> -->

<!-- 		<div ng-controller="TestataController"> -->
			<div class="col-xs-12 col-md-12">
				<!-- prima riga -->
				<div class="col-xs-12 col-md-2">
					<div class="col-xs-8 col-md-8 text-left detailsLabel">N� Archivio</div>
					<div class="col-xs-4 col-md-4 text-left">
						<span>{{ dati.numeroArchivio }}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-1">
					<div class="col-xs-6 col-md-6 text-left detailsLabel">Sub</div>
					<div class="col-xs-6 col-md-6 text-left">
						<span>{{ dati.sub }}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="col-xs-8 col-md-8 text-left detailsLabel">N� Iscrizione RID</div>
					<div class="col-xs-4 col-md-4 text-left">
						<span>{{ dati.numeroRid }}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="col-xs-6 col-md-6 text-left detailsLabel">Nome Diga</div>
					<div class="col-xs-6 col-md-6 text-left">
						<span>{{ dati.nomeDiga }}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="col-xs-6 col-md-6 text-left detailsLabel">Regione</div>
					<div class="col-xs-6 col-md-6 text-left">
						<span>{{ dati.nomeRegione }}</span>
					</div>
				</div>
			</div>
			<div class="col-xs-12">&nbsp;</div>
			<div class="col-xs-12 col-md-12">
				<!-- seconda riga -->
				<div class="col-xs-12 col-md-9">
					<div class="col-xs-2 col-md-2 text-left detailsLabel">Status</div>
					<div class="col-xs-10 col-md-10 text-left">
						<span>{{ dati.status }}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="col-xs-12">
						<div class="col-xs-8 col-md-8 text-left detailsLabel">Uff. Coor.</div>
						<div class="col-xs-4 col-md-4 text-left">
							<span>{{ dati.uffCoord.id }}</span>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="col-xs-8 col-md-8 text-left detailsLabel">Uff. Per.</div>
						<div class="col-xs-4 col-md-4 text-left">
							<span>{{ dati.uffPerif.id }}</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12">&nbsp;</div>
			<div class="col-xs-12 col-md-12">
				<!-- terza riga -->
				<div class="col-xs-12 col-md-6">
					<div class="col-xs-6 col-md-6 text-left detailsLabel">Funzion. sede
						centrale</div>
					<div class="col-xs-6 col-md-6 text-left">
						<span> {{ dati.uffCoord.funzionario.cognome + " " +
							dati.uffCoord.funzionario.nome }} </span>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="col-xs-6 col-md-6 text-left text-lowercase detailsLabel">
						Funzion. uff. periferico
					</div>
					<div class="col-xs-6 col-md-6 text-left ">
						<span> {{ dati.uffPerif.funzionario.cognome + " " +
							dati.uffPerif.funzionario.nome }} </span>
					</div>
				</div>
			</div>
<!-- 		</div> -->
<!-- </div> -->