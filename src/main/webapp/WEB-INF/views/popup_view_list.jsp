

<p class="TitoloScheda" align="CENTER"> {{assocTitolo}} </p>
<br />

<span class="h5">{{assocsTitolo1}}</span>
<table ng-table="tableAssocTo" show-filter="true" class="table">
	<tr>
		<td filter="{ 'displayName': 'text' }"></td>
	</tr>
	<tr ng-repeat="to in $data">
		<td>
			{{to.displayName}}
		</td>
	</tr>
</table>
