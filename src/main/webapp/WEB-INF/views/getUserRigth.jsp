<%@ page import="java.util.*,java.io.*"%>
<%@ page import="it.thematica.enterprise.app.login.*"%>
<%!
private static class EduReader { 
	public static RightNode getRigth(Object[] sessionObject, String idScheda) {
		char result[] = null;
		RightNode rn = null;
		UserRightsEntry edu = null;
		
		if(sessionObject != null){
			ByteArrayInputStream baIn = new ByteArrayInputStream((byte[])sessionObject[2]);
			ObjectInput objIn = null;
			try {
			  objIn = new ObjectInputStream(baIn);
			  
			  edu = (UserRightsEntry) objIn.readObject(); 

			} catch (Exception ex) {
				// ignore close exception
			} finally {
			  try {
				baIn.close();
			  } catch (IOException ex) {
				// ignore close exception
			  }
			  try {
				if (objIn != null) {
				  objIn.close();
				}
			  } catch (IOException ex) {
				// ignore close exception
			  }
			}
			
			if (edu.getNodes() != null)
			{
			  Object[] keys = edu.getNodes().keySet().toArray();
			  for (int i = 0; i < keys.length; i++) {
				if(keys[i].toString().equals(idScheda)){
					rn = (RightNode)edu.getNodes().get(keys[i]);
					// result[0] = ((RightNode)edu.getNodes().get(keys[i])).getType();
					// result[1] = ((RightNode)edu.getNodes().get(keys[i])).getVisible();
				}
			  }
			}
				
			
		}

		return rn;
	}
}

%>