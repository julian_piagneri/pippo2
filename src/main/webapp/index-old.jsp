<%@page import="java.util.ArrayList"%>
<%@page import="it.cinqueemmeinfo.dammap.dao.UserService"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="damMapApp">
	<head>
		<title>Home</title>
		<script type="text/javascript" src="resources/angularjs/v1.2.26/angular.min.js"></script>
		<script type="text/javascript" src="resources/scripts/app.js"></script>
	</head>
	<body>
		<div ng-controller="homeController">
			<h1>
				Hello world!  
			</h1>
			
			<P>  The time on the server is {{serverTime}}. </P>
			
			<p>  This server base url is {{baseUrl}} </p> 
			
			<!--  p>  To browse a sample map browser <button confirmed-click="goToMapViewer()" ng-confirm-click="are you sure you want to leave this page?">click here</button>  -->
			
			<%
				ServletContext sc = getServletContext(); 

				//"acdd1ff7-92c9-416f-878a-7eea251c835c"
				HashMap urMap = new HashMap();
				urMap.put("DCHIAROLLA", new Object[]{0, "49ef313b-6da5-4dc1-8c8f-880b8acff500", null});
				sc.setAttribute("urMap", urMap);
				

				/*ApplicationContext ac = RequestContextUtils.getWebApplicationContext(request);
				UserService userService = (UserService)ac.getBean("userService");*/
			%>

			<a href="http://localhost:8080/dammap/api/anagrafica/autorita?user=DCHIAROLLA&signature=12345">go here</a>

		</div>
	</body>
</html>