package it.cinqueemmeinfo.dammap.dao;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiAltroBean;
import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiGruppoBean;
import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiSchedaBean;
import it.cinqueemmeinfo.dammap.bean.fields.IdFenBean;
import it.cinqueemmeinfo.dammap.dao.entities.FenomeniFranosiDao;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;
import it.cinqueemmeinfo.dammap.utility.exceptions.NextValException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class SchedaFenomeniFranosiService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(SchedaFenomeniFranosiService.class);

	@Autowired
	private FenomeniFranosiDao ffDao;
	@Autowired
	private UserUtility usrUtil;

	public List<FenomeniFranosiSchedaBean> getFenomeniFranosiSchedeByDamID(String damID) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.FENOMENI_FRANOSI);
		
		return ffDao.getFenomeniFranosiSchedaByDamId(damID);
	}

	public FenomeniFranosiAltroBean getFenomeniFranosiAltroByIdFen(long id) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.FENOMENI_FRANOSI);
		
		return ffDao.getFenomeniFranosiAltroByIdFen(id);
	}

	public void updateFenomeniFranosiAltro(FenomeniFranosiAltroBean bean, long idScheda) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanWrite(DBUtility.FENOMENI_FRANOSI);
		ffDao.updateFenomeniFranosiAltro(bean, usrUtil.getUserId(), idScheda);
	}

	public void updateFenomeniFranosiGruppoByDamID(FenomeniFranosiGruppoBean gruppo, long idScheda) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanWrite(DBUtility.FENOMENI_FRANOSI);
		ffDao.updateFenomeniFranosiGruppoByDamID(gruppo, idScheda, usrUtil.getUserId());
	}

	public void updateFenomeniFranosiBySchedaID(FenomeniFranosiSchedaBean frana, long idScheda) throws SQLException, EntityNotFoundException {
		ffDao.updateFenomeniFranosiGruppoBySchedaID(frana, idScheda, usrUtil.getUserId());
	}

	public void deleteFenomeniFranosiGruppoByDamID(long idScheda) throws EntityNotFoundException, SQLException {
		usrUtil.checkCanWrite(DBUtility.FENOMENI_FRANOSI);
		ffDao.deleteFenomeniFranosiGruppoByDamID(idScheda);
	}

	public IdFenBean insertFenomeniFranosiGruppoByDamID(String damID, FenomeniFranosiSchedaBean scheda) throws SQLException, NextValException {
		usrUtil.checkCanWrite(DBUtility.FENOMENI_FRANOSI);
		return ffDao.insertFenomeniFranosi(damID, scheda, usrUtil.getUserId());
	}
}
