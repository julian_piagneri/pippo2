package it.cinqueemmeinfo.dammap.dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AltreVociRubricaBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.CasaDiGuardiaBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.ContactBean;
import it.cinqueemmeinfo.dammap.bean.entities.DamBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.junction.NominaIngegnereBean;
import it.cinqueemmeinfo.dammap.dao.entities.DamDao;
import it.cinqueemmeinfo.dammap.dao.entities.IngegnereDao;
import it.cinqueemmeinfo.dammap.dao.macro.ContactsDao;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

public class ContactsService implements Serializable {

	private static final long serialVersionUID = -2927571924602882936L;
	private static final Logger logger = LoggerFactory.getLogger(ContactsService.class);

	// @Autowired private DBUtility dbUtil;
	@Autowired
	private UserUtility usrUtil;
	@Autowired
	private DamDao dmDao;
	@Autowired
	private ContactsDao ctDao;
	@Autowired
	private IngegnereDao inDao;
	@Autowired
	private UserService userService;
	private boolean skipCheckScheda = true;

	// ******************* RUBRICA *************************

	public ContactBean getContactFromDamId(String damId) throws SQLException {

		ContactBean coBean = null;

		Boolean canReadAsConcess = false;
		Boolean canReadAsGestore = false;

		canReadAsConcess = userService.checkDamWriteRigthsForConcessionario(damId);
		canReadAsGestore = userService.checkDamWriteRigthsForGestore(damId);

		int hasAccess = usrUtil.getMaxPermissionForScheda(UserUtility.IDS_RUBRICA);

		if (usrUtil.canReadFromScheda(DBUtility.RUBRICA) || usrUtil.canReadFromScheda(DBUtility.RUBRICA_ESTERNI)
				|| canReadAsConcess || canReadAsGestore) {

			coBean = ctDao.getContactFromDamId(damId, hasAccess);

		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		//// int hasAccess =
		//// usrUtil.getMaxPermissionForScheda(UserUtility.IDS_RUBRICA);

		// try {
		// hasAccess = skipCheckScheda ||
		// usrUtil.canReadFromScheda(DBUtility.RUBRICA);
		// } catch (ExternalUserException e) {
		// hasAccess = skipCheckScheda ||
		// usrUtil.canReadFromScheda(DBUtility.RUBRICA_ESTERNI);
		// }

		//// if(hasAccess>=1) {
		//// coBean = ctDao.getContactFromDamId(damId, hasAccess);
		//// }

		return coBean;
	}

	public CasaDiGuardiaBean updateCasaDiGuardia(String damId, CasaDiGuardiaBean cgBean) throws SQLException {

		CasaDiGuardiaBean updatedCgBean = null;
		Boolean canWriteAsConcess = false;
		Boolean canWriteAsGestore = false;

		canWriteAsConcess = userService.checkDamWriteRigthsForConcessionario(damId);
		canWriteAsGestore = userService.checkDamWriteRigthsForGestore(damId);

		// skipCheckScheda ||
		if (usrUtil.canWriteIntoScheda(DBUtility.RUBRICA) || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA_ESTERNI)
				|| canWriteAsConcess || canWriteAsGestore) {
			updatedCgBean = ctDao.updateCasaDiGuardia(damId, cgBean, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return updatedCgBean;
	}

	public AltreVociRubricaBean createAltraVoceRubrica(String damId, AltreVociRubricaBean avBean)
			throws SQLException, EntityNotFoundException {

		AltreVociRubricaBean createdAvBean = null;

		if (skipCheckScheda || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA)) {
			createdAvBean = ctDao.createAltraVoceRubrica(damId, avBean, usrUtil.getUserId(), true);
		}

		return createdAvBean;
	}

	public AltreVociRubricaBean updateAltraVoceRubrica(String damId, AltreVociRubricaBean avBean)
			throws SQLException, EntityNotFoundException {

		AltreVociRubricaBean updatedAvBean = null;

		if (skipCheckScheda || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA)) {
			updatedAvBean = ctDao.updateAltraVoceRubrica(damId, avBean, usrUtil.getUserId(), true);
		}

		return updatedAvBean;
	}

	public Boolean removeAltraVoceRubrica(String damId, int progressivo) throws EntityNotFoundException, SQLException {

		Boolean isRemoved = false;

		if (skipCheckScheda || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA)) {
			isRemoved = ctDao.removeAltraVoceRubrica(damId, progressivo, usrUtil.getUserId(), true);
		}

		return isRemoved;
	}

	// ******************* DAM *************************

	public List<DamBean> getDamListWithSameIdInvaso(String damId) throws SQLException {

		List<DamBean> damList = null;

		Boolean canReadAsConcess = false;
		Boolean canReadAsGestore = false;

		canReadAsConcess = userService.checkDamWriteRigthsForConcessionario(damId);
		canReadAsGestore = userService.checkDamWriteRigthsForGestore(damId);

		int hasAccess = usrUtil.getMaxPermissionForScheda(UserUtility.IDS_RUBRICA);

		if (usrUtil.canReadFromScheda(DBUtility.RUBRICA) || usrUtil.canReadFromScheda(DBUtility.RUBRICA_ESTERNI)
				|| canReadAsConcess || canReadAsGestore) {
			damList = dmDao.getDamListWithSameIdInvaso(damId, usrUtil.getUserId(), hasAccess);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		// Boolean hasAccess = false;
		//
		// try {
		// hasAccess = skipCheckScheda ||
		// usrUtil.canReadFromScheda(DBUtility.RUBRICA);
		// } catch (ExternalUserException e) {
		// hasAccess = skipCheckScheda ||
		// usrUtil.canReadFromScheda(DBUtility.RUBRICA_ESTERNI);
		// }
		//
		// if (hasAccess) {
		// damList = dmDao.getDamListWithSameIdInvaso(damId,
		// usrUtil.getUserId());
		// }
		//// int hasAccess =
		// usrUtil.getMaxPermissionForScheda(UserUtility.IDS_RUBRICA);

		//// if(hasAccess>=1) {
		//// damList = dmDao.getDamListWithSameIdInvaso(damId,
		//// usrUtil.getUserId(), hasAccess);
		//// }

		return damList;
	}

	// ******************* AUTORITA PROTEZIONE CIVILE *************************

	public Boolean addAutoritaProtezioneCivileToDam(String damId, String entityId)
			throws SQLException, EntityNotFoundException {

		Boolean isCreated = false;
		// skipCheckScheda ||
		if (usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isCreated = dmDao.addEntityToDam(damId, new AutoritaProtezioneCivileBean(entityId), usrUtil.getUserId(),
					Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isCreated;
	}

	public Boolean removeAutoritaProtezioneCivileFromDam(String damId, String entityId)
			throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		// skipCheckScheda ||
		if (usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = dmDao.removeEntityFromDam(damId, new AutoritaProtezioneCivileBean(entityId),
					usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isRemoved;
	}

	// ******************* CONCESSIONARIO *************************

	public Boolean addConcessionarioToDam(String damId, String entityId) throws SQLException, EntityNotFoundException {

		Boolean isCreated = false;
		// skipCheckScheda ||
		if (usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isCreated = dmDao.addEntityToDam(damId, new ConcessionarioBean(entityId), usrUtil.getUserId(),
					Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isCreated;
	}

	public Boolean removeConcessionarioFromDam(String damId, String entityId)
			throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		// skipCheckScheda ||
		if (usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = dmDao.removeEntityFromDam(damId, new ConcessionarioBean(entityId), usrUtil.getUserId(),
					Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isRemoved;
	}

	// ******************* GESTORE *************************

	public Boolean addGestoreToDam(String damId, String entityId) throws SQLException, EntityNotFoundException {

		Boolean isCreated = false;
		// skipCheckScheda ||
		if (usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isCreated = dmDao.addEntityToDam(damId, new GestoreBean(entityId), usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isCreated;
	}

	public Boolean removeGestoreFromDam(String damId, String entityId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		// skipCheckScheda ||
		if (usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = dmDao.removeEntityFromDam(damId, new GestoreBean(entityId), usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isRemoved;
	}

	// ******************* INGEGNERE *************************

	public NominaIngegnereBean addIngegnereAsReponsabileToDam(String damId, NominaIngegnereBean inBean)
			throws Exception {

		NominaIngegnereBean createdNmBean = null;

		Boolean canWriteAsConcess = false;
		Boolean canWriteAsGestore = false;
		Boolean canWriteAsIng = false;
		String ingId = inBean.getId();

		canWriteAsConcess = userService.checkDamWriteRigthsForConcessionario(damId);
		canWriteAsGestore = userService.checkDamWriteRigthsForGestore(damId);
		canWriteAsIng = userService.checkRigthsForIngegnere(ingId);

		// if (skipCheckScheda ||
		// usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
		if ((usrUtil.canWriteIntoScheda(DBUtility.RUBRICA) || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA_ESTERNI)
				|| canWriteAsConcess || canWriteAsGestore) && (canWriteAsIng)) {
			createdNmBean = inDao.setIngegnereAsResponsabileToDam(damId, inBean, usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return createdNmBean;
	}

	public NominaIngegnereBean updateNominaIngegnereResponsabile(NominaIngegnereBean inBean) throws Exception {

		NominaIngegnereBean updatedNmBean = null;
		String damId = "";
		Boolean canWriteAsConcess = false;
		Boolean canWriteAsGestore = false;
		Boolean canWriteAsIng = false;
		String ingId = inBean.getId();

		damId = inBean.getNumeroArchivio().toString();
		damId = damId.concat(inBean.getSub());

		canWriteAsConcess = userService.checkDamWriteRigthsForConcessionario(damId);
		canWriteAsGestore = userService.checkDamWriteRigthsForGestore(damId);
		canWriteAsIng = userService.checkRigthsForIngegnere(ingId);

		// if (skipCheckScheda ||
		// usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
		if ((usrUtil.canWriteIntoScheda(DBUtility.RUBRICA) || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA_ESTERNI)
				|| canWriteAsConcess || canWriteAsGestore) && (canWriteAsIng)) {
			updatedNmBean = inDao.updateNominaIngegnereResponsabile(inBean, usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return updatedNmBean;
	}

	public Boolean removeIngegnereResponsabileFromDam(String damId, String ingId)
			throws EntityNotFoundException, SQLException {

		Boolean isRemoved = false;

		Boolean canWriteAsConcess = false;
		Boolean canWriteAsGestore = false;
		Boolean canWriteAsIng = false;

		canWriteAsConcess = userService.checkDamWriteRigthsForConcessionario(damId);
		canWriteAsGestore = userService.checkDamWriteRigthsForGestore(damId);
		canWriteAsIng = userService.checkRigthsForIngegnere(ingId);

		// if (skipCheckScheda ||
		// usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
		if ((usrUtil.canWriteIntoScheda(DBUtility.RUBRICA) || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA_ESTERNI)
				|| canWriteAsConcess || canWriteAsGestore) && (canWriteAsIng)) {
			isRemoved = inDao.removeIngegnereResponsabileFromDam(damId, ingId, usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isRemoved;
	}

	public NominaIngegnereBean addIngegnereAsSostitutoToDam(String damId, NominaIngegnereBean inBean) throws Exception {

		NominaIngegnereBean createdNmBean = null;

		Boolean canWriteAsConcess = false;
		Boolean canWriteAsGestore = false;
		Boolean canWriteAsIng = false;
		String ingId = inBean.getId();

		canWriteAsConcess = userService.checkDamWriteRigthsForConcessionario(damId);
		canWriteAsGestore = userService.checkDamWriteRigthsForGestore(damId);
		canWriteAsIng = userService.checkRigthsForIngegnere(ingId);

		// if (skipCheckScheda ||
		// usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
		if ((usrUtil.canWriteIntoScheda(DBUtility.RUBRICA) || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA_ESTERNI)
				|| canWriteAsConcess || canWriteAsGestore) && (canWriteAsIng)) {
			createdNmBean = inDao.setIngegnereAsSostitutoToDam(damId, inBean, usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return createdNmBean;
	}

	public NominaIngegnereBean updateNominaIngegnereSostituto(NominaIngegnereBean inBean) throws Exception {

		NominaIngegnereBean updatedNmBean = null;

		String damId = "";
		Boolean canWriteAsConcess = false;
		Boolean canWriteAsGestore = false;
		Boolean canWriteAsIng = false;
		String ingId = inBean.getId();

		damId = inBean.getNumeroArchivio().toString();
		damId = damId.concat(inBean.getSub());

		canWriteAsConcess = userService.checkDamWriteRigthsForConcessionario(damId);
		canWriteAsGestore = userService.checkDamWriteRigthsForGestore(damId);
		canWriteAsIng = userService.checkRigthsForIngegnere(ingId);

		// if (skipCheckScheda ||
		// usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
		if ((usrUtil.canWriteIntoScheda(DBUtility.RUBRICA) || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA_ESTERNI)
				|| canWriteAsConcess || canWriteAsGestore) && (canWriteAsIng)) {
			updatedNmBean = inDao.updateNominaIngegnereSostituto(inBean, usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return updatedNmBean;
	}

	public Boolean removeIngegnereSostitutoFromDam(String damId, String ingId)
			throws EntityNotFoundException, SQLException {

		Boolean isRemoved = false;

		Boolean canWriteAsConcess = false;
		Boolean canWriteAsGestore = false;
		Boolean canWriteAsIng = false;

		canWriteAsConcess = userService.checkDamWriteRigthsForConcessionario(damId);
		canWriteAsGestore = userService.checkDamWriteRigthsForGestore(damId);
		canWriteAsIng = userService.checkRigthsForIngegnere(ingId);

		// if (skipCheckScheda ||
		// usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
		if ((usrUtil.canWriteIntoScheda(DBUtility.RUBRICA) || usrUtil.canWriteIntoScheda(DBUtility.RUBRICA_ESTERNI)
				|| canWriteAsConcess || canWriteAsGestore) && (canWriteAsIng)) {
			isRemoved = inDao.removeIngegnereSostitutoFromDam(damId, ingId, usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isRemoved;
	}

	// ******************* ALTRA ENTE *************************

	public Boolean addAltraEnteToDam(String damId, String entityId) throws SQLException, EntityNotFoundException {

		Boolean isCreated = false;
		// skipCheckScheda ||
		if (usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isCreated = dmDao.addEntityToDam(damId, new AltraEnteBean(entityId), usrUtil.getUserId(), Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isCreated;
	}

	public Boolean removeAltraEnteFromDam(String damId, String entityId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		// skipCheckScheda ||
		if (usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = dmDao.removeEntityFromDam(damId, new AltraEnteBean(entityId), usrUtil.getUserId(),
					Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isRemoved;
	}

}
