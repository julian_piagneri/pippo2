package it.cinqueemmeinfo.dammap.dao;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.TestataBean;
import it.cinqueemmeinfo.dammap.dao.entities.TestataDao;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class TestataService {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(TestataService.class);
	
	@Autowired
	private TestataDao testataDao;

	public TestataBean getTestataByDamID(String damID) throws SQLException, EntityNotFoundException {
		return testataDao.getTestataByDamID(damID);
	}
}
