package it.cinqueemmeinfo.dammap.dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.DamBean;
import it.cinqueemmeinfo.dammap.bean.entities.EntityTypeBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.entities.IngegnereBean;
import it.cinqueemmeinfo.dammap.bean.entities.ProvinciaBean;
import it.cinqueemmeinfo.dammap.bean.entities.UserBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;
import it.cinqueemmeinfo.dammap.dao.entities.AltraEnteDao;
import it.cinqueemmeinfo.dammap.dao.entities.AutoritaProtezioneCivileDao;
import it.cinqueemmeinfo.dammap.dao.entities.ConcessionarioDao;
import it.cinqueemmeinfo.dammap.dao.entities.EntityTypeDao;
import it.cinqueemmeinfo.dammap.dao.entities.GestoreDao;
import it.cinqueemmeinfo.dammap.dao.entities.IngegnereDao;
import it.cinqueemmeinfo.dammap.dao.fields.AddressDao;
import it.cinqueemmeinfo.dammap.dao.fields.ContactInfoDao;
import it.cinqueemmeinfo.dammap.dao.macro.RegistryDao;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;
import it.cinqueemmeinfo.dammap.utility.exceptions.JunctionAlreadyExistsException;

public class RegistryService implements Serializable {

	private static final long serialVersionUID = 2949300263070475359L;
	private static final Logger logger = LoggerFactory.getLogger(RegistryService.class);
	
	//private final static Integer ID_ANAGRAFICA = 59;
	
	//@Autowired private DBUtility dbUtil;
	@Autowired private UserUtility usrUtil;
	@Autowired private AutoritaProtezioneCivileDao apcDao;
	@Autowired private ConcessionarioDao cnDao;
	@Autowired private GestoreDao gsDao;
	@Autowired private IngegnereDao inDao;
	@Autowired private AltraEnteDao aeDao;
	@Autowired private ContactInfoDao ciDao;
	@Autowired private AddressDao adDao;
	@Autowired private EntityTypeDao etDao;
	@Autowired private RegistryDao rgDao;
	//@Autowired private DamDao dmDao;
	@Autowired private UserService userService;
	
	private boolean skipCheckScheda=true;

	//******************* AUTORITA PROTEZIONE CIVILE *************************
	
	/**
	 * Creates a new Autorita Protezione Civile in the database
	 * @param apcBean
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException 
	 */
	public AutoritaProtezioneCivileBean createAutoritaProtezioneCivile(AutoritaProtezioneCivileBean apcBean) throws SQLException, EntityNotFoundException {
		
		AutoritaProtezioneCivileBean createdApcBean = null;
		
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			createdApcBean = apcDao.createAutoritaProtezioneCivile(apcBean, usrUtil.getUserId(), true);
		
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return createdApcBean;
	}
	
	public Boolean createAutoritaProtezioneCivileToUserJunction(String entityId, String targetUserId) throws SQLException, EntityNotFoundException, JunctionAlreadyExistsException {

		Boolean isCreated = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isCreated = rgDao.createEntityToUserJunction(new AutoritaProtezioneCivileBean(entityId), targetUserId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isCreated;
	}
	
	public Boolean removeAutoritaProtezioneCivileToUserJunction(String entityId, String targetUserId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = rgDao.removeEntityToUserJunction(new AutoritaProtezioneCivileBean(entityId), targetUserId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isRemoved;
	}

	/**
	 * Updates an Autorita Protezione Civile entity in the database
	 * @param apcBean
	 * @return
	 * @throws Exception 
	 */
	public AutoritaProtezioneCivileBean updateAutoritaProtezioneCivile(AutoritaProtezioneCivileBean apcBean) throws Exception {

		AutoritaProtezioneCivileBean updatedApcBean = null;
		
		//skipCheckScheda ||
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			updatedApcBean = apcDao.updateAutoritaProtezioneCivile(apcBean, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return updatedApcBean;
	}

	/**
	 * 
	 * @param entityId
	 * @return
	 * @throws SQLException
	 */
	public Boolean removeAutoritaProtezioneCivile(String entityId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = apcDao.removeAutoritaProtezioneCivile(entityId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isRemoved;
	}
	
	/**
	 * 
	 * @param apcBean
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException 
	 */
	public AutoritaProtezioneCivileBean getAutoritaProtezioneCivileWithParams(AutoritaProtezioneCivileBean apcBean) throws SQLException, EntityNotFoundException {

		AutoritaProtezioneCivileBean foundApcBean = null;
		
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			foundApcBean = apcDao.getAutoritaProtezioneCivileWithParams(hasAccess, userId, apcBean, true);
		}else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
					
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			 foundApcBean = apcDao.getAutoritaProtezioneCivileWithParams(apcBean, true);	
//		}
		
		return foundApcBean;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<AutoritaProtezioneCivileBean> getAutoritaProtezioneCivileList() throws SQLException {

		List<AutoritaProtezioneCivileBean> apcList = null;
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			apcList = apcDao.getAutoritaProtezioneCivileListWithParams(new AutoritaProtezioneCivileBean(), false, true);
//		}
		
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			apcList = apcDao.getAutoritaProtezioneCivileListWithParams(hasAccess, userId, new AutoritaProtezioneCivileBean(), false, true);
		}else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return apcList;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<AutoritaProtezioneCivileBean> getAutoritaProtezioneCivileListWithParams(AutoritaProtezioneCivileBean apcBean) throws SQLException {

		List<AutoritaProtezioneCivileBean> apcList = null;
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			apcList = apcDao.getAutoritaProtezioneCivileListWithParams(hasAccess, userId, apcBean, false, true);
		}
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			apcList = apcDao.getAutoritaProtezioneCivileListWithParams(apcBean, false, true);
//		}
		
		return apcList;
	}
	
	public ArrayList<DamBean> getDamListFromAutoritaProtezioneCivileId(String entityId) throws SQLException {
		
		ArrayList<DamBean> damList = null;
		
		Boolean checkEntityRead=false;
		ArrayList<String> entityRead = null;
		
		entityRead = userService.getUserRightsForEntity(new AutoritaProtezioneCivileBean());
		checkEntityRead = usrUtil.canRightsForEntity(entityRead, entityId);
		
		if(usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA) || checkEntityRead) {
			damList = rgDao.getDamListFromEntityId(new AutoritaProtezioneCivileBean(entityId), true);
		}else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}			
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			damList = rgDao.getDamListFromEntityId(new AutoritaProtezioneCivileBean(entityId), true);
//		}
		
		return damList;
	}
	
	public ArrayList<UserBean> getUserListFromAutoritaProtezioneCivileId(String entityId) throws SQLException {
		
		ArrayList<UserBean> usrList = null;
		
		Boolean checkEntityRead=false;
		ArrayList<String> entityRead = null;
		
		entityRead = userService.getUserRightsForEntity(new AutoritaProtezioneCivileBean());
		checkEntityRead = usrUtil.canRightsForEntity(entityRead, entityId);
		
		if(usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA) || checkEntityRead) {
			usrList = rgDao.getUserListFromEntityId(new AutoritaProtezioneCivileBean(entityId), "", true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}	
		
		//String userId = usrUtil.getUserId();
		//int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		//if(hasAccess==1){
		//	usrList = rgDao.getUserListFromEntityId(new AutoritaProtezioneCivileBean(entityId), userId, true);
		//} else {
			////usrList = rgDao.getUserListFromEntityId(new AutoritaProtezioneCivileBean(entityId), "", true);
		//}
		
		return usrList;
	}

	//******************* CONCESSIONARIO *************************

	public ConcessionarioBean getConcessionarioWithParams(ConcessionarioBean cnBean) throws SQLException, EntityNotFoundException {

		ConcessionarioBean foundCnBean = null;
		
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			foundCnBean = cnDao.getConcessionarioWithParams(hasAccess,userId,cnBean, true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			foundCnBean = cnDao.getConcessionarioWithParams(cnBean, true);
//		}
		
		return foundCnBean;
	}
	
	public List<ConcessionarioBean> getConcessionarioList() throws SQLException {

		List<ConcessionarioBean> cnList = null;
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			cnList = cnDao.getConcessionarioListWithParams("", new ConcessionarioBean(), false, true);
//		}
		
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			cnList = cnDao.getConcessionarioListWithParams(hasAccess, userId, "", new ConcessionarioBean(), false, true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		
		return cnList;
	}

	public List<ConcessionarioBean> getConcessionarioListWithParams(ConcessionarioBean cnBean) throws SQLException {

		List<ConcessionarioBean> cnList = null;
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			cnList = cnDao.getConcessionarioListWithParams("", cnBean, false, true);
//		}
		
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			cnList = cnDao.getConcessionarioListWithParams(hasAccess, userId, "", cnBean, false, true);
		}
		
		return cnList;
	}
	
	public ConcessionarioBean createConcessionario(ConcessionarioBean cnBean) throws SQLException, EntityNotFoundException {
		
		ConcessionarioBean createdCnBean = null;

		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			createdCnBean = cnDao.createConcessionario(cnBean, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return createdCnBean;
	}

	public ConcessionarioBean updateConcessionario(ConcessionarioBean cnBean) throws Exception {

		ConcessionarioBean updatedCnBean = null;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			updatedCnBean = cnDao.updateConcessionario(cnBean, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return updatedCnBean;
	}

	public Boolean removeConcessionario(String entityId) throws SQLException, EntityNotFoundException {

		Boolean isDeleted = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isDeleted = cnDao.removeConcessionario(entityId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isDeleted;
	}

	public List<ConcessionarioBean> getConcessionarioListFromIdGestore(String idGestore) throws SQLException {

		List<ConcessionarioBean> cnList = null; 
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			cnList = cnDao.getConcessionarioListWithParams(idGestore, new ConcessionarioBean(), false, true);	
//		}
		
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			cnList = cnDao.getConcessionarioListWithParams(hasAccess, userId, idGestore, new ConcessionarioBean(), false, true);
		} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return cnList;
	}
	
	public Boolean createConcessionarioGestoreJunction(int concessId, int gestoreId) throws SQLException, JunctionAlreadyExistsException {

		Boolean isCreated = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isCreated = cnDao.createConcessionarioGestoreJunction(concessId, gestoreId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isCreated;
	}
	
	public Boolean removeConcessionarioGestoreJunction(int concessId, int gestoreId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = cnDao.removeConcessionarioGestoreJunction(concessId, gestoreId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isRemoved;
	}

	public Boolean createConcessionarioToUserJunction(String entityId, String targetUserId) throws SQLException, EntityNotFoundException, JunctionAlreadyExistsException {

		Boolean isCreated = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isCreated = rgDao.createEntityToUserJunction(new ConcessionarioBean(entityId), targetUserId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isCreated;
	}
	
	public Boolean removeConcessionarioToUserJunction(String entityId, String targetUserId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = rgDao.removeEntityToUserJunction(new ConcessionarioBean(entityId), targetUserId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isRemoved;
	}

	public ArrayList<DamBean> getDamListFromConcessionarioId(String entityId) throws SQLException {
		
		ArrayList<DamBean> damList = null;
		
		Boolean checkEntityRead=false;
		ArrayList<String> entityRead = null;
		
		entityRead = userService.getUserRightsForEntity(new ConcessionarioBean());
		checkEntityRead = usrUtil.canRightsForEntity(entityRead, entityId);
		
		if(usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)  || checkEntityRead) {
			damList = rgDao.getDamListFromEntityId(new ConcessionarioBean(entityId), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}	
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			damList = rgDao.getDamListFromEntityId(new ConcessionarioBean(entityId), true);
//		}
		
		return damList;
	}
	
	public ArrayList<UserBean> getUserListFromConcessionarioId(String entityId) throws SQLException {

		ArrayList<UserBean> usrList = null;
		
		Boolean checkEntityRead=false;
		ArrayList<String> entityRead = null;
		
		entityRead = userService.getUserRightsForEntity(new ConcessionarioBean());
		checkEntityRead = usrUtil.canRightsForEntity(entityRead, entityId);
		
		if(usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)  || checkEntityRead) {
			usrList = rgDao.getUserListFromEntityId(new ConcessionarioBean(entityId), "", true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}	
		
//		String userId = usrUtil.getUserId();
//		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
//		if(hasAccess==1){
//			usrList = rgDao.getUserListFromEntityId(new ConcessionarioBean(entityId), userId, true);
//		} else {
			////usrList = rgDao.getUserListFromEntityId(new ConcessionarioBean(entityId), "", true);
//		}
				
		return usrList;
	}
	
	//******************* GESTORE *************************
	
	public GestoreBean getGestoreWithParams(GestoreBean gsBean) throws SQLException, EntityNotFoundException {

		GestoreBean foundGsBean = null;
		
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		String userId = usrUtil.getUserId();
		
		if(hasAccess>=1){
			foundGsBean = gsDao.getGestoreWithParams(hasAccess, userId, gsBean, true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
				
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			foundGsBean = gsDao.getGestoreWithParams(gsBean, true);
//		}
		
		return foundGsBean;
	}

	public List<GestoreBean> getGestoreList() throws SQLException {

		List<GestoreBean> gsList = null;
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		String userId = usrUtil.getUserId();
		
		if(hasAccess>=1){
			gsList = gsDao.getGestoreListWithParams(hasAccess, userId, "", new GestoreBean(), false, true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			gsList = gsDao.getGestoreListWithParams("", new GestoreBean(), false, true);
//		}

		return gsList;
	}

	public List<GestoreBean> getGestoreListWithParams(GestoreBean gsBean) throws SQLException {

		List<GestoreBean> gsList = null;
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		String userId = usrUtil.getUserId();
		
		if(hasAccess>=1){
			gsList = gsDao.getGestoreListWithParams(hasAccess, userId, "", gsBean, false, true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			gsList = gsDao.getGestoreListWithParams("", gsBean, false, true);
//		}
		
		return gsList;
	}
	
	public List<GestoreBean> getGestoreListFromIdConcessionario(String idConcessionario) throws SQLException {

		List<GestoreBean> gsList = null;
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		String userId = usrUtil.getUserId();
		
		if(hasAccess>=1){
			gsList = gsDao.getGestoreListWithParams(hasAccess, userId, idConcessionario, new GestoreBean(), false, true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
	    }
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			gsList = gsDao.getGestoreListWithParams(idConcessionario, new GestoreBean(), false, true);
//		}
		
		return gsList;
	}

	public GestoreBean createGestore(GestoreBean gsBean) throws SQLException, EntityNotFoundException {
		
		GestoreBean createdGsBean = null;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			createdGsBean = gsDao.createGestore(gsBean, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return createdGsBean;
	}

	public GestoreBean updateGestore(int entityId, GestoreBean gsBean) throws Exception {

		GestoreBean updatedGsBean = null;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			gsBean.setId(""+entityId);
			updatedGsBean = gsDao.updateGestore(gsBean, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return updatedGsBean;
	}

	public Boolean removeGestore(String entityId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = gsDao.removeGestore(entityId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isRemoved;
	}

	public Boolean createGestoreToUserJunction(String entityId, String targetUserId) throws SQLException, EntityNotFoundException, JunctionAlreadyExistsException {

		Boolean isCreated = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isCreated = rgDao.createEntityToUserJunction(new GestoreBean(entityId), targetUserId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return isCreated;
	}
	
	public Boolean removeGestoreToUserJunction(String entityId, String targetUserId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = rgDao.removeEntityToUserJunction(new GestoreBean(entityId), targetUserId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isRemoved;
	}

	public ArrayList<DamBean> getDamListFromGestoreId(String entityId) throws SQLException {
		
		ArrayList<DamBean> damList = null; 
		
		Boolean checkEntityRead=false;
		ArrayList<String> entityRead = null;
		
		entityRead = userService.getUserRightsForEntity(new GestoreBean());
		checkEntityRead = usrUtil.canRightsForEntity(entityRead, entityId);
		
		if(usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA) || checkEntityRead) {
			damList = rgDao.getDamListFromEntityId(new GestoreBean(entityId), true);
		}else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}		
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			damList = rgDao.getDamListFromEntityId(new GestoreBean(entityId), true);
//		}
		
		return damList;
	}
	
	public ArrayList<UserBean> getUserListFromGestoreId(String entityId) throws SQLException {
		
		ArrayList<UserBean> usrList = null;
		
		Boolean checkEntityRead=false;
		ArrayList<String> entityRead = null;
		
		entityRead = userService.getUserRightsForEntity(new GestoreBean());
		checkEntityRead = usrUtil.canRightsForEntity(entityRead, entityId);
		
		if(usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA) || checkEntityRead) {
			usrList = rgDao.getUserListFromEntityId(new GestoreBean(entityId), "", true);
		}else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}	
		
//		String userId = usrUtil.getUserId();
//		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
//		
//		if(hasAccess==1){
//			usrList = rgDao.getUserListFromEntityId(new GestoreBean(entityId), userId, true);
//		} else {
			////usrList = rgDao.getUserListFromEntityId(new GestoreBean(entityId), "", true);
//		}
		
		return usrList;
	}

	//******************* INGEGNERE *************************

	public IngegnereBean getIngegnereWithParams(IngegnereBean inBean) throws SQLException, EntityNotFoundException {

		IngegnereBean foundInBean = null;
		
		Boolean  canReadAsIng = false;
		String   ingId = inBean.getId();
		canReadAsIng = userService.checkRigthsForIngegnere(ingId);
		
		if(canReadAsIng){
			foundInBean = inDao.getIngegnereWithParams(inBean, true);	
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
				
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			foundInBean = inDao.getIngegnereWithParams(inBean, true);	
//		}
//		
		return foundInBean;
	}

	
	/*
	 * se non ha i diritti lista ingegneri va filtrata per l'entita legata a quel utente (concessionario)
	 * no entita = lista vuota
	 * 
	 */
	
	public List<IngegnereBean> getIngegnereList() throws SQLException {

		List<IngegnereBean> inList = null;
		String userId = usrUtil.getUserId();
		
		/*if(usrUtil.canReadData(ID_ANAGRAFICA, userId)) {
			inList = inDao.getIngegnereListWithParams(new IngegnereBean(), "", Boolean.FALSE, Boolean.TRUE);
		} else {
			inList = inDao.getIngegnereListWithParams(new IngegnereBean(), userId, Boolean.FALSE, Boolean.TRUE);
		}*/
		
//		try {
//			if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//				inList = inDao.getIngegnereListWithParams(new IngegnereBean(), "", Boolean.FALSE, Boolean.TRUE);
//			} 
//		} catch (AccessDeniedException e) {
//			inList = inDao.getIngegnereListWithParams(new IngegnereBean(), userId, Boolean.FALSE, Boolean.TRUE);
//		}
		
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			inList = inDao.getIngegnereListWithParams(new IngegnereBean(), userId, hasAccess, Boolean.FALSE, Boolean.TRUE);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return inList;
	}

	public List<IngegnereBean> getIngegnereListWithParams(IngegnereBean inBean) throws SQLException {

		List<IngegnereBean>  inList = null;
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			inList = inDao.getIngegnereListWithParams(inBean, "", false, true);
//		}
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			inList = inDao.getIngegnereListWithParams(inBean, userId, hasAccess, Boolean.FALSE, Boolean.TRUE);
		} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		return inList;
	}

	public IngegnereBean createIngegnere(IngegnereBean inBean) throws SQLException, EntityNotFoundException {
		
		IngegnereBean createdInBean = null;
		
		if(skipCheckScheda || usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)){
			createdInBean = inDao.createIngegnere(inBean, usrUtil.getUserId(), true);			
		}
		
		return createdInBean;
	}

	public IngegnereBean updateIngegnere(IngegnereBean inBean) throws Exception {

		IngegnereBean updatedInBean = null;
		Boolean  canWriteAsIng = false;
		String   ingId = inBean.getId();
		canWriteAsIng = userService.checkRigthsForIngegnere(ingId);
		
		if(canWriteAsIng){
		//if(skipCheckScheda || usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)){
			updatedInBean = inDao.updateIngegnere(inBean, usrUtil.getUserId(), true);			
		}else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
	    }

		return updatedInBean;
	}

	public Boolean removeIngegnere(String entityId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = false;
		Boolean  canWriteAsIng = false;
		canWriteAsIng = userService.checkRigthsForIngegnere(entityId);
				
		//if(skipCheckScheda || usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)){
		if(canWriteAsIng){
			isRemoved = inDao.removeIngegnere(entityId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isRemoved;
	}

	//******************* ALTRA ENTE *************************
	
	public AltraEnteBean getAltraEnteWithParams(AltraEnteBean aeBean) throws SQLException, EntityNotFoundException {

		AltraEnteBean foundAeBean = null; 
		
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			foundAeBean = aeDao.getAltraEnteWithParams(hasAccess, userId, aeBean, true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
				
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			foundAeBean = aeDao.getAltraEnteWithParams(aeBean, true);
//		}
		
		return foundAeBean;
	}

	public List<AltraEnteBean> getAltraEnteList() throws SQLException {

		List<AltraEnteBean> aeList = null;
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			aeList = aeDao.getAltraEnteListWithParams(new AltraEnteBean(), false, true);
//		}
		
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			aeList = aeDao.getAltraEnteListWithParams(hasAccess, userId, new AltraEnteBean(), false, true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}


		return aeList;
	}

	public List<AltraEnteBean> getAltraEnteListWithParams(AltraEnteBean aeBean) throws SQLException {

		List<AltraEnteBean>  aeList = null;
		
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			aeList = aeDao.getAltraEnteListWithParams(aeBean, false, true);
//		}
		
		String userId = usrUtil.getUserId();
		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
		
		if(hasAccess>=1){
			aeList = aeDao.getAltraEnteListWithParams(hasAccess, userId, aeBean, false, true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return aeList;
	}

	public AltraEnteBean createAltraEnte(AltraEnteBean aeBean) throws SQLException, EntityNotFoundException {
		
		AltraEnteBean createdAeBean = null;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			createdAeBean = aeDao.createAltraEnte(aeBean, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return createdAeBean;
	}

	public AltraEnteBean updateAltraEnte(AltraEnteBean aeBean) throws Exception {

		AltraEnteBean updatedAeBean = null;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			updatedAeBean = aeDao.updateAltraEnte(aeBean, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}

		return updatedAeBean;
	}

	public Boolean removeAltraEnte(String entityId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = null;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = aeDao.removeAltraEnte(entityId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isRemoved;
	}

	public Boolean createAltraEnteToUserJunction(String entityId, String targetUserId) throws SQLException, EntityNotFoundException, JunctionAlreadyExistsException {

		Boolean isCreated = false;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isCreated = rgDao.createEntityToUserJunction(new AltraEnteBean(entityId), targetUserId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isCreated;
	}
	
	public Boolean removeAltraEnteToUserJunction(String entityId, String targetUserId) throws SQLException, EntityNotFoundException {

		Boolean isRemoved = null;
		//skipCheckScheda || 
		if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
			isRemoved = rgDao.removeEntityToUserJunction(new AltraEnteBean(entityId), targetUserId, usrUtil.getUserId(), true);
		} else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
		return isRemoved;
	}

	public ArrayList<DamBean> getDamListFromAltraEnteId(String entityId) throws SQLException {
		
		ArrayList<DamBean> damList = null;
		
		Boolean checkEntityRead=false;
		ArrayList<String> entityRead = null;
		
		entityRead = userService.getUserRightsForEntity(new AltraEnteBean());
		checkEntityRead = usrUtil.canRightsForEntity(entityRead, entityId);
		
		if(usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA) || checkEntityRead) {
			damList = rgDao.getDamListFromEntityId(new AltraEnteBean(entityId), true);
		}else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}			
				
//
//		if(skipCheckScheda || usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
//			damList = rgDao.getDamListFromEntityId(new AltraEnteBean(entityId), true);
//		}
//		
		return damList;
	}
	
	public ArrayList<UserBean> getUserListFromAltraEnteId(String entityId) throws SQLException {
		
		ArrayList<UserBean> usrList = null;
		
		Boolean checkEntityRead=false;
		ArrayList<String> entityRead = null;
		
		entityRead = userService.getUserRightsForEntity(new AltraEnteBean());
		checkEntityRead = usrUtil.canRightsForEntity(entityRead, entityId);
		
		if(usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA) || checkEntityRead) {
			usrList = rgDao.getUserListFromEntityId(new AltraEnteBean(entityId), "", true);
		}else {
			logger.error("Access Denied - User can't write this resource.");
			throw new AccessDeniedException("Access Denied - User can't write this resource.");
		}
		
//		String userId = usrUtil.getUserId();
//		int hasAccess=usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);
//		
//		if(hasAccess==1){
//			usrList = rgDao.getUserListFromEntityId(new AltraEnteBean(entityId), userId, true);
//		} else {
			////usrList = rgDao.getUserListFromEntityId(new AltraEnteBean(entityId), "", true);
//		}
		
		return usrList;
	}

	//******************* USER *************************
	
	public ArrayList<UserBean> getUserList() throws SQLException {
		
		ArrayList<UserBean> usrList = null;
		//skipCheckScheda || 
		if(usrUtil.canReadFromScheda(DBUtility.ANAGRAFICA)) {
			usrList = rgDao.getUserList(true);
		}
		
		return usrList;
	}

	//******************* CONTACT INFO *************************
	
	/**
	 * 
	 * @param pnBean
	 * @param targetEntity
	 * @return
	 * @throws SQLException
	 */
	public ContactInfoBean createContactInfo(ContactInfoBean pnBean, BasicEntityBean targetEntity) throws SQLException {

		ContactInfoBean ciBean = null;
		String entityId = "";
		Boolean  canWriteAsIng = false;
		entityId = targetEntity.getId();
		
		if (targetEntity instanceof IngegnereBean) {
			
			canWriteAsIng = userService.checkRigthsForIngegnere(entityId);
			if(canWriteAsIng){
				ciBean = ciDao.createContactInfo(pnBean, targetEntity, usrUtil.getUserId(), true);
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
			
		}else {
			
			//skipCheckScheda ||
			if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
				ciBean = ciDao.createContactInfo(pnBean, targetEntity, usrUtil.getUserId(), true);
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
			
		}
				
		return ciBean; 
	}
	
	/**
	 * 
	 * @param pnBean
	 * @param targetEntity
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException 
	 */
	public ContactInfoBean updateContactInfo(int fieldId, ContactInfoBean cnBean, BasicEntityBean targetEntity) throws SQLException, EntityNotFoundException, AccessDeniedException {
		
		ContactInfoBean ciBean = null;
		
		Boolean checkEntityWrite=false;		
		String entityId = "";
		ArrayList<String> entityWrite = null;	
		Boolean  canWriteAsIng = false;		
		entityId = targetEntity.getId();
		
		if (targetEntity instanceof IngegnereBean) {
			
			canWriteAsIng = userService.checkRigthsForIngegnere(entityId);
			if(canWriteAsIng){
				cnBean.setId(""+fieldId);
				ciBean = ciDao.updateContactInfo(cnBean, targetEntity, usrUtil.getUserId(), true);	
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
									
		}else {
			
			entityWrite = userService.getUserRightsForEntity(targetEntity);
			checkEntityWrite = usrUtil.canRightsForEntity(entityWrite, entityId);			
			//skipCheckScheda || 	
			if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA_DETTAGLIO) || usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA) || checkEntityWrite) {
				cnBean.setId(""+fieldId);
				ciBean = ciDao.updateContactInfo(cnBean, targetEntity, usrUtil.getUserId(), true);					
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}			
		}
									
		return ciBean; 
	}
	
	/**
	 * 
	 * @param fieldId
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException
	 */
	public Boolean removeContactInfo(int fieldId, BasicEntityBean targetEntity) throws SQLException, EntityNotFoundException {
		
		Boolean isRemoved = false;
		
		String entityId = "";
		Boolean  canWriteAsIng = false;		
		entityId = targetEntity.getId();
				
		if (targetEntity instanceof IngegnereBean) {
			
			canWriteAsIng = userService.checkRigthsForIngegnere(entityId);
			if(canWriteAsIng){
				isRemoved = ciDao.removeContactInfo(""+fieldId, targetEntity, usrUtil.getUserId(), true);
			}else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");				
			}
			
		}else {
			
			//skipCheckScheda || 
			if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
				isRemoved = ciDao.removeContactInfo(""+fieldId, targetEntity, usrUtil.getUserId(), true);
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
			
		}
				
		return isRemoved;
	}


	//******************* ADDRESS *************************
	
	/**
	 * 
	 * @param adBean
	 * @param targetEntity
	 * @return
	 * @throws SQLException
	 */
	public AddressBean createAddress(AddressBean adBean, BasicEntityBean targetEntity) throws SQLException {

		AddressBean createdAdBean = null;
		String entityId = "";
		Boolean  canWriteAsIng = false;
		entityId = targetEntity.getId();
		
		if (targetEntity instanceof IngegnereBean) {
			
			canWriteAsIng = userService.checkRigthsForIngegnere(entityId);
			if(canWriteAsIng){
				createdAdBean = adDao.createAddress(adBean, targetEntity, usrUtil.getUserId(), true);
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
			
		} else {
			
			//skipCheckScheda || 
			if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
				createdAdBean = adDao.createAddress(adBean, targetEntity, usrUtil.getUserId(), true);
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
			
		}
			
		return createdAdBean;
	}
	
	/**
	 * 
	 * @param adBean
	 * @param targetEntity
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException 
	 */
	public AddressBean updateAddress(int fieldId, AddressBean adBean, BasicEntityBean targetEntity) throws SQLException, EntityNotFoundException {

		AddressBean updatedAdBean = null;
		
		Boolean checkEntityWrite=false;		
		String entityId = "";
		ArrayList<String> entityWrite = null;
		Boolean  canWriteAsIng = false;		
		entityId = targetEntity.getId();
		
		
		if (targetEntity instanceof IngegnereBean) {
				
			canWriteAsIng = userService.checkRigthsForIngegnere(entityId);
			if(canWriteAsIng){
				adBean.setId(""+fieldId);
				updatedAdBean = adDao.updateAddress(adBean, targetEntity, usrUtil.getUserId(), true);	
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
							
		} else {
			
			entityWrite = userService.getUserRightsForEntity(targetEntity);
			checkEntityWrite = usrUtil.canRightsForEntity(entityWrite, entityId);
			
			//skipCheckScheda ||
			if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA_DETTAGLIO) || usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA) || checkEntityWrite) {
				adBean.setId(""+fieldId);
				updatedAdBean = adDao.updateAddress(adBean, targetEntity, usrUtil.getUserId(), true);	
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
					
			
		}		
		
		return updatedAdBean;
	}
	
	/**
	 * 
	 * @param fieldId
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException
	 */
	public Boolean removeAddress(int fieldId, BasicEntityBean targetEntity) throws SQLException, EntityNotFoundException {
		Boolean isRemoved = false;
		
		String entityId = "";
		Boolean  canWriteAsIng = false;		
		entityId = targetEntity.getId();
		
		if (targetEntity instanceof IngegnereBean) {
			
			canWriteAsIng = userService.checkRigthsForIngegnere(entityId);
			if(canWriteAsIng){				
				isRemoved = adDao.removeAddress(""+fieldId, targetEntity, usrUtil.getUserId(), true);
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
		
		} else {
			
			//skipCheckScheda || 
			if(usrUtil.canWriteIntoScheda(DBUtility.ANAGRAFICA)) {
				isRemoved = adDao.removeAddress(""+fieldId, targetEntity, usrUtil.getUserId(), true);
			} else {
				logger.error("Access Denied - User can't write this resource.");
				throw new AccessDeniedException("Access Denied - User can't write this resource.");
			}
			
		}
				
		return isRemoved;
	}
	
	//******************* ENTITY TYPES *************************
	
	public ArrayList<ProvinciaBean> getProvinciaList() throws SQLException {
		return etDao.getProvinciaList();
	}
	
	public ArrayList<EntityTypeBean> getRegioneList() throws SQLException {
		return etDao.getRegioneList();
	}
	
	public ArrayList<EntityTypeBean> getTipoEnteList() throws SQLException {
		return etDao.getTipoEnteList();
	}
	
	public ArrayList<EntityTypeBean> getTipoAltraEnteList() throws SQLException {
		return etDao.getTipoAltraEnteList();
	}
	
	public ArrayList<EntityTypeBean> getTipoAutoritaList() throws SQLException {
		return etDao.getTipoAutoritaList();
	}
	
	public ArrayList<EntityTypeBean> getTipoContattoList() throws SQLException {
		return etDao.getTipoContattoList();
	}
}
