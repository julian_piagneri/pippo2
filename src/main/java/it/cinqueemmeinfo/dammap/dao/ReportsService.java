package it.cinqueemmeinfo.dammap.dao;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.ReportIdsBean;
import it.cinqueemmeinfo.dammap.bean.entities.ReportTemplateBean;
import it.cinqueemmeinfo.dammap.dao.macro.ReportsDao;

public class ReportsService implements Serializable {

	private static final long serialVersionUID = -2927571924602882936L;
	private static final Logger logger = LoggerFactory.getLogger(ReportsService.class);

	@Autowired
	private ReportsDao reportsDao;

	// ******************* REPORT *************************

	public int saveReportIds(Long userId, ReportIdsBean ids) throws SQLException {
		logger.info("Saving report");
		return reportsDao.saveReportIds(userId, ids);
	}

	public int saveJsonReport(Long userId, ReportIdsBean ids) throws SQLException {
		logger.info("Saving json report");
		return reportsDao.saveJsonReport(userId, ids);
	}

	public Boolean deleteReportByUser(Long userId) throws SQLException {
		logger.info("Deleting reports");
		return reportsDao.deleteReportsByUser(userId);
	}

	public ReportIdsBean getReportById(Long reportId) throws SQLException, IOException {
		return reportsDao.retrieveReportById(reportId);
	}

	public List<ReportIdsBean> getReportByUserId(Long userId) throws SQLException, IOException {
		return reportsDao.retrieveByUserId(userId);
	}

	public List<ReportTemplateBean> getAllTemplates() throws SQLException {
		return reportsDao.retrieveAllTemplates();
	}
}
