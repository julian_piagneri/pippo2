package it.cinqueemmeinfo.dammap.dao.macro;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cinqueemmeinfo.dammap.bean.entities.DamBean;
import it.cinqueemmeinfo.dammap.bean.entities.ReportIdsBean;
import it.cinqueemmeinfo.dammap.bean.entities.ReportTemplateBean;
import it.cinqueemmeinfo.dammap.bean.entities.VistaBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;

public class ReportsDao implements Serializable {

	@Autowired private DBUtility dbUtil;
	//@Autowired private BeanUtility bnUtil;
	private static final long serialVersionUID = 906472673246974813L;
	private static final Logger logger = LoggerFactory.getLogger(ReportsDao.class);
	
	/**
	 * 
	 * @param targetEntity
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public VistaBean retrieveViewsForUser(Long userId, boolean closeConnection) throws SQLException {
		
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		VistaBean element= null;
		
		try {
			//SELECT
				sql = " SELECT a.ID AS id, a.NAME AS name, a.DEFAULT_VIEW AS defaultView, a.STATUS AS status, "
						+ " a.ID_UTENTE AS idUtente "
						+ " FROM UTENTE_VISTA a "
						+ " WHERE a.ID_UTENTE = ? "; 
		
			//SET QUERY PARAM
			conn=dbUtil.getNewConnection();
			ps=conn.prepareStatement(sql);
			int i=1;
			ps.setLong(i++, userId);
			//EXECUTE
			rs=ps.executeQuery();
			if(rs.next()){
				element=this.fillVistaBean(rs);
			} 
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} catch (JsonProcessingException e) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + e.getMessage());
			e.printStackTrace();
		} finally {
			dbUtil.closeQuietly(conn, rs, ps);
		}
		
		return element;
	}
	
	private VistaBean fillVistaBean(ResultSet rs) throws SQLException, JsonProcessingException, IOException{
		VistaBean vistaBean=new VistaBean();
		vistaBean.setId(rs.getLong("id"));
		vistaBean.setIdUtente(rs.getLong("idUtente"));
		vistaBean.setName(rs.getString("name"));
		vistaBean.setDefaultView(rs.getInt("defaultView"));
		ObjectMapper mapper = new ObjectMapper();
	    JsonNode json = mapper.readTree(rs.getString("status"));
		vistaBean.setStatus(json);
		return vistaBean;
	}


	
	public ReportIdsBean retrieveReportById(Long reportId) throws SQLException, IOException {
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		ReportIdsBean element= null;
		
		try {
			//Carica la vista base (non si può caricare nella stessa query perchè possono esserci utenti senza righe
			/*
			sql=" SELECT a.ID AS id, a.ID_UTENTE AS idUtente, a.NAME AS name, "
				+ " a.CREATION_DATE AS creationDate, a.JSON_DATA AS jsonData "
				+ " FROM REPORT_IDS a WHERE a.ID=? ";
			*/
			sql=" SELECT a.ID AS id, a.ID_UTENTE AS idUtente, "
					+ " a.CREATION_DATE AS creationDate, a.JSON_DATA AS jsonData "
					+ " FROM REPORT_JSON a WHERE a.ID=? ";
			int i=1;
			conn=dbUtil.getNewConnection();
			ps=conn.prepareStatement(sql);
			ps.setLong(i++, reportId.longValue());
			rs=ps.executeQuery();
			if(rs.next()){
				element = this.fillReportBean(rs);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} catch (JsonProcessingException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} catch (IOException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			dbUtil.closeQuietly(conn, rs, ps);
		}
		
		return element;
	}
	
	private ReportIdsBean fillReportBean(ResultSet rs) throws SQLException, JsonProcessingException, IOException {
		ReportIdsBean reportIdsBean=new ReportIdsBean();
		reportIdsBean.setId(rs.getLong("id"));
		reportIdsBean.setIdUtente(rs.getLong("idUtente"));
		// reportIdsBean.setName(rs.getString("name"));
		reportIdsBean.setCreationDate(rs.getTimestamp("creationDate"));
		ObjectMapper mapper = new ObjectMapper();
	    JsonNode jsonData = mapper.readTree(rs.getString("jsonData"));
	    reportIdsBean.setJsonData(jsonData);
		return reportIdsBean;
	}
	
	public Boolean deleteReportsByUser(Long userId) throws SQLException {
		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		Connection conn=null;
		Boolean done = false;
		PreparedStatement dmPs = null;
		
		try {
			//Controlla se ancora non è stata creata la riga vista per l'utente
			conn=dbUtil.getNewConnection();
			sql="DELETE FROM REPORT_IDS WHERE ID_UTENTE=?";
			//Retrieving connection
			dmPs = conn.prepareStatement(sql);
			dmPs.setLong(paramsSet++, userId.longValue());

			
			updatedRows += dmPs.executeUpdate();
			
			if (updatedRows > 0) {
				done = true;
			} 
			
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			dbUtil.closeQuietly(conn);
			dbUtil.closeQuietly(dmPs);
		}
		
		return done;
	}

	public int saveReportIds(Long userId, ReportIdsBean ids) throws SQLException {
		int paramsSet = 1;
		String sql = "";
		Connection conn=null;
		PreparedStatement dmPs = null;
		int nextVal=-1;
		
		try {
			//Controlla se ancora non è stata creata la riga vista per l'utente
			conn=dbUtil.getNewConnection();
			
			//Cancella il report attuale dell'utente
			sql="DELETE FROM REPORT_IDS WHERE ID_UTENTE=?";
			//Retrieving connection
			dmPs = conn.prepareStatement(sql);
			dmPs.setLong(paramsSet++, userId.longValue());

			dmPs.executeUpdate();
			
			dbUtil.closeQuietly(dmPs);
			
			paramsSet = 1;
			nextVal=dbUtil.getNextSequenceId("REPORT_IDS_SEQ");
			sql="INSERT INTO REPORT_IDS(ID, ID_UTENTE, NAME, JSON_DATA, CREATION_DATE) VALUES (?, ?, ?, ?, ?)";
			//Retrieving connection
			dmPs = conn.prepareStatement(sql);
			dmPs.setInt(paramsSet++, nextVal);
			dmPs.setLong(paramsSet++, userId.longValue());
			dmPs.setString(paramsSet++, ids.getName());
			paramsSet=DBUtility.setClobParameter(dmPs, ids.getJsonData().toString(), paramsSet);//this.setListParameter(dmPs, ids.getJsonData(), paramsSet);
			dmPs.setTimestamp(paramsSet++, new Timestamp(System.currentTimeMillis()));
			dmPs.executeUpdate();
			
			//Salva gli id delle dighe associate al report
			List<DamBean> damIds=ids.getDamIds();
			for (DamBean damBean : damIds) {
				//closes the previous statement
				dbUtil.closeQuietly(dmPs);
				sql="INSERT INTO REPORT_DIGHE(NUMERO_ARCHIVIO, SUB, ID_REPORT) VALUES (?, ?, ?)";
				paramsSet=1;
				dmPs = conn.prepareStatement(sql);
				dmPs.setString(paramsSet++, damBean.getNumeroArchivio());
				dmPs.setString(paramsSet++, damBean.getSub());
				dmPs.setLong(paramsSet++, nextVal);
				dmPs.executeUpdate();
			}
			
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			dbUtil.closeQuietly(conn);
			dbUtil.closeQuietly(dmPs);
		}
		
		return nextVal;
	}


	public int saveJsonReport(Long userId, ReportIdsBean ids) throws SQLException {
		int paramsSet = 1;
		String sql = "";
		Connection conn=null;
		PreparedStatement dmPs = null;
		int nextVal=-1;
		
		try {
			//Controlla se ancora non è stata creata la riga vista per l'utente
			conn=dbUtil.getNewConnection();
			
			//Cancella il report attuale dell'utente
			sql="DELETE FROM REPORT_JSON WHERE ID_UTENTE=?";
			//Retrieving connection
			dmPs = conn.prepareStatement(sql);
			dmPs.setLong(paramsSet++, userId.longValue());

			dmPs.executeUpdate();
			
			dbUtil.closeQuietly(dmPs);
			
			paramsSet = 1;
			nextVal=dbUtil.getNextSequenceId("REPORT_JSON_SEQ");
			// sql="INSERT INTO REPORT_IDS(ID, ID_UTENTE, NAME, JSON_DATA, CREATION_DATE) VALUES (?, ?, ?, ?, ?)";
			sql="INSERT INTO REPORT_JSON(ID, ID_UTENTE, JSON_DATA, CREATION_DATE) VALUES (?, ?, ?, ?)";
			//Retrieving connection
			dmPs = conn.prepareStatement(sql);
			dmPs.setInt(paramsSet++, nextVal);
			dmPs.setLong(paramsSet++, userId.longValue());
			// dmPs.setString(paramsSet++, ids.getName());
			paramsSet=DBUtility.setClobParameter(dmPs, ids.getJsonData().toString(), paramsSet);//this.setListParameter(dmPs, ids.getJsonData(), paramsSet);
			dmPs.setTimestamp(paramsSet++, new Timestamp(System.currentTimeMillis()));
			dmPs.executeUpdate();
			
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			dbUtil.closeQuietly(conn);
			dbUtil.closeQuietly(dmPs);
		}
		
		return nextVal;
	}

	
	
	
	public List<ReportIdsBean> retrieveByUserId(Long userId) throws SQLException, IOException {
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		List<ReportIdsBean> elements= new ArrayList<ReportIdsBean>();
		
		try {
			//Carica la vista base (non si può caricare nella stessa query perchè possono esserci utenti senza righe
			/*
			sql=" SELECT a.ID AS id, a.ID_UTENTE AS idUtente, a.NAME AS name, "
				+ " a.CREATION_DATE AS creationDate, a.JSON_DATA AS jsonData "
				+ " FROM REPORT_IDS a WHERE a.ID_UTENTE=? ";
			*/
			sql=" SELECT a.ID AS id, a.ID_UTENTE AS idUtente, "
					+ " a.CREATION_DATE AS creationDate, a.JSON_DATA AS jsonData "
					+ " FROM REPORT_JSON a WHERE a.ID_UTENTE=? ";
			int i=1;
			conn=dbUtil.getNewConnection();
			ps=conn.prepareStatement(sql);
			ps.setLong(i++, userId);
			rs=ps.executeQuery();
			while(rs.next()){
				elements.add(this.fillReportBean(rs));
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} catch (JsonProcessingException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} catch (IOException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			dbUtil.closeQuietly(conn, rs, ps);
		}
		
		return elements;
	}
	
	
	private ReportTemplateBean fillReportTemplateBean(ResultSet rs) throws SQLException {
		ReportTemplateBean reportTemplateBean=new ReportTemplateBean();
		reportTemplateBean.setId(rs.getLong("id"));
		reportTemplateBean.setName(rs.getString("name"));
		reportTemplateBean.setTemplate(rs.getString("template"));
		reportTemplateBean.setUrl(rs.getString("url"));
		return reportTemplateBean;
	}

	public List<ReportTemplateBean> retrieveAllTemplates() throws SQLException {
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		List<ReportTemplateBean> elements= new ArrayList<ReportTemplateBean>();
		
		try {
			//Carica la vista base (non si può caricare nella stessa query perchè possono esserci utenti senza righe
			sql=" SELECT a.ID AS id, a.NAME AS name, a.TEMPLATE AS template, a.URL AS url "
				+ " FROM REPORT_TEMPLATES a ";
			conn=dbUtil.getNewConnection();
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()){
				elements.add(this.fillReportTemplateBean(rs));
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			dbUtil.closeQuietly(conn, rs, ps);
		}
		
		return elements;
	}
}
