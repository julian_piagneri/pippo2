package it.cinqueemmeinfo.dammap.dao.macro;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

public class UserDao implements Serializable {

	private static final long serialVersionUID = 7946764094483645924L;
	private static final Logger logger = LoggerFactory.getLogger(UserDao.class);
	private static final String DAM_PREFERENCES="FILECSS_12_0_N_N ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-*/<";
	public static final long DAYMILLIS=60*60*24;

	@Autowired private LoginDao loginDao;
	@Autowired private DBUtility dbUtil;
	@Autowired private UserUtility userUtil;
	
	/**
	 * 
	 * @param availableFields 
	 * @param updatePassword 
	 * @param updateExpiration 
	 * @param userId 
	 * @param apcBean
	 * @param userId
	 * @param closeConnection if true closes connection and commmits changes, if false transaction will be kept open for futher operations
	 * @return
	 * @throws Exception 
	 */
	public UserSecurityBean updateUtente(UserSecurityBean userBean, Map<String, String> availableFields, boolean updatePassword, boolean updateExpiration, int userId, boolean isNew) throws Exception {
		
		int paramsSet = 1;
		//int updatedRows = 0;
		String sql = "";
		PreparedStatement ps = null;
		UserSecurityBean foundUserBean = null;
		Connection conn=null;
		String fields=this.getAvailableFields(availableFields, userBean, isNew, true);
		try {
			//Retrieving connection
			conn=dbUtil.getNewConnection();
			//Carica l'id se l'utente è nuovo
			if(isNew){
				userId=this.getNextSequenceId("UTENTE_SEQUENZA");
				userBean.setId(userId);
//				ResultSet rs=null;
//				sql = "SELECT UTENTE_SEQUENZA.nextval FROM dual";
//				ps=conn.prepareStatement(sql);
//				rs = ps.executeQuery();
//				
//				if (rs != null && rs.next()) {
//					userId=rs.getInt(1);
//					userBean.setId(userId);
//				} else {
//					throw new SQLException("Couldn't retrieve next sequence value.");
//				}
//				dbUtil.closeQuietly(rs);
//				dbUtil.closeQuietly(ps);
			}

			//Se non vengono restituiti campi da modificare ma bisogna modificare la password
			//allora aggiorna perlomeno la data modifica 
			//Altrimenti si sta inserendo un nuovo utente
			if(!fields.trim().equals("") || updatePassword || isNew){
				if(isNew){
					sql=" INSERT INTO UTENTE ("+fields+" DATA_MODIFICA, ID_UTENTE, PREFERENZE, ID) VALUES " +
						" ("+this.addMarksForInsert(availableFields, true)+" ?, ?, ?, ?) ";
				} else {
					sql = "UPDATE UTENTE SET "+fields+" DATA_MODIFICA=?, ID_UTENTE=? WHERE ID=?";
				}
				ps = conn.prepareStatement(sql);
				
				paramsSet=this.setAvailableFields(availableFields, userBean, paramsSet, ps);
				
				ps.setTimestamp(paramsSet++, new Timestamp(System.currentTimeMillis()));
				ps.setString(paramsSet++, userUtil.getUserId()); //Id dell'utente che ha fatto l'ultima modifica
				if(isNew){
					ps.setString(paramsSet++, UserDao.DAM_PREFERENCES);
				}
				ps.setInt(paramsSet++, userId); //ID
				ps.executeUpdate();
				//updatedRows += ps.executeUpdate();
			}
			
			//deve aggiornare anche la password o la data di scadenza password
			if(updatePassword || updateExpiration){
				this.updatePassword(userBean, conn, updateExpiration, updatePassword);
			}
			
			//Controlla se è stato aggiornato qualcosa
			foundUserBean = loginDao.findByIdWithDetails(userBean.getId(), true);
			//updatedRows <= 0 && (!fields.equals("") || updatePassword)
			if (foundUserBean==null) { //Se non ci sono righe aggiornate ma ci si aspettava l'aggiornamento
				throw new EntityNotFoundException("Entity with ID="+userId+" not found.");
			}
			if(isNew){
				//Se l'utente è nuovo aggiunge un gruppo privato chiamato G_username 
				this.insertGroupForUser(foundUserBean, conn);
			}
		} catch (Exception e) {
			logger.error("Error while executing '" + sql + "'");
			e.printStackTrace();
			throw e;
		} finally {			
			DBUtility.closeQuietly(conn);
			DBUtility.closeQuietly(ps);
		}
		
		return foundUserBean;
	}
	
	private int getNextSequenceId(String sequence) throws SQLException{
		return dbUtil.getNextSequenceId(sequence);
//		String sql = "";
//		PreparedStatement ps = null;
//		Connection conn=null;
//		ResultSet rs=null;
//		int sequenceVal=-1;
//		try {
//			conn=dbUtil.getNewConnection();
//			sql = "SELECT "+sequence+".nextval FROM dual";
//			ps=conn.prepareStatement(sql);
//			rs = ps.executeQuery();
//			if (rs != null && rs.next()) {
//				sequenceVal=rs.getInt(1);
//			} else {
//				throw new SQLException("Couldn't retrieve next sequence value.");
//			}
//		} catch (SQLException sqle) {
//			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
//			sqle.printStackTrace();
//			throw sqle;
//		} finally {
//        	dbUtil.closeQuietly(conn, rs, ps);
//        }
//		return sequenceVal;
	}
	
	public int insertGroupForUser(UserSecurityBean userBean, Connection conn) throws SQLException {
		String sql = "";
		PreparedStatement ps = null;
		int sequenceId=-1;
		int updatedRows=0;
		try {
			sequenceId=this.getNextSequenceId("GRUPPO_SEQUENZA");
			sql="INSERT INTO GRUPPO (ID, DESCR, PRIVATO) VALUES (?, ?, ?)";
			int ip=1;
			ps = conn.prepareStatement(sql);
			ps.setInt(ip++, sequenceId);
			ps.setString(ip++, "G_"+userBean.getUsername());
			ps.setString(ip++, "S");
			updatedRows=ps.executeUpdate();
			if(updatedRows==1){
				DBUtility.closeQuietly(ps);
				sql="INSERT INTO UTENTE_GRUPPO (ID_UTENTE, ID_GRUPPO) VALUES (?, ?)";
				ip=1;
				ps = conn.prepareStatement(sql);
				ps.setInt(ip++, userBean.getId());
				ps.setInt(ip++, sequenceId);
				ps.executeUpdate();
			} else {
				throw new SQLException("Group for user with ID "+userBean.getId()+" not added");
			}
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
			throw sqle;
		} finally {
			conn.setAutoCommit(true);
        	DBUtility.closeQuietly(ps);
        }
		return updatedRows;
	}
	
	private int updatePassword(UserSecurityBean userBean, Connection conn, boolean updateExpiration, boolean updatePassword) throws SQLException{
		PreparedStatement ps=null;
		ResultSet rs=null;
		if(updatePassword==false && updateExpiration==false){
			//nulla da fare
			return 0;
		}
		try {
			//Controlla se la riga per la password in utente_extra non sia già stata inserita
			String sql = "SELECT COUNT(1) AS pwdRowCount FROM UTENTE_EXTRA WHERE ID_UTENTE=?";
			//ps = StatementFactory.getStatement(conn, sql, DebugLevel.ON);
			ps = conn.prepareStatement(sql);
			int paramsSet=1;
			ps.setInt(paramsSet++, userBean.getId()); //ID
			rs = ps.executeQuery();
			int count=0;
			if(rs.next()){
				count=rs.getInt("pwdRowCount");
			}
			DBUtility.closeQuietly(ps);
			DBUtility.closeQuietly(rs);
			
			String fields="";
			if(count==0){
				if(updatePassword){
					fields+=" PASSWORD, LAST_PWD_UPDATE, ";
				}
				sql="INSERT INTO UTENTE_EXTRA ( "+fields+" PWD_EXPIRE, ID_UTENTE) VALUES (?,?,?,?)";
			} else {
				if(updatePassword){
					fields+=" PASSWORD=?, LAST_PWD_UPDATE=?, ";
				}
				sql = "UPDATE UTENTE_EXTRA SET "+fields+" PWD_EXPIRE=? WHERE ID_UTENTE=?";
			}
			ps = conn.prepareStatement(sql);
			paramsSet=1;
			if(updatePassword){
				String pw=userBean.getPassword();
				ps.setString(paramsSet++, pw);
				//Aggiorna la data ultimo aggiornamento password
				ps.setTimestamp(paramsSet++, new Timestamp(System.currentTimeMillis()));
			}
			
			Map<String, Integer> expireDates=loginDao.getExpireDates();
			/*La data di scadenza deve essere determinata in questo modo:
			Se la si sta modificando allora la forza, altrimenti è la più grande tra la data ultimo aggiornamento +30 giorni e la data di scadenza attuale
			Siccome questa funzione viene chiamata se updateExpiration o updatePassword è =true allora se updateExpiration è false possiamo assumere
			che updatePassword lo sia per forza e quindi il campo scadenza va modificato
			*/
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(new Date());
//			cal.add(Calendar.DATE, expireDates.get(LoginDao.EXPIRES_LABEL));
			long newExpireDate=System.currentTimeMillis()+(expireDates.get(LoginDao.EXPIRES_LABEL)*UserDao.DAYMILLIS*1000);
			Timestamp expirationDate=updateExpiration ? userBean.getPwdExpireDate() : new Timestamp(Math.max(newExpireDate, userBean.getPwdExpireDate().getTime())); 
			ps.setTimestamp(paramsSet++, expirationDate);
			ps.setInt(paramsSet++, userBean.getId()); //ID
			int updatedRows=ps.executeUpdate();
			DBUtility.closeQuietly(ps);
			return updatedRows;
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			DBUtility.closeQuietly(ps);
			DBUtility.closeQuietly(rs);
		}
	}
	
	private int setAvailableFields(Map<String, String> availableFields,	UserSecurityBean userBean, int paramsSet, PreparedStatement ps) throws SQLException {
		Set<Entry<String, String>> values = availableFields.entrySet();
		for (Entry<String, String> field : values) {
			ps.setString(paramsSet++, field.getValue());
		}
		return paramsSet;
	}

	private String addMarksForInsert(Map<String, String> availableFields, boolean addCommaToEnd) {
		String str=" ";
		boolean first=true;
		Set<Entry<String, String>> values = availableFields.entrySet();
		for (Entry<String, String> field : values) {
			field.toString(); //Eclipse warns me that the variable is never used
			String comma=",";
			if(first){
				comma="";
				first=false;
			}
			
			str+=comma+" ?";
		}
		//Se c'è almeno un campo allora aggiunge la virgola finale
		if(!first && addCommaToEnd){
			str+=", ";
		}
		return str;
	}
	
	private String getAvailableFields(Map<String, String> availableFields, UserSecurityBean userBean, boolean isNew, boolean addCommaToEnd) {
		String str=" ";
		boolean first=true;
		Set<Entry<String, String>> values = availableFields.entrySet();
		String mark="=?";
		if(isNew){
			mark="";
		}
		for (Entry<String, String> field : values) {
			String comma=",";
			if(first){
				comma="";
				first=false;
			}
			
			str+=comma+" "+field.getKey()+mark;
		}
		//Se c'è almeno un campo allora aggiunge la virgola finale
		if(!first && addCommaToEnd){
			str+=", ";
		}
		return str;
	}

	public Boolean checkDamWriteRigths(String userId, BasicEntityBean targetEntity, String damId) throws SQLException {
		
		String sql = "";
		ResultSet usrRs = null;
		PreparedStatement usrPs = null;
		Connection conn = null;
		int totRowNum = 0;
		
		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
				
		try {

			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "SELECT COUNT(*) FROM diga_autorita dc " + 
						"LEFT JOIN utente_autorita uc on dc.id_autorita = uc.id_autorita " +
						"LEFT JOIN utente u on uc.id_utente = u.id " +
						"WHERE u.userid = ? AND dc.NUMERO_ARCHIVIO = ? AND dc.SUB = ? " +
						"AND u.CANCELLATO<>'S' AND uc.CANCELLATO<>'S' AND dc.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "SELECT COUNT(*) FROM diga_concess dc " + 
						"LEFT JOIN utente_concess uc on dc.id_concess = uc.id_concess " +
						"LEFT JOIN utente u on uc.id_utente = u.id " +
						"WHERE u.userid = ? AND dc.NUMERO_ARCHIVIO = ? AND dc.SUB = ? " +
						"AND u.CANCELLATO<>'S' AND uc.CANCELLATO<>'S' AND dc.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "SELECT COUNT(*) FROM diga_gestore dc " + 
						"LEFT JOIN utente_gestore uc on dc.id_gestore = uc.id_gestore " +
						"LEFT JOIN utente u on uc.id_utente = u.id " +
						"WHERE u.userid = ? AND dc.NUMERO_ARCHIVIO = ? AND dc.SUB = ? " +
						"AND u.CANCELLATO<>'S' AND uc.CANCELLATO<>'S' AND dc.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "SELECT COUNT(*) FROM diga_altra_ente dc " + 
						"LEFT JOIN utente_altra_ente uc on dc.id_ente = uc.id_ente " +
						"LEFT JOIN utente u on uc.id_utente = u.id " +
						"WHERE u.userid = ? AND dc.NUMERO_ARCHIVIO = ? AND dc.SUB = ? " +
						"AND u.CANCELLATO<>'S' AND uc.CANCELLATO<>'S' AND dc.CANCELLATO<>'S' ";
			}
			
			//preparing statement
			conn=dbUtil.getNewConnection();
			usrPs = conn.prepareStatement(sql);
			usrPs.setString(1, userId);
			usrPs.setInt(2, numeroArchivio);
			usrPs.setString(3, sub);
			//execute
			usrRs = usrPs.executeQuery();
			
			//retrieving count
			if (usrRs.next()) {
				totRowNum = usrRs.getInt(1);
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, usrRs, usrPs);
		}
		
		return (totRowNum > 0);
	}
	

	public ArrayList<String> getUserWriteRigthsForEntity(String userId, BasicEntityBean targetEntity) throws SQLException {
		
		String sql = "";
		ResultSet usrRs = null;
		PreparedStatement usrPs = null;
		Connection conn=null;
		ArrayList<String> listId = null;
				
		try {

			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "SELECT UC.ID_AUTORITA AS ID " + 
						"FROM UTENTE_AUTORITA UC " +
						"LEFT JOIN UTENTE U ON UC.ID_UTENTE = U.ID " +
						"WHERE U.USERID = ? AND UC.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "SELECT UC.ID_CONCESS AS ID " + 
						"FROM UTENTE_CONCESS UC " +
						"LEFT JOIN UTENTE U ON UC.ID_UTENTE = U.ID " +
						"WHERE U.USERID = ? AND UC.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "SELECT UC.ID_GESTORE AS ID " + 
						"FROM UTENTE_GESTORE UC " +
						"LEFT JOIN UTENTE U ON UC.ID_UTENTE = U.ID " +
						"WHERE U.USERID = ? AND UC.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "SELECT UC.ID_ENTE AS ID " + 
						"FROM UTENTE_ALTRA_ENTE UC " +
						"LEFT JOIN UTENTE U ON UC.ID_UTENTE = U.ID " +
						"WHERE U.USERID = ? AND UC.CANCELLATO<>'S' ";
			}
			
			//preparing statement
			conn=dbUtil.getNewConnection();
			usrPs = conn.prepareStatement(sql);
			usrPs.setString(1, userId);
			//execute
			usrRs = usrPs.executeQuery();

			listId = new ArrayList<String>();
			
			while (usrRs.next()) {
				listId.add(usrRs.getString(1));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, usrRs, usrPs);
		}
		
		return listId;
	}

	public ArrayList<String> getUserAccessRigthsForDam(String userId, String damId, String schedaId) throws SQLException {
		
		String sql = "";
		String resultTipo = "";
		String resultVisibile = "";
		ResultSet usrRs = null;
		PreparedStatement usrPs = null;
		Connection conn=null;
		ArrayList<String> listId = null;
				
		try {
			
			
			//extract numeroarchivio and sub from iddiga
			Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
			String sub = damId.substring(damId.length()-1, damId.length());

			sql = "SELECT SPD.TIPO, SPD.VISIBILE FROM UTENTE U " +
				"JOIN UTENTE_GRUPPO UG ON UG.ID_UTENTE = U.ID " + 
				"JOIN SCHEDA_PRIVILEGI_DEFAULT SPD ON SPD.ID_GRUPPO = UG.ID_GRUPPO " +
				"JOIN ACCESSO_DEFAULT AD ON AD.ID_GRUPPO = UG.ID_GRUPPO " +
				"WHERE U.USERID = ? AND AD.NUMERO_ARCHIVIO = ? AND AD.SUB = ? AND SPD.ID_SCHEDA = ? "; 
			
			//preparing statement
			conn=dbUtil.getNewConnection();
			usrPs = conn.prepareStatement(sql);
			usrPs.setString(1, userId);
			usrPs.setInt(2, numeroArchivio);
			usrPs.setString(3, sub);
			usrPs.setString(4, schedaId);
			//execute
			usrRs = usrPs.executeQuery();

			listId = new ArrayList<String>();
			
			while (usrRs.next()) {
				String tmpTipo = usrRs.getString("TIPO");
				
				if ("R".equals(tmpTipo) || "W".equals(tmpTipo)) {
					resultTipo = usrRs.getString("TIPO");
					resultVisibile = usrRs.getString("VISIBILE");
					if ("W".equals(tmpTipo)) break;
				}
			}
			
			if ("".equals(resultTipo) && "".equals(resultVisibile)) {
				resultTipo = "N";
				resultVisibile = "N";
			}
			
			listId.add(resultTipo);
			listId.add(resultVisibile);
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, usrRs, usrPs);
		}
		
		return listId;
	}
	
}
