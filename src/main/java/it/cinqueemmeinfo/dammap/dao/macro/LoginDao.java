package it.cinqueemmeinfo.dammap.dao.macro;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import it.cinqueemmeinfo.dammap.bean.security.RoleBean;
import it.cinqueemmeinfo.dammap.bean.security.UserDetailsBean;
import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.exception.DBConnectionException;
import it.cinqueemmeinfo.dammap.security.UnauthorizedException;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.InvalidPasswordException;
import it.cinqueemmeinfo.dammap.utility.exceptions.InvalidUserException;

public class LoginDao implements Serializable {

	private static final long serialVersionUID = -7173028155602764591L;
	private static final Logger logger = LoggerFactory.getLogger(LoginDao.class);
	private static final String ROLE_USER = "ROLE_USER";
	private static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String EXPIRES_LABEL="PWD_EXPIRE";
	public static final String WARNING_DAYS_LABEL="WARNING_DAYS";
	private static final int BCRYPT_STR = 13;
	
	@Autowired private DBUtility dbUtil;
	@Autowired UserUtility usrUtil;
	
	public List<UserSecurityBean> retrieveAllUsers() {
		
		String sql = "";
		List<UserSecurityBean> users = new ArrayList<UserSecurityBean>();
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		
		try {
			sql = "SELECT a.ID AS ID, a.USERID AS USERID, a.PASSWORD AS PASSWORD, a.LOGIN_ABILITATO AS LOGIN_ABILITATO FROM UTENTE a "
					+ "LEFT JOIN UTENTE_EXTRA b ON a.ID=b.ID_UTENTE ";

			conn=dbUtil.getNewConnection();
			ps = conn.prepareStatement(sql);
			
			rs = ps.executeQuery();
			
			while(rs.next()) {
				users.add(fillUserBean(rs, false));
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
		} finally {
        	dbUtil.closeQuietly(conn, rs, ps);
        }

		return users;
	}
	
public UserSecurityBean findByIdWithDetails(Integer userId, boolean getOffice) throws SQLException {
		
		String sql = "";
		UserSecurityBean usBean = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		
		try {
			sql = "SELECT a.ID AS ID, a.PREFERENZE, a.USERID AS USERID, b.ID AS userExtraId, b.PASSWORD AS PASSWORD, "
					+ " a.LOGIN_ABILITATO AS LOGIN_ABILITATO, a.NOME AS nome, a.COGNOME AS cognome, a.DESCR AS descrizione, "
					+ " a.TEL_UFFICIO as telUfficio, a.TEL_CELLULARE as telCellulare, a.TEL_CASA AS altriRecapiti, a.TIPO as ruolo, "
					+ " b.LAST_PWD_UPDATE AS lastPwdUpdate, b.PWD_EXPIRE AS pwdExpireDate, a.CATEGORIA AS categoria "
					+ " FROM UTENTE a "
					+ " LEFT JOIN UTENTE_EXTRA b ON a.ID=b.ID_UTENTE "
					+ " WHERE a.ID = ? AND CANCELLATO<>'S' ";

			conn=dbUtil.getNewConnection();
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userId);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				usBean = this.fillUserBean(rs, true);
			}
			dbUtil.closeQuietly(ps);
			dbUtil.closeQuietly(rs);
			//Aggiunge gli uffici
			if(getOffice){
				sql="SELECT ID_UFFICIO as ufficio FROM UTENTE_UFFICIO WHERE ID_FUNZIONARIO=? ";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, userId);
				
				rs = ps.executeQuery();
				Set<String> uffici=usBean.getUserDetails().getUffici();
				while(rs.next()) {
					uffici.add(rs.getString("ufficio"));
				}
			}
			
//			dbUtil.closeQuietly(ps);
//			dbUtil.closeQuietly(rs);
//			//Aggiunge la data di scadenza
//			sql="SELECT PWD_EXPIRE AS pwdExpire, WARNING_DAYS AS warningDays FROM OPZIONI_APPLICAZIONE";
//			ps = conn.prepareStatement(sql);
//			
//			rs = ps.executeQuery();
//			if(rs.next()) {
//				usBean.getUserDetails().setPwdExpire(rs.getInt("pwdExpire"));
//				usBean.getUserDetails().setWarningDays(rs.getInt("warningDays"));
//			}
			Map<String, Integer> expireDates = this.getExpireDates();
			usBean.getUserDetails().setPwdExpire(expireDates.get(LoginDao.EXPIRES_LABEL));
			usBean.getUserDetails().setWarningDays(expireDates.get(LoginDao.WARNING_DAYS_LABEL));
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
		} finally {
        	dbUtil.closeQuietly(conn, rs, ps);
        }

		return usBean;
	}
	
	public UserSecurityBean findByUsername(String userName, boolean checkExpired) throws SQLException {
		
		String sql = "";
		UserSecurityBean usBean = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		//checkExpired=false;
		try {
			sql = "SELECT a.ID AS ID, a.PREFERENZE, a.USERID AS USERID, b.ID AS userExtraId, a.PASSWORD AS oldPwd, "
					+ " b.PASSWORD AS PASSWORD, a.LOGIN_ABILITATO AS LOGIN_ABILITATO, "
					+ " b.LAST_PWD_UPDATE AS lastPwdUpdate, b.PWD_EXPIRE AS pwdExpireDate "
					+ " FROM UTENTE a "
					+ " LEFT JOIN UTENTE_EXTRA b ON a.ID=b.ID_UTENTE "
					+ " WHERE a.USERID = ? AND CANCELLATO<>'S' ";

			conn=dbUtil.getNewConnection();
			if(conn == null) throw new DBConnectionException("Connessione al database non riuscita");
			ps = conn.prepareStatement(sql);
			ps.setString(1, userName);
			
			rs = ps.executeQuery();
			String oldPwd="";
			long userExtraId=0;
			if (rs.next()) {
				usBean = this.fillUserBean(rs, false);
				oldPwd=rs.getString("oldPwd");
				userExtraId=rs.getLong("userExtraId");
			}
			dbUtil.closeQuietly(ps);
			dbUtil.closeQuietly(rs);
			//Se la password recuperata è vuota allora bisogna criptarla ed inserirla nel database
			if(usBean!=null && (usBean.getPassword()==null || usBean.getPassword().trim().equals(""))){
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(BCRYPT_STR);
				logger.info("hashing password for user: "+usBean.getUsername());
				String hashedPassword = passwordEncoder.encode(oldPwd);
				sql="UPDATE UTENTE_EXTRA SET PASSWORD = ? WHERE ID = ?";
				int ip=1;
				ps = conn.prepareStatement(sql);
				ps.setString(ip++, hashedPassword);
				ps.setLong(ip++, userExtraId);
				ps.executeUpdate();
				usBean.setPassword(hashedPassword);
				ps.close();
			}
			
			if (usBean != null) {
				//recupero ruoli
				usBean.setRole(this.retrieveUserRoles(usBean.getId()));
				if(checkExpired){
					//Controllo scadenza password
					Map<String, Integer> expireDates=this.getExpireDates();
					//Setta il campo password scaduta per far partire l'avviso a seconda dei giorni
					usBean.setPwdExpired(this.checkPwdExpiredOffset(-expireDates.get(LoginDao.WARNING_DAYS_LABEL), usBean.getPwdExpireDate()));
					//Se invece la password è realmente scaduta lancia un'eccezione e non fa loggare l'utente
					if(this.checkPwdExpired(usBean.getPwdExpireDate())){
						throw new UnauthorizedException("Password Scaduta");
					}
				}
				
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
			throw sqle;
		} finally {
        	dbUtil.closeQuietly(conn, rs, ps);
        }

		return usBean;
	}
	
	public Map<String, Integer> getExpireDates() throws SQLException {
		Map<String, Integer> expireDates=new HashMap<String, Integer>();
		try {
			int expire=Integer.valueOf(this.getOptionValue(LoginDao.EXPIRES_LABEL));
			int warningDays=Integer.valueOf(this.getOptionValue(LoginDao.WARNING_DAYS_LABEL));
			expireDates.put(LoginDao.EXPIRES_LABEL, expire);
			expireDates.put(LoginDao.WARNING_DAYS_LABEL, warningDays);
		} catch (SQLException sqle) {
			logger.error("Error while executing Query \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
			throw sqle;
		}
		return expireDates;
	}
	
	public String getOptionValue(String keyName) throws SQLException {
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		String keyValue="";
		try {
			conn=dbUtil.getNewConnection();
			sql="SELECT KEY_VALUE FROM OPZIONI_APPLICAZIONE WHERE KEY_NAME=?";
			int ip=1;
			ps = conn.prepareStatement(sql);
			ps.setString(ip++, keyName);
			rs = ps.executeQuery();
			if(rs.next()){
				keyValue=rs.getString("KEY_VALUE");
			}
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
			throw sqle;
		} finally {
        	dbUtil.closeQuietly(conn, rs, ps);
        }
		return keyValue;
	}
	
//	public Map<String, Integer> getExpireDates() throws SQLException {
//		String sql = "";
//		ResultSet rs = null;
//		PreparedStatement ps = null;
//		Connection conn=null;
//		Map<String, Integer> expireDates=new HashMap<String, Integer>();
//		try {
//			conn=dbUtil.getNewConnection();
//			sql="SELECT PWD_EXPIRE, WARNING_DAYS FROM OPZIONI_APPLICAZIONE";
//			//int ip=1;
//			ps = conn.prepareStatement(sql);
//			//ps.setLong(ip++, 1);
//			rs = ps.executeQuery();
//			if(rs.next()){
//				/*
//				Prende la data attuale e sottrae i giorni, dopodichè controlla
//				Che la data di ultima modifica password sia inferiore alla 
//				data attuale tolti i giorni, se lo è la password è scaduta
//				*/
//				int expire=rs.getInt("PWD_EXPIRE");
//				int warningDays=rs.getInt("WARNING_DAYS");
//				expireDates.put(LoginDao.EXPIRES_LABEL, expire);
//				expireDates.put(LoginDao.WARNING_DAYS_LABEL, warningDays);
//			}
//		} catch (SQLException sqle) {
//			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
//			sqle.printStackTrace();
//			throw sqle;
//		} finally {
//        	dbUtil.closeQuietly(conn, rs, ps);
//        }
//		return expireDates;
//	}
	
	//Controlla la scadenza della password con un offset
	private boolean checkPwdExpiredOffset(int offset, Timestamp expireDate){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(expireDate.getTime()));
		cal.add(Calendar.DATE, offset);
		Timestamp expireDateOffset = new Timestamp(cal.getTimeInMillis());
//		if(dateToCheck!=null && dateToCheck.compareTo(pwdCheck)<0){
//			return true;
//		}
//		return false;
		return checkPwdExpired(expireDateOffset);
	}
	
	private boolean checkPwdExpired(Timestamp expireDate){
		Timestamp now=new Timestamp(System.currentTimeMillis());
		if(expireDate!=null && now.compareTo(expireDate)>0){
			return true;
		}
		return false;
	}
	
	private UserSecurityBean fillUserBean(ResultSet rs, boolean addDetails) throws SQLException{
		UserSecurityBean usBean = new UserSecurityBean();
		usBean.setId(rs.getInt("ID"));
		usBean.setUsername(rs.getString("USERID"));
		usBean.setPassword(rs.getString("PASSWORD"));
		usBean.setExpires(0);
		usBean.setLastPwdUpdate(rs.getTimestamp("lastPwdUpdate"));
		usBean.setPwdExpireDate(rs.getTimestamp("pwdExpireDate"));
		usBean.setActive("S".equals(rs.getString("LOGIN_ABILITATO")));
		usBean.setPreferenze(rs.getString("PREFERENZE"));
		if(addDetails){
			UserDetailsBean details=new UserDetailsBean();
			details.setNome(rs.getString("nome"));
			details.setCognome(rs.getString("cognome"));
			details.setRuolo(rs.getString("ruolo"));
			details.setDescrizione(rs.getString("descrizione"));
			details.setTelUfficio(rs.getString("telUfficio"));
			details.setTelCellulare(rs.getString("telCellulare"));
			details.setAltriRecapiti(rs.getString("altriRecapiti"));
			details.setCategoria(rs.getString("categoria"));
			usBean.setUserDetails(details);
		}
		
		return usBean;
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public String getUserDefaultDam(String userId) throws SQLException {
		
		String sql = "";
		String damId = "";
		ResultSet rs = null;
		Connection conn=null;
		PreparedStatement ps = null;
		
		try {
			sql = "select NUMERO_ARCHIVIO, SUB from ACCESSO_DEFAULT where ID_GRUPPO in ("
				+ "select UG.ID_GRUPPO from UTENTE_GRUPPO UG left join UTENTE U on UG.ID_UTENTE = U.ID where U.USERID = ? ) and ROWNUM=1" ;
			conn=dbUtil.getNewConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, userId);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				damId = rs.getString("NUMERO_ARCHIVIO") + rs.getString("SUB");
			}
			
			if (damId == null || "".equals(damId)) {
				throw new SQLException("Couldn't find the default Dam for userid: " + userId);
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
		} finally {
        	dbUtil.closeQuietly(conn, rs, ps);
        }
		
		return damId;
	}
	
	/**
	 * 
	 * @param userId
	 * @param password
	 * @return
	 * @throws InvalidUserException
	 * @throws InvalidPasswordException
	 */
	public Boolean checkUserCredentials(String userId, String password) throws InvalidUserException, InvalidPasswordException {

		String sql = "";
		String dbName = "";
		String dbPassword = "";
		String loginEnabled = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		
		try {
			sql = "SELECT PASSWORD, LOGIN_ABILITATO, CONCAT(COGNOME, CONCAT(' ', NOME)) AS NOME FROM UTENTE WHERE USERID = ? AND CANCELLATO<>'S' ";
			conn=dbUtil.getNewConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, userId);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				dbPassword = rs.getString("PASSWORD");
				loginEnabled = rs.getString("LOGIN_ABILITATO");
				dbName = rs.getString("NOME");
			}
			
			if (dbPassword == null || "".equals(dbPassword)) {
				throw new InvalidUserException(userId + " does not exists.");
			} 
			
			if (!"S".equals(loginEnabled)) {
				throw new InvalidUserException("Login for " + userId + " is not enabled.");
			}
			
			if (password.toUpperCase().equals(dbPassword)) {
				//remember to keep track of userIP and register it into the database
				//check old app LoginCache to see the steps
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			//throw sqle;
			sqle.printStackTrace();
		} finally {
        	dbUtil.closeQuietly(conn, rs, ps);
        }
		
		return (password.toUpperCase().equals(dbPassword));
	}
	
	 /**
     * Creates old UserRights Object
     * @param userId
     * @return
     * @throws SQLException
     */
    public Set<RoleBean> retrieveUserRoles(Integer userId) throws SQLException {

        String sql = "";
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection conn = null;
        Set<RoleBean> roles=new HashSet<RoleBean>();

        try {

            if (userId == 0) {
                //per ing Chiarolla carica tutti i permessi, gli da visibilita 1 (visibile) e li imposta a 1000 (write)
                //TODO valuta se mantenere questa procedura in futuro
            	
                sql = "SELECT ID AS ID_SCHEDA, 1 AS VISIBILE, 1000 AS TIPO FROM SCHEDA WHERE ID_PADRE IS NOT NULL ";
                
                roles.add(new RoleBean(0, ROLE_ADMIN));
            } else {
                //per gli altri utenti carica tutti i permessi associati
                sql = "SELECT ID_SCHEDA, SUM(VISIBILE) AS VISIBILE, SUM(TIPO) AS TIPO " +
                        "FROM (SELECT ID_SCHEDA, DECODE(VISIBILE, 'S', 1, 0) AS VISIBILE , DECODE(TIPO, 'W', 1000, 'R', 1, 0) AS TIPO " +
                        "FROM ( SELECT * FROM VISTA_PRIVILEGI_UTENTE WHERE ID_UTENTE=?) ) GROUP BY (ID_SCHEDA) ";
                
            }
            //Tutti hanno il ruolo "USER"
            roles.add(new RoleBean(roles.size(), ROLE_USER));
            conn = dbUtil.getNewConnection();
            ps = conn.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            
            int i=1;
            while (rs.next()) {
                int id_scheda = rs.getInt("ID_SCHEDA");
                char visibile = rs.getInt("VISIBILE") > 0 ? 'S' : 'N';
                int t = rs.getInt("TIPO");
                char tipo ='N'; // t > 0 ? 'R' : t >= 1000 ? 'W' : 'N';
                
                if(t>=1000){
                    tipo='W';
                } else if(t>0) {
                    tipo='R';
                }

                String role=id_scheda+"-"+visibile+"-"+tipo;
                roles.add(new RoleBean(i, role));
                i++;
            }
            
            //TODO valutare se riempire anche il set dei gruppi usando ID_GRUPPO

        } catch (SQLException sqle) {
            logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
            sqle.printStackTrace();
        } finally {
        	dbUtil.closeQuietly(conn, rs, ps);
        }

        return roles;
    }
    
	/*public UserRightsEntry createUserRightsEntry(String userId) throws SQLException {
		
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		UserRightsEntry urBean = null;
		
		try {
			sql = "SELECT U.ID, CONCAT(U.COGNOME,CONCAT(' ', U.NOME)) AS NOME, U.PREFERENZE, UG.ID_GRUPPO " +
					"FROM UTENTE U LEFT JOIN UTENTE_GRUPPO UG ON U.ID=UG.ID_UTENTE WHERE U.USERID=? ";

			ps = dbUtil.getConnection().prepareStatement(sql);
			ps.setString(1, userId);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				urBean = new UserRightsEntry(userId, Boolean.TRUE);
				
				urBean.setId(rs.getInt("ID"));
				urBean.setName(rs.getString("NOME"));
				urBean.setPreferences(rs.getString("PREFERENZE"));
				urBean.setAdministrator((rs.getInt("ID_GRUPPO") == 0)); //id_gruppo == 0 ? user == admin
			}
			
			if (urBean.getId() == 0) {
				//per ing Chiarolla carica tutti i permessi, gli da visibilita 1 (visibile) e li imposta a 1000 (write)
				//TODO valuta se mantenere questa procedura in futuro
				sql = "SELECT ID AS ID_SCHEDA, 1 AS VISIBILE, 1000 AS TIPO FROM SCHEDA WHERE ID_PADRE IS NOT NULL ";
			} else {
				//per gli altri utenti carica tutti i permessi associati
				sql = "SELECT ID_SCHEDA, SUM(VISIBILE) AS VISIBILE, SUM(TIPO) AS TIPO " + 
						"FROM (SELECT ID_SCHEDA, DECODE(VISIBILE, 'S', 1, 0) AS VISIBILE , DECODE(TIPO, 'W', 1000, 'R', 1, 0) AS TIPO " + 
						"FROM ( SELECT * FROM VISTA_PRIVILEGI_UTENTE WHERE ID_UTENTE=?) ) GROUP BY (ID_SCHEDA) ";
			}

			ps = dbUtil.getConnection().prepareStatement(sql);
			ps.setString(1, userId);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				int id_scheda = rs.getInt("ID_SCHEDA");
				char visibile = rs.getInt("VISIBILE") > 0 ? 'S' : 'N';
				int t = rs.getInt("TIPO");
				char tipo = t > 0 ? 'R' : t >= 1000 ? 'W' : 'N';
			
				urBean.setNode(id_scheda, visibile, tipo);
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		}
		
		return urBean;
	}*/
	
	/*
		-login user
		select UTENTE.ID, concat(UTENTE.COGNOME,concat(' ',UTENTE.NOME)) as NOME, UTENTE.PREFERENZE, UTENTE_GRUPPO.ID_GRUPPO 
		from UTENTE left join UTENTE_GRUPPO on UTENTE.ID=UTENTE_GRUPPO.ID_UTENTE where UTENTE.USERID=?+7***+*+*****
		
		    userRightsEntry.setId(res.getInt("ID"));
		    userRightsEntry.setName(res.getString("NOME"));
		    userRightsEntry.setPreferences(res.getString("PREFERENZE"));
		    userRightsEntry.setAdministrator((res.getInt("ID_GRUPPO")==0)); //if id_gruppo == 0, user == admin
		
		-check permessi scheda admin
		id_utente == 0 ?
		select ID as id_scheda, 1 as VISIBILE, 1000 as TIPO from SCHEDA where id_padre is not null 
		
		carica tutti i permessi, gli da visibilita 1 (visibile) e li imposta a 1000 (write)
		
		-check permessi scheda utente regolare
		id_utente <> 0 ?
		select id_scheda, sum(visibile) as visibile, sum(tipo) as tipo 
		from (select id_scheda, decode(visibile, 'S', 1, 0) as visibile , decode(tipo, 'W', 1000, 'R', 1, 0) as tipo 
				from ( select * from vista_privilegi_utente where id_utente=?) )
		group by (id_scheda) 
		
		--creazione nodi utente
			while (res.next()) {}
			int id_scheda = res.getInt("id_scheda");
			char visibile = res.getInt("visibile") > 0 ? 'S' : 'N';
			int t = res.getInt("tipo");
			char tipo = t > 0 ? 'R' : t >= 1000 ? 'W' : 'N';
		
			userRightsEntry.setNode(id_scheda, visibile, tipo);
	*/
}
