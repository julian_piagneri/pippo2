package it.cinqueemmeinfo.dammap.dao.macro;

import it.cinqueemmeinfo.dammap.bean.entities.AltreVociRubricaBean;
import it.cinqueemmeinfo.dammap.bean.entities.CasaDiGuardiaBean;
import it.cinqueemmeinfo.dammap.bean.entities.ContactBean;
import it.cinqueemmeinfo.dammap.bean.entities.FunzionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.UfficioBean;
import it.cinqueemmeinfo.dammap.bean.junction.NominaIngegnereBean;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ContactsDao implements Serializable {

	private static final long serialVersionUID = -1032201955028357389L;
	private static final Logger logger = LoggerFactory.getLogger(ContactsDao.class);
	
	@Autowired private DBUtility dbUtil;
	@Autowired private BeanUtility bnUtil;

	public ContactBean getContactFromDamId(String damId, int hasAccess) throws SQLException {

		//------------------- QUERY STRING --------------------
		
		//SELECT ID_INGEGNERE_RESPONSABILE
		String sqlIngResp = "select PROGRESSIVO, ID_INGEGNERE, nvl(to_char(DATA_NOMINA,'dd/mm/yyyy'), '') as DATA_NOMINA, DATA_MODIFICA, ID_UTENTE, CANCELLATO  "
							+ "from INGEGNERE_RESPONSABILE "  
							+ "where NUMERO_ARCHIVIO = ? AND SUB = ? AND CANCELLATO<>'S' "
							+ "order by PROGRESSIVO DESC ";
		
		//SELECT ID_INGEGNERE_SOSTITUTO
		String sqlIngSost = "select PROGRESSIVO, ID_INGEGNERE, nvl(to_char(DATA_NOMINA,'dd/mm/yyyy'), '') as DATA_NOMINA, DATA_MODIFICA, ID_UTENTE, CANCELLATO  "
							+ "from INGEGNERE_SOSTITUTO "  
							+ "where NUMERO_ARCHIVIO = ? AND SUB = ? AND CANCELLATO<>'S' "
							+ "order by PROGRESSIVO DESC ";
		
		String sqlConc = "SELECT ID_CONCESS FROM DIGA_CONCESS DC LEFT JOIN CONCESSIONARIO C ON DC.ID_CONCESS = C.ID " +
							"WHERE DC.NUMERO_ARCHIVIO = ? AND DC.SUB = ? AND DC.CANCELLATO<>'S' AND C.CANCELLATO<>'S' ";
		
		String sqlGest = "SELECT DG.ID_GESTORE FROM DIGA_GESTORE DG LEFT JOIN GESTORE G ON DG.ID_GESTORE = G.ID " +
							"WHERE DG.NUMERO_ARCHIVIO = ? AND DG.SUB = ? AND DG.CANCELLATO<>'S' AND G.CANCELLATO<>'S' ";
		
		String sqlAuto = "SELECT DA.ID_AUTORITA FROM DIGA_AUTORITA DA LEFT JOIN AUTORITA_PROTEZIONE_CIVILE A ON DA.ID_AUTORITA = A.ID " +
							"WHERE DA.NUMERO_ARCHIVIO = ? AND DA.SUB = ? AND DA.CANCELLATO<>'S' AND A.CANCELLATO<>'S' ";
		
		String sqlEnte = "SELECT DE.ID_ENTE FROM DIGA_ALTRA_ENTE DE LEFT JOIN ALTRA_ENTE E ON DE.ID_ENTE = E.ID " +
							"WHERE DE.NUMERO_ARCHIVIO = ? AND DE.SUB = ? AND DE.CANCELLATO<>'S' AND E.CANCELLATO<>'S' ";
		
		//-----------------------------------------------------
		
		ContactBean coBean = getDamExtraData(damId); //DATI EXTRA DIGA
		coBean = getOfficePhoneNumbersFromIdDiga(damId, coBean); //NUMERI TELEFONO EXTRA + DATA EMERGENZA
		coBean.setUffCoord(getUfficioCoordFromIdDiga(damId)); //UFFICIO_COORD
		coBean.setUffPerif(getUfficioPerifFromIdDiga(damId)); //UFFICIO_PERIF
		coBean.setAltreVociRubrica(getAltreVociRubricaList(damId, new AltreVociRubricaBean())); //ALTRE VOCI
		coBean.setCasaDiGuarda(getCasaDiGuardiaFromIdDiga(damId, true)); //CASA DI GUARDIA
		coBean.setIdConcess(getEntityJunctionIdListFromIdDiga(damId, sqlConc)); //CONCESSIONARIO
		coBean.setIdGestore(getEntityJunctionIdListFromIdDiga(damId, sqlGest)); //GESTORE
		coBean.setIdAutorita(getEntityJunctionIdListFromIdDiga(damId, sqlAuto)); //AUTORITA
		coBean.setIdAltraEnte(getEntityJunctionIdListFromIdDiga(damId, sqlEnte)); //ALTRA ENTE
		coBean.setIdIngResp(getNominaIngegnereListFromIdDiga(damId, sqlIngResp)); //INGEGNERE RESPONSABILE
		coBean.setIdIngSost(getNominaIngegnereListFromIdDiga(damId, sqlIngSost)); //INGEGNERE SOSTITUTO

		return coBean;
	}
	
	/*	
	
	select d.NUMERO_ARCHIVIO, d.SUB, d.NOME_FIUME_SBARRATO, d.NOME_BACINO, d.DESCRIZIONE_CLASSIFICA_DIGA
		, to_char(d.VOLUME_TOTALE_INVASO_L584,'9G990D99') as VOLUME_TOTALE_INVASO_L584, to_char(d.QUOTA_MAX,'9G990D99') as QUOTA_MAX
		, to_char(d.QUOTA_MAX_REGOLAZIONE,'9G990D99') as QUOTA_MAX_REGOLAZIONE, to_char(d.VOLUME_AUTORIZZATO,'9G990D99') as VOLUME_AUTORIZZATO
		, to_char(d.QUOTA_AUTORIZZATA,'9G990D99') as QUOTA_AUTORIZZATA, to_char(d.PORTATA_MAX_PIENA_PROG,'9G990D99') as PORTATA_MAX_PIENA_PROG
		, d.DATA_CERTIFICATO_COLLAUDO, concat(dc.NOMI_COMUNI, concat(' (',concat(p.NOME,')'))) as NOME_COMUNE 
	from V_ELENCO_DIGHE_SINGOLI d
	left join vat_diga_comuni_prov_regioni dc on d.NUMERO_ARCHIVIO = dc.NUMERO_ARCHIVIO and d.SUB = dc.SUB
	left join provincia p on dc.SIGLE_PROVINCE = p.SIGLA and dc.NOMI_REGIONI = p.NOME_REGIONE
	where d.NUMERO_ARCHIVIO=2 and d.SUB='_'
	  

	--VISTA DIGHE FILTRATA PER ID_USER 
	select V_ELENCO_DIGHE_SINGOLI.* 
	from V_ELENCO_DIGHE_SINGOLI 
	INNER JOIN  (select distinct NUMERO_ARCHIVIO,SUB 
	              from ACCESSO_DEFAULT 
	              where ID_GRUPPO in (select ID_GRUPPO 
	                                  from UTENTE_GRUPPO 
	                                  where ID_UTENTE =1 ) ) AD on V_ELENCO_DIGHE_SINGOLI.NUMERO_ARCHIVIO=AD.NUMERO_ARCHIVIO and V_ELENCO_DIGHE_SINGOLI.SUB=AD.SUB 
	--QUERY DIGA 
		select NUMERO_ARCHIVIO, SUB, NOME_FIUME_SBARRATO, NOME_BACINO, DESCRIZIONE_CLASSIFICA_DIGA
			, to_char(VOLUME_TOTALE_INVASO_L584,'9G990D99') as VOLUME_TOTALE_INVASO_L584, to_char(QUOTA_MAX,'9G990D99') as QUOTA_MAX
			, to_char(QUOTA_MAX_REGOLAZIONE,'9G990D99') as QUOTA_MAX_REGOLAZIONE, to_char(VOLUME_AUTORIZZATO,'9G990D99') as VOLUME_AUTORIZZATO
			, to_char(QUOTA_AUTORIZZATA,'9G990D99') as QUOTA_AUTORIZZATA, to_char(PORTATA_MAX_PIENA_PROG,'9G990D99') as PORTATA_MAX_PIENA_PROG
			, DATA_CERTIFICATO_COLLAUDO 
		from V_ELENCO_DIGHE_SINGOLI 
		where NUMERO_ARCHIVIO=2 and SUB='_'

	--QUERY NOME REGIONE
		select concat(C.NOME_COMUNE,concat(' (',concat(P.NOME,')'))) as NOME_COMUNE 
		from (select C.NOME as NOME_COMUNE,C.SIGLA_PROVINCIA,C.NOME_REGIONE 
				from  (select COD_ISTAT_COMUNE,SIGLA_PROVINCIA,NOME_REGIONE FROM DIGA_COMUNE   
						where CANCELLATO<>'S' and NUMERO_ARCHIVIO=2 and SUB='_') DC 
		        left join COMUNE C on DC.COD_ISTAT_COMUNE=C.COD_ISTAT_COMUNE and DC.SIGLA_PROVINCIA=C.SIGLA_PROVINCIA and DC.NOME_REGIONE=C.NOME_REGIONE) C 
		left join PROVINCIA P on C.SIGLA_PROVINCIA=P.SIGLA and C.NOME_REGIONE=P.NOME_REGIONE
								
	--QUERY FIUMI DIGA						
		select FIUME.NOME as FIUME_VALLE, FV.ORDINE 
		from (select ID_FIUME, ORDINE from FIUME_VALLE 
				where CANCELLATO<>'S' and NUMERO_ARCHIVIO=2 and SUB='_') FV
		left join FIUME on FV.ID_FIUME=FIUME.ID
		order by ORDINE desc
								
	--QUERY UFFICIO COORD
		select ID as ID_UFF_COORD,TIPO as TIPO_UFF_COORD,DESCRIZIONE as DESCR_UFF_COORD,INDIRIZZO as INDIRIZZO_UFF_COORD
		, TELEFONO as TELEFONO_UFF_COORD,FAX as FAX_UFF_COORD, ID_FUNZ_DIRIGENTE as ID_DIRIGENTE_COORD 
		from UFFICIO 
		where ID=(select ID_UFF_COORD from DIGA where CANCELLATO<>'S' and NUMERO_ARCHIVIO=2 and SUB='_')
	--QUERY FUNZIONARIO UFFICIO COORD
		select ID, concat(concat(COGNOME,' '),NOME) as NOME_FUNZ, TEL_UFFICIO, TEL_CELLULARE, TEL_CASA  
		from UTENTE 
		where ID=(select ID_FUNZ_SEDE_CENTRALE from DIGA where CANCELLATO<>'S' and NUMERO_ARCHIVIO=2 and SUB='_')
	--QUERY DIRIGENTE UFFICIO COORD
		select ID,concat(concat(COGNOME,' '),NOME) as NOME_DIR, TEL_UFFICIO, TEL_CELLULARE, TEL_CASA  
		from UTENTE where ID= ID_DIRIGENTE_COORD (da prima query)

	--QUERY UFFICIO PERIFERICO
		select ID as ID_UFF_PERIF,TIPO as TIPO_UFF_PERIF,DESCRIZIONE as DESCR_UFF_PERIF,INDIRIZZO as INDIRIZZO_UFF_PERIF
		, TELEFONO as TELEFONO_UFF_PERIF,FAX as FAX_UFF_PERIF, ID_FUNZ_DIRIGENTE as ID_DIRIGENTE_PERIF 
		from UFFICIO where ID=(select ID_UFF_PERIF from DIGA where CANCELLATO<>'S' and NUMERO_ARCHIVIO=2 and SUB='_')
	--QUERY FUNZIONARIO UFFICIO PERIFERICO
		select ID,concat(concat(COGNOME,' '),NOME) as NOME_FUNZ, TEL_UFFICIO, TEL_CELLULARE, TEL_CASA 
		from UTENTE 
		where ID=(select ID_FUNZ_UFF_PERIF from DIGA where CANCELLATO<>'S' and NUMERO_ARCHIVIO=2 and SUB='_')
	--QUERY DIRIGENTE UFFICIO COORD
		select ID,concat(concat(COGNOME,' '),NOME) as NOME_DIR, TEL_UFFICIO, TEL_CELLULARE, TEL_CASA  from UTENTE where ID= ID_DIRIGENTE_PERIF (da prima query)
	*/
	

	public ContactBean getDamExtraData(String damId) throws SQLException {
		
		String sql = "";
		ResultSet coRs = null;
		PreparedStatement coPs = null;
		ContactBean coBean = null; 
		
		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
				
		try {
					
			sql = "select d.NUMERO_ARCHIVIO, d.SUB, d.NOME_DIGA, d.NOME_FIUME_SBARRATO, d.NOME_BACINO, d.DESCRIZIONE_CLASSIFICA_DIGA " +
					"	, to_char(d.VOLUME_TOTALE_INVASO_L584,'9G990D99') as VOLUME_TOTALE_INVASO_L584, to_char(d.QUOTA_MAX,'9G990D99') as QUOTA_MAX " +
					"	, to_char(d.QUOTA_MAX_REGOLAZIONE,'9G990D99') as QUOTA_MAX_REGOLAZIONE, to_char(d.VOLUME_AUTORIZZATO,'9G990D99') as VOLUME_AUTORIZZATO " +
					"	, to_char(d.QUOTA_AUTORIZZATA,'9G990D99') as QUOTA_AUTORIZZATA, to_char(d.PORTATA_MAX_PIENA_PROG,'9G990D99') as PORTATA_MAX_PIENA_PROG " +
					"	, d.DATA_CERTIFICATO_COLLAUDO, concat(dc.NOMI_COMUNI, concat(' (',concat(p.NOME,')'))) as NOME_COMUNE, df.NOME_FIUME_VALLE, d.NUM_ISCRIZIONE_RID " +
					"	, concat(concat(concat(concat(status_principale, ' - '), status_secondario), ' ') " +
					"	, concat(concat(descr_status_principale, ' - '), descr_status_secondario)) as STATUS " +
					"from V_ELENCO_DIGHE_SINGOLI d " +
					"left join vat_diga_fiumi_valle df on d.NUMERO_ARCHIVIO = df.NUMERO_ARCHIVIO and d.SUB = df.SUB " +
					"left join vat_diga_comuni_prov_regioni dc on d.NUMERO_ARCHIVIO = dc.NUMERO_ARCHIVIO and d.SUB = dc.SUB " +
					"left join provincia p on dc.SIGLE_PROVINCE = p.SIGLA and dc.NOMI_REGIONI = p.NOME_REGIONE " +
					"where d.NUMERO_ARCHIVIO=? and d.SUB=? ";

			//preparing statement
			coPs = dbUtil.getConnection().prepareStatement(sql);
			coPs.setInt(1, numeroArchivio);
			coPs.setString(2, sub);
			//execute
			coRs = coPs.executeQuery();
			
			coBean = new ContactBean();
	
			//retrieving 
			if (coRs.next()) {
				coBean.setNumeroArchivio(coRs.getString("NUMERO_ARCHIVIO"));
				coBean.setSub(coRs.getString("SUB"));
				coBean.setNomeDiga(coRs.getString("NOME_DIGA"));
				coBean.setStatus(coRs.getString("STATUS"));
				coBean.setNumeroRid(coRs.getString("NUM_ISCRIZIONE_RID"));
				coBean.setNomeFiumeSbarrato(coRs.getString("NOME_FIUME_SBARRATO"));
				coBean.setNomeBacino(coRs.getString("NOME_BACINO"));
				coBean.setDescrClassDiga(coRs.getString("DESCRIZIONE_CLASSIFICA_DIGA"));
				coBean.setVolTotInvaso(coRs.getString("VOLUME_TOTALE_INVASO_L584"));
				coBean.setQuotaMax(coRs.getString("QUOTA_MAX"));
				coBean.setQuotaMaxReg(coRs.getString("QUOTA_MAX_REGOLAZIONE"));
				coBean.setVolAutoriz(coRs.getString("VOLUME_AUTORIZZATO"));
				coBean.setQuotaAutoriz(coRs.getString("QUOTA_AUTORIZZATA"));
				coBean.setPortataMaxPiena(coRs.getString("PORTATA_MAX_PIENA_PROG"));
				coBean.setDataCertCollaudo(coRs.getString("DATA_CERTIFICATO_COLLAUDO"));
				coBean.setNomeComune(coRs.getString("NOME_COMUNE"));
				coBean.setFiumiDiga(coRs.getString("NOME_FIUME_VALLE"));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return coBean;
	}
	
	public ContactBean getOfficePhoneNumbersFromIdDiga(String damId, ContactBean coBean) throws SQLException {
		
		String sql = "";
		ResultSet coRs = null;
		PreparedStatement coPs = null;
		
		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
				
		try {
		
			//SELECT DATI EXTRA
			sql = "SELECT UC.TELEFONO as TELEFONO_SEDE_CENTRALE, UC.FAX as FAX_SEDE_CENTRALE " 
			    + ", UP.TELEFONO as TELEFONO_UFF_PERIFERICO, UP.FAX as FAX_UFF_PERIFERICO "
			    + ", nvl(to_char(DATA_PIANO_EMERGENZA,'dd/mm/yyyy'), '') as DATA_PIANO_EMERGENZA " 
				+ "FROM DIGA D "
				+ "LEFT JOIN UFFICIO UC ON UC.ID = D.ID_UFF_COORD "
				+ "LEFT JOIN UFFICIO UP ON UP.ID = D.ID_UFF_PERIF "
				+ "LEFT JOIN DPC DP ON DP.NUMERO_ARCHIVIO = D.NUMERO_ARCHIVIO AND DP.SUB = D.SUB "
				+ "WHERE D.NUMERO_ARCHIVIO = ? AND D.SUB = ? AND D.CANCELLATO<>'S' ";
			
			//preparing statement
			coPs = dbUtil.getConnection().prepareStatement(sql);
			coPs.setInt(1, numeroArchivio);
			coPs.setString(2, sub);
			//execute
			coRs = coPs.executeQuery();

			//retrieving 
			if (coRs.next()) {
				coBean.setTeleSedeCent(coRs.getString("TELEFONO_SEDE_CENTRALE"));
				coBean.setFaxSedeCent(coRs.getString("FAX_SEDE_CENTRALE"));
				coBean.setTeleUffPeri(coRs.getString("TELEFONO_UFF_PERIFERICO"));
				coBean.setFaxUffPeri(coRs.getString("FAX_UFF_PERIFERICO"));
				coBean.setDataPianoEmergenza(coRs.getString("DATA_PIANO_EMERGENZA"));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return coBean;
	}
	
	public UfficioBean getUfficioCoordFromIdDiga(String damId) throws SQLException {
		//--QUERY UFFICIO COORD
		String sqlUff = "select ID, TIPO, DESCRIZIONE, INDIRIZZO, TELEFONO, FAX, ID_FUNZ_DIRIGENTE from UFFICIO " +
						"where ID=(select ID_UFF_COORD from DIGA where CANCELLATO<>'S' and NUMERO_ARCHIVIO=? and SUB=?) ";
		//--QUERY FUNZIONARIO UFFICIO COORD
		String sqlFunz = "select ID, NOME, COGNOME, TEL_UFFICIO, TEL_CELLULARE, TEL_CASA from UTENTE " +
						"where ID=(select ID_FUNZ_SEDE_CENTRALE from DIGA where CANCELLATO<>'S' and NUMERO_ARCHIVIO=? and SUB=?) ";
		//--QUERY DIRIGENTE UFFICIO COORD
		String sqlDirig = "select ID, NOME, COGNOME, TEL_UFFICIO, TEL_CELLULARE, TEL_CASA  from UTENTE where ID=?";
		
		return getOfficeFromIdDiga(damId, sqlUff, sqlFunz, sqlDirig);
	}
	
	public UfficioBean getUfficioPerifFromIdDiga(String damId) throws SQLException {
		//--QUERY UFFICIO PERIFERICO
		String sqlUff = "select ID, TIPO, DESCRIZIONE, INDIRIZZO, TELEFONO, FAX, ID_FUNZ_DIRIGENTE from UFFICIO " + 
						"where ID=(select ID_UFF_PERIF from DIGA where CANCELLATO<>'S' and NUMERO_ARCHIVIO=? and SUB=?) ";
		//--QUERY FUNZIONARIO UFFICIO PERIFERICO
		String sqlFunz = "select ID, NOME, COGNOME, TEL_UFFICIO, TEL_CELLULARE, TEL_CASA from UTENTE " + 
						"where ID=(select ID_FUNZ_UFF_PERIF from DIGA where CANCELLATO<>'S' and NUMERO_ARCHIVIO=? and SUB=?) ";
		//--QUERY DIRIGENTE UFFICIO PERIFERICO
		String sqlDirig = "select ID, NOME, COGNOME, TEL_UFFICIO, TEL_CELLULARE, TEL_CASA  from UTENTE where ID=? ";
		
		return getOfficeFromIdDiga(damId, sqlUff, sqlFunz, sqlDirig);
	}
	
	private UfficioBean getOfficeFromIdDiga(String damId, String sqlUff, String sqlFunz, String sqlDirig) throws SQLException {

		ResultSet coRs = null;
		PreparedStatement coPs = null;
		UfficioBean ufBean = null;
		FunzionarioBean dirBean = null;
		FunzionarioBean funBean = null;
		String dirigId = "";
		String sql = "";
		
		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
				
		try {
			
			//preparing statement
			sql = sqlUff;
			coPs = dbUtil.getConnection().prepareStatement(sql);
			coPs.setInt(1, numeroArchivio);
			coPs.setString(2, sub);
			//execute
			coRs = coPs.executeQuery();

			//retrieving UFFICIO
			if (coRs.next()) {
				ufBean = new UfficioBean(coRs.getString("ID")
										, coRs.getString("TIPO")
										, coRs.getString("DESCRIZIONE")
										, coRs.getString("INDIRIZZO")
										, coRs.getString("TELEFONO")
										, coRs.getString("FAX")
										, null, null
										, "", "", "N");
				
				dirigId = coRs.getString("ID_FUNZ_DIRIGENTE");
			}
			
			coRs.close();
			
			//retrieving DIRIGENTE
			if(ufBean != null){
				if (dirigId != null && !"".equals(dirigId)) {
					sql = sqlDirig;
					coPs = dbUtil.getConnection().prepareStatement(sql);
					coPs.setInt(1, Integer.parseInt(dirigId));
					//execute
					coRs = coPs.executeQuery();

					if (coRs.next()) {
						dirBean = new FunzionarioBean(coRs.getString("ID"), coRs.getString("NOME"), coRs.getString("COGNOME"), coRs.getString("TEL_UFFICIO")
								, coRs.getString("TEL_CELLULARE"), coRs.getString("TEL_CASA"), "", "", "N");
						ufBean.setDirigente(dirBean);
					}

					coRs.close();
				} else {
					ufBean.setDirigente(new FunzionarioBean());
				}

				//retrieving FUNZIONARIO
				sql = sqlFunz;
				coPs = dbUtil.getConnection().prepareStatement(sql);
				coPs.setInt(1, numeroArchivio);
				coPs.setString(2, sub);
				//execute
				coRs = coPs.executeQuery();

				if (coRs.next()) {
					funBean = new FunzionarioBean(coRs.getString("ID"), coRs.getString("NOME"), coRs.getString("COGNOME"), coRs.getString("TEL_UFFICIO")
							, coRs.getString("TEL_CELLULARE"), coRs.getString("TEL_CASA"), "", "", "N");
					ufBean.setFunzionario(funBean);
				}
			}
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return ufBean;
	}

	public ArrayList<String> getEntityJunctionIdListFromIdDiga(String damId, String sql) throws SQLException {
		
		ResultSet coRs = null;
		PreparedStatement coPs = null;
		ArrayList<String> idList = null;
		
		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
				
		try {
			
			//preparing statement
			coPs = dbUtil.getConnection().prepareStatement(sql);
			coPs.setInt(1, numeroArchivio);
			coPs.setString(2, sub);
			//execute
			coRs = coPs.executeQuery();
			
			idList = new ArrayList<String>();
	
			//retrieving id list
			while(coRs.next()) {
				idList.add(coRs.getString(1));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return idList;
	}
	
	public ArrayList<NominaIngegnereBean> getNominaIngegnereListFromIdDiga(String damId, String sql) throws SQLException {

		ResultSet coRs = null;
		PreparedStatement coPs = null;
		ArrayList<NominaIngegnereBean> nmList = null;
		
		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
				
		try {
			
			//preparing statement
			coPs = dbUtil.getConnection().prepareStatement(sql);
			coPs.setInt(1, numeroArchivio);
			coPs.setString(2, sub);
			//execute
			coRs = coPs.executeQuery();
			
			nmList = new ArrayList<NominaIngegnereBean>();
	
			//retrieving nomine list
			while(coRs.next()) { 
				nmList.add(new NominaIngegnereBean(coRs.getString("ID_INGEGNERE") //ID INGEGNERE
												, coRs.getInt("PROGRESSIVO") //PROGRESSIVO
												, numeroArchivio //NUMERO ARCHIVIO
												, sub //SUB
												, coRs.getString("DATA_NOMINA") //DATA NOMINA
												, coRs.getString("DATA_MODIFICA") //DATA MODIFICA
												, coRs.getString("ID_UTENTE") //ID UTENTE
												, coRs.getString("CANCELLATO"))); //CANCELLATO
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return nmList;
	}

	//******************* ALTRE VOCI RUBRICA *************************
	
	public AltreVociRubricaBean getAltraVoceRubrica(String damId, int progressivo) throws SQLException, EntityNotFoundException {
		
		AltreVociRubricaBean avBean = new AltreVociRubricaBean();
		avBean.setProgressivo(progressivo);
		
		ArrayList<AltreVociRubricaBean> avList = getAltreVociRubricaList(damId, avBean);

		if (avList.size() > 0) {
			return avList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}
	
	public ArrayList<AltreVociRubricaBean> getAltreVociRubricaList(String damId, AltreVociRubricaBean avBean) throws SQLException {

		String sql = "";
		ResultSet coRs = null;
		PreparedStatement coPs = null;
		ArrayList<AltreVociRubricaBean> avList = null;
		
		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
				
		try {
			
			//SELECT ALTREVOCI
			if ("".equals(avBean.getProgressivo()) || avBean.getProgressivo() == null) { //LIST
				sql = "SELECT PROGRESSIVO, NUMERO_ARCHIVIO, SUB, ENTE, TELEFONO, FAX, DATA_MODIFICA, ID_UTENTE, CANCELLATO, ID_TIPO_ENTE "
					+ "FROM ALTRE_VOCI_RUBRICA "
					+ "WHERE NUMERO_ARCHIVIO = ? AND SUB = ? AND CANCELLATO<>'S' ";
			} else { //SINGLE
				sql = "SELECT PROGRESSIVO, NUMERO_ARCHIVIO, SUB, ENTE, TELEFONO, FAX, DATA_MODIFICA, ID_UTENTE, CANCELLATO, ID_TIPO_ENTE "
					+ "FROM ALTRE_VOCI_RUBRICA "
					+ "WHERE NUMERO_ARCHIVIO = ? AND SUB = ? AND PROGRESSIVO = ? AND CANCELLATO<>'S' ";
			}
			
			//preparing statement
			coPs = dbUtil.getConnection().prepareStatement(sql);
			coPs.setInt(1, numeroArchivio);
			coPs.setString(2, sub);
			
			if (!"".equals(avBean.getId()) && avBean.getId() != null) {
				coPs.setInt(3, avBean.getProgressivo());	
			}
			
			//execute
			coRs = coPs.executeQuery();
	
			avList = new ArrayList<AltreVociRubricaBean>();
			
			//retrieving nomine list
			while(coRs.next()) { 
				avList.add(new AltreVociRubricaBean(coRs.getString("NUMERO_ARCHIVIO") + coRs.getString("SUB") //ID
										, coRs.getInt("PROGRESSIVO") //PROGRESSIVO
										, coRs.getInt("NUMERO_ARCHIVIO") //NUMERO_ARCHIVIO
										, coRs.getString("SUB") //SUB
										, coRs.getString("ENTE") //SUB
										, coRs.getString("TELEFONO") //TELEFONO
										, coRs.getString("FAX") //FAX
										, coRs.getInt("ID_TIPO_ENTE") //ID TIPO ENTE
										, coRs.getString("DATA_MODIFICA") //DATA_MODIFICA
										, coRs.getString("ID_UTENTE") //ID_UTENTE
										, coRs.getString("CANCELLATO"))); //CANCELLATO

			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return avList;
	}

	public AltreVociRubricaBean createAltraVoceRubrica(String damId, AltreVociRubricaBean avBean, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {
		
		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		int nextValId = 0;
		ResultSet nextValPnRs = null;
		PreparedStatement cnPs = null;
		AltreVociRubricaBean foundAvBean = null;

		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
		
		try {
			
			if (closeConnection) {
				//Starting transaction 
				dbUtil.setAutoCommit(false);
			}
			
			//retrieve nextval
			sql = "select ALTRE_VOCI_RUBRICA_SEQUENZA.nextval from dual";
			
			nextValPnRs = dbUtil.getConnection().createStatement().executeQuery(sql);
			
			if (nextValPnRs != null && nextValPnRs.next()) {
				nextValId = nextValPnRs.getInt(1);
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}
		
			sql = "insert into ALTRE_VOCI_RUBRICA(PROGRESSIVO, NUMERO_ARCHIVIO, SUB, ENTE, TELEFONO, FAX, ID_TIPO_ENTE, ID_UTENTE) values ( ?, ?, ?, ?, ?, ?, ?, ? )";
			
			cnPs = dbUtil.getConnection().prepareStatement(sql);
			
			cnPs.setInt(paramsSet++, nextValId); //PROGRESSIVO
			cnPs.setInt(paramsSet++, numeroArchivio); //NUMERO_ARCHIVIO
			cnPs.setString(paramsSet++, sub); //SUB
			cnPs.setString(paramsSet++, avBean.getEnte()); //ENTE
			cnPs.setString(paramsSet++, avBean.getTelefono()); //TELEFONO
			cnPs.setString(paramsSet++, avBean.getFax()); //FAX
			cnPs.setInt(paramsSet++, avBean.getIdTipoEnte()); //ID_TIPO_ENTE
			cnPs.setString(paramsSet++, userId); //ID_UTENTE
			
			updatedRows += cnPs.executeUpdate();

			if (updatedRows > 0) {
				//commiting changes
				if (closeConnection) {
					dbUtil.getConnection().commit();
				}

				foundAvBean = getAltraVoceRubrica(numeroArchivio.toString()+sub, nextValId);
				
			} else {
				throw new SQLException("Record not created");
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			if (closeConnection) {
				dbUtil.rollback();
			}

			throw sqle;
		} finally {
			if (nextValPnRs != null) {
				nextValPnRs.close();
			}
			if (cnPs != null) {
				cnPs.close();
			}
			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}

		return foundAvBean;
	}

	public AltreVociRubricaBean updateAltraVoceRubrica(String damId, AltreVociRubricaBean avBean, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {

		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		PreparedStatement cnPs = null;
		AltreVociRubricaBean foundAvBean = null;
		
		try {
			
			foundAvBean = (AltreVociRubricaBean) bnUtil.compareAndFillBean(getAltraVoceRubrica(damId, avBean.getProgressivo()), avBean);
			
			sql = "update ALTRE_VOCI_RUBRICA set ENTE=?, TELEFONO=?, FAX=?, ID_TIPO_ENTE=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where NUMERO_ARCHIVIO=? and SUB=? and PROGRESSIVO=? ";
				
			cnPs = dbUtil.getConnection().prepareStatement(sql);
			
			cnPs.setString(paramsSet++, foundAvBean.getEnte()); //ENTE
			cnPs.setString(paramsSet++, foundAvBean.getTelefono()); //TELEFONO
			cnPs.setString(paramsSet++, foundAvBean.getFax()); //FAX
			cnPs.setInt(paramsSet++, foundAvBean.getIdTipoEnte()); //ID_TIPO_ENTE
			cnPs.setString(paramsSet++, userId); //ID_UTENTE
			cnPs.setInt(paramsSet++, foundAvBean.getNumeroArchivio()); //NUMERO_ARCHIVIO
			cnPs.setString(paramsSet++, foundAvBean.getSub()); //SUB
			cnPs.setInt(paramsSet++, foundAvBean.getProgressivo()); //PROGRESSIVO
			
			updatedRows += cnPs.executeUpdate();
			
			if (updatedRows > 0) {
				foundAvBean = getAltraVoceRubrica(damId, avBean.getProgressivo());
			} else {
				throw new EntityNotFoundException("Update not executed, couldn't find entity with the provided params");
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (cnPs != null) {
				cnPs.close();
			}
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}

		return foundAvBean;
	}

	public Boolean removeAltraVoceRubrica(String damId, int progressivo, String userId, boolean closeConnection) throws EntityNotFoundException, SQLException {

		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		PreparedStatement inPs = null;
		Boolean wasDeleted = false;

		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
		
		try {
			
			sql = "update ALTRE_VOCI_RUBRICA set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where NUMERO_ARCHIVIO=? and SUB=? and PROGRESSIVO=? ";
			
			//Retrieving connection
			inPs = dbUtil.getConnection().prepareStatement(sql);
			inPs.setString(paramsSet++, userId); //ID_UTENTE
			inPs.setString(paramsSet++, "S"); //CANCELLATO
			inPs.setInt(paramsSet++, numeroArchivio); //NUMERO_ARCHIVIO
			inPs.setString(paramsSet++, sub); //SUB
			inPs.setInt(paramsSet++, progressivo); //PROGRESSIVO
			
			updatedRows += inPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Entity with ID="+damId+" and PROGRESSIVO="+progressivo+" not found.");
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (inPs != null) {
				inPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return wasDeleted;
	}

	//******************* CASA DI GUARDIA *************************
	
	public CasaDiGuardiaBean getCasaDiGuardiaFromIdDiga(String damId, boolean closeConnection) throws SQLException {

		String sql = "";
		ResultSet coRs = null;
		PreparedStatement coPs = null;
		CasaDiGuardiaBean cgBean = null; 
		
		//extract numeroarchivio and sub from iddiga
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
				
		try {
		
			//SELECT CASA DI GUARDIA
			sql = "SELECT NUMERO_ARCHIVIO, SUB, TELEFONO, TELEFONO_POSTO_PRESIDIATO, TELEFONO_CANTIERE"
				+ ", DATA_MODIFICA, ID_UTENTE, CANCELLATO "
				+ "FROM CASA_GUARDIA "
				+ "WHERE NUMERO_ARCHIVIO = ? AND SUB = ? AND CANCELLATO<>'S' ";
			
			//preparing statement
			coPs = dbUtil.getConnection().prepareStatement(sql);
			coPs.setInt(1, numeroArchivio);
			coPs.setString(2, sub);
			//execute
			coRs = coPs.executeQuery();

			//retrieving 
			while(coRs.next()) {
				cgBean = new CasaDiGuardiaBean(coRs.getString("NUMERO_ARCHIVIO") + coRs.getString("SUB") //ID
											, coRs.getInt("NUMERO_ARCHIVIO") //NUMERO_ARCHIVIO
											, coRs.getString("SUB") //SUB
											, coRs.getString("TELEFONO") //TELEFONO
											, coRs.getString("TELEFONO_POSTO_PRESIDIATO") //TELEFONO_POSTO_PRESIDIATO
											, coRs.getString("TELEFONO_CANTIERE") //TELEFONO_CANTIERE
											, coRs.getString("DATA_MODIFICA") //DATA_MODIFICA
											, coRs.getString("ID_UTENTE") //ID_UTENTE
											, coRs.getString("CANCELLATO")); //CANCELLATO
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return cgBean;
	}
	
	public CasaDiGuardiaBean updateCasaDiGuardia(String damId, CasaDiGuardiaBean cgBean, String userId, boolean closeConnection) throws SQLException {
		
		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement cgPs = null;
		CasaDiGuardiaBean foundCgBean = null;
		
		try {
			
			foundCgBean = (CasaDiGuardiaBean) bnUtil.compareAndFillBean(getCasaDiGuardiaFromIdDiga(damId, true), cgBean);
			
			if (foundCgBean.getId() != null && !"".equals(foundCgBean.getId())) {
				sql = "UPDATE CASA_GUARDIA SET TELEFONO=?, TELEFONO_POSTO_PRESIDIATO=?, TELEFONO_CANTIERE=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? WHERE NUMERO_ARCHIVIO=? AND SUB=? ";
			} else {
				sql = "INSERT INTO CASA_GUARDIA (TELEFONO, TELEFONO_POSTO_PRESIDIATO, TELEFONO_CANTIERE, ID_UTENTE, NUMERO_ARCHIVIO, SUB) VALUES (?, ?, ?, ?, ?, ?) ";
			}

			//Retrieving connection
			cgPs = dbUtil.getConnection().prepareStatement(sql);

			cgPs.setString(paramsSet++, foundCgBean.getTelefono()); //TELEFONO
			cgPs.setString(paramsSet++, foundCgBean.getTelefonoPostoPresidiato()); //TELEFONO
			cgPs.setString(paramsSet++, foundCgBean.getTelefonoCantiere()); //TELEFONO
			cgPs.setString(paramsSet++, userId); //ID_UTENTE
			cgPs.setInt(paramsSet++, foundCgBean.getNumeroArchivio()); //TELEFONO
			cgPs.setString(paramsSet++, foundCgBean.getSub()); //TELEFONO
			
			updatedRows += cgPs.executeUpdate();
			
			if (updatedRows > 0 ) {
				foundCgBean = getCasaDiGuardiaFromIdDiga(damId, true);
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (cgPs != null) {
				cgPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return foundCgBean;
	}
}
