package it.cinqueemmeinfo.dammap.dao.macro;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.DamBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.entities.UserBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;
import it.cinqueemmeinfo.dammap.utility.exceptions.JunctionAlreadyExistsException;

public class RegistryDao implements Serializable {

	private static final long serialVersionUID = -5278665745523993692L;
	private static final Logger logger = LoggerFactory.getLogger(RegistryDao.class);
	
	@Autowired private DBUtility dbUtil;

	public Boolean createEntityToUserJunction(BasicEntityBean targetEntity, String targetUserId, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException, JunctionAlreadyExistsException {
		
		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		ResultSet nextValRs = null;
		PreparedStatement ps = null;
		int nextValId = 0;
		
		try {
			
			//check if junction is already created
			if (getUserListFromEntityId(targetEntity, targetUserId, true).size() == 0) {

				//Starting transaction 
				dbUtil.setAutoCommit(false);
				
				//Get next val id
				if (targetEntity instanceof AutoritaProtezioneCivileBean) {
					sql = "select UTENTE_AUTORITA_SEQUENZA.nextval from dual";
				} else if (targetEntity instanceof ConcessionarioBean) {
					sql = "select UTENTE_CONCESS_SEQUENZA.nextval from dual";
				} else if (targetEntity instanceof GestoreBean) {
					sql = "select UTENTE_GESTORE_SEQUENZA.nextval from dual";
				} else if (targetEntity instanceof AltraEnteBean) {
					sql = "select UTENTE_AENTE_SEQUENZA.nextval from dual";
				}
				
				nextValRs = dbUtil.getConnection().createStatement().executeQuery(sql);
				
				if (nextValRs != null && nextValRs.next()) {
					nextValId = nextValRs.getInt(1);
				} else {
					throw new SQLException("Couldn't retrieve next sequence value.");
				}
	
				//Insert junction
				if (targetEntity instanceof AutoritaProtezioneCivileBean) {
					sql = "insert into UTENTE_AUTORITA (ID, ID_UTENTE, ID_AUTORITA, ORDINE, ID_UTENTE_MOD ) values ( ?,?,?,?,? )";
				} else if (targetEntity instanceof ConcessionarioBean) {
					sql = "insert into UTENTE_CONCESS (ID, ID_UTENTE, ID_CONCESS, ORDINE, ID_UTENTE_MOD ) values ( ?,?,?,?,? )";
				} else if (targetEntity instanceof GestoreBean) {
					sql = "insert into UTENTE_GESTORE (ID, ID_UTENTE, ID_GESTORE, ORDINE, ID_UTENTE_MOD ) values ( ?,?,?,?,? )";
				} else if (targetEntity instanceof AltraEnteBean) {
					sql = "insert into UTENTE_ALTRA_ENTE (ID, ID_UTENTE, ID_ENTE, ORDINE, ID_UTENTE_MOD ) values ( ?,?,?,?,? )";
				}
				
				ps = dbUtil.getConnection().prepareStatement(sql);
				
				ps.setInt(paramsSet++, nextValId); //ID
				ps.setString(paramsSet++, targetUserId); //TARGET USER
				ps.setInt(paramsSet++, Integer.parseInt(targetEntity.getId())); //TARGET ENTITY
				ps.setInt(paramsSet++, nextValId); //TEMP?
				ps.setString(paramsSet++, userId); //ID_UTENTE
				
				updatedRows += ps.executeUpdate();
				
				if (updatedRows > 0) {
					//commiting changes
					if (closeConnection) {
						dbUtil.getConnection().commit();
					}
				} else {
					throw new SQLException("Error while creating record");
				}
			} else {
				throw new JunctionAlreadyExistsException("Error, User to Entity Junction already exists.");
			}

		} catch (SQLException sqle) {
			logger.error("SQLException with message: " + sqle.getMessage());
			if (closeConnection) {
				dbUtil.rollback();
			}
			throw sqle;
		} finally {
			if (nextValRs != null) {
				nextValRs.close();
			}
			if (ps != null) {
				ps.close();
			}
			
			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}
		
		return (updatedRows > 0);
	}

	public Boolean removeEntityToUserJunction(BasicEntityBean targetEntity, String targetUserId, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement cnPs = null;
		Boolean wasDeleted = false;
		
		try {

			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "update UTENTE_AUTORITA set DATA_MODIFICA=SYSDATE, ID_UTENTE_MOD=?, CANCELLATO=? where ID_AUTORITA=? AND ID_UTENTE=? AND CANCELLATO<>'S' ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "update UTENTE_CONCESS set DATA_MODIFICA=SYSDATE, ID_UTENTE_MOD=?, CANCELLATO=? where ID_CONCESS=? AND ID_UTENTE=? AND CANCELLATO<>'S' ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "update UTENTE_GESTORE set DATA_MODIFICA=SYSDATE, ID_UTENTE_MOD=?, CANCELLATO=? where ID_GESTORE=? AND ID_UTENTE=? AND CANCELLATO<>'S' ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "update UTENTE_ALTRA_ENTE set DATA_MODIFICA=SYSDATE, ID_UTENTE_MOD=?, CANCELLATO=? where ID_ENTE=? AND ID_UTENTE=? AND CANCELLATO<>'S' ";
			}
			
			//Retrieving connection
			cnPs = dbUtil.getConnection().prepareStatement(sql);
			cnPs.setString(paramsSet++, userId); //ID_UTENTE
			cnPs.setString(paramsSet++, "S"); //CANCELLATO
			cnPs.setInt(paramsSet++, Integer.parseInt(targetEntity.getId())); //ID_CONCESS
			cnPs.setString(paramsSet++, targetUserId); //ID_GESTORE
			
			updatedRows += cnPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Junction between Entity of type "+targetEntity.getClass().getName()+" with ID="+targetEntity.getId()+" and User with ID="+targetUserId+" not found.");
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (cnPs != null) {
				cnPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return wasDeleted;
	}
	
	public ArrayList<DamBean> getDamListFromEntityId(BasicEntityBean targetEntity, boolean closeConnection) throws SQLException {

		String sql = "";
		ResultSet coRs = null;
		PreparedStatement coPs = null;
		ArrayList<DamBean> damList = null;
				
		try {
			
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "SELECT D.NUMERO_ARCHIVIO, D.SUB, D.NOME_DIGA " + 
						"FROM DIGA D LEFT JOIN DIGA_AUTORITA DC ON D.NUMERO_ARCHIVIO = DC.NUMERO_ARCHIVIO AND D.SUB = DC.SUB " +
						"WHERE DC.ID_AUTORITA = ? AND DC.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "SELECT D.NUMERO_ARCHIVIO, D.SUB, D.NOME_DIGA " + 
						"FROM DIGA D LEFT JOIN DIGA_CONCESS DC ON D.NUMERO_ARCHIVIO = DC.NUMERO_ARCHIVIO AND D.SUB = DC.SUB " +
						"WHERE DC.ID_CONCESS = ? AND DC.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "SELECT D.NUMERO_ARCHIVIO, D.SUB, D.NOME_DIGA " + 
						"FROM DIGA D LEFT JOIN DIGA_GESTORE DC ON D.NUMERO_ARCHIVIO = DC.NUMERO_ARCHIVIO AND D.SUB = DC.SUB " +
						"WHERE DC.ID_GESTORE = ? AND DC.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "SELECT D.NUMERO_ARCHIVIO, D.SUB, D.NOME_DIGA " + 
						"FROM DIGA D LEFT JOIN DIGA_ALTRA_ENTE DC ON D.NUMERO_ARCHIVIO = DC.NUMERO_ARCHIVIO AND D.SUB = DC.SUB " +
						"WHERE DC.ID_ENTE = ? AND DC.CANCELLATO<>'S' ";
			}
			
			//preparing statement
			coPs = dbUtil.getConnection().prepareStatement(sql);
			coPs.setInt(1, Integer.parseInt(targetEntity.getId()));
			//execute
			coRs = coPs.executeQuery();
			
			damList = new ArrayList<DamBean>();
	
			//retrieving id list
			while(coRs.next()) {
				damList.add(new DamBean(coRs.getString("NUMERO_ARCHIVIO")+coRs.getString("SUB")
						, coRs.getString("NUMERO_ARCHIVIO")
						, coRs.getString("SUB")
						, coRs.getString("NOME_DIGA")));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return damList;
	}
	
	public ArrayList<UserBean> getUserListFromEntityId(BasicEntityBean targetEntity, String idUser, boolean closeConnection) throws SQLException {

		String sql = "";
		ResultSet coRs = null;
		PreparedStatement coPs = null;
		ArrayList<UserBean> usrList = null;
				
		try {
			
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "SELECT U.ID, U.USERID, U.NOME, U.COGNOME, U.CANCELLATO " +
						"FROM UTENTE U LEFT JOIN UTENTE_AUTORITA UA ON UA.ID_UTENTE = U.ID " + 
						"WHERE UA.ID_AUTORITA = ? AND U.CANCELLATO<>'S' AND UA.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "SELECT U.ID, U.USERID, U.NOME, U.COGNOME, U.CANCELLATO " +
						"FROM UTENTE U LEFT JOIN UTENTE_CONCESS UA ON UA.ID_UTENTE = U.ID " + 
						"WHERE UA.ID_CONCESS = ? AND U.CANCELLATO<>'S' AND UA.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "SELECT U.ID, U.USERID, U.NOME, U.COGNOME, U.CANCELLATO " +
						"FROM UTENTE U LEFT JOIN UTENTE_GESTORE UA ON UA.ID_UTENTE = U.ID " + 
						"WHERE UA.ID_GESTORE = ? AND U.CANCELLATO<>'S' AND UA.CANCELLATO<>'S' ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "SELECT U.ID, U.USERID, U.NOME, U.COGNOME, U.CANCELLATO " +
						"FROM UTENTE U LEFT JOIN UTENTE_ALTRA_ENTE UA ON UA.ID_UTENTE = U.ID " + 
						"WHERE UA.ID_ENTE = ? AND U.CANCELLATO<>'S' AND UA.CANCELLATO<>'S' ";
			}
			
			if (idUser != null && !"".equals(idUser)) {
				sql += " AND UA.ID_UTENTE = ? ";
			}
			
			//preparing statement
			coPs = dbUtil.getConnection().prepareStatement(sql);
			coPs.setInt(1, Integer.parseInt(targetEntity.getId()));
			
			if (idUser != null && !"".equals(idUser)) {
				coPs.setInt(2, Integer.parseInt(idUser));	
			}
			
			//execute
			coRs = coPs.executeQuery();
			
			usrList = new ArrayList<UserBean>();
	
			//retrieving list
			while(coRs.next()) {
				usrList.add(new UserBean(coRs.getString("ID")
										, coRs.getString("NOME")
										, coRs.getString("COGNOME")
										, "", "", "", "", "", ""
										, coRs.getString("USERID")
										, "", "", "", "", "", ""
										, coRs.getString("CANCELLATO")));

			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return usrList;
	}
	
	public ArrayList<UserBean> getUserList(boolean closeConnection) throws SQLException {

		String sql = "";
		ResultSet coRs = null;
		PreparedStatement coPs = null;
		ArrayList<UserBean> usrList = null;
				
		try {

			sql = "SELECT U.ID, U.NOME, U.COGNOME, U.DESCR, U.TEL_UFFICIO, U.TEL_CELLULARE, U.TEL_CASA, U.TIPO " +
					", U.USERID, U.CATEGORIA, U.ID_RIFERIMENTO, U.DATA_MODIFICA, U.ID_UTENTE, U.CANCELLATO " +
					"FROM UTENTE U WHERE U.CANCELLATO<>'S' ";
			
			//preparing statement
			coPs = dbUtil.getConnection().prepareStatement(sql);
			//execute
			coRs = coPs.executeQuery();
			
			usrList = new ArrayList<UserBean>();
	
			//retrieving list
			while(coRs.next()) {
				usrList.add(new UserBean(coRs.getString("ID")
										, coRs.getString("NOME")
										, coRs.getString("COGNOME")
										, coRs.getString("DESCR")
										, coRs.getString("TEL_UFFICIO")
										, coRs.getString("TEL_CELLULARE")
										, coRs.getString("TEL_CASA")
										, coRs.getString("TIPO")
										, ""
										, coRs.getString("USERID")
										, ""
										, coRs.getString("CATEGORIA")
										, coRs.getString("ID_RIFERIMENTO")
										, ""
										, coRs.getString("DATA_MODIFICA")
										, coRs.getString("ID_UTENTE")
										, coRs.getString("CANCELLATO")));

			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (coPs != null) {
				coPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return usrList;
	}
}
