package it.cinqueemmeinfo.dammap.dao;

import java.io.Serializable;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;

import it.cinqueemmeinfo.dammap.bean.entities.VistaBean;
import it.cinqueemmeinfo.dammap.dao.entities.VistaDao;

public class VistaService implements Serializable {

	private static final long serialVersionUID = -2927571924602882936L;
	private static final Logger logger = LoggerFactory.getLogger(VistaService.class);
	
	@Autowired private VistaDao vistaDao;
	
	//******************* VISTE *************************


	public VistaBean getViewsForUser(Long userId) throws SQLException {
		logger.info("Retrieving Views");
		return vistaDao.retrieveViewsForUser(userId, Boolean.TRUE);
	}

	public void saveViewsForUser(Long userId, JsonNode viste) throws SQLException {
		vistaDao.saveViewsForUser(userId, viste, Boolean.TRUE);
	}

	public JsonNode getBaseView() throws SQLException {
		return vistaDao.retrieveBaseView(Boolean.TRUE);
	}
}
