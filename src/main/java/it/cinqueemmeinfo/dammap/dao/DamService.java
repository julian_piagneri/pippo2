package it.cinqueemmeinfo.dammap.dao;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cinqueemmeinfo.dammap.bean.entities.DamBean;
import it.cinqueemmeinfo.dammap.bean.entities.FiumeBean;
import it.cinqueemmeinfo.dammap.dao.entities.DamDao;
import it.cinqueemmeinfo.dammap.utility.UserUtility;

public class DamService implements Serializable {

	private static final long serialVersionUID = -8511078728461123320L;
	//private static final Logger logger = LoggerFactory.getLogger(DamService.class);
	
	//@Autowired private DBUtility dbUtil;
	@Autowired private UserUtility usrUtil;
	@Autowired private DamDao dmDao;

	public List<DamBean> getDamList() throws SQLException {
		return dmDao.getDamList(usrUtil.getUserId(), Boolean.FALSE);
	}

	public List<DamBean> getDamExtraList() throws SQLException {
		//String userId="DCHIAROLLA";
		String userId=usrUtil.getUserId();
		return dmDao.getDamList(userId, Boolean.TRUE);
	}

	public List<DamBean> getDamExtraList(String filters) throws SQLException, JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		@SuppressWarnings("unchecked")
		HashMap<String, Integer> filtersMap = mapper.readValue(filters.toString(), new HashMap<String, Integer>().getClass());
		
		return dmDao.getDamList(usrUtil.getUserId(), Boolean.TRUE, filtersMap);
	}
	
	public Set<FiumeBean> getRiverExtraInfo() throws SQLException {
		return dmDao.getRiverExtraInfo();
	}

	public void setDamsFavorite(ArrayList<DamBean> dams, Long userId, boolean favorite) throws SQLException {
		dmDao.setDamsFavorite(dams, userId, favorite, Boolean.TRUE);		
	}
}
