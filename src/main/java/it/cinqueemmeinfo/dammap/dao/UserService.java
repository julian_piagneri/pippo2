package it.cinqueemmeinfo.dammap.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.entities.IngegnereBean;
import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.dao.entities.IngegnereDao;
import it.cinqueemmeinfo.dammap.dao.macro.LoginDao;
import it.cinqueemmeinfo.dammap.dao.macro.UserDao;
import it.cinqueemmeinfo.dammap.utility.MiscUtils;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.validation.ValidationBean;
import it.cinqueemmeinfo.dammap.validation.ValidationProcess;
import it.cinqueemmeinfo.dammap.validation.validators.ContainsValidator;
import it.cinqueemmeinfo.dammap.validation.validators.NotEmpty;
import it.cinqueemmeinfo.dammap.validation.validators.PasswordRepeat;
import it.cinqueemmeinfo.dammap.validation.validators.RegexValidator;
import it.cinqueemmeinfo.dammap.validation.validators.UniqueUsername;

@Component("userService")
public class UserService {

	@Autowired
	UserDao usrDao;
	@Autowired
	LoginDao loginDao;
	@Autowired
	UserUtility usrUtil;
	@Autowired
	IngegnereDao inDao;
	@Value("${password.regex}")
	String pwdRegex;
	@Value("${password.lower}")
	String pwdLower;
	@Value("${password.upper}")
	String pwdUpper;
	@Value("${password.numbers}")
	String pwdNumbers;
	@Value("${password.symbols}")
	String pwdSymbols;
	@Value("${password.minl}")
	String pwdMinl;

	public static final String CAN_ADMIN_USERS = "10100-S-W";
	// private static final Logger logger =
	// LoggerFactory.getLogger(UserService.class);

	// public Userde;

	public UserSecurityBean getUserWithDetails(Integer userId) throws SQLException {
		return loginDao.findByIdWithDetails(userId, true);
	}

	public Boolean checkDamWriteRigthsForAutorita(String damId) throws SQLException {
		return usrDao.checkDamWriteRigths(usrUtil.getUserId(), new AutoritaProtezioneCivileBean(), damId);
	}

	public Boolean checkDamWriteRigthsForAltraEnte(String damId) throws SQLException {
		return usrDao.checkDamWriteRigths(usrUtil.getUserId(), new AltraEnteBean(), damId);
	}

	public Boolean checkDamWriteRigthsForConcessionario(String damId) throws SQLException {
		return usrDao.checkDamWriteRigths(usrUtil.getUserId(), new ConcessionarioBean(), damId);
	}

	public Boolean checkDamWriteRigthsForGestore(String damId) throws SQLException {
		return usrDao.checkDamWriteRigths(usrUtil.getUserId(), new GestoreBean(), damId);
	}

	public ArrayList<String> getUserRightsForAutorita() throws SQLException {
		return usrDao.getUserWriteRigthsForEntity(usrUtil.getUserId(), new AutoritaProtezioneCivileBean());
	}

	public ArrayList<String> getUserRightsForEntity(BasicEntityBean entity) throws SQLException {
		return usrDao.getUserWriteRigthsForEntity(usrUtil.getUserId(), entity);
	}

	public ArrayList<String> getUserRightsForConcessionario() throws SQLException {
		return usrDao.getUserWriteRigthsForEntity(usrUtil.getUserId(), new ConcessionarioBean());
	}

	public ArrayList<String> getUserRightsForGestore() throws SQLException {
		return usrDao.getUserWriteRigthsForEntity(usrUtil.getUserId(), new GestoreBean());
	}

	public ArrayList<String> getUserRightsForAltraEnte() throws SQLException {
		return usrDao.getUserWriteRigthsForEntity(usrUtil.getUserId(), new AltraEnteBean());
	}

	public ArrayList<String> getUserAccessRigthsForDam(String damId, String schedaId) throws SQLException {
		return usrDao.getUserAccessRigthsForDam(usrUtil.getUserId(), damId, schedaId);
	}

	public Boolean checkRigthsForIngegnere(String ingId) throws SQLException {

		Boolean canAsIng = false;
		List<IngegnereBean> inList = null;
		String userId = usrUtil.getUserId();

		int hasAccess = usrUtil.getMaxPermissionForScheda(UserUtility.IDS_ANAGRAFICA);

		if (hasAccess >= 1) {
			inList = inDao.getIngegnereListWithParams(new IngegnereBean(), userId, hasAccess, Boolean.FALSE,
					Boolean.TRUE);
		}
		if (inList != null) {
			for (int i = 0; i < inList.size(); i++) {
				if (inList.get(i).getId().equals(ingId)) {
					canAsIng = true;
				}
			}
		}

		return canAsIng;
	}

	/**
	 * Aggiunge un campo alla mappa se il suo valore è stato modificato
	 * 
	 * @param availableFields
	 * @param isNew
	 */
	private void addIfEdited(Map<String, String> availableFields, String key, String newValue, String originalValue,
			boolean isNew) {
		if (isNew || (newValue != null && !newValue.equals(originalValue))) {
			availableFields.put(key, newValue);
		}
	}

	public UserSecurityBean updateUser(UserSecurityBean userBean, int userId, boolean isNew) throws Exception {
		// Controlliamo che non ci siano errori di validazione
		if (this.validateUser(userBean, isNew).hasErrors()) {
			throw new Exception();
		}

		Map<String, String> availableFields = new HashMap<String, String>();
		// Carica il valore originale
		UserSecurityBean original;
		if (isNew) {
			// Se è nuovo fa il paragone con se stesso
			original = userBean;
		} else {
			original = loginDao.findByIdWithDetails(userBean.getId(), true);
		}
		this.addFieldsIfEdited(availableFields, userBean, original, isNew);
		// Setta i campi che vanno modificati
		// availableFields.add("USERID");

		boolean updatePassword = false;
		// Se si sta cambiando la password o l'utente è nuovo
		if (isNew
				|| (userBean.checkForNewPassword() && userBean.getNewPassword().equals(userBean.getRepeatPassword()))) {
			this.addIfEdited(availableFields, "PASSWORD", userBean.getNewPassword().toUpperCase(),
					original.getPassword().toUpperCase(), isNew);
			userBean.setPassword(usrUtil.encodePassword(userBean.getNewPassword()));
			updatePassword = true;
		}

		// Aggiorna la data scadenza password. Solo gli admin possono inserire
		// valori arbitrari alla scadenza, gli altri possono modificarla solo
		// aggiornando la password
		boolean updateExpiration = false;
		if (userBean.getPwdExpireDate().getTime() != original.getPwdExpireDate().getTime()
				&& usrUtil.hasRole(UserService.CAN_ADMIN_USERS)) {
			updateExpiration = true;
		}

		return usrDao.updateUtente(userBean, availableFields, updatePassword, updateExpiration, userId, isNew);
	}

	/**
	 * Adds the fields for the query if they've been edited
	 * 
	 * @param availableFields
	 * @param userBean
	 * @param original
	 * @param isNew
	 * @param isNew
	 */
	private void addFieldsIfEdited(Map<String, String> availableFields, UserSecurityBean userBean,
			UserSecurityBean original, boolean isNew) {
		this.addIfEdited(availableFields, "NOME", userBean.getUserDetails().getNome(),
				original.getUserDetails().getNome(), isNew);

		this.addIfEdited(availableFields, "COGNOME", userBean.getUserDetails().getCognome(),
				original.getUserDetails().getCognome(), isNew);
		this.addIfEdited(availableFields, "TIPO", userBean.getUserDetails().getRuolo(),
				original.getUserDetails().getRuolo(), isNew);
		this.addIfEdited(availableFields, "TEL_UFFICIO", userBean.getUserDetails().getTelUfficio(),
				original.getUserDetails().getTelUfficio(), isNew);
		this.addIfEdited(availableFields, "TEL_CELLULARE", userBean.getUserDetails().getTelCellulare(),
				original.getUserDetails().getTelCellulare(), isNew);
		this.addIfEdited(availableFields, "TEL_CASA", userBean.getUserDetails().getAltriRecapiti(),
				original.getUserDetails().getAltriRecapiti(), isNew);
		this.addIfEdited(availableFields, "DESCR", userBean.getUserDetails().getDescrizione(),
				original.getUserDetails().getDescrizione(), isNew);

		// Se ha il permesso di scrittura sulla scheda utente allora può
		// modificare anche lo user id e il login abilitato
		if (usrUtil.hasRole(UserService.CAN_ADMIN_USERS)) {
			this.addIfEdited(availableFields, "USERID", userBean.getUsername().toUpperCase(),
					original.getUsername().toUpperCase(), isNew);
			this.addIfEdited(availableFields, "LOGIN_ABILITATO", MiscUtils.boolToDamString(userBean.isActive()),
					MiscUtils.boolToDamString(original.isActive()), isNew);
		}
	}

	// public ValidationBean validateUser(UserSecurityBean userBean) {
	// ValidationBean validation=new ValidationBean();
	// Map<String, String> messages=validation.getMessages();
	// //return registryService.createAutoritaProtezioneCivile(apcBean);
	// Validators.validate(new NotEmpty(), userBean.getUserDetails().getNome(),
	// "nome", "Il nome non può essere vuoto", messages);
	// Validators.validate(new NotEmpty(),
	// userBean.getUserDetails().getCognome(), "cognome", "Il cognome non può
	// essere vuoto", messages);
	// Validators.validate(new NotEmpty(), userBean.getUsername(), "username",
	// "Lo username non può essere vuoto", messages);
	// Validators.validate(new PasswordRepeat(), userBean, "newPassword", "Le
	// password inserite devono essere identiche", messages);
	// //Se non ci sono stati errori
	// boolean hasErrors=messages.size()>0;
	// if(!hasErrors){
	// validation.setStatus("ok");
	// } else {
	// validation.setStatus("error");
	// }
	// return validation;
	// }

	public ValidationBean validateUser(UserSecurityBean userBean, boolean isNew) throws SQLException {
		ValidationBean validation = new ValidationBean();
		// Map<String, String> messages=validation.getMessages();
		ValidationProcess process = new ValidationProcess();

		process.add("nome",
				new NotEmpty(userBean.getUserDetails().getNome()).setMessage("Il nome non può essere vuoto"));
		process.add("cognome",
				new NotEmpty(userBean.getUserDetails().getCognome()).setMessage("Il cognome non può essere vuoto"));
		process.add("username", new NotEmpty(userBean.getUsername()).setMessage("Lo username non può essere vuoto"));
		process.add("username",
				new UniqueUsername(userBean, loginDao.findByUsername(userBean.getUsername().toUpperCase(), false))
						.setMessage("Esiste già un utente con questo username"));

		// Aggiunge i validatori solo se si sta modificando la password
		// Tranne quello per controllare se le password sono identiche
		// Per il caso in cui una è lasciata vuota
		if (isNew) {
			// Se si sta inserendo un nuovo utente allora bisogna per forza
			// inserire la password
			process.add("newPassword",
					new NotEmpty(userBean.getNewPassword()).setMessage("La password non può essere vuota"));
		}
		process.add("newPassword",
				new PasswordRepeat(userBean).setMessage("Le password inserite devono essere identiche"));
		if (userBean.checkForNewPassword()) {

			String parsedRegex = pwdRegex.replace("{lower}", this.pwdLower).replace("{upper}", this.pwdUpper)
					.replace("{numbers}", this.pwdNumbers).replace("{symbols}", this.pwdSymbols)
					.replace("{minl}", this.pwdMinl);

			process.add("newPassword", new RegexValidator(userBean.getNewPassword(), parsedRegex)
					.setMessage("La password inserita non soddisfa i criteri di sicurezza: \n"
							+ " - Lunghezza Minima: 8 caratteri \n"
							+ " - Deve contenere almeno 1 carattere minuscolo non accentato \n"
							+ " - Deve contenere almeno 1 carattere maiuscolo non accentato \n"
							+ " - Deve contenere almeno 1 numero \n"
							+ " - Deve contenere almeno 1 carattere speciale tra questi: "
							+ " \\ ? / < ~ # ` ! @ $ % £ ^ & * ( ) + = } | : \" ; ' , > { \n"
							+ "- La password non può contenere spazi nè altri caratteri diversi da quelli sopra elencati"));

			process.add("newPassword",
					new ContainsValidator(userBean.getNewPassword(), userBean.getUsername()).setIgnoreCase(true)
							.setNegativeMatch(true).setMessage("La password non può contenere lo username"));

			process.add("newPassword",
					new ContainsValidator(userBean.getNewPassword(), userBean.getUserDetails().getNome())
							.setIgnoreCase(true).setNegativeMatch(true)
							.setMessage("La password non può contenere il nome dell'utente"));

			process.add("newPassword",
					new ContainsValidator(userBean.getNewPassword(), userBean.getUserDetails().getCognome())
							.setIgnoreCase(true).setNegativeMatch(true)
							.setMessage("La password non può contenere il cognome dell'utente"));
		}

		// String match="Merlin123!";
		// logger.info(parsedRegex);
		// String[] tests={"Merlin123!", "DCHIAROLLA123!", "Domenico100",
		// "Merli1!", "Merlin123!2£325\"\"!",
		// "ahafeihaegouhegaoueaVOOEUAHEGFOUEHUOHE9))))))))))))))))))))))))(((((((((((((((({}$$£££££££|\"$%£%&/()=?'^\\?/<~#`!@$%£^&*()+=}|:\";',>{"};
		// for (String string : tests) {
		// match=string;
		// boolean maaaaa=match.matches(parsedRegex);
		// logger.info(match+": "+(maaaaa ? "true" : "false"));
		// }
		// Se non ci sono stati errori
		boolean hasErrors = !process.validate(validation);
		if (!hasErrors) {
			validation.setStatus("ok");
		} else {
			validation.setStatus("error");
		}
		return validation;
	}

	public UserSecurityBean getNewUserBean() throws SQLException {
		UserSecurityBean newUser = new UserSecurityBean();
		Map<String, Integer> expireDates = loginDao.getExpireDates();
		newUser.setLastPwdUpdate(new Timestamp(System.currentTimeMillis()));
		newUser.setPwdExpireDate(new Timestamp(
				System.currentTimeMillis() + (expireDates.get(LoginDao.EXPIRES_LABEL) * UserDao.DAYMILLIS)));
		return newUser;
	}

	/*
	 * row = ResultStruct.createRow(); row.put("TELEFONO_SEDE_CENTRALE", null);
	 * row.put("FAX_SEDE_CENTRALE", null); row.put("TELEFONO_UFF_PERIFERICO",
	 * null); row.put("FAX_UFF_PERIFERICO", null);
	 * row.put("DATA_PIANO_EMERGENZA", null); result.addRow(row);
	 * 
	 * row = ResultStruct.createRow(); row.put("ID_CONCESSIONARIO", null);
	 * row.put("ID_GESTORE", null); row.put("ID_AUTORITA_PRO_CIV", null);
	 * row.put("DATA_MODIFICA", null); row.put("UTENTE_MODIFICA", null);
	 * result.addRow(row);
	 * 
	 * row = ResultStruct.createRow(); row.put("ID_INGEGNERE_RESPONSABILE",
	 * null); row.put("DATA_NOMINA_RESPONSABILE", null);
	 * row.put("NOME_INGEGNERE_RESPONSABILE", null);
	 * row.put("PROGRESSIVO_RESPONSABILE", null);
	 * row.put("ID_INGEGNERE_SOSTITUTO", null); row.put("DATA_NOMINA_SOSTITUTO",
	 * null); row.put("NOME_INGEGNERE_SOSTITUTO", null);
	 * row.put("PROGRESSIVO_SOSTITUTO", null); row.put("TELEFONO_CASA_GUARDIA",
	 * null); row.put("TELEFONO_POSTO_PRESIDIATO", null);
	 * row.put("TELEFONO_CANTIERE", null);
	 * row.put("DATA_VALIDAZIONE_CASA_GUARDIA", null);
	 * row.put("UTENTE_VALIDAZIONE_CASA_GUARDIA", null);
	 * row.put("DATA_MODIFICA", null); row.put("UTENTE_MODIFICA", null);
	 * result.addRow(row);
	 * 
	 * row = ResultStruct.createRow(); row.put("ALTRE_VOCI_RUBRICA",
	 * ResultStruct.create()); row.put("DATA_MODIFICA", null);
	 * row.put("UTENTE_MODIFICA", null); result.addRow(row);
	 * 
	 * return result;
	 */

}
