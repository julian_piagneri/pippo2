package it.cinqueemmeinfo.dammap.dao.entities;

import it.cinqueemmeinfo.dammap.bean.entities.EntityTypeBean;
import it.cinqueemmeinfo.dammap.bean.entities.ProvinciaBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class EntityTypeDao implements Serializable {

	private static final long serialVersionUID = 4166882895955290958L;
	private static final Logger logger = LoggerFactory.getLogger(EntityTypeDao.class);
	
	@Autowired private DBUtility dbUtil;

	public ArrayList<ProvinciaBean> getProvinciaList() throws SQLException {

		String sql = "";
		ResultSet etRs = null;
		PreparedStatement etPs = null;
		ArrayList<ProvinciaBean> etList = null;
				
		try {
			sql = "SELECT SIGLA, NOME_REGIONE, NOME, COD_ISTAT_PROVINCIA, COD_ISTAT_REGIONE, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
				  + "FROM PROVINCIA "
				  + "WHERE CANCELLATO<>'S' "
				  + "ORDER BY NOME_REGIONE ASC ";
			
			//preparing statement
			etPs = dbUtil.getConnection().prepareStatement(sql);
			//execute
			etRs = etPs.executeQuery();
			
			etList = new ArrayList<ProvinciaBean>();
	
			//retrieving id list
			while(etRs.next()) {
				etList.add(new ProvinciaBean("" //ID
											, etRs.getString("SIGLA") //SIGLA
											, etRs.getString("NOME_REGIONE") //NOME_REGIONE
											, etRs.getString("NOME") //NOME
											, etRs.getInt("COD_ISTAT_PROVINCIA") //COD_ISTAT_PROVINCIA
											, etRs.getInt("COD_ISTAT_REGIONE") //COD_ISTAT_REGIONE
											, etRs.getString("DATA_MODIFICA") //DATA_MODIFICA
											, etRs.getString("ID_UTENTE") //ID_UTENTE
											, etRs.getString("CANCELLATO"))); //CANCELLATO

			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (etPs != null) {
				etPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return etList;
	}
	
	public ArrayList<EntityTypeBean> getRegioneList() throws SQLException {

		String sql = "";
		ResultSet etRs = null;
		PreparedStatement etPs = null;
		ArrayList<EntityTypeBean> etList = null;
				
		try {
			sql = "SELECT ID, NOME, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
				  + "FROM REGIONE "
				  + "WHERE CANCELLATO<>'S' "
				  + "ORDER BY NOME ASC ";
			
			//preparing statement
			etPs = dbUtil.getConnection().prepareStatement(sql);
			//execute
			etRs = etPs.executeQuery();
			
			etList = new ArrayList<EntityTypeBean>();
	
			//retrieving id list
			while(etRs.next()) {
				etList.add(new EntityTypeBean(etRs.getString("ID")
											, etRs.getString("NOME")
											, ""
											, etRs.getString("DATA_MODIFICA")
											, etRs.getString("ID_UTENTE")
											, etRs.getString("CANCELLATO")));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (etPs != null) {
				etPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return etList;
	}

	public ArrayList<EntityTypeBean> getTipoEnteList() throws SQLException {

		String sql = "";
		ResultSet etRs = null;
		PreparedStatement etPs = null;
		ArrayList<EntityTypeBean> etList = null;
				
		try {
			sql = "SELECT ID, DESCRIZIONE, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
				  + "FROM TIPO_ENTE "
				  + "WHERE CANCELLATO<>'S' "
				  + "ORDER BY DESCRIZIONE ASC ";
			
			//preparing statement
			etPs = dbUtil.getConnection().prepareStatement(sql);
			//execute
			etRs = etPs.executeQuery();
			
			etList = new ArrayList<EntityTypeBean>();
	
			//retrieving id list
			while(etRs.next()) {
				etList.add(new EntityTypeBean(etRs.getString("ID")
											, ""
											, etRs.getString("DESCRIZIONE")
											, etRs.getString("DATA_MODIFICA")
											, etRs.getString("ID_UTENTE")
											, etRs.getString("CANCELLATO")));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (etPs != null) {
				etPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return etList;
	}

	public ArrayList<EntityTypeBean> getTipoAltraEnteList() throws SQLException {

		String sql = "";
		ResultSet etRs = null;
		PreparedStatement etPs = null;
		ArrayList<EntityTypeBean> etList = null;
				
		try {
			sql = "SELECT ID, TIPO, DESCRIZIONE "
				  + "FROM TIPO_ALTRA_ENTE "
				  + "ORDER BY TIPO ASC ";
			
			//preparing statement
			etPs = dbUtil.getConnection().prepareStatement(sql);
			//execute
			etRs = etPs.executeQuery();
			
			etList = new ArrayList<EntityTypeBean>();
	
			//retrieving id list
			while(etRs.next()) {
				etList.add(new EntityTypeBean(etRs.getString("ID")
											, etRs.getString("TIPO")
											, etRs.getString("DESCRIZIONE")
											, "", "", ""));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (etPs != null) {
				etPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return etList;
	}

	public ArrayList<EntityTypeBean> getTipoAutoritaList() throws SQLException {

		String sql = "";
		ResultSet etRs = null;
		PreparedStatement etPs = null;
		ArrayList<EntityTypeBean> etList = null;
				
		try {
			sql = "SELECT ID, NOME, DESCRIZIONE, ORDINE "
				  + "FROM TIPO_AUTORITA "
				  + "ORDER BY ORDINE ASC ";
			
			//preparing statement
			etPs = dbUtil.getConnection().prepareStatement(sql);
			//execute
			etRs = etPs.executeQuery();
			
			etList = new ArrayList<EntityTypeBean>();
	
			//retrieving id list
			while(etRs.next()) {
				EntityTypeBean entity=new EntityTypeBean(etRs.getString("ID")
						, etRs.getString("NOME")
						, etRs.getString("DESCRIZIONE")
						, "", "", "");
				etList.add(entity);
				entity.setOrdine(etRs.getInt("ORDINE"));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (etPs != null) {
				etPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return etList;
	}

	public ArrayList<EntityTypeBean> getTipoContattoList() throws SQLException {

		String sql = "";
		ResultSet etRs = null;
		PreparedStatement etPs = null;
		ArrayList<EntityTypeBean> etList = null;
				
		try {
			sql = "SELECT ID, TIPO, DESCRIZIONE "
				  + "FROM TIPO_CONTATTO "
				  + "ORDER BY TIPO ASC ";
			
			//preparing statement
			etPs = dbUtil.getConnection().prepareStatement(sql);
			//execute
			etRs = etPs.executeQuery();
			
			etList = new ArrayList<EntityTypeBean>();
	
			//retrieving id list
			while(etRs.next()) {
				etList.add(new EntityTypeBean(etRs.getString("ID")
											, etRs.getString("TIPO")
											, etRs.getString("DESCRIZIONE")
											, "", "", ""));
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (etPs != null) {
				etPs.close();
			}
			
			dbUtil.closeConnection();
		}
		
		return etList;
	}
}
