package it.cinqueemmeinfo.dammap.dao.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.ProgettoGetioneBean;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class ProgettoGestioneDao {

	private static final Logger logger = LoggerFactory.getLogger(ProgettoGestioneDao.class);

	@Autowired
	private DBUtility dbUtil;

	private static final class Field {
		private static final String NUMERO_ARCHIVIO = "NUMERO_ARCHIVIO";
		private static final String SUB = "SUB";
		private static final String CANCELLATO = "CANCELLATO";
		private static final String ID_UTENTE = "ID_UTENTE";
		@SuppressWarnings("unused")
		private static final String DATA_MODIFICA = "DATA_MODIFICA";

		private static final String DATA_PRESENTAZIONE = "DATA_PRESENTAZIONE";
		private static final String DATA_PARERE = "DATA_PARERE";
		private static final String DATA_APPROVAZIONE = "DATA_APPROVAZIONE";
		private static final String DATA_RILIEVO = "DATA_RILIEVO";

		private static final String DATA_PRESENTAZIONE_STR = "nvl(to_char(DATA_PRESENTAZIONE,'dd/mm/yyyy'), '') AS DATA_PRESENTAZIONE";
		private static final String DATA_PARERE_STR = "nvl(to_char(DATA_PARERE,'dd/mm/yyyy'), '') AS DATA_PARERE";
		private static final String DATA_APPROVAZIONE_STR = "nvl(to_char(DATA_APPROVAZIONE,'dd/mm/yyyy'), '') AS DATA_APPROVAZIONE";
		private static final String DATA_RILIEVO_STR = "nvl(to_char(DATA_RILIEVO,'dd/mm/yyyy'), '') AS DATA_RILIEVO";
		private static final String VOLUME_TOTALE = "VOLUME_TOTALE";
		private static final String NOTE = "NOTE";
	}

	private static class Table {
		private static final String DAO = "PROGETTI_GESTIONE";
	}

	private final static String FIND_ONE_WITHOUT_ID_FROM = "SELECT " + Field.DATA_APPROVAZIONE_STR + ", " + Field.ID_UTENTE + ", " + Field.DATA_PARERE_STR
			+ ", " + Field.DATA_PRESENTAZIONE_STR + ", " + Field.DATA_RILIEVO_STR + ", " + Field.VOLUME_TOTALE + ", " + Field.NOTE + " FROM " + Table.DAO
			+ " WHERE (" + Table.DAO + "." + Field.CANCELLATO + " = " + "'N' OR " + Table.DAO + "." + Field.CANCELLATO + " = " + "'n') ";

	private static final String INSERT_ONE_PROGETTO_GESTIONE = "INSERT INTO " + Table.DAO
			+ " (NUMERO_ARCHIVIO, SUB, DATA_PRESENTAZIONE, DATA_PARERE, DATA_APPROVAZIONE, DATA_RILIEVO, VOLUME_TOTALE, NOTE, " + Field.ID_UTENTE + ")"
			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public ProgettoGetioneBean getProgettoGestioneByDamID(String damId) throws SQLException, EntityNotFoundException {
		ProgettoGetioneBean result = null;
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damId, Field.NUMERO_ARCHIVIO, Field.SUB);
		PreparedStatement siPs = null;
		ResultSet rs = null;
		Connection conn = null;

		String sql = FIND_ONE_WITHOUT_ID_FROM + " AND " + Table.DAO + "." + Field.SUB + " = '" + primaryKey.get(Field.SUB) + "' AND " + Table.DAO + "."
				+ Field.NUMERO_ARCHIVIO + " = " + primaryKey.get(Field.NUMERO_ARCHIVIO);

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("========== SQL ==========\n {}", sql);
			}
			siPs = conn.prepareStatement(sql);
			rs = siPs.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			if (rs.next()) {
				result = fillOutBean(rs, primaryKey);
			} else {
				//throw new EntityNotFoundException("Couldn't find entity with the provided params");
				result = new ProgettoGetioneBean();
			}
		} catch (SQLException sqle) {
			logger.error("\nError while executing '{}' \nMessage: {}", sql, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, siPs);
		}

		return result;
	}

	public void updateProgettoGestione(ProgettoGetioneBean bean, String userId, String damID) throws SQLException {
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damID);

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(DBUtility.logicalDeleteSql(Table.DAO));
			ps.setLong(1, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
			ps.setString(2, (String) primaryKey.get(Field.SUB));
			ps.executeUpdate();
			ps.close();
			ps = conn.prepareStatement(INSERT_ONE_PROGETTO_GESTIONE);
			ps.setLong(1, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
			ps.setString(2, (String) primaryKey.get(Field.SUB));
			try {
				ps.setDate(3, BeanUtility.convertStringToDate(bean.getDataPresentazione()));
			} catch (ParseException pe) {
				ps.setNull(3, Types.DATE);
			} catch (NullPointerException e) {
				ps.setNull(3, Types.DATE);
			}
			try {
				ps.setDate(4, BeanUtility.convertStringToDate(bean.getDataParere()));
			} catch (ParseException e) {
				ps.setNull(4, Types.DATE);
			} catch (NullPointerException e) {
				ps.setNull(4, Types.DATE);
			}
			try {
				ps.setDate(5, BeanUtility.convertStringToDate(bean.getDataApprovazione()));
			} catch (ParseException e) {
				ps.setNull(5, Types.DATE);
			} catch (NullPointerException e) {
				ps.setNull(5, Types.DATE);
			}
			try {
				ps.setDate(6, BeanUtility.convertStringToDate(bean.getDataRilievo()));
			} catch (ParseException e) {
				ps.setNull(6, Types.DATE);
			} catch (NullPointerException e) {
				ps.setNull(6, Types.DATE);
			}
			if (bean.getVolumeTotale() != null) {
				ps.setDouble(7, bean.getVolumeTotale());
			} else {
				ps.setNull(7, Types.DOUBLE);
			}
			ps.setString(8, bean.getNote());
			ps.setString(9, userId);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
	}

	private ProgettoGetioneBean fillOutBean(ResultSet rs, Map<String, Object> primaryKey) throws SQLException {
		ProgettoGetioneBean result = new ProgettoGetioneBean();

		result.setDataApprovazione(rs.getString(Field.DATA_APPROVAZIONE));
		result.setDataParere(rs.getString(Field.DATA_PARERE));
		result.setDataPresentazione(rs.getString(Field.DATA_PRESENTAZIONE));
		result.setDataRilievo(rs.getString(Field.DATA_RILIEVO));
		result.setId("" + primaryKey.get(Field.NUMERO_ARCHIVIO) + primaryKey.get(Field.SUB));
		result.setNote(rs.getString(Field.NOTE));
		rs.getDouble(Field.VOLUME_TOTALE);
		result.setVolumeTotale(rs.wasNull() ? null : rs.getDouble(Field.VOLUME_TOTALE));
		result.setLastModifiedUserId(rs.getString(Field.ID_UTENTE));

		logger.debug("Bean: {}" + result.toString());

		return result;
	}
}