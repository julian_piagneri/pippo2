package it.cinqueemmeinfo.dammap.dao.entities;

import it.cinqueemmeinfo.dammap.bean.entities.VistaBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class VistaDao implements Serializable {

	@Autowired
	private DBUtility dbUtil;
	//@Autowired private BeanUtility bnUtil;
	private static final long serialVersionUID = 906472673246974813L;
	private static final Logger logger = LoggerFactory.getLogger(VistaDao.class);
	
	/**
	 * 
	 * @param targetEntity
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public VistaBean retrieveViewsForUser(Long userId, boolean closeConnection) throws SQLException {
		
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		VistaBean element= null;
		
		try {
			//SELECT
				sql = " SELECT a.ID AS id, a.NAME AS name, a.DEFAULT_VIEW AS defaultView, a.STATUS AS status, "
						+ " a.ID_UTENTE AS idUtente "
						+ " FROM UTENTE_VISTA a "
						+ " WHERE a.ID_UTENTE = ? "; 
		
			//SET QUERY PARAM
			conn=dbUtil.getNewConnection();
			ps=conn.prepareStatement(sql);
			int i=1;
			ps.setLong(i++, userId);
			//EXECUTE
			rs=ps.executeQuery();
			if(rs.next()){
				element=this.fillVistaBean(rs);
			} 
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} catch (JsonProcessingException e) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + e.getMessage());
			e.printStackTrace();
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
		
		return element;
	}
	
	@SuppressWarnings("unused")
	private List<VistaBean> fillVistaBeanList(ResultSet rs) throws SQLException, JsonProcessingException, IOException{
		List<VistaBean> values= new ArrayList<VistaBean>();
		while (rs.next()) {
			VistaBean vistaBean=this.fillVistaBean(rs);
			values.add(vistaBean);
		}
		return values;
	}
	
	private VistaBean fillVistaBean(ResultSet rs) throws SQLException, JsonProcessingException, IOException{
		VistaBean vistaBean=new VistaBean();
		vistaBean.setId(rs.getLong("id"));
		vistaBean.setIdUtente(rs.getLong("idUtente"));
		vistaBean.setName(rs.getString("name"));
		vistaBean.setDefaultView(rs.getInt("defaultView"));
		ObjectMapper mapper = new ObjectMapper();
	    JsonNode json = mapper.readTree(rs.getString("status"));
		vistaBean.setStatus(json);
		return vistaBean;
	}

	public boolean saveViewsForUser(Long userId, JsonNode viste, boolean closeConnection) throws SQLException {
		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		Connection conn=null;
		Boolean done = false;
		PreparedStatement dmPs = null;

		//recupera le viste dell'utente
		VistaBean element=this.retrieveViewsForUser(userId, Boolean.TRUE);
		
		try {
			//Controlla se ancora non è stata creata la riga vista per l'utente
			conn=dbUtil.getNewConnection();
			if(element==null){
				sql="INSERT INTO UTENTE_VISTA(STATUS, ID_UTENTE) VALUES (?, ?)";
				//Retrieving connection
				dmPs = conn.prepareStatement(sql);
				paramsSet=this.setViewsParameter(dmPs, viste.toString(), paramsSet);
				dmPs.setLong(paramsSet++, userId.longValue());
			} else {
				sql="UPDATE UTENTE_VISTA SET STATUS=? WHERE ID=?";
				dmPs = conn.prepareStatement(sql);
				paramsSet=this.setViewsParameter(dmPs, viste.toString(), paramsSet);
				dmPs.setLong(paramsSet++, element.getId());
			}
			
			updatedRows += dmPs.executeUpdate();

			if (updatedRows > 0) {
				done = true;
			} 
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn);
			DBUtility.closeQuietly(dmPs);
		}
		
		return done;
	}
	
	private int setViewsParameter(PreparedStatement dmPs, String value, int paramsSet) throws SQLException{
//		StringReader reader = new StringReader(value);
//		dmPs.setCharacterStream(paramsSet++, reader, value.length());
//		return paramsSet;
		return DBUtility.setClobParameter(dmPs, value, paramsSet);
	}

	public JsonNode retrieveBaseView(Boolean closeConnection) throws SQLException {
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		JsonNode element= null;
		
		try {
			//Carica la vista base (non si può caricare nella stessa query perchè possono esserci utenti senza righe
			sql="SELECT a.VIEWCONTENT AS baseView FROM UTENTE_VISTE_BASE a"; //WHERE a.ID=?";
			//int i=1;
			conn=dbUtil.getNewConnection();
			ps=conn.prepareStatement(sql);
			//ps.setLong(i++, 1);
			rs=ps.executeQuery();
			if(rs.next()){
				ObjectMapper mapper=new ObjectMapper();
				String baseView=rs.getString("baseView");
				element = mapper.readTree(baseView);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} catch (JsonProcessingException e) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + e.getMessage());
			e.printStackTrace();
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
		
		return element;
	}
}
