package it.cinqueemmeinfo.dammap.dao.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.ClassificazioneSismicaDigaBean;
import it.cinqueemmeinfo.dammap.bean.entities.SnellezzaVunerabilitaBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class SnellezzaVunerabilitaDao {

	private static final Logger logger = LoggerFactory.getLogger(SnellezzaVunerabilitaDao.class);

	@Autowired
	private DBUtility dbUtil;

	private static final class Field {
		private static final String NUMERO_ARCHIVIO = "NUMERO_ARCHIVIO";
		private static final String SUB = "SUB";
		private static final String CANCELLATO = "CANCELLATO";
		private static final String ID_UTENTE = "ID_UTENTE";
		private static final String DATA_MODIFICA = "DATA_MODIFICA";

		private static final String COEFICIENTE_SNELLEZZA_CL = "COEFICIENTE_SNELLEZZA_CL";
		private static final String CL_CRITICO = "CL_CRITICO";
		private static final String PGAC_FILTRAMENTO = "PGAC_FILTRAMENTO";
		private static final String PGAC_FESSURAZIONE = "PGAC_FESSURAZIONE";
		private static final String PGAC_SCORRIMENTO = "PGAC_SCORRIMENTO";
		private static final String DISCRIMINATORE = "DISCRIMINATORE";

		private static final String CLASS_SIS_DIGA = "CLASS_SIS";
	}

	public static final String TIPO_SNELLEZZA = "S";
	public static final String TIPO_VULNERABILITA = "V";

	private static final class Table {
		private static final String DAO = "SNELLEZZA_VUENRABILITA";
		private static final String CLASSIFICAZIONE = "CLAS_SISMICA_DIGA";
	}

	private static final String FIND_ALL_BY_ID = "SELECT " + Field.CL_CRITICO + ", " + Field.COEFICIENTE_SNELLEZZA_CL + ", " + Field.DISCRIMINATORE + ", "
			+ Field.PGAC_FESSURAZIONE + ", " + Field.PGAC_FILTRAMENTO + ", " + Field.PGAC_SCORRIMENTO + ", " + Field.DATA_MODIFICA + ", " + Field.ID_UTENTE
			+ " FROM " + Table.DAO + " WHERE (" + Table.DAO + "." + Field.CANCELLATO + " = " + "'N' OR " + Table.DAO + "." + Field.CANCELLATO + " = " + "'n') ";

	private static final String INSERT_ONE_SNELLEZZA_VUNERABILITA = "INSERT INTO SNELLEZZA_VUENRABILITA "
			+ "(NUMERO_ARCHIVIO, SUB, COEFICIENTE_SNELLEZZA_CL, CL_CRITICO, PGAC_FILTRAMENTO, PGAC_FESSURAZIONE, PGAC_SCORRIMENTO, DISCRIMINATORE, ID_UTENTE)"
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String GET_CLASSIFICAZIONE_BY_DAMID = "SELECT T." + Field.CLASS_SIS_DIGA + ", T." + Field.ID_UTENTE + ", T." + Field.DATA_MODIFICA
			+ " FROM " + Table.CLASSIFICAZIONE + " T WHERE (T." + Field.CANCELLATO + " = 'n' OR T. " + Field.CANCELLATO + " = 'N') AND T."
			+ Field.NUMERO_ARCHIVIO + " = ? AND T." + Field.SUB + " = ?";

	private static final String INSERT_ONE_CLASSIFICAZIONE = "INSERT INTO " + Table.CLASSIFICAZIONE + " (" + Field.CLASS_SIS_DIGA + ", " + Field.ID_UTENTE
			+ ", " + Field.NUMERO_ARCHIVIO + ", " + Field.SUB + ") VALUES (?, ?, ?, ?)";

	public ClassificazioneSismicaDigaBean getClassificazioneSismicaDigaByDamId(String damId) throws SQLException {
		ClassificazioneSismicaDigaBean result = null;
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damId);

		PreparedStatement prPs = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("========== SQL ==========\n {}", GET_CLASSIFICAZIONE_BY_DAMID);
			}
			prPs = conn.prepareStatement(GET_CLASSIFICAZIONE_BY_DAMID);
			prPs.setLong(1, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
			prPs.setString(2, (String) primaryKey.get(Field.SUB));
			rs = prPs.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			if (rs.next()) {
				result = fillOutClassificazioneSismicaDigaBean(rs, primaryKey);
			} else {
				result = new ClassificazioneSismicaDigaBean();
				// throw new EntityNotFoundException("Couldn't find entity with
				// the provided params");
			}
		} catch (SQLException sqle) {
			logger.error("\nError while executinge '{}' \nMessage: {}", GET_CLASSIFICAZIONE_BY_DAMID, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, prPs);
		}

		return result;
	}

	public SnellezzaVunerabilitaBean getSnellezzaVunerabilitaByDamID(String damId) throws SQLException, EntityNotFoundException {
		SnellezzaVunerabilitaBean result = null;
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damId, Field.NUMERO_ARCHIVIO, Field.SUB);

		PreparedStatement prPs = null;
		ResultSet rs = null;
		Connection conn = null;

		String sql = FIND_ALL_BY_ID + " AND " + Table.DAO + "." + Field.SUB + " = '" + primaryKey.get(Field.SUB) + "' AND " + Table.DAO + "."
				+ Field.NUMERO_ARCHIVIO + " = " + primaryKey.get(Field.NUMERO_ARCHIVIO);

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("========== SQL ==========\n {}", sql);
			}
			prPs = conn.prepareStatement(sql);
			rs = prPs.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			if (rs.next()) {
				result = fillOutBean(rs, primaryKey);
			} else {
				result = new SnellezzaVunerabilitaBean();
				// throw new EntityNotFoundException("Couldn't find entity with
				// the provided params");
			}
		} catch (SQLException sqle) {
			logger.error("\nError while executinge '{}' \nMessage: {}", sql, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, prPs);
		}

		return result;
	}

	public void updateSnellezzaVunerabilita(SnellezzaVunerabilitaBean bean, String damID, String userId) throws SQLException {
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damID);

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		int count = 0;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(DBUtility.logicalDeleteSql(Table.DAO));
			ps.setLong(1, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
			ps.setString(2, (String) primaryKey.get(Field.SUB));
			ps.executeUpdate();
			ps.close();
			ps = conn.prepareStatement(INSERT_ONE_SNELLEZZA_VUNERABILITA);
			ps.setLong(++count, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
			ps.setString(++count, (String) primaryKey.get(Field.SUB));
			if (bean.getCoeficienteSnellezzaCL() != null) {
				ps.setDouble(++count, bean.getCoeficienteSnellezzaCL());
			} else {
				ps.setNull(++count, Types.DOUBLE);
			}
			if (bean.getCLCritico() != null) {
				ps.setDouble(++count, bean.getCLCritico());
			} else {
				ps.setNull(++count, Types.DOUBLE);
			}
			if (bean.getPgacFiltramento() != null) {
				ps.setDouble(++count, bean.getPgacFiltramento());
			} else {
				ps.setNull(++count, Types.DOUBLE);
			}
			if (bean.getPgacFessurazione() != null) {
				ps.setDouble(++count, bean.getPgacFessurazione());
			} else {
				ps.setNull(++count, Types.DOUBLE);
			}
			if (bean.getPgacScorrimento() != null) {
				ps.setDouble(++count, bean.getPgacScorrimento());
			} else {
				ps.setNull(++count, Types.DOUBLE);
			}
			ps.setString(++count, bean.getDiscriminatore());
			ps.setString(++count, userId);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
	}

	public void updateClassificazioneSismicaDiga(ClassificazioneSismicaDigaBean bean, String damID, String userId) throws SQLException {
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damID);

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		int count = 0;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(DBUtility.logicalDeleteSql(Table.CLASSIFICAZIONE));
			ps.setLong(1, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
			ps.setString(2, (String) primaryKey.get(Field.SUB));
			ps.executeUpdate();
			ps.close();
			ps = conn.prepareStatement(INSERT_ONE_CLASSIFICAZIONE);
			if (bean.getClassificazioneSismicaDiga() != null) {
				ps.setLong(++count, bean.getClassificazioneSismicaDiga());
			} else {
				ps.setNull(++count, Types.NUMERIC);
			}
			ps.setString(++count, userId);
			ps.setLong(++count, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
			ps.setString(++count, (String) primaryKey.get(Field.SUB));
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
	}

	private ClassificazioneSismicaDigaBean fillOutClassificazioneSismicaDigaBean(ResultSet rs, Map<String, Object> primaryKey) throws SQLException {
		ClassificazioneSismicaDigaBean result = new ClassificazioneSismicaDigaBean();

		if (logger.isDebugEnabled()) {
			logger.debug("Filling out the {}...", Table.CLASSIFICAZIONE);
		}

		result.setId("" + primaryKey.get(Field.NUMERO_ARCHIVIO) + primaryKey.get(Field.SUB));
		result.setIsDeleted("N");
		result.setLastModifiedDate(rs.getString(Field.DATA_MODIFICA));
		result.setLastModifiedUserId(rs.getString(Field.ID_UTENTE));

		long value = rs.getLong(Field.CLASS_SIS_DIGA);
		result.setClassificazioneSismicaDiga(rs.wasNull() ? null : value);

		return result;
	}

	private SnellezzaVunerabilitaBean fillOutBean(ResultSet rs, Map<String, Object> primaryKey) throws SQLException {
		SnellezzaVunerabilitaBean result = new SnellezzaVunerabilitaBean();

		logger.debug("Filling out the {}...", Table.DAO);

		result.setDiscriminatore(rs.getString(Field.DISCRIMINATORE));
		result.setId("" + primaryKey.get(Field.NUMERO_ARCHIVIO) + primaryKey.get(Field.SUB));
		result.setIsDeleted("N");
		result.setLastModifiedDate(rs.getString(Field.DATA_MODIFICA));
		result.setLastModifiedUserId(rs.getString(Field.ID_UTENTE));

		fillOutSnellezza(rs, result);
		fillOutVulnerabilita(rs, result);

		return result;
	}

	private void fillOutSnellezza(ResultSet rs, SnellezzaVunerabilitaBean result) throws SQLException {
		rs.getDouble(Field.CL_CRITICO);
		result.setCLCritico(rs.wasNull() ? null : rs.getDouble(Field.CL_CRITICO));
		rs.getDouble(Field.COEFICIENTE_SNELLEZZA_CL);
		result.setCoeficienteSnellezzaCL(rs.wasNull() ? null : rs.getDouble(Field.COEFICIENTE_SNELLEZZA_CL));
	}

	private void fillOutVulnerabilita(ResultSet rs, SnellezzaVunerabilitaBean result) throws SQLException {
		rs.getDouble(Field.PGAC_FESSURAZIONE);
		result.setPgacFessurazione(rs.wasNull() ? null : rs.getDouble(Field.PGAC_FESSURAZIONE));
		rs.getDouble(Field.PGAC_FILTRAMENTO);
		result.setPgacFiltramento(rs.wasNull() ? null : rs.getDouble(Field.PGAC_FILTRAMENTO));
		rs.getDouble(Field.PGAC_SCORRIMENTO);
		result.setPgacScorrimento(rs.wasNull() ? null : rs.getDouble(Field.PGAC_SCORRIMENTO));
	}
}