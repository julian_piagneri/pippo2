package it.cinqueemmeinfo.dammap.dao.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.OndaPienaBean;
import it.cinqueemmeinfo.dammap.bean.entities.TipoOndePienaBean;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class OndaPienaDao {

	private static final Logger logger = LoggerFactory.getLogger(OndaPienaDao.class);

	@Autowired
	private DBUtility dbUtil;

	private static final String NUMERO_ARCHIVIO = "NUMERO_ARCHIVIO";
	private static final String SUB = "SUB";

	private static final String FIND_ALL_ONDE_PIENA = "SELECT T.DATA_STUDIO, T.DATA_ESAME, T.ESISTO_PARERE, T.DATA_PRESA_VISIONE, T.ID_TIPO, "
			+ "T.ID_UTENTE, T.DATA_MODIFICA FROM ONDA T " + "WHERE UPPER(T.CANCELLATO) = 'N' " + "AND T.NUMERO_ARCHIVIO = ? " + "AND T.SUB = ?";

	private static final String INSERT_ONDA = "INSERT INTO ONDA (NUMERO_ARCHIVIO, SUB, DATA_STUDIO, DATA_ESAME, ESISTO_PARERE, DATA_PRESA_VISIONE, ID_TIPO, ID_UTENTE)"
			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String GET_ALL_TIPO_ONDE_PIENA = "SELECT T.ID, T.TIPO, T.NOME, T.KEY, T.ONDA_LAYER, T.DESCRIZIONE FROM TIPO_ONDA_PIENA T";

	private static final String FIND_ONDE_PIENA_INFO = "SELECT T.DATA_MODIFICA, T.ID_UTENTE, T.PORTATA_TRANSITABILE" + " FROM ONDA_INFO T"
			+ " WHERE UPPER(T.CANCELLATO) = 'N' ";

	private static final String INSERT_ONE_ONDE_PIENA_INFO = "INSERT INTO ONDA_INFO (NUMERO_ARCHIVIO, SUB, PORTATA_TRANSITABILE, ID_UTENTE)"
			+ " VALUES (?, ?, ?, ?)";

	public void updateAllOndePienaByDamID(List<OndaPienaBean> beanList, String damID, String userId)
			throws SQLException, ParseException, EntityNotFoundException {
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damID);
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		int parameterIndex;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			conn.setAutoCommit(false);
			final int SIZE = beanList.size();
			for (int i = 0; i < SIZE; ++i) {
				parameterIndex = 0;
				ps = conn.prepareStatement(DBUtility.logicalDeleteSql("ONDA"));
				ps.setLong(++parameterIndex, (Long) primaryKey.get(NUMERO_ARCHIVIO));
				ps.setString(++parameterIndex, (String) primaryKey.get(SUB));
				ps.executeUpdate();
			}
			ps.close();
			for (OndaPienaBean bean : beanList) {
				parameterIndex = 0;
				ps = conn.prepareStatement(INSERT_ONDA);
				ps.setLong(++parameterIndex, (Long) primaryKey.get(NUMERO_ARCHIVIO));
				ps.setString(++parameterIndex, (String) primaryKey.get(SUB));
				ps.setDate(++parameterIndex, BeanUtility.convertStringToDate(bean.getDataStudio()));
				ps.setDate(++parameterIndex, BeanUtility.convertStringToDate(bean.getDataEsame_ParereDGdighe()));
				ps.setString(++parameterIndex, bean.getEsitoParere());
				ps.setDate(++parameterIndex, BeanUtility.convertStringToDate(bean.getDataPresaVisione()));
				ps.setLong(++parameterIndex, bean.getIdTipo());
				ps.setString(++parameterIndex, userId);
				ps.executeUpdate();
			}
			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} catch (ParseException pe) {
			DBUtility.newRollback(conn);
			logger.error("\nDate malformed: {}", pe.getMessage());
			throw pe;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
	}

	public List<OndaPienaBean> getAllOndePienaByDamID(String damId) throws SQLException {
		List<OndaPienaBean> result = new ArrayList<OndaPienaBean>();
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damId, NUMERO_ARCHIVIO, SUB);

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		int parameterIndex = 0;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			ps = conn.prepareStatement(FIND_ALL_ONDE_PIENA);
			ps.setLong(++parameterIndex, (Long) primaryKey.get(NUMERO_ARCHIVIO));
			ps.setString(++parameterIndex, (String) primaryKey.get(SUB));
			rs = ps.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			while (rs.next()) {
				result.add(fillOutBean(rs, primaryKey));
			}
			if (result.isEmpty()) {
				result.add(new OndaPienaBean());
			}
		} catch (SQLException sqle) {
			logger.error("\nError while executinge '{}' \nMessage: {}", FIND_ALL_ONDE_PIENA, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}

		return result;
	}

	public OndaPienaBean getOndePienaInfoByDamID(String damId) throws SQLException, EntityNotFoundException {
		OndaPienaBean result = new OndaPienaBean();
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damId, NUMERO_ARCHIVIO, SUB);

		PreparedStatement prPs = null;
		ResultSet rs = null;
		Connection conn = null;

		String sql = FIND_ONDE_PIENA_INFO + " AND T." + NUMERO_ARCHIVIO + " = " + primaryKey.get(NUMERO_ARCHIVIO) + " AND T." + SUB + " = '"
				+ primaryKey.get(SUB) + "'";

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("========== SQL ==========\n {}", sql);
			}
			prPs = conn.prepareStatement(sql);
			rs = prPs.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			if (rs.next()) {
				result = fillOutInfoBean(rs, primaryKey);
			} else {
				throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
		} catch (SQLException sqle) {
			logger.error("\nError while executinge '{}' \nMessage: {}", sql, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, prPs);
		}

		return result;
	}

	public List<TipoOndePienaBean> getAllTipoOndePiena() throws EntityNotFoundException, SQLException {
		List<TipoOndePienaBean> result = new ArrayList<TipoOndePienaBean>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			ps = conn.prepareStatement(GET_ALL_TIPO_ONDE_PIENA);
			rs = ps.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			while (rs.next()) {
				result.add(fillOutTipoBean(rs));
			}
			if (result.isEmpty()) {
				throw new EntityNotFoundException();
			}
		} catch (SQLException sqle) {
			logger.error("\nError while executinge '{}' \nMessage: {}", GET_ALL_TIPO_ONDE_PIENA, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}

		return result;
	}

	public void updateOndaPienaInfo(OndaPienaBean bean, String userId) throws SQLException {
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(bean.getId());

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}

			conn.setAutoCommit(false);

			ps = conn.prepareStatement(DBUtility.logicalDeleteSql("ONDA_INFO"));
			ps.setLong(1, (Long) primaryKey.get(NUMERO_ARCHIVIO));
			ps.setString(2, (String) primaryKey.get(SUB));
			ps.executeUpdate();
			ps.close();
			ps = conn.prepareStatement(INSERT_ONE_ONDE_PIENA_INFO);
			ps.setLong(1, (Long) primaryKey.get(NUMERO_ARCHIVIO));
			ps.setString(2, (String) primaryKey.get(SUB));
			ps.setDouble(3, bean.getPortata());
			ps.setString(4, userId);
			ps.executeUpdate();

			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
	}

	private TipoOndePienaBean fillOutTipoBean(ResultSet rs) throws SQLException {
		TipoOndePienaBean result = new TipoOndePienaBean();

		result.setIdTipo(rs.getLong("ID"));
		result.setNome(rs.getString("NOME"));
		result.setTipo(rs.getString("TIPO"));
		result.setKey(rs.getString("KEY"));
		result.setDescrizione(rs.getString("DESCRIZIONE"));
		result.setOndaLayer(rs.getString("ONDA_LAYER"));

		return result;
	}

	private OndaPienaBean fillOutInfoBean(ResultSet rs, Map<String, Object> primaryKey) throws SQLException {
		OndaPienaBean result = new OndaPienaBean();

		if (logger.isDebugEnabled()) {
			logger.debug("Filling out OndePienaBean in the {}...", this.getClass().getSimpleName());
		}

		rs.getDouble("PORTATA_TRANSITABILE");
		result.setPortata(rs.wasNull() ? null : rs.getDouble("PORTATA_TRANSITABILE"));

		result.setId("" + primaryKey.get(NUMERO_ARCHIVIO) + primaryKey.get(SUB));
		result.setIsDeleted("N");
		result.setLastModifiedDate(rs.getString("DATA_MODIFICA") != null ? rs.getString("DATA_MODIFICA") : "");
		result.setLastModifiedUserId(rs.getString("ID_UTENTE") != null ? rs.getString("ID_UTENTE") : "");

		return result;
	}

	private OndaPienaBean fillOutBean(ResultSet rs, Map<String, Object> primaryKey) throws SQLException {
		OndaPienaBean result = new OndaPienaBean();

		if (logger.isDebugEnabled()) {
			logger.debug("Filling out in the {}...", this.getClass().getSimpleName());
		}

		result.setDataEsame_ParereDGdighe(rs.getString("DATA_ESAME") != null ? rs.getString("DATA_ESAME") : "");
		result.setDataPresaVisione(rs.getString("DATA_PRESA_VISIONE") != null ? rs.getString("DATA_PRESA_VISIONE") : "");
		result.setDataStudio(rs.getString("DATA_STUDIO") != null ? rs.getString("DATA_STUDIO") : "");
		long value = rs.getLong("ID_TIPO");
		result.setIdTipo(rs.wasNull() ? null : value);
		result.setEsitoParere(rs.getString("ESISTO_PARERE") != null ? rs.getString("ESISTO_PARERE") : "");
		result.setId("" + primaryKey.get(NUMERO_ARCHIVIO) + primaryKey.get(SUB));
		result.setIsDeleted("N");
		result.setLastModifiedDate(rs.getString("DATA_MODIFICA") != null ? rs.getString("DATA_MODIFICA") : "");
		result.setLastModifiedUserId(rs.getString("ID_UTENTE") != null ? rs.getString("ID_UTENTE") : "");

		return result;
	}
}