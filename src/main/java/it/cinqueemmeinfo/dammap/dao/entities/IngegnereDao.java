package it.cinqueemmeinfo.dammap.dao.entities;

import it.cinqueemmeinfo.dammap.bean.entities.IngegnereBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;
import it.cinqueemmeinfo.dammap.bean.junction.NominaIngegnereBean;
import it.cinqueemmeinfo.dammap.dao.fields.AddressDao;
import it.cinqueemmeinfo.dammap.dao.fields.ContactInfoDao;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

import java.io.Serializable;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.javaunderground.jdbc.DebugLevel;
import com.javaunderground.jdbc.StatementFactory;


public class IngegnereDao implements Serializable {

	private static final long serialVersionUID = -4450908674680598480L;
	private static final Logger logger = LoggerFactory.getLogger(IngegnereDao.class);
	
	@Autowired private DBUtility dbUtil;
	@Autowired private BeanUtility bnUtil;
	@Autowired private AddressDao adDao;
	@Autowired private ContactInfoDao ciDao;

	public IngegnereBean getIngegnereWithParams(IngegnereBean inBean, boolean closeConnection) throws SQLException, EntityNotFoundException {
		
		List<IngegnereBean> inList = getIngegnereListWithParams(inBean, "", 0, true, closeConnection);
		
		if (inList.size() > 0) {
			return inList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}
	
	public List<IngegnereBean> getIngegnereListWithParams(IngegnereBean inBean, String userId, int hasAccess, Boolean getExtraInfos, Boolean closeConnection) throws SQLException {
		
		int paramsSet = 1;
		String sql = "";
		@SuppressWarnings("unused")
		String whereSql = "";
		IngegnereBean foundInBean = null;
		ArrayList<ContactInfoBean> ciList = null;
		ArrayList<AddressBean> adList = null;
		ArrayList<IngegnereBean> inList = new ArrayList<IngegnereBean>();
		
		ResultSet inRs = null;
		PreparedStatement inPs = null;
		
		try {
			
			//Prende i dettagli per un ingegnere di cui si ha già l'id
			if (!"".equals(inBean.getId()) && inBean.getId() != null) {
				sql = "select ID, NOME, COGNOME, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INGEGNERE "
						+ "WHERE CANCELLATO<>'S' AND ID = ? ";
			} else if (hasAccess==1){
				//Se hasAccess è 1 allora può vedere solo i dati degli ingegneri associati tramite concessionario
//				sql = "SELECT I.ID, I.NOME, I.COGNOME, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO " +  
//						"FROM UTENTE U " + 
//						"JOIN UTENTE_CONCESS UC ON UC.ID_UTENTE = U.ID " + 
//						"JOIN INGEGNERE_CONCESS IC ON UC.ID_CONCESS = IC.ID_CONCESSIONARIO " + 
//						"JOIN INGEGNERE I ON I.ID = IC.ID_INGEGNERE " + 
//						"WHERE U.USERID = ? AND I.CANCELLATO<>'S' AND IC.CANCELLATO<>'S' AND UC.CANCELLATO<>'S' ";
				sql= " SELECT I.ID, I.NOME, I.COGNOME, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO FROM "
						+ " UTENTE U JOIN UTENTE_CONCESS UC ON UC.ID_UTENTE = U.ID "
						+ " JOIN INGEGNERE_CONCESS IC ON UC.ID_CONCESS = IC.ID_CONCESSIONARIO "
						+ " JOIN INGEGNERE I ON I.ID = IC.ID_INGEGNERE "
						+ " WHERE U.USERID = ? AND I.CANCELLATO<>'S' AND IC.CANCELLATO<>'S' AND UC.CANCELLATO<>'S' "
						+ " UNION SELECT I.ID, I.NOME, I.COGNOME, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO FROM "
						+ " UTENTE U JOIN UTENTE_GESTORE UG ON UG.ID_UTENTE = U.ID "
						+ " JOIN DIGA_GESTORE DG ON DG.ID_GESTORE=UG.ID_GESTORE "
						+ " JOIN (SELECT * FROM INGEGNERE_RESPONSABILE UNION SELECT * FROM INGEGNERE_SOSTITUTO) IG "
						+ " ON DG.NUMERO_ARCHIVIO=IG.NUMERO_ARCHIVIO AND DG.SUB=IG.SUB "
						+ " JOIN INGEGNERE I ON I.ID = IG.ID_INGEGNERE "
						+ " WHERE U.USERID = ? AND I.CANCELLATO<>'S' AND IG.CANCELLATO<>'S' AND DG.CANCELLATO<>'S' "
						+ " AND UG.CANCELLATO<>'S' "
						+ " UNION SELECT I.ID, I.NOME, I.COGNOME, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO FROM "
						+ " UTENTE U JOIN UTENTE_CONCESS UC ON UC.ID_UTENTE = U.ID "
						+ " JOIN DIGA_CONCESS DC ON DC.ID_CONCESS=UC.ID_CONCESS "
						+ " JOIN (SELECT * FROM INGEGNERE_RESPONSABILE UNION SELECT * FROM INGEGNERE_SOSTITUTO) IG "
						+ " ON DC.NUMERO_ARCHIVIO=IG.NUMERO_ARCHIVIO AND DC.SUB=IG.SUB "
						+ " JOIN INGEGNERE I  ON I.ID = IG.ID_INGEGNERE "
						+ " WHERE U.USERID = ? AND I.CANCELLATO<>'S' AND IG.CANCELLATO<>'S' AND DC.CANCELLATO<>'S' "
						+ " AND UC.CANCELLATO<>'S'"; 
			} else if(hasAccess>1) { //Altrimenti può vederli tutti
				sql = "SELECT ID, NOME, COGNOME, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
					+ "FROM INGEGNERE WHERE CANCELLATO<>'S' ";
			}
			
			logger.warn(sql);
			
			//WHERE STRING BUILDER
			/*if (whereSql.length() > 0) {
				if (whereSql.startsWith("AND")) {
					whereSql = whereSql.substring(4, whereSql.length());
				}
				
				sql += " WHERE " + whereSql;
			}*/

			//ORDERBY
			//sql += "ORDER BY ID ASC ";

			//preparing statement
			
			DebugLevel dbgl=DebugLevel.ON;
			//inPs = dbUtil.getConnection().prepareStatement(sql);
			inPs = StatementFactory.getStatement(dbUtil.getConnection(),sql,dbgl);
			if (!"".equals(inBean.getId()) && inBean.getId() != null) {
				if (!"".equals(inBean.getId()) && inBean.getId() != null) {
					inPs.setInt(paramsSet++, Integer.parseInt(inBean.getId()));
				}
			} else if (hasAccess==1){
				//Se hasAccess è 1 allora può vedere solo i dati degli ingegneri associati tramite concessionario
				inPs.setString(paramsSet++, userId); //Ingegnere Concess
				inPs.setString(paramsSet++, userId); //Diga gestore
				inPs.setString(paramsSet++, userId); //Diga concess
			}
//			//Setting query parameters
//			if ("".equals(userId)) {
//				
//				/*if (!"".equals(inBean.getNome()) && inBean.getNome() != null) {
//					inPs.setString(paramsSet++, inBean.getNome());
//				}
//				if (!"".equals(inBean.getCognome()) && inBean.getCognome() != null) {
//					inPs.setString(paramsSet++, inBean.getCognome());
//				}*/
//			} else {
//				if (!"".equals(userId) && userId != null) {
//					
//				}
//			}
			
			//execute
			inRs = inPs.executeQuery();
			
			while (inRs.next()) {
				
				foundInBean = new IngegnereBean(inRs.getString("ID")
													, inRs.getString("NOME")
													, inRs.getString("COGNOME")
													, null, null
													, inRs.getString("DATA_MODIFICA")
													, inRs.getString("ID_UTENTE")
													, inRs.getString("CANCELLATO"));
				
				if (getExtraInfos) {
					//retrieve phone numbers
					ciList = (ArrayList<ContactInfoBean>) ciDao.getContactInfoListWithParams(foundInBean, false);
					//retrieve address list
					adList = (ArrayList<AddressBean>) adDao.getAddressListWithParams(foundInBean, false);
					
					foundInBean.setIndirizzo(adList);
					foundInBean.setContatto(ciList);
				}

				//ADD NEW ITEM
				inList.add(foundInBean);
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (inPs != null) {
				inPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return inList;
	}

	/**
	 * 
	 * @param inBean
	 * @param userId
	 * @param closeConnection if true closes connection and commmits changes, if false transaction will be kept open for futher operations
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException 
	 */
	public IngegnereBean createIngegnere(IngegnereBean inBean, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {
		
		String sql = "";
		String idConcess = "";
		String idIngConc = "";
		int paramsSet = 1;
		int updatedRows = 0;
		ResultSet inRs = null;
		//ResultSet inNextValRs = null;
		PreparedStatement inPs = null;
		IngegnereBean createdInBean = null;
		
		try {

			//Starting transaction 
			dbUtil.setAutoCommit(false);
			
			//Get entity id
			sql = "select INGEGNERE_SEQUENZA.nextval from dual";
			inRs = dbUtil.getConnection().createStatement().executeQuery(sql);
			
			if (inRs != null && inRs.next()) {
				inBean.setId(inRs.getString(1));
				inRs.close();
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}
			
			//INSERT INGEGNERE
			sql = "insert into INGEGNERE (ID, NOME, COGNOME, ID_UTENTE ) values ( ?,?,?,? )";
			
			inPs = dbUtil.getConnection().prepareStatement(sql);
			
			inPs.setString(paramsSet++, inBean.getId()); //ID
			inPs.setString(paramsSet++, inBean.getNome()); //NOME
			inPs.setString(paramsSet++, inBean.getCognome()); //COGNOME
			inPs.setString(paramsSet++, userId); //ID_UTENTE
			
			updatedRows = inPs.executeUpdate();
			
			if (updatedRows > 0) {
				//INSERT CONTACT INFO
				for (ContactInfoBean pnBean : inBean.getContatto()) {
					ciDao.createContactInfo(pnBean, inBean, userId, false);
				}
				
				//INSERT ADDRESS
				for (AddressBean adBean : inBean.getIndirizzo()) {
					adDao.createAddress(adBean, inBean, userId, false);
				}
				
				//RETRIEVING CREATED ROW
				createdInBean = getIngegnereWithParams(inBean, false);
				
				//CHECK IF LOGGED USER HAS A CONCESSIONARIO ATTACHED 
				sql = "SELECT UC.ID_CONCESS FROM UTENTE U LEFT JOIN UTENTE_CONCESS UC ON UC.ID_UTENTE = U.ID WHERE U.USERID = ? AND UC.CANCELLATO<>'S' ";
				
				inPs = dbUtil.getConnection().prepareStatement(sql);
				inPs.setString(1, userId); //ID_UTENTE
				inRs = inPs.executeQuery();
				
				if (inRs != null && inRs.next()) {
					idConcess = inRs.getString(1);
					inRs.close();
				}
				
				//IF USER HAS A CONCESSIONARIO ATTACHED CREATE A JUNCTION BETWEEN THE INGEGNERE AND THE CONCESSIONARIO
				if (idConcess != null && !"".equals(idConcess)) {

					//Get INGEGNERE_CONCESS nextval
					sql = "select INGEGNERE_CONCESS_SEQUENZA.nextval from dual";
					inRs = dbUtil.getConnection().createStatement().executeQuery(sql);
					
					if (inRs != null && inRs.next()) {
						idIngConc = inRs.getString(1);
						inRs.close();
					} else {
						throw new SQLException("Couldn't retrieve next sequence value.");
					}
					
					//INSERT INGEGNERE_CONCESS
					sql = "insert into INGEGNERE_CONCESS (ID, ID_INGEGNERE, ID_CONCESSIONARIO, ID_UTENTE_MOD ) values ( ?,?,?,? )";
					
					inPs = dbUtil.getConnection().prepareStatement(sql);
					paramsSet = 1;
					inPs.setString(paramsSet++, idIngConc); //ID
					inPs.setString(paramsSet++, inBean.getId()); //ID_INGEGNERE
					inPs.setString(paramsSet++, idConcess); //ID_CONCESSIONARIO
					inPs.setString(paramsSet++, userId); //ID_UTENTE_MOD
					
					updatedRows = inPs.executeUpdate();
				}
				
			}  else {
				throw new SQLException("Insert not executed, error while creating record");
			}
			
			//IF ROWS HAVE BEEN UPDATED COMMIT CHANGES
			if (closeConnection) {
				dbUtil.getConnection().commit();
			}

		} catch (SQLException sqle) {
			if (closeConnection) {
				dbUtil.rollback();
			}
			logger.error("SQLException with message: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (inRs != null) {
				inRs.close();
			}
			if (inPs != null) {
				inPs.close();
			}
			
			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}
		
		return createdInBean;
	}
	
	/**
	 * 
	 * @param inBean
	 * @param userId
	 * @param closeConnection if true closes connection and commmits changes, if false transaction will be kept open for futher operations
	 * @return
	 * @throws Exception 
	 */
	public IngegnereBean updateIngegnere(IngegnereBean inBean, String userId, boolean closeConnection) throws Exception {
		
		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement inPs = null;
		IngegnereBean foundInBean = null;
		
		try {
			
			sql = "update INGEGNERE set NOME=?, COGNOME=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=?";
			
			//Retrieving connection
			dbUtil.setAutoCommit(false);
			inPs = dbUtil.getConnection().prepareStatement(sql);
			
			inPs.setString(paramsSet++, inBean.getNome()); //NOME
			inPs.setString(paramsSet++, inBean.getCognome()); //COGNOME
			inPs.setString(paramsSet++, userId); //ID_UTENTE
			inPs.setInt(paramsSet++, Integer.parseInt(inBean.getId())); //ID
			
			updatedRows += inPs.executeUpdate();
			
			if(updatedRows > 0) {
				//UPDATE CONTACT INFO
				if (inBean.getContatto() != null) {
					for (ContactInfoBean pnBean : inBean.getContatto()) {
						ciDao.updateContactInfo(pnBean, inBean, userId, false); 
					}
				}
				//UPDATE ADDRESS
				if (inBean.getIndirizzo() != null) {
					for (AddressBean adBean : inBean.getIndirizzo()) {
						adDao.updateAddress(adBean, inBean, userId, false);
					}
				}
				//RETRIEVE UPDATED ROW
				foundInBean = getIngegnereWithParams(inBean, false);
			} else {
				throw new EntityNotFoundException("Entity with ID="+inBean.getId()+" not found.");
			}
			
			//COMMIT
			if (closeConnection) {
				dbUtil.getConnection().commit();
			}
			
		} catch (Exception e) {
			logger.error("Error while executing '" + sql + "'");
			if (closeConnection) {
				dbUtil.getConnection().rollback();
			}
			throw e;
		} finally {
			if (inPs != null) {
				inPs.close();
			}
			
			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}
		
		return foundInBean;
	}
	
	/**
	 * 
	 * @param entityId
	 * @param userId
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public Boolean removeIngegnere(String entityId, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {
		
		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement inPs = null;
		Boolean wasDeleted = false;
		
		try {
			
			sql = "update INGEGNERE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=?";
			
			//Retrieving connection
			inPs = dbUtil.getConnection().prepareStatement(sql);
			inPs.setString(paramsSet++, userId); //ID_UTENTE
			inPs.setString(paramsSet++, "S"); //CANCELLATO
			inPs.setInt(paramsSet++, Integer.parseInt(entityId)); //ID
			
			updatedRows += inPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Entity with ID="+entityId+" not found.");
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (inPs != null) {
				inPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return wasDeleted;
	}
	
	private NominaIngegnereBean getNominaIngegnere(String sql, NominaIngegnereBean inBean, boolean closeConnection) throws SQLException {
		
		int paramsSet = 1;
		ResultSet inRs = null;
		PreparedStatement inPs = null;
		NominaIngegnereBean foundInBean = null;
		
		try {
			
			//preparing statement
			inPs = dbUtil.getConnection().prepareStatement(sql);
			inPs.setInt(paramsSet++, inBean.getNumeroArchivio()); //NUMERO_ARCHIVIO
			inPs.setString(paramsSet++, inBean.getSub()); //SUB
			inPs.setInt(paramsSet++, Integer.parseInt(inBean.getId())); //ID_INGEGNERE
			
			//execute
			inRs = inPs.executeQuery();
			
			if (inRs.next()) {
				foundInBean = new NominaIngegnereBean(inRs.getString("ID_INGEGNERE") //ID_INGEGNERE
						, inRs.getInt("PROGRESSIVO") //PROGRESSIVO
						, inRs.getInt("NUMERO_ARCHIVIO") //NUMERO_ARCHIVIO
						, inRs.getString("SUB") //SUB
						, inRs.getString("DATA_NOMINA") //DATA_NOMINA
						, inRs.getString("DATA_MODIFICA") //DATA_MODIFICA
						, inRs.getString("ID_UTENTE") //ID_UTENTE
						, "N");
				
			}
				
		} catch (SQLException sqle) {
			logger.error("SQLException with message: " + sqle.getMessage());
		} finally {
			if (closeConnection) {
				inPs.close();
			}
		}
		
		return foundInBean;
	}
	
	public NominaIngegnereBean setIngegnereAsResponsabileToDam(String damId, NominaIngegnereBean inBean, String userId, boolean closeConnection) throws Exception {
		
		String nextValSql = "select INGEGNERE_RESP_SEQUENZA.nextval from dual";
		String insertSql = "insert into INGEGNERE_RESPONSABILE (PROGRESSIVO, NUMERO_ARCHIVIO, SUB, ID_INGEGNERE, DATA_NOMINA, ID_UTENTE ) values ( ?,?,?,?,?,? )";
		
		return setNominaIngegnereToDam(nextValSql, insertSql, damId, inBean, userId, closeConnection);
	}
	
	public NominaIngegnereBean updateNominaIngegnereResponsabile(NominaIngegnereBean inBean, String userId, boolean closeConnection) throws Exception {
		
		String sql = "UPDATE INGEGNERE_RESPONSABILE SET DATA_NOMINA=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? WHERE PROGRESSIVO=? ";

		String getSql = "SELECT PROGRESSIVO, NUMERO_ARCHIVIO, SUB, ID_INGEGNERE, DATA_NOMINA, DATA_MODIFICA, ID_UTENTE FROM INGEGNERE_RESPONSABILE " +
						"WHERE NUMERO_ARCHIVIO=? AND SUB=? AND ID_INGEGNERE=? AND CANCELLATO<>'S' ";
		
		return updateNominaIngegnere(sql, (NominaIngegnereBean)bnUtil.compareAndFillBean(getNominaIngegnere(getSql, inBean, true), inBean), userId, closeConnection);
	}

	public Boolean removeIngegnereResponsabileFromDam (String damId, String ingId, String userId, boolean closeConnection) throws EntityNotFoundException, SQLException {

		String sql = "update INGEGNERE_RESPONSABILE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? " +
						"where PROGRESSIVO=(select MAX(PROGRESSIVO) from INGEGNERE_RESPONSABILE " +
						"where numero_archivio=? and sub=? and id_ingegnere=? and cancellato<>'S')";
		
		return removeNominaIngegnereFromDam(sql, damId, ingId, userId, closeConnection);
	}
	
	public NominaIngegnereBean setIngegnereAsSostitutoToDam(String damId, NominaIngegnereBean inBean, String userId, boolean closeConnection) throws Exception {

		String nextValSql = "select INGEGNERE_SOST_SEQUENZA.nextval from dual";
		String insertSql = "insert into INGEGNERE_SOSTITUTO (PROGRESSIVO, NUMERO_ARCHIVIO, SUB, ID_INGEGNERE, DATA_NOMINA, ID_UTENTE ) values ( ?,?,?,?,?,? )";
		
		return setNominaIngegnereToDam(nextValSql, insertSql, damId, inBean, userId, closeConnection);
	}

	public NominaIngegnereBean updateNominaIngegnereSostituto(NominaIngegnereBean inBean, String userId, boolean closeConnection) throws Exception {
		
		String sql = "UPDATE INGEGNERE_SOSTITUTO SET DATA_NOMINA=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? WHERE PROGRESSIVO=? ";
		
		String getSql = "SELECT PROGRESSIVO, NUMERO_ARCHIVIO, SUB, ID_INGEGNERE, DATA_NOMINA, DATA_MODIFICA, ID_UTENTE FROM INGEGNERE_SOSTITUTO " +
						"WHERE NUMERO_ARCHIVIO=? AND SUB=? AND ID_INGEGNERE=? AND CANCELLATO<>'S' ";
				
		return updateNominaIngegnere(sql, (NominaIngegnereBean)bnUtil.compareAndFillBean(getNominaIngegnere(getSql, inBean, true), inBean), userId, closeConnection);
	}

	public Boolean removeIngegnereSostitutoFromDam(String damId, String ingId, String userId, boolean closeConnection) throws EntityNotFoundException, SQLException {
		
		String sql = "update INGEGNERE_SOSTITUTO set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? " +
						"where PROGRESSIVO=(select MAX(PROGRESSIVO) from INGEGNERE_SOSTITUTO " +
						"where numero_archivio=? and sub=? and id_ingegnere=? and cancellato<>'S')";
		
		return removeNominaIngegnereFromDam(sql, damId, ingId, userId, closeConnection);
	}
	
	private NominaIngegnereBean setNominaIngegnereToDam(String nextValSql, String insertSql, String damId, NominaIngegnereBean inBean, String userId, boolean closeConnection) throws Exception {

		int paramsSet = 1;
		int updatedRows = 0;
		Date dataNomina = null;
		
		ResultSet inNextValRs = null;
		PreparedStatement inPs = null;
		
		try {

			//extract numeroarchivio and sub from damId			
			inBean.setNumeroArchivio(Integer.parseInt(damId.substring(0, damId.length()-1)));
			inBean.setSub(damId.substring(damId.length()-1, damId.length()));

			//create formatted date
			if (inBean.getDataNomina() != null && !"".equals(inBean.getDataNomina())) {
				DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);
				dataNomina = new Date(format.parse(inBean.getDataNomina()).getTime());
			}
			
			//Starting transaction 
			dbUtil.setAutoCommit(false);

			//Get entity next id
			inNextValRs = dbUtil.getConnection().createStatement().executeQuery(nextValSql);
			
			if (inNextValRs != null && inNextValRs.next()) {
				inBean.setProgressivo(inNextValRs.getInt(1));
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}
			
			//INSERT INGEGNERE			
			inPs = dbUtil.getConnection().prepareStatement(insertSql);
			
			inPs.setInt(paramsSet++, inBean.getProgressivo()); //PROGRESSIVO
			inPs.setInt(paramsSet++, inBean.getNumeroArchivio()); //NUMERO_ARCHIVIO
			inPs.setString(paramsSet++, inBean.getSub()); //SUB
			inPs.setInt(paramsSet++, Integer.parseInt(inBean.getId())); //ID_INGEGNERE
			
			if (dataNomina != null) {
				inPs.setDate(paramsSet++, dataNomina); //DATA_NOMINA
			} else {
				inPs.setNull(paramsSet++, Types.DATE);
			}
			
			inPs.setString(paramsSet++, userId); //ID_UTENTE
			
			updatedRows += inPs.executeUpdate();

			if (updatedRows > 0) {
				//commiting changes
				if (closeConnection) {
					dbUtil.getConnection().commit();
				}
			} else {
				throw new SQLException("Record not created");
			}

		} catch (Exception sqle) {
			logger.error("SQLException with message: " + sqle.getMessage());
			
			if (closeConnection) {
				dbUtil.rollback();
			}
			throw sqle;
		} finally {
			if (inNextValRs != null) {
				inNextValRs.close();
			}
			if (inPs != null) {
				inPs.close();
			}
			
			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}
		
		return inBean;
	}
	
	private NominaIngegnereBean updateNominaIngegnere(String sql, NominaIngegnereBean inBean, String userId, boolean closeConnection) throws Exception {

		int paramsSet = 1;
		int updatedRows = 0;
		PreparedStatement inPs = null;
		Date dataNomina  = null;
		
		try {

			if (inBean.getDataNomina() != null && !"".equals(inBean.getDataNomina())) {
				DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);
				dataNomina = new Date(format.parse(inBean.getDataNomina()).getTime());
			}
			
			//UPDATE NOMINA INGEGNERE			
			inPs = dbUtil.getConnection().prepareStatement(sql);
			
			if (dataNomina != null) {
				inPs.setDate(paramsSet++, dataNomina); //DATA_NOMINA
			} else {
				inPs.setNull(paramsSet++, Types.DATE);
			}
			
			inPs.setString(paramsSet++, userId); //ID_UTENTE
			inPs.setInt(paramsSet++, inBean.getProgressivo()); //PROGRESSIVO
			
			updatedRows += inPs.executeUpdate();

			if (updatedRows > 0) {
				//something
			} else {
				throw new EntityNotFoundException("Entity with ID="+inBean.getProgressivo()+" not found.");
			}

		} catch (Exception sqle) {
			logger.error("SQLException with message: " + sqle.getMessage());
			
			if (closeConnection) {
				dbUtil.rollback();
			}
			throw sqle;
		} finally {
			if (inPs != null) {
				inPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return inBean;
	}
	
	private Boolean removeNominaIngegnereFromDam(String sql, String damId, String ingId, String userId, boolean closeConnection) throws EntityNotFoundException, SQLException {

		int paramsSet = 1;
		int updatedRows = 0;
		PreparedStatement inPs = null;
		Boolean wasDeleted = false;

		//extract numeroarchivio and sub from damId
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
		
		try {

			//Retrieving connection
			inPs = dbUtil.getConnection().prepareStatement(sql);
			inPs.setString(paramsSet++, userId); //ID_UTENTE
			inPs.setString(paramsSet++, "S"); //CANCELLATO
			inPs.setInt(paramsSet++, numeroArchivio); //NUMERO_ARCHIVIO
			inPs.setString(paramsSet++, sub);
			inPs.setInt(paramsSet++, Integer.parseInt(ingId));
			
			updatedRows += inPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Entity with ID="+ingId+" not found.");
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (inPs != null) {
				inPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return wasDeleted;
	}
}
