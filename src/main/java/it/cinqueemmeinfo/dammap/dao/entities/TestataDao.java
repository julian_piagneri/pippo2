package it.cinqueemmeinfo.dammap.dao.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.TestataBean;
import it.cinqueemmeinfo.dammap.dao.macro.ContactsDao;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class TestataDao {

	private static final Logger logger = LoggerFactory.getLogger(TestataDao.class);

	@Autowired
	private DBUtility dbUtil;	
	@Autowired
	private ContactsDao contactsDao;

	private static String NUMERO_ARCHIVIO = "NUMERO_ARCHIVIO";
	private static String SUB = "SUB";

	public TestataBean getTestataByDamID(String damID) throws SQLException, EntityNotFoundException {
		TestataBean result = null;

		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn = null;

		Map<String, Object> pk = DBUtility.getPrimaryKeyFromDamId(damID);

		final String sql = "select d.NUMERO_ARCHIVIO, d.SUB, dc.NOMI_REGIONI, d.NOME_DIGA, d.NOME_FIUME_SBARRATO, d.NOME_BACINO, d.DESCRIZIONE_CLASSIFICA_DIGA "
				+ "	, to_char(d.VOLUME_TOTALE_INVASO_L584,'9G990D99') as VOLUME_TOTALE_INVASO_L584, to_char(d.QUOTA_MAX,'9G990D99') as QUOTA_MAX "
				+ "	, to_char(d.QUOTA_MAX_REGOLAZIONE,'9G990D99') as QUOTA_MAX_REGOLAZIONE, to_char(d.VOLUME_AUTORIZZATO,'9G990D99') as VOLUME_AUTORIZZATO "
				+ "	, to_char(d.QUOTA_AUTORIZZATA,'9G990D99') as QUOTA_AUTORIZZATA, to_char(d.PORTATA_MAX_PIENA_PROG,'9G990D99') as PORTATA_MAX_PIENA_PROG "
				+ "	, d.DATA_CERTIFICATO_COLLAUDO, concat(dc.NOMI_COMUNI, concat(' (',concat(p.NOME,')'))) as NOME_COMUNE, df.NOME_FIUME_VALLE, d.NUM_ISCRIZIONE_RID "
				+ "	, concat(concat(concat(concat(status_principale, ' - '), status_secondario), ' ') "
				+ "	, concat(concat(descr_status_principale, ' - '), descr_status_secondario)) as STATUS " + "from V_ELENCO_DIGHE_SINGOLI d "
				+ "left join vat_diga_fiumi_valle df on d.NUMERO_ARCHIVIO = df.NUMERO_ARCHIVIO and d.SUB = df.SUB "
				+ "left join vat_diga_comuni_prov_regioni dc on d.NUMERO_ARCHIVIO = dc.NUMERO_ARCHIVIO and d.SUB = dc.SUB "
				+ "left join provincia p on dc.SIGLE_PROVINCE = p.SIGLA and dc.NOMI_REGIONI = p.NOME_REGIONE " + "where d.NUMERO_ARCHIVIO=? and d.SUB=? ";
		
		try {
			conn = dbUtil.getNewConnection();

			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("========== SQL ==========\n {}", sql);
			}
			ps = conn.prepareStatement(sql);
			ps.setLong(1, (Long) pk.get(NUMERO_ARCHIVIO));
			ps.setString(2, (String) pk.get(SUB));
			rs = ps.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			if (rs.next()) {
				result = fillBean(rs);
				result.setUffCoord(contactsDao.getUfficioCoordFromIdDiga(damID));
				result.setUffPerif(contactsDao.getUfficioPerifFromIdDiga(damID));
			} else {
				throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}

		return result;
	}

	private TestataBean fillBean(ResultSet rs) throws SQLException {
		TestataBean result = new TestataBean();

		result.setNumeroArchivio(rs.getString(NUMERO_ARCHIVIO));
		result.setSub(rs.getString(SUB));
		result.setId("" + result.getNumeroArchivio() + result.getSub());
		result.setNomeDiga(rs.getString("NOME_DIGA"));
		result.setNomeRegione(rs.getString("NOMI_REGIONI"));
		result.setStatus(rs.getString("STATUS"));
		result.setNumeroRid(rs.getString("NUM_ISCRIZIONE_RID"));
		result.setNomeFiumeSbarrato(rs.getString("NOME_FIUME_SBARRATO"));
		result.setNomeBacino(rs.getString("NOME_BACINO"));
		result.setDescrClassDiga(rs.getString("DESCRIZIONE_CLASSIFICA_DIGA"));
		result.setVolTotInvaso(rs.getString("VOLUME_TOTALE_INVASO_L584"));
		result.setQuotaMax(rs.getString("QUOTA_MAX"));
		result.setQuotaMaxReg(rs.getString("QUOTA_MAX_REGOLAZIONE"));
		result.setVolAutoriz(rs.getString("VOLUME_AUTORIZZATO"));
		result.setQuotaAutoriz(rs.getString("QUOTA_AUTORIZZATA"));
		result.setPortataMaxPiena(rs.getString("PORTATA_MAX_PIENA_PROG"));
		result.setDataCertCollaudo(rs.getString("DATA_CERTIFICATO_COLLAUDO"));
		result.setNomeComune(rs.getString("NOME_COMUNE"));
		result.setFiumiDiga(rs.getString("NOME_FIUME_VALLE"));

		return result;
	}
}
