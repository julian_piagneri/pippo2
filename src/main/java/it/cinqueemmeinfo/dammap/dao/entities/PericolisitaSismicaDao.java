package it.cinqueemmeinfo.dammap.dao.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.PericolisitaSismicaBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class PericolisitaSismicaDao {

	private static final Logger logger = LoggerFactory.getLogger(PericolisitaSismicaDao.class);

	@Autowired
	private DBUtility dbUtil;

	private static final class Field {
		private static final String NUMERO_ARCHIVIO = "NUMERO_ARCHIVIO";
		private static final String SUB = "SUB";
		@SuppressWarnings("unused")
		private static final String CANCELLATO = "CANCELLATO";
		private static final String ID_UTENTE = "ID_UTENTE";
		private static final String DATA_MODIFICA = "DATA_MODIFICA";
		private static final String ACCELERAZIONE_MASSIMA = "ACCELERAZIONE_MASSIMA";
		private static final String FATTORE_AMPLIFICAZIONE = "FATTORE_AMPLIFICAZIONE";
		private static final String TEMPO_RITORNO = "TEMPO_DI_RITORNO";
		private static final String ID_TEMPI_RITORNO = "ID";
		private static final String NOME_TEMPO_DI_RITORNO = "NOME";
	}

	private static final class Table {
		private static final String DAO = "PERICOLISITA_SISMICA";
		private static final String TIPO_RITORNO = "TIPO_TEMPI_DI_RITORNO_PERICO";
	}

	private static final String FIND_ALL_PERICOLISITA_BY_ID = 
			"SELECT BASE.ID_UTENTE, " +
			  "BASE.DATA_MODIFICA, " +
			  "BASE.ACCELERAZIONE_MASSIMA, " +
			  "BASE.FATTORE_AMPLIFICAZIONE, " +
			  "T2.NOME " +
			"FROM " +
			  "(SELECT * " +
			  "FROM TIPO_TEMPI_DI_RITORNO_PERICO T " +
			  "JOIN PERICOLISITA_SISMICA P " +
			  "ON (P.TEMPO_DI_RITORNO = T.ID) " +
			  "WHERE (P.CANCELLATO    = 'N' " +
			  "OR P.CANCELLATO        = 'n') " +
			  "AND (P.SUB             = ? " +
			  "AND P.NUMERO_ARCHIVIO  = ?) " +
			  ") BASE " +
			"RIGHT JOIN DIGHE_2X_OTT.TIPO_TEMPI_DI_RITORNO_PERICO T2 " +
			"ON BASE.ID = T2.ID ";

	private static final String FIND_ONE_TEMPI_DI_RITORNO_BY_NOME = "SELECT ID FROM " + Table.TIPO_RITORNO + " WHERE NOME = ? AND ROWNUM <= 1";

	private static final String INSERT_PERICOLISITA = "INSERT INTO " + Table.DAO
			+ " (NUMERO_ARCHIVIO, SUB, ACCELERAZIONE_MASSIMA, FATTORE_AMPLIFICAZIONE, TEMPO_DI_RITORNO, ID_UTENTE)" + " VALUES (?, ?, ?, ?, ?, ?)";

	public void updatePericolisita(List<PericolisitaSismicaBean> beanList, String damID, String userId) throws SQLException {
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damID);
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		long idTampiDiRitorno;
		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			conn.setAutoCommit(false);
			for (PericolisitaSismicaBean bean : beanList) {
				idTampiDiRitorno = getIdTampiDiRitorno(primaryKey, bean, ps, rs, conn);
				ps = conn.prepareStatement(DBUtility.logicalDeleteSql(Table.DAO, Field.TEMPO_RITORNO));
				ps.setLong(1, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
				ps.setString(2, (String) primaryKey.get(Field.SUB));
				ps.setLong(3, idTampiDiRitorno);
				ps.executeUpdate();
			}
			for (PericolisitaSismicaBean bean : beanList) {
				idTampiDiRitorno = getIdTampiDiRitorno(primaryKey, bean, ps, rs, conn);
				ps = conn.prepareStatement(INSERT_PERICOLISITA);
				ps.setLong(1, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
				ps.setString(2, (String) primaryKey.get(Field.SUB));
				if (bean.getAccelerazioneMassima() != null) {
					ps.setDouble(3, bean.getAccelerazioneMassima());
				} else {
					ps.setNull(3, Types.DOUBLE);
				}
				if (bean.getFattoreAmplificazione() != null) {
					ps.setDouble(4, bean.getFattoreAmplificazione());
				} else {
					ps.setNull(4, Types.DOUBLE);
				}
				ps.setLong(5, idTampiDiRitorno);
				ps.setString(6, userId);
				ps.executeUpdate();
			}
			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
	}

	public List<PericolisitaSismicaBean> getPericolisitaSismicaByDamId(String damId) throws SQLException, EntityNotFoundException {
		List<PericolisitaSismicaBean> result = null;
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damId, Field.NUMERO_ARCHIVIO, Field.SUB);

		PreparedStatement prPs = null;
		ResultSet rs = null;
		Connection conn = null;

		String sql = FIND_ALL_PERICOLISITA_BY_ID; //+ " AND (" + Table.DAO + "." + Field.SUB + " = '" + primaryKey.get(Field.SUB) + "' AND " + Table.DAO + "."
				//+ Field.NUMERO_ARCHIVIO + " = " + primaryKey.get(Field.NUMERO_ARCHIVIO) + ") OR " + Table.DAO + "." + Field.TEMPO_RITORNO + " IS NULL";
		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("========== SQL ==========\n {}", sql);
			}
			prPs = conn.prepareStatement(sql);
			prPs.setString(1, (String) primaryKey.get(Field.SUB));
			prPs.setLong(2, (Long) primaryKey.get(Field.NUMERO_ARCHIVIO));
			rs = prPs.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			result = new ArrayList<PericolisitaSismicaBean>();
			while (rs.next()) {
				result.add(fillOutBean(rs, primaryKey));
			}
			if (result.isEmpty()) {
				result.add(new PericolisitaSismicaBean());
				//throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
		} catch (SQLException sqle) {
			logger.error("\nError while executinge '{}' \nMessage: {}", sql, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, prPs);
		}

		return result;
	}

	private long getIdTampiDiRitorno(Map<String, Object> primaryKey, PericolisitaSismicaBean bean, PreparedStatement ps, ResultSet rs, Connection conn)
			throws SQLException {
		ps = conn.prepareStatement(FIND_ONE_TEMPI_DI_RITORNO_BY_NOME);
		ps.setString(1, bean.getTempoDiRitorno());
		rs = ps.executeQuery();
		if (rs == null) {
			throw new SQLException("Something went wrong while to execute query");
		} else if (rs.next()) {
			return rs.getLong(Field.ID_TEMPI_RITORNO);
		} else {
			throw new SQLException(bean.getTempoDiRitorno() + " is not present " + Table.TIPO_RITORNO);
		}
	}

	private PericolisitaSismicaBean fillOutBean(ResultSet rs, Map<String, Object> primaryKey) throws SQLException {
		PericolisitaSismicaBean result = new PericolisitaSismicaBean();

		logger.debug("Filling out the {}...", Table.DAO);

		rs.getDouble(Field.ACCELERAZIONE_MASSIMA);
		result.setAccelerazioneMassima(rs.wasNull() ? null : rs.getDouble(Field.ACCELERAZIONE_MASSIMA));
		rs.getDouble(Field.FATTORE_AMPLIFICAZIONE);
		result.setFattoreAmplificazione(rs.wasNull() ? null : rs.getDouble(Field.FATTORE_AMPLIFICAZIONE));
		rs.getString(Field.NOME_TEMPO_DI_RITORNO);
		result.setTempoDiRitorno(rs.wasNull() ? null : rs.getString(Field.NOME_TEMPO_DI_RITORNO));

		result.setId("" + primaryKey.get(Field.NUMERO_ARCHIVIO) + primaryKey.get(Field.SUB));
		result.setLastModifiedDate(rs.getString(Field.DATA_MODIFICA));
		result.setLastModifiedUserId(rs.getString(Field.ID_UTENTE));
		result.setIsDeleted("N");

		return result;
	}
}