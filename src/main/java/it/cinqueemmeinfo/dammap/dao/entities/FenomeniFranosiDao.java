package it.cinqueemmeinfo.dammap.dao.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiAltroBean;
import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiBean;
import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiCheckBean;
import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiGruppoBean;
import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiSchedaBean;
import it.cinqueemmeinfo.dammap.bean.fields.IdFenBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;
import it.cinqueemmeinfo.dammap.utility.exceptions.NextValException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class FenomeniFranosiDao {

	private static final Logger logger = LoggerFactory.getLogger(FenomeniFranosiDao.class);

	@Autowired
	private DBUtility dbUtil;

	private static final String NUMERO_ARCHIVIO = "NUMERO_ARCHIVIO";
	private static final String SUB = "SUB";

	private static final String GET_FENOMENI_FRANOSI_BY_DAMID = "SELECT BASE.ID IG_GRUPPO, BASE.DESCRIZIONE_GRUPPO, BASE.ESCLUSIVO, " 
			+ "COALESCE(FDG.SN, 'N') AS SN, FDG.ID_UTENTE FDG_ID_UTENTE, "
			+ "FDG.CANCELLATO FDG_CANCELLATO, FDG.DATA_MODIFICA FDG_DATA_MODIFICA, FFC.DESCRIZIONE_CHECK, "
			+ "CASE WHEN FFDC.ID_CHECK IS NULL THEN 'N' ELSE 'S' END AS IS_CHECKED, FFDC.ID_UTENTE FFDC_ID_UTENTE, FFDC.CANCELLATO FFDC_CANCELLATO, "
			+ "FFDC.DATA_MODIFICA FFDC_DATA_MODIFICA FROM (select * from (SELECT * FROM FEN_FRANOSI_SCHEDA WHERE (CANCELLATO = 'N' OR CANCELLATO = 'n') AND ID_FEN = ?"
			+ "AND NUMERO_ARCHIVIO = ? AND SUB = ?) FFS, FEN_FRANOSI_GRUPPO FFG) BASE LEFT JOIN (SELECT * FROM FEN_DIGHE_GRUPPO_SN WHERE (CANCELLATO = 'N' "
			+ "OR CANCELLATO = 'n')) FDG ON FDG.GRUPPO_SN = BASE.ID AND BASE.ID_FEN = FDG.ID_FEN LEFT JOIN FEN_FRANOSI_CHECK FFC ON "
			+ "FFC.GRUPPO_CHECK = BASE.ID LEFT JOIN (SELECT * FROM FEN_FRANOSI_DIGHE_CHECK WHERE (CANCELLATO = 'N' OR CANCELLATO = 'N')) "
			+ "FFDC ON FFDC.ID_CHECK = FFC.ID AND BASE.ID_FEN = FFDC.ID_FEN ORDER BY BASE.ID, FFC.ORDINE";

	private static final String GET_ALL_SCHEDA_ID_BY_DAMID = "SELECT T.DATA_MODIFICA, T.DESCRIZIONE, T.ID_FEN, T.ID_UTENTE " + "FROM FEN_FRANOSI_SCHEDA T "
			+ "WHERE (CANCELLATO = 'N' OR CANCELLATO = 'n') AND T.NUMERO_ARCHIVIO = ? AND T.SUB = ? " + "ORDER BY T.ID_FEN";
	
	private static final String UPDATE_SCHEDA_DESCRIPTION = "UPDATE FEN_FRANOSI_SCHEDA SET DESCRIZIONE = ?, ID_UTENTE = ? WHERE ID_FEN = ? ";
	
	private static final String LOGICAL_DELETE_OF_GRUPPO = "UPDATE FEN_DIGHE_GRUPPO_SN SET CANCELLATO = \'S\' WHERE ID_FEN = ? AND GRUPPO_SN = ?";

	private static final String INSERT_GRUPPO = "INSERT INTO FEN_DIGHE_GRUPPO_SN (ID_FEN, GRUPPO_SN, SN, ID_UTENTE) " + "VALUES (?, ?, ?, ?)";

	private static final String GET_ID_CHECKS = "SELECT FFC.ID " + "FROM FEN_FRANOSI_CHECK FFC "
			+ "JOIN FEN_FRANOSI_DIGHE_CHECK FFDC ON FFC.ID = FFDC.ID_CHECK " + "WHERE GRUPPO_CHECK = ? AND ID_FEN = ?";

	private static final String GET_ID_CHECK_BY_ID_GRUPPO_AND_DESCRIZIONE_CHECK = "SELECT FFC.ID " + "FROM FEN_FRANOSI_CHECK FFC "
			+ "WHERE GRUPPO_CHECK = ? AND DESCRIZIONE_CHECK = ?";

	private static final String INSERT_FEN_FRANOSI_DIGHE_CHECK = "INSERT INTO FEN_FRANOSI_DIGHE_CHECK (ID_FEN, ID_CHECK, ID_UTENTE) VALUES (?, ?, ?)";

	private static final String LOGICAL_DELETE_OF_GRUPPO_BY_ID_FEN = "UPDATE FEN_DIGHE_GRUPPO_SN SET CANCELLATO = \'S\' WHERE ID_FEN = ?";

	private static final String LOGICAL_DELETE_FEN_FRANOSI_DIGHE_CHECK = "UPDATE FEN_FRANOSI_DIGHE_CHECK SET CANCELLATO = \'S\' WHERE ID_FEN = ?";

	private static final String LOGICAL_DELETE_FEN_FRANOSI_DIGHE_CHECK_BY_ID_CHECK = LOGICAL_DELETE_FEN_FRANOSI_DIGHE_CHECK + " AND ID_CHECK = ?";

	private static final String LOGICAL_DELETE_FEN_FRANOSI_DIGHE_SCHEDA = "UPDATE FEN_FRANOSI_SCHEDA SET CANCELLATO = \'S\' WHERE ID_FEN = ?";

	private static final String INSERT_FEN_FRANOSI_DIGHE_SCHEDA = "INSERT INTO FEN_FRANOSI_SCHEDA (NUMERO_ARCHIVIO, SUB, ID_UTENTE, ID_FEN, DESCRIZIONE) VALUES (?, ?, ?, ?, ?)";

	private static final String INSERT_ONE_FEN_FRANOSI_ALTRO_DISTANZA = "INSERT INTO FEN_FRANOSI_ALTRO_DISTANZA (ID_FEN, ALTRO, DISTANZA, ID_UTENTE) VALUES (?, ?, ?, ?)";

	private static final String LOGICAL_DELETE_FEN_FRANOSI_ALTRO_DISTANZA = "UPDATE FEN_FRANOSI_ALTRO_DISTANZA SET CANCELLATO = \'S\' WHERE ID_FEN = ?";

	private static final String GET_ALTRO_DISTANZA_BY_ID_FEN = "SELECT T.ALTRO, T.DATA_MODIFICA, T.DISTANZA, T.ID_UTENTE "
			+ "FROM FEN_FRANOSI_ALTRO_DISTANZA T " + "WHERE ID_FEN = ? AND (CANCELLATO = 'N' OR CANCELLATO = 'n')";

	private static final String GET_ONE_FEN_FRANOSI_SCHEDA_BY_ID = "SELECT ID_FEN FROM FEN_FRANOSI_SCHEDA WHERE ID_FEN = ? AND (CANCELLATO = 'N' OR CANCELLATO = 'n')";

	public List<FenomeniFranosiSchedaBean> getFenomeniFranosiSchedaByDamId(String damId) throws SQLException, EntityNotFoundException {
		List<FenomeniFranosiSchedaBean> result = new ArrayList<FenomeniFranosiSchedaBean>(0);

		for (Bean bean : getAllSchedaIdByDamID(damId)) {
			FenomeniFranosiSchedaBean element = new FenomeniFranosiSchedaBean();
			element.setIdSchedaFenomeniFranosi(bean.getIdFen());
			element.setDescrizione(bean.getDescrizione());
			element.setId(damId);
			element.setLastModifiedDate(bean.getDate());
			element.setLastModifiedUserId(bean.getIdUtente());
			element.setFenomeniFranosi(getFenomeniFranosiByDamId(damId, bean.getIdFen()));
			result.add(element);
		}

		return result;
	}

	public List<FenomeniFranosiBean> getFenomeniFranosiByDamId(String damId, long idFen) throws SQLException, EntityNotFoundException {
		List<FenomeniFranosiBean> result = new ArrayList<FenomeniFranosiBean>(0);
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damId, NUMERO_ARCHIVIO, SUB);
		List<FenomeniFranosiGruppoBean> gruppi = new ArrayList<FenomeniFranosiGruppoBean>(0);
		List<FenomeniFranosiCheckBean> check = new ArrayList<FenomeniFranosiCheckBean>(0);
		FenomeniFranosiGruppoBean gruppiTmp = null;

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("========== SQL ==========\n {}", GET_FENOMENI_FRANOSI_BY_DAMID);
			}
			ps = conn.prepareStatement(GET_FENOMENI_FRANOSI_BY_DAMID);
			ps.setLong(1, idFen);
			ps.setLong(2, (Long) primaryKey.get(NUMERO_ARCHIVIO));
			ps.setString(3, (String) primaryKey.get(SUB));
			rs = ps.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			String currGroupId = "";
			while (rs.next()) {
				if (!currGroupId.equals(rs.getString("IG_GRUPPO"))) {
					if (rs.getRow() > 1) {
						gruppi.add(gruppiTmp);
						check = new ArrayList<FenomeniFranosiCheckBean>();
					}
					currGroupId = rs.getString("IG_GRUPPO");
				}
				check.add(fillOutCheckBean(rs));
				gruppiTmp = fillOutGroupBean(rs, check);
			}
			gruppi.add(gruppiTmp);
			result.add(fillOutBean(gruppi));
			/*
			if (result.isEmpty()) {
				throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
			*/
		} catch (SQLException sqle) {
			logger.error("\nError while executing '{}' \nMessage: {}", GET_FENOMENI_FRANOSI_BY_DAMID, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}

		return result;
	}

	public FenomeniFranosiAltroBean getFenomeniFranosiAltroByIdFen(long idFen) throws SQLException, EntityNotFoundException {
		FenomeniFranosiAltroBean result = new FenomeniFranosiAltroBean();

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("========== SQL ==========\n {}", GET_ALTRO_DISTANZA_BY_ID_FEN);
			}
			ps = conn.prepareStatement(GET_ALTRO_DISTANZA_BY_ID_FEN);
			ps.setLong(1, idFen);
			rs = ps.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			if (rs.next()) {
				result = fillOutAltroBean(rs, idFen);
			} else {
				throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
		} catch (SQLException sqle) {
			logger.error("\nError while executing '{}' \nMessage: {}", GET_ALTRO_DISTANZA_BY_ID_FEN, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}

		return result;
	}

	public void updateFenomeniFranosiAltro(FenomeniFranosiAltroBean bean, String userId, long idScheda) throws SQLException, EntityNotFoundException {
		if (!existFenomeniFranosiScheda(idScheda)) {
			throw new EntityNotFoundException("The 'Fenomeni Franosi Scheda' with id=" + idScheda + " is not present in the system");
		}
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(LOGICAL_DELETE_FEN_FRANOSI_ALTRO_DISTANZA);
			ps.setLong(1, idScheda);
			ps.executeUpdate();
			ps.close();
			ps = conn.prepareStatement(INSERT_ONE_FEN_FRANOSI_ALTRO_DISTANZA);
			ps.setLong(1, idScheda);
			ps.setString(2, bean.getAltro());
			ps.setDouble(3, bean.getDistanza());
			ps.setString(4, userId);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
	}

	public void updateFenomeniFranosiGruppoByDamID(FenomeniFranosiGruppoBean gruppo, long idScheda, String userId) throws SQLException, EntityNotFoundException {
		if (!existFenomeniFranosiScheda(idScheda)) {
			throw new EntityNotFoundException("The 'Fenomeni Franosi Scheda' with id=" + idScheda + " is not present in the system");
		}
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			conn.setAutoCommit(false);
			updateFFDigheGruppo(gruppo, idScheda, userId, conn, rs, ps);
			updateFFDigaCheck(gruppo, idScheda, userId, conn, rs, ps);
			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
	}
	
	public void updateFenomeniFranosiGruppoBySchedaID(FenomeniFranosiSchedaBean scheda, long idScheda, String userId) throws SQLException, EntityNotFoundException {
		if (!existFenomeniFranosiScheda(idScheda)) {
			throw new EntityNotFoundException("The 'Fenomeni Franosi Scheda' with id=" + idScheda + " is not present in the system");
		}

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			conn.setAutoCommit(false);
			
			
			if (logger.isDebugEnabled()) {
				logger.debug("SQL: {}", UPDATE_SCHEDA_DESCRIPTION);
			}

			ps = conn.prepareStatement(UPDATE_SCHEDA_DESCRIPTION);
			ps.setString(1, scheda.getDescrizione());
			ps.setString(2, userId);
			ps.setLong(3, idScheda);
			
//			System.out.println(ps.toString());
			
			ps.executeUpdate();
			
//			updateFFDigheGruppo(gruppo, idScheda, userId, conn, rs, ps);
//			updateFFDigaCheck(gruppo, idScheda, userId, conn, rs, ps);
			
			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
		
		
	}
	
	public void deleteFenomeniFranosiGruppoByDamID(long idScheda) throws SQLException, EntityNotFoundException {
		deleteDigheScheda(idScheda);
		deleteDigheGruppo(idScheda);
		deleteDigheCheck(idScheda);
		deleteDigheAltroDistanza(idScheda);
	}

	public IdFenBean insertFenomeniFranosi(String damID, FenomeniFranosiSchedaBean scheda, String userId) throws SQLException, NextValException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		long newID = 0;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			conn.setAutoCommit(false);
			newID = dbUtil.getNextID("FEN_FRANOSI_SCHEDA");
			insertNewRow(damID, scheda, newID, userId, ps, conn);
			conn.commit();
		} catch (NextValException nve) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", nve.getMessage());
			throw nve;
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}

		return new IdFenBean(newID);
	}
	
	private boolean existFenomeniFranosiScheda(long idScheda) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		boolean result = false;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			ps = conn.prepareStatement(GET_ONE_FEN_FRANOSI_SCHEDA_BY_ID);
			ps.setLong(1, idScheda);
			rs = ps.executeQuery();
			result = rs.next();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}

		return result;
	}

	private void deleteDigheAltroDistanza(long idScheda) throws SQLException {
		PreparedStatement ps = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("SQL: {}", LOGICAL_DELETE_FEN_FRANOSI_ALTRO_DISTANZA);
			}
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(LOGICAL_DELETE_FEN_FRANOSI_ALTRO_DISTANZA);
			ps.setLong(1, idScheda);
			if (ps.executeUpdate() < 1) {
				throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
			conn.commit();
		} catch (EntityNotFoundException ene) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", ene.getMessage());
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, null, ps);
		}
	}

	private void insertNewRow(String damID, FenomeniFranosiSchedaBean scheda, long newID, String userId, PreparedStatement ps, Connection conn) throws SQLException {
		Map<String, Object> pk = DBUtility.getPrimaryKeyFromDamId(damID);

		if (logger.isDebugEnabled()) {
			logger.debug("SQL: {}", INSERT_FEN_FRANOSI_DIGHE_SCHEDA);
		}

		ps = conn.prepareStatement(INSERT_FEN_FRANOSI_DIGHE_SCHEDA);
		ps.setLong(1, (Long) pk.get(NUMERO_ARCHIVIO));
		ps.setString(2, (String) pk.get(SUB));
		ps.setString(3, userId);
		ps.setLong(4, newID);
		ps.setString(5, scheda.getDescrizione());
		
		ps.executeUpdate();
	}

	private void deleteDigheScheda(long idScheda) throws SQLException, EntityNotFoundException {
		PreparedStatement ps = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("SQL: {}", LOGICAL_DELETE_FEN_FRANOSI_DIGHE_SCHEDA);
			}
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(LOGICAL_DELETE_FEN_FRANOSI_DIGHE_SCHEDA);
			ps.setLong(1, idScheda);
			if (ps.executeUpdate() < 1) {
				throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
			conn.commit();
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, null, ps);
		}
	}

	private void deleteDigheCheck(long idScheda) throws SQLException {
		PreparedStatement ps = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("SQL: {}", LOGICAL_DELETE_FEN_FRANOSI_DIGHE_CHECK);
			}
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(LOGICAL_DELETE_FEN_FRANOSI_DIGHE_CHECK);
			ps.setLong(1, idScheda);
			if (ps.executeUpdate() < 1) {
				throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
			conn.commit();
		} catch (EntityNotFoundException ene) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", ene.getMessage());
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, null, ps);
		}
	}

	private void deleteDigheGruppo(long idScheda) throws SQLException {
		PreparedStatement ps = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("SQL: {}", LOGICAL_DELETE_FEN_FRANOSI_DIGHE_CHECK);
			}
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(LOGICAL_DELETE_OF_GRUPPO_BY_ID_FEN);
			ps.setLong(1, idScheda);
			if (ps.executeUpdate() < 1) {
				throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
			conn.commit();
		} catch (EntityNotFoundException ene) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", ene.getMessage());
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, null, ps);
		}
	}

	private void updateFFDigheGruppo(FenomeniFranosiGruppoBean gruppo, long idScheda, String userId, Connection conn, ResultSet rs, PreparedStatement ps)
			throws SQLException {
		if (logger.isDebugEnabled()) {
			logger.debug("SQL: {}", LOGICAL_DELETE_OF_GRUPPO);
		}

		ps = conn.prepareStatement(LOGICAL_DELETE_OF_GRUPPO);
		ps.setLong(1, idScheda);
		ps.setLong(2, Long.parseLong(gruppo.getId()));
		ps.executeUpdate();

		if (logger.isDebugEnabled()) {
			logger.debug("SQL: {}", INSERT_GRUPPO);
		}

		ps = conn.prepareStatement(INSERT_GRUPPO);
		ps.setLong(1, idScheda);
		ps.setLong(2, Long.parseLong(gruppo.getId()));
		ps.setString(3, gruppo.getSn());
		ps.setString(4, userId);
		ps.executeUpdate();
	}

	private void updateFFDigaCheck(FenomeniFranosiGruppoBean gruppo, long idScheda, String userId, Connection conn, ResultSet rs, PreparedStatement ps)
			throws SQLException {
		if (logger.isDebugEnabled()) {
			logger.debug("SQL: {}", LOGICAL_DELETE_FEN_FRANOSI_DIGHE_CHECK_BY_ID_CHECK);
		}

		for (long idCheck : getIdChecks(Long.parseLong(gruppo.getId()), idScheda, conn, rs, ps)) {
			ps = conn.prepareStatement(LOGICAL_DELETE_FEN_FRANOSI_DIGHE_CHECK_BY_ID_CHECK);
			ps.setLong(1, idScheda);
			ps.setLong(2, idCheck);
			ps.executeUpdate();
		}

		for (FenomeniFranosiCheckBean bean : gruppo.getFenDigheCheck()) {
			if (bean.getIsChecked().toUpperCase().equals("S")) {
				long id = getIdChek(Long.parseLong(gruppo.getId()), bean.getDescrizioneCheck(), conn, rs, ps);
				insertCheck(id, idScheda, userId, conn, rs, ps);
			}
		}
	}

	private void insertCheck(long id, long idScheda, String user, Connection conn, ResultSet rs, PreparedStatement ps) throws SQLException {
		if (logger.isDebugEnabled()) {
			logger.debug("SQL: {}", INSERT_FEN_FRANOSI_DIGHE_CHECK);
		}

		ps = conn.prepareStatement(INSERT_FEN_FRANOSI_DIGHE_CHECK);
		ps.setLong(1, idScheda);
		ps.setLong(2, id);
		ps.setString(3, user);
		ps.executeUpdate();
	}

	private long getIdChek(long idGruppo, String descrizioneCheck, Connection conn, ResultSet rs, PreparedStatement ps) throws SQLException {
		long result = 0;

		if (logger.isDebugEnabled()) {
			logger.debug("========== SQL ==========\n {}\n{}", GET_ID_CHECK_BY_ID_GRUPPO_AND_DESCRIZIONE_CHECK, idGruppo);
		}
		ps = conn.prepareStatement(GET_ID_CHECK_BY_ID_GRUPPO_AND_DESCRIZIONE_CHECK);
		ps.setLong(1, idGruppo);
		ps.setString(2, descrizioneCheck);
		rs = ps.executeQuery();
		if (rs == null) {
			throw new SQLException("Something went wrong while to execute query");
		}
		if (rs.next()) {
			result = rs.getLong("ID");
		}

		return result;
	}

	private List<Long> getIdChecks(long idGruppo, long idScheda, Connection conn, ResultSet rs, PreparedStatement ps) throws SQLException {
		List<Long> result = new ArrayList<Long>();

		if (logger.isDebugEnabled()) {
			logger.debug("========== SQL ==========\n {}\n{}", GET_ID_CHECKS, idGruppo);
		}
		ps = conn.prepareStatement(GET_ID_CHECKS);
		ps.setLong(1, idGruppo);
		ps.setLong(2, idScheda);
		rs = ps.executeQuery();
		if (rs == null) {
			throw new SQLException("Something went wrong while to execute query");
		}
		while (rs.next()) {
			result.add(rs.getLong("ID"));
		}

		return result;
	}

	private FenomeniFranosiAltroBean fillOutAltroBean(ResultSet rs, long idFen) throws SQLException {
		FenomeniFranosiAltroBean result = new FenomeniFranosiAltroBean();

		logger.debug("Filling out the FenomeniFranosiAltroBean...");

		result.setIdFen(idFen);
		result.setAltro(rs.getString("ALTRO") != null ? rs.getString("ALTRO") : "");
		result.setDistanza(rs.getDouble("DISTANZA"));
		result.setLastModifiedDate(rs.getString("DATA_MODIFICA"));
		result.setLastModifiedUserId(rs.getString("ID_UTENTE"));

		return result;
	}

	private FenomeniFranosiBean fillOutBean(List<FenomeniFranosiGruppoBean> fenDigheGruppi) throws SQLException {
		FenomeniFranosiBean result = new FenomeniFranosiBean();

		logger.debug("Filling out the FenomeniFranosiBean...");

		result.setFenDigheGruppi(fenDigheGruppi);

		return result;
	}

	private FenomeniFranosiGruppoBean fillOutGroupBean(ResultSet rs, List<FenomeniFranosiCheckBean> fenDigheCheck) throws SQLException {
		FenomeniFranosiGruppoBean result = new FenomeniFranosiGruppoBean();
		String tmp = "";

		result.setFenDigheCheck(fenDigheCheck);

		result.setId(rs.getString("IG_GRUPPO"));

		tmp = rs.getString("DESCRIZIONE_GRUPPO");
		result.setDescrizioneGruppo(tmp == null ? "" : tmp);

		tmp = rs.getString("ESCLUSIVO");
		result.setEsclusivo(tmp == null ? "" : tmp);

		tmp = rs.getString("FDG_CANCELLATO");
		result.setIsDeleted(tmp == null ? "" : tmp);

		tmp = rs.getString("FDG_DATA_MODIFICA");
		result.setLastModifiedDate(tmp == null ? "" : tmp);

		tmp = rs.getString("FDG_ID_UTENTE");
		result.setLastModifiedUserId(tmp == null ? "" : tmp);

		tmp = rs.getString("SN");
		result.setSn(tmp == null ? "" : tmp);

		return result;
	}

	private List<Bean> getAllSchedaIdByDamID(String damId) throws SQLException, EntityNotFoundException {
		List<Bean> result = new ArrayList<Bean>(0);
		Map<String, Object> primaryKey = DBUtility.getPrimaryKeyFromDamId(damId, NUMERO_ARCHIVIO, SUB);

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = dbUtil.getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("database connection failed");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("========== SQL ==========\n {}", GET_ALL_SCHEDA_ID_BY_DAMID);
			}
			ps = conn.prepareStatement(GET_ALL_SCHEDA_ID_BY_DAMID);
			ps.setLong(1, (Long) primaryKey.get(NUMERO_ARCHIVIO));
			ps.setString(2, (String) primaryKey.get(SUB));
			rs = ps.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			while (rs.next()) {
				Bean element = new Bean();
				element.setIdFen(rs.getLong("ID_FEN"));
				element.setDate(rs.getString("DATA_MODIFICA"));
				element.setIdUtente(rs.getString("ID_UTENTE"));
				element.setDescrizione(rs.getString("DESCRIZIONE"));
				result.add(element);
			}
			/*
			if (result.isEmpty()) {
				throw new EntityNotFoundException("Couldn't find entity with the provided params");
			}
			*/
		} catch (SQLException sqle) {
			logger.error("\nError while executing '{}' \nMessage: {}", GET_ALL_SCHEDA_ID_BY_DAMID, sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}

		return result;
	}

	private FenomeniFranosiCheckBean fillOutCheckBean(ResultSet rs) throws SQLException {
		FenomeniFranosiCheckBean result = new FenomeniFranosiCheckBean();
		String tmp = "";

		tmp = rs.getString("DESCRIZIONE_CHECK");
		result.setDescrizioneCheck(tmp == null ? "" : tmp);

		tmp = rs.getString("FFDC_CANCELLATO");
		result.setIsDeleted(tmp == null ? "" : tmp);

		tmp = rs.getString("FFDC_DATA_MODIFICA");
		result.setLastModifiedDate(tmp == null ? "" : tmp);

		tmp = rs.getString("FFDC_ID_UTENTE");
		result.setLastModifiedUserId(tmp == null ? "" : tmp);

		tmp = rs.getString("IS_CHECKED");
		result.setIsChecked(tmp == null ? "" : tmp);

		return result;
	}

	protected class Bean {

		private long idFen;
		private String date;
		private String idUtente;
		private String descrizione;

		/**
		 * @return the idFen
		 */
		public long getIdFen() {
			return idFen;
		}

		/**
		 * @param idFen
		 *            the idFen to set
		 */
		public void setIdFen(long idFen) {
			this.idFen = idFen;
		}

		/**
		 * @return the date
		 */
		public String getDate() {
			return date;
		}

		/**
		 * @param date
		 *            the date to set
		 */
		public void setDate(String date) {
			this.date = date;
		}

		/**
		 * @return the idUtente
		 */
		public String getIdUtente() {
			return idUtente;
		}

		/**
		 * @param idUtente
		 *            the idUtente to set
		 */
		public void setIdUtente(String idUtente) {
			this.idUtente = idUtente;
		}

		/**
		 * @return the descrizione
		 */
		public String getDescrizione() {
			return descrizione;
		}

		/**
		 * @param descrizione
		 *            the descrizione to set
		 */
		public void setDescrizione(String descrizione) {
			this.descrizione = descrizione;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((date == null) ? 0 : date.hashCode());
			result = prime * result + ((descrizione == null) ? 0 : descrizione.hashCode());
			result = prime * result + (int) (idFen ^ (idFen >>> 32));
			result = prime * result + ((idUtente == null) ? 0 : idUtente.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof Bean)) {
				return false;
			}
			Bean other = (Bean) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (date == null) {
				if (other.date != null) {
					return false;
				}
			} else if (!date.equals(other.date)) {
				return false;
			}
			if (descrizione == null) {
				if (other.descrizione != null) {
					return false;
				}
			} else if (!descrizione.equals(other.descrizione)) {
				return false;
			}
			if (idFen != other.idFen) {
				return false;
			}
			if (idUtente == null) {
				if (other.idUtente != null) {
					return false;
				}
			} else if (!idUtente.equals(other.idUtente)) {
				return false;
			}
			return true;
		}

		private FenomeniFranosiDao getOuterType() {
			return FenomeniFranosiDao.this;
		}
	}
}