package it.cinqueemmeinfo.dammap.dao.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;
import it.cinqueemmeinfo.dammap.dao.fields.AddressDao;
import it.cinqueemmeinfo.dammap.dao.fields.ContactInfoDao;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

@SuppressWarnings("unused")
public class AltraEnteDao implements Serializable {

	private static final long serialVersionUID = -9101898710826709104L;
	private static final Logger logger = LoggerFactory.getLogger(AltraEnteDao.class);

	@Autowired
	private DBUtility dbUtil;
	@Autowired
	private BeanUtility bnUtil;
	@Autowired
	private AddressDao adDao;
	@Autowired
	private ContactInfoDao ciDao;

	public AltraEnteBean getAltraEnteWithParams(AltraEnteBean aeBean, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		List<AltraEnteBean> aeList = getAltraEnteListWithParams(2, "", aeBean, true, closeConnection);

		if (aeList.size() > 0) {
			return aeList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}

	public AltraEnteBean getAltraEnteWithParams(int hasAccess, String userId, AltraEnteBean aeBean,
			boolean closeConnection) throws SQLException, EntityNotFoundException {

		List<AltraEnteBean> aeList = getAltraEnteListWithParams(hasAccess, userId, aeBean, true, closeConnection);

		if (aeList.size() > 0) {
			return aeList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}

	public List<AltraEnteBean> getAltraEnteListWithParams(int hasAccess, String userId, AltraEnteBean aeBean,
			boolean getExtraInfos, boolean closeConnection) throws SQLException {

		int paramsSet = 1;
		String sql = "";
		String whereSql = "";
		AltraEnteBean foundAeBean = null;
		ArrayList<ContactInfoBean> ciList = null;
		ArrayList<AddressBean> adList = null;
		ArrayList<AltraEnteBean> aeList = new ArrayList<AltraEnteBean>();

		ResultSet aeRs = null;
		PreparedStatement aePs = null;
		Connection conn = null;

		try {

			if (!"".equals(aeBean.getId()) && aeBean.getId() != null) {
				sql = "SELECT AE.ID, AE.NOME, AE.TIPO_ENTE, AE.DATA_MODIFICA, AE.ID_UTENTE, AE.CANCELLATO "
						+ "FROM ALTRA_ENTE AE " + this.addAssociatedOnlyJoinQuery(hasAccess)
						+ "WHERE AE.CANCELLATO<>'S' AND AE.ID = ? "
						+ this.addAssociatedOnlyWhereQuery(hasAccess, userId) + " ORDER BY ID ASC ";
			} else {
				sql = "SELECT AE.ID, AE.NOME, AE.TIPO_ENTE, AE.DATA_MODIFICA, AE.ID_UTENTE, AE.CANCELLATO "
						+ "FROM ALTRA_ENTE AE " + this.addAssociatedOnlyJoinQuery(hasAccess)
						+ "WHERE AE.CANCELLATO<>'S' " + this.addAssociatedOnlyWhereQuery(hasAccess, userId)
						+ " ORDER BY AE.ID ASC ";
			}

			// WHERE PARAM CHECK
			/*
			 * whereSql += "CANCELLATO = ? ";
			 * 
			 * if (!"".equals(aeBean.getId()) && aeBean.getId() != null) {
			 * whereSql += "AND ID = ? "; } if (!"".equals(aeBean.getNome()) &&
			 * aeBean.getNome() != null) { whereSql += "AND NOME LIKE ?  "; } if
			 * (aeBean.getTipoEnte() != null) { whereSql += "AND TIPO_ENTE = ? "
			 * ; }
			 * 
			 * //WHERE STRING BUILDER if (whereSql.length() > 0) { if
			 * (whereSql.startsWith("AND")) { whereSql = whereSql.substring(4,
			 * whereSql.length()); }
			 * 
			 * sql += " WHERE " + whereSql; }
			 * 
			 * //ORDERBY sql += "ORDER BY ID ASC ";
			 */

			// preparing statement
			conn = dbUtil.getNewConnection();
			aePs = conn.prepareStatement(sql);

			// Setting query parameters
			// aePs.setString(paramsSet++, "N");

			if (!"".equals(aeBean.getId()) && aeBean.getId() != null) {
				aePs.setInt(paramsSet++, Integer.parseInt(aeBean.getId()));
			}
			/*
			 * if (!"".equals(aeBean.getNome()) && aeBean.getNome() != null) {
			 * aePs.setString(paramsSet++, aeBean.getNome()); } if
			 * (aeBean.getTipoEnte() != null) { aePs.setInt(paramsSet++,
			 * aeBean.getTipoEnte()); }
			 */

			if (hasAccess == 1 && !userId.equals("") && userId != null) {
				aePs.setString(paramsSet++, userId);
			}

			// execute
			aeRs = aePs.executeQuery();

			while (aeRs.next()) {

				foundAeBean = new AltraEnteBean(aeRs.getString("ID"), aeRs.getString("NOME"), aeRs.getInt("TIPO_ENTE"),
						null, null, aeRs.getString("DATA_MODIFICA"), aeRs.getString("ID_UTENTE"),
						aeRs.getString("CANCELLATO"));

				if (getExtraInfos) {
					// retrieve phone numbers
					ciList = (ArrayList<ContactInfoBean>) ciDao.getContactInfoListWithParams(foundAeBean, false);
					// retrieve address list
					adList = (ArrayList<AddressBean>) adDao.getAddressListWithParams(foundAeBean, false);

					foundAeBean.setIndirizzo(adList);
					foundAeBean.setContatto(ciList);
				}

				// ADD NEW ITEM
				aeList.add(foundAeBean);
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(aePs);
			DBUtility.closeQuietly(conn);
		}

		return aeList;
	}

	private String addAssociatedOnlyWhereQuery(int hasAccess, String userId) {
		// Adds a join on utente_concess if the user has access only to the
		// associated values
		if (hasAccess == 1 && !userId.equals("") && userId != null) {
			return " AND UAE.ID_UTENTE IN (SELECT UP.ID FROM UTENTE UP WHERE UP.USERID= ?) ";
		}
		return " ";
	}

	private String addAssociatedOnlyJoinQuery(int hasAccess) {
		// Adds a join on utente_concess if the user has access only to the
		// associated values
		if (hasAccess == 1) {
			return " LEFT JOIN UTENTE_ALTRA_ENTE UAE ON AE.ID=UAE.ID_ENTE AND UAE.CANCELLATO='N' ";
		}
		return " ";
	}

	/**
	 * 
	 * @param aeBean
	 * @param userId
	 * @param closeConnection
	 *            if true closes connection and commmits changes, if false
	 *            transaction will be kept open for futher operations
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException
	 */
	public AltraEnteBean createAltraEnte(AltraEnteBean aeBean, String userId, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		ResultSet aeNextValRs = null;
		PreparedStatement aePs = null;
		AltraEnteBean createdAeBean = null;
		Connection conn = null;

		try {

			// Starting transaction
			// Get entity id
			sql = "select AENTE_SEQUENZA.nextval from dual";
			conn = dbUtil.getNewConnection();
			if (closeConnection) {
				conn.setAutoCommit(false);
			}
			aeNextValRs = conn.createStatement().executeQuery(sql);

			if (aeNextValRs != null && aeNextValRs.next()) {
				aeBean.setId(aeNextValRs.getString(1));
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}

			// INSERT ALTRA_ENTE
			sql = "insert into ALTRA_ENTE (ID, NOME, TIPO_ENTE, ID_UTENTE ) values ( ?,?,?,? )";

			aePs = conn.prepareStatement(sql);

			aePs.setString(paramsSet++, aeBean.getId()); // ID
			aePs.setString(paramsSet++, aeBean.getNome()); // NOME
			aePs.setInt(paramsSet++, aeBean.getTipoEnte()); // TIPO_ENTE
			aePs.setString(paramsSet++, userId); // ID_UTENTE

			updatedRows += aePs.executeUpdate();

			if (closeConnection) {
				conn.commit();
			}

			if (updatedRows > 0) {
				// INSERT CONTACT INFO
				for (ContactInfoBean pnBean : aeBean.getContatto()) {
					ciDao.createContactInfo(pnBean, aeBean, userId, false);
				}

				// INSERT ADDRESS
				for (AddressBean adBean : aeBean.getIndirizzo()) {
					adDao.createAddress(adBean, aeBean, userId, false);
				}

				// RETRIEVING CREATED ROW
				createdAeBean = getAltraEnteWithParams(aeBean, false);
			} else {
				throw new SQLException("Insert not executed, error while creating record");
			}

			// commiting changes
			if (closeConnection) {
				conn.commit();
			}

		} catch (SQLException sqle) {
			if (closeConnection) {
				conn.rollback();
			}
			logger.error("SQLException with message: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (closeConnection) {
				conn.setAutoCommit(true);
			}
			DBUtility.closeQuietly(conn, aeNextValRs, aePs);
		}

		return createdAeBean;
	}

	/**
	 * 
	 * @param aeBean
	 * @param userId
	 * @param closeConnection
	 *            if true closes connection and commmits changes, if false
	 *            transaction will be kept open for futher operations
	 * @return
	 * @throws Exception
	 */
	public AltraEnteBean updateAltraEnte(AltraEnteBean aeBean, String userId, boolean closeConnection)
			throws Exception {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement aePs = null;
		AltraEnteBean foundAeBean = null;
		Connection conn = null;

		try {
			foundAeBean = (AltraEnteBean) bnUtil.compareAndFillBean(getAltraEnteWithParams(aeBean, true), aeBean);

			sql = "update ALTRA_ENTE set NOME=?, TIPO_ENTE=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=?";

			// Retrieving connection
			conn = dbUtil.getNewConnection();
			conn.setAutoCommit(false);
			aePs = conn.prepareStatement(sql);

			aePs.setString(paramsSet++, foundAeBean.getNome()); // NOME
			aePs.setInt(paramsSet++, foundAeBean.getTipoEnte()); // TIPO_ENTE
			aePs.setString(paramsSet++, userId); // ID_UTENTE
			aePs.setInt(paramsSet++, Integer.parseInt(foundAeBean.getId())); // ID

			updatedRows += aePs.executeUpdate();

			if (closeConnection) {
				conn.commit();
			}

			if (updatedRows > 0) {
				// UPDATE CONTACT INFO
				if (aeBean.getContatto() != null) {
					for (ContactInfoBean pnBean : aeBean.getContatto()) {
						ciDao.updateContactInfo(pnBean, aeBean, userId, false);
					}
				}
				// UPDATE ADDRESS
				if (aeBean.getIndirizzo() != null) {
					for (AddressBean adBean : aeBean.getIndirizzo()) {
						adDao.updateAddress(adBean, aeBean, userId, false);
					}
				}

				// RETRIEVE UPDATED ROW
				foundAeBean = getAltraEnteWithParams(aeBean, true);

			} else {
				throw new EntityNotFoundException("Entity with ID=" + aeBean.getId() + " not found.");
			}

			// COMMIT
			if (closeConnection) {
				conn.commit();
			}

		} catch (Exception e) {
			logger.error("Error while executing '" + sql + "'");
			if (closeConnection) {
				conn.rollback();
			}
			throw e;
		} finally {
			if (aePs != null) {
				aePs.close();
			}

			if (closeConnection) {
				conn.setAutoCommit(true);
			}
			DBUtility.closeQuietly(aePs);
			DBUtility.closeQuietly(conn);
		}

		return foundAeBean;
	}

	/**
	 * 
	 * @param entityId
	 * @param userId
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public Boolean removeAltraEnte(String entityId, String userId, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement aePs = null;
		Boolean wasDeleted = false;
		Connection conn = null;

		try {

			sql = "update ALTRA_ENTE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=?";

			// Retrieving connection
			conn = dbUtil.getNewConnection();
			aePs = conn.prepareStatement(sql);
			aePs.setString(paramsSet++, userId); // ID_UTENTE
			aePs.setString(paramsSet++, "S"); // CANCELLATO
			aePs.setInt(paramsSet++, Integer.parseInt(entityId)); // ID

			updatedRows += aePs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Entity with ID=" + entityId + " not found.");
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn);
			DBUtility.closeQuietly(aePs);
		}

		return wasDeleted;
	}
}