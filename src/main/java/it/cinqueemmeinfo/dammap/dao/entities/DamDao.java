package it.cinqueemmeinfo.dammap.dao.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.CampoInfoBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.DamBean;
import it.cinqueemmeinfo.dammap.bean.entities.DamExtraDataComuneBean;
import it.cinqueemmeinfo.dammap.bean.entities.DamExtraDataIngegnereBean;
import it.cinqueemmeinfo.dammap.bean.entities.DamExtraDataUtilizzazioneBean;
import it.cinqueemmeinfo.dammap.bean.entities.FiumeBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.entities.InfoDigaExtraBean;
import it.cinqueemmeinfo.dammap.bean.entities.InfoExtraBean;
import it.cinqueemmeinfo.dammap.bean.entities.InfoFiumeExtraBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

public class DamDao implements Serializable {

	private static final long serialVersionUID = 3083265384697240485L;
	private static final Logger logger = LoggerFactory.getLogger(DamDao.class);
	
	@Autowired private DBUtility dbUtil;
	
	//******************* DIGA *************************	

	//TODO VMON_DIGA_INVASO >> vista dighe ed invasi 
	
	//Check DamList.JAR > DamListBean > userDamList()
	public ArrayList<DamBean> getDamList(String userId, boolean loadExtraData) throws SQLException {
		return getDamList(userId, loadExtraData, new HashMap<String, Integer>());
	}
	
	private String getFilterString(String filterName, String filterSql, String and, HashMap<String, Integer> filters){
		String sql="";
		//Deve applicare il filtro
		if(filters.get(filterName)!=null && filters.get(filterName).intValue()==1){
			//Se è il primo filtro aggiunge WHERE altrimenti AND/OR
			if(this.filterCount>0){
				sql+=" "+and+" ";
			} else {
				sql+=" WHERE ( ";
			}
			sql+=" "+filterSql+" ";
			this.filterCount++;
		}
		return sql;
	}
	
	private int setFilterParameters(int ip, PreparedStatement dmPs, String filterName, String[] parameters, HashMap<String, Integer> filters) throws SQLException{
		if(filters.get(filterName)!=null && filters.get(filterName).intValue()==1){
			for (String parameter : parameters) {
				dmPs.setString(ip++, parameter);
			}
		}
		return ip;
	}
	
	public int countForExternal(String userId) throws SQLException {	
		//userId="DCHIAROLLA";
		String sql = "";
		PreparedStatement dmPs = null;
		ResultSet rs = null;
		Connection conn=null;
		int dams=-1;
		try {

			sql = " SELECT count(*) AS dams FROM V_ELENCO_DIGHE_SINGOLI DS " +
			" INNER JOIN  ( " + 
			" select distinct NUMERO_ARCHIVIO, SUB from ACCESSO_DEFAULT " + 
			" where ID_GRUPPO in (select ID_GRUPPO from UTENTE_GRUPPO UG " + 
			" left join UTENTE U on UG.ID_UTENTE = U.ID where U.USERID = ?) ) "
			+ " AD  on DS.NUMERO_ARCHIVIO = AD.NUMERO_ARCHIVIO and DS.SUB = AD.SUB ";
			
			conn=dbUtil.getNewConnection();
			dmPs = conn.prepareStatement(sql);
			int ip=1;
			dmPs.setString(ip++, userId); //USERID ACCESSO_DEFAULT
			rs = dmPs.executeQuery();
			
			if(rs.next()){
				dams=rs.getInt("dams");
			}
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, dmPs);
		}
		return dams;
	}
	
	private int filterCount=0;
	public ArrayList<DamBean> getDamList(String userId, boolean loadExtraData, HashMap<String, Integer> filters) throws SQLException {
		//userId="DCHIAROLLA";
		String sql = "";
		PreparedStatement dmPs = null;
		ResultSet rs = null;
		Connection conn=null;
		ArrayList<DamBean> digheList = null;
		Map<String, DamBean> digheMap=null;
		Map<String, LinkedHashMap<String, CampoInfoBean>> extraInfo=new LinkedHashMap<String, LinkedHashMap<String,CampoInfoBean>>();
		try {

			sql = " select DS.*, DT.ID_FUNZ_SEDE_CENTRALE AS idFunzionarioCentrale, "
					+ " DT.ID_FUNZ_UFF_PERIF AS idFunzionarioPerif, DECODE(c.ID_UTENTE, NULL, 0, 1) AS preferita FROM V_ELENCO_DIGHE_SINGOLI DS " +
			/*		" INNER JOIN  (select distinct NUMERO_ARCHIVIO, SUB from ACCESSO_DEFAULT where ID_GRUPPO in " + 
								" (select ID_GRUPPO from UTENTE_GRUPPO UG left join UTENTE U on UG.ID_UTENTE = U.ID where U.USERID = ? ) ) AD " + 
					            " on DS.NUMERO_ARCHIVIO = AD.NUMERO_ARCHIVIO and DS.SUB = AD.SUB ";*/
			"INNER JOIN  ( " + 
			"select distinct NUMERO_ARCHIVIO, SUB from ACCESSO_DEFAULT " + 
			"where ID_GRUPPO in (select ID_GRUPPO from UTENTE_GRUPPO UG " + 
			"left join UTENTE U on UG.ID_UTENTE = U.ID where U.USERID = ?) " + 
			
			"UNION SELECT DISTINCT NUMERO_ARCHIVIO, SUB FROM DIGA_CONCESS DC " +
			"LEFT JOIN UTENTE_CONCESS UC ON UC.ID_CONCESS = DC.ID_CONCESS " +
			"LEFT JOIN UTENTE U ON UC.ID_UTENTE = U.ID WHERE U.USERID = ? "
			+ " AND UC.CANCELLATO= ? " +
			
			"UNION SELECT DISTINCT NUMERO_ARCHIVIO, SUB FROM DIGA_GESTORE DG " +
			"LEFT JOIN UTENTE_GESTORE UG ON UG.ID_GESTORE = DG.ID_GESTORE " +
			"LEFT JOIN UTENTE U ON UG.ID_UTENTE = U.ID WHERE U.USERID = ? "
			+ " AND UG.CANCELLATO= ? " +
			") AD  on DS.NUMERO_ARCHIVIO = AD.NUMERO_ARCHIVIO and DS.SUB = AD.SUB "
			+ " LEFT JOIN DIGA DT ON DS.NUMERO_ARCHIVIO=DT.NUMERO_ARCHIVIO AND DS.SUB=DT.SUB ";

			this.filterCount=0;
			//Applica il filtro di selezione
			if(filters.get("select")!=null && filters.get("select").intValue()==1){
				sql+=" INNER JOIN DIGHE_PREFERITE c ON c.NUMERO_ARCHIVIO=DS.NUMERO_ARCHIVIO AND c.SUB=DS.SUB "
					+ " AND c.ID_UTENTE IN (SELECT UP.ID FROM UTENTE UP WHERE UP.USERID= ?) ";
			} else {
				sql+=" LEFT JOIN DIGHE_PREFERITE c ON c.NUMERO_ARCHIVIO=DS.NUMERO_ARCHIVIO AND c.SUB=DS.SUB "
					+ " AND c.ID_UTENTE IN (SELECT UP.ID FROM UTENTE UP WHERE UP.USERID= ?) ";
			}
			if(filters.get("select")!=null && filters.get("select").intValue()==2){
				sql+=" INNER JOIN DIGHE_PREFERITE c ON c.NUMERO_ARCHIVIO=DS.NUMERO_ARCHIVIO AND c.SUB=DS.SUB "
					+ " AND c.ID_UTENTE IN (SELECT UP.ID FROM UTENTE UP WHERE UP.USERID= ?) ";
			}
			//Applica i filtri
			sql+=this.getFilterString("rid", " DS.COMPETENZA_SND = ? ", " OR ", filters);
			sql+=this.getFilterString("accertamento", " DS.STATUS_PRINCIPALE = ? ", " OR ", filters);
			sql+=this.getFilterString("progetto", " DS.STATUS_PRINCIPALE IN (?, ?, ?) ", " OR ", filters);
			sql+=this.getFilterString("competenza", " DS.STATUS_PRINCIPALE = ? ", " OR ", filters);
			sql+=this.getFilterString("altro", " DS.STATUS_PRINCIPALE IN (?, ?) ", " OR ", filters);
			//Chiude con una parentesi se c'è almeno un filtro
			if(this.filterCount>0){
				sql+=" ) ";
			}
			sql+=" order by DS.NUMERO_ARCHIVIO, DS.SUB ";
			conn=dbUtil.getNewConnection();
			dmPs = conn.prepareStatement(sql);
			int ip=1;
			dmPs.setString(ip++, userId); //USERID ACCESSO_DEFAULT
			dmPs.setString(ip++, userId); //USERID UTENTE_CONCESS
			dmPs.setString(ip++, "N"); //CANCELLATO UTENTE_CONCESS
			dmPs.setString(ip++, userId); //USERID UTENTE_GESTORE
			dmPs.setString(ip++, "N"); //CANCELLATO UTENTE_GESTORE
			
			if(filters.get("select")!=null && filters.get("select").intValue()==1){
				dmPs.setString(ip++, userId);
			} else {
				dmPs.setString(ip++, userId);
			}
			
			ip=this.setFilterParameters(ip, dmPs, "rid", new String[]{"S"}, filters);
			ip=this.setFilterParameters(ip, dmPs, "accertamento", new String[]{"AC"}, filters);
			ip=this.setFilterParameters(ip, dmPs, "progetto", new String[]{"PR", "PM", "PE"}, filters);
			ip=this.setFilterParameters(ip, dmPs, "competenza", new String[]{"PD"}, filters);
			ip=this.setFilterParameters(ip, dmPs, "altro", new String[]{"AD", "ND"}, filters);
			
			rs = dmPs.executeQuery();
			//System.out.println(sql);
			//digheList = new ArrayList<DamBean>();
			digheMap=new LinkedHashMap<String, DamBean>();
			while (rs.next()) {
				
				DamBean dgBean = new DamBean();
				
				dgBean.setId(rs.getString("NUMERO_ARCHIVIO")+rs.getString("SUB"));
				dgBean.setNumeroArchivio(rs.getString("NUMERO_ARCHIVIO"));
				dgBean.setSub(rs.getString("SUB"));
				
				dgBean.setPreferita(rs.getBoolean("preferita"));
				
				dgBean.setNomeDiga(rs.getString("NOME_DIGA"));
				dgBean.setLatCentrale(rs.getString("LAT_CENTRALE"));
				dgBean.setLongCentrale(rs.getString("LONG_CENTRALE"));
				dgBean.setNomeLago(rs.getString("NOME_LAGO"));
				dgBean.setBacino(rs.getString("NOME_BACINO"));
				dgBean.setNomeFiumeSbarrato(rs.getString("NOME_FIUME_SBARRATO"));
				dgBean.setStatus1(rs.getString("STATUS_PRINCIPALE") + " - " + rs.getString("DESCR_STATUS_PRINCIPALE"));
				dgBean.setStatus2(rs.getString("STATUS_SECONDARIO") + " - " + rs.getString("DESCR_STATUS_SECONDARIO"));
				dgBean.setStatus1Sigla(rs.getString("STATUS_PRINCIPALE"));
				dgBean.setStatus2Sigla(rs.getString("STATUS_SECONDARIO"));
				dgBean.setStatus1Dettaglio(rs.getString("DESCR_STATUS_PRINCIPALE"));
				dgBean.setStatus2Dettaglio(rs.getString("DESCR_STATUS_SECONDARIO"));
				dgBean.setCompetenzaSnd(rs.getString("COMPETENZA_SND"));
				dgBean.setAnnoConsegnaLavori(rs.getString("ANNO_CONSEGNA_LAVORI"));
				dgBean.setAnnoUltimazLavori(rs.getString("ANNO_ULTIMAZIONE_LAVORI"));
				dgBean.setDataCertCollaudo(rs.getString("DATA_CERTIFICATO_COLLAUDO"));
				dgBean.setAltezzaDigaDm(rs.getString("ALTEZZA_DIGA_DM"));
				dgBean.setAltezzaDigaL584(rs.getString("ALTEZZA_DIGA_L584"));
				dgBean.setDescrClassDiga(rs.getString("DESCRIZIONE_CLASSIFICA_DIGA"));
				dgBean.setQuotaMax(rs.getString("QUOTA_MAX"));
				dgBean.setQuotaMaxRegolaz(rs.getString("QUOTA_MAX_REGOLAZIONE"));
				dgBean.setQuotaAutoriz(rs.getString("QUOTA_AUTORIZZATA"));
				dgBean.setVolumeTotInvasoDm(rs.getString("VOLUME_TOTALE_INVASO_DM"));
				dgBean.setVolumeTotInvasoL584(rs.getString("VOLUME_TOTALE_INVASO_L584"));
				dgBean.setVolumeLaminazione(rs.getString("VOLUME_LAMINAZIONE"));
				dgBean.setPortMaxPienaProgetto(rs.getString("PORTATA_MAX_PIENA_PROG"));
				dgBean.setVolumeAutoriz(rs.getString("VOLUME_AUTORIZZATO"));
				dgBean.setFunzSedeCentrale(rs.getString("FUNZIONARIO_SEDE_CENTRALE"));
				dgBean.setFunzUffPerif(rs.getString("FUNZIONARIO_UFF_PERIFERICO"));
				dgBean.setUffSedeCentrale(rs.getString("UFFICIO_SEDE_CENTRALE"));
				dgBean.setUffPeriferico(rs.getString("UFFICIO_PERIFERICO"));
				dgBean.setNumIscrizRid(rs.getString("NUM_ISCRIZIONE_RID"));
				dgBean.setCondotteForzate(rs.getString("CONDOTTE_FORZATE"));
				dgBean.setIdInvaso(rs.getString("ID_INVASO"));
				dgBean.setIdFunzionarioCentrale(rs.getInt("idFunzionarioCentrale"));
				dgBean.setIdFunzionarioPerif(rs.getInt("idFunzionarioPerif"));
				
				//digheList.add(dgBean);
				//Inserisco in una mappa per controllarli per la query successiva
				digheMap.put(dgBean.getId(), dgBean);
			}
			
			if (loadExtraData) {
				extraInfo=this.getDamExtraInfo(userId);
				
				Set<Entry<String, LinkedHashMap<String, CampoInfoBean>>> extraInfoEntries = extraInfo.entrySet();
				for (Entry<String, LinkedHashMap<String, CampoInfoBean>> entry : extraInfoEntries) {
					digheMap.get(entry.getKey()).setExtraInfo(entry.getValue());
				}
				
				dmPs.close();
				rs.close();
				
				sql = "SELECT DM.NUMERO_ARCHIVIO, DM.SUB, DM.NOME_COMUNE, DM.NOME_PROVINCIA, DM.NOME_REGIONE, DM.ZONA_SISMICA_COMUNE " +
						", DECODE(DM.OPERA_PRESA, 'S', NVL(DM.NOME_COMUNE, '')||' ('||DM.SIGLA_PROVINCIA||'-'||DM.NOME_REGIONE||')', 'N', '') AS COMUNE_OPERA_PRESA " +
						", DM.NOME_FIUME_VALLE, DM.ALTRA_DENOMINAZIONE, DM.UTILIZZAZIONE, DM.PRIORITA, DM.TIPO_ORGANO_SCARICO, DM.DIGHE_INVASO, "
						//Nuovi Dati
						+ " CONC.NOME AS concName, GEST.NOME AS gestName, ING.NOME AS engName, ING.COGNOME AS engSurname, ING.TELEFONO AS engPhone " +
						" FROM V_ELENCO_DIGHE_MULTIPLI DM " +
						/*"INNER JOIN  (SELECT DISTINCT NUMERO_ARCHIVIO,SUB FROM ACCESSO_DEFAULT WHERE ID_GRUPPO IN (SELECT ID_GRUPPO FROM UTENTE_GRUPPO UG " + 
						            "LEFT JOIN UTENTE U ON UG.ID_UTENTE = U.ID WHERE U.USERID = ? ) ) AD ON DM.NUMERO_ARCHIVIO=AD.NUMERO_ARCHIVIO AND DM.SUB=AD.SUB " + */
						
						" INNER JOIN  ( " + 
						"select distinct NUMERO_ARCHIVIO, SUB from ACCESSO_DEFAULT " + 
						"where ID_GRUPPO in (select ID_GRUPPO from UTENTE_GRUPPO UG " + 
						"left join UTENTE U on UG.ID_UTENTE = U.ID where U.USERID = ?) " + 
						"UNION SELECT DISTINCT NUMERO_ARCHIVIO, SUB FROM DIGA_CONCESS DC " +
						"LEFT JOIN UTENTE_CONCESS UC ON UC.ID_CONCESS = DC.ID_CONCESS " +
						"LEFT JOIN UTENTE U ON UC.ID_UTENTE = U.ID WHERE U.USERID = ? " +
						"UNION SELECT DISTINCT NUMERO_ARCHIVIO, SUB FROM DIGA_GESTORE DG " +
						"LEFT JOIN UTENTE_GESTORE UG ON UG.ID_GESTORE = DG.ID_GESTORE " +
						"LEFT JOIN UTENTE U ON UG.ID_UTENTE = U.ID WHERE U.USERID = ? " +
						") AD ON DM.NUMERO_ARCHIVIO=AD.NUMERO_ARCHIVIO AND DM.SUB=AD.SUB "            
						
						//Concessionario
						+ " LEFT JOIN DIGA_CONCESS DCONC ON DM.NUMERO_ARCHIVIO=DCONC.NUMERO_ARCHIVIO AND DM.SUB=DCONC.SUB "
						+ " LEFT JOIN CONCESSIONARIO CONC ON DCONC.ID_CONCESS=CONC.ID "
						
						//Gestore
						+ " LEFT JOIN DIGA_GESTORE DGEST ON DM.NUMERO_ARCHIVIO=DGEST.NUMERO_ARCHIVIO AND DM.SUB=DGEST.SUB "
						+ " LEFT JOIN GESTORE GEST ON DGEST.ID_GESTORE=GEST.ID "
						
						//Ingegnere
						+ " LEFT JOIN INGEGNERE_RESPONSABILE INGRESP ON DM.NUMERO_ARCHIVIO=INGRESP.NUMERO_ARCHIVIO AND DM.SUB=INGRESP.SUB "
						+ " LEFT JOIN INGEGNERE ING ON INGRESP.ID_INGEGNERE=ING.ID "

						+ " ORDER BY DM.NUMERO_ARCHIVIO, DM.SUB, DM.ORDINE_FIUME_VALLE DESC, DM.PRIORITA DESC ";
	
				ip=1;
				//System.out.println(sql);
				dmPs = conn.prepareStatement(sql);
				dmPs.setString(ip++, userId); //USERID ACCESSO_DEFAULT
				dmPs.setString(ip++, userId); //USERID UTENTE_CONCESS
				dmPs.setString(ip++, userId); //USERID UTENTE_GESTORE
				
				rs = dmPs.executeQuery();
				
				
				
				int i=1;
				while (rs.next()) {
					
					String idDiga=rs.getString("NUMERO_ARCHIVIO")+rs.getString("SUB");
					DamBean dgBean=digheMap.get(idDiga);
					if(dgBean!=null){
						//DamExtraDataBean dedBean = new DamExtraDataBean(rs.getString("NUMERO_ARCHIVIO")+rs.getString("SUB"), rs.getString("NUMERO_ARCHIVIO"), rs.getString("SUB"));
						DamExtraDataComuneBean comune=new DamExtraDataComuneBean();
						comune.setNumeroArchivio(rs.getString("NUMERO_ARCHIVIO"));
						comune.setSub(rs.getString("SUB"));
						
						comune.setNomeComune(rs.getString("NOME_COMUNE"));
						comune.setNomeProvincia(rs.getString("NOME_PROVINCIA"));
						comune.setNomeRegione(rs.getString("NOME_REGIONE"));
						comune.setZonaSismicaComune(rs.getString("ZONA_SISMICA_COMUNE"));
						//Aggiunge solo se almeno un dato non era null
						if(comune.isInit()){
							comune.setGroup("comune-"+i);
							dgBean.getNomiComune().add(comune);
						}
						
						DamExtraDataUtilizzazioneBean utilizzazione = new DamExtraDataUtilizzazioneBean();
						utilizzazione.setNumeroArchivio(rs.getString("NUMERO_ARCHIVIO"));
						utilizzazione.setSub(rs.getString("SUB"));
						
						utilizzazione.setUtilizzo(rs.getString("UTILIZZAZIONE"));
						utilizzazione.setPriorita(rs.getString("PRIORITA"));
						//Aggiunge solo se almeno un dato non era null
						if(utilizzazione.isInit()){
							utilizzazione.setGroup("utilizzazione-"+i);
							dgBean.getUtilizzo().add(utilizzazione);
						}
						
						//CONC.NOME AS concName, GEST.NOME AS gestName, ING.NOME AS engName, ING.COGNOME AS engSurname, ING.TELEFONO AS engPhone 
						DamExtraDataIngegnereBean ingegnere=new DamExtraDataIngegnereBean();
						ingegnere.setNumeroArchivio(rs.getString("NUMERO_ARCHIVIO"));
						ingegnere.setSub(rs.getString("SUB"));
						
						ingegnere.setNome(rs.getString("engName"), rs.getString("engSurname"));
						ingegnere.setTelefono(rs.getString("engPhone"));
						if(ingegnere.isInit()){
							ingegnere.setGroup("ingegneri-"+i);
							dgBean.addIngegnere(ingegnere);
						}
						
						
						dgBean.addConcessionario(rs.getString("concName"));
						dgBean.addGestore(rs.getString("gestName"));
						
						dgBean.addComuneOperaPresa(rs.getString("COMUNE_OPERA_PRESA"));
						dgBean.addNomeFiumeValle(rs.getString("NOME_FIUME_VALLE"));
						dgBean.addTipoOrganoScarico(rs.getString("TIPO_ORGANO_SCARICO"));
						dgBean.addAltraDenominaz(rs.getString("ALTRA_DENOMINAZIONE"));
						dgBean.addDigheInvaso(rs.getString("DIGHE_INVASO"));
							
						//dgBean.getAltriDati().add(dedBean);
					}
					i++;
				}
			}
		
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, dmPs);
		}
		digheList=new ArrayList<DamBean>(digheMap.values());
		return digheList;
	}
	
	/*public List<DamBean> getDamList() throws SQLException {
		
		String sql = "";
		List<DamBean> damList;
		
		try {
			
			sql = "SELECT TD.NUMERO_ARCHIVIO, TD.SUB, TD.NOME_DIGA, TD.STATUS1, TD.STATUS2, TD.LOCALITA " + 
					", TD.REGIONE_RIFERIMENTO, TD.PROV_RIFERIMENTO, TD.UTILIZZ_PREV, TD.UP, TD.SC, D.ID_BACINO AS BACINO " +
					", DI.ID_INVASO, DI.DIGA_PRINCIPALE, D.CANCELLATO, D.ID_UTENTE, D.DATA_MODIFICA " + 
					"FROM VAT_TUTTEDIGHE TD " +
					"LEFT JOIN VMON_DIGA_INVASO DI ON TD.NUMERO_ARCHIVIO = DI.NUMERO_ARCHIVIO AND TD.SUB = DI.SUB " +
					"LEFT JOIN DIGA D ON TD.NUMERO_ARCHIVIO = D.NUMERO_ARCHIVIO AND TD.SUB = D.SUB " +
					"WHERE D.CANCELLATO<>'S' ";
			
			ResultSet rs = dbUtil.getConnection().createStatement().executeQuery(sql);
			
			damList = new ArrayList<DamBean>();
			
			while (rs.next()) {
				damList.add(new DamBean(rs.getString("NUMERO_ARCHIVIO")+rs.getString("SUB")
										, rs.getString("NUMERO_ARCHIVIO")
										, rs.getString("SUB")
										, rs.getString("NOME_DIGA")
										, rs.getString("STATUS1")
										, rs.getString("STATUS2")
										, rs.getString("LOCALITA")
										, rs.getString("REGIONE_RIFERIMENTO")
										, rs.getString("PROV_RIFERIMENTO")
										, rs.getString("UTILIZZ_PREV")
										, rs.getString("UP")
										, rs.getString("SC")
										, rs.getString("BACINO")
										, rs.getString("ID_INVASO")
										, rs.getString("DIGA_PRINCIPALE")
										, rs.getString("DATA_MODIFICA") //lastModifiedDate
										, rs.getString("ID_UTENTE") //lastModifiedUserId
										, rs.getString("CANCELLATO") //isDeleted
										));
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			dbUtil.closeConnection();
		}
		
		return damList;
	}*/
	
	public List<DamBean> getDamListWithSameIdInvaso(String damId, String userId, int hasAccess) throws SQLException {

		String sql = "";
		List<DamBean> damList;
		PreparedStatement dmPs = null;
		ResultSet rs = null;
		Connection conn=null;
		
		try {
			
			//extract numeroarchivio and sub from iddiga
			Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
			String sub = damId.substring(damId.length()-1, damId.length());
			
			sql = "select DS.*" +
					"from V_ELENCO_DIGHE_SINGOLI DS " +
					/*"INNER JOIN  (select distinct NUMERO_ARCHIVIO, SUB from ACCESSO_DEFAULT where ID_GRUPPO in " + 
								"(select ID_GRUPPO from UTENTE_GRUPPO UG left join UTENTE U on UG.ID_UTENTE = U.ID where U.USERID = ? ) ) AD " +*/
//								"INNER JOIN  ( " + 
//								"select distinct NUMERO_ARCHIVIO, SUB from ACCESSO_DEFAULT " + 
//								
//								"where ID_GRUPPO in (select ID_GRUPPO from UTENTE_GRUPPO UG " + 
//								"left join UTENTE U on UG.ID_UTENTE = U.ID where U.USERID = ?) " + 
//								
//								"UNION SELECT DISTINCT NUMERO_ARCHIVIO, SUB FROM DIGA_CONCESS DC " +
//								"LEFT JOIN UTENTE_CONCESS UC ON UC.ID_CONCESS = DC.ID_CONCESS " +
//								"LEFT JOIN UTENTE U ON UC.ID_UTENTE = U.ID WHERE U.USERID = ? " +
//								
//								"UNION SELECT DISTINCT NUMERO_ARCHIVIO, SUB FROM DIGA_GESTORE DG " +
//								"LEFT JOIN UTENTE_GESTORE UG ON UG.ID_GESTORE = DG.ID_GESTORE " +
//								"LEFT JOIN UTENTE U ON UG.ID_UTENTE = U.ID WHERE U.USERID = ? " +
//								
//								") AD on DS.NUMERO_ARCHIVIO = AD.NUMERO_ARCHIVIO and DS.SUB = AD.SUB " +
					"WHERE DS.ID_INVASO = (SELECT ID_INVASO FROM V_ELENCO_DIGHE_SINGOLI WHERE NUMERO_ARCHIVIO = ? AND SUB = ?) " +
					"order by DS.NUMERO_ARCHIVIO, DS.SUB";

			conn=dbUtil.getNewConnection();
			dmPs = conn.prepareStatement(sql);
			int i=1;
			//dmPs.setString(i++, userId); //USERID ACCESSO_DEFAULT
			//dmPs.setString(i++, userId); //USERID UTENTE_CONCESS
			//dmPs.setString(i++, userId); //USERID UTENTE_GESTORE
			dmPs.setInt(i++, numeroArchivio); //NUMERO_ARCHIVIO
			dmPs.setString(i++, sub); //SUB
			
			rs = dmPs.executeQuery();
			
			damList = new ArrayList<DamBean>();
			
			while (rs.next()) {
				
				DamBean dmBean = new DamBean(rs.getString("NUMERO_ARCHIVIO")+rs.getString("SUB")
											, rs.getString("NUMERO_ARCHIVIO")
											, rs.getString("SUB")
											, rs.getString("NOME_DIGA"));
				
				dmBean.setBacino(rs.getString("NOME_BACINO"));
				dmBean.setIdInvaso(rs.getString("ID_INVASO"));
				
				damList.add(dmBean);
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, dmPs);
		}
		
		return damList;
	}
	
	//Map<String, LinkedHashMap<String, CampoInfoBean>>
	public Set<FiumeBean> getRiverExtraInfo() throws SQLException {

		String sql = "";
		PreparedStatement dmPs = null;
		ResultSet rs = null;
		Connection conn=null;
		Set<FiumeBean> rivers=new LinkedHashSet<FiumeBean>();
		Map<String, LinkedHashMap<String, CampoInfoBean>> extraInfoMap=new LinkedHashMap<String, LinkedHashMap<String, CampoInfoBean>>();
		try {
			
			conn=dbUtil.getNewConnection();
			sql = " SELECT a.ID AS idInfo, a.FIUME AS fiume, a.CAMPO AS campo, a.VALORE_CHAR AS valoreChar, a.VALORE_NUM AS valoreNum, "
					+ " b.ID AS idCampo, b.NOME AS nome, b.UNITA AS unita, b.NUMERICO AS numerico "
					+ " FROM CAMPO_INFO_FIUME_EXTRA b LEFT JOIN INFO_FIUME_EXTRA a ON b.ID=a.CAMPO ";
			dmPs = conn.prepareStatement(sql);

			rs = dmPs.executeQuery();
			//Hashmap<String (ID Oggetto), HashMap<String(nome campo), CampoInfo>>
			while(rs.next()){
				//Carica i dati del valore singolo
				InfoFiumeExtraBean info=this.fillInfoFiumeExtraBean(rs);
				//Controlla se la mappa dei campi per una certa diga sia stato già settato
				if(extraInfoMap.get(info.getIdentifier())==null){
					extraInfoMap.put(info.getIdentifier(), new LinkedHashMap<String, CampoInfoBean>());
				}

				LinkedHashMap<String, CampoInfoBean> newDamMap=extraInfoMap.get(info.getIdentifier());

				//Controlla che il bean del campo sia stato già creato
				if(newDamMap.get(rs.getString("nome"))==null){
					newDamMap.put(rs.getString("nome"), this.fillCampoInfoBean(rs));
				}

				//Aggiunge il nuovo infodigaextrabean all'array di valori
				newDamMap.get(rs.getString("nome")).addValue(info);

			}
			
			DBUtility.closeQuietly(rs);
			DBUtility.closeQuietly(dmPs);
			
			//Recupera tutti i fiumi
			sql = " SELECT c.ID AS id, c.ID_UTENTE AS idUtente, c.DATA_MODIFICA AS dataModifica, c.CANCELLATO AS cancellato, "
				+ "c.ID_BACINO AS idBacino, c.ID_VERSANTE AS idVersante, c.ID_TIPO_FIUME AS idTipoFiume, "
				+ "c.ID_FIUME_PRINCIPALE AS idFiumePrincipale, c.NOME AS nome, c.ORDINE AS ordine FROM FIUME c ";
			
			
			dmPs = conn.prepareStatement(sql);
			rs = dmPs.executeQuery();
			
			while(rs.next()){
				FiumeBean fiume=this.fillRiverBean(rs);
				if(extraInfoMap.get(new Long(fiume.getId()).toString())!=null){
					fiume.setExtraInfo(extraInfoMap.get(new Long(fiume.getId()).toString()));
				}
				rivers.add(fiume);
			}
			
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, dmPs);
		}
		
		return rivers;
	}
	
	private FiumeBean fillRiverBean(ResultSet rs) throws SQLException {
		FiumeBean fiume=new FiumeBean();
		fiume.setId(rs.getLong("id"));
		fiume.setNome(rs.getString("nome"));
		fiume.setCancellato(rs.getString("cancellato"));
		fiume.setDataModifica(rs.getTimestamp("dataModifica"));
		fiume.setIdBacino(rs.getString("idBacino"));
		fiume.setIdFiumePrincipale(rs.getLong("idFiumePrincipale"));
		fiume.setIdTipoFiume(rs.getString("idTipoFiume"));
		fiume.setIdUtente(rs.getString("idUtente"));
		fiume.setIdVersante(rs.getString("idVersante"));
		fiume.setOrdine(rs.getInt("ordine"));
		return fiume;
	}
	
	public Map<String, LinkedHashMap<String, CampoInfoBean>> getDamExtraInfo(String userId) throws SQLException {

		//userId="DCHIAROLLA";
		String sql = "";
		PreparedStatement dmPs = null;
		ResultSet rs = null;
		Connection conn=null;
		Map<String, LinkedHashMap<String, CampoInfoBean>> extraInfoMap=new LinkedHashMap<String, LinkedHashMap<String, CampoInfoBean>>();
		try {

			sql = " select DS.ID AS idInfo, DS.ARC AS archivio, DS.SUB AS sub, DS.CAMPO AS campo, DS.VALORE_CHAR AS valoreChar, DS.VALORE_NUM AS valoreNum, "
					+ "CDE.ID AS idCampo, CDE.NOME as nome, CDE.UNITA AS unita, CDE.NUMERICO AS numerico FROM INFO_DIGA_EXTRA DS " +
			"INNER JOIN  ( " + 
			"select distinct NUMERO_ARCHIVIO, SUB from ACCESSO_DEFAULT " + 
			"where ID_GRUPPO in (select ID_GRUPPO from UTENTE_GRUPPO UG " + 
			"left join UTENTE U on UG.ID_UTENTE = U.ID where U.USERID = ?) " + 
			
			"UNION SELECT DISTINCT NUMERO_ARCHIVIO, SUB FROM DIGA_CONCESS DC " +
			"LEFT JOIN UTENTE_CONCESS UC ON UC.ID_CONCESS = DC.ID_CONCESS " +
			"LEFT JOIN UTENTE U ON UC.ID_UTENTE = U.ID WHERE U.USERID = ? "
			+ " AND UC.CANCELLATO= ? " +
			
			"UNION SELECT DISTINCT NUMERO_ARCHIVIO, SUB FROM DIGA_GESTORE DG " +
			"LEFT JOIN UTENTE_GESTORE UG ON UG.ID_GESTORE = DG.ID_GESTORE " +
			"LEFT JOIN UTENTE U ON UG.ID_UTENTE = U.ID WHERE U.USERID = ? "
			+ " AND UG.CANCELLATO= ? " +
			") AD on DS.ARC = AD.NUMERO_ARCHIVIO and DS.SUB = AD.SUB LEFT JOIN CAMPO_INFO_DIGA_EXTRA CDE ON CDE.ID=DS.CAMPO ";


			sql+=" order by DS.ARC, DS.SUB ";
			conn=dbUtil.getNewConnection();
			dmPs = conn.prepareStatement(sql);
			int ip=1;
			dmPs.setString(ip++, userId); //USERID ACCESSO_DEFAULT
			dmPs.setString(ip++, userId); //USERID UTENTE_CONCESS
			dmPs.setString(ip++, "N"); //CANCELLATO UTENTE_CONCESS
			dmPs.setString(ip++, userId); //USERID UTENTE_GESTORE
			dmPs.setString(ip++, "N"); //CANCELLATO UTENTE_GESTORE
			
			rs = dmPs.executeQuery();
			//Hashmap<String (ID Oggetto), HashMap<String(nome campo), CampoInfo>>
			while(rs.next()){
				//Carica i dati del valore singolo
				InfoDigaExtraBean info=this.fillInfoDigaExtraBean(rs);
				//Controlla se la mappa dei campi per una certa diga sia stato già settato
				if(extraInfoMap.get(info.getIdentifier())==null){
					extraInfoMap.put(info.getIdentifier(), new LinkedHashMap<String, CampoInfoBean>());
				}
				
				LinkedHashMap<String, CampoInfoBean> newDamMap=extraInfoMap.get(info.getIdentifier());
				
				//Controlla che il bean del campo sia stato già creato
				if(newDamMap.get(rs.getString("nome"))==null){
					newDamMap.put(rs.getString("nome"), this.fillCampoInfoBean(rs));
				}
				
				//Aggiunge il nuovo infodigaextrabean all'array di valori
				newDamMap.get(rs.getString("nome")).addValue(info);
				
			}
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, dmPs);
		}
		
		return extraInfoMap;
	}
	
	private CampoInfoBean fillCampoInfoBean(ResultSet rs) throws SQLException {
		CampoInfoBean campoInfo=new CampoInfoBean();
		campoInfo.setId(rs.getLong("idCampo"));
		campoInfo.setNome(rs.getString("nome"));
		campoInfo.setNumeric(rs.getBoolean("numerico"));
		campoInfo.setUnita(rs.getString("unita"));
		return campoInfo;
	}

	private InfoDigaExtraBean fillInfoDigaExtraBean(ResultSet rs) throws SQLException{
		InfoDigaExtraBean info=new InfoDigaExtraBean();
		this.fillInfoExtraBean(rs, info);
		info.setArchivio(rs.getString("archivio"));
		info.setSub(rs.getString("sub"));
		return info;
	}
	
	private InfoFiumeExtraBean fillInfoFiumeExtraBean(ResultSet rs) throws SQLException{
		InfoFiumeExtraBean info=new InfoFiumeExtraBean();
		this.fillInfoExtraBean(rs, info);
		info.setFiume(rs.getLong("fiume"));
		return info;
	}
	
	/**
	 * Fills common data for both extra diga and extra fiume beans
	 * @param rs
	 * @param info
	 * @return
	 * @throws SQLException
	 */
	private InfoExtraBean fillInfoExtraBean(ResultSet rs, InfoExtraBean info) throws SQLException{
		info.setId(rs.getLong("idInfo"));
		info.setNumeric(rs.getBoolean("numerico"));
		info.setCampo(rs.getLong("campo"));
		if(info.isNumeric()){
			info.setValore(new Double(rs.getDouble("valoreNum")).toString());
		} else {
			info.setValore(rs.getString("valoreChar"));
		}
		return info;
	}
	
	public Boolean setDamsFavorite(ArrayList<DamBean> dams, Long userId, boolean favorite, boolean closeConnection) throws SQLException {

		String sql = "";
		int updatedRows = 0;
		ResultSet rs = null;
		PreparedStatement dmPs = null;
		Connection conn=null;
		
		try {
			//Carica i preferiti già esistenti
			sql = "select * FROM DIGHE_PREFERITE WHERE ID_UTENTE = ?";
			conn=dbUtil.getNewConnection();
			dmPs=conn.prepareStatement(sql);
			int ip=1;
			dmPs.setString(ip++, new Long(userId).toString());
			rs = dmPs.executeQuery();
			Map<String, Boolean> currentFavorites=new HashMap<String, Boolean>();
			while (rs.next()) {
				String favoriteDamId=rs.getString("NUMERO_ARCHIVIO")+rs.getString("SUB");
				currentFavorites.put(favoriteDamId, true);
			}
			dmPs.close();
			rs.close();
			//Scorre le dighe da inserire come preferite
			for(DamBean dam : dams) {
				String numeroArchivio=dam.getNumeroArchivio();
				String sub=dam.getSub();
				//Controlla che esista già la preferenza 
				/* 
				 Determina che azione intraprendere
				 Se favorite è false e c'è un risultato, lo deve cancellare
				 Se favorite è true e non ci sono risultati deve aggiungerlo
				 negli altri casi non fa nulla
				*/
				ip=1;
				boolean mustQuery=false;
				logger.info(new Long(userId).toString());
				if(favorite && (currentFavorites.get(numeroArchivio+sub)==null || currentFavorites.get(numeroArchivio+sub).equals(false))){
					sql = "INSERT INTO DIGHE_PREFERITE (ID_UTENTE, NUMERO_ARCHIVIO, SUB) VALUES (?, ?, ?)";
					mustQuery=true;
				} else if(!favorite && (currentFavorites.get(numeroArchivio+sub)!=null && currentFavorites.get(numeroArchivio+sub).equals(true))){
					sql = "DELETE FROM DIGHE_PREFERITE WHERE ID_UTENTE = ? AND NUMERO_ARCHIVIO = ? AND SUB = ?";
					mustQuery=true;
				}
				if(mustQuery){
					dmPs=conn.prepareStatement(sql);
					dmPs.setString(ip++, new Long(userId).toString());
					dmPs.setLong(ip++, Long.parseLong(numeroArchivio));
					dmPs.setString(ip++, sub);
					updatedRows += dmPs.executeUpdate();
					dmPs.close();
				}
			}			
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		}  finally {
			DBUtility.closeQuietly(conn, rs, dmPs);
		}

		return (updatedRows > 0);
	}

	public Boolean addEntityToDam(String damId, BasicEntityBean targetEntity, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {

		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		int nextValId = 0;
		Connection conn=null;
		
		ResultSet nextValDmRs = null;
		PreparedStatement dmPs = null;
		
		//extract numeroarchivio and sub from damId
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
		
		try {
			conn=dbUtil.getNewConnection();
			if (closeConnection) {
				//Starting transaction 
				conn.setAutoCommit(false);
			}
			
			//retrieve nextval for targetEntity 
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "select DIGA_AUTORITA_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "select DIGA_CONCESS_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "select DIGA_GESTORE_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "select DIGA_AENTE_SEQUENZA.nextval from dual";
			}
			
			
			nextValDmRs = conn.createStatement().executeQuery(sql);
			
			if (nextValDmRs != null && nextValDmRs.next()) {
				nextValId = nextValDmRs.getInt(1);
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}
		
			//insert into targetEntity junction table
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "insert into DIGA_AUTORITA (ID, NUMERO_ARCHIVIO, SUB, ID_AUTORITA, DATA_NOMINA, ID_UTENTE) values ( ?, ?, ?, ?, SYSDATE, ?)";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "insert into DIGA_CONCESS (ID, NUMERO_ARCHIVIO, SUB, ID_CONCESS, DATA_NOMINA, ID_UTENTE) values ( ?, ?, ?, ?, SYSDATE, ?)";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "insert into DIGA_GESTORE (ID, NUMERO_ARCHIVIO, SUB, ID_GESTORE, DATA_NOMINA, ID_UTENTE) values ( ?, ?, ?, ?, SYSDATE, ?)";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "insert into DIGA_ALTRA_ENTE (ID, NUMERO_ARCHIVIO, SUB, ID_ENTE, DATA_NOMINA, ID_UTENTE) values ( ?, ?, ?, ?, SYSDATE, ?)";
			}
			
			dmPs = conn.prepareStatement(sql);
			
			dmPs.setInt(paramsSet++, nextValId); //ID
			dmPs.setInt(paramsSet++, numeroArchivio); //NUMERO_ARCHIVIO
			dmPs.setString(paramsSet++, sub); //SUB
			dmPs.setInt(paramsSet++, Integer.parseInt(targetEntity.getId())); //ID_ENTITY
			dmPs.setString(paramsSet++, userId); //ID_UTENTE
			
			updatedRows += dmPs.executeUpdate();
			
			//commiting changes
			if (closeConnection) {
				conn.commit();
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			if (closeConnection) {
				conn.rollback();
			}

			throw sqle;
		} finally {
			DBUtility.closeQuietly(dmPs);
			DBUtility.closeQuietly(nextValDmRs);
			
			if (closeConnection) {
				conn.setAutoCommit(true);
				DBUtility.closeQuietly(conn);
			} else {
				//In questo modo le connessioni si chiudono sempre tanto ce n'è una sola per metodo
				DBUtility.closeQuietly(conn);
			}
		}

		return (updatedRows > 0);
	}
	
	public Boolean removeEntityFromDam(String damId, BasicEntityBean targetEntity, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		Boolean wasDeleted = false;
		PreparedStatement dmPs = null;
		Connection conn=null;

		//extract numeroarchivio and sub from damId
		Integer numeroArchivio = Integer.parseInt(damId.substring(0, damId.length()-1));
		String sub = damId.substring(damId.length()-1, damId.length());
		
		try {

			//remove targetEntity junction table
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "update DIGA_AUTORITA set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where NUMERO_ARCHIVIO=? AND SUB=? AND ID_AUTORITA =? ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "update DIGA_CONCESS set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where NUMERO_ARCHIVIO=? AND SUB=? AND ID_CONCESS =? ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "update DIGA_GESTORE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where NUMERO_ARCHIVIO=? AND SUB=? AND ID_GESTORE =? ";
			}else if (targetEntity instanceof AltraEnteBean) {
				sql = "update DIGA_ALTRA_ENTE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where NUMERO_ARCHIVIO=? AND SUB=? AND ID_ENTE =? ";
			}
			
			//Retrieving connection
			conn=dbUtil.getNewConnection();
			dmPs = conn.prepareStatement(sql);
			dmPs.setString(paramsSet++, userId); //ID_UTENTE
			dmPs.setString(paramsSet++, "S"); //CANCELLATO
			dmPs.setInt(paramsSet++, numeroArchivio); //NUMERO_ARCHIVIO
			dmPs.setString(paramsSet++, sub); //SUB
			dmPs.setInt(paramsSet++, Integer.parseInt(targetEntity.getId())); //ID_ENTITY
			
			updatedRows += dmPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Dam ID= "+damId+" and Entity ID="+targetEntity.getId()+" not found.");
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn);
			DBUtility.closeQuietly(dmPs);
		}
		
		return wasDeleted;
	}

}
