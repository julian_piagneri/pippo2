package it.cinqueemmeinfo.dammap.dao.entities;

import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;
import it.cinqueemmeinfo.dammap.dao.fields.AddressDao;
import it.cinqueemmeinfo.dammap.dao.fields.ContactInfoDao;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class GestoreDao implements Serializable {

	private static final long serialVersionUID = -2309103738977369595L;
	private static final Logger logger = LoggerFactory.getLogger(GestoreDao.class);

	@Autowired
	private DBUtility dbUtil;
	@Autowired
	private BeanUtility bnUtil;
	@Autowired
	private ContactInfoDao ciDao;
	@Autowired
	private AddressDao adDao;

	public GestoreBean getGestoreWithParams(GestoreBean gsBean, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		List<GestoreBean> gsList = getGestoreListWithParams(2, "", "", gsBean, true, closeConnection);

		if (gsList.size() > 0) {
			return gsList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}

	public GestoreBean getGestoreWithParams(int hasAccess, String userId, GestoreBean gsBean, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		List<GestoreBean> gsList = getGestoreListWithParams(hasAccess, userId, "", gsBean, true, closeConnection);

		if (gsList.size() > 0) {
			return gsList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}

	public List<GestoreBean> getGestoreListWithParams(int hasAccess, String userId, String idConcessionario,
			GestoreBean gsBean, boolean getExtraInfos, boolean closeConnection) throws SQLException {

		int paramsSet = 1;
		String sql = "";
		String whereSql = "";
		GestoreBean foundGsBean = null;
		ArrayList<ContactInfoBean> ciList = null;
		ArrayList<AddressBean> adList = null;
		ArrayList<GestoreBean> gsList = new ArrayList<GestoreBean>();

		ResultSet gsRs = null;
		PreparedStatement gsPs = null;

		try {

			if (!"".equals(idConcessionario)) {
				sql = "SELECT G.ID, G.NOME, G.ID_CONCESSIONARIO, G.DATA_MODIFICA, G.ID_UTENTE, G.CANCELLATO "
						+ "FROM GESTORE G " + "LEFT JOIN CONCESS_GESTORE CG ON CG.ID_GESTORE = G.ID ";
			} else {
				sql = "SELECT G.ID, G.NOME, G.ID_CONCESSIONARIO, G.DATA_MODIFICA, G.ID_UTENTE, G.CANCELLATO "
						+ "FROM GESTORE G ";
			}

			if (hasAccess == 1) {
				sql += " LEFT JOIN UTENTE_GESTORE UG ON G.ID=UG.ID_GESTORE AND UG.CANCELLATO='N'  ";
			}

			// WHERE PARAM CHECK
			whereSql += "G.CANCELLATO = 'N' ";

			if (!"".equals(gsBean.getId()) && gsBean.getId() != null) {
				whereSql += "AND G.ID = ? ";
			}
			/*
			 * if (!"".equals(gsBean.getNome()) && gsBean.getNome() != null) {
			 * whereSql += "AND G.NOME LIKE ?  "; }
			 */
			if (!"".equals(idConcessionario) && idConcessionario != null) {
				whereSql += "AND CG.ID_CONCESS = ? AND CG.CANCELLATO = 'N' ";
			}

			if (hasAccess == 1 && !userId.equals("") && userId != null) {
				whereSql += " AND UG.ID_UTENTE IN (SELECT UP.ID FROM UTENTE UP WHERE UP.USERID= ?) ";
			}

			// WHERE STRING BUILDER
			if (whereSql.length() > 0) {
				if (whereSql.startsWith("AND")) {
					whereSql = whereSql.substring(4, whereSql.length());
				}

				sql += " WHERE " + whereSql;
			}

			// ORDERBY
			sql += "ORDER BY ID ASC ";

			// preparing statement
			gsPs = dbUtil.getConnection().prepareStatement(sql);

			// Setting query parameters
			if (!"".equals(gsBean.getId()) && gsBean.getId() != null) {
				gsPs.setInt(paramsSet++, Integer.parseInt(gsBean.getId()));
			}
			/*
			 * if (!"".equals(gsBean.getNome()) && gsBean.getNome() != null) {
			 * gsPs.setString(paramsSet++, gsBean.getNome()); }
			 */
			if (!"".equals(idConcessionario) && idConcessionario != null) {
				gsPs.setString(paramsSet++, idConcessionario);
			}

			if (hasAccess == 1 && !userId.equals("") && userId != null) {
				gsPs.setString(paramsSet++, userId);
			}

			// execute
			gsRs = gsPs.executeQuery();

			while (gsRs.next()) {

				foundGsBean = new GestoreBean(gsRs.getString("ID"), gsRs.getString("NOME"), null, null,
						gsRs.getString("ID_CONCESSIONARIO"), gsRs.getString("DATA_MODIFICA"),
						gsRs.getString("ID_UTENTE"), gsRs.getString("CANCELLATO"));

				if (getExtraInfos) {
					// retrieve phone numbers
					ciList = (ArrayList<ContactInfoBean>) ciDao.getContactInfoListWithParams(foundGsBean, false);
					// retrieve address list
					adList = (ArrayList<AddressBean>) adDao.getAddressListWithParams(foundGsBean, false);

					foundGsBean.setIndirizzo(adList);
					foundGsBean.setContatto(ciList);
				}

				// ADD NEW ITEM
				gsList.add(foundGsBean);
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (gsPs != null) {
				gsPs.close();
			}

			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}

		return gsList;
	}

	public List<GestoreBean> getGestoreListFromIdConcessionario(String idConcessionario, boolean closeConnection)
			throws SQLException {

		List<GestoreBean> gsList = getGestoreListWithParams(2, "", idConcessionario, new GestoreBean(), false,
				closeConnection);

		return gsList;
	}

	/**
	 * 
	 * @param gsBean
	 * @param userId
	 * @param closeConnection
	 *            if true closes connection and commmits changes, if false
	 *            transaction will be kept open for futher operations
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException
	 */
	public GestoreBean createGestore(GestoreBean gsBean, String userId, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		ResultSet gsNextValRs = null;
		PreparedStatement gsPs = null;
		GestoreBean createdGsBean = null;

		try {

			// Starting transaction
			dbUtil.setAutoCommit(false);

			// Get entity id
			sql = "select GESTORE_SEQUENZA.nextval from dual";
			gsNextValRs = dbUtil.getConnection().createStatement().executeQuery(sql);

			if (gsNextValRs != null && gsNextValRs.next()) {
				gsBean.setId(gsNextValRs.getString(1));
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}

			// INSERT GESTORE
			sql = "insert into GESTORE (ID, NOME, ID_UTENTE ) values ( ?,?,? )";

			gsPs = dbUtil.getConnection().prepareStatement(sql);

			gsPs.setString(paramsSet++, gsBean.getId()); // ID
			gsPs.setString(paramsSet++, gsBean.getNome()); // NOME
			gsPs.setString(paramsSet++, userId); // ID_UTENTE

			updatedRows += gsPs.executeUpdate();

			if (closeConnection) {
				dbUtil.getConnection().commit();
			}

			if (updatedRows > 0) {
				// INSERT CONTACT INFO
				for (ContactInfoBean pnBean : gsBean.getContatto()) {
					ciDao.createContactInfo(pnBean, gsBean, userId, false);
				}

				// INSERT ADDRESS
				for (AddressBean adBean : gsBean.getIndirizzo()) {
					adDao.createAddress(adBean, gsBean, userId, false);
				}

				// RETRIEVING CREATED ROW
				createdGsBean = getGestoreWithParams(gsBean, false);
			} else {
				throw new SQLException("Insert not executed, error while creating record");
			}

			if (closeConnection) {
				dbUtil.getConnection().commit();
			}

		} catch (SQLException sqle) {
			if (closeConnection) {
				dbUtil.rollback();
			}
			logger.error("SQLException with message: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (gsNextValRs != null) {
				gsNextValRs.close();
			}
			if (gsPs != null) {
				gsPs.close();
			}

			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}

		return createdGsBean;
	}

	/**
	 * 
	 * @param gsBean
	 * @param userId
	 * @param closeConnection
	 *            if true closes connection and commmits changes, if false
	 *            transaction will be kept open for futher operations
	 * @return
	 * @throws Exception
	 */
	public GestoreBean updateGestore(GestoreBean gsBean, String userId, boolean closeConnection) throws Exception {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement gsPs = null;
		GestoreBean foundGsBean = null;

		try {

			foundGsBean = (GestoreBean) bnUtil.compareAndFillBean(getGestoreWithParams(gsBean, true), gsBean);

			sql = "update GESTORE set NOME=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=?";

			// Retrieving connection
			dbUtil.setAutoCommit(false);
			gsPs = dbUtil.getConnection().prepareStatement(sql);

			gsPs.setString(paramsSet++, foundGsBean.getNome()); // NOME
			gsPs.setString(paramsSet++, userId); // ID_UTENTE
			gsPs.setInt(paramsSet++, Integer.parseInt(foundGsBean.getId())); // ID

			updatedRows += gsPs.executeUpdate();

			if (closeConnection) {
				dbUtil.getConnection().commit();
			}

			if (updatedRows > 0) {
				// UPDATE CONTACT INFO
				if (gsBean.getContatto() != null) {
					for (ContactInfoBean pnBean : gsBean.getContatto()) {
						ciDao.updateContactInfo(pnBean, gsBean, userId, false);
					}
				}
				// UPDATE ADDRESS
				if (gsBean.getIndirizzo() != null) {
					for (AddressBean adBean : gsBean.getIndirizzo()) {
						adDao.updateAddress(adBean, gsBean, userId, false);
					}
				}

				foundGsBean = getGestoreWithParams(gsBean, true);
			} else {
				throw new EntityNotFoundException("Entity with ID=" + gsBean.getId() + " not found.");
			}

			// commit
			if (closeConnection) {
				dbUtil.getConnection().commit();
			}

		} catch (Exception e) {
			logger.error("Error while executing '" + sql + "'");
			if (closeConnection) {
				dbUtil.getConnection().rollback();
			}
			throw e;
		} finally {
			if (gsPs != null) {
				gsPs.close();
			}

			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}

		return foundGsBean;
	}

	/**
	 * 
	 * @param entityId
	 * @param userId
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public Boolean removeGestore(String entityId, String userId, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement gsPs = null;
		Boolean wasDeleted = false;

		try {

			sql = "update GESTORE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=?";

			// Retrieving connection
			gsPs = dbUtil.getConnection().prepareStatement(sql);
			gsPs.setString(paramsSet++, userId); // ID_UTENTE
			gsPs.setString(paramsSet++, "S"); // CANCELLATO
			gsPs.setInt(paramsSet++, Integer.parseInt(entityId)); // ID

			updatedRows += gsPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Entity with ID=" + entityId + " not found.");
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (gsPs != null) {
				gsPs.close();
			}

			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}

		return wasDeleted;
	}
}
