package it.cinqueemmeinfo.dammap.dao.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;
import it.cinqueemmeinfo.dammap.dao.fields.AddressDao;
import it.cinqueemmeinfo.dammap.dao.fields.ContactInfoDao;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

public class AutoritaProtezioneCivileDao implements Serializable {

	private static final long serialVersionUID = -3476634173686482695L;
	private static final Logger logger = LoggerFactory.getLogger(AutoritaProtezioneCivileDao.class);

	@Autowired
	private DBUtility dbUtil;
	@Autowired
	private BeanUtility bnUtil;
	@Autowired
	private AddressDao adDao;
	@Autowired
	private ContactInfoDao ciDao;

	/**
	 * Connection must be handled outside
	 * 
	 * @param apcBean
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException
	 */
	public AutoritaProtezioneCivileBean getAutoritaProtezioneCivileWithParams(AutoritaProtezioneCivileBean apcBean, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		List<AutoritaProtezioneCivileBean> apcList = getAutoritaProtezioneCivileListWithParams(2, "", apcBean, true, closeConnection);

		if (apcList.size() > 0) {
			return apcList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}

	public AutoritaProtezioneCivileBean getAutoritaProtezioneCivileWithParams(int hasAccess, String userId, AutoritaProtezioneCivileBean apcBean,
			boolean closeConnection) throws SQLException, EntityNotFoundException {

		List<AutoritaProtezioneCivileBean> apcList = getAutoritaProtezioneCivileListWithParams(hasAccess, userId, apcBean, true, closeConnection);

		if (apcList.size() > 0) {
			return apcList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}

	/**
	 * Connection must be handled outside
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<AutoritaProtezioneCivileBean> getAutoritaProtezioneCivileListWithParams(int hasAccess, String userId, AutoritaProtezioneCivileBean apcBean,
			boolean getExtraInfos, boolean closeConnection) throws SQLException {

		int paramsSet = 1;
		String sql = "";
		@SuppressWarnings("unused")
		String whereSql = "";
		AutoritaProtezioneCivileBean foundApcBean = null;
		ArrayList<ContactInfoBean> pnList = null;
		ArrayList<AddressBean> adList = null;
		ArrayList<AutoritaProtezioneCivileBean> apcList = new ArrayList<AutoritaProtezioneCivileBean>();

		ResultSet apcRs = null;
		PreparedStatement apcPs = null;
		Connection conn = null;

		try {

			// SELECT
			if (!"".equals(apcBean.getId()) && apcBean.getId() != null) {
				sql = "SELECT APC.ID, APC.SIGLA_PROVINCIA, APC.NOME_REGIONE, APC.DESCRIZIONE "
						+ ", APC.TIPO_AUTORITA, APC.DATA_MODIFICA, APC.ID_UTENTE, APC.CANCELLATO " + "FROM AUTORITA_PROTEZIONE_CIVILE APC "
						+ this.addAssociatedOnlyJoinQuery(hasAccess) + "WHERE APC.CANCELLATO<>'S' AND APC.ID = ? "
						+ this.addAssociatedOnlyWhereQuery(hasAccess, userId) + " ORDER BY APC.ID ASC ";
			} else {
				sql = "SELECT APC.ID, APC.SIGLA_PROVINCIA, APC.NOME_REGIONE, APC.DESCRIZIONE "
						+ ", APC.TIPO_AUTORITA, APC.DATA_MODIFICA, APC.ID_UTENTE, APC.CANCELLATO " + "FROM AUTORITA_PROTEZIONE_CIVILE APC "
						+ this.addAssociatedOnlyJoinQuery(hasAccess) + "WHERE APC.CANCELLATO<>'S' " + this.addAssociatedOnlyWhereQuery(hasAccess, userId)
						+ " ORDER BY APC.ID ASC ";
			}

			logger.info("Executing: " + sql);

			// WHERE PARAM CHECK
			/*
			 * whereSql += "";
			 * 
			 * if (!"".equals(apcBean.getId()) && apcBean.getId() != null) {
			 * whereSql += "AND APC.ID = ? "; } if
			 * (!"".equals(apcBean.getSiglaProvincia()) &&
			 * apcBean.getSiglaProvincia() != null) { whereSql +=
			 * "AND APC.SIGLA_PROVINCIA = ? "; } if
			 * (!"".equals(apcBean.getNomeRegione()) && apcBean.getNomeRegione()
			 * != null) { whereSql += "AND APC.NOME_REGIONE = ? "; } if
			 * (!"".equals(apcBean.getDescrizione()) && apcBean.getDescrizione()
			 * != null) { whereSql += "AND APC.DESCRIZIONE LIKE ? "; } if
			 * (apcBean.getTipoAutorita() != null) { whereSql +=
			 * "AND APC.TIPO_AUTORITA = ? "; } //WHERE STRING BUILDER if
			 * (whereSql.length() > 0) { if (whereSql.startsWith("AND")) {
			 * whereSql = whereSql.substring(4, whereSql.length()); }
			 * 
			 * sql += " WHERE " + whereSql; }
			 * 
			 * //ORDERBY sql += "ORDER BY APC.ID ASC ";
			 */

			// preparing statement
			conn = dbUtil.getNewConnection();
			apcPs = conn.prepareStatement(sql);

			// Setting query parameters
			// apcPs.setString(paramsSet++, "N"); //add checks for admin?

			if (!"".equals(apcBean.getId()) && apcBean.getId() != null) {
				apcPs.setInt(paramsSet++, Integer.parseInt(apcBean.getId()));
			}
			/*
			 * if (!"".equals(apcBean.getSiglaProvincia()) &&
			 * apcBean.getSiglaProvincia() != null) {
			 * apcPs.setString(paramsSet++, apcBean.getSiglaProvincia()); } if
			 * (!"".equals(apcBean.getNomeRegione()) && apcBean.getNomeRegione()
			 * != null) { apcPs.setString(paramsSet++,
			 * apcBean.getNomeRegione()); } if
			 * (!"".equals(apcBean.getDescrizione()) && apcBean.getDescrizione()
			 * != null) { apcPs.setString(paramsSet++,
			 * "%"+apcBean.getDescrizione()+"%"); } if
			 * (!"".equals(apcBean.getDescrizione()) &&
			 * apcBean.getTipoAutorita() != null) { apcPs.setString(paramsSet++,
			 * apcBean.getTipoAutorita()); }
			 */

			if (hasAccess == 1 && !userId.equals("") && userId != null) {
				apcPs.setString(paramsSet++, userId);
			}

			// execute
			apcRs = apcPs.executeQuery();

			while (apcRs.next()) {

				foundApcBean = new AutoritaProtezioneCivileBean(apcRs.getString("ID"), apcRs.getString("SIGLA_PROVINCIA"), apcRs.getString("NOME_REGIONE"),
						apcRs.getString("DESCRIZIONE"), null // CONTATTO
						, null // INDIRIZZO
						, apcRs.getString("TIPO_AUTORITA"), apcRs.getString("DATA_MODIFICA"), apcRs.getString("ID_UTENTE"), apcRs.getString("CANCELLATO"));

				if (getExtraInfos) {
					// retrieve contacts info
					pnList = (ArrayList<ContactInfoBean>) ciDao.getContactInfoListWithParams(foundApcBean, false);
					// retrieve address list
					adList = (ArrayList<AddressBean>) adDao.getAddressListWithParams(foundApcBean, false);

					foundApcBean.setContatto(pnList);
					foundApcBean.setIndirizzo(adList);
				}

				// ADD NEW ITEM
				apcList.add(foundApcBean);
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, apcRs, apcPs);
		}

		return apcList;
	}

	private String addAssociatedOnlyJoinQuery(int hasAccess) {
		// Adds a join on utente_concess if the user has access only to the
		// associated values
		if (hasAccess == 1) {
			return " LEFT JOIN UTENTE_AUTORITA UA ON APC.ID=UA.ID_AUTORITA AND UA.CANCELLATO='N' ";
		}
		return " ";
	}

	private String addAssociatedOnlyWhereQuery(int hasAccess, String userId) {
		// Adds a join on utente_concess if the user has access only to the
		// associated values
		if (hasAccess == 1 && !userId.equals("") && userId != null) {
			return " AND UA.ID_UTENTE IN (SELECT UP.ID FROM UTENTE UP WHERE UP.USERID= ?) ";
		}
		return " ";
	}

	/**
	 * 
	 * @param apcBean
	 * @param userId
	 * @param closeConnection
	 *            if true closes connection and commmits changes, if false
	 *            transaction will be kept open for futher operations
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException
	 */
	public AutoritaProtezioneCivileBean createAutoritaProtezioneCivile(AutoritaProtezioneCivileBean apcBean, String userId, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		ResultSet apcNextValRs = null;
		PreparedStatement apcPs = null;
		AutoritaProtezioneCivileBean createdApcBean = null;
		Connection conn = null;

		try {

			// Starting transaction

			// Get entity id
			sql = "select AUTORITA_PRO_CIV_SEQUENZA.nextval from dual";
			conn = dbUtil.getNewConnection();
			if (closeConnection) {
				conn.setAutoCommit(false);
			}
			apcNextValRs = conn.createStatement().executeQuery(sql);

			if (apcNextValRs != null && apcNextValRs.next()) {
				apcBean.setId(apcNextValRs.getString(1));
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}

			// test query for autogeneratedkeys return
			/*
			 * sql =
			 * "insert into AUTORITA_PROTEZIONE_CIVILE ( ID, SIGLA_PROVINCIA, NOME_REGIONE, DESCRIZIONE, ID_UTENTE, TIPO_AUTORITA ) "
			 * +
			 * "select AUTORITA_PRO_CIV_SEQUENZA.nextval, SIGLA_PROVINCIA, NOME_REGIONE, DESCRIZIONE, ID_UTENTE, TIPO_AUTORITA from ("
			 * +
			 * "select ? as SIGLA_PROVINCIA, ? as NOME_REGIONE, ? as DESCRIZIONE, ? as ID_UTENTE, ? as TIPO_AUTORITA from dual)"
			 * ;
			 */

			// returns if driver supports getGeneratedKeys
			// Boolean isSupported =
			// dbUtil.getConnection().getMetaData().supportsGetGeneratedKeys();

			// get generated id
			/*
			 * ResultSet keyRs = ps.getGeneratedKeys(); if (keyRs != null &&
			 * keyRs.next()){ id = keyRs.getInt(1); }
			 */

			// INSERT AUTORITA_PROTEZIONE_CIVILE
			sql = "insert into AUTORITA_PROTEZIONE_CIVILE (ID, SIGLA_PROVINCIA, NOME_REGIONE, DESCRIZIONE, ID_UTENTE, TIPO_AUTORITA ) values ( ?,?,?,?,?,? )";

			apcPs = conn.prepareStatement(sql);

			apcPs.setString(paramsSet++, apcBean.getId()); // ID
			apcPs.setString(paramsSet++, apcBean.getSiglaProvincia()); // SIGLA_PROVINCIA
			apcPs.setString(paramsSet++, apcBean.getNomeRegione()); // NOME_REGIONE
			apcPs.setString(paramsSet++, apcBean.getDescrizione()); // DESCRIZIONE
			apcPs.setString(paramsSet++, userId); // ID_UTENTE
			apcPs.setInt(paramsSet++, Integer.parseInt(apcBean.getTipoAutorita())); // TIPO_AUTORITA

			updatedRows += apcPs.executeUpdate();

			if (closeConnection) {
				conn.commit();
			}

			if (updatedRows > 0) {
				// INSERT CONTACT INFO
				for (ContactInfoBean pnBean : apcBean.getContatto()) {
					ciDao.createContactInfo(pnBean, apcBean, userId, false);
				}

				// INSERT ADDRESS
				for (AddressBean adBean : apcBean.getIndirizzo()) {
					adDao.createAddress(adBean, apcBean, userId, false);
				}

				if (closeConnection) {
					conn.commit();
				}

				// RETRIEVING CREATED ROW
				createdApcBean = getAutoritaProtezioneCivileWithParams(apcBean, false);
			} else {
				throw new SQLException("Insert not executed, error while creating record");
			}

			if (closeConnection) {
				conn.commit();
			}

		} catch (SQLException sqle) {
			if (closeConnection) {
				conn.rollback();
			}
			logger.error("SQLException with message: " + sqle.getMessage());
			throw sqle;
		} finally {
			conn.setAutoCommit(true);
			DBUtility.closeQuietly(conn, apcNextValRs, apcPs);
		}

		return createdApcBean;
	}

	/**
	 * 
	 * @param apcBean
	 * @param userId
	 * @param closeConnection
	 *            if true closes connection and commmits changes, if false
	 *            transaction will be kept open for futher operations
	 * @return
	 * @throws Exception
	 */
	public AutoritaProtezioneCivileBean updateAutoritaProtezioneCivile(AutoritaProtezioneCivileBean apcBean, String userId, boolean closeConnection)
			throws Exception {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement apcPs = null;
		AutoritaProtezioneCivileBean foundApcBean = null;
		Connection conn = null;

		try {

			foundApcBean = (AutoritaProtezioneCivileBean) bnUtil.compareAndFillBean(getAutoritaProtezioneCivileWithParams(apcBean, false), apcBean);

			sql = "update AUTORITA_PROTEZIONE_CIVILE set SIGLA_PROVINCIA=?, NOME_REGIONE=?, DESCRIZIONE=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=?, TIPO_AUTORITA=? where ID=?";

			// Retrieving connection
			conn = dbUtil.getNewConnection();
			if (closeConnection) {
				conn.setAutoCommit(false);
			}
			apcPs = conn.prepareStatement(sql);

			apcPs.setString(paramsSet++, foundApcBean.getSiglaProvincia()); // SIGLA_PROVINCIA
			apcPs.setString(paramsSet++, foundApcBean.getNomeRegione()); // NOME_REGIONE
			apcPs.setString(paramsSet++, foundApcBean.getDescrizione()); // DESCRIZIONE
			apcPs.setString(paramsSet++, userId); // ID_UTENTE
			apcPs.setInt(paramsSet++, Integer.parseInt(foundApcBean.getTipoAutorita())); // TIPO
			apcPs.setInt(paramsSet++, Integer.parseInt(foundApcBean.getId())); // ID

			updatedRows += apcPs.executeUpdate();

			// commit
			if (closeConnection) {
				conn.commit();
			}

			if (updatedRows > 0) {
				// UPDATE CONTACT INFO
				if (apcBean.getContatto() != null) {
					for (ContactInfoBean pnBean : apcBean.getContatto()) {
						ciDao.updateContactInfo(pnBean, apcBean, userId, false);
					}
				}
				// UPDATE ADDRESS
				if (apcBean.getIndirizzo() != null) {
					for (AddressBean adBean : apcBean.getIndirizzo()) {
						adDao.updateAddress(adBean, apcBean, userId, false);
					}
				}

				// RETRIEVING UPDATED ROW
				foundApcBean = getAutoritaProtezioneCivileWithParams(apcBean, false);
			} else {
				throw new EntityNotFoundException("Entity with ID=" + apcBean.getId() + " not found.");
			}

			// commit
			if (closeConnection) {
				conn.commit();
			}

		} catch (Exception e) {
			logger.error("Error while executing '" + sql + "'");
			if (closeConnection) {
				conn.rollback();
			}
			throw e;
		} finally {
			if (closeConnection) {
				conn.setAutoCommit(true);
			}
			DBUtility.closeQuietly(conn);
			DBUtility.closeQuietly(apcPs);
		}

		return foundApcBean;
	}

	/**
	 * 
	 * @param entityId
	 * @param userId
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public Boolean removeAutoritaProtezioneCivile(String entityId, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement apcPs = null;
		Boolean wasDeleted = false;
		Connection conn = null;

		try {

			sql = "update AUTORITA_PROTEZIONE_CIVILE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=?";

			// Retrieving connection
			conn = dbUtil.getNewConnection();
			apcPs = conn.prepareStatement(sql);
			apcPs.setString(paramsSet++, userId); // ID_UTENTE
			apcPs.setString(paramsSet++, "S"); // CANCELLATO
			apcPs.setInt(paramsSet++, Integer.parseInt(entityId)); // ID

			updatedRows += apcPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Entity with ID=" + entityId + " not found.");
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			DBUtility.closeQuietly(apcPs);
			DBUtility.closeQuietly(conn);
		}

		return wasDeleted;
	}
}
