package it.cinqueemmeinfo.dammap.dao.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;
import it.cinqueemmeinfo.dammap.dao.fields.AddressDao;
import it.cinqueemmeinfo.dammap.dao.fields.ContactInfoDao;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;
import it.cinqueemmeinfo.dammap.utility.exceptions.JunctionAlreadyExistsException;

@SuppressWarnings("unused")
public class ConcessionarioDao implements Serializable {

	private static final long serialVersionUID = 9121064923993490273L;
	private static final Logger logger = LoggerFactory.getLogger(ConcessionarioDao.class);

	@Autowired
	private DBUtility dbUtil;
	@Autowired
	private BeanUtility bnUtil;
	@Autowired
	private ContactInfoDao ciDao;
	@Autowired
	private AddressDao adDao;

	public ConcessionarioBean getConcessionarioWithParams(ConcessionarioBean cnBean, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		List<ConcessionarioBean> cnList = getConcessionarioListWithParams(2, "", "", cnBean, true, closeConnection);

		if (cnList.size() > 0) {
			return cnList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}

	public ConcessionarioBean getConcessionarioWithParams(int hasAccess, String userId, ConcessionarioBean cnBean,
			boolean closeConnection) throws SQLException, EntityNotFoundException {

		List<ConcessionarioBean> cnList = getConcessionarioListWithParams(hasAccess, userId, "", cnBean, true,
				closeConnection);

		if (cnList.size() > 0) {
			return cnList.get(0);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with the provided params");
		}
	}

	public List<ConcessionarioBean> getConcessionarioListWithParams(int hasAccess, String userId, String idGestore,
			ConcessionarioBean cnBean, boolean getExtraInfos, boolean closeConnection) throws SQLException {

		int paramsSet = 1;
		String sql = "";
		String whereSql = "";
		ConcessionarioBean foundCnBean = null;
		ArrayList<ContactInfoBean> pnList = null;
		ArrayList<AddressBean> adList = null;
		ArrayList<ConcessionarioBean> cnList = new ArrayList<ConcessionarioBean>();

		ResultSet cnRs = null;
		PreparedStatement cnPs = null;
		Connection conn = null;

		try {

			if (!"".equals(idGestore) && !"".equals(cnBean.getId()) && cnBean.getId() != null) {
				sql = "select C.ID, C.NOME, C.NUM_ISCRIZIONE_RID, C.DATA_MODIFICA, C.ID_UTENTE, C.CANCELLATO "
						+ "from CONCESSIONARIO C " + "left join CONCESS_GESTORE CG on CG.ID_CONCESS = C.ID "
						+ this.addAssociatedOnlyJoinQuery(hasAccess)
						+ "WHERE C.CANCELLATO = 'N' AND CG.ID_CONCESS = ? AND CG.ID_GESTORE = ? AND CG.CANCELLATO = 'N'"
						+ this.addAssociatedOnlyWhereQuery(hasAccess, userId) + " ORDER BY C.ID ASC ";
			} else if (!"".equals(idGestore)) {
				sql = "select C.ID, C.NOME, C.NUM_ISCRIZIONE_RID, C.DATA_MODIFICA, C.ID_UTENTE, C.CANCELLATO "
						+ "from CONCESSIONARIO C " + "left join CONCESS_GESTORE CG on CG.ID_CONCESS = C.ID "
						+ this.addAssociatedOnlyJoinQuery(hasAccess)
						+ "WHERE C.CANCELLATO = 'N' AND CG.ID_GESTORE = ? AND CG.CANCELLATO = 'N'"
						+ this.addAssociatedOnlyWhereQuery(hasAccess, userId) + " ORDER BY C.ID ASC ";
			} else if (!"".equals(cnBean.getId()) && cnBean.getId() != null) {
				sql = "select C.ID, C.NOME, C.NUM_ISCRIZIONE_RID, C.DATA_MODIFICA, C.ID_UTENTE, C.CANCELLATO "
						+ "from CONCESSIONARIO C " + this.addAssociatedOnlyJoinQuery(hasAccess)
						+ "WHERE C.CANCELLATO = 'N' AND C.ID = ?" + this.addAssociatedOnlyWhereQuery(hasAccess, userId)
						+ " ORDER BY C.ID ASC ";
			} else {
				sql = "select C.ID, C.NOME, C.NUM_ISCRIZIONE_RID, C.DATA_MODIFICA, C.ID_UTENTE, C.CANCELLATO "
						+ "from CONCESSIONARIO C " + this.addAssociatedOnlyJoinQuery(hasAccess)
						+ "WHERE C.CANCELLATO = 'N'" + this.addAssociatedOnlyWhereQuery(hasAccess, userId)
						+ " ORDER BY C.ID ASC ";
			}

			logger.warn(sql);
			// WHERE PARAM CHECK
			/*
			 * whereSql += "C.CANCELLATO = 'N' ";
			 * 
			 * if (!"".equals(cnBean.getId()) && cnBean.getId() != null) {
			 * whereSql += "AND C.ID = ? "; } if (cnBean.getNumIscrizioneRid()
			 * != null) { whereSql += "AND C.NUM_ISCRIZIONE_RID = ? "; } if
			 * (!"".equals(idGestore) && idGestore != null) { whereSql +=
			 * "AND CG.ID_GESTORE = ? AND CG.CANCELLATO = 'N' "; } //WHERE
			 * STRING BUILDER if (whereSql.length() > 0) { if
			 * (whereSql.startsWith("AND")) { whereSql = whereSql.substring(4,
			 * whereSql.length()); }
			 * 
			 * sql += " WHERE " + whereSql; }
			 * 
			 * //ORDERBY sql += "ORDER BY ID ASC ";
			 */

			// preparing statement
			conn = dbUtil.getNewConnection();
			cnPs = conn.prepareStatement(sql);

			// Setting query parameters

			/*
			 * if (cnBean.getNumIscrizioneRid() != null) {
			 * cnPs.setInt(paramsSet++, cnBean.getNumIscrizioneRid()); }
			 */

			if ((!"".equals(idGestore) && idGestore != null)
					&& (!"".equals(cnBean.getId()) && cnBean.getId() != null)) {
				cnPs.setString(paramsSet++, idGestore);
				cnPs.setInt(paramsSet++, Integer.parseInt(cnBean.getId()));

			} else if (!"".equals(idGestore) && idGestore != null) {
				cnPs.setString(paramsSet++, idGestore);

			} else if (!"".equals(cnBean.getId()) && cnBean.getId() != null) {
				cnPs.setInt(paramsSet++, Integer.parseInt(cnBean.getId()));
			}

			if (hasAccess == 1 && !userId.equals("") && userId != null) {
				cnPs.setString(paramsSet++, userId);
			}
			// execute
			cnRs = cnPs.executeQuery();

			while (cnRs.next()) {

				foundCnBean = new ConcessionarioBean(cnRs.getString("ID"), cnRs.getString("NOME"), null, null,
						cnRs.getInt("NUM_ISCRIZIONE_RID"), cnRs.getString("DATA_MODIFICA"), cnRs.getString("ID_UTENTE"),
						cnRs.getString("CANCELLATO"));

				if (getExtraInfos) {
					// retrieve phone numbers
					pnList = (ArrayList<ContactInfoBean>) ciDao.getContactInfoListWithParams(foundCnBean, false);
					// retrieve address list
					adList = (ArrayList<AddressBean>) adDao.getAddressListWithParams(foundCnBean, false);

					foundCnBean.setIndirizzo(adList);
					foundCnBean.setContatto(pnList);
				}

				// ADD NEW ITEM
				cnList.add(foundCnBean);
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, cnRs, cnPs);
		}

		return cnList;
	}

	private String addAssociatedOnlyWhereQuery(int hasAccess, String userId) {
		// Adds a join on utente_concess if the user has access only to the
		// associated values
		if (hasAccess == 1 && !userId.equals("") && userId != null) {
			return " AND UC.ID_UTENTE IN (SELECT UP.ID FROM UTENTE UP WHERE UP.USERID= ?) ";
		}
		return " ";
	}

	private String addAssociatedOnlyJoinQuery(int hasAccess) {
		// Adds a join on utente_concess if the user has access only to the
		// associated values
		if (hasAccess == 1) {
			return " LEFT JOIN UTENTE_CONCESS UC ON C.ID=UC.ID_CONCESS AND UC.CANCELLATO='N' ";
		}
		return " ";
	}

	public List<ConcessionarioBean> getConcessionarioListFromIdGestore(String idGestore, boolean closeConnection)
			throws SQLException {

		List<ConcessionarioBean> cnList = getConcessionarioListWithParams(2, "", idGestore, new ConcessionarioBean(),
				false, closeConnection);

		return cnList;
	}

	/**
	 * 
	 * @param cnBean
	 * @param userId
	 * @param closeConnection
	 *            if true closes connection and commmits changes, if false
	 *            transaction will be kept open for futher operations
	 * @return
	 * @throws SQLException
	 * @throws EntityNotFoundException
	 */
	public ConcessionarioBean createConcessionario(ConcessionarioBean cnBean, String userId, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		ResultSet cnNextValRs = null;
		PreparedStatement cnPs = null;
		ConcessionarioBean createdCnBean = null;
		Connection conn = null;

		try {

			// Starting transaction

			// Get entity id
			sql = "select CONCESSIONARIO_SEQUENZA.nextval from dual";
			conn = dbUtil.getNewConnection();
			conn.setAutoCommit(false);
			cnNextValRs = conn.createStatement().executeQuery(sql);

			if (cnNextValRs != null && cnNextValRs.next()) {
				cnBean.setId(cnNextValRs.getString(1));
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}

			// INSERT CONCESSIONARIO
			sql = "insert into CONCESSIONARIO (ID, NOME, NUM_ISCRIZIONE_RID, ID_UTENTE ) values ( ?,?,?,? )";

			cnPs = conn.prepareStatement(sql);

			cnPs.setString(paramsSet++, cnBean.getId()); // ID
			cnPs.setString(paramsSet++, cnBean.getNome()); // NOME
			cnPs.setInt(paramsSet++, cnBean.getNumIscrizioneRid()); // NUM_ISCRIZIONE_RID
			cnPs.setString(paramsSet++, userId); // ID_UTENTE

			updatedRows += cnPs.executeUpdate();

			if (closeConnection) {
				conn.commit();
			}

			if (updatedRows > 0) {

				// INSERT CONTACT INFO
				for (ContactInfoBean pnBean : cnBean.getContatto()) {
					ciDao.createContactInfo(pnBean, cnBean, userId, false);
				}

				// INSERT ADDRESS
				for (AddressBean adBean : cnBean.getIndirizzo()) {
					adDao.createAddress(adBean, cnBean, userId, false);
				}

				// RETRIEVING CREATED ROW
				createdCnBean = getConcessionarioWithParams(cnBean, false);
			} else {
				throw new SQLException("Insert not executed, error while creating record");
			}

			if (closeConnection) {
				conn.commit();
			}

		} catch (SQLException sqle) {
			if (closeConnection) {
				conn.rollback();
			}
			logger.error("SQLException with message: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (cnNextValRs != null) {
				cnNextValRs.close();
			}
			if (cnPs != null) {
				cnPs.close();
			}

			if (closeConnection) {
				conn.setAutoCommit(true);
				conn.close();
			} else {
				conn.close();
			}
		}

		return createdCnBean;
	}

	/**
	 * 
	 * @param cnBean
	 * @param userId
	 * @param closeConnection
	 *            if true closes connection and commmits changes, if false
	 *            transaction will be kept open for futher operations
	 * @return
	 * @throws Exception
	 */
	public ConcessionarioBean updateConcessionario(ConcessionarioBean cnBean, String userId, boolean closeConnection)
			throws Exception {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement cnPs = null;
		ConcessionarioBean foundCnBean = null;
		Connection conn = null;
		try {

			foundCnBean = (ConcessionarioBean) bnUtil.compareAndFillBean(getConcessionarioWithParams(cnBean, true),
					cnBean);

			sql = "update CONCESSIONARIO set NOME=?, NUM_ISCRIZIONE_RID=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=?";

			// Retrieving connection
			conn = dbUtil.getNewConnection();
			conn.setAutoCommit(false);
			cnPs = conn.prepareStatement(sql);

			cnPs.setString(paramsSet++, foundCnBean.getNome()); // SIGLA_PROVINCIA
			cnPs.setInt(paramsSet++, foundCnBean.getNumIscrizioneRid()); // NOME_REGIONE
			cnPs.setString(paramsSet++, userId); // ID_UTENTE
			cnPs.setInt(paramsSet++, Integer.parseInt(foundCnBean.getId())); // ID

			updatedRows += cnPs.executeUpdate();

			if (closeConnection) {
				conn.commit();
			}

			if (updatedRows > 0) {
				// UPDATE CONTACT INFO
				if (cnBean.getContatto() != null) {
					for (ContactInfoBean pnBean : cnBean.getContatto()) {
						ciDao.updateContactInfo(pnBean, cnBean, userId, false);
					}
				}
				// UPDATE ADDRESS
				if (cnBean.getIndirizzo() != null) {
					for (AddressBean adBean : cnBean.getIndirizzo()) {
						adDao.updateAddress(adBean, cnBean, userId, false);
					}
				}

				// RETRIEVE UPDATED ROW
				foundCnBean = getConcessionarioWithParams(cnBean, true);
			} else {
				throw new EntityNotFoundException("Entity with ID=" + cnBean.getId() + " not found.");
			}

			// COMMIT
			if (closeConnection) {
				conn.commit();
			}

		} catch (Exception e) {
			logger.error("Error while executing '" + sql + "'");
			if (closeConnection) {
				conn.rollback();
			}
			throw e;
		} finally {
			if (cnPs != null) {
				cnPs.close();
			}

			if (closeConnection) {
				conn.setAutoCommit(true);
				conn.close();
			} else {
				conn.close();
			}
		}

		return foundCnBean;
	}

	/**
	 * 
	 * @param entityId
	 * @param userId
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public Boolean removeConcessionario(String entityId, String userId, boolean closeConnection)
			throws SQLException, EntityNotFoundException {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement cnPs = null;
		Boolean wasDeleted = false;
		Connection conn = null;

		try {

			sql = "update CONCESSIONARIO set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=?";

			// Retrieving connection
			conn = dbUtil.getNewConnection();
			cnPs = conn.prepareStatement(sql);
			cnPs.setString(paramsSet++, userId); // ID_UTENTE
			cnPs.setString(paramsSet++, "S"); // CANCELLATO
			cnPs.setInt(paramsSet++, Integer.parseInt(entityId)); // ID

			updatedRows += cnPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Entity with ID=" + entityId + " not found.");
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (cnPs != null) {
				cnPs.close();
			}

			if (closeConnection) {
				conn.close();
			} else {
				conn.close();
			}
		}

		return wasDeleted;
	}

	public Boolean createConcessionarioGestoreJunction(int concessId, int gestoreId, String userId,
			boolean closeConnection) throws SQLException, JunctionAlreadyExistsException {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		ResultSet cnNextValRs = null;
		PreparedStatement cnPs = null;
		Boolean wasCreated = false;
		int junctionId = 0;
		Connection conn = null;

		try {

			// check if junction is already created
			if (getConcessionarioListWithParams(2, "", "" + gestoreId, new ConcessionarioBean("" + concessId), false,
					true).size() == 0) {

				// Starting transaction

				// Get junction id
				sql = "select CONCESS_GESTORE_SEQUENZA.nextval from dual";
				conn = dbUtil.getNewConnection();
				conn.setAutoCommit(false);
				cnNextValRs = conn.createStatement().executeQuery(sql);

				if (cnNextValRs != null && cnNextValRs.next()) {
					junctionId = cnNextValRs.getInt(1);
				} else {
					throw new SQLException("Couldn't retrieve next sequence value.");
				}

				// INSERT JUNCTION CONCESSIONARIO GESTORE
				sql = "insert into CONCESS_GESTORE (ID, ID_CONCESS, ID_GESTORE, ID_UTENTE ) values ( ?,?,?,? )";

				cnPs = conn.prepareStatement(sql);

				cnPs.setInt(paramsSet++, junctionId); // ID
				cnPs.setInt(paramsSet++, concessId); // ID_CONCESS
				cnPs.setInt(paramsSet++, gestoreId); // ID_GESTORE
				cnPs.setString(paramsSet++, userId); // ID_UTENTE

				updatedRows += cnPs.executeUpdate();

				if (updatedRows > 0) {
					// commiting changes
					if (closeConnection) {
						conn.commit();
					}

					wasCreated = true;
				} else {
					if (closeConnection) {
						conn.rollback();
					}

					throw new SQLException("Error while creating Concessionario to Gestore Junction.");
				}

			} else {
				throw new JunctionAlreadyExistsException("Error, Concessionario to Gestore Junction already exists.");
			}

		} catch (SQLException sqle) {
			if (closeConnection) {
				conn.rollback();
			}

			if ("".equals(sql))
				logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			conn.setAutoCommit(true);
			DBUtility.closeQuietly(conn, cnNextValRs, cnPs);
		}

		return wasCreated;
	}

	public Boolean removeConcessionarioGestoreJunction(int concessId, int gestoreId, String userId,
			boolean closeConnection) throws SQLException, EntityNotFoundException {

		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement cnPs = null;
		Boolean wasDeleted = false;
		Connection conn = null;

		try {
			// Starting transaction

			sql = "UPDATE DIGA_GESTORE SET CANCELLATO='S', DATA_MODIFICA=SYSDATE, ID_UTENTE=? " + // update
																									// values
					" WHERE (NUMERO_ARCHIVIO, SUB) IN " + "(SELECT DC.NUMERO_ARCHIVIO, DC.SUB "
					+ "FROM CONCESS_GESTORE CG " + "LEFT JOIN DIGA_CONCESS DC ON DC.ID_CONCESS = CG.ID_CONCESS "
					+ "WHERE CG.ID_GESTORE=? AND CG.ID_CONCESS=? " + "AND CG.CANCELLATO<>'S' AND DC.CANCELLATO<>'S' " + // 1st
																														// query
																														// where
																														// params
					"MINUS " + "SELECT DC.NUMERO_ARCHIVIO, DC.SUB " + "FROM CONCESS_GESTORE CG "
					+ "LEFT JOIN DIGA_CONCESS DC ON DC.ID_CONCESS = CG.ID_CONCESS "
					+ "WHERE CG.ID_GESTORE=? AND CG.ID_CONCESS<>? " + "AND CG.CANCELLATO<>'S' AND DC.CANCELLATO<>'S' ) "
					+ // 2nd query where params
					"AND ID_GESTORE=? AND CANCELLATO<>'S' "; // update where
																// params

			// Retrieving connection
			conn = dbUtil.getNewConnection();
			conn.setAutoCommit(false);
			cnPs = conn.prepareStatement(sql);

			paramsSet = 1;
			cnPs.setString(paramsSet++, userId); // ID_UTENTE update value
			cnPs.setInt(paramsSet++, gestoreId); // ID_GESTORE 1st query param
			cnPs.setInt(paramsSet++, concessId); // ID_CONCESS 1st query param
			cnPs.setInt(paramsSet++, gestoreId); // ID_GESTORE 2nd query param
			cnPs.setInt(paramsSet++, concessId); // ID_CONCESS 2nd query param
			cnPs.setInt(paramsSet++, gestoreId); // ID_GESTORE where param

			updatedRows += cnPs.executeUpdate();

			sql = "UPDATE CONCESS_GESTORE SET DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? WHERE ID_CONCESS=? AND ID_GESTORE=? AND CANCELLATO<>'S' ";

			// Retrieving connection
			cnPs = conn.prepareStatement(sql);

			paramsSet = 1;
			cnPs.setString(paramsSet++, userId); // ID_UTENTE
			cnPs.setString(paramsSet++, "S"); // CANCELLATO
			cnPs.setInt(paramsSet++, concessId); // ID_CONCESS
			cnPs.setInt(paramsSet++, gestoreId); // ID_GESTORE

			updatedRows += cnPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
			} else {
				throw new EntityNotFoundException("Junction between Concessionario with ID=" + concessId
						+ " and Gestore with ID=" + gestoreId + " not found.");
			}

			if (updatedRows > 0) {
				// commiting changes
				if (closeConnection) {
					conn.commit();
				}
			} else {
				if (closeConnection) {
					conn.rollback();
				}

				throw new EntityNotFoundException("Junction between Concessionario with ID=" + concessId
						+ " and Gestore with ID=" + gestoreId + " not found.");
			}

		} catch (SQLException sqle) {

			if (closeConnection) {
				conn.rollback();
			}

			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			conn.setAutoCommit(true);
			DBUtility.closeQuietly(conn);
			DBUtility.closeQuietly(cnPs);
		}

		return wasDeleted;
	}

}
