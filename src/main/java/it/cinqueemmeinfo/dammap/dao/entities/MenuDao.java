package it.cinqueemmeinfo.dammap.dao.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.MenuBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;

public class MenuDao implements Serializable {

	@Autowired private DBUtility dbUtil;
	//@Autowired private BeanUtility bnUtil;
	private static final long serialVersionUID = 906472673246974813L;
	private static final Logger logger = LoggerFactory.getLogger(MenuDao.class);
	
	
	/**
	 * 
	 * @param targetEntity
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public List<HashMap<String, String>> retrieveSingleDam(long userId, long numeroArchivio, String sub, boolean closeConnection) throws SQLException {
		
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		List<HashMap<String, String>> allElements;
		
		try {
			//SELECT
			sql =" SELECT V_ELENCO_DIGHE_SINGOLI.* FROM "+
				" V_ELENCO_DIGHE_SINGOLI "+
				" INNER JOIN  "+
				" (SELECT DISTINCT NUMERO_ARCHIVIO,SUB FROM ACCESSO_DEFAULT "+
				" WHERE ID_GRUPPO IN "+
				" (SELECT ID_GRUPPO FROM UTENTE_GRUPPO WHERE ID_UTENTE = ? ) ) AD "+
				" ON V_ELENCO_DIGHE_SINGOLI.NUMERO_ARCHIVIO=AD.NUMERO_ARCHIVIO AND V_ELENCO_DIGHE_SINGOLI.SUB=AD.SUB "+
				" WHERE V_ELENCO_DIGHE_SINGOLI.NUMERO_ARCHIVIO=? AND V_ELENCO_DIGHE_SINGOLI.SUB=? "
				;
		
			//SET QUERY PARAM
			conn=dbUtil.getNewConnection();
			ps=conn.prepareStatement(sql);
			int i=1;
			ps.setLong(i++, userId);
			ps.setLong(i++, numeroArchivio);
			ps.setString(i++, sub);
			//EXECUTE
			rs=ps.executeQuery();
			allElements=this.fillDamDataListForApplet(rs);
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			dbUtil.closeQuietly(conn, rs, ps);
		}
		
		return allElements;
	}
	
	private HashMap<String, String> fillDamDataForApplet(ResultSet rset) throws SQLException{
		HashMap<String, String> damDetails=new HashMap<String, String>();
		int i=1;
		String parametri="ParametriApplet";
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getInt("NUMERO_ARCHIVIO")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("SUB")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("COMPETENZA_SND")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("NOME_DIGA")));
		String status=rset.getString("STATUS_PRINCIPALE")+" - "+rset.getString("STATUS_SECONDARIO")+" ";
		status+=rset.getString("DESCR_STATUS_PRINCIPALE")+" - "+rset.getString("DESCR_STATUS_SECONDARIO");
		damDetails.put(parametri+(i++), this.emptyIfNull(""+status));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("NUM_ISCRIZIONE_RID")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("UFFICIO_SEDE_CENTRALE")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("UFFICIO_PERIFERICO")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("FUNZIONARIO_SEDE_CENTRALE")));
		damDetails.put(parametri+(i++), this.emptyIfNull(""+rset.getString("FUNZIONARIO_UFF_PERIFERICO")));
		return damDetails;
	}
	
	private String emptyIfNull(String str){
		if(str==null || str.equals("null") || str.trim().equals("")){
			return "";
		}
		return str;
	}
	
	private List<HashMap<String, String>> fillDamDataListForApplet(ResultSet rs) throws SQLException{
		List<HashMap<String, String>> values= new ArrayList<HashMap<String, String>>();
		while (rs.next()) {
			values.add(this.fillDamDataForApplet(rs));
		}
		return values;
	}
	
	/**
	 * 
	 * @param targetEntity
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public List<MenuBean> retrieveMenuForUser(Long userId, boolean closeConnection) throws SQLException {
		
		String sql = "";
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection conn=null;
		List<MenuBean> menu = new ArrayList<MenuBean>();
		
		try {
			//SELECT
				sql = "SELECT b.ID_SCHEDA AS idScheda, b.ID_GRUPPO AS idGruppo, a.ID_PADRE AS idPadre, a.DESCR AS descr, "
						+ "a.DESCR_EXT AS descrExt, a.TIPO AS tipo, d.ORDINE AS ordine, b.VISIBILE AS visibile, "
						+ "b.TIPO AS tipoPermesso, c.IS_SCHEDA AS isScheda, c.HREF AS href, c.TYPE as type "
						+ "FROM SCHEDA_PRIVILEGI_DEFAULT b LEFT JOIN SCHEDA a ON b.ID_SCHEDA=a.ID "
						+ "LEFT JOIN SCHEDA_EXTRA c ON a.ID=c.ID_SCHEDA "
						+ "LEFT JOIN SCHEDA_PRIVILEGI_EXTRA d ON b.ID_SCHEDA=d.ID_SCHEDA AND b.ID_GRUPPO=d.ID_GRUPPO "
						+ "WHERE b.ID_GRUPPO IN "
						+ "(SELECT ID_GRUPPO FROM UTENTE_GRUPPO WHERE ID_UTENTE=? ) "
						+ "AND c.IS_SCHEDA=? AND b.VISIBILE=? ORDER BY b.ID_SCHEDA, a.ID_PADRE, d.ORDINE "; 
		
			//SET QUERY PARAM
			conn=dbUtil.getNewConnection();
			ps=conn.prepareStatement(sql);
			int i=1;
			ps.setLong(i++, userId);
			ps.setString(i++, "S");
			ps.setString(i++, "S");
			//EXECUTE
			rs=ps.executeQuery();
			List<MenuBean> allElements=this.fillMenuBeanList(rs);
			List<MenuBean> removeElements=new ArrayList<MenuBean>();
			//Rimuove i duplicati
			Map<Long, MenuBean> keepElements=new HashMap<Long, MenuBean>();
			for(MenuBean element : allElements){
				MenuBean tempElem=keepElements.get(element.getIdScheda());
				//Se l'elemento non è nella mappa allora lo inserisce
				if(tempElem==null){
					keepElements.put(element.getIdScheda(), element);
				} else {
					//Altrimenti controlla il tipo W > R > N
					if(tempElem.getTipoPermesso().equals("N")){
						//Se nella mappa c'è N allora element va aggiunto
						removeElements.add(tempElem);
						keepElements.put(element.getIdScheda(), element);
					} else if(tempElem.getTipoPermesso().equals("R") && element.getTipoPermesso().equals("W")){
						//Se nella mappa c'è R ed il nuovo permesso è W allora va sostituito
						removeElements.add(tempElem);
						keepElements.put(element.getIdScheda(), element);
					} else {
						//In tutti gli altri casi (tempElem==W oppure tempElem==R && element==N), element viene cancellato
						removeElements.add(element);
					}
					
				}
			}
			//Rimuove tutti i duplicati
			allElements.removeAll(removeElements);
			//Crea il menu ed i sottomenu
			for (MenuBean menuBean : allElements) {
				//Cerca i figli della voce di menu
				for (MenuBean tempMenuBean : allElements) {
					//Se è un figlio viene aggiunto alla lista
					if(menuBean.getIdScheda().equals(tempMenuBean.getIdPadre())){
						menuBean.addFiglio(tempMenuBean);
					}
				}
				//Se idPadre è 0 allora è una voce di menu principale
				if(menuBean.getIdPadre().equals(new Long(0))){
					menu.add(menuBean);
				}
			}

			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			dbUtil.closeQuietly(conn, rs, ps);
		}
		
		return menu;
	}
	
	private List<MenuBean> fillMenuBeanList(ResultSet rs) throws SQLException{
		List<MenuBean> values= new ArrayList<MenuBean>();
		while (rs.next()) {
			MenuBean menuBean=this.fillMenuBean(rs);
			values.add(menuBean);
		}
		return values;
	}
	
	private MenuBean fillMenuBean (ResultSet rs) throws SQLException{
		MenuBean menuBean=new MenuBean();
		menuBean.setIdScheda(rs.getLong("idScheda"));
		menuBean.setDescr(rs.getString("descr"));
		menuBean.setDescrExt(rs.getString("descrExt"));
		menuBean.setIdGruppo(rs.getLong("idGruppo"));
		menuBean.setIdPadre(rs.getLong("idPadre"));
		menuBean.setIdScheda(rs.getLong("idScheda"));
		menuBean.setOrdine(rs.getLong("ordine"));
		menuBean.setTipo(rs.getLong("tipo"));
		menuBean.setTipoPermesso(rs.getString("tipoPermesso"));
		menuBean.setVisibile(rs.getString("visibile"));
		menuBean.setIsScheda(rs.getString("isScheda"));
		menuBean.setHref(rs.getString("href"));
		menuBean.setType(rs.getLong("type"));
		return menuBean;
	}
}
