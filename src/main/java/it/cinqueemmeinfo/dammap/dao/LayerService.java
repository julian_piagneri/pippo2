package it.cinqueemmeinfo.dammap.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletContext;

public class LayerService {

	public String getAllLayer(ServletContext context) {

    	//String layerList = "";
    	String line = null;
    	StringBuilder stringBuilder = new StringBuilder();
    	
    	//ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("root-context.xml");
    	//Resource layerListFile = ctx.getResource("layerList.json");
    	
    	//InputStream inputStream = LayerService.class.getClassLoader().getResourceAsStream("url:http://localhost:8080/dammap/resources/layerList.json");

    	try {
    		InputStream inputStream = context.getResourceAsStream("WEB-INF/layerList.json");
    		
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			
			while((line = reader.readLine()) != null ) {
		        stringBuilder.append(line);
		        stringBuilder.append("\n");
		    }
			
			reader.close();
			inputStream.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			//ctx.close();
		}
		
		return stringBuilder.toString();
	}
}
