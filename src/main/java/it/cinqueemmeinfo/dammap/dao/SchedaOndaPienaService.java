package it.cinqueemmeinfo.dammap.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.OndaPienaBean;
import it.cinqueemmeinfo.dammap.bean.entities.TipoOndePienaBean;
import it.cinqueemmeinfo.dammap.dao.entities.OndaPienaDao;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
// TODO: Non esendoci una scheda ancora una scheda associata a questa service
// non � ancora possibile garantire i permessi di scrittura e/o di lettura
public class SchedaOndaPienaService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(SchedaOndaPienaService.class);

	@Autowired
	private OndaPienaDao opDao;
	@Autowired
	private UserUtility usrUtil;

	public List<OndaPienaBean> getAllOndePienaByDamID(String damID) throws SQLException {
		usrUtil.checkCanRead(DBUtility.ONDA_DI_PIENA);
		
		return opDao.getAllOndePienaByDamID(damID);
	}

	public OndaPienaBean getOndePienaInfoByDamID(String damID) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.ONDA_DI_PIENA);
		
		return opDao.getOndePienaInfoByDamID(damID);
	}

	public void updateOndaPienaInfo(OndaPienaBean bean) throws SQLException {
		usrUtil.checkCanWrite(DBUtility.ONDA_DI_PIENA);
		opDao.updateOndaPienaInfo(bean, usrUtil.getUserId());
	}

	public void updateAllOndePienaByDamID(List<OndaPienaBean> beanList, String damID) throws SQLException, ParseException, EntityNotFoundException {
		usrUtil.checkCanWrite(DBUtility.ONDA_DI_PIENA);
		opDao.updateAllOndePienaByDamID(beanList, damID, usrUtil.getUserId());
	}

	public List<TipoOndePienaBean> getAllTipoOndePiena() throws EntityNotFoundException, SQLException {
		usrUtil.checkCanRead(DBUtility.ONDA_DI_PIENA);
		
		return opDao.getAllTipoOndePiena();
	}
}
