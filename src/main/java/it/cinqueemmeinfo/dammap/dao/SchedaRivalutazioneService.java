
package it.cinqueemmeinfo.dammap.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.DateRivalutazioniIdrologicheBean;
import it.cinqueemmeinfo.dammap.bean.entities.DateTipoOrigineBean;
import it.cinqueemmeinfo.dammap.bean.entities.FonteBean;
import it.cinqueemmeinfo.dammap.bean.entities.FontePortateBean;
import it.cinqueemmeinfo.dammap.bean.entities.FontePortatePortateBean;
import it.cinqueemmeinfo.dammap.bean.entities.NotePortateBean;
import it.cinqueemmeinfo.dammap.bean.entities.PortateRivalutateBean;
import it.cinqueemmeinfo.dammap.bean.entities.SchedaInvasoBean;
import it.cinqueemmeinfo.dammap.dao.entities.DateRivalutazioniDao;
import it.cinqueemmeinfo.dammap.dao.entities.PortateRivalutateDao;
import it.cinqueemmeinfo.dammap.dao.entities.SchedaRivalutazioneDao;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class SchedaRivalutazioneService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(SchedaRivalutazioneService.class);

	@Autowired
	private SchedaRivalutazioneDao srDao;
	@Autowired
	private DateRivalutazioniDao drDao;
	@Autowired
	private PortateRivalutateDao prDao;
	@Autowired
	private UserUtility usrUtil;

	public SchedaInvasoBean getSchedaInvaso(String damId) throws SQLException {
		usrUtil.checkCanRead(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		
		return srDao.getInvasoByDamId(damId);
	}

	public DateRivalutazioniIdrologicheBean getDateRivalutazioni(String damId) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		
		return drDao.getRivalutazioneByDamId(damId);
	}

	public List<PortateRivalutateBean> getPortateRivalutateDao(String damId) throws SQLException {
		usrUtil.checkCanRead(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);

		return prDao.getPortateRivalutateByDamId(damId);
	}

	public List<DateTipoOrigineBean> getAllTipoOrigineDao() throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		
		return drDao.findAllTipoOrigne();
	}

	public List<FonteBean> getAllFonteDao() throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		
		return prDao.getAllFonte();
	}

	public NotePortateBean getNote(String damId) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		
		return prDao.getNoteByDamID(damId);
	}

	public FontePortateBean getFonte(String damId) throws SQLException {
		usrUtil.checkCanRead(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		
		return prDao.getFonteByDamID(damId);
	}

	public void updateSchedaInvaso(SchedaInvasoBean toUpdate, String damID) throws SQLException {
		usrUtil.checkCanWrite(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		srDao.updateSchedaInvaso(toUpdate, damID, usrUtil.getUserId());
	}

	public void updateDateRivalutazioni(DateRivalutazioniIdrologicheBean bean, String damID) throws SQLException, ParseException {
		usrUtil.checkCanWrite(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		drDao.updateDateRivalutazioni(bean, usrUtil.getUserId(), damID);
	}

	public void updateNote(NotePortateBean bean, String damId) throws SQLException {
		usrUtil.checkCanWrite(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		prDao.updateNote(bean, damId, usrUtil.getUserId());
	}

	public void updateFonte(FontePortateBean bean, String damID) throws SQLException {
		usrUtil.checkCanWrite(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		prDao.updateFonte(bean, damID, usrUtil.getUserId());
	}

	public void updatePortateRivalutate(List<PortateRivalutateBean> beanList, String damID) throws SQLException {
		usrUtil.checkCanWrite(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		prDao.updatePortateRivalutate(beanList, damID, usrUtil.getUserId());
	}

	public void updateFontePortate(FontePortatePortateBean bean, String damID) throws SQLException {
		usrUtil.checkCanWrite(DBUtility.RIVALUTAZIONE_IDROLOGICO_IDRAULICHE);
		prDao.updatePortateRivalutate(bean.getPortate(), damID, usrUtil.getUserId());
		prDao.updateFonte(bean.getFonte(), damID, usrUtil.getUserId());
	}
}