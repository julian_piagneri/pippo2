package it.cinqueemmeinfo.dammap.dao.fields;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.fields.EmailBean;
import it.cinqueemmeinfo.dammap.utility.DBUtility;

@Deprecated
public class EmailDao implements Serializable {

	@Autowired
	private DBUtility dbUtil;
	private static final long serialVersionUID = 88610395582638876L;
	private static final Logger logger = LoggerFactory.getLogger(EmailDao.class);

	public EmailBean getEmailWithParams(EmailBean emBean, boolean closeConnection) throws SQLException {

		// List<EmailBean>

		return emBean;
	}

	public List<EmailBean> getEmailListWithParams(BasicEntityBean targetEntity, boolean closeConnection)
			throws SQLException {

		String sql = "";
		String entityType = "";
		@SuppressWarnings("unused")
		int paramsSet = 1;

		ResultSet emRs = null;
		PreparedStatement emPs = null;
		ArrayList<EmailBean> emList = null;

		try {

			// CHECK ENTITY
			entityType = dbUtil.getEntityType(targetEntity);

			// SELECT
			sql = "select E.ID, E.ID_" + entityType
					+ ", E.\"ORDER\", E.EMAIL, E.DATA_MODIFICA, E.ID_UTENTE, E.CANCELLATO " + "from EMAIL_" + entityType
					+ " E " + "where E.ID_" + entityType + " = ? AND E.CANCELLATO = 'N' " + "order by E.\"ORDER\" ASC ";

			// SET QUERY PARAM
			emPs = dbUtil.getConnection().prepareStatement(sql);
			emPs.setInt(1, Integer.parseInt(targetEntity.getId())); // may be
																	// necessary
																	// a case if
																	// targetEntity
																	// is a Dam
			// EXECUTE
			emRs = emPs.executeQuery();
			// INIT LIST
			emList = new ArrayList<EmailBean>();

			while (emRs.next()) {
				// CREATE AND ADD NEW ITEM
				emList.add(new EmailBean(emRs.getString("ID"), emRs.getString("ID_" + entityType), emRs.getInt("ORDER"),
						emRs.getString("EMAIL"), emRs.getString("DATA_MODIFICA"), emRs.getString("ID_UTENTE"),
						emRs.getString("CANCELLATO")));
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (emPs != null) {
				emPs.close();
			}

			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}

		return emList;
	}
}
