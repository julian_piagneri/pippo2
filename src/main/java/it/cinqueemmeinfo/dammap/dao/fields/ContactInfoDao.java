package it.cinqueemmeinfo.dammap.dao.fields;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.entities.IngegnereBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@SuppressWarnings("unused")
public class ContactInfoDao implements Serializable {

	@Autowired private DBUtility dbUtil;
	@Autowired private BeanUtility bnUtil;
	private static final long serialVersionUID = 2384135769322146422L;
	private static final Logger logger = LoggerFactory.getLogger(ContactInfoDao.class);

	private ContactInfoBean getContactIntoForEntityWithOrderNumForType(BasicEntityBean targetEntity, int contactType, boolean getMax, boolean closeConnection) throws SQLException {
		
		String sql = "";
		int paramsSet = 1;
		ResultSet cnRs = null;
		PreparedStatement cnPs = null;
		ContactInfoBean foundCnBean = null;
		
		try {			
			//SELECT
			if (getMax) {
				if (targetEntity instanceof AutoritaProtezioneCivileBean) {
					sql = "select ID, ID_AUTORITA, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_AUTORITA where ID_AUTORITA=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MAX(ORDINE) FROM CONTATTO_AUTORITA WHERE ID_AUTORITA=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof ConcessionarioBean) {
					sql = "select ID, ID_CONCESS, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_CONCESS where ID_CONCESS=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MAX(ORDINE) FROM CONTATTO_CONCESS WHERE ID_CONCESS=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof GestoreBean) {
					sql = "select ID, ID_GESTORE, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_GESTORE where ID_GESTORE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MAX(ORDINE) FROM CONTATTO_GESTORE WHERE ID_GESTORE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof IngegnereBean) {
					sql = "select ID, ID_INGEGNERE, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_INGEGNERE where ID_INGEGNERE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MAX(ORDINE) FROM CONTATTO_INGEGNERE WHERE ID_INGEGNERE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof AltraEnteBean) {
					sql = "select ID, ID_ENTE, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_ALTRA_ENTE where ID_ENTE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MAX(ORDINE) FROM CONTATTO_ALTRA_ENTE WHERE ID_ENTE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				}
			} else {
				if (targetEntity instanceof AutoritaProtezioneCivileBean) {
					sql = "select ID, ID_AUTORITA, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_AUTORITA where ID_AUTORITA=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MIN(ORDINE) FROM CONTATTO_AUTORITA WHERE ID_AUTORITA=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof ConcessionarioBean) {
					sql = "select ID, ID_CONCESS, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_CONCESS where ID_CONCESS=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MIN(ORDINE) FROM CONTATTO_CONCESS WHERE ID_CONCESS=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof GestoreBean) {
					sql = "select ID, ID_GESTORE, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_GESTORE where ID_GESTORE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MIN(ORDINE) FROM CONTATTO_GESTORE WHERE ID_GESTORE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof IngegnereBean) {
					sql = "select ID, ID_INGEGNERE, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_INGEGNERE where ID_INGEGNERE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MIN(ORDINE) FROM CONTATTO_INGEGNERE WHERE ID_INGEGNERE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof AltraEnteBean) {
					sql = "select ID, ID_ENTE, ORDINE, CONTATTO, TIPO_CONTATTO, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
							+ "from CONTATTO_ALTRA_ENTE where ID_ENTE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S' "
							+ "AND ORDINE = (SELECT MIN(ORDINE) FROM CONTATTO_ALTRA_ENTE WHERE ID_ENTE=? AND TIPO_CONTATTO=? AND CANCELLATO<>'S') ";
				}
			}
			
			//SET QUERY PARAM
			cnPs = dbUtil.getConnection().prepareStatement(sql);
			cnPs.setInt(1, Integer.parseInt(targetEntity.getId()));
			cnPs.setInt(2, contactType);
			cnPs.setInt(3, Integer.parseInt(targetEntity.getId()));
			cnPs.setInt(4, contactType);
			
			//EXECUTE
			cnRs = cnPs.executeQuery();
			
			while (cnRs.next()) {
				//CREATE NEW ITEM
				paramsSet = 1;
				
				foundCnBean = new ContactInfoBean(cnRs.getString(paramsSet++)
											, cnRs.getString(paramsSet++)
											, cnRs.getInt(paramsSet++)
											, cnRs.getString(paramsSet++)
											, cnRs.getInt(paramsSet++)
											, cnRs.getString(paramsSet++)
											, cnRs.getString(paramsSet++)
											, cnRs.getString(paramsSet++));
			} 
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (cnPs != null) {
				cnPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return foundCnBean;
	}
	
	/**
	 * 
	 * @param pnBean
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public ContactInfoBean getContactInfoWithParams(String fieldId, BasicEntityBean targetEntity, boolean closeConnection) throws SQLException {

		String sql = "";
		int paramsSet = 1;
		
		ContactInfoBean cnBean = null;
		ResultSet cnRs = null;
		PreparedStatement cnPs = null;
		
		try {			
			//SELECT
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "select T.ID, T.ID_AUTORITA, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_AUTORITA T where T.ID = ? AND T.ID_AUTORITA = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "select T.ID, T.ID_CONCESS, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_CONCESS T where T.ID = ? AND T.ID_CONCESS = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "select T.ID, T.ID_GESTORE, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_GESTORE T where T.ID = ? AND T.ID_GESTORE = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "select T.ID, T.ID_INGEGNERE, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_INGEGNERE T where T.ID = ? AND T.ID_INGEGNERE = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "select T.ID, T.ID_ENTE, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_ALTRA_ENTE T where T.ID = ? AND T.ID_ENTE = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			}
			
			//SET QUERY PARAM
			cnPs = dbUtil.getConnection().prepareStatement(sql);
			cnPs.setInt(1, Integer.parseInt(fieldId));
			cnPs.setInt(2, Integer.parseInt(targetEntity.getId()));
			
			//EXECUTE
			cnRs = cnPs.executeQuery();
			
			while (cnRs.next()) {
				//CREATE NEW ITEM
				paramsSet = 1;
				
				cnBean = new ContactInfoBean(cnRs.getString(paramsSet++)
											, cnRs.getString(paramsSet++)
											, cnRs.getInt(paramsSet++)
											, cnRs.getString(paramsSet++)
											, cnRs.getInt(paramsSet++)
											, cnRs.getString(paramsSet++)
											, cnRs.getString(paramsSet++)
											, cnRs.getString(paramsSet++));
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (cnPs != null) {
				cnPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return cnBean;
	}

	/**
	 * 
	 * @param targetEntity
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public List<ContactInfoBean> getContactInfoListWithParams(BasicEntityBean targetEntity, boolean closeConnection) throws SQLException {
		
		String sql = "";
		int paramsSet = 1;
		
		ResultSet pnRs = null;
		PreparedStatement cnPs = null;
		ArrayList<ContactInfoBean> pnList = null;
		
		try {
			//SELECT
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "select T.ID, T.ID_AUTORITA, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_AUTORITA T where T.ID_AUTORITA = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "select T.ID, T.ID_CONCESS, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_CONCESS T where T.ID_CONCESS = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "select T.ID, T.ID_GESTORE, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_GESTORE T where T.ID_GESTORE = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "select T.ID, T.ID_INGEGNERE, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_INGEGNERE T where T.ID_INGEGNERE = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "select T.ID, T.ID_ENTE, T.ORDINE, T.CONTATTO, T.TIPO_CONTATTO, T.DATA_MODIFICA, T.ID_UTENTE, T.CANCELLATO "
						+ "from CONTATTO_ALTRA_ENTE T where T.ID_ENTE = ? AND T.CANCELLATO<>'S' order by T.ORDINE ASC ";
			}
			
			//SET QUERY PARAM
			cnPs = dbUtil.getConnection().prepareStatement(sql);
			cnPs.setInt(1, Integer.parseInt(targetEntity.getId())); //may be necessary a case if targetEntity is a Dam
			//EXECUTE
			pnRs = cnPs.executeQuery();
			//INIT LIST
			pnList = new ArrayList<ContactInfoBean>();
			
			while (pnRs.next()) {
				//CREATE AND ADD NEW ITEM
				paramsSet = 1;
				
				pnList.add(new ContactInfoBean(pnRs.getString(paramsSet++)
												, pnRs.getString(paramsSet++)
												, pnRs.getInt(paramsSet++)
												, pnRs.getString(paramsSet++)
												, pnRs.getInt(paramsSet++)
												, pnRs.getString(paramsSet++)
												, pnRs.getString(paramsSet++)
												, pnRs.getString(paramsSet++)));
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (cnPs != null) {
				cnPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return pnList;
	}

	public ContactInfoBean createContactInfo(ContactInfoBean pnBean, BasicEntityBean targetEntity, String userId, boolean closeConnection) throws SQLException {
		
		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		ResultSet nextValPnRs = null;
		PreparedStatement cnPs = null;
		ContactInfoBean createdCiBean = null;
		ContactInfoBean foundCiBean = null;
		
		try {
			
			if (closeConnection) {
				//Starting transaction 
				dbUtil.setAutoCommit(false);
			}
			
			//retrieve nextval from targetEntity ContactInfo sequence
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "select CONT_AUTORITA_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "select CONT_CONCESS_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "select CONT_GESTORE_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "select CONT_INGEGNERE_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "select CONT_AENTE_SEQUENZA.nextval from dual";
			}
			
			nextValPnRs = dbUtil.getConnection().createStatement().executeQuery(sql);
			
			if (nextValPnRs != null && nextValPnRs.next()) {
				pnBean.setId(nextValPnRs.getString(1));
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}
			
			//retrieve next orderNum for this entity
			foundCiBean = getContactIntoForEntityWithOrderNumForType(targetEntity, pnBean.getContactType(), true, false);
			
			if (foundCiBean != null) {
				pnBean.setOrderNum(foundCiBean.getOrderNum()+1);	
			} else {
				pnBean.setOrderNum(1);
			}
		
			//insert into targetEntity ContactInfo table
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "insert into CONTATTO_AUTORITA (ID, ID_AUTORITA, ORDINE, CONTATTO, TIPO_CONTATTO, ID_UTENTE) values ( ?, ?, ?, ?, ?, ?)";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "insert into CONTATTO_CONCESS (ID, ID_CONCESS, ORDINE, CONTATTO, TIPO_CONTATTO, ID_UTENTE) values ( ?, ?, ?, ?, ?, ?)";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "insert into CONTATTO_GESTORE (ID, ID_GESTORE, ORDINE, CONTATTO, TIPO_CONTATTO, ID_UTENTE) values ( ?, ?, ?, ?, ?, ?)";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "insert into CONTATTO_INGEGNERE (ID, ID_INGEGNERE, ORDINE, CONTATTO, TIPO_CONTATTO, ID_UTENTE) values ( ?, ?, ?, ?, ?, ?)";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "insert into CONTATTO_ALTRA_ENTE (ID, ID_ENTE, ORDINE, CONTATTO, TIPO_CONTATTO, ID_UTENTE) values ( ?, ?, ?, ?, ?, ?)";
			}
			
			cnPs = dbUtil.getConnection().prepareStatement(sql);
			
			cnPs.setInt(paramsSet++, Integer.parseInt(pnBean.getId())); //ID
			cnPs.setInt(paramsSet++, Integer.parseInt(targetEntity.getId())); //ID_ENTITY
			cnPs.setInt(paramsSet++, pnBean.getOrderNum()); //ORDINE
			cnPs.setString(paramsSet++, pnBean.getContactInfo()); //CONTACT
			cnPs.setInt(paramsSet++, pnBean.getContactType()); //TIPO_CONTATTO
			cnPs.setString(paramsSet++, userId); //ID_UTENTE
			
			updatedRows += cnPs.executeUpdate();
			
			if (closeConnection) {
				dbUtil.getConnection().commit();
			}

			//RETRIEVING CREATED RECORD
			foundCiBean = getContactInfoWithParams(pnBean.getId(), targetEntity, true);

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			if (closeConnection) {
				dbUtil.rollback();
			}

			throw sqle;
		} finally {
			
			if (nextValPnRs != null) {
				nextValPnRs.close();
			}
			if (cnPs != null) {
				cnPs.close();
			}
			
			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}

		return foundCiBean;
	}

	public ContactInfoBean updateContactInfo(ContactInfoBean cnBean, BasicEntityBean targetEntity, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {

		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		PreparedStatement cnPs = null;
		ContactInfoBean foundCnBean = null;
		
		try {
			//fill bean with missing data
			foundCnBean = (ContactInfoBean) bnUtil.compareAndFillBean(getContactInfoWithParams(cnBean.getId(), targetEntity, true), cnBean);
			
			//update targetEntity ContactInfo table
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "update CONTATTO_AUTORITA set ORDINE=?, CONTATTO=?, TIPO_CONTATTO=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? and ID_AUTORITA=?";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "update CONTATTO_CONCESS set ORDINE=?, CONTATTO=?, TIPO_CONTATTO=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? and ID_CONCESS=?";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "update CONTATTO_GESTORE set ORDINE=?, CONTATTO=?, TIPO_CONTATTO=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? and ID_GESTORE=?";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "update CONTATTO_INGEGNERE set ORDINE=?, CONTATTO=?, TIPO_CONTATTO=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? and ID_INGEGNERE=?";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "update CONTATTO_ALTRA_ENTE set ORDINE=?, CONTATTO=?, TIPO_CONTATTO=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? and ID_ENTE=?";
			}
			
			cnPs = dbUtil.getConnection().prepareStatement(sql);
			
			cnPs.setInt(paramsSet++, foundCnBean.getOrderNum()); //ORDINE
			cnPs.setString(paramsSet++, foundCnBean.getContactInfo()); //CONTATTO
			cnPs.setInt(paramsSet++, foundCnBean.getContactType()); //TIPO_CONTATTO
			cnPs.setString(paramsSet++, userId); //ID_UTENTE
			cnPs.setInt(paramsSet++, Integer.parseInt(foundCnBean.getId())); //ID
			cnPs.setInt(paramsSet++, Integer.parseInt(targetEntity.getId())); //ID_ENTITY
			
			updatedRows += cnPs.executeUpdate();
			
			if (updatedRows > 0) {
				foundCnBean = getContactInfoWithParams(cnBean.getId(), targetEntity, true);
			} else {
				throw new EntityNotFoundException("Entity with ID="+cnBean.getId()+" not found.");
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (cnPs != null) {
				cnPs.close();
			}
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}

		return foundCnBean;
	}
	
	public Boolean removeContactInfo(String fieldId, BasicEntityBean targetEntity, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {
		
		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement cnPs = null;
		Boolean wasDeleted = false;
		ContactInfoBean foundCnBean = null;
		
		try {
			
			
			if (closeConnection) {
				//Starting transaction 
				dbUtil.setAutoCommit(false);
			}
			
			//1. recupero tutti i dati per quel ID
			foundCnBean = getContactInfoWithParams(fieldId, targetEntity, false);

			//remove targetEntity ContactInfo table
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "update CONTATTO_AUTORITA set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "update CONTATTO_CONCESS set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "update CONTATTO_GESTORE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "update CONTATTO_INGEGNERE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "update CONTATTO_ALTRA_ENTE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			}
			
			//Retrieving connection
			cnPs = dbUtil.getConnection().prepareStatement(sql);
			cnPs.setString(paramsSet++, userId); //ID_UTENTE
			cnPs.setString(paramsSet++, "S"); //CANCELLATO
			cnPs.setInt(paramsSet++, Integer.parseInt(fieldId)); //ID
			
			//2. cancellazione logica
			updatedRows += cnPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
				
				if (foundCnBean.getOrderNum() == 1) {
					//3. se ordine == 1 controllo che ci siano altri dati con ordine > 1
					foundCnBean = getContactIntoForEntityWithOrderNumForType(targetEntity, foundCnBean.getContactType(), false, false);
					
					if (foundCnBean != null) {
						//4. se ci sono altri dati con ordine diverso recupero il piu basso e lo passo ad 1
						foundCnBean.setOrderNum(1);
						updateContactInfo(foundCnBean, targetEntity, foundCnBean.getLastModifiedUserId(), false);
					}		
				}
				
			} else {
				throw new EntityNotFoundException("Entity with ID="+fieldId+" not found.");
			}

			if (closeConnection) {
				//commiting changes
				dbUtil.getConnection().commit();
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			if (closeConnection) {
				dbUtil.rollback();
			}
			throw sqle;
		} finally {
			if (cnPs != null) {
				cnPs.close();
			}
			
			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}
		
		return wasDeleted;
	}
}
