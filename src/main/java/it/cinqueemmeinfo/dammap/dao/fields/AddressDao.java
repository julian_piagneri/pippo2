package it.cinqueemmeinfo.dammap.dao.fields;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.entities.IngegnereBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;
import it.cinqueemmeinfo.dammap.utility.BeanUtility;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@SuppressWarnings("unused")
public class AddressDao implements Serializable {

	@Autowired private DBUtility dbUtil;
	@Autowired private BeanUtility bnUtil;
	private static final long serialVersionUID = 906472673246974813L;
	private static final Logger logger = LoggerFactory.getLogger(AddressDao.class);
	
	private AddressBean getAddressForEntityFromOrderNum(BasicEntityBean targetEntity, boolean getMax, boolean closeConnection) throws SQLException {

		String sql = "";
		int paramsSet = 1;
		ResultSet adRs = null;
		PreparedStatement adPs = null;
		AddressBean foundAdBean = null;
		
		try {

			if (getMax) {
				if (targetEntity instanceof AutoritaProtezioneCivileBean) {
					sql = "select ID, ID_AUTORITA, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_AUTORITA where ID_AUTORITA = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MAX(ORDINE) FROM INDIRIZZO_AUTORITA WHERE ID_AUTORITA=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof ConcessionarioBean) {
					sql = "select ID, ID_CONCESS, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_CONCESS where ID_CONCESS = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MAX(ORDINE) FROM INDIRIZZO_CONCESS WHERE ID_CONCESS=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof GestoreBean) {
					sql = "select ID, ID_GESTORE, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_GESTORE where ID_GESTORE = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MAX(ORDINE) FROM INDIRIZZO_GESTORE WHERE ID_GESTORE=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof IngegnereBean) {
					sql = "select ID, ID_INGEGNERE, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_INGEGNERE where ID_INGEGNERE = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MAX(ORDINE) FROM INDIRIZZO_INGEGNERE WHERE ID_INGEGNERE=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof AltraEnteBean) {
					sql = "select ID, ID_ENTE, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_ALTRA_ENTE where ID_ENTE = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MAX(ORDINE) FROM INDIRIZZO_ALTRA_ENTE WHERE ID_ENTE=? AND CANCELLATO<>'S') ";
				}
			} else {
				if (targetEntity instanceof AutoritaProtezioneCivileBean) {
					sql = "select ID, ID_AUTORITA, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_AUTORITA where ID_AUTORITA = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MIN(ORDINE) FROM INDIRIZZO_AUTORITA WHERE ID_AUTORITA=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof ConcessionarioBean) {
					sql = "select ID, ID_CONCESS, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_CONCESS where ID_CONCESS = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MIN(ORDINE) FROM INDIRIZZO_CONCESS WHERE ID_CONCESS=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof GestoreBean) {
					sql = "select ID, ID_GESTORE, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_GESTORE where ID_GESTORE = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MIN(ORDINE) FROM INDIRIZZO_GESTORE WHERE ID_GESTORE=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof IngegnereBean) {
					sql = "select ID, ID_INGEGNERE, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_INGEGNERE where ID_INGEGNERE = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MIN(ORDINE) FROM INDIRIZZO_INGEGNERE WHERE ID_INGEGNERE=? AND CANCELLATO<>'S') ";
				} else if (targetEntity instanceof AltraEnteBean) {
					sql = "select ID, ID_ENTE, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, DATA_MODIFICA, ID_UTENTE, CANCELLATO "
						+ "from INDIRIZZO_ALTRA_ENTE where ID_ENTE = ? AND CANCELLATO<>'S' "
						+ "AND ORDINE = (SELECT MIN(ORDINE) FROM INDIRIZZO_ALTRA_ENTE WHERE ID_ENTE=? AND CANCELLATO<>'S') ";
				}
			}
			
			//SET QUERY PARAM
			adPs = dbUtil.getConnection().prepareStatement(sql);
			adPs.setInt(1, Integer.parseInt(targetEntity.getId()));
			adPs.setInt(2, Integer.parseInt(targetEntity.getId()));
			
			//EXECUTE
			adRs = adPs.executeQuery();
			
			while (adRs.next()) {
				//CREATE AND ADD NEW ITEM
				paramsSet = 1;
				
				foundAdBean = new AddressBean(adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getInt(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++));
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (adPs != null) {
				adPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return foundAdBean;
	}
	
	/**
	 * 
	 * @param adBean
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public AddressBean getAddressWithParams(String fieldId, BasicEntityBean targetEntity, boolean closeConnection) throws SQLException {

		String sql = "";
		int paramsSet = 1;
		ResultSet adRs = null;
		PreparedStatement adPs = null;
		AddressBean adBean = null;
		
		try {
			
			//SELECT			
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "select I.ID, I.ID_AUTORITA, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_AUTORITA I  where I.ID = ? AND I.ID_AUTORITA = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "select I.ID, I.ID_CONCESS, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_CONCESS I  where I.ID = ? AND I.ID_CONCESS = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "select I.ID, I.ID_GESTORE, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_GESTORE I  where I.ID = ? AND I.ID_GESTORE = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "select I.ID, I.ID_INGEGNERE, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_INGEGNERE I  where I.ID = ? AND I.ID_INGEGNERE = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "select I.ID, I.ID_ENTE, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_ALTRA_ENTE I  where I.ID = ? AND I.ID_ENTE = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			}
			
			//SET QUERY PARAM
			adPs = dbUtil.getConnection().prepareStatement(sql);
			adPs.setInt(1, Integer.parseInt(fieldId));
			adPs.setInt(2, Integer.parseInt(targetEntity.getId()));
			
			//EXECUTE
			adRs = adPs.executeQuery();
			
			while (adRs.next()) {
				//CREATE AND ADD NEW ITEM
				paramsSet = 1;
				
				adBean = new AddressBean(adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getInt(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++));
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (adPs != null) {
				adPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return adBean;
	}

	/**
	 * 
	 * @param targetEntity
	 * @param closeConnection
	 * @return
	 * @throws SQLException
	 */
	public List<AddressBean> getAddressListWithParams(BasicEntityBean targetEntity, boolean closeConnection) throws SQLException {
		
		String sql = "";
		int paramsSet = 1;
		ResultSet adRs = null;
		PreparedStatement adPs = null;
		ArrayList<AddressBean> adList = null;
		
		try {
			//SELECT
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "select I.ID, I.ID_AUTORITA, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_AUTORITA I  where I.ID_AUTORITA = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "select I.ID, I.ID_CONCESS, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_CONCESS I  where I.ID_CONCESS = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "select I.ID, I.ID_GESTORE, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_GESTORE I  where I.ID_GESTORE = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "select I.ID, I.ID_INGEGNERE, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_INGEGNERE I  where I.ID_INGEGNERE = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "select I.ID, I.ID_ENTE, I.ORDINE, I.INDIRIZZO, I.CAP, I.COMUNE, I.SIGLA_PROVINCIA, I.DATA_MODIFICA, I.ID_UTENTE, I.CANCELLATO "
					+ "from INDIRIZZO_ALTRA_ENTE I  where I.ID_ENTE = ? AND I.CANCELLATO<>'S' order by I.ORDINE ASC ";
			}
			
			//SET QUERY PARAM
			adPs = dbUtil.getConnection().prepareStatement(sql);
			adPs.setInt(1, Integer.parseInt(targetEntity.getId())); //may be necessary a case if targetEntity is a Dam
			//EXECUTE
			adRs = adPs.executeQuery();
			//INIT LIST
			adList = new ArrayList<AddressBean>();
			
			while (adRs.next()) {
				//CREATE AND ADD NEW ITEM
				paramsSet = 1;
				
				adList.add(new AddressBean(adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getInt(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)
										, adRs.getString(paramsSet++)));
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			if (adPs != null) {
				adPs.close();
			}
			
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}
		
		return adList;
	}
	
	public AddressBean createAddress(AddressBean adBean, BasicEntityBean targetEntity, String userId, boolean closeConnection) throws SQLException {
		
		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		ResultSet nextValAdRs = null;
		PreparedStatement adPs = null;
		AddressBean foundAdBean = null;
		
		try {
			
			if (closeConnection) {
				//Starting transaction 
				dbUtil.setAutoCommit(false);
			}
			
			//retrieve nextval from targetEntity Address sequence
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "select IND_AUTORITA_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "select INDIRIZZO_CONCESS_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "select INDIRIZZO_GESTORE_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "select INDIRIZZO_INGEGNERE_SEQUENZA.nextval from dual";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "select INDIRIZZO_AENTE_SEQUENZA.nextval from dual";
			}
			
			nextValAdRs = dbUtil.getConnection().createStatement().executeQuery(sql);
			
			if (nextValAdRs != null && nextValAdRs.next()) {
				adBean.setId(nextValAdRs.getString(1));
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}
			
			//retrieve next orderNum for this entity
			foundAdBean = getAddressForEntityFromOrderNum(targetEntity, true, false);
			
			if (foundAdBean != null) {
				adBean.setOrderNum(foundAdBean.getOrderNum()+1);	
			} else {
				adBean.setOrderNum(1);
			}
		
			//insert into targetEntity Address table
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				//sql = "insert into INDIRIZZO_AUTORITA (ID, ID_AUTORITA, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, ID_UTENTE) values ( ?, ?, ?, ?, ?, ?, ?, ?)";
				sql = "INSERT INTO INDIRIZZO_AUTORITA (ID_UTENTE, ID, ID_AUTORITA, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA, REGIONE) " +
						"SELECT ?, ?, ?, ?, ?, ?, ?, SIGLA, NOME_REGIONE FROM PROVINCIA WHERE SIGLA = ? ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "insert into INDIRIZZO_CONCESS (ID_UTENTE, ID, ID_CONCESS, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA) values ( ?, ?, ?, ?, ?, ?, ?, ?)";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "insert into INDIRIZZO_GESTORE (ID_UTENTE, ID, ID_GESTORE, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA) values ( ?, ?, ?, ?, ?, ?, ?, ?)";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "insert into INDIRIZZO_INGEGNERE (ID_UTENTE, ID, ID_INGEGNERE, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA) values ( ?, ?, ?, ?, ?, ?, ?, ?)";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "insert into INDIRIZZO_ALTRA_ENTE (ID_UTENTE, ID, ID_ENTE, ORDINE, INDIRIZZO, CAP, COMUNE, SIGLA_PROVINCIA) values ( ?, ?, ?, ?, ?, ?, ?, ?)";
			}
			
			adPs = dbUtil.getConnection().prepareStatement(sql);
			
			adPs.setString(paramsSet++, userId); //ID_UTENTE
			adPs.setInt(paramsSet++, Integer.parseInt(adBean.getId())); //ID
			adPs.setInt(paramsSet++, Integer.parseInt(targetEntity.getId())); //ID_ENTITY
			adPs.setInt(paramsSet++, adBean.getOrderNum()); //ORDINE
			adPs.setString(paramsSet++, adBean.getIndirizzo()); //INDIRIZZO
			adPs.setString(paramsSet++, adBean.getCap()); //CAP
			adPs.setString(paramsSet++, adBean.getNomeComune()); //COMUNE
			adPs.setString(paramsSet++, adBean.getSiglaProvincia()); //SIGLA_PROVINCIA
			
			updatedRows += adPs.executeUpdate();
			
			if (closeConnection) {
				//commiting changes
				dbUtil.getConnection().commit();
			}
			
			//RETRIEVING CREATED RECORD
			foundAdBean =  getAddressWithParams(adBean.getId(), targetEntity, true);
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			if (closeConnection) {
				dbUtil.rollback();
			}

			throw sqle;
		} finally {
			
			if (nextValAdRs != null) {
				nextValAdRs.close();
			}
			if (adPs != null) {
				adPs.close();
			}
			
			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}

		return foundAdBean;
	}


	public AddressBean updateAddress(AddressBean adBean, BasicEntityBean targetEntity, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {

		String sql = "";
		int paramsSet = 1;
		int updatedRows = 0;
		AddressBean foundAdBean = null;
		PreparedStatement adPs = null;
		
		try {
			
			foundAdBean =  (AddressBean) bnUtil.compareAndFillBean(getAddressWithParams(adBean.getId(), targetEntity, true), adBean);
			
			//update targetEntity Address table			
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "update INDIRIZZO_AUTORITA set ORDINE=?, INDIRIZZO=?, CAP=?, COMUNE=?, SIGLA_PROVINCIA=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? AND ID_AUTORITA=? ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "update INDIRIZZO_CONCESS set ORDINE=?, INDIRIZZO=?, CAP=?, COMUNE=?, SIGLA_PROVINCIA=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? AND ID_CONCESS=? ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "update INDIRIZZO_GESTORE set ORDINE=?, INDIRIZZO=?, CAP=?, COMUNE=?, SIGLA_PROVINCIA=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? AND ID_GESTORE=? ";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "update INDIRIZZO_INGEGNERE set ORDINE=?, INDIRIZZO=?, CAP=?, COMUNE=?, SIGLA_PROVINCIA=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? AND ID_INGEGNERE=? ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "update INDIRIZZO_ALTRA_ENTE set ORDINE=?, INDIRIZZO=?, CAP=?, COMUNE=?, SIGLA_PROVINCIA=?, DATA_MODIFICA=SYSDATE, ID_UTENTE=? where ID=? AND ID_ENTE=? ";
			}
			
			adPs = dbUtil.getConnection().prepareStatement(sql);
			
			adPs.setInt(paramsSet++, foundAdBean.getOrderNum()); //ORDINE
			adPs.setString(paramsSet++, foundAdBean.getIndirizzo()); //INDIRIZZO
			adPs.setString(paramsSet++, foundAdBean.getCap()); //CAP
			adPs.setString(paramsSet++, foundAdBean.getNomeComune()); //CAP
			adPs.setString(paramsSet++, foundAdBean.getSiglaProvincia()); //CAP
			adPs.setString(paramsSet++, userId); //ID_UTENTE
			adPs.setInt(paramsSet++, Integer.parseInt(foundAdBean.getId())); //ID
			adPs.setInt(paramsSet++, Integer.parseInt(targetEntity.getId())); //ID_ENTITY
			
			updatedRows += adPs.executeUpdate();
			
			if (updatedRows > 0) {
				foundAdBean =  getAddressWithParams(adBean.getId(), targetEntity, true);
			} else {
				throw new EntityNotFoundException("Entity with ID="+adBean.getId()+" not found.");
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			throw sqle;
		} finally {
			if (adPs != null) {
				adPs.close();
			}
			if (closeConnection) {
				dbUtil.closeConnection();
			}
		}

		return foundAdBean;
	}
	
	public Boolean removeAddress(String fieldId, BasicEntityBean targetEntity, String userId, boolean closeConnection) throws SQLException, EntityNotFoundException {
		
		int paramsSet = 1;
		int updatedRows = 0;
		String sql = "";
		PreparedStatement apcPs = null;
		Boolean wasDeleted = false;
		AddressBean foundAdBean = null;
		
		try {
			
			if (closeConnection) {
				//Starting transaction 
				dbUtil.setAutoCommit(false);
			}

			//1. recupero tutti i dati per quel ID
			foundAdBean = getAddressWithParams(fieldId, targetEntity, false);
			
			//update targetEntity Address table			
			if (targetEntity instanceof AutoritaProtezioneCivileBean) {
				sql = "update INDIRIZZO_AUTORITA set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			} else if (targetEntity instanceof ConcessionarioBean) {
				sql = "update INDIRIZZO_CONCESS set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			} else if (targetEntity instanceof GestoreBean) {
				sql = "update INDIRIZZO_GESTORE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			} else if (targetEntity instanceof IngegnereBean) {
				sql = "update INDIRIZZO_INGEGNERE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			} else if (targetEntity instanceof AltraEnteBean) {
				sql = "update INDIRIZZO_ALTRA_ENTE set DATA_MODIFICA=SYSDATE, ID_UTENTE=?, CANCELLATO=? where ID=? ";
			}
			
			//Retrieving connection
			apcPs = dbUtil.getConnection().prepareStatement(sql);
			apcPs.setString(paramsSet++, userId); //ID_UTENTE
			apcPs.setString(paramsSet++, "S"); //CANCELLATO
			apcPs.setInt(paramsSet++, Integer.parseInt(fieldId)); //ID

			//2. cancellazione logica
			updatedRows += apcPs.executeUpdate();

			if (updatedRows > 0) {
				wasDeleted = true;
				
				if (foundAdBean.getOrderNum() == 1) {
					//3. se ordine == 1 controllo che ci siano altri dati con ordine > 1
					foundAdBean = getAddressForEntityFromOrderNum(targetEntity, false, false);

					//4. se ci sono altri dati con ordine diverso recupero il piu basso e lo passo ad 1
					if (foundAdBean != null) {
						foundAdBean.setOrderNum(1);
						updateAddress(foundAdBean, targetEntity, foundAdBean.getLastModifiedUserId(), false);
					}
				}

			} else {
				throw new EntityNotFoundException("Entity with ID="+fieldId+" not found.");
			}
			
			if (closeConnection) {
				//commiting changes
				dbUtil.getConnection().commit();
			}
			
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "'");
			if (closeConnection) {
				dbUtil.rollback();
			}
			throw sqle;
		} finally {
			if (apcPs != null) {
				apcPs.close();
			}
			
			if (closeConnection) {
				dbUtil.setAutoCommit(true);
				dbUtil.closeConnection();
			}
		}
		
		return wasDeleted;
	}
	
}
