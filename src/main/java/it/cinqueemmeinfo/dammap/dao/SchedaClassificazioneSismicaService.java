package it.cinqueemmeinfo.dammap.dao;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.ClassificazioneSismicaDigaBean;
import it.cinqueemmeinfo.dammap.bean.entities.PericolisitaSismicaBean;
import it.cinqueemmeinfo.dammap.bean.entities.SnellezzaVunerabilitaBean;
import it.cinqueemmeinfo.dammap.dao.entities.PericolisitaSismicaDao;
import it.cinqueemmeinfo.dammap.dao.entities.SnellezzaVunerabilitaDao;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class SchedaClassificazioneSismicaService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(SchedaClassificazioneSismicaService.class);

	@Autowired
	private PericolisitaSismicaDao psDao;
	@Autowired
	private SnellezzaVunerabilitaDao svDao;
	@Autowired
	private UserUtility usrUtil;

	public List<PericolisitaSismicaBean> allPericolisitaDao(String damID) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.CLASSIFICAZIONE_SISMICA);

		return psDao.getPericolisitaSismicaByDamId(damID);
	}

	public SnellezzaVunerabilitaBean getSnellezzaVunerabilitaByDamID(String damID) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.CLASSIFICAZIONE_SISMICA);

		return svDao.getSnellezzaVunerabilitaByDamID(damID);
	}

	public void updateSnellezzaVunerabilita(SnellezzaVunerabilitaBean bean, String damID) throws SQLException {
		usrUtil.checkCanWrite(DBUtility.CLASSIFICAZIONE_SISMICA);
		svDao.updateSnellezzaVunerabilita(bean, damID, usrUtil.getUserId());
	}

	public void updatePericolisita(List<PericolisitaSismicaBean> bean, String damID) throws SQLException {
		usrUtil.checkCanWrite(DBUtility.CLASSIFICAZIONE_SISMICA);
		psDao.updatePericolisita(bean, damID, usrUtil.getUserId());
	}

	public ClassificazioneSismicaDigaBean getClassificazioneSismicaDigaByDamId(String damId) throws SQLException {
		usrUtil.checkCanRead(DBUtility.CLASSIFICAZIONE_SISMICA);

		return svDao.getClassificazioneSismicaDigaByDamId(damId);
	}
	
	public void updateClassificazioneSismicaDiga(ClassificazioneSismicaDigaBean bean, String damID) throws SQLException {
		usrUtil.checkCanWrite(DBUtility.CLASSIFICAZIONE_SISMICA);
		svDao.updateClassificazioneSismicaDiga(bean, damID, usrUtil.getUserId());
	}
}