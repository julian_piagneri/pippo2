package it.cinqueemmeinfo.dammap.dao;

import java.sql.SQLException;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.ProgettoGetioneBean;
import it.cinqueemmeinfo.dammap.dao.entities.ProgettoGestioneDao;
import it.cinqueemmeinfo.dammap.utility.DBUtility;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class SchedaProgettoGestioneService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(SchedaProgettoGestioneService.class);

	@Autowired
	private ProgettoGestioneDao prDao;
	@Autowired
	private UserUtility usrUtil;

	public ProgettoGetioneBean getProgettoGestioneByDamID(String damID) throws SQLException, EntityNotFoundException {
		usrUtil.checkCanRead(DBUtility.PROGETTO_DI_GESTIONE);

		return prDao.getProgettoGestioneByDamID(damID);
	}

	public void updateProgettoGestione(ProgettoGetioneBean bean, String damID) throws SQLException, ParseException {
		usrUtil.checkCanWrite(DBUtility.PROGETTO_DI_GESTIONE);
		prDao.updateProgettoGestione(bean, usrUtil.getUserId(), damID);
	}
}
