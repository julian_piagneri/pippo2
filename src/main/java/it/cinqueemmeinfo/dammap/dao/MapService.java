package it.cinqueemmeinfo.dammap.dao;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Current implementation of the JsonDAO that allows data retrieval from JSON sources
 * @author Wladimir Totino
 *
 */
@Repository
public class MapService {
	
	/**
	 * Generates an http entity for apache http client for a get request
	 * @param url
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private HttpEntity getHttpEntity(String url) throws ClientProtocolException, IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(url);
		CloseableHttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		return entity;
	}
	
	/**
	 * Generates an http entity for apache http client for a put request
	 * @param url
	 * @param body
	 * @param contentType
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	private HttpEntity getHttpPutEntity(String url, String body, String contentType) throws ClientProtocolException, IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpput = new HttpPut(url);
		httpput.setHeader("Content-Type", contentType+"; charset=utf-8");
		StringEntity bodyEntity = new StringEntity(body);
		httpput.setEntity(bodyEntity);
		CloseableHttpResponse response = httpclient.execute(httpput);
		HttpEntity entity = response.getEntity();
		return entity;
	}
	
	/**
	 * Generates an http entity for apache http client for a post request
	 * @param url
	 * @param body
	 * @param contentType
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	private HttpEntity getHttpPostEntity(String url, String body, String contentType) throws ClientProtocolException, IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpput = new HttpPost(url);
		httpput.setHeader("Content-Type", contentType+"; charset=utf-8");
		StringEntity bodyEntity = new StringEntity(body);
		httpput.setEntity(bodyEntity);
		CloseableHttpResponse response = httpclient.execute(httpput);
		HttpEntity entity = response.getEntity();
		return entity;
	}
		
	public JsonNode getJsonObject(String url) throws ClientProtocolException, IOException {
		HttpEntity entity = this.getHttpEntity(url);
		if (entity != null) {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readTree(EntityUtils.toString(entity));
		}
		throw new IOException("There was an error trying to complete the request");
	}
	
	public String getJsonAsString(String url) throws ClientProtocolException, IOException {
		HttpEntity entity = this.getHttpEntity(url);
		if (entity != null) {
			return EntityUtils.toString(entity);
		}
		throw new IOException("There was an error trying to complete the request");
	}
}
