package it.cinqueemmeinfo.dammap.dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import it.cinqueemmeinfo.dammap.bean.entities.MenuBean;
import it.cinqueemmeinfo.dammap.dao.entities.MenuDao;

public class MenuService implements Serializable {

	private static final long serialVersionUID = -2927571924602882936L;
	//private static final Logger logger = LoggerFactory.getLogger(MenuService.class);
	
	//@Autowired private DBUtility dbUtil;
	//@Autowired private UserUtility usrUtil;
	@Autowired private MenuDao menuDao;
	
	//******************* RUBRICA *************************

	public List<MenuBean> getMenuForUser(Long userId) throws SQLException {
		//add check user rights
		return menuDao.retrieveMenuForUser(userId, Boolean.TRUE);
	}

	public List<HashMap<String,String>> getDamDetails(Long userId, long numeroArchivio, String sub) throws SQLException {
		// TODO Auto-generated method stub
		return menuDao.retrieveSingleDam(userId, numeroArchivio, sub, Boolean.TRUE);
	}
}
