package it.cinqueemmeinfo.dammap.bean.entities;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DamBean extends BasicEntityBean {

	private static final long serialVersionUID = 7842856643747811077L;
	
	private String numeroArchivio;
	private String sub;
	private String nomeDiga;
	private String latCentrale; //LAT_CENTRALE
	private String longCentrale; //LONG_CENTRALE
	private String nomeLago; //NOME_LAGO
	private String bacino;
	private String nomeFiumeSbarrato; //NOME_FIUME_SBARRATO
	private String status1; //STATUS_PRINCIPALE + DESCR_STATUS_PRINCIPALE
	private String status2; //STATUS_SECONDARIO + DESCR_STATUS_SECONDARIO
	private String competenzaSnd; //COMPETENZA_SND
	private String annoConsegnaLavori; //ANNO_CONSEGNA_LAVORI
	private String annoUltimazLavori; //ANNO_ULTIMAZIONE_LAVORI
	private String dataCertCollaudo; //DATA_CERTIFICATO_COLLAUDO
	private String altezzaDigaDm; //ALTEZZA_DIGA_DM
	private String altezzaDigaL584; //ALTEZZA_DIGA_L584
	private String descrClassDiga; //DESCRIZIONE_CLASSIFICA_DIGA
	private String quotaMax; //QUOTA_MAX
	private String quotaMaxRegolaz; //QUOTA_MAX_REGOLAZIONE
	private String quotaAutoriz; //QUOTA_AUTORIZZATA
	private String volumeTotInvasoDm; //VOLUME_TOTALE_INVASO_DM
	private String volumeTotInvasoL584; //VOLUME_TOTALE_INVASO_L584
	private String volumeLaminazione; //VOLUME_LAMINAZIONE
	private String portMaxPienaProgetto; //PORTATA_MAX_PIENA_PROGETTO
	private String volumeAutoriz; //VOLUME_AUTORIZZATO
	private String funzSedeCentrale; //FUNZIONARIO_SEDE_CENTRALE
	private String funzUffPerif; //FUNZIONARIO_UFF_PERIFERICO
	private String uffSedeCentrale; //UFFICIO_SEDE_CENTRALE
	private String uffPeriferico; //UFFICIO_PERIFERICO
	private String numIscrizRid; //NUM_ISCRIZIONE_RID
	private String condotteForzate; //CONDOTTE_FORZATE
	private String status1Sigla;
	private String status1Dettaglio;
	private String status2Sigla;
	private String status2Dettaglio;
	private String idInvaso;
	private ArrayList<DamExtraDataBean> altriDati;
	private int idFunzionarioCentrale;
	private int idFunzionarioPerif;
	
	private boolean preferita;
	
	private LinkedHashMap<String, CampoInfoBean> extraInfo=new LinkedHashMap<String, CampoInfoBean>();
	private Set<DamExtraDataComuneBean> nomiComune=new LinkedHashSet<DamExtraDataComuneBean>();
	private Set<String> nomeFiumeValle=new LinkedHashSet<String>();
	private Set<String> comuneOperaPresa=new LinkedHashSet<String>();
	private Set<String> tipoOrganoScarico=new LinkedHashSet<String>();
	private Set<String> altraDenominaz=new LinkedHashSet<String>();
	private Set<DamExtraDataUtilizzazioneBean> utilizzo=new LinkedHashSet<DamExtraDataUtilizzazioneBean>();
	private Set<DamExtraDataIngegnereBean> ingegneri=new LinkedHashSet<DamExtraDataIngegnereBean>();
	private Set<String> digheInvaso=new LinkedHashSet<String>();
	private Set<String> concessionari=new LinkedHashSet<String>();
	private Set<String> gestori=new LinkedHashSet<String>();
	
	public DamBean() { }
	
	public DamBean(String id, String numeroArchivio, String sub, String nomeDiga) {
		super(id);
		this.numeroArchivio = numeroArchivio;
		this.sub = sub;
		this.nomeDiga = nomeDiga;
	}

	public DamBean(String id, String numeroArchivio, String sub, String nomeDiga,
			String latCentrale, String longCentrale, String nomeLago,
			String bacino, String nomeFiumeSbarrato, String status1,
			String status2, String competenzaSnd, String annoConsegnaLavori,
			String annoUltimazLavori, String dataCertCollaudo,
			String altezzaDigaDm, String altezzaDigaL584, String descrClassDiga, 
			String quotaMax, String quotaMaxRegolaz, String quotaAutoriz,
			String volumeTotInvasoDm, String volumeTotInvasoL584,
			String volumeLaminazione, String portMaxPienaProgetto,
			String volumeAutoriz, String funzSedeCentrale, String funzUffPerif,
			String uffSedeCentrale, String uffPeriferico, String numIscrizRid,
			String condotteForzate, String idInvaso, String lastModifiedDate
			, String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.numeroArchivio = numeroArchivio;
		this.sub = sub;
		this.nomeDiga = nomeDiga;
		this.latCentrale = latCentrale;
		this.longCentrale = longCentrale;
		this.nomeLago = nomeLago;
		this.bacino = bacino;
		this.nomeFiumeSbarrato = nomeFiumeSbarrato;
		this.status1 = status1;
		this.status2 = status2;
		this.competenzaSnd = competenzaSnd;
		this.annoConsegnaLavori = annoConsegnaLavori;
		this.annoUltimazLavori = annoUltimazLavori;
		this.dataCertCollaudo = dataCertCollaudo;
		this.altezzaDigaDm = altezzaDigaDm;
		this.altezzaDigaL584 = altezzaDigaL584;
		this.descrClassDiga = descrClassDiga;
		this.quotaMax = quotaMax;
		this.quotaMaxRegolaz = quotaMaxRegolaz;
		this.quotaAutoriz = quotaAutoriz;
		this.volumeTotInvasoDm = volumeTotInvasoDm;
		this.volumeTotInvasoL584 = volumeTotInvasoL584;
		this.volumeLaminazione = volumeLaminazione;
		this.portMaxPienaProgetto = portMaxPienaProgetto;
		this.volumeAutoriz = volumeAutoriz;
		this.funzSedeCentrale = funzSedeCentrale;
		this.funzUffPerif = funzUffPerif;
		this.uffSedeCentrale = uffSedeCentrale;
		this.uffPeriferico = uffPeriferico;
		this.numIscrizRid = numIscrizRid;
		this.condotteForzate = condotteForzate;
		this.idInvaso = idInvaso;
	}

	public String getNumeroArchivio() {
		return numeroArchivio;
	}

	public void setNumeroArchivio(String numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getNomeDiga() {
		return nomeDiga;
	}

	public void setNomeDiga(String nomeDiga) {
		this.nomeDiga = nomeDiga;
	}

	public String getLatCentrale() {
		return latCentrale;
	}

	public void setLatCentrale(String latCentrale) {
		this.latCentrale = latCentrale;
	}

	public String getLongCentrale() {
		return longCentrale;
	}

	public void setLongCentrale(String longCentrale) {
		this.longCentrale = longCentrale;
	}

	public String getNomeLago() {
		return nomeLago;
	}

	public void setNomeLago(String nomeLago) {
		this.nomeLago = nomeLago;
	}

	public String getBacino() {
		return bacino;
	}

	public void setBacino(String bacino) {
		this.bacino = bacino;
	}

	public String getNomeFiumeSbarrato() {
		return nomeFiumeSbarrato;
	}

	public void setNomeFiumeSbarrato(String nomeFiumeSbarrato) {
		this.nomeFiumeSbarrato = nomeFiumeSbarrato;
	}

	public String getStatus1() {
		return status1;
	}

	public void setStatus1(String status1) {
		this.status1 = status1;
	}

	public String getStatus2() {
		return status2;
	}

	public void setStatus2(String status2) {
		this.status2 = status2;
	}

	public String getStatus1Sigla() {
		return status1Sigla;
	}

	public void setStatus1Sigla(String status1Sigla) {
		this.status1Sigla = status1Sigla;
	}

	public String getStatus1Dettaglio() {
		return status1Dettaglio;
	}

	public void setStatus1Dettaglio(String status1Dettaglio) {
		this.status1Dettaglio = status1Dettaglio;
	}

	public String getStatus2Sigla() {
		return status2Sigla;
	}

	public void setStatus2Sigla(String status2Sigla) {
		this.status2Sigla = status2Sigla;
	}

	public String getStatus2Dettaglio() {
		return status2Dettaglio;
	}

	public void setStatus2Dettaglio(String status2Dettaglio) {
		this.status2Dettaglio = status2Dettaglio;
	}

	public String getCompetenzaSnd() {
		return competenzaSnd;
	}

	public void setCompetenzaSnd(String competenzaSnd) {
		this.competenzaSnd = competenzaSnd;
	}

	public String getAnnoConsegnaLavori() {
		return annoConsegnaLavori;
	}

	public void setAnnoConsegnaLavori(String annoConsegnaLavori) {
		this.annoConsegnaLavori = annoConsegnaLavori;
	}

	public String getAnnoUltimazLavori() {
		return annoUltimazLavori;
	}

	public void setAnnoUltimazLavori(String annoUltimazLavori) {
		this.annoUltimazLavori = annoUltimazLavori;
	}

	public String getDataCertCollaudo() {
		return dataCertCollaudo;
	}

	public void setDataCertCollaudo(String dataCertCollaudo) {
		this.dataCertCollaudo = dataCertCollaudo;
	}

	public String getAltezzaDigaDm() {
		return altezzaDigaDm;
	}

	public void setAltezzaDigaDm(String altezzaDigaDm) {
		this.altezzaDigaDm = altezzaDigaDm;
	}

	public String getAltezzaDigaL584() {
		return altezzaDigaL584;
	}

	public void setAltezzaDigaL584(String altezzaDigaL584) {
		this.altezzaDigaL584 = altezzaDigaL584;
	}

	public String getDescrClassDiga() {
		return descrClassDiga;
	}

	public void setDescrClassDiga(String descrClassDiga) {
		this.descrClassDiga = descrClassDiga;
	}
	
	public String getQuotaMax() {
		return quotaMax;
	}

	public void setQuotaMax(String quotaMax) {
		this.quotaMax = quotaMax;
	}
	
	public String getQuotaMaxRegolaz() {
		return quotaMaxRegolaz;
	}

	public void setQuotaMaxRegolaz(String quotaMaxRegolaz) {
		this.quotaMaxRegolaz = quotaMaxRegolaz;
	}

	public String getQuotaAutoriz() {
		return quotaAutoriz;
	}

	public void setQuotaAutoriz(String quotaAutoriz) {
		this.quotaAutoriz = quotaAutoriz;
	}

	public String getVolumeTotInvasoDm() {
		return volumeTotInvasoDm;
	}

	public void setVolumeTotInvasoDm(String volumeTotInvasoDm) {
		this.volumeTotInvasoDm = volumeTotInvasoDm;
	}

	public String getVolumeTotInvasoL584() {
		return volumeTotInvasoL584;
	}

	public void setVolumeTotInvasoL584(String volumeTotInvasoL584) {
		this.volumeTotInvasoL584 = volumeTotInvasoL584;
	}

	public String getVolumeLaminazione() {
		return volumeLaminazione;
	}

	public void setVolumeLaminazione(String volumeLaminazione) {
		this.volumeLaminazione = volumeLaminazione;
	}

	public String getPortMaxPienaProgetto() {
		return portMaxPienaProgetto;
	}

	public void setPortMaxPienaProgetto(String portMaxPienaProgetto) {
		this.portMaxPienaProgetto = portMaxPienaProgetto;
	}

	public String getVolumeAutoriz() {
		return volumeAutoriz;
	}

	public void setVolumeAutoriz(String volumeAutoriz) {
		this.volumeAutoriz = volumeAutoriz;
	}

	public String getFunzSedeCentrale() {
		return funzSedeCentrale;
	}

	public void setFunzSedeCentrale(String funzSedeCentrale) {
		this.funzSedeCentrale = funzSedeCentrale;
	}

	public String getFunzUffPerif() {
		return funzUffPerif;
	}

	public void setFunzUffPerif(String funzUffPerif) {
		this.funzUffPerif = funzUffPerif;
	}

	public String getUffSedeCentrale() {
		return uffSedeCentrale;
	}

	public void setUffSedeCentrale(String uffSedeCentrale) {
		this.uffSedeCentrale = uffSedeCentrale;
	}

	public String getUffPeriferico() {
		return uffPeriferico;
	}

	public void setUffPeriferico(String uffPeriferico) {
		this.uffPeriferico = uffPeriferico;
	}

	public String getNumIscrizRid() {
		return numIscrizRid;
	}

	public void setNumIscrizRid(String numIscrizRid) {
		this.numIscrizRid = numIscrizRid;
	}

	public String getCondotteForzate() {
		return condotteForzate;
	}

	public void setCondotteForzate(String condotteForzate) {
		this.condotteForzate = condotteForzate;
	}

	public String getIdInvaso() {
		return idInvaso;
	}

	public void setIdInvaso(String idInvaso) {
		this.idInvaso = idInvaso;
	}

	public ArrayList<DamExtraDataBean> getAltriDati() {
		if (altriDati == null) {
			altriDati = new ArrayList<DamExtraDataBean>();
		}
		return altriDati;
	}

	public void setAltriDati(ArrayList<DamExtraDataBean> altriDati) {
		this.altriDati = altriDati;
	}

	public Set<DamExtraDataComuneBean> getNomiComune() {
		return nomiComune;
	}

	public void setNomiComune(Set<DamExtraDataComuneBean> nomiComune) {
		this.nomiComune = nomiComune;
	}

	public Set<String> getComuneOperaPresa() {
		return comuneOperaPresa;
	}

	public void setComuneOperaPresa(Set<String> comuneOperaPresa) {
		this.comuneOperaPresa = comuneOperaPresa;
	}
	
	public void addComuneOperaPresa(String comuneOperaPresa){
		if(comuneOperaPresa!=null){
			this.comuneOperaPresa.add(comuneOperaPresa);
		}
	}

	public Set<String> getTipoOrganoScarico() {
		return tipoOrganoScarico;
	}

	public void setTipoOrganoScarico(Set<String> tipoOrganoScarico) {
		this.tipoOrganoScarico = tipoOrganoScarico;
	}
	
	public void addTipoOrganoScarico(String tipoOrganoScarico){
		if(tipoOrganoScarico!=null){
			this.tipoOrganoScarico.add(tipoOrganoScarico);
		}
	}

	public Set<String> getAltraDenominaz() {
		return altraDenominaz;
	}

	public void setAltraDenominaz(Set<String> altraDenominaz) {
		this.altraDenominaz = altraDenominaz;
	}
	
	public void addAltraDenominaz(String altraDenominaz){
		if(altraDenominaz!=null){
			this.altraDenominaz.add(altraDenominaz);
		}
	}

	public boolean getPreferita() {
		return preferita;
	}

	public void setPreferita(boolean preferita) {
		this.preferita = preferita;
	}

	public Set<DamExtraDataUtilizzazioneBean> getUtilizzo() {
		return utilizzo;
	}

	public void setUtilizzo(Set<DamExtraDataUtilizzazioneBean> utilizzo) {
		this.utilizzo = utilizzo;
	}

	public Set<String> getDigheInvaso() {
		return digheInvaso;
	}

	public void setDigheInvaso(Set<String> digheInvaso) {
		this.digheInvaso = digheInvaso;
	}
	
	public void addDigheInvaso(String digheInvaso){
		if(digheInvaso!=null){
			this.digheInvaso.add(digheInvaso);
		}
	}

	public Set<String> getNomeFiumeValle() {
		return nomeFiumeValle;
	}

	public void setNomeFiumeValle(Set<String> nomeFiumeValle) {
		this.nomeFiumeValle = nomeFiumeValle;
	}
	
	public int getIdFunzionarioCentrale() {
		return idFunzionarioCentrale;
	}

	public void setIdFunzionarioCentrale(int idFunzionarioCentrale) {
		this.idFunzionarioCentrale = idFunzionarioCentrale;
	}

	public int getIdFunzionarioPerif() {
		return idFunzionarioPerif;
	}

	public void setIdFunzionarioPerif(int idFunzionarioPerif) {
		this.idFunzionarioPerif = idFunzionarioPerif;
	}

	public void addNomeFiumeValle(String nomeFiumeValle){
		if(nomeFiumeValle!=null){
			this.nomeFiumeValle.add(nomeFiumeValle);
		}
	}

	public Set<DamExtraDataIngegnereBean> getIngegneri() {
		return ingegneri;
	}

	public void setIngegneri(Set<DamExtraDataIngegnereBean> ingegneri) {
		this.ingegneri = ingegneri;
	}
	
	public void addIngegnere(DamExtraDataIngegnereBean ingegnere){
		if(nomeFiumeValle!=null){
			this.ingegneri.add(ingegnere);
		}
	}

	public Set<String> getConcessionari() {
		return concessionari;
	}

	public void setConcessionari(Set<String> concessionari) {
		this.concessionari = concessionari;
	}
	
	public void addConcessionario(String concessionario){
		if(concessionario!=null){
			this.concessionari.add(concessionario);
		}
	}

	public Set<String> getGestori() {
		return gestori;
	}

	public void setGestori(Set<String> gestori) {
		this.gestori = gestori;
	}
	
	public void addGestore(String gestore){
		if(gestore!=null){
			this.gestori.add(gestore);
		}
	}

	public LinkedHashMap<String, CampoInfoBean> getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(LinkedHashMap<String, CampoInfoBean> extraInfo) {
		this.extraInfo = extraInfo;
	}
	
}
