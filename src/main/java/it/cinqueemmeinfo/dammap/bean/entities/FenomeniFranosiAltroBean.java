package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FenomeniFranosiAltroBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 4692873425398881619L;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long idFen = null;
	private String altro;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double distanza = null;

	/**
	 * @return the idFen
	 */
	public Long getIdFen() {
		return idFen;
	}

	/**
	 * @param idFen
	 *            the idFen to set
	 */
	public void setIdFen(Long idFen) {
		this.idFen = idFen;
	}

	/**
	 * @return the altro
	 */
	public String getAltro() {
		return altro;
	}

	/**
	 * @param altro
	 *            the altro to set
	 */
	public void setAltro(String altro) {
		this.altro = altro;
	}

	/**
	 * @return the distanza
	 */
	public Double getDistanza() {
		return distanza;
	}

	/**
	 * @param distanza
	 *            the distanza to set
	 */
	public void setDistanza(Double distanza) {
		this.distanza = distanza;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((altro == null) ? 0 : altro.hashCode());
		result = prime * result + ((distanza == null) ? 0 : distanza.hashCode());
		result = prime * result + ((idFen == null) ? 0 : idFen.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof FenomeniFranosiAltroBean)) {
			return false;
		}
		FenomeniFranosiAltroBean other = (FenomeniFranosiAltroBean) obj;
		if (altro == null) {
			if (other.altro != null) {
				return false;
			}
		} else if (!altro.equals(other.altro)) {
			return false;
		}
		if (distanza == null) {
			if (other.distanza != null) {
				return false;
			}
		} else if (!distanza.equals(other.distanza)) {
			return false;
		}
		if (idFen == null) {
			if (other.idFen != null) {
				return false;
			}
		} else if (!idFen.equals(other.idFen)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FenomeniFranosiAltroBean [idFen=" + idFen + ", altro=" + altro + ", distanza=" + distanza + "]";
	}
}