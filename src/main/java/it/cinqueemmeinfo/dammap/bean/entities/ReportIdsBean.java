package it.cinqueemmeinfo.dammap.bean.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;

public class ReportIdsBean implements Serializable {

	private static final long serialVersionUID = 1233647382902762145L;

	private Long id;
	private Long idUtente;
	private String name;
	private List<DamBean> damIds = new ArrayList<DamBean>();
	private JsonNode jsonData;
	private Timestamp creationDate;

	public ReportIdsBean() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	public Long getIdUtente() {
		return idUtente;
	}

	public void setIdUtente(Long idUtente) {
		this.idUtente = idUtente;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonInclude
	public JsonNode getJsonData() {
		return jsonData;
	}

	public void setJsonData(JsonNode jsonData) {
		this.jsonData = jsonData;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public List<DamBean> getDamIds() {
		return damIds;
	}

	public void setDamIds(List<DamBean> damIds) {
		this.damIds = damIds;
	}

	@Override
	public String toString() {
		return "ReportBean [getId()=" + getId() + ", getIdUtente()=" + getIdUtente() + ", getName()=" + getName()
				+ ", getIdsList()=" + getJsonData() + ", getCreationDate()=" + getCreationDate() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idUtente == null) ? 0 : idUtente.hashCode());
		result = prime * result + ((jsonData == null) ? 0 : jsonData.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ReportIdsBean)) {
			return false;
		}
		ReportIdsBean other = (ReportIdsBean) obj;
		return this.toString().equals(other.toString());
	}
}
