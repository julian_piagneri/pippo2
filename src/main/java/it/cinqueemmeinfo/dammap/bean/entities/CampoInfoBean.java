package it.cinqueemmeinfo.dammap.bean.entities;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

public class CampoInfoBean implements Serializable {

	private static final long serialVersionUID = 6498063362463067114L;
	/**
	 * Hashmap<String (ID Oggetto), HashMap<String(nome campo), CampoInfo>>
	 * 
	 * CampoInfo contiene una lista di valori
	 * 
	 * 
	 */

	private long id;
	private String nome;
	private String unita;
	private boolean numeric;
	private Set<InfoExtraBean> values = new LinkedHashSet<InfoExtraBean>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUnita() {
		return unita;
	}

	public void setUnita(String unita) {
		this.unita = unita;
	}

	public boolean isNumeric() {
		return numeric;
	}

	public void setNumeric(boolean numeric) {
		this.numeric = numeric;
	}

	public Set<InfoExtraBean> getValues() {
		return values;
	}

	public void setValues(Set<InfoExtraBean> values) {
		this.values = values;
	}

	public void addValue(InfoExtraBean value) {
		this.getValues().add(value);
	}

}
