package it.cinqueemmeinfo.dammap.bean.entities;

public class InfoFiumeExtraBean extends InfoExtraBean {
	
	private static final long serialVersionUID = -646169082289834560L;
	private long fiume;

	public long getFiume() {
		return fiume;
	}

	public void setFiume(long fiume) {
		this.fiume = fiume;
	}

	@Override
	public String getIdentifier() {
		return new Long(fiume).toString();
	}

}
