package it.cinqueemmeinfo.dammap.bean.entities;

import java.io.Serializable;


public class ReportTemplateBean implements Serializable {

	private static final long serialVersionUID = 1233647382902762145L;
	
	private Long id;
	private String name;
	private String url;
	private String template;
	
	public ReportTemplateBean() { }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((template == null) ? 0 : template.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ReportTemplateBean)) {
			return false;
		}
		ReportTemplateBean other = (ReportTemplateBean) obj;
		return this.toString().equals(other.toString());
	}

	@Override
	public String toString() {
		return "ReportTemplateBean [getId()=" + getId() + ", getName()=" + getName() + ", getUrl()=" + getUrl()
				+ ", getTemplate()=" + getTemplate() + "]";
	}
}
