package it.cinqueemmeinfo.dammap.bean.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;

public class FiumeBean implements Serializable {
	
	private static final long serialVersionUID = -1262509465564398090L;
	
	private long id;
	private String IdUtente;
	private Timestamp dataModifica;
	private String cancellato;
	private String idBacino;
	private String idVersante;
	private String idTipoFiume;
	private long idFiumePrincipale;
	private String nome;
	private int ordine;
	private LinkedHashMap<String, CampoInfoBean> extraInfo=new LinkedHashMap<String, CampoInfoBean>();
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getIdUtente() {
		return IdUtente;
	}
	
	public void setIdUtente(String idUtente) {
		IdUtente = idUtente;
	}
	
	public Timestamp getDataModifica() {
		return dataModifica;
	}
	
	public void setDataModifica(Timestamp dataModifica) {
		this.dataModifica = dataModifica;
	}
	
	public String getCancellato() {
		return cancellato;
	}
	
	public void setCancellato(String cancellato) {
		this.cancellato = cancellato;
	}
	
	public String getIdBacino() {
		return idBacino;
	}
	
	public void setIdBacino(String idBacino) {
		this.idBacino = idBacino;
	}
	
	public String getIdVersante() {
		return idVersante;
	}
	
	public void setIdVersante(String idVersante) {
		this.idVersante = idVersante;
	}
	
	public String getIdTipoFiume() {
		return idTipoFiume;
	}
	
	public void setIdTipoFiume(String idTipoFiume) {
		this.idTipoFiume = idTipoFiume;
	}
	
	public long getIdFiumePrincipale() {
		return idFiumePrincipale;
	}
	
	public void setIdFiumePrincipale(long idFiumePrincipale) {
		this.idFiumePrincipale = idFiumePrincipale;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getOrdine() {
		return ordine;
	}
	
	public void setOrdine(int ordine) {
		this.ordine = ordine;
	}
	
	public LinkedHashMap<String, CampoInfoBean> getExtraInfo() {
		return extraInfo;
	}
	
	public void setExtraInfo(LinkedHashMap<String, CampoInfoBean> extraInfo) {
		this.extraInfo = extraInfo;
	}
	
	@Override
	public String toString() {
		return "FiumeBean [getId()=" + getId() + ", getIdUtente()="
				+ getIdUtente() + ", getDataModifica()=" + getDataModifica()
				+ ", getCancellato()=" + getCancellato() + ", getIdBacino()="
				+ getIdBacino() + ", getIdVersante()=" + getIdVersante()
				+ ", getIdTipoFiume()=" + getIdTipoFiume()
				+ ", getIdFiumePrincipale()=" + getIdFiumePrincipale()
				+ ", getNome()=" + getNome() + ", getOrdine()=" + getOrdine()
				+ ", getExtraInfo()=" + getExtraInfo() + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((IdUtente == null) ? 0 : IdUtente.hashCode());
		result = prime * result
				+ ((cancellato == null) ? 0 : cancellato.hashCode());
		result = prime * result
				+ ((dataModifica == null) ? 0 : dataModifica.hashCode());
		result = prime * result
				+ ((extraInfo == null) ? 0 : extraInfo.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result
				+ ((idBacino == null) ? 0 : idBacino.hashCode());
		result = prime * result
				+ (int) (idFiumePrincipale ^ (idFiumePrincipale >>> 32));
		result = prime * result
				+ ((idTipoFiume == null) ? 0 : idTipoFiume.hashCode());
		result = prime * result
				+ ((idVersante == null) ? 0 : idVersante.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ordine;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof FiumeBean))
			return false;
		
		return this.toString().equals(obj.toString());
	}
	
}
