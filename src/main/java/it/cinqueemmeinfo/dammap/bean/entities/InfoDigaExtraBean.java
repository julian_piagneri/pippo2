package it.cinqueemmeinfo.dammap.bean.entities;

public class InfoDigaExtraBean extends InfoExtraBean {

	private static final long serialVersionUID = 764899585346624632L;
	private String archivio;
	private String sub;
	
	public String getArchivio() {
		return archivio;
	}
	public void setArchivio(String archivio) {
		this.archivio = archivio;
	}
	public String getSub() {
		return sub;
	}
	public void setSub(String sub) {
		this.sub = sub;
	}
	@Override
	public String getIdentifier() {
		return this.getArchivio()+this.getSub();
	}

}
