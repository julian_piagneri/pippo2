package it.cinqueemmeinfo.dammap.bean.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class MenuBean implements Serializable {

	private static final long serialVersionUID = 1233647382902762145L;
	
	private Long idScheda;
	private Long idGruppo;
	private Long idPadre;
	private String descr;
	private String descrExt;
	private Long tipo;
	private Long ordine;
	private String href;
	private Long type;
	
	private String visibile;
	private String tipoPermesso;
	private String isScheda;
	
	private List<MenuBean> figli=new ArrayList<MenuBean>();
	
	public MenuBean() { }

	public MenuBean(Long idScheda, Long idGruppo, Long idPadre, String descr,
			String descrExt, Long tipo, Long ordine, String visibile,
			String tipoPermesso, String visibileMenu, String href) {
		this.idScheda = idScheda;
		this.idGruppo = idGruppo;
		this.idPadre = idPadre;
		this.descr = descr;
		this.descrExt = descrExt;
		this.tipo = tipo;
		this.ordine = ordine;
		this.visibile = visibile;
		this.tipoPermesso = tipoPermesso;
		this.isScheda = visibileMenu;
		this.href=href;
	}

	public Long getIdScheda() {
		return idScheda;
	}

	public void setIdScheda(Long idScheda) {
		this.idScheda = idScheda;
	}

	public Long getIdGruppo() {
		return idGruppo;
	}

	public void setIdGruppo(Long idGruppo) {
		this.idGruppo = idGruppo;
	}

	public Long getIdPadre() {
		return idPadre;
	}

	public void setIdPadre(Long idPadre) {
		this.idPadre = idPadre;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getDescrExt() {
		return descrExt;
	}

	public void setDescrExt(String descrExt) {
		this.descrExt = descrExt;
	}

	public Long getTipo() {
		return tipo;
	}

	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}

	public Long getOrdine() {
		return ordine;
	}

	public void setOrdine(Long ordine) {
		this.ordine = ordine;
	}

	public String getVisibile() {
		return visibile;
	}

	public void setVisibile(String visibile) {
		this.visibile = visibile;
	}

	public String getTipoPermesso() {
		return tipoPermesso;
	}

	public void setTipoPermesso(String tipoPermesso) {
		this.tipoPermesso = tipoPermesso;
	}

	public String getIsScheda() {
		return isScheda;
	}

	public void setIsScheda(String isScheda) {
		this.isScheda = isScheda;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public List<MenuBean> getFigli() {
		return figli;
	}

	public void setFigli(List<MenuBean> figli) {
		this.figli = figli;
	}
	
	public void addFiglio(MenuBean figlio){
		this.figli.add(figlio);
	}

	@Override
	public String toString() {
		return "MenuBean [idScheda=" + idScheda + ", idGruppo=" + idGruppo
				+ ", idPadre=" + idPadre + ", descr=" + descr + ", descrExt="
				+ descrExt + ", tipo=" + tipo + ", ordine=" + ordine
				+ ", href=" + href + ", visibile=" + visibile
				+ ", tipoPermesso=" + tipoPermesso + ", isScheda=" + isScheda
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descr == null) ? 0 : descr.hashCode());
		result = prime * result
				+ ((descrExt == null) ? 0 : descrExt.hashCode());
		result = prime * result
				+ ((idGruppo == null) ? 0 : idGruppo.hashCode());
		result = prime * result + ((idPadre == null) ? 0 : idPadre.hashCode());
		result = prime * result
				+ ((idScheda == null) ? 0 : idScheda.hashCode());
		result = prime * result + ((ordine == null) ? 0 : ordine.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		result = prime * result
				+ ((tipoPermesso == null) ? 0 : tipoPermesso.hashCode());
		result = prime * result
				+ ((visibile == null) ? 0 : visibile.hashCode());
		result = prime * result
				+ ((isScheda == null) ? 0 : isScheda.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null){
			return false;
		}
		
		if (this == obj){
			return true;
		}
		
		if (!(obj instanceof MenuBean)){
			return false;
		}
		return this.toString().equals(obj.toString());
	}


}
