package it.cinqueemmeinfo.dammap.bean.entities;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

public class CasaDiGuardiaBean extends BasicEntityBean {

	private static final long serialVersionUID = 4502383542966410266L;

	private Integer numeroArchivio;
	private String sub;
	private String telefono;
	private String telefonoPostoPresidiato;
	private String telefonoCantiere;

	public CasaDiGuardiaBean() {
	}

	public CasaDiGuardiaBean(String id, Integer numeroArchivio, String sub, String telefono,
			String telefonoPostoPresidiato, String telefonoCantiere, String lastModifiedDate, String lastModifiedUserId,
			String isDeleted) {

		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.numeroArchivio = numeroArchivio;
		this.sub = sub;
		this.telefono = telefono;
		this.telefonoPostoPresidiato = telefonoPostoPresidiato;
		this.telefonoCantiere = telefonoCantiere;
	}

	public Integer getNumeroArchivio() {
		return numeroArchivio;
	}

	public void setNumeroArchivio(Integer numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefonoPostoPresidiato() {
		return telefonoPostoPresidiato;
	}

	public void setTelefonoPostoPresidiato(String telefonoPostoPresidiato) {
		this.telefonoPostoPresidiato = telefonoPostoPresidiato;
	}

	public String getTelefonoCantiere() {
		return telefonoCantiere;
	}

	public void setTelefonoCantiere(String telefonoCantiere) {
		this.telefonoCantiere = telefonoCantiere;
	}
}
