package it.cinqueemmeinfo.dammap.bean.entities;

import java.util.ArrayList;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;

public class AltraEnteBean extends BasicEntityBean {

	private static final long serialVersionUID = 1176901069031054269L;

	private String nome;
	private Integer tipoEnte;
	private ArrayList<AddressBean> indirizzo;
	private ArrayList<ContactInfoBean> contatto;
	
	public AltraEnteBean() {
		this.indirizzo = new ArrayList<AddressBean>();
		this.contatto = new ArrayList<ContactInfoBean>();
	}
	
	public AltraEnteBean(String id) {
		super(id, "", "", "");
		this.nome = "";
		this.tipoEnte = null;
		this.indirizzo = new ArrayList<AddressBean>();
		this.contatto = new ArrayList<ContactInfoBean>();
	}

	public AltraEnteBean(String id, String nome, Integer tipoEnte, ArrayList<AddressBean> indirizzo,
						ArrayList<ContactInfoBean> contatto, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.nome = nome;
		this.tipoEnte = tipoEnte;
		this.indirizzo = indirizzo;
		this.contatto = contatto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getTipoEnte() {
		return tipoEnte;
	}

	public void setTipoEnte(Integer tipoEnte) {
		this.tipoEnte = tipoEnte;
	}

	public ArrayList<AddressBean> getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(ArrayList<AddressBean> indirizzo) {
		this.indirizzo = indirizzo;
	}

	public ArrayList<ContactInfoBean> getContatto() {
		return contatto;
	}

	public void setContatto(ArrayList<ContactInfoBean> contatto) {
		this.contatto = contatto;
	}

	@Override
	public String toString() {
		return "AltraEnteBean [getNome()=" + getNome() + ", getTipoEnte()="
				+ getTipoEnte() + ", getIndirizzo()=" + getIndirizzo()
				+ ", getContatto()=" + getContatto() + ", getId()=" + getId()
				+ ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", getLastModifiedUserId()=" + getLastModifiedUserId()
				+ ", getIsDeleted()=" + getIsDeleted() + "]";
	}
}
