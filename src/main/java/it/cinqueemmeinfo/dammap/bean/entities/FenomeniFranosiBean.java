package it.cinqueemmeinfo.dammap.bean.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Bean per la scheda Fenomeni Franosi
 * 
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
public class FenomeniFranosiBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = -2867918940934918225L;

	private List<FenomeniFranosiGruppoBean> fenDigheGruppi;

	/**
	 * @return the fenDigheGruppi
	 */
	public List<FenomeniFranosiGruppoBean> getFenDigheGruppi() {
		return fenDigheGruppi;
	}

	/**
	 * @param fenDigheGruppi
	 *            the fenDigheGruppi to set
	 */
	public void setFenDigheGruppi(List<FenomeniFranosiGruppoBean> fenDigheGruppi) {
		this.fenDigheGruppi = fenDigheGruppi;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fenDigheGruppi == null) ? 0 : fenDigheGruppi.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FenomeniFranosiBean)) {
			return false;
		}
		FenomeniFranosiBean other = (FenomeniFranosiBean) obj;
		if (fenDigheGruppi == null) {
			if (other.fenDigheGruppi != null) {
				return false;
			}
		} else if (!fenDigheGruppi.equals(other.fenDigheGruppi)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FenomeniFranosiBean [fenDigheGruppi=" + fenDigheGruppi + "]";
	}
}