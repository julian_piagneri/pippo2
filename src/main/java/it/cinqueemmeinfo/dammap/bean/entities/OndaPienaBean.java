package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class OndaPienaBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 4057693296117476057L;

	private String dataStudio;
	private String dataEsame_ParereDGdighe;
	private String esitoParere;
	private String dataPresaVisione;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long idTipo;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double portata;

	public OndaPienaBean() {
		super();
		setDataEsame_ParereDGdighe(null);
		setDataPresaVisione(null);
		setDataStudio(null);
		setEsitoParere(null);
		setIdTipo(null);
		setPortata(null);
	}

	/**
	 * @return the dataStudio
	 */
	public String getDataStudio() {
		return dataStudio;
	}

	/**
	 * @param dataStudio
	 *            the dataStudio to set
	 */
	public void setDataStudio(String dataStudio) {
		this.dataStudio = dataStudio;
	}

	/**
	 * @return the dataEsame_ParereDGdighe
	 */
	public String getDataEsame_ParereDGdighe() {
		return dataEsame_ParereDGdighe;
	}

	/**
	 * @param dataEsame_ParereDGdighe
	 *            the dataEsame_ParereDGdighe to set
	 */
	public void setDataEsame_ParereDGdighe(String dataEsame_ParereDGdighe) {
		this.dataEsame_ParereDGdighe = dataEsame_ParereDGdighe;
	}

	/**
	 * @return the esitoParere
	 */
	public String getEsitoParere() {
		return esitoParere;
	}

	/**
	 * @param esitoParere
	 *            the esitoParere to set
	 */
	public void setEsitoParere(String esitoParere) {
		this.esitoParere = esitoParere;
	}

	/**
	 * @return the dataPresaVisione
	 */
	public String getDataPresaVisione() {
		return dataPresaVisione;
	}

	/**
	 * @param dataPresaVisione
	 *            the dataPresaVisione to set
	 */
	public void setDataPresaVisione(String dataPresaVisione) {
		this.dataPresaVisione = dataPresaVisione;
	}

	/**
	 * @return the idTipo
	 */
	public Long getIdTipo() {
		return idTipo;
	}

	/**
	 * @param idTipo
	 *            the idTipo to set
	 */
	public void setIdTipo(Long idTipo) {
		this.idTipo = idTipo;
	}

	/**
	 * @return the portata
	 */
	public Double getPortata() {
		return portata;
	}

	/**
	 * @param portata
	 *            the portata to set
	 */
	public void setPortata(Double portata) {
		this.portata = portata;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataEsame_ParereDGdighe == null) ? 0 : dataEsame_ParereDGdighe.hashCode());
		result = prime * result + ((dataPresaVisione == null) ? 0 : dataPresaVisione.hashCode());
		result = prime * result + ((dataStudio == null) ? 0 : dataStudio.hashCode());
		result = prime * result + ((esitoParere == null) ? 0 : esitoParere.hashCode());
		result = prime * result + ((idTipo == null) ? 0 : idTipo.hashCode());
		result = prime * result + ((portata == null) ? 0 : portata.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof OndaPienaBean)) {
			return false;
		}
		OndaPienaBean other = (OndaPienaBean) obj;
		if (dataEsame_ParereDGdighe == null) {
			if (other.dataEsame_ParereDGdighe != null) {
				return false;
			}
		} else if (!dataEsame_ParereDGdighe.equals(other.dataEsame_ParereDGdighe)) {
			return false;
		}
		if (dataPresaVisione == null) {
			if (other.dataPresaVisione != null) {
				return false;
			}
		} else if (!dataPresaVisione.equals(other.dataPresaVisione)) {
			return false;
		}
		if (dataStudio == null) {
			if (other.dataStudio != null) {
				return false;
			}
		} else if (!dataStudio.equals(other.dataStudio)) {
			return false;
		}
		if (esitoParere == null) {
			if (other.esitoParere != null) {
				return false;
			}
		} else if (!esitoParere.equals(other.esitoParere)) {
			return false;
		}
		if (idTipo == null) {
			if (other.idTipo != null) {
				return false;
			}
		} else if (!idTipo.equals(other.idTipo)) {
			return false;
		}
		if (portata == null) {
			if (other.portata != null) {
				return false;
			}
		} else if (!portata.equals(other.portata)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OndePienaBean [dataStudio=" + dataStudio + ", dataEsame_ParereDGdighe=" + dataEsame_ParereDGdighe + ", esitoParere=" + esitoParere
				+ ", dataPresaVisione=" + dataPresaVisione + ", idTipo=" + idTipo + ", portata=" + portata + "]";
	}
}