package it.cinqueemmeinfo.dammap.bean.entities;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

public class AltreVociRubricaBean extends BasicEntityBean {

	private static final long serialVersionUID = 7422490126075548459L;
	
	//id = numeroarchivio+sub
	private Integer progressivo;
	private Integer numeroArchivio;
	private String sub;
	private String ente;
	private String telefono;
	private String fax;
	private Integer idTipoEnte;
	
	public AltreVociRubricaBean() { }
	
	public AltreVociRubricaBean(String id, Integer progressivo,
								Integer numeroArchivio, String sub, String ente, String telefono,
								String fax, Integer idTipoEnte, String lastModifiedDate,
								String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.progressivo = progressivo;
		this.numeroArchivio = numeroArchivio;
		this.sub = sub;
		this.ente = ente;
		this.telefono = telefono;
		this.fax = fax;
		this.idTipoEnte = idTipoEnte;
	}

	public Integer getProgressivo() {
		return progressivo;
	}

	public void setProgressivo(Integer progressivo) {
		this.progressivo = progressivo;
	}

	public Integer getNumeroArchivio() {
		return numeroArchivio;
	}

	public void setNumeroArchivio(Integer numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getEnte() {
		return ente;
	}

	public void setEnte(String ente) {
		this.ente = ente;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Integer getIdTipoEnte() {
		return idTipoEnte;
	}

	public void setIdTipoEnte(Integer idTipoEnte) {
		this.idTipoEnte = idTipoEnte;
	}
	
	
}
