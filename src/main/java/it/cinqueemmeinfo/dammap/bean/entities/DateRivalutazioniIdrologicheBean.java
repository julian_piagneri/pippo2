package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * Bean related to 'Scheda Rivalutazione idrologico-idraulica'
 * 
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DateRivalutazioniIdrologicheBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 7221722675871972661L;

	private String nomeDiga;
	private String dataVerificaIdrologica;
	private String dataArrivoUtd;
	private String dataArrivoDG;
	private String dataParereUIDR;
	private String dataPrescrizione;
	private String note;
	private String origineRivalutazione;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Integer idOrigineRivalutazione;

	/**
	 * @return the nomeDiga
	 */
	public String getNomeDiga() {
		return nomeDiga;
	}

	/**
	 * @param nomeDiga
	 *            the nomeDiga to set
	 */
	public void setNomeDiga(String nomeDiga) {
		this.nomeDiga = nomeDiga;
	}

	/**
	 * @return the dataVerificaIdrologica
	 */
	public String getDataVerificaIdrologica() {
		return dataVerificaIdrologica;
	}

	/**
	 * @param dataVerificaIdrologica
	 *            the dataVerificaIdrologica to set
	 */
	public void setDataVerificaIdrologica(String dataVerificaIdrologica) {
		this.dataVerificaIdrologica = dataVerificaIdrologica;
	}

	/**
	 * @return the dataArrivoUtd
	 */
	public String getDataArrivoUtd() {
		return dataArrivoUtd;
	}

	/**
	 * @param dataArrivoUtd
	 *            the dataArrivoUtd to set
	 */
	public void setDataArrivoUtd(String dataArrivoUtd) {
		this.dataArrivoUtd = dataArrivoUtd;
	}

	/**
	 * @return the dataArrivoDG
	 */
	public String getDataArrivoDG() {
		return dataArrivoDG;
	}

	/**
	 * @param dataArrivoDG
	 *            the dataArrivoDG to set
	 */
	public void setDataArrivoDG(String dataArrivoDG) {
		this.dataArrivoDG = dataArrivoDG;
	}

	/**
	 * @return the dataParereUIDR
	 */
	public String getDataParereUIDR() {
		return dataParereUIDR;
	}

	/**
	 * @param dataParereUIDR
	 *            the dataParereUIDR to set
	 */
	public void setDataParereUIDR(String dataParereUIDR) {
		this.dataParereUIDR = dataParereUIDR;
	}

	/**
	 * @return the dataPrescrizione
	 */
	public String getDataPrescrizione() {
		return dataPrescrizione;
	}

	/**
	 * @param dataPrescrizione
	 *            the dataPrescrizione to set
	 */
	public void setDataPrescrizione(String dataPrescrizione) {
		this.dataPrescrizione = dataPrescrizione;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the origineRivalutazione
	 */
	public String getOrigineRivalutazione() {
		return origineRivalutazione;
	}

	/**
	 * @param origineRivalutazione
	 *            the origineRivalutazione to set
	 */
	public void setOrigineRivalutazione(String origineRivalutazione) {
		this.origineRivalutazione = origineRivalutazione;
	}

	/**
	 * @return the idOrigineRivalutazione
	 */
	public Integer getIdOrigineRivalutazione() {
		return idOrigineRivalutazione;
	}

	/**
	 * @param idOrigineRivalutazione
	 *            the idOrigineRivalutazione to set
	 */
	public void setIdOrigineRivalutazione(Integer idOrigineRivalutazione) {
		this.idOrigineRivalutazione = idOrigineRivalutazione;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataArrivoDG == null) ? 0 : dataArrivoDG.hashCode());
		result = prime * result + ((dataArrivoUtd == null) ? 0 : dataArrivoUtd.hashCode());
		result = prime * result + ((dataParereUIDR == null) ? 0 : dataParereUIDR.hashCode());
		result = prime * result + ((dataPrescrizione == null) ? 0 : dataPrescrizione.hashCode());
		result = prime * result + ((dataVerificaIdrologica == null) ? 0 : dataVerificaIdrologica.hashCode());
		result = prime * result + ((idOrigineRivalutazione == null) ? 0 : idOrigineRivalutazione.hashCode());
		result = prime * result + ((nomeDiga == null) ? 0 : nomeDiga.hashCode());
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((origineRivalutazione == null) ? 0 : origineRivalutazione.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof DateRivalutazioniIdrologicheBean)) {
			return false;
		}
		DateRivalutazioniIdrologicheBean other = (DateRivalutazioniIdrologicheBean) obj;
		if (dataArrivoDG == null) {
			if (other.dataArrivoDG != null) {
				return false;
			}
		} else if (!dataArrivoDG.equals(other.dataArrivoDG)) {
			return false;
		}
		if (dataArrivoUtd == null) {
			if (other.dataArrivoUtd != null) {
				return false;
			}
		} else if (!dataArrivoUtd.equals(other.dataArrivoUtd)) {
			return false;
		}
		if (dataParereUIDR == null) {
			if (other.dataParereUIDR != null) {
				return false;
			}
		} else if (!dataParereUIDR.equals(other.dataParereUIDR)) {
			return false;
		}
		if (dataPrescrizione == null) {
			if (other.dataPrescrizione != null) {
				return false;
			}
		} else if (!dataPrescrizione.equals(other.dataPrescrizione)) {
			return false;
		}
		if (dataVerificaIdrologica == null) {
			if (other.dataVerificaIdrologica != null) {
				return false;
			}
		} else if (!dataVerificaIdrologica.equals(other.dataVerificaIdrologica)) {
			return false;
		}
		if (idOrigineRivalutazione == null) {
			if (other.idOrigineRivalutazione != null) {
				return false;
			}
		} else if (!idOrigineRivalutazione.equals(other.idOrigineRivalutazione)) {
			return false;
		}
		if (nomeDiga == null) {
			if (other.nomeDiga != null) {
				return false;
			}
		} else if (!nomeDiga.equals(other.nomeDiga)) {
			return false;
		}
		if (note == null) {
			if (other.note != null) {
				return false;
			}
		} else if (!note.equals(other.note)) {
			return false;
		}
		if (origineRivalutazione == null) {
			if (other.origineRivalutazione != null) {
				return false;
			}
		} else if (!origineRivalutazione.equals(other.origineRivalutazione)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DateRivalutazioniIdrologicheBean [nomeDiga=" + nomeDiga + ", dataVerificaIdrologica=" + dataVerificaIdrologica + ", dataArrivoUtd="
				+ dataArrivoUtd + ", dataArrivoDG=" + dataArrivoDG + ", dataParereUIDR=" + dataParereUIDR + ", dataPrescrizione=" + dataPrescrizione + ", note="
				+ note + ", origineRivalutazione=" + origineRivalutazione + ", idOrigineRivalutazione=" + idOrigineRivalutazione + "]";
	}
}
