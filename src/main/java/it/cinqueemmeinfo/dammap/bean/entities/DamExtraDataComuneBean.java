package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DamExtraDataComuneBean extends BasicEntityBean {

	private static final long serialVersionUID = -8615709861922850835L;

	private String numeroArchivio;
	private String sub;
	private String group;
	private String nomeComune=""; //NOME_COMUNE
	private String nomeProvincia=""; //NOME_PROVINCIA
	private String nomeRegione=""; //NOME_REGIONE
	private String zonaSismicaComune=""; //ZONA_SISMICA_COMUNE
	private boolean init=false; //Controlla che ci siano dei dati
	
	public DamExtraDataComuneBean() { }

	public String getNumeroArchivio() {
		return numeroArchivio;
	}

	public void setNumeroArchivio(String numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getNomeComune() {
		return nomeComune;
	}

	public void setNomeComune(String nomeComune) {
		if(nomeComune!=null){
			this.setInit(true);
			this.nomeComune = nomeComune;
		}
	}

	public String getNomeProvincia() {
		return nomeProvincia;
	}

	public void setNomeProvincia(String nomeProvincia) {
		if(nomeProvincia!=null){
			this.setInit(true);
			this.nomeProvincia = nomeProvincia;
		}
	}

	public String getNomeRegione() {
		return nomeRegione;
	}

	public void setNomeRegione(String nomeRegione) {
		if(nomeRegione!=null){
			this.setInit(true);
			this.nomeRegione = nomeRegione;
		}
	}

	public String getZonaSismicaComune() {
		return zonaSismicaComune;
	}

	public void setZonaSismicaComune(String zonaSismicaComune) {
		if(zonaSismicaComune!=null){
			this.setInit(true);
			this.zonaSismicaComune = zonaSismicaComune;
		}
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public boolean isInit() {
		return init;
	}

	private void setInit(boolean init) {
		this.init = init;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((nomeComune == null) ? 0 : nomeComune.hashCode());
		result = prime * result
				+ ((nomeProvincia == null) ? 0 : nomeProvincia.hashCode());
		result = prime * result
				+ ((nomeRegione == null) ? 0 : nomeRegione.hashCode());
		result = prime * result
				+ ((numeroArchivio == null) ? 0 : numeroArchivio.hashCode());
		result = prime * result + ((sub == null) ? 0 : sub.hashCode());
		result = prime
				* result
				+ ((zonaSismicaComune == null) ? 0 : zonaSismicaComune
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DamExtraDataComuneBean))
			return false;
		DamExtraDataComuneBean other = (DamExtraDataComuneBean) obj;
		return this.toString().equals(other.toString());
	}

	@Override
	public String toString() {
		return "DamExtraDataComuneBean [getNumeroArchivio()="
				+ getNumeroArchivio() + ", getSub()=" + getSub()
				+ ", getNomeComune()=" + getNomeComune()
				+ ", getNomeProvincia()=" + getNomeProvincia()
				+ ", getNomeRegione()=" + getNomeRegione()
				+ ", getZonaSismicaComune()=" + getZonaSismicaComune()
				+ ", hashCode()=" + hashCode() + "]";
	}
}
