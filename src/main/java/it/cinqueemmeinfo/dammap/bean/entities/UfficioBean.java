package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

public class UfficioBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 1800239234022608838L;

	private String tipo;
	private String descrizione;
	private String indirizzo;
	private String telefono;
	private String fax;
	// private ArrayList<FunzionarioBean> dirigente;
	// private ArrayList<FunzionarioBean> funzionario;
	private FunzionarioBean dirigente;
	private FunzionarioBean funzionario;

	public UfficioBean() {
		// this.dirigente = new ArrayList<FunzionarioBean>();
		// this.funzionario = new ArrayList<FunzionarioBean>();
	}

	public UfficioBean(String id) {
		super(id, "", "", "");
		// this.dirigente = new ArrayList<FunzionarioBean>();
		// this.funzionario = new ArrayList<FunzionarioBean>();
	}

	public UfficioBean(String id, String tipo, String descrizione, String indirizzo, String telefono, String fax, FunzionarioBean dirigente,
			FunzionarioBean funzionario, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {

		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.tipo = tipo;
		this.descrizione = descrizione;
		this.indirizzo = indirizzo;
		this.telefono = telefono;
		this.fax = fax;
		this.dirigente = dirigente;
		this.funzionario = funzionario;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @param descrizione
	 *            the descrizione to set
	 */
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * @return the indirizzo
	 */
	public String getIndirizzo() {
		return indirizzo;
	}

	/**
	 * @param indirizzo
	 *            the indirizzo to set
	 */
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono
	 *            the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the dirigente
	 */
	public FunzionarioBean getDirigente() {
		return dirigente;
	}

	/**
	 * @param dirigente
	 *            the dirigente to set
	 */
	public void setDirigente(FunzionarioBean dirigente) {
		this.dirigente = dirigente;
	}

	/**
	 * @return the funzionario
	 */
	public FunzionarioBean getFunzionario() {
		return funzionario;
	}

	/**
	 * @param funzionario
	 *            the funzionario to set
	 */
	public void setFunzionario(FunzionarioBean funzionario) {
		this.funzionario = funzionario;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((descrizione == null) ? 0 : descrizione.hashCode());
		result = prime * result + ((dirigente == null) ? 0 : dirigente.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result + ((funzionario == null) ? 0 : funzionario.hashCode());
		result = prime * result + ((indirizzo == null) ? 0 : indirizzo.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof UfficioBean)) {
			return false;
		}
		UfficioBean other = (UfficioBean) obj;
		if (descrizione == null) {
			if (other.descrizione != null) {
				return false;
			}
		} else if (!descrizione.equals(other.descrizione)) {
			return false;
		}
		if (dirigente == null) {
			if (other.dirigente != null) {
				return false;
			}
		} else if (!dirigente.equals(other.dirigente)) {
			return false;
		}
		if (fax == null) {
			if (other.fax != null) {
				return false;
			}
		} else if (!fax.equals(other.fax)) {
			return false;
		}
		if (funzionario == null) {
			if (other.funzionario != null) {
				return false;
			}
		} else if (!funzionario.equals(other.funzionario)) {
			return false;
		}
		if (indirizzo == null) {
			if (other.indirizzo != null) {
				return false;
			}
		} else if (!indirizzo.equals(other.indirizzo)) {
			return false;
		}
		if (telefono == null) {
			if (other.telefono != null) {
				return false;
			}
		} else if (!telefono.equals(other.telefono)) {
			return false;
		}
		if (tipo == null) {
			if (other.tipo != null) {
				return false;
			}
		} else if (!tipo.equals(other.tipo)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UfficioBean [tipo=" + tipo + ", descrizione=" + descrizione + ", indirizzo=" + indirizzo + ", telefono=" + telefono + ", fax=" + fax
				+ ", dirigente=" + dirigente + ", funzionario=" + funzionario + "]";
	}
}