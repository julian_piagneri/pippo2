package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DamExtraDataBean extends BasicEntityBean {

	private static final long serialVersionUID = -8615709861922850835L;

	private String numeroArchivio;
	private String sub;
	private String nomeComune; // NOME_COMUNE
	private String nomeProvincia; // NOME_PROVINCIA
	private String nomeRegione; // NOME_REGIONE
	private String comuneOperaPresa; // COMUNE_OPERA_PRESA
	private String zonaSismicaComune; // ZONA_SISMICA_COMUNE
	private String nomeFiumeValle; // NOME_FIUME_VALLE
	private String tipoOrganoScarico; // TIPO_ORGANO_SCARICO
	private String altraDenominaz; // ALTRA_DENOMINAZIONE
	private String utilizzo; // UTILIZZAZIONE
	private String priorita; // PRIORITA
	private String digheInvaso; // DIGHE_INVASO

	public DamExtraDataBean() {
	}

	public DamExtraDataBean(String id, String numeroArchivio, String sub) {
		super(id);
		this.numeroArchivio = numeroArchivio;
		this.sub = sub;
	}

	public DamExtraDataBean(String id, String numeroArchivio, String sub, String nomeComune, String nomeProvincia,
			String nomeRegione, String comuneOperaPresa, String zonaSismicaComune, String nomeFiumeValle,
			String tipoOrganoScarico, String altraDenominaz, String utilizzo, String priorita, String digheInvaso) {

		super(id);
		this.numeroArchivio = numeroArchivio;
		this.sub = sub;
		this.nomeComune = nomeComune;
		this.nomeProvincia = nomeProvincia;
		this.nomeRegione = nomeRegione;
		this.comuneOperaPresa = comuneOperaPresa;
		this.zonaSismicaComune = zonaSismicaComune;
		this.nomeFiumeValle = nomeFiumeValle;
		this.tipoOrganoScarico = tipoOrganoScarico;
		this.altraDenominaz = altraDenominaz;
		this.utilizzo = utilizzo;
		this.priorita = priorita;
		this.digheInvaso = digheInvaso;
	}

	public String getNumeroArchivio() {
		return numeroArchivio;
	}

	public void setNumeroArchivio(String numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getComuneOperaPresa() {
		return comuneOperaPresa;
	}

	public void setComuneOperaPresa(String comuneOperaPresa) {
		this.comuneOperaPresa = comuneOperaPresa;
	}

	public String getNomeComune() {
		return nomeComune;
	}

	public void setNomeComune(String nomeComune) {
		this.nomeComune = nomeComune;
	}

	public String getNomeProvincia() {
		return nomeProvincia;
	}

	public void setNomeProvincia(String nomeProvincia) {
		this.nomeProvincia = nomeProvincia;
	}

	public String getNomeRegione() {
		return nomeRegione;
	}

	public void setNomeRegione(String nomeRegione) {
		this.nomeRegione = nomeRegione;
	}

	public String getZonaSismicaComune() {
		return zonaSismicaComune;
	}

	public void setZonaSismicaComune(String zonaSismicaComune) {
		this.zonaSismicaComune = zonaSismicaComune;
	}

	public String getNomeFiumeValle() {
		return nomeFiumeValle;
	}

	public void setNomeFiumeValle(String nomeFiumeValle) {
		this.nomeFiumeValle = nomeFiumeValle;
	}

	public String getTipoOrganoScarico() {
		return tipoOrganoScarico;
	}

	public void setTipoOrganoScarico(String tipoOrganoScarico) {
		this.tipoOrganoScarico = tipoOrganoScarico;
	}

	public String getAltraDenominaz() {
		return altraDenominaz;
	}

	public void setAltraDenominaz(String altraDenominaz) {
		this.altraDenominaz = altraDenominaz;
	}

	public String getUtilizzo() {
		return utilizzo;
	}

	public void setUtilizzo(String utilizzo) {
		this.utilizzo = utilizzo;
	}

	public String getPriorita() {
		return priorita;
	}

	public void setPriorita(String priorita) {
		this.priorita = priorita;
	}

	public String getDigheInvaso() {
		return digheInvaso;
	}

	public void setDigheInvaso(String digheInvaso) {
		this.digheInvaso = digheInvaso;
	}
}
