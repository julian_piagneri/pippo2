package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PortateRivalutateBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 2195060001093542481L;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double portataAfflusso;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double portataLaminata;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double quotaInvaso;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double franco;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long tempoDiRitorno;

	/**
	 * 
	 */
	public PortateRivalutateBean() {
		this(null, null, null, null, null);
	}

	/**
	 * @param portataAfflusso
	 * @param portataLaminata
	 * @param quotaInvaso
	 * @param franco
	 * @param tempoDiRitorno
	 */
	protected PortateRivalutateBean(Double portataAfflusso, Double portataLaminata, Double quotaInvaso, Double franco, Long tempoDiRitorno) {
		super();
		this.portataAfflusso = portataAfflusso;
		this.portataLaminata = portataLaminata;
		this.quotaInvaso = quotaInvaso;
		this.franco = franco;
		this.tempoDiRitorno = tempoDiRitorno;
	}

	/**
	 * @return the portataAfflusso
	 */
	public Double getPortataAfflusso() {
		return portataAfflusso;
	}

	/**
	 * @param portataAfflusso
	 *            the portataAfflusso to set
	 */
	public void setPortataAfflusso(Double portataAfflusso) {
		this.portataAfflusso = portataAfflusso;
	}

	/**
	 * @return the portataLaminata
	 */
	public Double getPortataLaminata() {
		return portataLaminata;
	}

	/**
	 * @param portataLaminata
	 *            the portataLaminata to set
	 */
	public void setPortataLaminata(Double portataLaminata) {
		this.portataLaminata = portataLaminata;
	}

	/**
	 * @return the quotaInvaso
	 */
	public Double getQuotaInvaso() {
		return quotaInvaso;
	}

	/**
	 * @param quotaInvaso
	 *            the quotaInvaso to set
	 */
	public void setQuotaInvaso(Double quotaInvaso) {
		this.quotaInvaso = quotaInvaso;
	}

	/**
	 * @return the franco
	 */
	public Double getFranco() {
		return franco;
	}

	/**
	 * @param franco
	 *            the franco to set
	 */
	public void setFranco(Double franco) {
		this.franco = franco;
	}

	/**
	 * @return the tempoDiRitorno
	 */
	public Long getTempoDiRitorno() {
		return tempoDiRitorno;
	}

	/**
	 * @param tempoDiRitorno
	 *            the tempoDiRitorno to set
	 */
	public void setTempoDiRitorno(Long tempoDiRitorno) {
		this.tempoDiRitorno = tempoDiRitorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((franco == null) ? 0 : franco.hashCode());
		result = prime * result + ((portataAfflusso == null) ? 0 : portataAfflusso.hashCode());
		result = prime * result + ((portataLaminata == null) ? 0 : portataLaminata.hashCode());
		result = prime * result + ((quotaInvaso == null) ? 0 : quotaInvaso.hashCode());
		result = prime * result + ((tempoDiRitorno == null) ? 0 : tempoDiRitorno.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof PortateRivalutateBean)) {
			return false;
		}
		PortateRivalutateBean other = (PortateRivalutateBean) obj;
		if (franco == null) {
			if (other.franco != null) {
				return false;
			}
		} else if (!franco.equals(other.franco)) {
			return false;
		}
		if (portataAfflusso == null) {
			if (other.portataAfflusso != null) {
				return false;
			}
		} else if (!portataAfflusso.equals(other.portataAfflusso)) {
			return false;
		}
		if (portataLaminata == null) {
			if (other.portataLaminata != null) {
				return false;
			}
		} else if (!portataLaminata.equals(other.portataLaminata)) {
			return false;
		}
		if (quotaInvaso == null) {
			if (other.quotaInvaso != null) {
				return false;
			}
		} else if (!quotaInvaso.equals(other.quotaInvaso)) {
			return false;
		}
		if (tempoDiRitorno == null) {
			if (other.tempoDiRitorno != null) {
				return false;
			}
		} else if (!tempoDiRitorno.equals(other.tempoDiRitorno)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PortateRivalutateBean [portataAfflusso=" + portataAfflusso + ", portataLaminata=" + portataLaminata + ", quotaInvaso=" + quotaInvaso
				+ ", franco=" + franco + ", tempoDiRitorno=" + tempoDiRitorno + "]";
	}
}
