package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DamExtraDataUtilizzazioneBean extends BasicEntityBean {

	private static final long serialVersionUID = -8615709861922850835L;

	private String numeroArchivio;
	private String sub;
	private String utilizzo=""; //UTILIZZAZIONE
	private String priorita=""; //PRIORITA
	private String group;
	private boolean init=false; //Controlla che ci siano dei dati

	public DamExtraDataUtilizzazioneBean() { }

	public String getNumeroArchivio() {
		return numeroArchivio;
	}

	public void setNumeroArchivio(String numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getUtilizzo() {
		return utilizzo;
	}

	public void setUtilizzo(String utilizzo) {
		if(utilizzo!=null){
			this.setInit(true);
			this.utilizzo = utilizzo;
		}
	}

	public String getPriorita() {
		return priorita;
	}

	public void setPriorita(String priorita) {
		if(priorita!=null){
			this.setInit(true);
			this.priorita = priorita;
		}
	}
	
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
	public boolean isInit() {
		return init;
	}

	private void setInit(boolean init) {
		this.init = init;
	}
	
	public DamExtraDataUtilizzazioneBean duplicate(){
		DamExtraDataUtilizzazioneBean cloned = new DamExtraDataUtilizzazioneBean();
		//cloned.setNumeroArchivio(this.getNumeroArchivio());
		//cloned.setSub(this.getSub());
		cloned.setUtilizzo(this.getUtilizzo());
		//cloned.setGroup(this.getGroup());
		cloned.setPriorita(this.getPriorita());		
		return cloned;
	}

	@Override
	public String toString() {
		return "DamExtraDataUtilizzazioneBean [getNumeroArchivio()="
				+ getNumeroArchivio() + ", getSub()=" + getSub()
				+ ", getUtilizzo()=" + getUtilizzo() + ", getPriorita()="
				+ getPriorita() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((numeroArchivio == null) ? 0 : numeroArchivio.hashCode());
		result = prime * result
				+ ((priorita == null) ? 0 : priorita.hashCode());
		result = prime * result + ((sub == null) ? 0 : sub.hashCode());
		result = prime * result
				+ ((utilizzo == null) ? 0 : utilizzo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DamExtraDataUtilizzazioneBean))
			return false;
		DamExtraDataUtilizzazioneBean other = (DamExtraDataUtilizzazioneBean) obj;
		return this.toString().equals(other.toString());
	}
	
}
