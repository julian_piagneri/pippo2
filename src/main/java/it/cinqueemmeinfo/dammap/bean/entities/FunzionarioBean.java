package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

public class FunzionarioBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 2131179837807900815L;

	private String nome;
	private String cognome;
	private String telUfficio;
	private String telCellulare;
	private String telCasa;

	public FunzionarioBean() {
		;
	}

	public FunzionarioBean(String id, String nome, String cognome, String telUfficio, String telCellulare, String telCasa, String lastModifiedDate,
			String lastModifiedUserId, String isDeleted) {

		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.nome = nome;
		this.cognome = cognome;
		this.telUfficio = telUfficio;
		this.telCellulare = telCellulare;
		this.telCasa = telCasa;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the cognome
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * @param cognome
	 *            the cognome to set
	 */
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	/**
	 * @return the telUfficio
	 */
	public String getTelUfficio() {
		return telUfficio;
	}

	/**
	 * @param telUfficio
	 *            the telUfficio to set
	 */
	public void setTelUfficio(String telUfficio) {
		this.telUfficio = telUfficio;
	}

	/**
	 * @return the telCellulare
	 */
	public String getTelCellulare() {
		return telCellulare;
	}

	/**
	 * @param telCellulare
	 *            the telCellulare to set
	 */
	public void setTelCellulare(String telCellulare) {
		this.telCellulare = telCellulare;
	}

	/**
	 * @return the telCasa
	 */
	public String getTelCasa() {
		return telCasa;
	}

	/**
	 * @param telCasa
	 *            the telCasa to set
	 */
	public void setTelCasa(String telCasa) {
		this.telCasa = telCasa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cognome == null) ? 0 : cognome.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((telCasa == null) ? 0 : telCasa.hashCode());
		result = prime * result + ((telCellulare == null) ? 0 : telCellulare.hashCode());
		result = prime * result + ((telUfficio == null) ? 0 : telUfficio.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof FunzionarioBean)) {
			return false;
		}
		FunzionarioBean other = (FunzionarioBean) obj;
		if (cognome == null) {
			if (other.cognome != null) {
				return false;
			}
		} else if (!cognome.equals(other.cognome)) {
			return false;
		}
		if (nome == null) {
			if (other.nome != null) {
				return false;
			}
		} else if (!nome.equals(other.nome)) {
			return false;
		}
		if (telCasa == null) {
			if (other.telCasa != null) {
				return false;
			}
		} else if (!telCasa.equals(other.telCasa)) {
			return false;
		}
		if (telCellulare == null) {
			if (other.telCellulare != null) {
				return false;
			}
		} else if (!telCellulare.equals(other.telCellulare)) {
			return false;
		}
		if (telUfficio == null) {
			if (other.telUfficio != null) {
				return false;
			}
		} else if (!telUfficio.equals(other.telUfficio)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FunzionarioBean [nome=" + nome + ", cognome=" + cognome + ", telUfficio=" + telUfficio + ", telCellulare=" + telCellulare + ", telCasa="
				+ telCasa + "]";
	}
}
