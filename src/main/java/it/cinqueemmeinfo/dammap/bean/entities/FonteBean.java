package it.cinqueemmeinfo.dammap.bean.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FonteBean implements Serializable {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 7976998781387764616L;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long idFonte;
	private String fonte;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Integer predefinito;

	/**
	 * @return the idFonte
	 */
	public Long getIdFonte() {
		return idFonte;
	}

	/**
	 * @param idFonte
	 *            the idFonte to set
	 */
	public void setIdFonte(Long idFonte) {
		this.idFonte = idFonte;
	}

	/**
	 * @return the fonte
	 */
	public String getFonte() {
		return fonte;
	}

	/**
	 * @param fonte
	 *            the fonte to set
	 */
	public void setFonte(String fonte) {
		this.fonte = fonte;
	}

	/**
	 * @return the predefinito
	 */
	public Integer getPredefinito() {
		return predefinito;
	}

	/**
	 * @param predefinito
	 *            the predefinito to set
	 */
	public void setPredefinito(Integer predefinito) {
		this.predefinito = predefinito;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fonte == null) ? 0 : fonte.hashCode());
		result = prime * result + ((idFonte == null) ? 0 : idFonte.hashCode());
		result = prime * result + ((predefinito == null) ? 0 : predefinito.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FonteBean)) {
			return false;
		}
		FonteBean other = (FonteBean) obj;
		if (fonte == null) {
			if (other.fonte != null) {
				return false;
			}
		} else if (!fonte.equals(other.fonte)) {
			return false;
		}
		if (idFonte == null) {
			if (other.idFonte != null) {
				return false;
			}
		} else if (!idFonte.equals(other.idFonte)) {
			return false;
		}
		if (predefinito == null) {
			if (other.predefinito != null) {
				return false;
			}
		} else if (!predefinito.equals(other.predefinito)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FonteBean [idFonte=" + idFonte + ", fonte=" + fonte + ", predefinito=" + predefinito + "]";
	}
}
