package it.cinqueemmeinfo.dammap.bean.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FenomeniFranosiSchedaBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 5898422007462578671L;

	private long idSchedaFenomeniFranosi;
	private String descrizione;
	private List<FenomeniFranosiBean> fenomeniFranosi;

	/**
	 * @return the idSchedaFenomeniFranosi
	 */
	public long getIdSchedaFenomeniFranosi() {
		return idSchedaFenomeniFranosi;
	}

	/**
	 * @param idSchedaFenomeniFranosi
	 *            the idSchedaFenomeniFranosi to set
	 */
	public void setIdSchedaFenomeniFranosi(long idSchedaFenomeniFranosi) {
		this.idSchedaFenomeniFranosi = idSchedaFenomeniFranosi;
	}

	/**
	 * @return the descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @param descrizione
	 *            the descrizione to set
	 */
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * @return the fenomeniFranosi
	 */
	public List<FenomeniFranosiBean> getFenomeniFranosi() {
		return fenomeniFranosi;
	}

	/**
	 * @param fenomeniFranosi
	 *            the fenomeniFranosi to set
	 */
	public void setFenomeniFranosi(List<FenomeniFranosiBean> fenomeniFranosi) {
		this.fenomeniFranosi = fenomeniFranosi;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((descrizione == null) ? 0 : descrizione.hashCode());
		result = prime * result + ((fenomeniFranosi == null) ? 0 : fenomeniFranosi.hashCode());
		result = prime * result + (int) (idSchedaFenomeniFranosi ^ (idSchedaFenomeniFranosi >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof FenomeniFranosiSchedaBean)) {
			return false;
		}
		FenomeniFranosiSchedaBean other = (FenomeniFranosiSchedaBean) obj;
		if (descrizione == null) {
			if (other.descrizione != null) {
				return false;
			}
		} else if (!descrizione.equals(other.descrizione)) {
			return false;
		}
		if (fenomeniFranosi == null) {
			if (other.fenomeniFranosi != null) {
				return false;
			}
		} else if (!fenomeniFranosi.equals(other.fenomeniFranosi)) {
			return false;
		}
		if (idSchedaFenomeniFranosi != other.idSchedaFenomeniFranosi) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FenomeniFranosiSchedaBean [idSchedaFenomeniFranosi=" + idSchedaFenomeniFranosi + ", descrizione=" + descrizione + ", fenomeniFranosi="
				+ fenomeniFranosi + "]";
	}
}