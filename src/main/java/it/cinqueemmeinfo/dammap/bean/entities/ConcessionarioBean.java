package it.cinqueemmeinfo.dammap.bean.entities;

import java.util.ArrayList;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;

public class ConcessionarioBean extends BasicEntityBean {

	private static final long serialVersionUID = 5908854435312132155L;
	
	private String nome;
	private ArrayList<AddressBean> indirizzo;
	private ArrayList<ContactInfoBean> contatto;
	private Integer numIscrizioneRid;

	public ConcessionarioBean() {
		this.indirizzo = new ArrayList<AddressBean>();
		this.contatto = new ArrayList<ContactInfoBean>();
	}
	
	public ConcessionarioBean(String id) {
		super(id, "", "", "");
		this.nome = "";
		this.indirizzo = new ArrayList<AddressBean>();
		this.contatto = new ArrayList<ContactInfoBean>();
		this.numIscrizioneRid = null;
	}
	
	public ConcessionarioBean(String id, String nome, ArrayList<AddressBean> indirizzo
								, ArrayList<ContactInfoBean> contatto, Integer numIscrizioneRid
								, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.contatto = contatto;
		this.numIscrizioneRid = numIscrizioneRid;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public ArrayList<AddressBean> getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(ArrayList<AddressBean> indirizzo) {
		this.indirizzo = indirizzo;
	}
	public ArrayList<ContactInfoBean> getContatto() {
		return contatto;
	}
	public void setContatto(ArrayList<ContactInfoBean> contatto) {
		this.contatto = contatto;
	}
	public Integer getNumIscrizioneRid() {
		return numIscrizioneRid;
	}
	public void setNumIscrizioneRid(Integer numIscrizioneRid) {
		this.numIscrizioneRid = numIscrizioneRid;
	}

	@Override
	public String toString() {
		return "ConcessionarioBean [getNome()=" + getNome()
				+ ", getIndirizzo()=" + getIndirizzo() + ", getContatto()="
				+ getContatto() + ", getNumIscrizioneRid()="
				+ getNumIscrizioneRid() + ", getId()=" + getId()
				+ ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", getLastModifiedUserId()=" + getLastModifiedUserId()
				+ ", getIsDeleted()=" + getIsDeleted() + "]";
	}
}
