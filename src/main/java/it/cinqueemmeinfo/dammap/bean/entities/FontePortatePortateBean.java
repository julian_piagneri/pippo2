package it.cinqueemmeinfo.dammap.bean.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FontePortatePortateBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = -4240282588056128063L;

	private FontePortateBean fonte;
	private List<PortateRivalutateBean> portate;

	/**
	 * @return the fonte
	 */
	public FontePortateBean getFonte() {
		return fonte;
	}

	/**
	 * @param fonte
	 *            the fonte to set
	 */
	public void setFonte(FontePortateBean fonte) {
		this.fonte = fonte;
	}

	/**
	 * @return the portate
	 */
	public List<PortateRivalutateBean> getPortate() {
		return portate;
	}

	/**
	 * @param portate
	 *            the portate to set
	 */
	public void setPortate(List<PortateRivalutateBean> portate) {
		this.portate = portate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((fonte == null) ? 0 : fonte.hashCode());
		result = prime * result + ((portate == null) ? 0 : portate.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof FontePortatePortateBean)) {
			return false;
		}
		FontePortatePortateBean other = (FontePortatePortateBean) obj;
		if (fonte == null) {
			if (other.fonte != null) {
				return false;
			}
		} else if (!fonte.equals(other.fonte)) {
			return false;
		}
		if (portate == null) {
			if (other.portate != null) {
				return false;
			}
		} else if (!portate.equals(other.portate)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FontePortatePortateBean [fonte=" + fonte + ", portate=" + portate + "]";
	}
}
