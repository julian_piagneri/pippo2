package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FontePortateBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 5259081234717576192L;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long fonteAfflusso;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long fonteLaminata;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long fonteInvaso;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long fonteFranco;

	/**
	 * @return the fonteAfflusso
	 */
	public Long getFonteAfflusso() {
		return fonteAfflusso;
	}

	/**
	 * @param fonteAfflusso
	 *            the fonteAfflusso to set
	 */
	public void setFonteAfflusso(Long fonteAfflusso) {
		this.fonteAfflusso = fonteAfflusso == 0 ? null : fonteAfflusso;
	}

	/**
	 * @return the fonteLaminata
	 */
	public Long getFonteLaminata() {
		return fonteLaminata;
	}

	/**
	 * @param fonteLaminata
	 *            the fonteLaminata to set
	 */
	public void setFonteLaminata(Long fonteLaminata) {
		this.fonteLaminata = fonteLaminata == 0 ? null : fonteLaminata;
	}

	/**
	 * @return the fonteInvaso
	 */
	public Long getFonteInvaso() {
		return fonteInvaso;
	}

	/**
	 * @param fonteInvaso
	 *            the fonteInvaso to set
	 */
	public void setFonteInvaso(Long fonteInvaso) {
		this.fonteInvaso = fonteInvaso == 0 ? null : fonteInvaso;
	}

	/**
	 * @return the fonteFranco
	 */
	public Long getFonteFranco() {
		return fonteFranco;
	}

	/**
	 * @param fonteFranco
	 *            the fonteFranco to set
	 */
	public void setFonteFranco(Long fonteFranco) {
		this.fonteFranco = fonteFranco == 0 ? null : fonteFranco;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((fonteAfflusso == null) ? 0 : fonteAfflusso.hashCode());
		result = prime * result + ((fonteFranco == null) ? 0 : fonteFranco.hashCode());
		result = prime * result + ((fonteInvaso == null) ? 0 : fonteInvaso.hashCode());
		result = prime * result + ((fonteLaminata == null) ? 0 : fonteLaminata.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof FontePortateBean)) {
			return false;
		}
		FontePortateBean other = (FontePortateBean) obj;
		if (fonteAfflusso == null) {
			if (other.fonteAfflusso != null) {
				return false;
			}
		} else if (!fonteAfflusso.equals(other.fonteAfflusso)) {
			return false;
		}
		if (fonteFranco == null) {
			if (other.fonteFranco != null) {
				return false;
			}
		} else if (!fonteFranco.equals(other.fonteFranco)) {
			return false;
		}
		if (fonteInvaso == null) {
			if (other.fonteInvaso != null) {
				return false;
			}
		} else if (!fonteInvaso.equals(other.fonteInvaso)) {
			return false;
		}
		if (fonteLaminata == null) {
			if (other.fonteLaminata != null) {
				return false;
			}
		} else if (!fonteLaminata.equals(other.fonteLaminata)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FontePortateBean [fonteAfflusso=" + fonteAfflusso + ", fonteLaminata=" + fonteLaminata + ", fonteInvaso=" + fonteInvaso + ", fonteFranco="
				+ fonteFranco + "]";
	}
}