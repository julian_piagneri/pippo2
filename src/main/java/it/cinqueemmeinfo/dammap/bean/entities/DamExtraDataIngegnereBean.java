package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DamExtraDataIngegnereBean extends BasicEntityBean {

	private static final long serialVersionUID = -8615709861922850835L;

	private String numeroArchivio;
	private String sub;
	private String nome="";
	private String telefono="";
	private String group;
	private boolean init=false; //Controlla che ci siano dei dati

	public DamExtraDataIngegnereBean() { }

	public String getNumeroArchivio() {
		return numeroArchivio;
	}

	public void setNumeroArchivio(String numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome, String cognome) {
		if(nome!=null || cognome!=null){
			this.setInit(true);
			this.nome = nome+" "+cognome;
		}
	}
	
	public void setNome(String nome) {
		if(nome!=null){
			this.setInit(true);
			this.nome = nome;
		}
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		if(telefono!=null){
			this.setInit(true);
			this.telefono = telefono;
		}
	}
		
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public boolean isInit() {
		return init;
	}

	private void setInit(boolean init) {
		this.init = init;
	}
	
	public DamExtraDataIngegnereBean duplicate(){
		DamExtraDataIngegnereBean cloned = new DamExtraDataIngegnereBean();
		//cloned.setNumeroArchivio(this.getNumeroArchivio());
		//cloned.setSub(this.getSub());
		cloned.setNome(this.getNome());
		//cloned.setGroup(this.getGroup());
		cloned.setTelefono(this.getTelefono());		
		return cloned;
	}

	@Override
	public String toString() {
		return "DamExtraDataUtilizzazioneBean [getNumeroArchivio()="
				+ getNumeroArchivio() + ", getSub()=" + getSub()
				+ ", getNome()=" + getNome() + ", getTelefono()="
				+ getTelefono() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((numeroArchivio == null) ? 0 : numeroArchivio.hashCode());
		result = prime * result
				+ ((telefono == null) ? 0 : telefono.hashCode());
		result = prime * result + ((sub == null) ? 0 : sub.hashCode());
		result = prime * result
				+ ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DamExtraDataIngegnereBean))
			return false;
		DamExtraDataIngegnereBean other = (DamExtraDataIngegnereBean) obj;
		return this.toString().equals(other.toString());
	}
	
}
