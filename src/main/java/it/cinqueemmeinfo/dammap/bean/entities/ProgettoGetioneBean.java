package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ProgettoGetioneBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 8915748806352722309L;

	private String dataPresentazione;
	private String dataParere;
	private String dataApprovazione;
	private String dataRilievo;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double volumeTotale = null;
	private String note;

	/**
	 * @param dataPresentazione
	 * @param dataParere
	 * @param dataApprovazione
	 * @param dataRilievo
	 * @param volumeTotale
	 * @param note
	 */
	protected ProgettoGetioneBean(String dataPresentazione, String dataParere, String dataApprovazione, String dataRilievo, Double volumeTotale, String note) {
		super();
		this.dataPresentazione = dataPresentazione;
		this.dataParere = dataParere;
		this.dataApprovazione = dataApprovazione;
		this.dataRilievo = dataRilievo;
		this.volumeTotale = volumeTotale;
		this.note = note;
	}

	/**
	 * 
	 */
	public ProgettoGetioneBean() {
		this(null, null, null, null, null, null);
	}

	/**
	 * @return the dataPresentazione
	 */
	public String getDataPresentazione() {
		return dataPresentazione;
	}

	/**
	 * @param dataPresentazione
	 *            the dataPresentazione to set
	 */
	public void setDataPresentazione(String dataPresentazione) {
		this.dataPresentazione = dataPresentazione;
	}

	/**
	 * @return the dataParere
	 */
	public String getDataParere() {
		return dataParere;
	}

	/**
	 * @param dataParere
	 *            the dataParere to set
	 */
	public void setDataParere(String dataParere) {
		this.dataParere = dataParere;
	}

	/**
	 * @return the dataApprovazione
	 */
	public String getDataApprovazione() {
		return dataApprovazione;
	}

	/**
	 * @param dataApprovazione
	 *            the dataApprovazione to set
	 */
	public void setDataApprovazione(String dataApprovazione) {
		this.dataApprovazione = dataApprovazione;
	}

	/**
	 * @return the dataRilievo
	 */
	public String getDataRilievo() {
		return dataRilievo;
	}

	/**
	 * @param dataRilievo
	 *            the dataRilievo to set
	 */
	public void setDataRilievo(String dataRilievo) {
		this.dataRilievo = dataRilievo;
	}

	/**
	 * @return the volumeTotale
	 */
	public Double getVolumeTotale() {
		return volumeTotale;
	}

	/**
	 * @param volumeTotale
	 *            the volumeTotale to set
	 */
	public void setVolumeTotale(Double volumeTotale) {
		this.volumeTotale = volumeTotale;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataApprovazione == null) ? 0 : dataApprovazione.hashCode());
		result = prime * result + ((dataParere == null) ? 0 : dataParere.hashCode());
		result = prime * result + ((dataPresentazione == null) ? 0 : dataPresentazione.hashCode());
		result = prime * result + ((dataRilievo == null) ? 0 : dataRilievo.hashCode());
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((volumeTotale == null) ? 0 : volumeTotale.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof ProgettoGetioneBean)) {
			return false;
		}
		ProgettoGetioneBean other = (ProgettoGetioneBean) obj;
		if (dataApprovazione == null) {
			if (other.dataApprovazione != null) {
				return false;
			}
		} else if (!dataApprovazione.equals(other.dataApprovazione)) {
			return false;
		}
		if (dataParere == null) {
			if (other.dataParere != null) {
				return false;
			}
		} else if (!dataParere.equals(other.dataParere)) {
			return false;
		}
		if (dataPresentazione == null) {
			if (other.dataPresentazione != null) {
				return false;
			}
		} else if (!dataPresentazione.equals(other.dataPresentazione)) {
			return false;
		}
		if (dataRilievo == null) {
			if (other.dataRilievo != null) {
				return false;
			}
		} else if (!dataRilievo.equals(other.dataRilievo)) {
			return false;
		}
		if (note == null) {
			if (other.note != null) {
				return false;
			}
		} else if (!note.equals(other.note)) {
			return false;
		}
		if (volumeTotale == null) {
			if (other.volumeTotale != null) {
				return false;
			}
		} else if (!volumeTotale.equals(other.volumeTotale)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProgettoGetioneBean [dataPresentazione=" + dataPresentazione + ", dataParere=" + dataParere + ", dataApprovazione=" + dataApprovazione
				+ ", dataRilievo=" + dataRilievo + ", volumeTotale=" + volumeTotale + ", note=" + note + "]";
	}
}