package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TipoOndePienaBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = -7516387524031302616L;

	private long idTipo;
	private String tipo;
	private String nome;
	private String key;
	private String descrizione;
	private String ondaLayer;

	/**
	 * @return the idTipo
	 */
	public long getIdTipo() {
		return idTipo;
	}

	/**
	 * @param idTipo
	 *            the idTipo to set
	 */
	public void setIdTipo(long idTipo) {
		this.idTipo = idTipo;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @param descrizione
	 *            the descrizione to set
	 */
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * @return the ondaLayer
	 */
	public String getOndaLayer() {
		return ondaLayer;
	}

	/**
	 * @param ondaLayer
	 *            the ondaLayer to set
	 */
	public void setOndaLayer(String ondaLayer) {
		this.ondaLayer = ondaLayer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((descrizione == null) ? 0 : descrizione.hashCode());
		result = prime * result + (int) (idTipo ^ (idTipo >>> 32));
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((ondaLayer == null) ? 0 : ondaLayer.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof TipoOndePienaBean)) {
			return false;
		}
		TipoOndePienaBean other = (TipoOndePienaBean) obj;
		if (descrizione == null) {
			if (other.descrizione != null) {
				return false;
			}
		} else if (!descrizione.equals(other.descrizione)) {
			return false;
		}
		if (idTipo != other.idTipo) {
			return false;
		}
		if (key == null) {
			if (other.key != null) {
				return false;
			}
		} else if (!key.equals(other.key)) {
			return false;
		}
		if (nome == null) {
			if (other.nome != null) {
				return false;
			}
		} else if (!nome.equals(other.nome)) {
			return false;
		}
		if (ondaLayer == null) {
			if (other.ondaLayer != null) {
				return false;
			}
		} else if (!ondaLayer.equals(other.ondaLayer)) {
			return false;
		}
		if (tipo == null) {
			if (other.tipo != null) {
				return false;
			}
		} else if (!tipo.equals(other.tipo)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TipoOndePienaBean [idTipo=" + idTipo + ", tipo=" + tipo + ", nome=" + nome + ", key=" + key + ", descrizione=" + descrizione + ", ondaLayer="
				+ ondaLayer + "]";
	}
}