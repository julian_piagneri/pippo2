package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ClassificazioneSismicaDigaBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 7675801038686818111L;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long classificazioneSismicaDiga = null;

	public ClassificazioneSismicaDigaBean() {
		this(null);
	}

	/**
	 * @param classificazioneSismicaDiga
	 */
	protected ClassificazioneSismicaDigaBean(Long classificazioneSismicaDiga) {
		super();
		this.classificazioneSismicaDiga = classificazioneSismicaDiga;
	}

	/**
	 * @return the classificazioneSismicaDiga
	 */
	public Long getClassificazioneSismicaDiga() {
		return classificazioneSismicaDiga;
	}

	/**
	 * @param classificazioneSismicaDiga
	 *            the classificazioneSismicaDiga to set
	 */
	public void setClassificazioneSismicaDiga(Long classificazioneSismicaDiga) {
		this.classificazioneSismicaDiga = classificazioneSismicaDiga;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((classificazioneSismicaDiga == null) ? 0 : classificazioneSismicaDiga.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof ClassificazioneSismicaDigaBean)) {
			return false;
		}
		ClassificazioneSismicaDigaBean other = (ClassificazioneSismicaDigaBean) obj;
		if (classificazioneSismicaDiga == null) {
			if (other.classificazioneSismicaDiga != null) {
				return false;
			}
		} else if (!classificazioneSismicaDiga.equals(other.classificazioneSismicaDiga)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ClassificazioneSismicaDiga [classificazioneSismicaDiga=" + classificazioneSismicaDiga + "]";
	}
}
