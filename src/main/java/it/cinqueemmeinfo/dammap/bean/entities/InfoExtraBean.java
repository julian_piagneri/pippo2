package it.cinqueemmeinfo.dammap.bean.entities;

import java.io.Serializable;

public abstract class InfoExtraBean implements Serializable {

	private static final long serialVersionUID = 6573628608346073367L;
	protected long id;
	protected long campo;
	protected String valore;
	protected boolean numeric;
	
	public abstract String getIdentifier();

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getCampo() {
		return campo;
	}
	public void setCampo(long campo) {
		this.campo = campo;
	}
	public String getValore() {
		return valore;
	}
	public void setValore(String valore) {
		this.valore = valore;
	}
	public boolean isNumeric() {
		return numeric;
	}
	public void setNumeric(boolean numeric) {
		this.numeric = numeric;
	}
	
	@Override
	public String toString() {
		return "InfoExtraBean [getIdentifier()=" + getIdentifier()
				+ ", getId()=" + getId() + ", getCampo()=" + getCampo()
				+ ", getValore()=" + getValore() + ", isNumeric()="
				+ isNumeric() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (campo ^ (campo >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (numeric ? 1231 : 1237);
		result = prime * result + ((valore == null) ? 0 : valore.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof InfoExtraBean))
			return false;
		return this.toString().equals(obj.toString());
	}
	
	

}
