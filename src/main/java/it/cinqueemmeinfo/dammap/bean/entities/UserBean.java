package it.cinqueemmeinfo.dammap.bean.entities;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

public class UserBean extends BasicEntityBean {

	private static final long serialVersionUID = -3663795088012809874L;

	private String nome;
	private String cognome;
	private String descr;
	private String telUfficio;
	private String telCellulare;
	private String telCasa;
	private String tipo;
	private String preferenze;
	private String userId;
	private String filtri;
	private String categoria;
	private String idRiferimento;
	private String loginAbilitato;
	
	public UserBean() { }
	
	public UserBean(String id) {
		super(id, "", "", "");
	}

	public UserBean(String id, String nome, String cognome, 
			String descr, String telUfficio, String telCellulare, String telCasa, String tipo,
			String preferenze, String userId, String filtri, String categoria,
			String idRiferimento, String loginAbilitato, String lastModifiedDate,
			String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.nome = nome;
		this.cognome = cognome;
		this.descr = descr;
		this.telUfficio = telUfficio;
		this.telCellulare = telCellulare;
		this.telCasa = telCasa;
		this.tipo = tipo;
		this.preferenze = preferenze;
		this.userId = userId;
		this.filtri = filtri;
		this.categoria = categoria;
		this.idRiferimento = idRiferimento;
		this.loginAbilitato = loginAbilitato;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getTelUfficio() {
		return telUfficio;
	}

	public void setTelUfficio(String telUfficio) {
		this.telUfficio = telUfficio;
	}

	public String getTelCellulare() {
		return telCellulare;
	}

	public void setTelCellulare(String telCellulare) {
		this.telCellulare = telCellulare;
	}

	public String getTelCasa() {
		return telCasa;
	}

	public void setTelCasa(String telCasa) {
		this.telCasa = telCasa;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPreferenze() {
		return preferenze;
	}

	public void setPreferenze(String preferenze) {
		this.preferenze = preferenze;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFiltri() {
		return filtri;
	}

	public void setFiltri(String filtri) {
		this.filtri = filtri;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getIdRiferimento() {
		return idRiferimento;
	}

	public void setIdRiferimento(String idRiferimento) {
		this.idRiferimento = idRiferimento;
	}

	public String getLoginAbilitato() {
		return loginAbilitato;
	}

	public void setLoginAbilitato(String loginAbilitato) {
		this.loginAbilitato = loginAbilitato;
	}
}
