package it.cinqueemmeinfo.dammap.bean.entities;

import java.util.ArrayList;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;

public class GestoreBean extends BasicEntityBean {

	private static final long serialVersionUID = 7374576332484783796L;

	private String nome;
	private ArrayList<AddressBean> indirizzo;
	private ArrayList<ContactInfoBean> contatto;
	private String idConcessionario;

	public GestoreBean() {
		this.indirizzo = new ArrayList<AddressBean>();
		this.contatto = new ArrayList<ContactInfoBean>();
	}

	public GestoreBean(String id) {
		super(id, "", "", "");
		this.nome = "";
		this.indirizzo = new ArrayList<AddressBean>();
		this.contatto = new ArrayList<ContactInfoBean>();
		this.idConcessionario = "";
	}

	public GestoreBean(String id, String nome, ArrayList<AddressBean> indirizzo, ArrayList<ContactInfoBean> contatto,
			String idConcessionario, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {

		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.contatto = contatto;
		this.idConcessionario = idConcessionario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ArrayList<AddressBean> getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(ArrayList<AddressBean> indirizzo) {
		this.indirizzo = indirizzo;
	}

	public ArrayList<ContactInfoBean> getContatto() {
		return contatto;
	}

	public void setContatto(ArrayList<ContactInfoBean> contatto) {
		this.contatto = contatto;
	}

	public String getIdConcessionario() {
		return idConcessionario;
	}

	public void setIdConcessionario(String idConcessionario) {
		this.idConcessionario = idConcessionario;
	}

	@Override
	public String toString() {
		return "GestoreBean [getNome()=" + getNome() + ", getIndirizzo()=" + getIndirizzo() + ", getContatto()="
				+ getContatto() + ", getIdConcessionario()=" + getIdConcessionario() + ", getId()=" + getId()
				+ ", getLastModifiedDate()=" + getLastModifiedDate() + ", getLastModifiedUserId()="
				+ getLastModifiedUserId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
