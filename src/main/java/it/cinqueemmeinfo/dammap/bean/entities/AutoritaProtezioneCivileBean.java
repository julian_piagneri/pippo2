package it.cinqueemmeinfo.dammap.bean.entities;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;

import java.util.ArrayList;

public class AutoritaProtezioneCivileBean extends BasicEntityBean {

	private static final long serialVersionUID = -2564274843432652896L;

	private String siglaProvincia;
	private String nomeRegione;
	private String descrizione;
	private ArrayList<ContactInfoBean> contatto;
	private ArrayList<AddressBean> indirizzo;
	private String tipoAutorita;
	
	public AutoritaProtezioneCivileBean() {
		this.contatto = new ArrayList<ContactInfoBean>();
		this.indirizzo = new ArrayList<AddressBean>();
	}
	
	public AutoritaProtezioneCivileBean(String id) {
		super(id, "", "", "");
		this.siglaProvincia = "";
		this.nomeRegione = "";
		this.descrizione = "";
		this.contatto = new ArrayList<ContactInfoBean>();
		this.indirizzo = new ArrayList<AddressBean>();
		this.tipoAutorita = null;
	}
	
	public AutoritaProtezioneCivileBean(String id, String siglaProvincia, String nomeRegione, String descrizione
										, ArrayList<ContactInfoBean> contatto, ArrayList<AddressBean> indirizzo, String tipoAutorita
										, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.siglaProvincia = siglaProvincia;
		this.nomeRegione = nomeRegione;
		this.descrizione = descrizione;
		this.contatto = contatto;
		this.indirizzo = indirizzo;
		this.tipoAutorita = tipoAutorita;
	}

	public String getSiglaProvincia() {
		return siglaProvincia;
	}

	public void setSiglaProvincia(String siglaProvincia) {
		this.siglaProvincia = siglaProvincia;
	}

	public String getNomeRegione() {
		return nomeRegione;
	}

	public void setNomeRegione(String nomeRegione) {
		this.nomeRegione = nomeRegione;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public ArrayList<ContactInfoBean> getContatto() {
		return contatto;
	}

	public void setContatto(ArrayList<ContactInfoBean> contatto) {
		this.contatto = contatto;
	}

	public ArrayList<AddressBean> getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(ArrayList<AddressBean> indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getTipoAutorita() {
		return tipoAutorita;
	}

	public void setTipoAutorita(String tipoAutorita) {
		this.tipoAutorita = tipoAutorita;
	}

	@Override
	public String toString() {
		return "AutoritaProtezioneCivileBean [getSiglaProvincia()="
				+ getSiglaProvincia() + ", getNomeRegione()="
				+ getNomeRegione() + ", getDescrizione()=" + getDescrizione()
				+ ", getContatto()=" + getContatto() + ", getIndirizzo()="
				+ getIndirizzo() + ", getTipoAutorita()=" + getTipoAutorita()
				+ ", getId()=" + getId() + ", getLastModifiedDate()="
				+ getLastModifiedDate() + ", getLastModifiedUserId()="
				+ getLastModifiedUserId() + ", getIsDeleted()="
				+ getIsDeleted() + "]";
	}
	
}
