package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TestataBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 8196897023639125643L;

	private String numeroArchivio; // NUMERO_ARCHIVIO
	private String sub; // SUB
	private String nomeDiga;
	private String nomeFiumeSbarrato; // NOME_FIUME_SBARRATO
	private String nomeBacino; // NOME_BACINO
	private String descrClassDiga; // DESCRIZIONE_CLASSIFICA_DIGA
	private String volTotInvaso; // VOLUME_TOTALE_INVASO_L584
	private String quotaMax; // QUOTA_MAX
	private String quotaMaxReg; // QUOTA_MAX_REGOLAZIONE
	private String volAutoriz; // VOLUME_AUTORIZZATO
	private String quotaAutoriz; // QUOTA_AUTORIZZATA
	private String portataMaxPiena; // PORTATA_MAX_PIENA_PROG
	private String dataCertCollaudo; // DATA_CERTIFICATO_COLLAUDO
	private String nomeComune;
	private String nomeRegione; // NOME_REGIONE
	private String fiumiDiga; // FIUMI_DIGA
	private String numeroRid;
	private String status;
	private UfficioBean uffCoord; // UFFICIO_COORD
	private UfficioBean uffPerif; // UFFICIO_PERIFERICO

	/**
	 * @return the numeroArchivio
	 */
	public String getNumeroArchivio() {
		return numeroArchivio;
	}

	/**
	 * @param numeroArchivio
	 *            the numeroArchivio to set
	 */
	public void setNumeroArchivio(String numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	/**
	 * @return the sub
	 */
	public String getSub() {
		return sub;
	}

	/**
	 * @param sub
	 *            the sub to set
	 */
	public void setSub(String sub) {
		this.sub = sub;
	}

	/**
	 * @return the nomeDiga
	 */
	public String getNomeDiga() {
		return nomeDiga;
	}

	/**
	 * @param nomeDiga
	 *            the nomeDiga to set
	 */
	public void setNomeDiga(String nomeDiga) {
		this.nomeDiga = nomeDiga;
	}

	/**
	 * @return the nomeFiumeSbarrato
	 */
	public String getNomeFiumeSbarrato() {
		return nomeFiumeSbarrato;
	}

	/**
	 * @param nomeFiumeSbarrato
	 *            the nomeFiumeSbarrato to set
	 */
	public void setNomeFiumeSbarrato(String nomeFiumeSbarrato) {
		this.nomeFiumeSbarrato = nomeFiumeSbarrato;
	}

	/**
	 * @return the nomeBacino
	 */
	public String getNomeBacino() {
		return nomeBacino;
	}

	/**
	 * @param nomeBacino
	 *            the nomeBacino to set
	 */
	public void setNomeBacino(String nomeBacino) {
		this.nomeBacino = nomeBacino;
	}

	/**
	 * @return the descrClassDiga
	 */
	public String getDescrClassDiga() {
		return descrClassDiga;
	}

	/**
	 * @param descrClassDiga
	 *            the descrClassDiga to set
	 */
	public void setDescrClassDiga(String descrClassDiga) {
		this.descrClassDiga = descrClassDiga;
	}

	/**
	 * @return the volTotInvaso
	 */
	public String getVolTotInvaso() {
		return volTotInvaso;
	}

	/**
	 * @param volTotInvaso
	 *            the volTotInvaso to set
	 */
	public void setVolTotInvaso(String volTotInvaso) {
		this.volTotInvaso = volTotInvaso;
	}

	/**
	 * @return the quotaMax
	 */
	public String getQuotaMax() {
		return quotaMax;
	}

	/**
	 * @param quotaMax
	 *            the quotaMax to set
	 */
	public void setQuotaMax(String quotaMax) {
		this.quotaMax = quotaMax;
	}

	/**
	 * @return the quotaMaxReg
	 */
	public String getQuotaMaxReg() {
		return quotaMaxReg;
	}

	/**
	 * @param quotaMaxReg
	 *            the quotaMaxReg to set
	 */
	public void setQuotaMaxReg(String quotaMaxReg) {
		this.quotaMaxReg = quotaMaxReg;
	}

	/**
	 * @return the volAutoriz
	 */
	public String getVolAutoriz() {
		return volAutoriz;
	}

	/**
	 * @param volAutoriz
	 *            the volAutoriz to set
	 */
	public void setVolAutoriz(String volAutoriz) {
		this.volAutoriz = volAutoriz;
	}

	/**
	 * @return the quotaAutoriz
	 */
	public String getQuotaAutoriz() {
		return quotaAutoriz;
	}

	/**
	 * @param quotaAutoriz
	 *            the quotaAutoriz to set
	 */
	public void setQuotaAutoriz(String quotaAutoriz) {
		this.quotaAutoriz = quotaAutoriz;
	}

	/**
	 * @return the portataMaxPiena
	 */
	public String getPortataMaxPiena() {
		return portataMaxPiena;
	}

	/**
	 * @param portataMaxPiena
	 *            the portataMaxPiena to set
	 */
	public void setPortataMaxPiena(String portataMaxPiena) {
		this.portataMaxPiena = portataMaxPiena;
	}

	/**
	 * @return the dataCertCollaudo
	 */
	public String getDataCertCollaudo() {
		return dataCertCollaudo;
	}

	/**
	 * @param dataCertCollaudo
	 *            the dataCertCollaudo to set
	 */
	public void setDataCertCollaudo(String dataCertCollaudo) {
		this.dataCertCollaudo = dataCertCollaudo;
	}

	/**
	 * @return the nomeComune
	 */
	public String getNomeComune() {
		return nomeComune;
	}

	/**
	 * @param nomeComune
	 *            the nomeComune to set
	 */
	public void setNomeComune(String nomeComune) {
		this.nomeComune = nomeComune;
	}

	/**
	 * @return the nomeRegione
	 */
	public String getNomeRegione() {
		return nomeRegione;
	}

	/**
	 * @param nomeRegione
	 *            the nomeRegione to set
	 */
	public void setNomeRegione(String nomeRegione) {
		this.nomeRegione = nomeRegione;
	}

	/**
	 * @return the fiumiDiga
	 */
	public String getFiumiDiga() {
		return fiumiDiga;
	}

	/**
	 * @param fiumiDiga
	 *            the fiumiDiga to set
	 */
	public void setFiumiDiga(String fiumiDiga) {
		this.fiumiDiga = fiumiDiga;
	}

	/**
	 * @return the numeroRid
	 */
	public String getNumeroRid() {
		return numeroRid;
	}

	/**
	 * @param numeroRid
	 *            the numeroRid to set
	 */
	public void setNumeroRid(String numeroRid) {
		this.numeroRid = numeroRid;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the uffCoord
	 */
	public UfficioBean getUffCoord() {
		return uffCoord;
	}

	/**
	 * @param uffCoord
	 *            the uffCoord to set
	 */
	public void setUffCoord(UfficioBean uffCoord) {
		this.uffCoord = uffCoord;
	}

	/**
	 * @return the uffPerif
	 */
	public UfficioBean getUffPerif() {
		return uffPerif;
	}

	/**
	 * @param uffPerif
	 *            the uffPerif to set
	 */
	public void setUffPerif(UfficioBean uffPerif) {
		this.uffPerif = uffPerif;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TestataBean [numeroArchivio=" + numeroArchivio + ", sub=" + sub + ", nomeDiga=" + nomeDiga + ", nomeFiumeSbarrato=" + nomeFiumeSbarrato
				+ ", nomeBacino=" + nomeBacino + ", descrClassDiga=" + descrClassDiga + ", volTotInvaso=" + volTotInvaso + ", quotaMax=" + quotaMax
				+ ", quotaMaxReg=" + quotaMaxReg + ", volAutoriz=" + volAutoriz + ", quotaAutoriz=" + quotaAutoriz + ", portataMaxPiena=" + portataMaxPiena
				+ ", dataCertCollaudo=" + dataCertCollaudo + ", nomeComune=" + nomeComune + ", nomeRegione=" + nomeRegione + ", fiumiDiga=" + fiumiDiga
				+ ", numeroRid=" + numeroRid + ", status=" + status + ", uffCoord=" + uffCoord + ", uffPerif=" + uffPerif + "]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataCertCollaudo == null) ? 0 : dataCertCollaudo.hashCode());
		result = prime * result + ((descrClassDiga == null) ? 0 : descrClassDiga.hashCode());
		result = prime * result + ((fiumiDiga == null) ? 0 : fiumiDiga.hashCode());
		result = prime * result + ((nomeBacino == null) ? 0 : nomeBacino.hashCode());
		result = prime * result + ((nomeComune == null) ? 0 : nomeComune.hashCode());
		result = prime * result + ((nomeDiga == null) ? 0 : nomeDiga.hashCode());
		result = prime * result + ((nomeFiumeSbarrato == null) ? 0 : nomeFiumeSbarrato.hashCode());
		result = prime * result + ((nomeRegione == null) ? 0 : nomeRegione.hashCode());
		result = prime * result + ((numeroArchivio == null) ? 0 : numeroArchivio.hashCode());
		result = prime * result + ((numeroRid == null) ? 0 : numeroRid.hashCode());
		result = prime * result + ((portataMaxPiena == null) ? 0 : portataMaxPiena.hashCode());
		result = prime * result + ((quotaAutoriz == null) ? 0 : quotaAutoriz.hashCode());
		result = prime * result + ((quotaMax == null) ? 0 : quotaMax.hashCode());
		result = prime * result + ((quotaMaxReg == null) ? 0 : quotaMaxReg.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((sub == null) ? 0 : sub.hashCode());
		result = prime * result + ((uffCoord == null) ? 0 : uffCoord.hashCode());
		result = prime * result + ((uffPerif == null) ? 0 : uffPerif.hashCode());
		result = prime * result + ((volAutoriz == null) ? 0 : volAutoriz.hashCode());
		result = prime * result + ((volTotInvaso == null) ? 0 : volTotInvaso.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof TestataBean)) {
			return false;
		}
		TestataBean other = (TestataBean) obj;
		if (dataCertCollaudo == null) {
			if (other.dataCertCollaudo != null) {
				return false;
			}
		} else if (!dataCertCollaudo.equals(other.dataCertCollaudo)) {
			return false;
		}
		if (descrClassDiga == null) {
			if (other.descrClassDiga != null) {
				return false;
			}
		} else if (!descrClassDiga.equals(other.descrClassDiga)) {
			return false;
		}
		if (fiumiDiga == null) {
			if (other.fiumiDiga != null) {
				return false;
			}
		} else if (!fiumiDiga.equals(other.fiumiDiga)) {
			return false;
		}
		if (nomeBacino == null) {
			if (other.nomeBacino != null) {
				return false;
			}
		} else if (!nomeBacino.equals(other.nomeBacino)) {
			return false;
		}
		if (nomeComune == null) {
			if (other.nomeComune != null) {
				return false;
			}
		} else if (!nomeComune.equals(other.nomeComune)) {
			return false;
		}
		if (nomeDiga == null) {
			if (other.nomeDiga != null) {
				return false;
			}
		} else if (!nomeDiga.equals(other.nomeDiga)) {
			return false;
		}
		if (nomeFiumeSbarrato == null) {
			if (other.nomeFiumeSbarrato != null) {
				return false;
			}
		} else if (!nomeFiumeSbarrato.equals(other.nomeFiumeSbarrato)) {
			return false;
		}
		if (nomeRegione == null) {
			if (other.nomeRegione != null) {
				return false;
			}
		} else if (!nomeRegione.equals(other.nomeRegione)) {
			return false;
		}
		if (numeroArchivio == null) {
			if (other.numeroArchivio != null) {
				return false;
			}
		} else if (!numeroArchivio.equals(other.numeroArchivio)) {
			return false;
		}
		if (numeroRid == null) {
			if (other.numeroRid != null) {
				return false;
			}
		} else if (!numeroRid.equals(other.numeroRid)) {
			return false;
		}
		if (portataMaxPiena == null) {
			if (other.portataMaxPiena != null) {
				return false;
			}
		} else if (!portataMaxPiena.equals(other.portataMaxPiena)) {
			return false;
		}
		if (quotaAutoriz == null) {
			if (other.quotaAutoriz != null) {
				return false;
			}
		} else if (!quotaAutoriz.equals(other.quotaAutoriz)) {
			return false;
		}
		if (quotaMax == null) {
			if (other.quotaMax != null) {
				return false;
			}
		} else if (!quotaMax.equals(other.quotaMax)) {
			return false;
		}
		if (quotaMaxReg == null) {
			if (other.quotaMaxReg != null) {
				return false;
			}
		} else if (!quotaMaxReg.equals(other.quotaMaxReg)) {
			return false;
		}
		if (status == null) {
			if (other.status != null) {
				return false;
			}
		} else if (!status.equals(other.status)) {
			return false;
		}
		if (sub == null) {
			if (other.sub != null) {
				return false;
			}
		} else if (!sub.equals(other.sub)) {
			return false;
		}
		if (uffCoord == null) {
			if (other.uffCoord != null) {
				return false;
			}
		} else if (!uffCoord.equals(other.uffCoord)) {
			return false;
		}
		if (uffPerif == null) {
			if (other.uffPerif != null) {
				return false;
			}
		} else if (!uffPerif.equals(other.uffPerif)) {
			return false;
		}
		if (volAutoriz == null) {
			if (other.volAutoriz != null) {
				return false;
			}
		} else if (!volAutoriz.equals(other.volAutoriz)) {
			return false;
		}
		if (volTotInvaso == null) {
			if (other.volTotInvaso != null) {
				return false;
			}
		} else if (!volTotInvaso.equals(other.volTotInvaso)) {
			return false;
		}
		return true;
	}
}
