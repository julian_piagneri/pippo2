package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SnellezzaVunerabilitaBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = -862846003600270766L;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double coeficienteSnellezzaCL = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double CLCritico = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double pgacFiltramento = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double pgacFessurazione = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double pgacScorrimento = null;

	private String discriminatore;

	/**
	 * @return the coeficienteSnellezzaCL
	 */
	public Double getCoeficienteSnellezzaCL() {
		return coeficienteSnellezzaCL;
	}

	/**
	 * @param coeficienteSnellezzaCL
	 *            the coeficienteSnellezzaCL to set
	 */
	public void setCoeficienteSnellezzaCL(Double coeficienteSnellezzaCL) {
		this.coeficienteSnellezzaCL = coeficienteSnellezzaCL;
	}

	/**
	 * @return the cLCritico
	 */
	public Double getCLCritico() {
		return CLCritico;
	}

	/**
	 * @param cLCritico
	 *            the cLCritico to set
	 */
	public void setCLCritico(Double cLCritico) {
		CLCritico = cLCritico;
	}

	/**
	 * @return the pgacFiltramento
	 */
	public Double getPgacFiltramento() {
		return pgacFiltramento;
	}

	/**
	 * @param pgacFiltramento
	 *            the pgacFiltramento to set
	 */
	public void setPgacFiltramento(Double pgacFiltramento) {
		this.pgacFiltramento = pgacFiltramento;
	}

	/**
	 * @return the pgacFessurazione
	 */
	public Double getPgacFessurazione() {
		return pgacFessurazione;
	}

	/**
	 * @param pgacFessurazione
	 *            the pgacFessurazione to set
	 */
	public void setPgacFessurazione(Double pgacFessurazione) {
		this.pgacFessurazione = pgacFessurazione;
	}

	/**
	 * @return the pgacScorrimento
	 */
	public Double getPgacScorrimento() {
		return pgacScorrimento;
	}

	/**
	 * @param pgacScorrimento
	 *            the pgacScorrimento to set
	 */
	public void setPgacScorrimento(Double pgacScorrimento) {
		this.pgacScorrimento = pgacScorrimento;
	}

	/**
	 * @return the discriminatore
	 */
	public String getDiscriminatore() {
		return discriminatore;
	}

	/**
	 * @param discriminatore
	 *            the discriminatore to set
	 */
	public void setDiscriminatore(String discriminatore) {
		this.discriminatore = discriminatore;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((CLCritico == null) ? 0 : CLCritico.hashCode());
		result = prime * result + ((coeficienteSnellezzaCL == null) ? 0 : coeficienteSnellezzaCL.hashCode());
		result = prime * result + ((discriminatore == null) ? 0 : discriminatore.hashCode());
		result = prime * result + ((pgacFessurazione == null) ? 0 : pgacFessurazione.hashCode());
		result = prime * result + ((pgacFiltramento == null) ? 0 : pgacFiltramento.hashCode());
		result = prime * result + ((pgacScorrimento == null) ? 0 : pgacScorrimento.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof SnellezzaVunerabilitaBean)) {
			return false;
		}
		SnellezzaVunerabilitaBean other = (SnellezzaVunerabilitaBean) obj;
		if (CLCritico == null) {
			if (other.CLCritico != null) {
				return false;
			}
		} else if (!CLCritico.equals(other.CLCritico)) {
			return false;
		}
		if (coeficienteSnellezzaCL == null) {
			if (other.coeficienteSnellezzaCL != null) {
				return false;
			}
		} else if (!coeficienteSnellezzaCL.equals(other.coeficienteSnellezzaCL)) {
			return false;
		}
		if (discriminatore == null) {
			if (other.discriminatore != null) {
				return false;
			}
		} else if (!discriminatore.equals(other.discriminatore)) {
			return false;
		}
		if (pgacFessurazione == null) {
			if (other.pgacFessurazione != null) {
				return false;
			}
		} else if (!pgacFessurazione.equals(other.pgacFessurazione)) {
			return false;
		}
		if (pgacFiltramento == null) {
			if (other.pgacFiltramento != null) {
				return false;
			}
		} else if (!pgacFiltramento.equals(other.pgacFiltramento)) {
			return false;
		}
		if (pgacScorrimento == null) {
			if (other.pgacScorrimento != null) {
				return false;
			}
		} else if (!pgacScorrimento.equals(other.pgacScorrimento)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SnellezzaVunerabilitaBean [coeficienteSnellezzaCL=" + coeficienteSnellezzaCL + ", CLCritico=" + CLCritico + ", pgacFiltramento="
				+ pgacFiltramento + ", pgacFessurazione=" + pgacFessurazione + ", pgacScorrimento=" + pgacScorrimento + ", discriminatore=" + discriminatore
				+ "]";
	}
}
