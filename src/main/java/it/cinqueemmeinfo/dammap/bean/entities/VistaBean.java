package it.cinqueemmeinfo.dammap.bean.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;


public class VistaBean implements Serializable {

	private static final long serialVersionUID = 1233647382902762145L;
	
	private Long id;
	private Long idUtente;
	private String name;
	private int defaultView;
	private JsonNode status;
	private String selectFilter;
	private String showFilter;
	private JsonNode baseView;
	
	
	public VistaBean() { }

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	public Long getIdUtente() {
		return idUtente;
	}


	public void setIdUtente(Long idUtente) {
		this.idUtente = idUtente;
	}

	@JsonIgnore
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public int getDefaultView() {
		return defaultView;
	}


	public void setDefaultView(int defaultView) {
		this.defaultView = defaultView;
	}

	@JsonInclude
	public JsonNode getStatus() {
		return status;
	}


	public void setStatus(JsonNode status) {
		this.status = status;
	}

	@JsonIgnore
	public String getSelectFilter() {
		return selectFilter;
	}


	public void setSelectFilter(String selectFilter) {
		this.selectFilter = selectFilter;
	}

	@JsonIgnore
	public String getShowFilter() {
		return showFilter;
	}


	public void setShowFilter(String showFilter) {
		this.showFilter = showFilter;
	}


	public JsonNode getBaseView() {
		return baseView;
	}

	public void setBaseView(JsonNode baseView) {
		this.baseView = baseView;
	}

	@Override
	public String toString() {
		return "VistaBean [getId()=" + getId() + ", getIdUtente()="
				+ getIdUtente() + ", getName()=" + getName()
				+ ", getDefaultView()=" + getDefaultView() + ", getStatus()="
				+ getStatus() + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + defaultView;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((idUtente == null) ? 0 : idUtente.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof VistaBean))
			return false;
		VistaBean other = (VistaBean) obj;
		if (defaultView != other.defaultView)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idUtente == null) {
			if (other.idUtente != null)
				return false;
		} else if (!idUtente.equals(other.idUtente))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

}