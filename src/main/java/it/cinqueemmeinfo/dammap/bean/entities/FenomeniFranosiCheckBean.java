package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FenomeniFranosiCheckBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 4638044181722387181L;

	private String isChecked;
	private String descrizioneCheck;

	/**
	 * @return the isChecked
	 */
	public String getIsChecked() {
		return isChecked;
	}

	/**
	 * @param isChecked
	 *            the isChecked to set
	 */
	public void setIsChecked(String isChecked) {
		this.isChecked = isChecked;
	}

	/**
	 * @return the descrizioneCheck
	 */
	public String getDescrizioneCheck() {
		return descrizioneCheck;
	}

	/**
	 * @param descrizioneCheck
	 *            the descrizioneCheck to set
	 */
	public void setDescrizioneCheck(String descrizioneCheck) {
		this.descrizioneCheck = descrizioneCheck;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((descrizioneCheck == null) ? 0 : descrizioneCheck.hashCode());
		result = prime * result + ((isChecked == null) ? 0 : isChecked.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof FenomeniFranosiCheckBean)) {
			return false;
		}
		FenomeniFranosiCheckBean other = (FenomeniFranosiCheckBean) obj;
		if (descrizioneCheck == null) {
			if (other.descrizioneCheck != null) {
				return false;
			}
		} else if (!descrizioneCheck.equals(other.descrizioneCheck)) {
			return false;
		}
		if (isChecked == null) {
			if (other.isChecked != null) {
				return false;
			}
		} else if (!isChecked.equals(other.isChecked)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FenomeniFranosiCheckBean [isChecked=" + isChecked + ", descrizioneCheck=" + descrizioneCheck + "]";
	}
}
