package it.cinqueemmeinfo.dammap.bean.entities;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

public class EntityTypeBean extends BasicEntityBean {

	private static final long serialVersionUID = 3492884780042180480L;
	
	String nome;
	String descrizione;
	private int ordine;
	
	public EntityTypeBean() { }
	
	public EntityTypeBean(String id, String nome, String descrizione
						, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.nome = nome;
		this.descrizione = descrizione;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public int getOrdine() {
		return ordine;
	}

	public void setOrdine(int ordine) {
		this.ordine = ordine;
	}
}
