package it.cinqueemmeinfo.dammap.bean.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class FenomeniFranosiGruppoBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 4226709866058012812L;

	private String descrizioneGruppo;
	private String esclusivo;
	private String sn;
	private List<FenomeniFranosiCheckBean> fenDigheCheck;

	/**
	 * @return the descrizioneGruppo
	 */
	public String getDescrizioneGruppo() {
		return descrizioneGruppo;
	}

	/**
	 * @param descrizioneGruppo
	 *            the descrizioneGruppo to set
	 */
	public void setDescrizioneGruppo(String descrizioneGruppo) {
		this.descrizioneGruppo = descrizioneGruppo;
	}

	/**
	 * @return the esclusivo
	 */
	public String getEsclusivo() {
		return esclusivo;
	}

	/**
	 * @param esclusivo
	 *            the esclusivo to set
	 */
	public void setEsclusivo(String esclusivo) {
		this.esclusivo = esclusivo;
	}

	/**
	 * @return the sn
	 */
	public String getSn() {
		return sn;
	}

	/**
	 * @param sn
	 *            the sn to set
	 */
	public void setSn(String sn) {
		this.sn = sn;
	}

	/**
	 * @return the fenDigheCheck
	 */
	public List<FenomeniFranosiCheckBean> getFenDigheCheck() {
		return fenDigheCheck;
	}

	/**
	 * @param fenDigheCheck
	 *            the fenDigheCheck to set
	 */
	public void setFenDigheCheck(List<FenomeniFranosiCheckBean> fenDigheCheck) {
		this.fenDigheCheck = fenDigheCheck;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((descrizioneGruppo == null) ? 0 : descrizioneGruppo.hashCode());
		result = prime * result + ((esclusivo == null) ? 0 : esclusivo.hashCode());
		result = prime * result + ((fenDigheCheck == null) ? 0 : fenDigheCheck.hashCode());
		result = prime * result + ((sn == null) ? 0 : sn.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof FenomeniFranosiGruppoBean)) {
			return false;
		}
		FenomeniFranosiGruppoBean other = (FenomeniFranosiGruppoBean) obj;
		if (descrizioneGruppo == null) {
			if (other.descrizioneGruppo != null) {
				return false;
			}
		} else if (!descrizioneGruppo.equals(other.descrizioneGruppo)) {
			return false;
		}
		if (esclusivo == null) {
			if (other.esclusivo != null) {
				return false;
			}
		} else if (!esclusivo.equals(other.esclusivo)) {
			return false;
		}
		if (fenDigheCheck == null) {
			if (other.fenDigheCheck != null) {
				return false;
			}
		} else if (!fenDigheCheck.equals(other.fenDigheCheck)) {
			return false;
		}
		if (sn == null) {
			if (other.sn != null) {
				return false;
			}
		} else if (!sn.equals(other.sn)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FenomeniFranosiGruppoBean [descrizioneGruppo=" + descrizioneGruppo + ", esclusivo=" + esclusivo + ", sn=" + sn + ", fenDigheCheck="
				+ fenDigheCheck + "]";
	}
}
