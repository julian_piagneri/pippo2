package it.cinqueemmeinfo.dammap.bean.entities;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

public class ProvinciaBean extends BasicEntityBean {

	private static final long serialVersionUID = -8706393232925127405L;

	private String sigla;
	private String nomeRegione;
	private String nome;
	private Integer codiceIstatProvincia;
	private Integer codiceIstatRegione;
	
	public ProvinciaBean() { }
	
	public ProvinciaBean(String id, String sigla,	String nomeRegione
						, String nome, Integer codiceIstatProvincia, Integer codiceIstatRegione
						, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.sigla = sigla;
		this.nomeRegione = nomeRegione;
		this.nome = nome;
		this.codiceIstatProvincia = codiceIstatProvincia;
		this.codiceIstatRegione = codiceIstatRegione;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNomeRegione() {
		return nomeRegione;
	}

	public void setNomeRegione(String nomeRegione) {
		this.nomeRegione = nomeRegione;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCodiceIstatProvincia() {
		return codiceIstatProvincia;
	}

	public void setCodiceIstatProvincia(Integer codiceIstatProvincia) {
		this.codiceIstatProvincia = codiceIstatProvincia;
	}

	public Integer getCodiceIstatRegione() {
		return codiceIstatRegione;
	}

	public void setCodiceIstatRegione(Integer codiceIstatRegione) {
		this.codiceIstatRegione = codiceIstatRegione;
	}
	
	
}
