package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * Bean related to 'Scheda Rivalutazione idrologico-idraulica: Scheda Invaso'
 * 
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SchedaInvasoBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 2224399316715390345L;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Long numeroArchivio;
	private String sub;
	private String nomeDiga;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double altitudineMediaBacina = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double tempoDiCorrivizione = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double francoNettoMinimoDM1982 = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double francoNettoMinimoSchemaNtdighe = null;

	/**
	 * @param numeroArchivio
	 * @param sub
	 * @param nomeDiga
	 * @param altitudineMediaBacina
	 * @param tempoDiCorrivizione
	 * @param francoNettoMinimoDM1982
	 * @param francoNettoMinimoSchemaNtdighe
	 */
	private SchedaInvasoBean(Long numeroArchivio, String sub, String nomeDiga, Double altitudineMediaBacina, Double tempoDiCorrivizione,
			Double francoNettoMinimoDM1982, Double francoNettoMinimoSchemaNtdighe) {
		super();
		this.numeroArchivio = numeroArchivio;
		this.sub = sub;
		this.nomeDiga = nomeDiga;
		this.altitudineMediaBacina = altitudineMediaBacina;
		this.tempoDiCorrivizione = tempoDiCorrivizione;
		this.francoNettoMinimoDM1982 = francoNettoMinimoDM1982;
		this.francoNettoMinimoSchemaNtdighe = francoNettoMinimoSchemaNtdighe;
	}

	/**
	 * 
	 */
	public SchedaInvasoBean() {
		this(null, null, null, null, null, null, null);
	}

	/**
	 * @return the numeroArchivio
	 */
	public Long getNumeroArchivio() {
		return numeroArchivio;
	}

	/**
	 * @param numeroArchivio
	 *            the numeroArchivio to set
	 */
	public void setNumeroArchivio(Long numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	/**
	 * @return the sub
	 */
	public String getSub() {
		return sub;
	}

	/**
	 * @param sub
	 *            the sub to set
	 */
	public void setSub(String sub) {
		this.sub = sub;
	}

	/**
	 * @return the nomeDiga
	 */
	public String getNomeDiga() {
		return nomeDiga;
	}

	/**
	 * @param nomeDiga
	 *            the nomeDiga to set
	 */
	public void setNomeDiga(String nomeDiga) {
		this.nomeDiga = nomeDiga;
	}

	/**
	 * @return the altitudineMediaBacina
	 */
	public Double getAltitudineMediaBacina() {
		return altitudineMediaBacina;
	}

	/**
	 * @param altitudineMediaBacina
	 *            the altitudineMediaBacina to set
	 */
	public void setAltitudineMediaBacina(Double altitudineMediaBacina) {
		this.altitudineMediaBacina = altitudineMediaBacina;
	}

	/**
	 * @return the tempoDiCorrivizione
	 */
	public Double getTempoDiCorrivizione() {
		return tempoDiCorrivizione;
	}

	/**
	 * @param tempoDiCorrivizione
	 *            the tempoDiCorrivizione to set
	 */
	public void setTempoDiCorrivizione(Double tempoDiCorrivizione) {
		this.tempoDiCorrivizione = tempoDiCorrivizione;
	}

	/**
	 * @return the francoNettoMinimoDM1982
	 */
	public Double getFrancoNettoMinimoDM1982() {
		return francoNettoMinimoDM1982;
	}

	/**
	 * @param francoNettoMinimoDM1982
	 *            the francoNettoMinimoDM1982 to set
	 */
	public void setFrancoNettoMinimoDM1982(Double francoNettoMinimoDM1982) {
		this.francoNettoMinimoDM1982 = francoNettoMinimoDM1982;
	}

	/**
	 * @return the francoNettoMinimoSchemaNtdighe
	 */
	public Double getFrancoNettoMinimoSchemaNtdighe() {
		return francoNettoMinimoSchemaNtdighe;
	}

	/**
	 * @param francoNettoMinimoSchemaNtdighe
	 *            the francoNettoMinimoSchemaNtdighe to set
	 */
	public void setFrancoNettoMinimoSchemaNtdighe(Double francoNettoMinimoSchemaNtdighe) {
		this.francoNettoMinimoSchemaNtdighe = francoNettoMinimoSchemaNtdighe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((altitudineMediaBacina == null) ? 0 : altitudineMediaBacina.hashCode());
		result = prime * result + ((francoNettoMinimoDM1982 == null) ? 0 : francoNettoMinimoDM1982.hashCode());
		result = prime * result + ((francoNettoMinimoSchemaNtdighe == null) ? 0 : francoNettoMinimoSchemaNtdighe.hashCode());
		result = prime * result + ((nomeDiga == null) ? 0 : nomeDiga.hashCode());
		result = prime * result + ((numeroArchivio == null) ? 0 : numeroArchivio.hashCode());
		result = prime * result + ((sub == null) ? 0 : sub.hashCode());
		result = prime * result + ((tempoDiCorrivizione == null) ? 0 : tempoDiCorrivizione.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof SchedaInvasoBean)) {
			return false;
		}
		SchedaInvasoBean other = (SchedaInvasoBean) obj;
		if (altitudineMediaBacina == null) {
			if (other.altitudineMediaBacina != null) {
				return false;
			}
		} else if (!altitudineMediaBacina.equals(other.altitudineMediaBacina)) {
			return false;
		}
		if (francoNettoMinimoDM1982 == null) {
			if (other.francoNettoMinimoDM1982 != null) {
				return false;
			}
		} else if (!francoNettoMinimoDM1982.equals(other.francoNettoMinimoDM1982)) {
			return false;
		}
		if (francoNettoMinimoSchemaNtdighe == null) {
			if (other.francoNettoMinimoSchemaNtdighe != null) {
				return false;
			}
		} else if (!francoNettoMinimoSchemaNtdighe.equals(other.francoNettoMinimoSchemaNtdighe)) {
			return false;
		}
		if (nomeDiga == null) {
			if (other.nomeDiga != null) {
				return false;
			}
		} else if (!nomeDiga.equals(other.nomeDiga)) {
			return false;
		}
		if (numeroArchivio == null) {
			if (other.numeroArchivio != null) {
				return false;
			}
		} else if (!numeroArchivio.equals(other.numeroArchivio)) {
			return false;
		}
		if (sub == null) {
			if (other.sub != null) {
				return false;
			}
		} else if (!sub.equals(other.sub)) {
			return false;
		}
		if (tempoDiCorrivizione == null) {
			if (other.tempoDiCorrivizione != null) {
				return false;
			}
		} else if (!tempoDiCorrivizione.equals(other.tempoDiCorrivizione)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SchedaInvasoBean [numeroArchivio=" + numeroArchivio + ", sub=" + sub + ", nomeDiga=" + nomeDiga + ", altitudineMediaBacina="
				+ altitudineMediaBacina + ", tempoDiCorrivizione=" + tempoDiCorrivizione + ", francoNettoMinimoDM1982=" + francoNettoMinimoDM1982
				+ ", francoNettoMinimoSchemaNtdighe=" + francoNettoMinimoSchemaNtdighe + "]";
	}
}