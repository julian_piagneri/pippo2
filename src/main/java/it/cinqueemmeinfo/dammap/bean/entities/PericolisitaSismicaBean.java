package it.cinqueemmeinfo.dammap.bean.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PericolisitaSismicaBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = 1488077671222237912L;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double accelerazioneMassima = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonSerialize(include = Inclusion.NON_NULL)
	private Double fattoreAmplificazione = null;
	private String tempoDiRitorno = null;

	/**
	 * @return the accelerazioneMassima
	 */
	public Double getAccelerazioneMassima() {
		return accelerazioneMassima;
	}

	/**
	 * @param accelerazioneMassima
	 *            the accelerazioneMassima to set
	 */
	public void setAccelerazioneMassima(Double accelerazioneMassima) {
		this.accelerazioneMassima = accelerazioneMassima;
	}

	/**
	 * @return the fattoreAmplificazione
	 */
	public Double getFattoreAmplificazione() {
		return fattoreAmplificazione;
	}

	/**
	 * @param fattoreAmplificazione
	 *            the fattoreAmplificazione to set
	 */
	public void setFattoreAmplificazione(Double fattoreAmplificazione) {
		this.fattoreAmplificazione = fattoreAmplificazione;
	}

	/**
	 * @return the tempoDiRitorno
	 */
	public String getTempoDiRitorno() {
		return tempoDiRitorno;
	}

	/**
	 * @param tempoDiRitorno
	 *            the tempoDiRitorno to set
	 */
	public void setTempoDiRitorno(String tempoDiRitorno) {
		this.tempoDiRitorno = tempoDiRitorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accelerazioneMassima == null) ? 0 : accelerazioneMassima.hashCode());
		result = prime * result + ((fattoreAmplificazione == null) ? 0 : fattoreAmplificazione.hashCode());
		result = prime * result + ((tempoDiRitorno == null) ? 0 : tempoDiRitorno.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof PericolisitaSismicaBean)) {
			return false;
		}
		PericolisitaSismicaBean other = (PericolisitaSismicaBean) obj;
		if (accelerazioneMassima == null) {
			if (other.accelerazioneMassima != null) {
				return false;
			}
		} else if (!accelerazioneMassima.equals(other.accelerazioneMassima)) {
			return false;
		}
		if (fattoreAmplificazione == null) {
			if (other.fattoreAmplificazione != null) {
				return false;
			}
		} else if (!fattoreAmplificazione.equals(other.fattoreAmplificazione)) {
			return false;
		}
		if (tempoDiRitorno == null) {
			if (other.tempoDiRitorno != null) {
				return false;
			}
		} else if (!tempoDiRitorno.equals(other.tempoDiRitorno)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PericolisitaSismicaBean [accelerazioneMassima=" + accelerazioneMassima + ", fattoreAmplificazione=" + fattoreAmplificazione
				+ ", tempoDiRitorno=" + tempoDiRitorno + "]";
	}
}
