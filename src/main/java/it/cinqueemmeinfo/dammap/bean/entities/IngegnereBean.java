package it.cinqueemmeinfo.dammap.bean.entities;

import java.util.ArrayList;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;

public class IngegnereBean extends BasicEntityBean {

	private static final long serialVersionUID = -664391240875257797L;

	private String nome;
	private String cognome;
	private ArrayList<AddressBean> indirizzo;
	private ArrayList<ContactInfoBean> contatto;
	
	public IngegnereBean() {
		this.indirizzo = new ArrayList<AddressBean>();
		this.contatto = new ArrayList<ContactInfoBean>();
	}
	
	public IngegnereBean(String id) {
		super(id, "", "", "");
		this.nome = "";
		this.cognome = "";
		this.indirizzo = new ArrayList<AddressBean>();
		this.contatto = new ArrayList<ContactInfoBean>();
	}

	public IngegnereBean(String id, String nome, String cognome, ArrayList<AddressBean> indirizzo
						, ArrayList<ContactInfoBean> contatto, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.nome = nome;
		this.cognome = cognome;
		this.indirizzo = indirizzo;
		this.contatto = contatto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public ArrayList<AddressBean> getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(ArrayList<AddressBean> indirizzo) {
		this.indirizzo = indirizzo;
	}

	public ArrayList<ContactInfoBean> getContatto() {
		return contatto;
	}

	public void setContatto(ArrayList<ContactInfoBean> contatto) {
		this.contatto = contatto;
	}
}
