package it.cinqueemmeinfo.dammap.bean.entities;

import it.cinqueemmeinfo.dammap.bean.junction.NominaIngegnereBean;

import java.io.Serializable;
import java.util.ArrayList;

public class ContactBean implements Serializable {

	private static final long serialVersionUID = 7986593134038672033L;

	// extra dam data
	private String numeroArchivio; // NUMERO_ARCHIVIO
	private String sub; // SUB
	private String nomeDiga;
	private String nomeFiumeSbarrato; // NOME_FIUME_SBARRATO
	private String nomeBacino; // NOME_BACINO
	private String descrClassDiga; // DESCRIZIONE_CLASSIFICA_DIGA
	private String volTotInvaso; // VOLUME_TOTALE_INVASO_L584
	private String quotaMax; // QUOTA_MAX
	private String quotaMaxReg; // QUOTA_MAX_REGOLAZIONE
	private String volAutoriz; // VOLUME_AUTORIZZATO
	private String quotaAutoriz; // QUOTA_AUTORIZZATA
	private String portataMaxPiena; // PORTATA_MAX_PIENA_PROG
	private String dataCertCollaudo; // DATA_CERTIFICATO_COLLAUDO
	private String nomeComune; // NOME_REGIONE
	private String fiumiDiga; // FIUMI_DIGA
	private String numeroRid;
	private String status;
	private UfficioBean uffCoord; // UFFICIO_COORD
	private UfficioBean uffPerif; // UFFICIO_PERIFERICO
	// office data
	private String teleSedeCent;
	private String faxSedeCent;
	private String teleUffPeri;
	private String faxUffPeri;
	private String dataPianoEmergenza;
	private ArrayList<AltreVociRubricaBean> altreVociRubrica;
	private CasaDiGuardiaBean casaDiGuarda;
	// table junction codes
	private ArrayList<String> idConcess;
	private ArrayList<String> idGestore;
	private ArrayList<String> idAutorita;
	private ArrayList<String> idAltraEnte;
	private ArrayList<NominaIngegnereBean> idIngResp;
	private ArrayList<NominaIngegnereBean> idIngSost;

	public ContactBean() {
		this.altreVociRubrica = new ArrayList<AltreVociRubricaBean>();
		this.idConcess = new ArrayList<String>();
		this.idGestore = new ArrayList<String>();
		this.idAutorita = new ArrayList<String>();
		this.idAltraEnte = new ArrayList<String>();
		this.idIngResp = new ArrayList<NominaIngegnereBean>();
		this.idIngSost = new ArrayList<NominaIngegnereBean>();
	}

	public ContactBean(String numeroArchivio, String sub, String nomeDiga, String nomeFiumeSbarrato, String nomeBacino, String descrClassDiga,
			String volTotInvaso, String quotaMax, String quotaMaxReg, String volAutoriz, String quotaAutoriz, String portataMaxPiena, String dataCertCollaudo,
			String nomeComune, String fiumiDiga, String numeroRid, String status, UfficioBean uffCoord, UfficioBean uffPerif, String teleSedeCent,
			String faxSedeCent, String teleUffPeri, String faxUffPeri, String dataPianoEmergenza, ArrayList<AltreVociRubricaBean> altreVociRubrica,
			CasaDiGuardiaBean casaDiGuarda, ArrayList<String> idConcess, ArrayList<String> idGestore, ArrayList<String> idAutorita,
			ArrayList<String> idAltraEnte, ArrayList<NominaIngegnereBean> idIngResp, ArrayList<NominaIngegnereBean> idIngSost) {

		super();
		this.numeroArchivio = numeroArchivio;
		this.sub = sub;
		this.nomeDiga = nomeDiga;
		this.nomeFiumeSbarrato = nomeFiumeSbarrato;
		this.nomeBacino = nomeBacino;
		this.descrClassDiga = descrClassDiga;
		this.volTotInvaso = volTotInvaso;
		this.quotaMax = quotaMax;
		this.quotaMaxReg = quotaMaxReg;
		this.volAutoriz = volAutoriz;
		this.quotaAutoriz = quotaAutoriz;
		this.portataMaxPiena = portataMaxPiena;
		this.dataCertCollaudo = dataCertCollaudo;
		this.nomeComune = nomeComune;
		this.fiumiDiga = fiumiDiga;
		this.numeroRid = numeroRid;
		this.status = status;
		this.uffCoord = uffCoord;
		this.uffPerif = uffPerif;
		this.teleSedeCent = teleSedeCent;
		this.faxSedeCent = faxSedeCent;
		this.teleUffPeri = teleUffPeri;
		this.faxUffPeri = faxUffPeri;
		this.dataPianoEmergenza = dataPianoEmergenza;
		this.altreVociRubrica = altreVociRubrica;
		this.casaDiGuarda = casaDiGuarda;
		this.idConcess = idConcess;
		this.idGestore = idGestore;
		this.idAutorita = idAutorita;
		this.idAltraEnte = idAltraEnte;
		this.idIngResp = idIngResp;
		this.idIngSost = idIngSost;
	}

	public ArrayList<String> getIdConcess() {
		return idConcess;
	}

	public void setIdConcess(ArrayList<String> idConcess) {
		this.idConcess = idConcess;
	}

	public ArrayList<String> getIdGestore() {
		return idGestore;
	}

	public void setIdGestore(ArrayList<String> idGestore) {
		this.idGestore = idGestore;
	}

	public ArrayList<String> getIdAutorita() {
		return idAutorita;
	}

	public void setIdAutorita(ArrayList<String> idAutorita) {
		this.idAutorita = idAutorita;
	}

	public ArrayList<String> getIdAltraEnte() {
		return idAltraEnte;
	}

	public void setIdAltraEnte(ArrayList<String> idAltraEnte) {
		this.idAltraEnte = idAltraEnte;
	}

	public ArrayList<NominaIngegnereBean> getIdIngResp() {
		return idIngResp;
	}

	public void setIdIngResp(ArrayList<NominaIngegnereBean> idIngResp) {
		this.idIngResp = idIngResp;
	}

	public ArrayList<NominaIngegnereBean> getIdIngSost() {
		return idIngSost;
	}

	public void setIdIngSost(ArrayList<NominaIngegnereBean> idIngSost) {
		this.idIngSost = idIngSost;
	}

	public String getTeleSedeCent() {
		return teleSedeCent;
	}

	public void setTeleSedeCent(String teleSedeCent) {
		this.teleSedeCent = teleSedeCent;
	}

	public String getFaxSedeCent() {
		return faxSedeCent;
	}

	public void setFaxSedeCent(String faxSedeCent) {
		this.faxSedeCent = faxSedeCent;
	}

	public String getTeleUffPeri() {
		return teleUffPeri;
	}

	public void setTeleUffPeri(String teleUffPeri) {
		this.teleUffPeri = teleUffPeri;
	}

	public String getFaxUffPeri() {
		return faxUffPeri;
	}

	public void setFaxUffPeri(String faxUffPeri) {
		this.faxUffPeri = faxUffPeri;
	}

	public String getDataPianoEmergenza() {
		return dataPianoEmergenza;
	}

	public void setDataPianoEmergenza(String dataPianoEmergenza) {
		this.dataPianoEmergenza = dataPianoEmergenza;
	}

	public ArrayList<AltreVociRubricaBean> getAltreVociRubrica() {
		return altreVociRubrica;
	}

	public void setAltreVociRubrica(ArrayList<AltreVociRubricaBean> altreVociRubrica) {
		this.altreVociRubrica = altreVociRubrica;
	}

	public CasaDiGuardiaBean getCasaDiGuarda() {
		return casaDiGuarda;
	}

	public void setCasaDiGuarda(CasaDiGuardiaBean casaDiGuarda) {
		this.casaDiGuarda = casaDiGuarda;
	}

	public String getNumeroArchivio() {
		return numeroArchivio;
	}

	public void setNumeroArchivio(String numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getNomeFiumeSbarrato() {
		return nomeFiumeSbarrato;
	}

	public void setNomeFiumeSbarrato(String nomeFiumeSbarrato) {
		this.nomeFiumeSbarrato = nomeFiumeSbarrato;
	}

	public String getNomeBacino() {
		return nomeBacino;
	}

	public void setNomeBacino(String nomeBacino) {
		this.nomeBacino = nomeBacino;
	}

	public String getDescrClassDiga() {
		return descrClassDiga;
	}

	public void setDescrClassDiga(String descrClassDiga) {
		this.descrClassDiga = descrClassDiga;
	}

	public String getVolTotInvaso() {
		return volTotInvaso;
	}

	public void setVolTotInvaso(String volTotInvaso) {
		this.volTotInvaso = volTotInvaso;
	}

	public String getQuotaMax() {
		return quotaMax;
	}

	public void setQuotaMax(String quotaMax) {
		this.quotaMax = quotaMax;
	}

	public String getQuotaMaxReg() {
		return quotaMaxReg;
	}

	public void setQuotaMaxReg(String quotaMaxReg) {
		this.quotaMaxReg = quotaMaxReg;
	}

	public String getVolAutoriz() {
		return volAutoriz;
	}

	public void setVolAutoriz(String volAutoriz) {
		this.volAutoriz = volAutoriz;
	}

	public String getQuotaAutoriz() {
		return quotaAutoriz;
	}

	public void setQuotaAutoriz(String quotaAutoriz) {
		this.quotaAutoriz = quotaAutoriz;
	}

	public String getPortataMaxPiena() {
		return portataMaxPiena;
	}

	public void setPortataMaxPiena(String portataMaxPiena) {
		this.portataMaxPiena = portataMaxPiena;
	}

	public String getDataCertCollaudo() {
		return dataCertCollaudo;
	}

	public void setDataCertCollaudo(String dataCertCollaudo) {
		this.dataCertCollaudo = dataCertCollaudo;
	}

	public String getNomeComune() {
		return nomeComune;
	}

	public void setNomeComune(String nomeComune) {
		this.nomeComune = nomeComune;
	}

	public String getFiumiDiga() {
		return fiumiDiga;
	}

	public void setFiumiDiga(String fiumiDiga) {
		this.fiumiDiga = fiumiDiga;
	}

	public UfficioBean getUffCoord() {
		return uffCoord;
	}

	public void setUffCoord(UfficioBean uffCoord) {
		this.uffCoord = uffCoord;
	}

	public UfficioBean getUffPerif() {
		return uffPerif;
	}

	public void setUffPerif(UfficioBean uffPerif) {
		this.uffPerif = uffPerif;
	}

	public String getNomeDiga() {
		return nomeDiga;
	}

	public void setNomeDiga(String nomeDiga) {
		this.nomeDiga = nomeDiga;
	}

	public String getNumeroRid() {
		return numeroRid;
	}

	public void setNumeroRid(String numeroRid) {
		this.numeroRid = numeroRid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
