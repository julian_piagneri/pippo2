package it.cinqueemmeinfo.dammap.bean.fields;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IdFenBean extends BasicEntityBean {

	@JsonIgnoreProperties
	private static final long serialVersionUID = -5799352359069571449L;

	private long idFen;

	public IdFenBean(long idFen) {
		super();
		setIdFen(idFen);
	}

	/**
	 * @return the idFen
	 */
	public long getIdFen() {
		return idFen;
	}

	/**
	 * @param idFen
	 *            the idFen to set
	 */
	public void setIdFen(long idFen) {
		this.idFen = idFen;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (idFen ^ (idFen >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof IdFenBean)) {
			return false;
		}
		IdFenBean other = (IdFenBean) obj;
		if (idFen != other.idFen) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IdFen [idFen=" + idFen + "]";
	}
}
