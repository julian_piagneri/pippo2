package it.cinqueemmeinfo.dammap.bean.fields;

import it.cinqueemmeinfo.dammap.bean.BasicFieldBean;

public class AddressBean extends BasicFieldBean {

	private static final long serialVersionUID = 1233647382902762145L;

	private String indirizzo;
	private String cap;
	private String nomeComune;
	private String siglaProvincia;

	public AddressBean() {
	}

	public AddressBean(String id, String idEntity, Integer order, String indirizzo, String cap, String nomeComune,
			String siglaProvincia, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {

		super(id, idEntity, order, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.indirizzo = indirizzo;
		this.cap = cap;
		this.nomeComune = nomeComune;
		this.siglaProvincia = siglaProvincia;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getNomeComune() {
		return nomeComune;
	}

	public void setNomeComune(String nomeComune) {
		this.nomeComune = nomeComune;
	}

	public String getSiglaProvincia() {
		return siglaProvincia;
	}

	public void setSiglaProvincia(String siglaProvincia) {
		this.siglaProvincia = siglaProvincia;
	}

	@Override
	public String toString() {
		return "AddressBean [getIndirizzo()=" + getIndirizzo() + ", getCap()=" + getCap() + ", getNomeComune()="
				+ getNomeComune() + ", getSiglaProvincia()=" + getSiglaProvincia() + ", getIdEntity()=" + getIdEntity()
				+ ", getOrderNum()=" + getOrderNum() + ", getId()=" + getId() + ", getLastModifiedDate()="
				+ getLastModifiedDate() + ", getLastModifiedUserId()=" + getLastModifiedUserId() + ", getIsDeleted()="
				+ getIsDeleted() + "]";
	}
}
