package it.cinqueemmeinfo.dammap.bean.fields;

import it.cinqueemmeinfo.dammap.bean.BasicFieldBean;

@Deprecated
public class EmailBean extends BasicFieldBean {

	private static final long serialVersionUID = -5526996788313093277L;

	private String emailAddress;
	
	public EmailBean(String id, String idEntity, Integer order, String emailAddress
					, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {
		
		super(id, idEntity, order, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.emailAddress = emailAddress;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public String toString() {
		return "EmailBean [getEmailAddress()=" + getEmailAddress()
				+ ", getIdEntity()=" + getIdEntity() + ", getOrderNum()="
				+ getOrderNum() + ", getId()=" + getId()
				+ ", getLastModifiedDate()=" + getLastModifiedDate()
				+ ", getLastModifiedUserId()=" + getLastModifiedUserId()
				+ ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
