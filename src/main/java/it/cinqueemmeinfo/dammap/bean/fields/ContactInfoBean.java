package it.cinqueemmeinfo.dammap.bean.fields;

import it.cinqueemmeinfo.dammap.bean.BasicFieldBean;

public class ContactInfoBean extends BasicFieldBean {

	private static final long serialVersionUID = 7167037991041165754L;

	private String contactInfo;
	private Integer contactType;

	public ContactInfoBean() {
		;
	}

	public ContactInfoBean(String id, String idEntity, Integer order, String contactInfo, Integer contactType,
			String lastModifiedDate, String lastModifiedUserId, String isDeleted) {

		super(id, idEntity, order, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.contactInfo = contactInfo;
		this.contactType = contactType;
	}

	public String getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	public Integer getContactType() {
		return contactType;
	}

	public void setContactType(Integer contactType) {
		this.contactType = contactType;
	}

	@Override
	public String toString() {
		return "ContactInfoBean [getContactInfo()=" + getContactInfo() + ", getContactType()=" + getContactType()
				+ ", getIdEntity()=" + getIdEntity() + ", getOrderNum()=" + getOrderNum() + ", getId()=" + getId()
				+ ", getLastModifiedDate()=" + getLastModifiedDate() + ", getLastModifiedUserId()="
				+ getLastModifiedUserId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}
}
