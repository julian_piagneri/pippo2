package it.cinqueemmeinfo.dammap.bean;

public class MapServer {

	private String url;
	private String project;
	private String map;
	private String exceptions;
	private String gisClientMap;
	private String lang;
	private String layers;
	private String version;
	private String serverType;

	public MapServer (String url, String project, String map, String exceptions, String gisClientMap, String lang, String layers, String version, String serverType) {
		
		this.url = url;
		this.project = project;
		this.map = map;
		this.exceptions = exceptions;
		this.gisClientMap = gisClientMap;
		this.lang = lang;
		this.layers = layers;
		this.version = version;
		this.serverType = serverType;
	}

	public String getUrl() {
		return url;
	}

	public String getProject() {
		return project;
	}

	public String getMap() {
		return map;
	}

	public String getExceptions() {
		return exceptions;
	}

	public String getGisClientMap() {
		return gisClientMap;
	}

	public String getLang() {
		return lang;
	}

	public String getLayers() {
		return layers;
	}

	public String getVersion() {
		return version;
	}

	public String getServerType() {
		return serverType;
	}

	@Override
	public String toString() {
		return "MapServer [url=" + url + ", project=" + project + ", map="
				+ map + ", exceptions=" + exceptions + ", gisClientMap="
				+ gisClientMap + ", lang=" + lang + ", layers=" + layers
				+ ", version=" + version + ", serverType=" + serverType + "]";
	}
	
}
