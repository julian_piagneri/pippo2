package it.cinqueemmeinfo.dammap.bean.junction;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

public class NominaIngegnereBean extends BasicEntityBean {

	private static final long serialVersionUID = -5624970152679564840L;
	
	private Integer progressivo;
	private Integer numeroArchivio;
	private String sub;
	private String dataNomina;
	
	public NominaIngegnereBean() { }

	public NominaIngegnereBean(String id, Integer progressivo, Integer numeroArchivio, String sub, String dataNomina
								, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.progressivo = progressivo;
		this.numeroArchivio = numeroArchivio;
		this.sub = sub;
		this.dataNomina = dataNomina;
	}

	public Integer getProgressivo() {
		return progressivo;
	}

	public void setProgressivo(Integer progressivo) {
		this.progressivo = progressivo;
	}

	public Integer getNumeroArchivio() {
		return numeroArchivio;
	}

	public void setNumeroArchivio(Integer numeroArchivio) {
		this.numeroArchivio = numeroArchivio;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getDataNomina() {
		return dataNomina;
	}

	public void setDataNomina(String dataNomina) {
		this.dataNomina = dataNomina;
	}

}
