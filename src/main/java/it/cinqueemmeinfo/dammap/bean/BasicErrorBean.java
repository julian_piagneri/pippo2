package it.cinqueemmeinfo.dammap.bean;

import java.io.Serializable;

public class BasicErrorBean implements Serializable {

	private static final long serialVersionUID = -6812284907890948085L;

	String error;
	String message;
	
	public BasicErrorBean() { }
	
	public BasicErrorBean(String error, String message) {
		super();
		this.error = error;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}
}
