package it.cinqueemmeinfo.dammap.bean;


public class BasicFieldBean extends BasicEntityBean {

	private static final long serialVersionUID = -7665458965054194663L;
	
	private String idEntity;
	private Integer orderNum;
	
	public BasicFieldBean() {}
	
	public BasicFieldBean(String id, String idEntity, Integer orderNum, String lastModifiedDate, String lastModifiedUserId
						, String isDeleted) {
		
		super(id, lastModifiedDate, lastModifiedUserId, isDeleted);
		this.idEntity = idEntity;
		this.orderNum = orderNum;
	}
	
	public String getIdEntity() {
		return idEntity;
	}
	public void setIdEntity(String idEntity) {
		this.idEntity = idEntity;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
}
