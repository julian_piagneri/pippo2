package it.cinqueemmeinfo.dammap.bean;

import java.io.Serializable;

public abstract class BasicEntityBean implements Serializable {

	private static final long serialVersionUID = -4606955603073598892L;

	private String id;
	private String lastModifiedDate;
	private String lastModifiedUserId;
	private String isDeleted;

	public BasicEntityBean() {
		this(null, null, null, null);
	}

	public BasicEntityBean(String id) {
		this(id, null, null, null);
	}

	public BasicEntityBean(String id, String lastModifiedDate, String lastModifiedUserId, String isDeleted) {
		super();
		setId(id);
		setLastModifiedDate(lastModifiedDate);
		setLastModifiedUserId(lastModifiedUserId);
		setIsDeleted(isDeleted);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public String getLastModifiedDate() {
		return lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate
	 *            the lastModifiedDate to set
	 */
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * @return the lastModifiedUserId
	 */
	public String getLastModifiedUserId() {
		return lastModifiedUserId;
	}

	/**
	 * @param lastModifiedUserId
	 *            the lastModifiedUserId to set
	 */
	public void setLastModifiedUserId(String lastModifiedUserId) {
		this.lastModifiedUserId = lastModifiedUserId;
	}

	/**
	 * @return the isDeleted
	 */
	public String getIsDeleted() {
		return isDeleted;
	}

	/**
	 * @param isDeleted
	 *            the isDeleted to set
	 */
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isDeleted == null) ? 0 : isDeleted.hashCode());
		result = prime * result + ((lastModifiedDate == null) ? 0 : lastModifiedDate.hashCode());
		result = prime * result + ((lastModifiedUserId == null) ? 0 : lastModifiedUserId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BasicEntityBean)) {
			return false;
		}
		BasicEntityBean other = (BasicEntityBean) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (isDeleted == null) {
			if (other.isDeleted != null) {
				return false;
			}
		} else if (!isDeleted.equals(other.isDeleted)) {
			return false;
		}
		if (lastModifiedDate == null) {
			if (other.lastModifiedDate != null) {
				return false;
			}
		} else if (!lastModifiedDate.equals(other.lastModifiedDate)) {
			return false;
		}
		if (lastModifiedUserId == null) {
			if (other.lastModifiedUserId != null) {
				return false;
			}
		} else if (!lastModifiedUserId.equals(other.lastModifiedUserId)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BasicEntityBean [id=" + id + ", lastModifiedDate=" + lastModifiedDate + ", lastModifiedUserId=" + lastModifiedUserId + ", isDeleted="
				+ isDeleted + "]";
	}
}