package it.cinqueemmeinfo.dammap.bean.security;

import java.util.LinkedHashSet;
import java.util.Set;

public class UserDetailsBean {
	private String nome="";
	private String cognome="";
	private String descrizione="";
	private String telUfficio="";
	private String telCellulare="";
	private String altriRecapiti="";
	private String ruolo="";
	private String categoria="";
	private Set<String> uffici=new LinkedHashSet<String>();
	private Integer pwdExpire=-1;
	private Integer warningDays=-1;

	
	public Integer getPwdExpire() {
		return pwdExpire;
	}
	public void setPwdExpire(Integer pwdExpire) {
		this.pwdExpire = pwdExpire;
	}
	public int getWarningDays() {
		return warningDays;
	}
	public void setWarningDays(int warningDays) {
		this.warningDays = warningDays;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getTelUfficio() {
		return telUfficio;
	}
	public void setTelUfficio(String telUfficio) {
		this.telUfficio = telUfficio;
	}
	public String getTelCellulare() {
		return telCellulare;
	}
	public void setTelCellulare(String telCellulare) {
		this.telCellulare = telCellulare;
	}
	public String getAltriRecapiti() {
		return altriRecapiti;
	}
	public void setAltriRecapiti(String altriRecapiti) {
		this.altriRecapiti = altriRecapiti;
	}
	public String getRuolo() {
		return ruolo;
	}
	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public Set<String> getUffici() {
		return uffici;
	}
	public void setUffici(Set<String> uffici) {
		this.uffici = uffici;
	}
	
	public void addUfficio(String ufficio){
		this.getUffici().add(ufficio);
	}
}
