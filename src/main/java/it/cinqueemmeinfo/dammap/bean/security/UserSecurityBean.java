package it.cinqueemmeinfo.dammap.bean.security;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserSecurityBean implements UserDetails {

	private static final long serialVersionUID = -865490897023623503L;

	private Integer id;
	private String username = "";
	private String password = "";
	private String newPassword = "";
	private String repeatPassword = "";
	private String preferenze = "";
	private long expires = -1;
	private boolean isActive = false;
	private Timestamp lastPwdUpdate;
	private Timestamp pwdExpireDate;
	private boolean isPwdExpired = false;
	private boolean isExternal = false;
	private Set<RoleBean> role = new HashSet<RoleBean>();
	private Set<GroupBean> group = new HashSet<GroupBean>();
	private UserDetailsBean userDetails = null;

	public UserSecurityBean() {
	}

	public UserSecurityBean(String username) {
		this.username = username;
	}

	public UserSecurityBean(Integer id, String username, String password, long expires, boolean isActive, Set<RoleBean> role, Set<GroupBean> group) {

		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.isActive = isActive;
		this.role = role;
		this.group = group;
	}

	@JsonProperty
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public String getPreferenze() {
		return preferenze;
	}

	public void setPreferenze(String preferenze) {
		this.preferenze = preferenze;
	}

	public long getExpires() {
		return expires;
	}

	public void setExpires(long expires) {
		this.expires = expires;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Set<RoleBean> getRole() {
		return role;
	}

	public void setRole(Set<RoleBean> role) {
		this.role = role;
	}

	@JsonIgnore
	public Set<GroupBean> getGroup() {
		return group;
	}

	public void setGroup(Set<GroupBean> group) {
		this.group = group;
	}

	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return role;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return this.isActive();
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return this.isActive();
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return this.isActive();
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return this.isActive();
	}

	public Timestamp getLastPwdUpdate() {
		return lastPwdUpdate;
	}

	public void setLastPwdUpdate(Timestamp lastPwdUpdate) {
		this.lastPwdUpdate = lastPwdUpdate;
	}

	public boolean isPwdExpired() {
		return isPwdExpired;
	}

	public void setPwdExpired(boolean isPwdExpired) {
		this.isPwdExpired = isPwdExpired;
	}

	public boolean isExternal() {
		return isExternal;
	}

	public void setExternal(boolean isExternal) {
		this.isExternal = isExternal;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getRepeatPassword() {
		return repeatPassword;
	}

	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}

	@JsonIgnore
	public boolean isSessionExpired() {
		return this.getExpires() <= new Date().getTime();
	}

	@JsonIgnore
	public User getDetails() {
		String userName = this.getUsername();
		String password = this.getPassword();
		boolean enabled = this.isActive();
		boolean accountNonExpired = this.isActive();
		boolean credentialsNonExpired = this.isActive();
		boolean accountNonLocked = this.isActive();

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (RoleBean role : this.getRole()) {
			authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
		}

		// Aggiunge i ruoli dei gruppi
		for (GroupBean group : this.getGroup()) {
			Set<RoleBean> roles = group.getRoles();

			for (RoleBean role : roles) {
				authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
			}
		}

		User user = new User(userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);

		return user;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + getUsername();
	}

	public UserDetailsBean getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetailsBean userDetails) {
		this.userDetails = userDetails;
	}

	/**
	 * Controlla se si sta cercando di inserire una nuova password
	 * 
	 * @return
	 */
	@JsonIgnore
	public boolean checkForNewPassword() {
		return !this.getNewPassword().equals("") && !this.getRepeatPassword().equals("") && this.getNewPassword() != null && this.getRepeatPassword() != null;
	}

	public Timestamp getPwdExpireDate() {
		return pwdExpireDate;
	}

	public void setPwdExpireDate(Timestamp pwdExpireDate) {
		this.pwdExpireDate = pwdExpireDate;
	}
}
