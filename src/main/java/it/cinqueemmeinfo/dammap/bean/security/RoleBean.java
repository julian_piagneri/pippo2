package it.cinqueemmeinfo.dammap.bean.security;

import java.io.Serializable;

import org.springframework.security.core.GrantedAuthority;

public class RoleBean implements Serializable, GrantedAuthority {

	private static final long serialVersionUID = 5940522297585957192L;
	
	private Integer id;
	private String authority;
	
	public RoleBean() { }

	public RoleBean(Integer id, String authority) {
		super();
		this.id = id;
		this.authority = authority;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RoleBean))
			return false;

		RoleBean ua = (RoleBean) obj;
		return ua.getAuthority() == this.getAuthority() || ua.getAuthority().equals(this.getAuthority());
	}

	@Override
	public int hashCode() {
		return getAuthority() == null ? 0 : getAuthority().hashCode();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + getAuthority();
	}
}
