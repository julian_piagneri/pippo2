package it.cinqueemmeinfo.dammap.bean.security;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class GroupBean implements Serializable {

	private static final long serialVersionUID = -4818297609103669174L;
	
	private Integer id;
	private String group;
	private Set<RoleBean> roles = new HashSet<RoleBean>();
	
	public GroupBean() { }

	public GroupBean(Integer id, String group, Set<RoleBean> roles) {
		super();
		this.id = id;
		this.group = group;
		this.roles = roles;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Set<RoleBean> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleBean> roles) {
		this.roles = roles;
	}

}
