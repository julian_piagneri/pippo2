package it.cinqueemmeinfo.dammap.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.DamBean;
import it.cinqueemmeinfo.dammap.bean.entities.EntityTypeBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.entities.IngegnereBean;
import it.cinqueemmeinfo.dammap.bean.entities.ProvinciaBean;
import it.cinqueemmeinfo.dammap.bean.entities.UserBean;
import it.cinqueemmeinfo.dammap.bean.fields.AddressBean;
import it.cinqueemmeinfo.dammap.bean.fields.ContactInfoBean;
import it.cinqueemmeinfo.dammap.bean.junction.NominaIngegnereBean;
import it.cinqueemmeinfo.dammap.dao.ContactsService;
import it.cinqueemmeinfo.dammap.dao.RegistryService;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;
import it.cinqueemmeinfo.dammap.utility.exceptions.JunctionAlreadyExistsException;

/**
 * 
 * @author Valerio De Rosa
 */
@Controller
@RequestMapping("/api/anagrafica")
public class RegistryController {
	
	private static final Logger logger = LoggerFactory.getLogger(RegistryController.class);
	
	@Autowired private RegistryService registryService;
	@Autowired private ContactsService contactsService;
	@Autowired ServletContext context;

	//******************* AUTORITA *************************
	
	@RequestMapping(value = "/autorita"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<AutoritaProtezioneCivileBean> getApcList() throws SQLException, AccessDeniedException { //LIST
		
		logger.info("Retrieving Autorita Protezione Civile list.");

		return registryService.getAutoritaProtezioneCivileList();
	}

	@RequestMapping(value = "/autorita/{entityId}"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody AutoritaProtezioneCivileBean getApcFromId(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //SINGLE FROM ID
		logger.info("Retrieving Autorita Protezione Civile from ID: " + entityId);
		
		return registryService.getAutoritaProtezioneCivileWithParams(new AutoritaProtezioneCivileBean(""+entityId));
	}
	
	@RequestMapping(value = "/autorita"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody AutoritaProtezioneCivileBean insertApc(@RequestBody AutoritaProtezioneCivileBean apcBean) throws SQLException, EntityNotFoundException { //INSERT
		logger.info("Creating new Autorita Protezione Civile.");

		return registryService.createAutoritaProtezioneCivile(apcBean);
	}
	
	@RequestMapping(value = "/autorita/{entityId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody AutoritaProtezioneCivileBean updateApc(@PathVariable int entityId, @RequestBody AutoritaProtezioneCivileBean apcBean) throws Exception { //UPDATE
		logger.info("Updating Autorita Protezione Civile object with id="+apcBean.getId());
		
		return registryService.updateAutoritaProtezioneCivile(apcBean);
	}

	@RequestMapping(value = "/autorita/{entityId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeApc(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //DELETE
		logger.info("Removing Autorita Protezione Civile object with ID: "+entityId);

		registryService.removeAutoritaProtezioneCivile(""+entityId);
	}

	@RequestMapping(value = "/autorita/{entityId}/contatto"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody ContactInfoBean insertApcCi(@PathVariable int entityId, @RequestBody ContactInfoBean ciBean) throws SQLException { //INSERT CONTACTINFO
		logger.info("Creating new ContactInfo for Autorita with id="+entityId);

		return registryService.createContactInfo(ciBean, new AutoritaProtezioneCivileBean(""+entityId));
	}

	@RequestMapping(value = "/autorita/{entityId}/contatto/{contactId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody ContactInfoBean updateApcCi(@PathVariable int entityId, @PathVariable int contactId, @RequestBody ContactInfoBean ciBean) throws SQLException, EntityNotFoundException { //UPDATE CONTACTINFO
		logger.info("Updating ContactInfo id="+contactId+" for Autorita with id="+entityId);
				
		return registryService.updateContactInfo(contactId, ciBean, new AutoritaProtezioneCivileBean(""+entityId));
	}

	@RequestMapping(value = "/autorita/{entityId}/contatto/{contactId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeApcCi(@PathVariable int entityId, @PathVariable int contactId) throws SQLException, EntityNotFoundException { //DELETE CONTACTINFO
		logger.info("Deleting ContactInfo id="+contactId+" for Autorita with id="+entityId);
		
		registryService.removeContactInfo(contactId, new AutoritaProtezioneCivileBean(""+entityId));
	}

	@RequestMapping(value = "/autorita/{entityId}/indirizzo"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody AddressBean insertApcAd(@PathVariable int entityId, @RequestBody AddressBean adBean) throws SQLException { //INSERT ADDRESS
		logger.info("Creating new Address for Autorita with id="+entityId);

		return registryService.createAddress(adBean, new AutoritaProtezioneCivileBean(""+entityId));
	}

	@RequestMapping(value = "/autorita/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody AddressBean updateApcAd(@PathVariable int entityId, @PathVariable int addressId, @RequestBody AddressBean adBean) throws SQLException, EntityNotFoundException { //UPDATE ADDRESS
		logger.info("Updating Address id="+addressId+" for Autorita with id="+entityId);
		
		return registryService.updateAddress(addressId, adBean, new AutoritaProtezioneCivileBean(""+entityId));
	}

	@RequestMapping(value = "/autorita/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeApcAd(@PathVariable int entityId, @PathVariable int addressId) throws SQLException, EntityNotFoundException { //DELETE ADDRESS
		logger.info("Deleting Address id="+addressId+" for Autorita with id="+entityId);
		
		registryService.removeAddress(addressId, new AutoritaProtezioneCivileBean(""+entityId));
	}

	@RequestMapping(value = "/autorita/{entityId}/diga/{damId}"
			, method = RequestMethod.POST
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody Boolean addAutoritaProtezioneCivileToDam(@PathVariable String damId, @PathVariable String entityId) throws SQLException, EntityNotFoundException { //ADD AUTORITA
		logger.info("Adding Autorita Protezione Civile with ID=" + entityId + " into Dam with ID=" + damId);

		return contactsService.addAutoritaProtezioneCivileToDam(damId, entityId);
	}
	
	@RequestMapping(value = "/autorita/{entityId}/diga/{damId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody Boolean removeAutoritaProtezioneCivileFromDam(@PathVariable String damId, @PathVariable String entityId) throws SQLException, EntityNotFoundException { //REMOVE AUTORITA
		logger.info("Removing Autorita with ID=" + entityId + " from Contatto with ID=" + damId);

		return contactsService.removeAutoritaProtezioneCivileFromDam(damId, entityId);
	}

	@RequestMapping(value = "/autorita/{entityId}/diga"
			, method = RequestMethod.GET
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<DamBean> getDamListFromAutoritaId(@PathVariable String entityId) throws SQLException { //DAM LIST FROM AUTORITA ID
		return registryService.getDamListFromAutoritaProtezioneCivileId(entityId);
	}

	@RequestMapping(value = "/autorita/{entityId}/utente/{userId}"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody void createAutoritaToUserJunction(@PathVariable String entityId, @PathVariable String userId) throws SQLException, EntityNotFoundException, JunctionAlreadyExistsException { //CREATE JUNCTION AUTORITA USER
		logger.info("Creating junction for Autorita with id="+entityId+" with User:" +userId);

		registryService.createAutoritaProtezioneCivileToUserJunction(entityId, userId);
	}

	@RequestMapping(value = "/autorita/{entityId}/utente/{userId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody void deleteAutoritaToUserJunction(@PathVariable String entityId, @PathVariable String userId) throws SQLException, EntityNotFoundException { //DELETE JUNCTION AUTORITA USER
		logger.info("Creating junction for Autorita with id="+entityId+" with User:" +userId);

		registryService.removeAutoritaProtezioneCivileToUserJunction(entityId, userId);
	}
	
	@RequestMapping(value = "/autorita/{entityId}/utente"
			, method = RequestMethod.GET
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<UserBean> getUserListFromAutoritaProtezioneCivileId(@PathVariable String entityId) throws SQLException { //USER LIST FROM AUTORITA ID
		return registryService.getUserListFromAutoritaProtezioneCivileId(entityId);
	}
	
	//******************* CONCESSIONARIO *************************
	
	@RequestMapping(value = "/concessionario"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<ConcessionarioBean> getCnList() throws SQLException { //LIST
		logger.info("Retrieving Concessionario list.");

		return registryService.getConcessionarioList();
	}
	

	@RequestMapping(value = "/concessionario/{entityId}"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody ConcessionarioBean getCnFromId(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //SINGLE FROM ID
		logger.info("Retrieving Concessionario with ID: " + entityId);

		return registryService.getConcessionarioWithParams(new ConcessionarioBean(""+entityId));
	}

	@RequestMapping(value = "/concessionario"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody ConcessionarioBean insertCn(@RequestBody ConcessionarioBean cnBean) throws SQLException, EntityNotFoundException { //INSERT
		logger.info("Creating new Concessionario.");

		return registryService.createConcessionario(cnBean);
	}
	
	@RequestMapping(value = "/concessionario/{entityId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody ConcessionarioBean updateCn(@PathVariable int entityId, @RequestBody ConcessionarioBean cnBean) throws Exception { //UPDATE
		logger.info("Updating Concessionario object with id="+cnBean.getId());
		
		return registryService.updateConcessionario(cnBean);
	}

	@RequestMapping(value = "/concessionario/{entityId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeCn(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //DELETE
		logger.info("Removing Concessionario object with ID: "+entityId);

		registryService.removeConcessionario(""+entityId);
	}

	@RequestMapping(value = "/concessionario/{entityId}/contatto"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody ContactInfoBean insertCnCi(@PathVariable int entityId, @RequestBody ContactInfoBean ciBean) throws SQLException { //INSERT CONTACTINFO
		logger.info("Creating new ContactInfo for Concessionario with id="+entityId);

		return registryService.createContactInfo(ciBean, new ConcessionarioBean(""+entityId));
	}

	@RequestMapping(value = "/concessionario/{entityId}/contatto/{contactId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody ContactInfoBean updateCnCi(@PathVariable int entityId, @PathVariable int contactId, @RequestBody ContactInfoBean ciBean) throws SQLException, EntityNotFoundException { //UPDATE CONTACTINFO
		logger.info("Updating ContactInfo id="+contactId+" for Concessionario with id="+entityId);
		
		return registryService.updateContactInfo(contactId, ciBean, new ConcessionarioBean(""+entityId));
	}

	@RequestMapping(value = "/concessionario/{entityId}/contatto/{contactId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeCnCi(@PathVariable int entityId, @PathVariable int contactId) throws SQLException, EntityNotFoundException { //DELETE CONTACTINFO
		logger.info("Deleting ContactInfo id="+contactId+" for Concessionario with id="+entityId);
		
		registryService.removeContactInfo(contactId, new ConcessionarioBean(""+entityId));
	}

	@RequestMapping(value = "/concessionario/{entityId}/indirizzo"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody AddressBean insertCnAd(@PathVariable int entityId, @RequestBody AddressBean adBean) throws SQLException { //INSERT ADDRESS
		logger.info("Creating new Address for Concessionario with id="+entityId);

		return registryService.createAddress(adBean, new ConcessionarioBean(""+entityId));
	}

	@RequestMapping(value = "/concessionario/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody AddressBean updateCnAd(@PathVariable int entityId, @PathVariable int addressId, @RequestBody AddressBean adBean) throws SQLException, EntityNotFoundException { //UPDATE ADDRESS
		logger.info("Updating Address id="+addressId+" for Concessionario with id="+entityId);
		
		return registryService.updateAddress(addressId, adBean, new ConcessionarioBean(""+entityId));
	}

	@RequestMapping(value = "/concessionario/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeCnAd(@PathVariable int entityId, @PathVariable int addressId) throws SQLException, EntityNotFoundException { //DELETE ADDRESS
		logger.info("Deleting Address id="+addressId+" for Concessionario with id="+entityId);
		
		registryService.removeAddress(addressId, new ConcessionarioBean(""+entityId));
	}

	@RequestMapping(value = "/concessionario/{entityId}/diga/{damId}"
			, method = RequestMethod.POST
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody Boolean addConcessionarioToDam(@PathVariable String damId, @PathVariable String entityId) throws SQLException, EntityNotFoundException { //ADD CONCESSIONARIO
		logger.info("Adding Concessionario with ID=" + entityId + " into Dam with ID=" + damId);

		return contactsService.addConcessionarioToDam(damId, entityId);
	}
	
	@RequestMapping(value = "/concessionario/{entityId}/diga/{damId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody Boolean removeConcessionarioFromDam(@PathVariable String damId, @PathVariable String entityId) throws SQLException, EntityNotFoundException { //REMOVE CONCESSIONARIO
		logger.info("Removing Concessionario with ID=" + entityId + " from Contatto with ID=" + damId);

		return contactsService.removeConcessionarioFromDam(damId, entityId);
	}
	
	@RequestMapping(value = "/concessionario/{entityId}/diga"
			, method = RequestMethod.GET
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<DamBean> getDamListFromConcessionarioId(@PathVariable String entityId) throws SQLException { //DAM LIST FROM ID CONCESS
		return registryService.getDamListFromConcessionarioId(entityId);
	}

	@RequestMapping(value = "/concessionario/{entityId}/utente/{userId}"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody void createConcessionarioToUserJunction(@PathVariable String entityId, @PathVariable String userId) throws SQLException, EntityNotFoundException, JunctionAlreadyExistsException { //CREATE JUNCTION CONCESS USER
		logger.info("Creating junction for Concessionario with id="+entityId+" with User:" +userId);

		registryService.createConcessionarioToUserJunction(entityId, userId);
	}

	@RequestMapping(value = "/concessionario/{entityId}/utente/{userId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody void deleteConcessionarioToUserJunction(@PathVariable String entityId, @PathVariable String userId) throws SQLException, EntityNotFoundException { //DELETE JUNCTION CONCESS USER
		logger.info("Creating junction for Concessionario with id="+entityId+" with User:" +userId);

		registryService.removeConcessionarioToUserJunction(entityId, userId);
	}

	@RequestMapping(value = "/concessionario/{entityId}/utente"
			, method = RequestMethod.GET
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<UserBean> getUserListFromConcessionarioId(@PathVariable String entityId) throws SQLException { //USER LIST FROM CONCESS ID
		return registryService.getUserListFromConcessionarioId(entityId);
	}

	@RequestMapping(value = "/assoc/concessionario/{concessId}/gestore"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<GestoreBean> getGsListFromCnId(@PathVariable int concessId) throws SQLException { //GESTORE LIST FROM ID CONCESS
		logger.info("Retrieving Gestore list with connection to Concessionario ID: " + concessId);

		return registryService.getGestoreListFromIdConcessionario(""+concessId);
	}

	@RequestMapping(value = "/assoc/concessionario/{concessId}/gestore/{gestoreId}"
					, method = RequestMethod.POST
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody void createCnGeJunction(@PathVariable int concessId, @PathVariable int gestoreId) throws SQLException, JunctionAlreadyExistsException { //CREATE JUNCTION GESTORE
		logger.info("Creating juction between Concessionario with ID "+concessId+" and Gestore ID: " + gestoreId);

		registryService.createConcessionarioGestoreJunction(concessId, gestoreId);
	}

	@RequestMapping(value = "/assoc/concessionario/{concessId}/gestore/{gestoreId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody void deleteCnGeJunction(@PathVariable int concessId, @PathVariable int gestoreId) throws SQLException, EntityNotFoundException { //DELETE JUNCTION GESTORE
		logger.info("Deleting juction between Concessionario with ID "+concessId+" and Gestore ID: " + gestoreId);

		registryService.removeConcessionarioGestoreJunction(concessId, gestoreId);
	}
	
	//******************* GESTORE *************************
	
	@RequestMapping(value = "/gestore"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<GestoreBean> getGsList() throws SQLException { //LIST
	logger.info("Retrieving Gestore list.");

		return registryService.getGestoreList();
	}
	
	@RequestMapping(value = "/gestore/{entityId}"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody GestoreBean getGsFromId(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //SINGLE FROM ID
		logger.info("Retrieving Gestore with ID: " + entityId);

		return registryService.getGestoreWithParams(new GestoreBean(""+entityId));
	}

	@RequestMapping(value = "/gestore"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody GestoreBean insertGs(@RequestBody GestoreBean gsBean) throws SQLException, EntityNotFoundException { //INSERT
		logger.info("Creating new Gestore.");

		return registryService.createGestore(gsBean);
	}
	
	@RequestMapping(value = "/gestore/{entityId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody GestoreBean updateGs(@PathVariable int entityId, @RequestBody GestoreBean gsBean) throws Exception { //UPDATE
		logger.info("Updating Gestore object with id="+gsBean.getId());
		
		return registryService.updateGestore(entityId, gsBean);
	}

	@RequestMapping(value = "/gestore/{entityId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeGs(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //DELETE
		logger.info("Removing Gestore object with ID: "+entityId);

		registryService.removeGestore(""+entityId);
	}

	@RequestMapping(value = "/gestore/{entityId}/contatto"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody ContactInfoBean insertGsCi(@PathVariable int entityId, @RequestBody ContactInfoBean ciBean) throws SQLException { //INSERT CONTACTINFO
		logger.info("Creating new ContactInfo for Gestore with id="+entityId);

		return registryService.createContactInfo(ciBean, new GestoreBean(""+entityId));
	}

	@RequestMapping(value = "/gestore/{entityId}/contatto/{contactId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody ContactInfoBean updateGsCi(@PathVariable int entityId, @PathVariable int contactId, @RequestBody ContactInfoBean ciBean) throws SQLException, EntityNotFoundException { //UPDATE CONTACTINFO
		logger.info("Updating ContactInfo id="+contactId+" for Gestore with id="+entityId);
		
		return registryService.updateContactInfo(contactId, ciBean, new GestoreBean(""+entityId));
	}

	@RequestMapping(value = "/gestore/{entityId}/contatto/{contactId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeGsCi(@PathVariable int entityId, @PathVariable int contactId) throws SQLException, EntityNotFoundException { //DELETE CONTACTINFO
		logger.info("Deleting ContactInfo id="+contactId+" for Gestore with id="+entityId);
		
		registryService.removeContactInfo(contactId, new GestoreBean(""+entityId));
	}

	@RequestMapping(value = "/gestore/{entityId}/indirizzo"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody AddressBean insertGsAd(@PathVariable int entityId, @RequestBody AddressBean adBean) throws SQLException { //INSERT ADDRESS
		logger.info("Creating new Address for Gestore with id="+entityId);

		return registryService.createAddress(adBean, new GestoreBean(""+entityId));
	}

	@RequestMapping(value = "/gestore/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody AddressBean updateGsAd(@PathVariable int entityId, @PathVariable int addressId, @RequestBody AddressBean adBean) throws SQLException, EntityNotFoundException { //UPDATE ADDRESS
		logger.info("Updating Address id="+addressId+" for Gestore with id="+entityId);
		
		return registryService.updateAddress(addressId, adBean, new GestoreBean(""+entityId));
	}

	@RequestMapping(value = "/gestore/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeGsAd(@PathVariable int entityId, @PathVariable int addressId) throws SQLException, EntityNotFoundException { //DELETE ADDRESS
		logger.info("Deleting Address id="+addressId+" for Gestore with id="+entityId);
		
		registryService.removeAddress(addressId, new GestoreBean(""+entityId));
	}
	
	@RequestMapping(value = "/gestore/{entityId}/diga/{damId}"
					, method = RequestMethod.POST
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody Boolean addGestoreToDam(@PathVariable String damId, @PathVariable String entityId) throws SQLException, EntityNotFoundException { //ADD GESTORE
		logger.info("Adding Gestore with ID=" + entityId + " into Dam with ID=" + damId);
		
		return contactsService.addGestoreToDam(damId, entityId);
	}
	
	@RequestMapping(value = "/gestore/{entityId}/diga/{damId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 	
	public @ResponseBody Boolean removeGestoreFromDam(@PathVariable String damId, @PathVariable String entityId) throws SQLException, EntityNotFoundException { //REMOVE GESTORE
		logger.info("Removing Gestore with ID=" + entityId + " from Dam with ID=" + damId);
		
		return contactsService.removeGestoreFromDam(damId, entityId);
	}
	
	@RequestMapping(value = "/gestore/{entityId}/diga"
			, method = RequestMethod.GET
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<DamBean> getDamListFromGestoreId(@PathVariable String entityId) throws SQLException { //DAM LIST FROM GESTORE ID
		return registryService.getDamListFromGestoreId(entityId);
	}

	@RequestMapping(value = "/gestore/{entityId}/utente/{userId}"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody void createGestoreToUserJunction(@PathVariable String entityId, @PathVariable String userId) throws SQLException, EntityNotFoundException, JunctionAlreadyExistsException { //CREATE JUNCTION GESTORE USER
		logger.info("Creating junction for Gestore with id="+entityId+" with User:" +userId);

		registryService.createGestoreToUserJunction(entityId, userId);
	}

	@RequestMapping(value = "/gestore/{entityId}/utente/{userId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody void deleteGestoreToUserJunction(@PathVariable String entityId, @PathVariable String userId) throws SQLException, EntityNotFoundException { //DELETE JUNCTION GESTORE USER
		logger.info("Creating junction for Gestore with id="+entityId+" with User:" +userId);

		registryService.removeGestoreToUserJunction(entityId, userId);
	}

	@RequestMapping(value = "/gestore/{entityId}/utente"
			, method = RequestMethod.GET
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<UserBean> getUserListFromGestoreId(@PathVariable String entityId) throws SQLException { //USER LIST FROM GESTORE ID
		return registryService.getUserListFromGestoreId(entityId);
	}
	
	@RequestMapping(value = "/assoc/gestore/{gestoreId}/concessionario"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<ConcessionarioBean> getCnListFromGeId(@PathVariable int gestoreId) throws SQLException { //CONCESS LIST FROM ID GESTORE
		logger.info("Retrieving Concessionario list with connection to Gestore ID: " + gestoreId);

		return registryService.getConcessionarioListFromIdGestore(""+gestoreId);
	}

	@RequestMapping(value = "/assoc/gestore/{gestoreId}/concessionario/{concessId}"
					, method = RequestMethod.POST
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody void createGeCnJunction(@PathVariable int gestoreId, @PathVariable int concessId) throws SQLException, JunctionAlreadyExistsException { //CREATE JUNCTION CONCESSIONARIO
		logger.info("Creating juction between Concessionario with ID "+concessId+" and Gestore ID: " + gestoreId);

		registryService.createConcessionarioGestoreJunction(concessId, gestoreId);
	}

	@RequestMapping(value = "/assoc/gestore/{gestoreId}/concessionario/{concessId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody void deleteGeCnJunction(@PathVariable int gestoreId, @PathVariable int concessId) throws SQLException, EntityNotFoundException { //DELETE JUNCTION CONCESSIONARIO
		logger.info("Deleting juction between Concessionario with ID "+concessId+" and Gestore ID: " + gestoreId);

		registryService.removeConcessionarioGestoreJunction(concessId, gestoreId);
	}
	
	//******************* INGEGNERE *************************
	
	@RequestMapping(value = "/ingegnere"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<IngegnereBean> getInList() throws SQLException { //LIST
		logger.info("Retrieving Ingegnere list.");
		
		return registryService.getIngegnereList();
	}
	
	@RequestMapping(value = "/ingegnere/{entityId}"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody IngegnereBean getInFromId(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //SINGLE FROM ID
		logger.info("Retrieving Ingegnere with ID: " + entityId);
		
		return registryService.getIngegnereWithParams(new IngegnereBean(""+entityId));
	}

	@RequestMapping(value = "/ingegnere"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody IngegnereBean insertIn(@RequestBody IngegnereBean inBean) throws SQLException, EntityNotFoundException { //INSERT
		logger.info("Creating new Ingegnere.");

		return registryService.createIngegnere(inBean);
	}
	
	@RequestMapping(value = "/ingegnere/{entityId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody IngegnereBean updateIn(@PathVariable int entityId, @RequestBody IngegnereBean inBean) throws Exception { //UPDATE
		logger.info("Updating Ingegnere object with id="+inBean.getId());
		
		return registryService.updateIngegnere(inBean);
	}

	@RequestMapping(value = "/ingegnere/{entityId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeIn(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //DELETE
		logger.info("Removing Ingegnere object with ID: "+entityId);

		registryService.removeIngegnere(""+entityId);
	}

	@RequestMapping(value = "/ingegnere/{entityId}/contatto"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody ContactInfoBean insertInCi(@PathVariable int entityId, @RequestBody ContactInfoBean ciBean) throws SQLException { //INSERT CONTACTINFO
		logger.info("Creating new ContactInfo for Ingegnere with id="+entityId);

		return registryService.createContactInfo(ciBean, new IngegnereBean(""+entityId));
	}

	@RequestMapping(value = "/ingegnere/{entityId}/contatto/{contactId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody ContactInfoBean updateInCi(@PathVariable int entityId, @PathVariable int contactId, @RequestBody ContactInfoBean ciBean) throws SQLException, EntityNotFoundException { //UPDATE CONTACTINFO
		logger.info("Updating ContactInfo id="+contactId+" for Ingegnere with id="+entityId);
		
		return registryService.updateContactInfo(contactId, ciBean, new IngegnereBean(""+entityId));
	}

	@RequestMapping(value = "/ingegnere/{entityId}/contatto/{contactId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeInCi(@PathVariable int entityId, @PathVariable int contactId) throws SQLException, EntityNotFoundException { //DELETE CONTACTINFO
		logger.info("Deleting ContactInfo id="+contactId+" for Ingegnere with id="+entityId);
		
		registryService.removeContactInfo(contactId, new IngegnereBean(""+entityId));
	}

	@RequestMapping(value = "/ingegnere/{entityId}/indirizzo"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody AddressBean insertInAd(@PathVariable int entityId, @RequestBody AddressBean adBean) throws SQLException { //INSERT ADDRESS
		logger.info("Creating new Address for Ingegnere with id="+entityId);

		return registryService.createAddress(adBean, new IngegnereBean(""+entityId));
	}

	@RequestMapping(value = "/ingegnere/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody AddressBean updateInAd(@PathVariable int entityId, @PathVariable int addressId, @RequestBody AddressBean adBean) throws SQLException, EntityNotFoundException { //UPDATE ADDRESS
		logger.info("Updating Address id="+addressId+" for Ingegnere with id="+entityId);
		
		return registryService.updateAddress(addressId, adBean, new IngegnereBean(""+entityId));
	}

	@RequestMapping(value = "/ingegnere/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeInAd(@PathVariable int entityId, @PathVariable int addressId) throws SQLException, EntityNotFoundException { //DELETE ADDRESS
		logger.info("Deleting Address id="+addressId+" for Ingegnere with id="+entityId);
		
		registryService.removeAddress(addressId, new IngegnereBean(""+entityId));
	}

	@RequestMapping(value = "/ingegnere/responsabile/{entityId}/diga/{damId}"
					, method = RequestMethod.POST
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody NominaIngegnereBean addIngegnereAsReponsabileToDam(@PathVariable String damId, @PathVariable String entityId, @RequestBody NominaIngegnereBean inBean) throws Exception { //ADD INGEGNERE RESPONSABILE
		logger.info("Adding Ingegnere with ID=" + entityId + " as Responsabile to Dam with ID=" + damId);
		
		inBean.setId(entityId);
		
		return contactsService.addIngegnereAsReponsabileToDam(damId, inBean);
	}

	@RequestMapping(value = "/ingegnere/responsabile/{entityId}/diga/{damId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody NominaIngegnereBean updateNominaIngegnereResponsabile(@PathVariable String damId, @PathVariable String entityId, @RequestBody NominaIngegnereBean inBean) throws Exception { //UPDATE DATA_NOMINA INGEGNERE RESPONSABILE
		logger.info("Adding Ingegnere with ID=" + entityId + " as Responsabile to Dam with ID=" + damId);
		
		inBean.setId(entityId);
		//extract numeroarchivio and sub from damId			
		inBean.setNumeroArchivio(Integer.parseInt(damId.substring(0, damId.length()-1)));
		inBean.setSub(damId.substring(damId.length()-1, damId.length()));
		
		return contactsService.updateNominaIngegnereResponsabile(inBean);
	}
	
	@RequestMapping(value = "/ingegnere/responsabile/{entityId}/diga/{damId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody Boolean removeIngegnereResponsabileFromDam(@PathVariable String damId, @PathVariable String entityId) throws EntityNotFoundException, SQLException { //REMOVE INGEGNERE RESPONSABILE
		logger.info("Removing Ingegnere Responsabile with ID=" + entityId + " from Dam with ID=" + damId);

		return contactsService.removeIngegnereResponsabileFromDam(damId, entityId);
	}

	@RequestMapping(value = "/ingegnere/sostituto/{entityId}/diga/{damId}"
					, method = RequestMethod.POST
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody NominaIngegnereBean addIngegnereAsSostitutoToDam(@PathVariable String damId, @PathVariable String entityId, @RequestBody NominaIngegnereBean inBean) throws Exception { //ADD INGEGNERE SOSTITUTO
		logger.info("Adding Ingegnere with ID=" + entityId + " as Sostituto to Dam with ID=" + damId);

		inBean.setId(entityId);
		
		return contactsService.addIngegnereAsSostitutoToDam(damId, inBean);
	}

	@RequestMapping(value = "/ingegnere/sostituto/{entityId}/diga/{damId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody NominaIngegnereBean updateNominaIngegnereSostituto(@PathVariable String damId, @PathVariable String entityId, @RequestBody NominaIngegnereBean inBean) throws Exception { //UPDATE DATA_NOMINA INGEGNERE SOSTITUTO
		logger.info("Adding Ingegnere with ID=" + entityId + " as Responsabile to Dam with ID=" + damId);
		
		inBean.setId(entityId);
		//extract numeroarchivio and sub from damId			
		inBean.setNumeroArchivio(Integer.parseInt(damId.substring(0, damId.length()-1)));
		inBean.setSub(damId.substring(damId.length()-1, damId.length()));
		
		return contactsService.updateNominaIngegnereSostituto(inBean);
	}
	
	@RequestMapping(value = "/ingegnere/sostituto/{entityId}/diga/{damId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody Boolean removeIngegnereSostitutoFromDam(@PathVariable String damId, @PathVariable String entityId) throws EntityNotFoundException, SQLException { //REMOVE INGEGNERE SOSTITUTO
		logger.info("Removing Ingegnere Sostituto with ID=" + entityId + " from Dam with ID=" + damId);
		
		return contactsService.removeIngegnereSostitutoFromDam(damId, entityId);
	}

	//******************* ALTRA ENTE *************************
	
	@RequestMapping(value = "/altraente"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<AltraEnteBean> getAeList() throws SQLException { //LIST
		logger.info("Retrieving AltraEnte list.");
		
		return registryService.getAltraEnteList();
	}
	
	@RequestMapping(value = "/altraente/{entityId}"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody AltraEnteBean getAeFromId(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //SINGLE FROM ID
		logger.info("Retrieving AltraEnte with ID: " + entityId);
		
		return registryService.getAltraEnteWithParams(new AltraEnteBean(""+entityId));
	}

	@RequestMapping(value = "/altraente"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody AltraEnteBean insertAe(@RequestBody AltraEnteBean aeBean) throws SQLException, EntityNotFoundException { //INSERT
		logger.info("Creating new AltraEnte.");

		return registryService.createAltraEnte(aeBean);
	}
	
	@RequestMapping(value = "/altraente/{entityId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody AltraEnteBean updateAe(@PathVariable int entityId, @RequestBody AltraEnteBean aeBean) throws Exception { //UPDATE
		logger.info("Updating AltraEnte object with id="+aeBean.getId());
		
		return registryService.updateAltraEnte(aeBean);
	}

	@RequestMapping(value = "/altraente/{entityId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeAe(@PathVariable int entityId) throws SQLException, EntityNotFoundException { //DELETE
		logger.info("Removing AltraEnte object with ID: "+entityId);

		registryService.removeAltraEnte(""+entityId);
	}

	@RequestMapping(value = "/altraente/{entityId}/contatto"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody ContactInfoBean insertAeCi(@PathVariable int entityId, @RequestBody ContactInfoBean ciBean) throws SQLException { //INSERT CONTACTINFO
		logger.info("Creating new ContactInfo for AltraEnte with id="+entityId);

		return registryService.createContactInfo(ciBean, new AltraEnteBean(""+entityId));
	}

	@RequestMapping(value = "/altraente/{entityId}/contatto/{contactId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody ContactInfoBean updateAeCi(@PathVariable int entityId, @PathVariable int contactId, @RequestBody ContactInfoBean ciBean) throws SQLException, EntityNotFoundException { //UPDATE CONTACTINFO
		logger.info("Updating ContactInfo id="+contactId+" for AltraEnte with id="+entityId);
		
		return registryService.updateContactInfo(contactId, ciBean, new AltraEnteBean(""+entityId));
	}

	@RequestMapping(value = "/altraente/{entityId}/contatto/{contactId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeAeCi(@PathVariable int entityId, @PathVariable int contactId) throws SQLException, EntityNotFoundException { //DELETE CONTACTINFO
		logger.info("Deleting ContactInfo id="+contactId+" for AltraEnte with id="+entityId);
		
		registryService.removeContactInfo(contactId, new AltraEnteBean(""+entityId));
	}

	@RequestMapping(value = "/altraente/{entityId}/indirizzo"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody AddressBean insertAeAd(@PathVariable int entityId, @RequestBody AddressBean adBean) throws SQLException { //INSERT ADDRESS
		logger.info("Creating new Address for AltraEnte with id="+entityId);

		return registryService.createAddress(adBean, new AltraEnteBean(""+entityId));
	}

	@RequestMapping(value = "/altraente/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.PUT
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody AddressBean updateAeAd(@PathVariable int entityId, @PathVariable int addressId, @RequestBody AddressBean adBean) throws SQLException, EntityNotFoundException { //UPDATE ADDRESS
		logger.info("Updating Address id="+addressId+" for AltraEnte with id="+entityId);
		
		return registryService.updateAddress(addressId, adBean, new AltraEnteBean(""+entityId));
	}

	@RequestMapping(value = "/altraente/{entityId}/indirizzo/{addressId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeAeAd(@PathVariable int entityId, @PathVariable int addressId) throws SQLException, EntityNotFoundException { //DELETE ADDRESS
		logger.info("Deleting Address id="+addressId+" for AltraEnte with id="+entityId);
		
		registryService.removeAddress(addressId, new AltraEnteBean(""+entityId));
	}
	
	@RequestMapping(value = "/altraente/{entityId}/diga/{damId}"
					, method = RequestMethod.POST
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody Boolean addAltraEnteToDam(@PathVariable String entityId, @PathVariable String damId) throws SQLException, EntityNotFoundException { //ADD ALTRA ENTE
		logger.info("Adding Altra Ente with ID=" + entityId + " into Dam with ID=" + damId);
		
		return contactsService.addAltraEnteToDam(damId, entityId);
	}
	
	@RequestMapping(value = "/altraente/{entityId}/diga/{damId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody Boolean removeAltraEnteFromDam(@PathVariable String entityId, @PathVariable String damId) throws SQLException, EntityNotFoundException { //REMOVE ALTRA ENTE
		logger.info("Removing Altra Ente with ID=" + entityId + " from Dam with ID=" + damId);
		
		return contactsService.removeAltraEnteFromDam(damId, entityId);
	}
	
	@RequestMapping(value = "/altraente/{entityId}/diga"
			, method = RequestMethod.GET
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<DamBean> getDamListFromAltraEnteId(@PathVariable String entityId) throws SQLException { //DAM LIST FROM ENTE ID
		return registryService.getDamListFromAltraEnteId(entityId);
	}

	@RequestMapping(value = "/altraente/{entityId}/utente/{userId}"
					, method = RequestMethod.POST
					, consumes={"application/json"})
	@ResponseStatus(HttpStatus.CREATED) 
	public @ResponseBody void createAltraEnteToUserJunction(@PathVariable String entityId, @PathVariable String userId) throws SQLException, EntityNotFoundException, JunctionAlreadyExistsException { //CREATE JUNCTION ENTE USER
		logger.info("Creating junction for Altra Ente with id="+entityId+" with User:" +userId);

		registryService.createAltraEnteToUserJunction(entityId, userId);
	}

	@RequestMapping(value = "/altraente/{entityId}/utente/{userId}"
					, method = RequestMethod.DELETE
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody void deleteAltraEnteToUserJunction(@PathVariable String entityId, @PathVariable String userId) throws SQLException, EntityNotFoundException { //DELETE JUNCTION ENTE USER
		logger.info("Creating junction for Altra Ente with id="+entityId+" with User:" +userId);

		registryService.removeAltraEnteToUserJunction(entityId, userId);
	}

	@RequestMapping(value = "/altraente/{entityId}/utente"
			, method = RequestMethod.GET
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<UserBean> getUserListFromAltraEnteId(@PathVariable String entityId) throws SQLException { //USER LIST FROM GESTORE ID
		return registryService.getUserListFromAltraEnteId(entityId);
	}
	
	//******************* USER *************************	
	
	@RequestMapping(value = "/utente"
			, method = RequestMethod.GET
			, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<UserBean> getUserList() throws SQLException { //LIST
		return registryService.getUserList();
	}
	
	//******************* TIPI ENTITA *************************
	
	@RequestMapping(value = "/elenco/provincia"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<ProvinciaBean> getProvinciaList() throws SQLException { //LIST
		logger.info("Retrieving Provincia list.");

		return registryService.getProvinciaList();
	}
	
	
	@RequestMapping(value = "/elenco/regione"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<EntityTypeBean> getRegioneList() throws SQLException { //LIST
		logger.info("Retrieving Regione list.");

		return registryService.getRegioneList();
	}
	
	@RequestMapping(value = "/elenco/tipo/ente"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<EntityTypeBean> getTipoEnteList() throws SQLException { //LIST
		logger.info("Retrieving Tipo Ente list.");

		return registryService.getTipoEnteList();
	}

	@RequestMapping(value = "/elenco/tipo/altraente"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<EntityTypeBean> getTipoAltraEnteList() throws SQLException { //LIST
		logger.info("Retrieving Tipo Altra Ente list.");

		return registryService.getTipoAltraEnteList();
	}

	@RequestMapping(value = "/elenco/tipo/autorita"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<EntityTypeBean> getTipoAutoritaList() throws SQLException { //LIST
		logger.info("Retrieving Tipo Autorita list.");
	
		return registryService.getTipoAutoritaList();
	}

	@RequestMapping(value = "/elenco/tipo/contatto"
					, method = RequestMethod.GET
					, produces={"application/json"})
	@ResponseStatus(HttpStatus.OK) 
	public @ResponseBody List<EntityTypeBean> getTipoContattoList() throws SQLException { //LIST
		logger.info("Retrieving Tipo Contatto list.");

		return registryService.getTipoContattoList();
	}
}
