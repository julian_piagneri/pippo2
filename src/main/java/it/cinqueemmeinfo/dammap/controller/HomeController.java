package it.cinqueemmeinfo.dammap.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;

import it.cinqueemmeinfo.dammap.bean.entities.DamBean;
import it.cinqueemmeinfo.dammap.bean.entities.VistaBean;
import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.dao.DamService;
import it.cinqueemmeinfo.dammap.dao.SchedaRivalutazioneService;
import it.cinqueemmeinfo.dammap.dao.UserService;
import it.cinqueemmeinfo.dammap.dao.VistaService;
import it.cinqueemmeinfo.dammap.security.UserDetailsService;

/**
 * Handles requests for the application home page.
 * 
 * @author Valerio De Rosa
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private DamService damService;
	// @Autowired ServletContext context;
	@Autowired
	UserService userService;
	@Autowired
	VistaService vistaService;
	@Autowired
	UserDetailsService userDetailsService;
	@Autowired
	SchedaRivalutazioneService schedaRivService;

	private final static String REPORT_BUILDER = "report_builder";
	private final static String ANAGRAFICA = "anagrafica";
	private final static String RUBRICA = "rubrica";
	private final static String LOGIN = "login";
	private final static String HOME = "home";

	private static final String TESTATA = "header";

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public @ResponseBody ModelAndView login() {

		ModelAndView mv = new ModelAndView(LOGIN);
		// mv.addObject("userService", userService);

		return mv;
	}
	
	@RequestMapping(value = "/paginaTestata", method = RequestMethod.GET)
	public @ResponseBody ModelAndView testata() {
		ModelAndView mv = new ModelAndView(TESTATA);

		return mv;
	}

	@RequestMapping(value = "api/logout", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> logout(HttpServletRequest request) {

		userDetailsService.logout(request);

		return new ResponseEntity<String>("Logout", HttpStatus.OK);
	}

	// @RequestMapping(value = "/{viewName}.do", method = RequestMethod.GET)
	// public @ResponseBody ModelAndView getHome(@PathVariable String viewName)
	// {
	//
	// ModelAndView mv = new ModelAndView(viewName);
	// //mv.addObject("userService", userService);
	//
	// return mv;
	// }

	@RequestMapping(value = "/{viewName:user-edit|overlay-list|user-new|cartografico|login-form|home-content|loading|glyphicon}", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getView(@PathVariable String viewName) {
		ModelAndView mv = new ModelAndView(viewName);
		return mv;
	}

	@RequestMapping(value = "sections/{sectionName:user}/{viewName:utenti}", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getSectionsView(@PathVariable String viewName, @PathVariable String sectionName) {
		ModelAndView mv = new ModelAndView("sections/" + sectionName + "/" + viewName);

		return mv;
	}

	@RequestMapping(value = "/anagrafica", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getAnagrafica() {

		ModelAndView mv = new ModelAndView(ANAGRAFICA);
		mv.addObject("userService", userService);

		return mv;
	}

	@RequestMapping(value = "/rubrica", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getRubrica() {

		ModelAndView mv = new ModelAndView(RUBRICA);
		mv.addObject("userService", userService);

		return mv;
	}

	@RequestMapping(value = "/reportBuilder", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getReportBuilder() {

		ModelAndView mv = new ModelAndView(REPORT_BUILDER);
		mv.addObject("userService", userService);

		return mv;
	}

	@RequestMapping(value = "/")
	public @ResponseBody ModelAndView getPage() {
		return new ModelAndView(HomeController.HOME);
	}

	@RequestMapping(value = "/api/diga", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<DamBean> getDamList() throws SQLException {
		logger.info("Retrieving Dam list.");

		return damService.getDamList();
	}

	@RequestMapping(value = "/api/diga/extra", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<DamBean> getDamExtraList() throws SQLException {
		logger.info("Retrieving Dam list with Extra data.");

		return damService.getDamExtraList();
	}

	@RequestMapping(value = "api/diga/favorite/{favorite}", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody String setDamsFavorite(@RequestBody ArrayList<DamBean> dams, @PathVariable Boolean favorite) throws SQLException {
		logger.info("Updating favorites for user");

		Long userId = new Long(((UserSecurityBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getId());
		damService.setDamsFavorite(dams, userId, favorite);

		return "";
	}

	@RequestMapping(value = "/api/diga/extra/filters/{filters}", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<DamBean> getDamExtraList(@PathVariable String filters)
			throws SQLException, JsonParseException, JsonMappingException, IOException {
		logger.info("Retrieving Dam list with Extra data.");

		return damService.getDamExtraList(filters);
	}

	@RequestMapping(value = "/api/damSession/put/{archivio}/{sub}", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void setDamParameters(HttpSession session, @PathVariable String archivio, @PathVariable String sub) throws SQLException {
		session.setAttribute("ParametriApplet1", archivio);
		session.setAttribute("ParametriApplet2", sub);
	}

	@RequestMapping(value = "/api/viste/all", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody JsonNode getAllViews() throws SQLException {
		logger.info("Retrieving Views list for user");
		Long userId = new Long(((UserSecurityBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getId());
		VistaBean vista = vistaService.getViewsForUser(userId);
		return vista == null ? null : vista.getStatus();
	}

	@RequestMapping(value = "/api/viste/base", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody JsonNode getBaseView() throws SQLException {
		logger.info("Retrieving Views list for user");
		JsonNode vista = vistaService.getBaseView();

		return vista;
	}

	@RequestMapping(value = "/api/viste/save", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody JsonNode saveViews(@RequestBody JsonNode viste) throws SQLException {
		logger.info("Retrieving Views list for user");

		Long userId = new Long(((UserSecurityBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getId());
		vistaService.saveViewsForUser(userId, viste);

		return viste;
	}
}