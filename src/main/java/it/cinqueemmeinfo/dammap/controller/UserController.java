package it.cinqueemmeinfo.dammap.controller;

import java.sql.SQLException;

import org.apache.http.auth.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.dao.UserService;
import it.cinqueemmeinfo.dammap.dao.macro.LoginDao;
import it.cinqueemmeinfo.dammap.security.UserAuthentication;
import it.cinqueemmeinfo.dammap.utility.UserUtility;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;
import it.cinqueemmeinfo.dammap.validation.ValidationBean;

@Controller
@RestController
@RequestMapping("/")
public class UserController {
	

	@Autowired
	LoginDao userRepository;
	@Autowired
	UserService userService;
	@Autowired
	private UserUtility userUtil;
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	// private boolean disabled=true;

	@RequestMapping(value = "/api/users/current", method = RequestMethod.GET)
	public UserSecurityBean getCurrent() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof UserAuthentication) {
			return ((UserAuthentication) authentication).getDetails();
		}
		return new UserSecurityBean(authentication.getName()); // anonymous user
																// support
	}

	/**
	 * Returns the user as a Spring security User class
	 * 
	 * @return
	 * @throws AuthenticationException
	 */
	@RequestMapping(value = "/api/users/sso", method = RequestMethod.GET)
	public User getSingleSignOn() throws AuthenticationException {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof UserAuthentication) {
			return ((UserAuthentication) authentication).toUser();
		}
		throw new AuthenticationException(); // anonymous user support
	}

	@RequestMapping(value = "/api/users/details", method = RequestMethod.GET)
	public UserSecurityBean getDetails(@RequestParam(value = "id", required = false) Integer userId)
			throws SQLException {
		if (userId == null) {
			userId = userUtil.getUserDetails().getId();
		}

		// Se non si sta aprendo la propria scheda allora devi avere il permesso
		// di scrittura sulla scheda utenti
		if (userUtil.getMaxAccessForUsersById(userId) <= 0) {
			throw new AccessDeniedException("Impossibile accedere");
		}
		return userService.getUserWithDetails(userId);
	}

	@RequestMapping(value = "/api/users/new", method = RequestMethod.GET)
	public UserSecurityBean getNewUser() throws SQLException {
		// Se non si sta aprendo la propria scheda allora devi avere il permesso
		// di scrittura sulla scheda utenti
		if (!userUtil.hasRole(UserService.CAN_ADMIN_USERS)) {
			throw new AccessDeniedException("Impossibile accedere");
		}
		UserSecurityBean newUser = userService.getNewUserBean();
		return newUser;
	}

	/**
	 * To send data for validation POST is correct
	 * 
	 * @see http://stackoverflow.com/questions/12100335/proper-rest-verb-for-
	 *      checking-if-sensitive-data-input-is-valid/12100416#12100416
	 */
	@RequestMapping(value = "/api/users/validate", method = RequestMethod.POST, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody ValidationBean validateUser(@RequestBody UserSecurityBean userBean)
			throws SQLException, EntityNotFoundException { // INSERT
		logger.info("Validating user");

		boolean isNew = false;
		if (userBean.getId() == null) {
			isNew = true;
		}
		return userService.validateUser(userBean, isNew);
	}

	// è corretto inviare tutti i dati
	// http://programmers.stackexchange.com/questions/208271/how-should-a-rest-api-handle-put-requests-to-partially-modifiable-resources
	@RequestMapping(value = "/api/users/update/{userId}", method = RequestMethod.PUT, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody UserSecurityBean updateUser(@PathVariable int userId, @RequestBody UserSecurityBean userBean)
			throws Exception { // UPDATE
		logger.info("Updating User object with id=" + userId);
		// non si può modificare un utente con un id quando nei dati c'è un
		// altro id
		if (userBean.getId() != userId) {
			throw new Exception();
		}
		if (userUtil.getMaxAccessForUsersById(userBean.getId()) <= 0) {
			throw new AccessDeniedException("Impossibile accedere");
		}
		return userService.updateUser(userBean, userId, false);
	}

	@RequestMapping(value = "/api/users/update", method = RequestMethod.POST, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody UserSecurityBean insertUser(@RequestBody UserSecurityBean userBean) throws Exception { // INSERT
		logger.info("Creating new User.");
		// Si sta usando questo metodo per salvare un utente già esistente
		if (userBean.getId() != null) {
			throw new Exception();
		}
		// Se non si sta aprendo la propria scheda allora devi avere il permesso
		// di scrittura sulla scheda utenti
		if (!userUtil.hasRole(UserService.CAN_ADMIN_USERS)) {
			throw new AccessDeniedException("Impossibile accedere");
		}
		return userService.updateUser(userBean, -1, true);
	}

	// @RequestMapping(value = "/api/users/all", method = RequestMethod.GET,
	// produces="text/plain")
	// public String encodePassForAllUsers() {
	// if(this.disabled){
	// throw new RuntimeException();
	// }
	// List<UserSecurityBean> users = userRepository.retrieveAllUsers();
	// StringBuffer queries=new StringBuffer();
	// //BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(13);
	// for (UserSecurityBean user : users) {
	// logger.info("hashing password for user: "+user.getUsername());
	// String hashedPassword = this.encodePassword(user.getPassword());
	// String query="INSERT INTO DIGHE_2X.UTENTE_EXTRA (ID_UTENTE,PASSWORD)
	// values ('"+user.getId()+"','"+hashedPassword+"'); ";
	// queries.append(query);
	// }
	// return queries.toString();
	// }

	// @RequestMapping(value = "/api/users/current", method =
	// RequestMethod.PATCH)
	// public ResponseEntity<String> changePassword(@RequestBody final
	// UserSecurityBean user) throws SQLException {
	// final Authentication authentication =
	// SecurityContextHolder.getContext().getAuthentication();
	// final UserSecurityBean currentUser =
	// userRepository.findByUsername(authentication.getName());
	//
	// /*if (user.getNewPassword() == null || user.getNewPassword().length() <
	// 4) {
	// return new ResponseEntity<String>("new password to short",
	// HttpStatus.UNPROCESSABLE_ENTITY);
	// }
	//
	// final BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder();
	// if (!pwEncoder.matches(user.getPassword(), currentUser.getPassword())) {
	// return new ResponseEntity<String>("old password mismatch",
	// HttpStatus.UNPROCESSABLE_ENTITY);
	// }
	//
	// currentUser.setPassword(pwEncoder.encode(user.getNewPassword()));*/
	// //userRepository.saveAndFlush(currentUser);
	// return new ResponseEntity<String>("password changed", HttpStatus.OK);
	// }

	// @RequestMapping(value = "/admin/api/users/{user}/grant/role/{role}",
	// method = RequestMethod.POST)
	// public ResponseEntity<String> grantRole(@PathVariable UserSecurityBean
	// user, @PathVariable String role) {
	// if (user == null) {
	// return new ResponseEntity<String>("invalid user id",
	// HttpStatus.UNPROCESSABLE_ENTITY);
	// }
	//
	// //user.grantRole(role);
	// //userRepository.saveAndFlush(user);
	// return new ResponseEntity<String>("role granted", HttpStatus.OK);
	// }

	// @RequestMapping(value = "/admin/api/users/{user}/revoke/role/{role}",
	// method = RequestMethod.POST)
	// public ResponseEntity<String> revokeRole(@PathVariable UserSecurityBean
	// user, @PathVariable String role) {
	// if (user == null) {
	// return new ResponseEntity<String>("invalid user id",
	// HttpStatus.UNPROCESSABLE_ENTITY);
	// }
	//
	// //user.revokeRole(new UserAuthority(role));
	// //userRepository.saveAndFlush(user);
	// return new ResponseEntity<String>("role revoked", HttpStatus.OK);
	// }

	// @RequestMapping(value = "/admin/api/users", method = RequestMethod.GET)
	// public List<UserSecurityBean> list() {
	// return null; //userRepository.findAll();
	// }
}