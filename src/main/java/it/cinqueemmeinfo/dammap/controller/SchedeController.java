package it.cinqueemmeinfo.dammap.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import it.cinqueemmeinfo.dammap.bean.entities.ClassificazioneSismicaDigaBean;
import it.cinqueemmeinfo.dammap.bean.entities.DateRivalutazioniIdrologicheBean;
import it.cinqueemmeinfo.dammap.bean.entities.DateTipoOrigineBean;
import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiAltroBean;
import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiGruppoBean;
import it.cinqueemmeinfo.dammap.bean.entities.FenomeniFranosiSchedaBean;
import it.cinqueemmeinfo.dammap.bean.entities.FonteBean;
import it.cinqueemmeinfo.dammap.bean.entities.FontePortateBean;
import it.cinqueemmeinfo.dammap.bean.entities.FontePortatePortateBean;
import it.cinqueemmeinfo.dammap.bean.entities.NotePortateBean;
import it.cinqueemmeinfo.dammap.bean.entities.OndaPienaBean;
import it.cinqueemmeinfo.dammap.bean.entities.PericolisitaSismicaBean;
import it.cinqueemmeinfo.dammap.bean.entities.PortateRivalutateBean;
import it.cinqueemmeinfo.dammap.bean.entities.ProgettoGetioneBean;
import it.cinqueemmeinfo.dammap.bean.entities.SchedaInvasoBean;
import it.cinqueemmeinfo.dammap.bean.entities.SnellezzaVunerabilitaBean;
import it.cinqueemmeinfo.dammap.bean.entities.TestataBean;
import it.cinqueemmeinfo.dammap.bean.entities.TipoOndePienaBean;
import it.cinqueemmeinfo.dammap.bean.fields.IdFenBean;
import it.cinqueemmeinfo.dammap.dao.SchedaClassificazioneSismicaService;
import it.cinqueemmeinfo.dammap.dao.SchedaFenomeniFranosiService;
import it.cinqueemmeinfo.dammap.dao.SchedaOndaPienaService;
import it.cinqueemmeinfo.dammap.dao.SchedaProgettoGestioneService;
import it.cinqueemmeinfo.dammap.dao.SchedaRivalutazioneService;
import it.cinqueemmeinfo.dammap.dao.TestataService;
import it.cinqueemmeinfo.dammap.dao.UserService;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;
import it.cinqueemmeinfo.dammap.utility.exceptions.NextValException;

/**
 * Handles requests for the application Schede pages.
 * 
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
@RestController
public class SchedeController {

	private static final Logger logger = LoggerFactory.getLogger(SchedeController.class);

	private static final String RIVALUTAZIONE = "schede_rivalutazione";
	private static final String SISMICA = "classificazione_sismica";
	private static final String FEN_FRANOSI = "fenomeni_franosi";
	private static final String PROGETTO_GESTIONE = "prog_gestione";
	private static final String ONDE_PIENA = "onde_piena";

	@Autowired
	private SchedaRivalutazioneService schedaRivService;
	@Autowired
	private SchedaClassificazioneSismicaService schedaSisService;
	@Autowired
	private SchedaProgettoGestioneService schedaGestService;
	@Autowired
	private SchedaFenomeniFranosiService schedaFenomeniFranosiService;
	@Autowired
	private SchedaOndaPienaService schedaOndePienaService;
	@Autowired
	private UserService userService;
	@Autowired
	private TestataService testataService;

	@RequestMapping(value = "/api/rivalutazione_idrologico_idrauliche", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public SchedaInvasoBean geSchedaInvaso(@RequestParam(value = "damID") String damID) throws SQLException {
		logger.info("Retrieving 'Scheda Invaso' list about {}.", damID);

		return schedaRivService.getSchedaInvaso(damID);
	}

	@RequestMapping(value = "/api/rivalutazione_idrologico_idrauliche/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateSchedaInvaso(@RequestBody SchedaInvasoBean toUpdate, @PathVariable String damID) throws SQLException {
		logger.info("Updating 'Scheda Invaso'");

		schedaRivService.updateSchedaInvaso(toUpdate, damID);
	}

	@RequestMapping(value = "/api/date_rivalutazioni_idrologiche", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public DateRivalutazioniIdrologicheBean geDateRivalutazioniIdrologiche(@RequestParam(value = "damID") String damID)
			throws SQLException, EntityNotFoundException {
		logger.info("Retrieving 'Date Rivalutazioni Idrologiche' about {}.", damID);

		return schedaRivService.getDateRivalutazioni(damID);
	}

	@RequestMapping(value = "/api/date_rivalutazioni_idrologiche/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateDateRivalutazioniIdrologiche(@RequestBody DateRivalutazioniIdrologicheBean bean, @PathVariable String damID)
			throws SQLException, ParseException {
		logger.info("Updating 'Date Rivalutazioni Idrologiche'.");

		schedaRivService.updateDateRivalutazioni(bean, damID);
	}

	// VIENE MODIFICATO A MANO SUL DATABASE QUINDI NON SI DEVE IMPLEMTARE IL
	// METODO PUT CORRISPONDENTE
	@RequestMapping(value = "/api/tipo_origine", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public List<DateTipoOrigineBean> getAllOrigne() throws SQLException, EntityNotFoundException {
		logger.info("Retrieving all 'Tipo Origine'.");

		return schedaRivService.getAllTipoOrigineDao();
	}

	// VIENE MODIFICATO A MANO SUL DATABASE QUINDI NON SI DEVE IMPLEMTARE IL
	// METODO PUT CORRISPONDENTE
	@RequestMapping(value = "/api/all_fonti", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public List<FonteBean> getAllFonti() throws SQLException, EntityNotFoundException {
		logger.info("Retrieving all 'Fonte'.");

		return schedaRivService.getAllFonteDao();
	}

	@RequestMapping(value = "/api/portate_note", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public NotePortateBean getNote(@RequestParam(value = "damID") String damID) throws SQLException, EntityNotFoundException {
		logger.info("Retrieving a 'NotePortateBean'.");

		return schedaRivService.getNote(damID);
	}

	@RequestMapping(value = "/api/portate_note/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateNote(@RequestBody NotePortateBean bean, @PathVariable String damID) throws SQLException {
		logger.info("Updating a 'NotePortateBean'.");

		schedaRivService.updateNote(bean, damID);
	}

	@RequestMapping(value = "/api/all_pericolisita", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public List<PericolisitaSismicaBean> getAllPericolisita(@RequestParam(value = "damID") String damID) throws SQLException, EntityNotFoundException {
		logger.info("Retrieving all 'Pericolisita' sismica'.");

		return schedaSisService.allPericolisitaDao(damID);
	}

	@RequestMapping(value = "/api/all_pericolisita/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updatePericolisita(@RequestBody List<PericolisitaSismicaBean> bean, @PathVariable String damID) throws SQLException {
		logger.info("Updating 'Pericolisita' sismica'.");

		schedaSisService.updatePericolisita(bean, damID);
	}

	@RequestMapping(value = "/api/snellezza_vulnerabilita", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public SnellezzaVunerabilitaBean getSnellezzaVunerabilita(@RequestParam(value = "damID") String damID) throws SQLException, EntityNotFoundException {
		logger.info("Retrieving a 'SnellezzaVunerabilitaBean'.");

		return schedaSisService.getSnellezzaVunerabilitaByDamID(damID);
	}

	@RequestMapping(value = "/api/snellezza_vulnerabilita/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateSnellezzaVunerabilita(@RequestBody SnellezzaVunerabilitaBean bean, @PathVariable String damID) throws SQLException {
		logger.info("Updating a 'SnellezzaVunerabilitaBean'.");

		schedaSisService.updateSnellezzaVunerabilita(bean, damID);
	}

	@RequestMapping(value = "/api/progetto_gestione", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public ProgettoGetioneBean getProgettoGestione(@RequestParam(value = "damID") String damID) throws SQLException, EntityNotFoundException {
		logger.info("Retrieving a 'ProgettoGetioneBean'.");

		return schedaGestService.getProgettoGestioneByDamID(damID);
	}

	@RequestMapping(value = "/api/progetto_gestione/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateProgettoGestione(@RequestBody ProgettoGetioneBean bean, @PathVariable String damID) throws SQLException, ParseException {
		logger.info("Updating a 'ProgettoGetioneBean'.");

		schedaGestService.updateProgettoGestione(bean, damID);
	}

	@RequestMapping(value = "/api/fenomeni_franosi", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public List<FenomeniFranosiSchedaBean> getFenomeniFranosi(@RequestParam(value = "damID") String damID) throws SQLException, EntityNotFoundException {
		logger.info("Retrieving a 'FenomeniFranosiBean'.");

		return schedaFenomeniFranosiService.getFenomeniFranosiSchedeByDamID(damID);
	}

	// TODO: quando inserisci un nuovo check o gruppo la tabella relativa alla
	// scheda non cambia valore nel campo ID_UTENTE
	@RequestMapping(value = "/api/fenomeni_franosi/gruppo/{idScheda}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateFenomeniFranosiGruppo(@RequestBody FenomeniFranosiGruppoBean gruppo, @PathVariable long idScheda)
			throws SQLException, EntityNotFoundException {
		logger.info("Updating a 'FenomeniFranosiBean'.");

		schedaFenomeniFranosiService.updateFenomeniFranosiGruppoByDamID(gruppo, idScheda);
	}

	@RequestMapping(value = "/api/fenomeni_franosi/{idScheda}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateFenomeniFranosi(@RequestBody FenomeniFranosiSchedaBean frana, @PathVariable long idScheda) throws SQLException, EntityNotFoundException {
		logger.info("Updating a 'FenomeniFranosiBean'.");

		schedaFenomeniFranosiService.updateFenomeniFranosiBySchedaID(frana, idScheda);
	}

	@RequestMapping(value = "/api/fenomeni_franosi/{idScheda}", method = RequestMethod.DELETE)
	public void deleteFenomeniFranosi(@PathVariable long idScheda) throws SQLException, EntityNotFoundException {
		logger.info("Deleting a 'FenomeniFranosiBean'.");

		schedaFenomeniFranosiService.deleteFenomeniFranosiGruppoByDamID(idScheda);
	}

	@RequestMapping(value = "/api/fenomeni_franosi/{damID}", method = RequestMethod.POST, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	public IdFenBean insertFenomeniFranosi(@PathVariable String damID, @RequestBody FenomeniFranosiSchedaBean scheda) throws SQLException, NextValException {
		logger.info("Inserting a 'FenomeniFranosiBean'.");

		return schedaFenomeniFranosiService.insertFenomeniFranosiGruppoByDamID(damID, scheda);
	}

	@RequestMapping(value = "/api/fenomeni_franosi_altro/{idScheda}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateFenomeniFranosiAltro(@RequestBody FenomeniFranosiAltroBean bean, @PathVariable long idScheda)
			throws SQLException, EntityNotFoundException {
		logger.info("Updating a 'FenomeniFranosiBean'.");

		schedaFenomeniFranosiService.updateFenomeniFranosiAltro(bean, idScheda);
	}

	@RequestMapping(value = "/api/fenomeni_franosi_altro", params = { "idFen" }, method = RequestMethod.GET, produces = { "application/json" })
	public FenomeniFranosiAltroBean getFenomeniFranosiAltro(@RequestParam(value = "idFen") long idFen) throws SQLException, EntityNotFoundException {
		logger.info("Retrieving a 'FenomeniFranosiBean'.");

		return schedaFenomeniFranosiService.getFenomeniFranosiAltroByIdFen(idFen);
	}

	@RequestMapping(value = "/api/portate", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public List<PortateRivalutateBean> getPortate(@RequestParam(value = "damID") String damID) throws SQLException {
		logger.info("Retrieving 'Portate Rivalutate' about {}.", damID);

		return schedaRivService.getPortateRivalutateDao(damID);
	}

	@RequestMapping(value = "/api/portate/{damID}", method = RequestMethod.PUT, produces = { "application/json" }, consumes = { "application/json" })
	public void updatePortate(@RequestBody List<PortateRivalutateBean> beanList, @PathVariable String damID) throws SQLException {
		logger.info("Updating 'Portate Rivalutate'.");
		
		schedaRivService.updatePortateRivalutate(beanList, damID);
	}

	@RequestMapping(value = "/api/portate_fonte", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public FontePortateBean getFonte(@RequestParam(value = "damID") String damID) throws SQLException {
		logger.info("Retrieving a 'FontePortateBean'.");

		return schedaRivService.getFonte(damID);
	}

	@RequestMapping(value = "/api/portate_fonte/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateFonte(@RequestBody FontePortateBean bean, @PathVariable String damID) throws SQLException {
		logger.info("Updating a 'FontePortateBean'.");

		schedaRivService.updateFonte(bean, damID);
	}

	@RequestMapping(value = "/api/portate_portate_fonte/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updatePortateFonte(@RequestBody FontePortatePortateBean bean, @PathVariable String damID) throws SQLException {
		logger.info("Updating a 'FontePortatePortateBean' {}", bean.toString());
		
		schedaRivService.updateFontePortate(bean, damID);
	}
	
	@RequestMapping(value = "/api/onda_piena", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public List<OndaPienaBean> getAllOndePiena(@RequestParam(value = "damID") String damID) throws SQLException, EntityNotFoundException {
		logger.info("Retrieving all 'OndePienaBean'.");

		return schedaOndePienaService.getAllOndePienaByDamID(damID);
	}

	@RequestMapping(value = "/api/onda_piena/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateAllOndePiena(@RequestBody List<OndaPienaBean> beanList, @PathVariable String damID) throws SQLException, ParseException, EntityNotFoundException {
		logger.info("Updating all 'OndePienaBean'.");

		schedaOndePienaService.updateAllOndePienaByDamID(beanList, damID);
	}
	
	@RequestMapping(value = "/api/all_tipo_onda_piena", method = RequestMethod.GET, produces = { "application/json" })
	public List<TipoOndePienaBean> getAllOndePiena() throws EntityNotFoundException, SQLException {
		logger.info("Retrieving all 'Tipo Onda Piena'.");

		return schedaOndePienaService.getAllTipoOndePiena();
	}

	@RequestMapping(value = "/api/onda_piena_info", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public OndaPienaBean getOndePienaInfo(@RequestParam(value = "damID") String damID) throws SQLException, EntityNotFoundException {
		logger.info("Retrieving all 'OndePienaBean'.");

		return schedaOndePienaService.getOndePienaInfoByDamID(damID);
	}

	@RequestMapping(value = "/api/onda_piena_info", method = RequestMethod.PUT)
	public void updateOndaPienaInfo(@RequestBody OndaPienaBean node) throws SQLException {
		logger.info("Updating OndaPiena");

		schedaOndePienaService.updateOndaPienaInfo(node);
	}

	@RequestMapping(value = "/api/testata", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public TestataBean getTestata(@RequestParam(value = "damID") String damID) throws SQLException, EntityNotFoundException {
		logger.info("Retrieving 'TestataBean'.");

		return testataService.getTestataByDamID(damID);
	}
	
	@RequestMapping(value = "/api/classificazione_sisimica_diga", params = { "damID" }, method = RequestMethod.GET, produces = { "application/json" })
	public ClassificazioneSismicaDigaBean getClassificazioneSismicaDiga(@RequestParam(value = "damID") String damID) throws SQLException {
		logger.info("Retrieving 'ClassificazioneSismicaDigaBean'.");

		return schedaSisService.getClassificazioneSismicaDigaByDamId(damID);
	}
	
	@RequestMapping(value = "/api/classificazione_sisimica_diga/{damID}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateClassificazioneSismicaDiga(@RequestBody ClassificazioneSismicaDigaBean bean, @PathVariable String damID) throws SQLException {
		logger.info("Updating a 'ClassificazioneSismicaDigaBean' {}");
		
		schedaSisService.updateClassificazioneSismicaDiga(bean, damID);
	}

	/* VIEWS */
	/////////////////////////////////////////////////////////////////////////////////////////////////
	@RequestMapping(value = "/rivalutazione", method = RequestMethod.GET)
	public ModelAndView getRivalutazione() {
		logger.info("Retrieving view: {}.", RIVALUTAZIONE);

		ModelAndView mv = new ModelAndView(RIVALUTAZIONE);
		mv.addObject("userService", userService);

		return mv;
	}

	@RequestMapping(value = "/sismica", method = RequestMethod.GET)
	public ModelAndView getPericolisita() {
		logger.info("Retrieving view: {}.", SISMICA);

		ModelAndView mv = new ModelAndView(SISMICA);
		mv.addObject("userService", userService);

		return mv;
	}

	@RequestMapping(value = "/prog_gestione", method = RequestMethod.GET)
	public ModelAndView getProgettoGestione() {
		logger.info("Retrieving view: {}.", PROGETTO_GESTIONE);

		ModelAndView mv = new ModelAndView(PROGETTO_GESTIONE);
		mv.addObject("userService", userService);

		return mv;
	}

	@RequestMapping(value = "/frane", method = RequestMethod.GET)
	public ModelAndView getfenomeniFranosi() {
		logger.info("Retrieving view: {}.", FEN_FRANOSI);

		ModelAndView mv = new ModelAndView(FEN_FRANOSI);
		mv.addObject("userService", userService);

		return mv;
	}

	@RequestMapping(value = "/onde_piena", method = RequestMethod.GET)
	public ModelAndView getOndeDiPiena() {
		logger.info("Retrieving view: {}.", ONDE_PIENA);

		ModelAndView mv = new ModelAndView(ONDE_PIENA);
		mv.addObject("userService", userService);

		return mv;
	}
}