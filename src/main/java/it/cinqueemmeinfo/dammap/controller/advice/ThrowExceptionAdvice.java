package it.cinqueemmeinfo.dammap.controller.advice;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import it.cinqueemmeinfo.dammap.bean.BasicErrorBean;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;
import it.cinqueemmeinfo.dammap.utility.exceptions.JunctionAlreadyExistsException;

@ControllerAdvice
public class ThrowExceptionAdvice {

	// TODO handle custom errors for internal occurencies (i.e. request ID not
	// present? send custom error to client)

	// TEST VALIDATION
	@SuppressWarnings("unused")
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody BasicErrorBean processValidationError(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();

		return new BasicErrorBean("prova", "prova");
	}

	@ExceptionHandler(EntityNotFoundException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody BasicErrorBean processEntityNotFoundException(EntityNotFoundException e) {

		return new BasicErrorBean(EntityNotFoundException.class.getSimpleName(), e.getMessage());
	}

	@ExceptionHandler(JunctionAlreadyExistsException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody BasicErrorBean processJunctionAlreadyExistsException(JunctionAlreadyExistsException e) {

		return new BasicErrorBean(JunctionAlreadyExistsException.class.getSimpleName(), e.getMessage());
	}

	@ExceptionHandler(NullPointerException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody BasicErrorBean processNullPointerException(NullPointerException e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));

		return new BasicErrorBean(e.getClass().toString(), sw.toString());
	}

	@ExceptionHandler(SQLException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody BasicErrorBean processSQLException(SQLException e) {

		return new BasicErrorBean(e.getSQLState(), e.getMessage());
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public @ResponseBody BasicErrorBean processAccessDeniedException(AccessDeniedException e) {

		return new BasicErrorBean(e.getClass().getSimpleName(), e.getMessage());
	}

	/*
	 * @ExceptionHandler(DataIntegrityViolationException.class)
	 * 
	 * @ResponseStatus(HttpStatus.BAD_REQUEST) public @ResponseBody
	 * BasicErrorBean
	 * processDataIntegrityViolationException(DataIntegrityViolationException e)
	 * {
	 * 
	 * return new BasicErrorBean(e.getClass().getSimpleName(), e.getMessage());
	 * }
	 */

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody BasicErrorBean handleUncaughtException(Exception ex) throws IOException {
		if (ex.getCause() != null) {
			return new BasicErrorBean(ex.getClass().getSimpleName(), ex.getCause().getMessage());
		} else {
			return new BasicErrorBean(ex.getClass().getSimpleName(), ex.getMessage());
		}
	}
}
