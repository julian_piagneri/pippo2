package it.cinqueemmeinfo.dammap.controller.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

@ControllerAdvice
public class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {
	
	//handles JSONP calls
    public JsonpAdvice() {
        super("callback");
    }
}
