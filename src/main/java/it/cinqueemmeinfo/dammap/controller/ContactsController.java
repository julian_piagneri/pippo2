package it.cinqueemmeinfo.dammap.controller;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import it.cinqueemmeinfo.dammap.bean.entities.AltreVociRubricaBean;
import it.cinqueemmeinfo.dammap.bean.entities.CasaDiGuardiaBean;
import it.cinqueemmeinfo.dammap.bean.entities.ContactBean;
import it.cinqueemmeinfo.dammap.bean.entities.DamBean;
import it.cinqueemmeinfo.dammap.dao.ContactsService;
import it.cinqueemmeinfo.dammap.utility.exceptions.EntityNotFoundException;

@Controller
@RequestMapping("/api/rubrica")
public class ContactsController {

	private static final Logger logger = LoggerFactory.getLogger(ContactsController.class);

	@Autowired
	private ContactsService contactsService;

	// ******************* RUBRICA *************************

	@RequestMapping(value = "/{damId}", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody ContactBean getContactFromId(@PathVariable String damId) throws SQLException { // LIST
																										// FROM
																										// ID
		logger.info("Retrieving Contatto rubrica from ID: " + damId);

		return contactsService.getContactFromDamId(damId);
	}

	@RequestMapping(value = "/{damId}/casaguardia", method = RequestMethod.PUT, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody CasaDiGuardiaBean updateCasaDiGuardia(@PathVariable String damId, @RequestBody CasaDiGuardiaBean cgBean) throws SQLException { // UPDATE
																																						// CASADIGUARDIA
		logger.info("Updating Casa di Guardia for Dam ID=" + damId);

		return contactsService.updateCasaDiGuardia(damId, cgBean);
	}

	@RequestMapping(value = "/{damId}/altravoce", method = RequestMethod.POST, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody AltreVociRubricaBean insertAltraVoceRubrica(@PathVariable String damId, @RequestBody AltreVociRubricaBean avBean)
			throws SQLException, EntityNotFoundException { // INSERT
															// ALTREVOCI
		logger.info("Creating new Altre Voci Rubrica.");

		return contactsService.createAltraVoceRubrica(damId, avBean);
	}

	@RequestMapping(value = "/{damId}/altravoce/{orderNum}", method = RequestMethod.PUT, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody AltreVociRubricaBean updateAltraVoceRubrica(@PathVariable String damId, @PathVariable int orderNum,
			@RequestBody AltreVociRubricaBean avBean) throws SQLException, EntityNotFoundException { // UPDATE
																										// ALTREVOCI
		logger.info("Updating Altra Voce Rubrica object for Dam ID=" + damId);

		avBean.setProgressivo(orderNum);

		return contactsService.updateAltraVoceRubrica(damId, avBean);
	}

	@RequestMapping(value = "/{damId}/altravoce/{orderNum}", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody void removeAltraVoceRubrica(@PathVariable String damId, @PathVariable int orderNum) throws SQLException, EntityNotFoundException { // DELETE
																																							// ALTRE
																																							// VOCI
		logger.info("Removing Altra Voce Rubrica object with Dam ID=" + damId + " and Order Number =" + orderNum);

		contactsService.removeAltraVoceRubrica(damId, orderNum);
	}

	// ******************* DAM *************************

	@RequestMapping(value = "/diga/{entityId}/invaso", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<DamBean> getDamListWithSameIdInvaso(@PathVariable String entityId) throws SQLException { // LIST
																														// DIGHE
																														// STESSO
																														// INVASO
		return contactsService.getDamListWithSameIdInvaso(entityId);
	}

	// ******************* AUTORITA PROTEZIONE CIVILE *************************

	// ******************* CONCESSIONARIO *************************

	// ******************* GESTORE *************************

	// ******************* INGEGNERE *************************

	// ******************* ALTRA ENTE *************************

}
