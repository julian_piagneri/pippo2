package it.cinqueemmeinfo.dammap.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/error")
public class ErrorController {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ErrorController.class);
	
	@RequestMapping(value = "/invalidtoken", produces={"application/json"})
	@ResponseStatus(HttpStatus.UNAUTHORIZED) 
	public void errorInvalidToken() throws AccessDeniedException { //INVALID TOKEN
		throw new AccessDeniedException("Unauthorized: Authentication token was either missing or invalid.");
	}
}