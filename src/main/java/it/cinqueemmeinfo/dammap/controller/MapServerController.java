package it.cinqueemmeinfo.dammap.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import it.cinqueemmeinfo.dammap.bean.MapServer;

@Deprecated
@Controller
public class MapServerController {
	
	private MapServer initMapServer() {
		return new MapServer("http://10.10.0.72/gisclient33/public/services/ows.php"
							, "freegis", "freegis", "BLANK", "1", "en", "g_hillshade.tile_hillshade"
							, "1.1.1", "mapserver");
	}
	
	@RequestMapping(value="/mapServer/", method=RequestMethod.GET)
	public @ResponseBody MapServer getMapServerConfig() {
		return initMapServer();
	}

}
