package it.cinqueemmeinfo.dammap.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import it.cinqueemmeinfo.dammap.bean.entities.MenuBean;
import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.dao.MenuService;

@Controller
@RequestMapping("/api/menu")
public class MenuController {

	private static final Logger logger = LoggerFactory.getLogger(MenuController.class);

	@Autowired
	private MenuService menuService;

	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	@Secured("ROLE_USER")
	public @ResponseBody List<MenuBean> getContactFromId(SecurityContextHolderAwareRequestWrapper request)
			throws SQLException {
		Long userId = new Long(
				((UserSecurityBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getId());
		// Long userId=new Long(1);
		logger.info("Retrieving Menu for user: " + userId);
		return this.menuService.getMenuForUser(userId);
	}

	@RequestMapping(value = "/getDam/{userId}/{archivio}/{sub}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@Secured("ROLE_USER")
	public @ResponseBody List<HashMap<String, String>> getDamDetails(SecurityContextHolderAwareRequestWrapper request,
			@PathVariable long userId, @PathVariable long archivio, @PathVariable String sub) throws SQLException {
		// Long userId=new Long(1);
		logger.info("Retrieving Dam Details for user: " + userId);
		return this.menuService.getDamDetails(userId, archivio, sub);
	}
}
