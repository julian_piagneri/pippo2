package it.cinqueemmeinfo.dammap.controller;

import it.cinqueemmeinfo.dammap.security.UnauthorizedException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ErrorsInterceptor {
	
	@ExceptionHandler(value=UnauthorizedException.class)
	public @ResponseBody String showUnauthError(UnauthorizedException e, HttpServletRequest request) { 
		return "error";
	}

}
