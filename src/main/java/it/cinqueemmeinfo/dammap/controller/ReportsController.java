package it.cinqueemmeinfo.dammap.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.JsonNode;

import it.cinqueemmeinfo.dammap.bean.entities.ReportIdsBean;
import it.cinqueemmeinfo.dammap.bean.entities.ReportTemplateBean;
import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.dao.ReportsService;

@Controller
@RequestMapping("/api/report")
public class ReportsController {
	private static final Logger logger = LoggerFactory.getLogger(ReportsController.class);

	@Value("${pentaho.report.url}")
	private String pentahoReportUrl;

	@Autowired
	private ReportsService reportsService;

	@RequestMapping(value = "/saveids", method = RequestMethod.POST, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody int saveReportIds(@RequestBody ReportIdsBean ids) throws SQLException {
		logger.info("saving report");
		Long userId = new Long(
				((UserSecurityBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getId());
		return reportsService.saveReportIds(userId, ids);
	}
	
	
	
	@RequestMapping(value = "/jsonData", method = RequestMethod.POST, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody int saveJsonReport(@RequestBody ReportIdsBean ids) throws SQLException {
		logger.info("saving json report");
		Long userId = new Long(
				((UserSecurityBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getId());
		return reportsService.saveJsonReport(userId, ids);
	}
	
	
	@RequestMapping(value = "/delete/user/current", method = RequestMethod.DELETE, consumes = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Boolean deleteReportsByCurrentUser() throws SQLException {
		logger.info("deleting reports for current user");
		Long userId = new Long(
				((UserSecurityBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getId());
		return reportsService.deleteReportByUser(userId);
	}

	@RequestMapping(value = "/jsonData/{reportId}", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody JsonNode getJsonById(@PathVariable Long reportId) throws SQLException, IOException {
		logger.info("Retrieving JSON reports");

		return reportsService.getReportById(reportId).getJsonData();
	}

	@RequestMapping(value = "/user/current", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<ReportIdsBean> getReportsByCurrentUser() throws SQLException, IOException {
		logger.info("Retrieving Reports for user");
		Long userId = new Long(
				((UserSecurityBean) SecurityContextHolder.getContext().getAuthentication().getDetails()).getId());
		return reportsService.getReportByUserId(userId);
	}

	@RequestMapping(value = "/templates", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<ReportTemplateBean> getTemplates() throws SQLException {
		logger.info("Retrieving Reports for user");
		return reportsService.getAllTemplates();
	}
}
