package it.cinqueemmeinfo.dammap.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import it.cinqueemmeinfo.dammap.bean.MapServer;
import it.cinqueemmeinfo.dammap.bean.entities.FiumeBean;
import it.cinqueemmeinfo.dammap.dao.DamService;
import it.cinqueemmeinfo.dammap.dao.MapService;

@Deprecated
@Controller
public class MapViewerController {

	private static final Logger logger = LoggerFactory.getLogger(MapViewerController.class);
	@Autowired
	private MapService mapService;
	@Autowired
	private DamService damService;
	@Value("${map.server.url}")
	String mapUrl;

	private MapServer initMapServer() {
		return new MapServer("http://10.10.0.72/gisclient33/public/services/ows.php", "freegis", "freegis", "BLANK", "1", "en", "g_hillshade.tile_hillshade",
				"1.1.1", "mapserver");
	}

	@RequestMapping(value = "/mapViewer", method = RequestMethod.GET)
	public ModelAndView mapTest(ModelMap modelMap) {

		ModelAndView mav = new ModelAndView("mapViewer");
		mav.addObject("srvCfg", initMapServer());

		return mav;
	}

	@RequestMapping(value = "/api/map/data", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody String getMapData(@RequestParam Map<String, String> allRequestParams) throws ClientProtocolException, IOException {
		logger.info("Retrieving Views list for user");
		String params = "";
		boolean first = true;
		Set<Entry<String, String>> entrySet = allRequestParams.entrySet();
		for (Entry<String, String> param : entrySet) {
			String and = "&";
			if (first) {
				first = false;
				and = "?";
			}
			params += and + param.getKey() + "=" + param.getValue();
		}
		return this.mapService.getJsonAsString(this.mapUrl + params);
	}

	@RequestMapping(value = "/api/extrafiumiinfo", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Set<FiumeBean> getDamList() throws SQLException {
		logger.info("Retrieving Dam list.");
		return damService.getRiverExtraInfo();
	}

}
