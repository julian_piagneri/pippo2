package it.cinqueemmeinfo.dammap.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Eccezione generata per inviare messaggi di errore personalizzati in caso
 * mancata autorizzazione
 * 
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends AuthenticationException {

	private static final long serialVersionUID = -1600811827158735705L;

	public UnauthorizedException(String msg) {
		super(msg);
	}

}
