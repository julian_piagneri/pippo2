package it.cinqueemmeinfo.dammap.security;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;

public class TokenAuthenticationService {

	private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";
	private static final String AUTH_PARAM_NAME = "t";
	private static final long EXPIRES = 1000 * 60 * 60 * 24; // * 10;
	public static final String USER_MAP_NAME = "userMap";

	private TokenHandler tokenHandler;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	ServletContext servletContext;

	protected TokenAuthenticationService() {
		super();
	}

	@Autowired
	public TokenAuthenticationService(@Value("${token.secret}") String secret) {
		// Genera un token a partire da una stringa impostata sul server
		tokenHandler = new TokenHandler(DatatypeConverter.parseBase64Binary(secret));
	}

	public void addAuthentication(HttpServletResponse response, UserAuthentication authentication) {
		final UserSecurityBean user = authentication.getDetails();
		user.setExpires(System.currentTimeMillis() + EXPIRES);
		// Carica la mappa con tutti gli utenti dal context
		Map<String, UserSecurityBean> userMap = this.getUsersMap();
		// Genera il token
		String token = tokenHandler.createTokenForUser(user);
		// Rimuove l'utente se è loggato con un altro token
		this.removeLoggedUsers(user, userMap);
		// Inserisce l'utente nella mappa usando la parte statica del token
		userMap.put(token, user);
		response.addHeader(AUTH_HEADER_NAME, token);
	}

	@SuppressWarnings("unchecked")
	private Map<String, UserSecurityBean> getUsersMap() {
		Map<String, UserSecurityBean> userMap = (Map<String, UserSecurityBean>) servletContext.getAttribute(USER_MAP_NAME);
		if (userMap == null) {
			// Se la mappa non c'è la crea
			userMap = new HashMap<String, UserSecurityBean>();
			servletContext.setAttribute(USER_MAP_NAME, userMap);
		}
		return userMap;
	}

	/**
	 * Removes the same user from the users map
	 */
	private void removeLoggedUsers(UserSecurityBean currentUser, Map<String, UserSecurityBean> userMap) {
		Set<Entry<String, UserSecurityBean>> users = userMap.entrySet();
		Set<String> usersToRemove = new HashSet<String>();
		for (Entry<String, UserSecurityBean> entry : users) {
			UserSecurityBean user = entry.getValue();
			// If there's already a logged user in the list or the session is
			// expired it removes the user from the map
			if (currentUser.getUsername().equalsIgnoreCase(user.getUsername()) || user.isSessionExpired()) {
				usersToRemove.add(entry.getKey());
			}
		}
		for (String userToRemove : usersToRemove) {
			userMap.remove(userToRemove);
		}
	}

	public boolean removeAuthenticationFromContext(HttpServletRequest request) {
		UserSecurityBean user = null;
		final String token = this.getTokenValue(request);
		if (token != null) {
			Map<String, UserSecurityBean> userMap = this.getUsersMap();
			user = userMap.remove(token);
		}
		return user != null;
	}

	private String getTokenValue(HttpServletRequest request) {
		// Il token può essere recuperato o dall'header X-AUTH-TOKEN oppure
		// come parametro "t" nell'url proveniente da vecchie pagine
		String tokenH = request.getHeader(AUTH_HEADER_NAME);
		if (tokenH == null) {
			tokenH = request.getParameter(AUTH_PARAM_NAME);
		}
		return tokenH == null ? request.getParameter(TokenAuthenticationService.AUTH_PARAM_NAME) : tokenH;
	}

	public Authentication getAuthenticationFromContext(HttpServletRequest request) {
		final String token = this.getTokenValue(request);
		final UserSecurityBean user;
		if (token != null) {
			Map<String, UserSecurityBean> userMap = this.getUsersMap();
			UserSecurityBean userTemp = userMap.get(token);
			if (userTemp != null) {
				// Checks wheter the session is expired
				if (userTemp.isSessionExpired()) {
					userMap.remove(token);
					return null;
				} else {
					// Reloads the user from the DB to check for updates
					// user=
					user = userDetailsService.loadUserByUsername(userTemp.getUsername());
					user.setExpires(userTemp.getExpires());
					userMap.put(token, user);
				}
				return new UserAuthentication(user);
			}
		}
		return null;
	}

	/**
	 * Unusued
	 */
	public Authentication getAuthentication(HttpServletRequest request) {
		final String token = request.getHeader(TokenAuthenticationService.AUTH_HEADER_NAME);
		if (token != null) {
			final UserSecurityBean user = tokenHandler.parseUserFromToken(token);
			if (user != null) {
				return new UserAuthentication(user);
			}
		}
		return null;
	}
}
