package it.cinqueemmeinfo.dammap.security.old;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import it.cinqueemmeinfo.dammap.utility.SignatureEncryptionUtil;

public class AccessTokenInterceptor extends HandlerInterceptorAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(AccessTokenInterceptor.class);
	
	private static final String USER_PARAM = "user";
	private static final String SIGNATURE_PARAM = "signature";
	private static final String TIMESTAMP_PARAM = "timestamp";
	private static final String INVALID_TOKEN_URL = "/error/invalidtoken";
	
	@Autowired Boolean isDebug;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

    	if (!isDebug) {
	    	SignatureEncryptionUtil seUtil = new SignatureEncryptionUtil();
	    	
	    	String userId = request.getParameter(USER_PARAM);
	    	String signature = request.getParameter(SIGNATURE_PARAM);
	    	String timestamp = request.getParameter(TIMESTAMP_PARAM);
	    	ServletContext context = request.getSession().getServletContext();
	    	
	    	try {
	    		
				//check request signature
	    		seUtil.checkSignature(context, userId, signature, timestamp);
	 
	    		/*else {
	        		response.sendError(HttpServletResponse.SC_FORBIDDEN);//sendRedirect("login.do");
	        		return false;
	        	}*/
	    	} catch (AccessDeniedException e) {
	    		///api/error/invalidtoken
	    		logger.error(e.getMessage() + " || Token="+signature);
	    		request.getRequestDispatcher(INVALID_TOKEN_URL).forward(request, response);
	    		return false;
	    	}
    	}
    	
    	return true;
    }
 
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //use this to add attributes in the modelAndView to use in the view page
    }
 
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
