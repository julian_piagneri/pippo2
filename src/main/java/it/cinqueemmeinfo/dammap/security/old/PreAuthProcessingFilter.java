package it.cinqueemmeinfo.dammap.security.old;

import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import it.cinqueemmeinfo.dammap.bean.security.RoleBean;
import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.utility.SignatureEncryptionUtil;
import it.thematica.enterprise.app.login.UserRightsEntry;

public class PreAuthProcessingFilter extends AbstractPreAuthenticatedProcessingFilter {

	private static final String USER_PARAM = "user";
	private static final String SIGNATURE_PARAM = "signature";
	private static final String TIMESTAMP_PARAM = "timestamp";
	
	/*public PreAuthProcessingFilter() { //inizializzo la chain e recupero dati utente da request
		setAuthenticationDetailsSource(new MyAuthenticationDetailsSource());
	}*/

	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) { // return utente
		// return userObject if auth
		// return null if not auth

		String userId = request.getParameter(USER_PARAM);
    	String signature = request.getParameter(SIGNATURE_PARAM);
    	String timestamp = request.getParameter(TIMESTAMP_PARAM);
    	ServletContext context = request.getSession().getServletContext();
		
    	SignatureEncryptionUtil seUtil = new SignatureEncryptionUtil();

    	try {
			//check request signature
    		seUtil.checkSignature(context, userId, signature, timestamp);

    	} catch (Exception e) { //AccessDeniedException
    		//logger.error(e.getMessage() + " || Token="+signature);
    		return null;
    	}

		// create container for pre-auth data
		return retrieveUserBean(context, userId);
	}

	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest arg0) { // return credenziali
		return "N/A";
	}

/*	public static class MyAuthenticationDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, UserSecurityBean> {
		// roles probably should be encrypted somehow
		static final String ROLES_PARAMETER = "pre_auth_roles";

		@Override
		public UserSecurityBean buildDetails(HttpServletRequest request) { //recupero dati utente dalla request
			
			String userId = request.getParameter("user");
	    	String signature = request.getParameter("signature");
	    	String timestamp = request.getParameter("timestamp");
	    	ServletContext context = request.getSession().getServletContext();
			
	    	SignatureEncryptionUtil seUtil = new SignatureEncryptionUtil();

	    	try {
				//check request signature
	    		seUtil.checkSignature(context, userId, signature, timestamp);

	    	} catch (Exception e) { //AccessDeniedException
	    		//logger.error(e.getMessage() + " || Token="+signature);
	    		return null;
	    	}

			// create container for pre-auth data
			return retrieveUserBean(context, userId);
		}
		*/
		@SuppressWarnings("rawtypes")
		private UserSecurityBean retrieveUserBean(ServletContext context, String userId) {
			
			UserRightsEntry urBean = null;
			UserSecurityBean usBean = null;
			
			//get context hashmap with old application data
			HashMap urMap = (HashMap)context.getAttribute("urMap");
			//get restKey
			Object[] oldAppData = (Object[])urMap.get(userId);
			urBean = (UserRightsEntry)oldAppData[2];
			
			if (urBean != null) {
				usBean =  new UserSecurityBean();
				usBean.setId(urBean.getId());
				usBean.setPassword("");
				usBean.setUsername(urBean.getUserId());
				usBean.setActive(true);
				
				RoleBean role = new RoleBean(1, "ROLE_USER");
				usBean.getRole().add(role);
			}

			return usBean;
		}
	//}

	@Override
	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		super.setAuthenticationManager(authenticationManager);
	}

}
