package it.cinqueemmeinfo.dammap.security.old;

import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;

public class PreAuthUserDetailsService
		implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

	@Override
	public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {

		UserSecurityBean usBean = (UserSecurityBean) token.getPrincipal();

		return usBean.getDetails();
	}

}
