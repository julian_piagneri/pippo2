package it.cinqueemmeinfo.dammap.security;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.dao.entities.DamDao;
import it.cinqueemmeinfo.dammap.dao.macro.LoginDao;

public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	private LoginDao lgDao;
	@Autowired
	private DamDao damDao;
	@Autowired
	private TokenAuthenticationService tokenService;

	private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

	/**
	 * Rimuove l'utente dall'hashmap degli utenti loggati quando questo effettua
	 * il logout
	 * 
	 * @param request
	 * @return
	 */
	public boolean logout(HttpServletRequest request) {
		return tokenService.removeAuthenticationFromContext(request);
	}

	/**
	 * Recupera un utente dato lo username
	 * 
	 * @param username
	 * @return
	 */
	public UserSecurityBean getUser(String username) {
		UserSecurityBean user;
		try {
			user = lgDao.findByUsername(username, true);
			if (user == null) {
				throw new UsernameNotFoundException("user not found");
			}
			// Imposta un utente come esterno se non ha dighe associate
			user.setExternal(damDao.countForExternal(username) <= 0);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new UsernameNotFoundException("Si è verificato un errore durante il recupero dell'utente");
		}
		return user;
	}

	@Override
	public final UserSecurityBean loadUserByUsername(String username) throws UsernameNotFoundException {
		final UserSecurityBean user = this.getUser(username);
		// Controlla che l'utente sia attivo e la password corretta
		detailsChecker.check(user);
		return user;
	}
}