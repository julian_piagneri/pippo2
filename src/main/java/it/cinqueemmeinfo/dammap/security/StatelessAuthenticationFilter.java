package it.cinqueemmeinfo.dammap.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

class StatelessAuthenticationFilter extends GenericFilterBean {

	private final TokenAuthenticationService tokenAuthenticationService;

	protected StatelessAuthenticationFilter(TokenAuthenticationService taService) {
		this.tokenAuthenticationService = taService;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		// Recupera i dati dell'utente a partire dal token, indicando a spring
		// security che l'utente è autenticato
		SecurityContextHolder.getContext().setAuthentication(tokenAuthenticationService.getAuthenticationFromContext((HttpServletRequest) req));
		// tokenAuthenticationService.getAuthentication((HttpServletRequest)
		// req));
		chain.doFilter(req, res); // always continue
	}
}