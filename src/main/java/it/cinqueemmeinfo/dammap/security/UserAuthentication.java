package it.cinqueemmeinfo.dammap.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;

/**
 * Classe model che rappresenta l'utente
 */
public class UserAuthentication implements Authentication {

	private static final long serialVersionUID = -8812952892758114560L;
	private final UserSecurityBean user;
	private boolean authenticated = true;

	public UserAuthentication(UserSecurityBean user) {
		this.user = user;
	}

	@Override
	public String getName() {
		return user.getUsername();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return user.getAuthorities();
	}

	@Override
	public Object getCredentials() {
		return user.getPassword();
	}

	@Override
	public UserSecurityBean getDetails() {
		return user;
	}

	@Override
	public Object getPrincipal() {
		return user.getUsername();
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	@Override
	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public User toUser() {
		User user = new User(this.user.getUsername(), this.user.getPassword(), this.user.isEnabled(), this.user.isAccountNonExpired(),
				this.user.isCredentialsNonExpired(), this.user.isAccountNonLocked(), this.user.getAuthorities());
		return user;
	}
}
