package it.cinqueemmeinfo.dammap.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.exception.DBConnectionException;

class StatelessLoginFilter extends AbstractAuthenticationProcessingFilter {

	private final TokenAuthenticationService tokenAuthenticationService;
	private final UserDetailsService userDetailsService;

	protected StatelessLoginFilter(String urlMapping, TokenAuthenticationService tokenAuthenticationService, UserDetailsService userDetailsService,
			AuthenticationManager authManager) {
		super(new AntPathRequestMatcher(urlMapping));
		this.userDetailsService = userDetailsService;
		this.tokenAuthenticationService = tokenAuthenticationService;
		setAuthenticationManager(authManager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {

		final UserSecurityBean user = new ObjectMapper().readValue(request.getInputStream(), UserSecurityBean.class);

		user.setUsername(user.getUsername().toUpperCase()); // usernames are
															// stored in all
															// caps.
		// user.setPassword(user.getPassword().toUpperCase()); //passwords are
		// stored in all caps.

		final UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
		return getAuthenticationManager().authenticate(loginToken);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication)
			throws IOException, ServletException {

		// Lookup the complete User object from the database and create an
		// Authentication for it
		final UserSecurityBean authenticatedUser = userDetailsService.loadUserByUsername(authentication.getName());
		final UserAuthentication userAuthentication = new UserAuthentication(authenticatedUser);

		// Add the custom token as HTTP header to the response
		tokenAuthenticationService.addAuthentication(response, userAuthentication);

		// Add the authentication to the Security context
		SecurityContextHolder.getContext().setAuthentication(userAuthentication);
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed)
			throws IOException, ServletException {
		// Metodo chiamato nel caso in cui venga lanciata una
		// UnauthorizedException
		Throwable cause = failed;
		boolean found = false;
		boolean knowException = false;
		String isUnauthError = "false";
		String failedMessage = "";

		while (!found) {
			// Cerca la root cause, oppure si ferma se abbiamo trovato una
			// UnauthorizedException
			if (cause.getCause() == null || cause.getClass() == UnauthorizedException.class) {
				found = true;
			} else {
				cause = cause.getCause();
			}
		}
		// super.unsuccessfulAuthentication(request, response, failed);
		// response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
		// failed.getMessage());

		if (cause.getClass() == UnauthorizedException.class) {

			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			isUnauthError = "true";
			failedMessage = failed.getMessage();
			knowException = true;

		}

		if (cause.getClass() == BadCredentialsException.class) {

			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			isUnauthError = "true";
			failedMessage = "Username e Password incorretti";
			knowException = true;
		}

		if (cause.getClass() == DBConnectionException.class) {

			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			isUnauthError = "false";
			failedMessage = failed.getMessage();
			knowException = true;
		}

		if (knowException) {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("{" + "\"message\": \"" + failedMessage + "\"," + "\"isUnauthError\": " + isUnauthError + "}");

		} else {
			super.unsuccessfulAuthentication(request, response, failed);
		}
	}
}