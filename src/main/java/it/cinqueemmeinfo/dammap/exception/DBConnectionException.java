package it.cinqueemmeinfo.dammap.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Eccezione generata per inviare messaggi di errore personalizzati in caso mancata autorizzazione
 * 
 */
@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
public class DBConnectionException extends AuthenticationException {

	public DBConnectionException(String msg) {
		super(msg);
	}

}
