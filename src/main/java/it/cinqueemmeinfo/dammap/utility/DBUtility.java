package it.cinqueemmeinfo.dammap.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;
import it.cinqueemmeinfo.dammap.bean.entities.AltraEnteBean;
import it.cinqueemmeinfo.dammap.bean.entities.AutoritaProtezioneCivileBean;
import it.cinqueemmeinfo.dammap.bean.entities.ConcessionarioBean;
import it.cinqueemmeinfo.dammap.bean.entities.GestoreBean;
import it.cinqueemmeinfo.dammap.bean.entities.IngegnereBean;
import it.cinqueemmeinfo.dammap.utility.exceptions.NextValException;

public class DBUtility implements Serializable {

	private Connection conn;
	// private Connection connPool;
	// private int openQueries = 0;
	private static final Logger logger = LoggerFactory.getLogger(DBUtility.class);
	private static final long serialVersionUID = 1957327507479309808L;

	public static final String ANAGRAFICA = "ANAGRAFICA";
	public static final String ANAGRAFICA_DETTAGLIO = "ANAGRAFICA_DETTAGLIO";
	public static final String RUBRICA = "RUBRICA";
	public static final String RUBRICA_ESTERNI = "RUBRICA_ESTERNI";

	// old stuff, might be useful later
	private static final String AUTORITA = "AUTORITA";
	private static final String CONCESSIONARIO = "CONCESS";
	private static final String GESTORE = "GESTORE";
	private static final String INGEGNERE = "INGEGNERE";
	private static final String ALTRA_ENTE = "ALTRA_ENTE";
	
	// nuove schede
	public static final String CLASSIFICAZIONE_SISMICA = "CLASS_SISMICA";
	public static final String RIVALUTAZIONE_IDROLOGICO_IDRAULICHE = "RIVALUTAZIONE";
	public static final String INVASO = "INVASO";
	public static final String FENOMENI_FRANOSI = "FRANE";
	public static final String PROGETTO_DI_GESTIONE = "PROG_GESTIONE";
	public static final String ONDA_DI_PIENA = "ONDE_PIENA";
	
	@Autowired
	private UserUtility usrUtil;

	@Autowired
	private DataSource dataSource;

	/**
	 * Metodo deprecato, usare getNewConnection() per creare una nuova
	 * connessione salvare la connessione in una variabile all'interno del
	 * metodo Usare closeQuietly(connection) o closeQuietly(conn, rs, ps) per
	 * chiudere la connessione
	 * 
	 * @return
	 */
	@Deprecated
	public Connection getConnection() {
		try {
			if (conn == null) {
				// opening connection to the database
				conn = dataSource.getConnection();
			}
		} catch (SQLException e) {
			logger.error("Datasource not found \nMessage '" + e.getMessage() + "'");
		}

		return conn;
	}

	// public Connection getNewConnection() {
	// try {
	// if (connPool == null) {
	// //opening connection to the database
	// connPool= dataSource.getConnection();
	// }
	// } catch (SQLException e) {
	// logger.error("Datasource not found \nMessage '"+ e.getMessage()+"'");
	// }
	// logger.info("opening connection");
	// this.openQueries++;
	// return connPool;
	// }

	public static int setClobParameter(PreparedStatement dmPs, String value, int paramsSet) throws SQLException {
		StringReader reader = new StringReader(value);
		dmPs.setCharacterStream(paramsSet++, reader, value.length());
		return paramsSet;
	}

	public int getNextSequenceId(String sequence) throws SQLException {
		String sql = "";
		PreparedStatement ps = null;
		Connection conn = null;
		ResultSet rs = null;
		int sequenceVal = -1;
		try {
			conn = this.getNewConnection();
			sql = "SELECT " + sequence + ".nextval FROM dual";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs != null && rs.next()) {
				sequenceVal = rs.getInt(1);
			} else {
				throw new SQLException("Couldn't retrieve next sequence value.");
			}
		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			sqle.printStackTrace();
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, rs, ps);
		}
		return sequenceVal;
	}

	public Connection getNewConnection() {
		try {
			// opening connection to the database
			return dataSource.getConnection();
		} catch (SQLException e) {
			logger.error("Datasource not found \nMessage '" + e.getMessage() + "'");
		}
		return null;
	}

	public static List<String> getList(ResultSet rs, String columnName) throws IOException, SQLException {
		List<String> al = new ArrayList<String>();
		Array z = rs.getArray(columnName);
		for (Object obj : (Object[]) z.getArray()) {
			try {
				String arr = (String) obj;
				al.add(arr);
			} catch (ClassCastException e) {
				System.out.println("Object is not a String");
				e.printStackTrace();
			}
		}
		return al;
	}

	@Deprecated
	/**
	 * Deprecato, usare conn.setAutoCommit(); dalla connessione data da
	 * getNewConnection() o il metodo newSetAutoCommit
	 */
	public void setAutoCommit(boolean autoCommit) {
		if (conn != null) {
			try {
				// changing autoCommit property
				conn.setAutoCommit(autoCommit);
			} catch (SQLException e) {
				logger.error("Error while changing autoCommit property \nMessage '" + e.getMessage() + "'");
			}
		}
	}

	/**
	 * Sets the connection's auto-commit mode to the given state. If a
	 * connection is in auto-commit mode, then all its SQL statements will be
	 * executed and committed as individual transactions. Otherwise, its SQL
	 * statements are grouped into transactions that are terminated by a call to
	 * either the method commit or the static method newRollback. By default,
	 * new connections are in auto-commit mode. The commit occurs when the
	 * statement completes.
	 * 
	 * NOTE: If this static method is called during a transaction and the
	 * auto-commit mode is changed, the transaction is committed. If
	 * newSetAutoCommit is called and the auto-commit mode is not changed, the
	 * call is a no-op.
	 * 
	 * @param conn
	 *            {@link Connection} to apply this static method
	 * @param autoCommit
	 *            {@code true} to enable auto-commit mode; {@code false} to
	 *            disable it
	 * 
	 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
	 */
	public static void newSetAutoCommit(Connection conn, boolean autoCommit) {
		if (conn != null) {
			try {
				conn.setAutoCommit(autoCommit);
			} catch (SQLException e) {
				logger.error("Error while changing autoCommit property \nMessage '" + e.getMessage() + "'");
			}
		}
	}

	/**
	 * Like DBtility.newSetAutoCommit(conn, {@code false})
	 * 
	 * @param conn
	 *            {@link Connection} to apply this static method
	 * @see it.cinqueemmeinfo.dammap.utility.newSetAutoCommit(Connection conn,
	 *      boolean autoCommit)
	 * 
	 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
	 */
	public static void newSetAutoCommit(Connection conn) {
		newSetAutoCommit(conn, false);
	}

	@Deprecated
	/**
	 * Deprecato, usare conn.rollback(); dalla connessione data da
	 * getNewConnection() o newRollback
	 */
	public void rollback() {
		if (conn != null) {
			try {
				logger.error("Transaction is being rolled back");
				// performing rollback of transaction
				conn.rollback();
			} catch (SQLException e) {
				logger.error("Error during transaction rollback \nMessage '" + e.getMessage() + "'");
			}
		}
	}

	/**
	 * Undoes all changes made in the transaction and releases any database
	 * locks currently held by the {@link Connection conn} object. This method
	 * should be used only when auto-commit mode has been disabled.
	 * 
	 * @param conn
	 *            {@link Connection} to apply this static method
	 */
	public static void newRollback(Connection conn) {
		if (conn != null) {
			try {
				logger.error("Transaction is being rolled back");
				conn.rollback();
			} catch (SQLException e) {
				logger.error("Error during transaction rollback \nMessage '" + e.getMessage() + "'");
			}
		}
	}

	@Deprecated
	/**
	 * Metodo deprecato, usare getNewConnection() per creare una nuova
	 * connessione salvare la connessione in una variabile all'interno del
	 * metodo Usare closeQuietly(connection) o closeQuietly(conn, rs, ps) per
	 * chiudere la connessione
	 * 
	 * @return
	 */
	public void closeConnection() {
		if (conn != null) {
			logger.info("closing conn");
			try {
				// closing connection
				conn.close();
			} catch (SQLException e) {
				logger.error("Error while closing Database connection \nMessage '" + e.getMessage() + "'");
			} finally {
				conn = null;
			}
		}
	}

	public static void closeQuietly(Connection conn, ResultSet rs, Statement ps) {
		closeQuietly(conn);
		closeQuietly(rs);
		closeQuietly(ps);
	}

	public static void closeQuietly(Connection conn) {
		if (conn != null) {
			try {
				// if(!rs.isClosed()){
				// closing resultset
				conn.close();
				// }
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error("Error while closing ResultSet \nMessage '" + e.getMessage() + "'");
			}
		}
	}

	// public void closeQuietly(Connection connection) {
	// if (this.connPool != null) {
	// //Closes a connection only if there are no more active queries
	// this.openQueries--;
	// logger.info("reducing queries");
	// if(this.openQueries<=0){
	// logger.info("closing connection");
	// try {
	// if(this.connPool!=null){
	// this.connPool.close();
	// }
	// } catch (SQLException e) {
	// e.printStackTrace();
	// logger.error("Error while closing Database connection \nMessage '"+
	// e.getMessage()+"'");
	// } finally {
	// this.openQueries=0;
	// this.connPool = null;
	// }
	// }
	// }
	// }

	public static void closeQuietly(ResultSet rs) {
		if (rs != null) {
			try {
				// if(!rs.isClosed()){
				// closing resultset
				rs.close();
				// }
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error("Error while closing ResultSet \nMessage '" + e.getMessage() + "'");
			}
		}
	}

	public static void closeQuietly(Statement ps) {
		if (ps != null) {
			try {
				// if(!ps.isClosed()){
				// closing preparedStatement
				ps.close();
				// }
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error("Error while closing PreparedStatement \nMessage '" + e.getMessage() + "'");
			}
		}
	}

	public String getQueryString(String queryName) {

		String queryString = "";

		try {
			Properties prop = new Properties();
			InputStream inputStream = DBUtility.class.getClassLoader().getResourceAsStream("/sql_oracle.properties");
			prop.load(inputStream);
			queryString = prop.getProperty(queryName);
		} catch (FileNotFoundException e) {
			logger.error("SQL properties file not found");
		} catch (IOException e) {
			logger.error("Error while parsing SQL properties file");
		}

		return queryString;
	}

	public String getEntityType(BasicEntityBean targetEntity) {
		String entityType = "";

		if (targetEntity instanceof AutoritaProtezioneCivileBean) {
			entityType = AUTORITA;
		} else if (targetEntity instanceof ConcessionarioBean) {
			entityType = CONCESSIONARIO;
		} else if (targetEntity instanceof GestoreBean) {
			entityType = GESTORE;
		} else if (targetEntity instanceof IngegnereBean) {
			entityType = INGEGNERE;
		} else if (targetEntity instanceof AltraEnteBean) {
			entityType = ALTRA_ENTE;
		}

		return entityType;
	}

	/**
	 * Retrieves the primary key from the {@link String damId} variable that
	 * linkeding up
	 * 
	 * @param damId
	 *            variable {@code String} to unpack
	 * @return returns a {@code Map} containing the primary key
	 * @throws IndexOutOfBoundsException
	 *             if the length of the variable does not conform
	 * @throws NumberFormatException
	 *             if the first part of the {@code String} does not contain a
	 *             parsable long
	 * 
	 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
	 */
	public static Map<String, Object> getPrimaryKeyFromDamId(String damId, final String NUMERO_ARCHIVIO, final String SUB)
			throws IndexOutOfBoundsException, NumberFormatException {
		Map<String, Object> result = new HashMap<String, Object>(2);
		String sub = damId.charAt(damId.length() - 1) + "";
		String numeroArchivio = damId.substring(0, damId.length() - 1);
		result.put(NUMERO_ARCHIVIO, Long.parseLong(numeroArchivio));
		result.put(SUB, sub);

		return result;
	}

	/**
	 * Retrieves the primary key from the {@link String damId} variable that
	 * linkeding up
	 * 
	 * @param damId
	 *            variable {@code String} to unpack
	 * @return returns a {@code Map} containing the primary key
	 * @throws IndexOutOfBoundsException
	 *             if the length of the variable does not conform
	 * @throws NumberFormatException
	 *             if the first part of the {@code String} does not contain a
	 *             parsable long
	 * 
	 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
	 */
	public static Map<String, Object> getPrimaryKeyFromDamId(String damId) throws IndexOutOfBoundsException, NumberFormatException {
		return getPrimaryKeyFromDamId(damId, "NUMERO_ARCHIVIO", "SUB");
	}

	/**
	 * 
	 * @param table
	 * @param fields
	 * @return
	 * 
	 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
	 */
	public static String logicalDeleteSql(String table, String... fields) {
		String result = "UPDATE " + table + " SET CANCELLATO = \'S\' WHERE NUMERO_ARCHIVIO = ? AND SUB = ?";

		for (String field : fields) {
			result = result + " AND " + field.toUpperCase() + " = ?";
		}

		return result;
	}

	/**
	 * 
	 * @param TABLE_NAME
	 * @return
	 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
	 * @throws NextValException
	 */
	public long getNextID(final String TABLE_NAME) throws NextValException {
		String sequenceName = TABLE_NAME + "_SEQ";
		String query = "SELECT " + sequenceName + ".nextval FROM dual";
		long result = 0;

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = getNewConnection();
			if (conn == null) {
				logger.error("Database connection failed");
				throw new SQLException("Database connection failed");
			}
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs == null) {
				throw new SQLException("Something went wrong while to execute query");
			}
			if (rs.next()) {
				result = rs.getLong(1);
			} else {
				throw new NextValException("Couldn't get a value for id");
			}
		} catch (SQLException sqle) {
			DBUtility.newRollback(conn);
			logger.error("\nMessage: {}", sqle.getMessage());
			throw new NextValException(sqle.getMessage());
		} finally {
			closeQuietly(conn, rs, ps);
		}

		return result;
	}
}