package it.cinqueemmeinfo.dammap.utility;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.dao.UserService;
import it.cinqueemmeinfo.dammap.security.UserAuthentication;
import it.thematica.enterprise.app.login.UserRightsEntry;

public class UserUtility implements Serializable {

	private static final long serialVersionUID = 5829216581672367077L;
	private static final Logger logger = LoggerFactory.getLogger(UserUtility.class);

	private static final String DEBUG_USER = "D3bUGu53R";
	private static final String VISIBLE = "-S-";
	private static final String NOT_VISIBLE = "-N-";
	private static final String READONLY = "R";
	private static final String WRITEABLE = "W";
	private static final String USERRIGHTS = "urMap";
	@SuppressWarnings("unused")
	private static final String ANONYMOUS = "anonymousUser";
	private static final String NO_PERMISSION = "N";

	public static final String[] IDS_RUBRICA = { DBUtility.RUBRICA, DBUtility.RUBRICA_ESTERNI };
	public static final String[] IDS_ANAGRAFICA = { DBUtility.ANAGRAFICA };

	@Autowired
	Boolean isDebug;
	@Autowired
	DBUtility dbUtil;

	private String retrieveUserIdFromSecurityContext() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication instanceof UserAuthentication) {
			return ((UserAuthentication) authentication).getDetails().getUsername();
		} else if (authentication == null) {
			// Spring Security è disabilitato
			return null;
		}

		return authentication.getName(); // anonymous user support
	}

	public int getMaxAccessForUsersById(Integer userId) {
		int access = 0;
		if (this.hasRole(UserService.CAN_ADMIN_USERS)) {
			// Se ha il permesso di scrittura sugli utenti
			access = 2;
		} else if (this.getUserDetails().getId().intValue() == userId.intValue()) {
			// Altrimenti può acceddere solo alla sua scheda
			access = 1;
		}
		return access;
	}

	public String encodePassword(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(13);
		return this.encodePassword(passwordEncoder, password);
	}

	public String encodePassword(PasswordEncoder passwordEncoder, String password) {
		return passwordEncoder.encode(password);
	}

	private String retrieveUserIdFromRequest() {

		// obtaining request
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		// retrieving userid from request param
		String userId = request.getParameter("user");

		if (isDebug && userId == null) {
			userId = DEBUG_USER;
		} else {
			if (!isUserLogged(userId)) {
				throw new AccessDeniedException("Unauthorized: Authentication of user " + userId + " can't be confirmed.");
			}
		}

		return userId;
	}

	private String getSchedaIdFromKey(String key) throws SQLException {

		String sql = "";
		String foundIdScheda = "";
		ResultSet usrRs = null;
		PreparedStatement usrPs = null;
		Connection conn = null;
		try {

			sql = "SELECT ID_SCHEDA FROM SCHEDA_EXTRA WHERE KEY = ? ";
			// preparing statement
			conn = dbUtil.getNewConnection();
			usrPs = conn.prepareStatement(sql);
			usrPs.setString(1, key);
			// execute
			usrRs = usrPs.executeQuery();

			if (usrRs.next()) {
				foundIdScheda = usrRs.getString("ID_SCHEDA");
			}

			if ("".equals(foundIdScheda) || foundIdScheda == null) {
				throw new SQLException("No match found for the specified schedaKey.");
			}

		} catch (SQLException sqle) {
			logger.error("Error while executing '" + sql + "' \nMessage: " + sqle.getMessage());
			throw sqle;
		} finally {
			DBUtility.closeQuietly(conn, usrRs, usrPs);
		}

		return foundIdScheda;
	}

	/**
	 * checks if user is logged in, uses old token method
	 * 
	 * @param userId
	 *            username
	 * @return true if user is retrieved correctly from application context
	 */
	@Deprecated
	@SuppressWarnings("rawtypes")
	public Boolean isUserLogged(String userId) {

		Object[] userData = null;

		// obtaining request
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		ServletContext context = request.getSession().getServletContext();

		// get context hashmap with old application data
		HashMap urMap = (HashMap) context.getAttribute(USERRIGHTS);

		if (urMap != null)
			userData = (Object[]) urMap.get(userId);

		return (userData != null);
	}

	public String getUserId() {
		// Prova a recuperare lo username da Spring Security
		String username = this.retrieveUserIdFromSecurityContext();
		if (username == null) {
			// Prova a recuperare lo username dalla request (vecchio sistema) se
			// Spring non è attivo
			username = retrieveUserIdFromRequest();
		}
		return username;
	}

	public Boolean canAccessData(Integer pageId) {

		UserSecurityBean usBean = null;

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication instanceof UserAuthentication) {
			usBean = ((UserAuthentication) authentication).getDetails();
		}

		if (usBean != null) {

		}

		return true;
	}

	/**
	 * checks if user can read data from the specified page, uses the old token
	 * method
	 * 
	 * @param pageId
	 *            id of the page to read
	 * @param userId
	 *            username
	 * @return true if uer can read data from the specified page
	 */
	@Deprecated
	@SuppressWarnings("rawtypes")
	public Boolean canReadData(Integer pageId, String userId) {

		Object[] userData = null;
		UserRightsEntry edu = null;
		String nodeType = "";

		if (!isDebug) {
			// obtaining request
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			ServletContext context = request.getSession().getServletContext();

			// get context hashmap with old application data
			HashMap urMap = (HashMap) context.getAttribute(USERRIGHTS);

			if (urMap != null) {
				userData = (Object[]) urMap.get(userId);

				ByteArrayInputStream baIn = new ByteArrayInputStream((byte[]) userData[2]);
				ObjectInput objIn = null;

				try {
					objIn = new ObjectInputStream(baIn);

					edu = (UserRightsEntry) objIn.readObject();

				} catch (IOException e) {
					logger.error(e.getMessage());
				} catch (ClassNotFoundException e) {
					logger.error(e.getMessage());
				} finally {
					try {
						baIn.close();
					} catch (IOException ex) {
					}
					try {
						if (objIn != null)
							objIn.close();
					} catch (IOException ex) {
					}
				}
			}

			if (edu != null) {
				nodeType = "" + edu.getNode(pageId).getType();
			}

			return (READONLY.equals(nodeType) || WRITEABLE.equals(nodeType));
		} else {
			return true;
		}
	}

	/**
	 * Checks if user has read rights on the specified resource
	 * 
	 * @param resourceKey
	 *            resource that the user wants to read
	 * @return true if user has access rights to read that resource
	 * @throws AccessDeniedException
	 *             if user doesn't have access rights
	 * @throws SQLException
	 *             if the specified resourceKey has no corresponding resourceId
	 */
	public boolean canReadFromScheda(String resourceKey) throws SQLException {

		// String resourceId = "";
		// //Boolean hasAccess = false;
		// Boolean hasAccess = false;
		//
		// /*resourceId = getSchedaIdFromKey(resourceKey);
		// hasAccess = (hasRole(resourceId+VISIBLE+READONLY) ||
		// hasRole(resourceId+VISIBLE+WRITEABLE));
		//
		//
		// if (!hasAccess) {
		// throw new AccessDeniedException("Access Denied - User can't read this
		// resource.");
		// }*/
		//
		// resourceId = this.getSchedaIdFromKey(resourceKey);
		// if(this.hasRole(resourceId+UserUtility.VISIBLE+UserUtility.NO_PERMISSION)){
		// throw new ExternalUserException("User is external");
		// }
		// hasAccess =
		// (hasRole(resourceId+UserUtility.VISIBLE+UserUtility.READONLY) ||
		// hasRole(resourceId+UserUtility.VISIBLE+UserUtility.WRITEABLE));
		//
		// return hasAccess;
		String[] resource = { resourceKey };
		return this.getMaxPermissionForScheda(resource) >= 2;
	}

	/**
	 * Checks if user has access rights on the specified resource so he view the
	 * associated data only
	 * 
	 * @param resourceKey
	 *            resource that the user wants to read
	 * @return true if user has access rights to view that resource
	 * @throws SQLException
	 *             if the specified resourceKey has no corresponding resourceId
	 */
	public boolean canAccessScheda(String[] resourceKey) throws SQLException {
		return this.getMaxPermissionForScheda(resourceKey) >= 1;
	}

	/**
	 * Returns the highest permission a user has on an array of resources
	 * 
	 * @param resourceKey
	 *            resource that the user wants to read
	 * @return int the highest permissions as an integer, 0 if no permission
	 * @throws SQLException
	 *             if the specified resourceKey has no corresponding resourceId
	 */
	public int getMaxPermissionForScheda(String[] resourceKey) throws SQLException {
		// Ipotesi di lavoro, non ha alcun permesso sulla scheda
		int hasAccess = 0;

		// Scorre tutti i permessi disponibili (più schede possono essere
		// applicate alla stessa pagina)
		for (String resource : resourceKey) {
			// Prende l'ID della scheda dal database
			String resourceId = this.getSchedaIdFromKey(resource);
			/*
			 * Genera i 3 tipi di permessi che si possono avere sulla scheda
			 * (Solo lettura, sola scrittura, nessun permesso - vede solo gli
			 * oggetti associati tramite concessionario)
			 */
			Map<String, Integer> permissions = new HashMap<String, Integer>();
			permissions.put(resourceId + UserUtility.VISIBLE + UserUtility.NO_PERMISSION, 1);
			permissions.put(resourceId + UserUtility.NOT_VISIBLE + UserUtility.NO_PERMISSION, 1);
			permissions.put(resourceId + UserUtility.VISIBLE + UserUtility.READONLY, 2);
			permissions.put(resourceId + UserUtility.NOT_VISIBLE + UserUtility.READONLY, 2);
			permissions.put(resourceId + UserUtility.VISIBLE + UserUtility.WRITEABLE, 3);
			permissions.put(resourceId + UserUtility.NOT_VISIBLE + UserUtility.WRITEABLE, 3);

			// Scorre i possibili permessi per vedere se l'utente ne è in
			// possesso
			Set<Entry<String, Integer>> permEntries = permissions.entrySet();
			for (Entry<String, Integer> permission : permEntries) {
				// Se l'utente possiede un certo permesso allora hasAccess è il
				// massimo tra il valore attuale e quello assegnato al permesso
				if (this.hasRole(permission.getKey())) {
					hasAccess = Math.max(hasAccess, permission.getValue().intValue());
				}
			}
		}
		return hasAccess;
	}

	/**
	 * Checks if user has write rights on the specified resource
	 * 
	 * @param resourceKey
	 *            resource that the user wants to access
	 * @return true if user has access rights to write inside that resource
	 * @throws AccessDeniedException
	 *             if user doesn't have access rights
	 * @throws SQLException
	 *             if the specified resourceKey has no corresponding resourceId
	 */
	public Boolean canWriteIntoScheda(String resourceKey) throws SQLException {

		// String resourceId = "";
		// Boolean hasAccess = false;
		// Boolean hasAccess = true;

		/*
		 * resourceId = getSchedaIdFromKey(resourceKey); hasAccess =
		 * hasRole(resourceId+VISIBLE+WRITEABLE);
		 * 
		 * if (!hasAccess) { throw new AccessDeniedException(
		 * "Access Denied - User can't write this resource."); }
		 */

		// return hasAccess;
		String[] resource = { resourceKey };
		return this.getMaxPermissionForScheda(resource) >= 3;
	}

	public Boolean canRightsForEntity(ArrayList<String> targetEntity, String entityId) {

		Boolean isEntityPresent = false;
		if (targetEntity != null) {

			for (int i = 0; i < targetEntity.size(); i++)
				if (targetEntity.get(i).equals(entityId)) {
					isEntityPresent = true;
				}
		}
		return isEntityPresent;
	}

	/**
	 * Check if user ha this specific role
	 * 
	 * @param role
	 *            required role
	 * @return true if user has the required role, false otherwise
	 */
	@SuppressWarnings("unchecked")
	public boolean hasRole(String role) {
		boolean hasRole = false;
		UserSecurityBean userDetails = getUserDetails();

		if (userDetails != null) {
			Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) userDetails.getAuthorities();
			if (isRolePresent(authorities, role)) {
				hasRole = true;
			}
		}

		return hasRole;
	}

	/**
	 * Get info about currently logged in user
	 * 
	 * @return UserSecurityBean if found in the context, null otherwise
	 */
	public UserSecurityBean getUserDetails() {

		UserSecurityBean userDetails = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {
			Object principal = auth.getDetails();// auth.getPrincipal();

			if (principal instanceof UserSecurityBean) {
				userDetails = (UserSecurityBean) principal;
			}
		}

		return userDetails;
	}

	/**
	 * Check if a role is present in the authorities of current user
	 * 
	 * @param authorities
	 *            all authorities assigned to current user
	 * @param role
	 *            required authority
	 * @return true if role is present in list of authorities assigned to
	 *         current user, false otherwise
	 */
	private boolean isRolePresent(Collection<GrantedAuthority> authorities, String role) {
		boolean isRolePresent = false;
		for (GrantedAuthority grantedAuthority : authorities) {
			isRolePresent = grantedAuthority.getAuthority().equals(role);
			if (isRolePresent)
				break;
		}

		return isRolePresent;
	}

	private static void errorMessage(String readOrWrite, String scheda) throws SQLException {
		readOrWrite = readOrWrite.toLowerCase(Locale.ENGLISH);
		logger.error("Access Denied - User can't {} this resource.", readOrWrite);
		throw new AccessDeniedException(String.format("Access Denied - User can't %s '%s resource'.", readOrWrite, scheda));
	}

	public void checkCanRead(String scheda) throws SQLException {
		if (!canReadFromScheda(scheda)) {
			errorMessage("read", scheda);
		}
	}

	public void checkCanWrite(String scheda) throws SQLException {
		if (!canWriteIntoScheda(scheda)) {
			errorMessage("write", scheda);
		}
	}
}