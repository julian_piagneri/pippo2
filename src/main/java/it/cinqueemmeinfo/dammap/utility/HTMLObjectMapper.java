package it.cinqueemmeinfo.dammap.utility;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HTMLObjectMapper extends ObjectMapper {

	private static final long serialVersionUID = -4929228413035346561L;

	public HTMLObjectMapper() {
		super();
		this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		this.getFactory().setCharacterEscapes(new HTMLCharacterEscapes());
	}
}
