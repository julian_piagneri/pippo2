package it.cinqueemmeinfo.dammap.utility;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.cinqueemmeinfo.dammap.bean.BasicEntityBean;

public class BeanUtility implements Serializable {

	private static final long serialVersionUID = -7489314125038164425L;
	private static final Logger logger = LoggerFactory.getLogger(BeanUtility.class);

	public BasicEntityBean compareAndFillBean(BasicEntityBean sourceBean, BasicEntityBean targetBean) {// throws
																										// Exception
																										// {

		try {

			if (sourceBean != null && targetBean != null) {
				for (PropertyDescriptor propDesc : Introspector.getBeanInfo(targetBean.getClass()).getPropertyDescriptors()) {
					Method mtdRead = propDesc.getReadMethod();
					Method mtdWrite = propDesc.getWriteMethod();

					if (mtdRead.invoke(targetBean, new Object[0]) == null) {
						if (mtdWrite != null) {
							Object obj = mtdRead.invoke(sourceBean, new Object[0]);
							mtdWrite.invoke(targetBean, new Object[] { obj });
						}
					}
				}
			}

		} catch (Exception e) {
			logger.error("Error while comparing beans, message: " + e.getMessage());
			// throw e;
		}

		return targetBean;
	}

	public static Date convertStringToDate(String date) throws ParseException {
		return new Date((new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY)).parse(date).getTime());
	}
}
