package it.cinqueemmeinfo.dammap.utility.exceptions;

public class InvalidPasswordException extends IllegalArgumentException {

	private static final long serialVersionUID = 1656343993294620126L;

	public InvalidPasswordException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidPasswordException(String message) {
		super(message);
	}
}