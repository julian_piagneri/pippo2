package it.cinqueemmeinfo.dammap.utility.exceptions;

/**
 * @author Achugo Donald Emeka <donaldachugo@5minformatica.it>
 *
 */
public class NextValException extends Exception {

	private static final long serialVersionUID = 6444766852300033726L;

	public NextValException(String msg) {
		super(msg);
	}

	public NextValException() {
		this("Couldn't generate a new id with nextVal");
	}
}