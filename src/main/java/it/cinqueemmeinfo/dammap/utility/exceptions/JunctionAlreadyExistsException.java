package it.cinqueemmeinfo.dammap.utility.exceptions;

import java.io.IOException;

public class JunctionAlreadyExistsException extends IOException {

	private static final long serialVersionUID = 3341152364127842412L;

	public JunctionAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
	}

	public JunctionAlreadyExistsException(String message) {
		super(message);
	}
}