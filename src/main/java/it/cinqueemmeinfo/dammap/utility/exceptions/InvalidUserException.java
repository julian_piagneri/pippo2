package it.cinqueemmeinfo.dammap.utility.exceptions;

public class InvalidUserException extends IllegalArgumentException {

	private static final long serialVersionUID = 2488492364137488095L;

	public InvalidUserException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidUserException(String message) {
		super(message);
	}
	
}
