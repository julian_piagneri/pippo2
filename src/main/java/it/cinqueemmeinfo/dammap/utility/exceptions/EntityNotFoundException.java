package it.cinqueemmeinfo.dammap.utility.exceptions;

import java.io.FileNotFoundException;

public class EntityNotFoundException extends FileNotFoundException {

	private static final long serialVersionUID = 2931409246446302541L;

	public EntityNotFoundException() {
		this("Couldn't find entity with the provided params");
	}

	public EntityNotFoundException(String message) {
		super(message);
	}
}