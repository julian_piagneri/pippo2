package it.cinqueemmeinfo.dammap.utility;

import java.io.Serializable;
import java.security.MessageDigest;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class SignatureEncryptionUtil implements Serializable {

	private static final long serialVersionUID = -5926982127105122627L;
	
	public static final String ENCODING_CHARSET = "UTF-8";
    public static final String ENCRYPTION_ALGORITHM = "SHA-1";
	
	@SuppressWarnings("rawtypes")
	public Boolean checkSignature(ServletContext context, String userId, String signature, String timestamp) throws Exception, AccessDeniedException {
		
		//1. recuperare la url + parametri
		//2. recuperare parametro user name
		//2a. recuperare token dal context per quel user name
		//3. recuperare parametro signature
		//signature = uri+userParam+key
		
		String uri = "";
		String url = "";
		String query = "";
		String restKey = "";
		String generatedSignature = "";
		HttpServletRequest request = null;
		
		try {
			//obtaining request
			request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			//build url
			uri = request.getRequestURI();
			query = request.getQueryString();
			
			if (query != null && !"".equals(query)){
				url = uri + "?" + query.substring(0, query.indexOf("&"));
				
				//get context hashmap with old application data
				HashMap urMap = (HashMap)context.getAttribute("urMap");
				//get restKey
				Object[] oldAppData = (Object[])urMap.get(userId);
				restKey = (String)oldAppData[1];
				//generate signature
				generatedSignature = byteArrayToHexString(encryptToByteArray(url+restKey));
			} else {
				throw new AccessDeniedException("No querystring parameters set");
			}

		} catch (Exception e) {
			throw e;
		}
		
		//compare request signature with the generated one
		if (generatedSignature.equals(signature)) {
			return true;
		} else {
			throw new AccessDeniedException("Access token not signed properly");
		}
	}
	
	/**
	 * 
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptToByteArray(String input) throws Exception {
	    MessageDigest msgDgt = MessageDigest.getInstance(ENCRYPTION_ALGORITHM);
	    msgDgt.reset();
	    msgDgt.update(input.getBytes());
	    
	    return msgDgt.digest();
	}

	/**
	 * 
	 * @param b
	 * @return 
	 * @throws Exception
	 */
	public static String byteArrayToHexString(byte[] b) throws Exception {
		String result = "";
		
		for (int i=0; i < b.length; i++) {
			result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		
		return result;
	}
}
