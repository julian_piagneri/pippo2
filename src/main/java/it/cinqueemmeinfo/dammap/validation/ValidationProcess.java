package it.cinqueemmeinfo.dammap.validation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import it.cinqueemmeinfo.dammap.validation.validators.gen.Validator;

public class ValidationProcess {
	private Map<String, ArrayList<Validator<?>>> validators = new LinkedHashMap<String, ArrayList<Validator<?>>>();

	public void add(String fieldName, Validator<?> validator) {
		if (!this.validators.containsKey(fieldName)) {
			this.validators.put(fieldName, new ArrayList<Validator<?>>());
		}
		this.validators.get(fieldName).add(validator);
	}

	public boolean validate(ValidationBean validation) {
		Set<Entry<String, ArrayList<Validator<?>>>> validators = this.validators.entrySet();
		for (Entry<String, ArrayList<Validator<?>>> validator : validators) {
			// Prova ad effettuare la validazione, se c'è un errore allora lo
			// aggiunge ai messaggi e incrementa
			// il contatore degli errori
			ArrayList<Validator<?>> validatorList = validator.getValue();
			for (Validator<?> validatorClass : validatorList) {
				try {
					validatorClass.validate();
				} catch (ValidationException e) {
					List<String> errorMessages = e.getMessages();
					for (String errorMessage : errorMessages) {
						validation.addMessage(validator.getKey(), errorMessage);
					}
				}
			}
		}
		return !validation.hasErrors();
	}

}
