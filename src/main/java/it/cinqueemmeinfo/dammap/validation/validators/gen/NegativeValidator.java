package it.cinqueemmeinfo.dammap.validation.validators.gen;

import java.util.HashMap;

public abstract class NegativeValidator<T> extends Validator<T> {
	
	public NegativeValidator(HashMap<String, String> options){
		super(options);
	}
	public NegativeValidator() {}
	protected boolean negative=false;
	protected boolean ignoreCase=false;
	
	/**
	 * Sets matching to negative, that is, it will accept the string if it doesn't match the comparison
	 * @param negative
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T setNegativeMatch(boolean negative) {
		this.negative=negative;
		return (T) this;
	}
	
	@SuppressWarnings("unchecked")
	public T setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase=ignoreCase;
		return (T) this;
	}
}
