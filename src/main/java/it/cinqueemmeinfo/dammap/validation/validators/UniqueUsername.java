package it.cinqueemmeinfo.dammap.validation.validators;

import java.util.HashMap;

import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.validation.ValidationException;
import it.cinqueemmeinfo.dammap.validation.validators.gen.Validator;

public class UniqueUsername extends Validator<UniqueUsername> {

	private UserSecurityBean userBean;
	private UserSecurityBean otherUser;
	public static final String IGNORE_CASE = "ignore-case";

	public UniqueUsername(HashMap<String, String> options, UserSecurityBean userBean, UserSecurityBean otherUser) {
		super(options);
		this.setValue(userBean, otherUser);
	}

	public UniqueUsername(UserSecurityBean userBean, UserSecurityBean otherUser) {
		this.setValue(userBean, otherUser);
	}

	private void setValue(UserSecurityBean userBean, UserSecurityBean otherUser) {
		this.userBean = userBean;
		this.otherUser = otherUser;
	}

	@Override
	public boolean validate() throws ValidationException {
		if (this.userBean != null && this.otherUser != null) {
			// Controlla che non esista un altro utente con lo stesso username.
			// Lo username è unico quindi
			// la query di LoginDao che restituisce un solo utente va bene
			if (otherUser.getId().intValue() != this.userBean.getId().intValue()) {
				throw new ValidationException(this.messages);
			}
		}
		return true;
	}

}
