package it.cinqueemmeinfo.dammap.validation.validators;

import java.util.HashMap;

import it.cinqueemmeinfo.dammap.validation.ValidationException;
import it.cinqueemmeinfo.dammap.validation.validators.gen.NegativeValidator;

public class RegexValidator extends NegativeValidator<RegexValidator> {

	private String value;
	private String regex;

	public RegexValidator(HashMap<String, String> options, String value, String regex) {
		super(options);
		this.setValue(value, regex);
	}

	public RegexValidator(String value, String regex) {
		this.setValue(value, regex);
	}

	private void setValue(String value, String regex) {
		this.value = value;
		this.regex = regex;
	}

	@Override
	public boolean validate() throws ValidationException {
		if (this.value == null) {
			return true;
		}
		if (this.ignoreCase) {
			this.value = this.value.toLowerCase();
		}
		boolean match = this.value.matches(this.regex);
		if (this.negative) {
			match = !match;
		}
		if (!match) {
			throw new ValidationException(this.messages);
		}
		return true;
	}

}
