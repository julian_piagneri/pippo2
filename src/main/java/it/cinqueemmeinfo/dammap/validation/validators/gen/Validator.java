package it.cinqueemmeinfo.dammap.validation.validators.gen;

import java.util.ArrayList;
import java.util.HashMap;

import it.cinqueemmeinfo.dammap.validation.ValidationException;

public abstract class Validator<T> {
	
	public static String MESSAGE="message";
	
	protected ArrayList<String> messages=new ArrayList<String>();
	
	public Validator(HashMap<String, String> options){
		if(options.containsKey(Validator.MESSAGE)){
			this.messages.add(options.get(Validator.MESSAGE));
		}
	}
	
	public Validator(){}
	
	@SuppressWarnings("unchecked")
	public T setMessage(String message){
		this.messages.add(message);
		return (T) this;
	}

	public abstract boolean validate() throws ValidationException;
}
