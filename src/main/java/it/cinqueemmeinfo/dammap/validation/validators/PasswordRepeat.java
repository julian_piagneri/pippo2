package it.cinqueemmeinfo.dammap.validation.validators;

import java.util.HashMap;

import it.cinqueemmeinfo.dammap.bean.security.UserSecurityBean;
import it.cinqueemmeinfo.dammap.validation.ValidationException;
import it.cinqueemmeinfo.dammap.validation.validators.gen.Validator;

public class PasswordRepeat extends Validator<PasswordRepeat> {

	private UserSecurityBean userBean;

	public PasswordRepeat(HashMap<String, String> options, UserSecurityBean userBean) {
		super(options);
		this.setValue(userBean);
	}

	public PasswordRepeat(UserSecurityBean userBean) {
		this.setValue(userBean);
	}

	private void setValue(UserSecurityBean userBean) {
		this.userBean = userBean;
	}

	@Override
	public boolean validate() throws ValidationException {
		if (this.userBean != null) {
			// Se le password corrispondono allora
			if (userBean.getNewPassword() != null && userBean.getNewPassword().equals(userBean.getRepeatPassword())) {
				return true;
			}
			throw new ValidationException(messages);
		}
		return true;
	}

}
