package it.cinqueemmeinfo.dammap.validation.validators;

import java.util.HashMap;

import it.cinqueemmeinfo.dammap.validation.ValidationException;
import it.cinqueemmeinfo.dammap.validation.validators.gen.Validator;

public class NotEmpty extends Validator<NotEmpty> {
	
	private String value;

	public NotEmpty(HashMap<String, String> options, String value) {
		super(options);
		this.setValue(value);
	}
	
	public NotEmpty(String value) {
		this.setValue(value);
	}
	
	private void setValue(String value){
		this.value=value;
	}

	@Override
	public boolean validate() throws ValidationException {
		if(this.value!=null && this.value.isEmpty()){
			throw new ValidationException(this.messages);
		}
		return true;
	}

}
