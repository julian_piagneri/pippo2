package it.cinqueemmeinfo.dammap.validation.validators;

import java.util.HashMap;

import it.cinqueemmeinfo.dammap.validation.ValidationException;
import it.cinqueemmeinfo.dammap.validation.validators.gen.NegativeValidator;

public class ContainsValidator extends NegativeValidator<ContainsValidator> {
	
	private String value;
	private String contained;
	

	public ContainsValidator(HashMap<String, String> options, String value, String contained) {
		super(options);
		this.setValue(value, contained);
	}
	
	public ContainsValidator(String value, String contained) {
		this.setValue(value, contained);
	}
	
	private void setValue(String value, String contained) {
		this.value=value;
		this.contained=contained;
	}

	@Override
	public boolean validate() throws ValidationException {
		if(this.value==null){
			return true;
		}
		if(this.ignoreCase){
			this.value=this.value.toLowerCase();
			this.contained=this.contained.toLowerCase();
		}
		boolean contains=this.value.contains(this.contained);
		if(this.negative){
			contains=!contains;
		}
		if(!contains){
			throw new ValidationException(this.messages);
		}
		return true;
	}

}
