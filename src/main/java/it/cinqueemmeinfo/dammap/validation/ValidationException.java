package it.cinqueemmeinfo.dammap.validation;

import java.util.ArrayList;

public class ValidationException extends Exception {

	private static final long serialVersionUID = 2488492364137488095L;
	
	ArrayList<String> messages;

	public ValidationException(ArrayList<String> messages) {
		this.messages=messages;
	}

	public ArrayList<String> getMessages() {
		return this.messages;
	}
	
}
