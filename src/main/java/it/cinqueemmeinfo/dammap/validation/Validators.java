package it.cinqueemmeinfo.dammap.validation;

import java.util.HashMap;
import java.util.Map;

import it.cinqueemmeinfo.dammap.validation.validators.NotEmpty;
import it.cinqueemmeinfo.dammap.validation.validators.gen.Validator;

public class Validators {
	
	private static Map<String, Class<? extends Validator<?>>> validators=new HashMap<String, Class<? extends Validator<?>>>();
	
	static {
        Validators.validators.put("not-empty", NotEmpty.class);
    }

	public static boolean validate(String validator, Object value, String fieldName, String message, Map<String, String> messages) throws ValidationException{
		boolean check=false;
		try {
			//Carica il validatore
			Validator<?> val=Validators.getValidator(validator);
			check=Validators.validate(val, value, fieldName, message, messages);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return check;
	}
	
	public static boolean validate(Validator<?> validator, Object value, String fieldName, String message, Map<String, String> messages) throws ValidationException{
		boolean check=validator.validate();
		if(!check){
			messages.put(fieldName, message);
		}
		return check;
	}
	
	private static Validator<?> getValidator(String identifier) throws InstantiationException, IllegalAccessException{
		Class<? extends Validator<?>> clazz=Validators.validators.get(identifier);
		return clazz.newInstance();
	}
}
