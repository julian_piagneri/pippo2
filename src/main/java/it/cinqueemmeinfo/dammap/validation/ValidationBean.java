package it.cinqueemmeinfo.dammap.validation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


public class ValidationBean implements Serializable {

	private static final long serialVersionUID = 1233647382902762145L;
	
	private String status;
	private HashMap<String, ArrayList<String>> messages=new HashMap<String, ArrayList<String>>();
	private int errorsCount=0;
	
	public ValidationBean() { }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public HashMap<String, ArrayList<String>> getMessages() {
		return messages;
	}

	public void setMessages(HashMap<String, ArrayList<String>> messages) {
		this.messages = messages;
	}
	
	/**
	 * Adds a message to the errors
	 * @param key
	 * @param message
	 */
	public void addMessage(String key, String message){
		HashMap<String, ArrayList<String>> messages=this.getMessages();
		if(!messages.containsKey(key)){
			messages.put(key, new ArrayList<String>());
		}
		messages.get(key).add(message);
		this.errorsCount++;
	}
	
	public boolean hasErrors(){
		return errorsCount>0;
	}

	public int getErrorsCount() {
		return errorsCount;
	}

	public void setErrorsCount(int errorsCount) {
		this.errorsCount = errorsCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((messages == null) ? 0 : messages.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ValidationBean))
			return false;
		ValidationBean other = (ValidationBean) obj;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ValidationBean [status=" + status + ", messages=" + messages
				+ "]";
	}

}
