package it.thematica.enterprise.app.login;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({ "serial", "rawtypes", "unchecked" })
public class UserRightsEntry implements Comparable, Serializable {

  private int id = -1;
  private String userId = null;
  private String name = null;
  private boolean administrator = false;
  private String preferences = null;
  private Map nodes = null;
  
  public UserRightsEntry(String userId, boolean initNodes)
  {
    this.userId = userId;
    if (initNodes) {
      initNodes();
    }
  }
  
  public void initNodes()
  {
    this.nodes = new HashMap();
  }
  
  long getDimesion()
  {
    long dimensione = 5L;
    if (this.userId != null) {
      dimensione += this.userId.length();
    }
    if (this.preferences != null) {
      dimensione += this.preferences.length();
    }
    if (this.nodes != null) {
      for (int i = 0; i < this.nodes.size(); i++) {
        dimensione += 2L;
      }
    }
    return dimensione;
  }
  

public void setNode(int index, RightNode node)
  {
    if (this.nodes != null) {
      this.nodes.put(new Integer(index), node);
    }
  }
  
  public void setNode(int index, char visibile, char tipo)
  {
    if (this.nodes != null) {
      this.nodes.put(new Integer(index), new RightNode(visibile, tipo));
    }
  }
  
  public RightNode getNode(int index)
  {
    Object result = null;
    if (this.nodes != null) {
      result = this.nodes.get(new Integer(index));
    }
    if (result != null) {
      return (RightNode)result;
    }
    return null;
  }
  
  public boolean getNodeVisible(int index)
  {
    Object result = null;
    if (this.nodes != null) {
      result = this.nodes.get(new Integer(index));
    }
    if (result != null) {
      return ((RightNode)result).getVisible();
    }
    return false;
  }
  
  public String getUserId()
  {
    return this.userId;
  }
  
  public boolean isAdministrator()
  {
    return this.administrator;
  }
  
  public void setAdministrator(boolean newValue)
  {
    this.administrator = newValue;
  }
  
  public String getPreferences()
  {
    return this.preferences;
  }
  
  public void setPreferences(String preferenze)
  {
    this.preferences = preferenze;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public void setId(int newId)
  {
    this.id = newId;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setName(String newName)
  {
    this.name = newName;
  }
  
  public Map getNodes()
  {
    return this.nodes;
  }
  
  public void setNodes(Map newNodes)
  {
    this.nodes = newNodes;
  }
  
  public int compareTo(Object obj)
  {
    if ((obj instanceof UserRightsEntry)) {
      return this.userId.compareTo(((UserRightsEntry)obj).userId);
    }
    if ((obj instanceof String)) {
      return this.userId.compareTo((String)obj);
    }
    return 0;
  }
  
  public void print()
  {
    System.out.println("id=" + this.id);
    System.out.println("userId=" + this.userId);
    System.out.println("administrator=" + this.administrator);
    System.out.println("preferences=" + this.preferences);
    if (this.nodes != null)
    {
      Object[] keys = this.nodes.keySet().toArray();
      for (int i = 0; i < keys.length; i++)
      {
        System.out.print("nodes[" + keys[i].toString() + "]= ");
        ((RightNode)this.nodes.get(keys[i])).println();
      }
    }
    System.out.println("");
  }
}