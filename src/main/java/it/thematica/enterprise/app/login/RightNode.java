package it.thematica.enterprise.app.login;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RightNode implements Serializable
{
  static char HIDDEN = 'N';
  static char VISIBLE = 'S';
  static char READONLY = 'R';
  static char WRITEABLE = 'W';
  static char FORBIDDEN = 'N';
  private char visible;
  private char type;
  
  RightNode() {}
  
  RightNode(char visibile, char tipo)
  {
    this.visible = visibile;
    this.type = tipo;
  }
  
  RightNode(RightNode obj)
  {
    this.visible = obj.visible;
    this.type = obj.type;
  }
  
  long getDimesion()
  {
    return 2L;
  }
  
  public boolean getVisible()
  {
    return this.visible == 'S';
  }
  
  public char getType()
  {
    return this.type;
  }
  
  public void setVisible(char newVisible)
  {
    this.visible = newVisible;
  }
  
  public void setType(char newType)
  {
    this.type = newType;
  }
  
  void println()
  {
    System.out.println("visible=" + this.visible + ", type=" + this.type);
  }
}